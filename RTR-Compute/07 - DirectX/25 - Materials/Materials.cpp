#include "Icon.h"

#include <d3d11.h>
#include <d3dcompiler.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#include "Sphere.h"

#pragma warning(disable : 4838)  // to suppress unsigned int to int conversion warning
#include "XNAMath_204\xnamath.h"

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

int gHeight_ak = 600;
int gWidth_ak = 800;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gpFile_ak = NULL;
HWND ghwnd_ak = NULL;
DWORD dwStyle_ak = NULL;
WINDOWPLACEMENT wpPrev_ak = {sizeof(WINDOWPLACEMENT)};
bool gbFullScreen_ak = false;

bool gbActiveWindow_ak = false;
char gszLogFileName_ak[] = "Logs.txt";

IDXGISwapChain *gpIDXGISwapChain_ak = NULL;
ID3D11Device *gpID3D11Device_ak = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext_ak = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView_ak = NULL;

ID3D11VertexShader *gpID3D11VertexShader_ak = NULL;
ID3D11PixelShader *gpID3D11PixelShader_ak = NULL;

ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Position_Sphere_ak = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Normal_Sphere_ak = NULL;
ID3D11Buffer *gpID3D11Buffer_IndexBuffer_ak = NULL;

ID3D11InputLayout *gpID3D11InputLayout_ak = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer_ak = NULL;

ID3D11RasterizerState *gpID3D11RasterizerState_ak = NULL;

ID3D11DepthStencilView *gpID3D11DepthStencilView_ak = NULL;

struct CBUFFER {
    XMMATRIX WorldMatrix;
    XMMATRIX ViewMatrix;
    XMMATRIX ProjectionMatrix;

    XMVECTOR La;
    XMVECTOR Ld;
    XMVECTOR Ls;
    XMVECTOR LightPosition;

    XMVECTOR Ka;
    XMVECTOR Kd;
    XMVECTOR Ks;
    float MaterialShininess;

    UINT KeyPressed;
};
bool bLight_ak = false;

float lightAmbient_ak[] = {0.0f, 0.0f, 0.0f, 1.0f};
float lightDiffused_ak[] = {1.0f, 1.0f, 1.0f, 1.0f};
float lightPosition_ak[] = {100.0f, 100.0f, -100.0f, 1.0f};
float lightSpecular_ak[] = {1.0f, 1.0f, 1.0f, 1.0f};

float materialAmbient_ak[] = {0.0f, 0.0f, 0.0f, 1.0f};
float materialDiffused_ak[] = {1.0f, 1.0f, 1.0f, 1.0f};
float materialSpecular_ak[] = {1.0f, 1.0f, 1.0f, 1.0f};
float materialShininess_ak = 50.0f;

XMMATRIX gPerspectiveProjectionMatrix_ak;

float gClearColor_ak[4];

float sphere_vertices_ak[1146];
float sphere_normals_ak[1146];
float sphere_textures_ak[764];
unsigned short sphere_elements_ak[2280];

int gNumVertices_ak;
int gNumElements_ak;

float angleForXRotation_ak = 0.0f;
float angleForYRotation_ak = 0.0f;
float angleForZRotation_ak = 0.0f;

int keyPressed_ak = 0;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
    HRESULT Initialize(void);
    void Display(void);
    void Uninitialize(void);
    void Update(void);
    WNDCLASSEX wndclass_ak;
    HWND hwnd_ak;
    MSG msg_ak;
    TCHAR szAppName_ak[] = TEXT("MyApp");
    bool bDone_ak = false;

    if (fopen_s(&gpFile_ak, gszLogFileName_ak, "w") != 0) {
        MessageBox(NULL, TEXT("Failure creating file"), TEXT("File IO failed"), MB_ICONSTOP | MB_OK);
        exit(0);
    } else {
        fclose(gpFile_ak);
    }

    int iScreenWidth = GetSystemMetrics(SM_CXSCREEN);
    int iScreenHeight = GetSystemMetrics(SM_CYSCREEN);

    int iScreenCenterX = iScreenWidth / 2;
    int iScreenCenterY = iScreenHeight / 2;

    int iWindowCenterX = WIN_WIDTH / 2;
    int iWindowsCenterY = WIN_HEIGHT / 2;

    int iWindowX = iScreenCenterX - iWindowCenterX;
    int iWindowY = iScreenCenterY - iWindowsCenterY;

    wndclass_ak.cbSize = sizeof(WNDCLASSEX);
    wndclass_ak.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wndclass_ak.cbClsExtra = 0;
    wndclass_ak.cbWndExtra = 0;
    wndclass_ak.hInstance = hInstance;
    wndclass_ak.lpfnWndProc = WndProc;
    wndclass_ak.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));
    wndclass_ak.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass_ak.lpszClassName = szAppName_ak;
    wndclass_ak.lpszMenuName = NULL;
    wndclass_ak.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass_ak.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));

    RegisterClassEx(&wndclass_ak);

    hwnd_ak = CreateWindowEx(
        WS_EX_APPWINDOW, szAppName_ak, TEXT("D3D11 Materials"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, iWindowX, iWindowY, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);
    ghwnd_ak = hwnd_ak;

    HRESULT hr_ak = Initialize();
    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "Initialize() failed.\n");
        fclose(gpFile_ak);
        hwnd_ak = NULL;
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "Initialize() succeeded.\n");
        fclose(gpFile_ak);
    }
    ShowWindow(hwnd_ak, iCmdShow);
    SetForegroundWindow(hwnd_ak);
    SetFocus(hwnd_ak);

    while (bDone_ak == false) {
        if (PeekMessage(&msg_ak, NULL, 0, 0, PM_REMOVE)) {
            if (msg_ak.message == WM_QUIT) {
                bDone_ak = true;
            } else {
                TranslateMessage(&msg_ak);
                DispatchMessage(&msg_ak);
            }
        } else {
            if (gbActiveWindow_ak == true) {
                Display();
                Update();
            }
        }
    }
    Uninitialize();

    return ((int)msg_ak.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
    void ToggleFullScreen(void);
    HRESULT Resize(int, int);
    void Uninitialize(void);
    HRESULT hr_ak;
    switch (iMsg) {
        case WM_SETFOCUS: {
            gbActiveWindow_ak = true;
            break;
        }
        case WM_KILLFOCUS: {
            gbActiveWindow_ak = false;
            break;
        }
        case WM_ERASEBKGND: {
            return (0);
        }
        case WM_SIZE: {
            if (gpID3D11DeviceContext_ak) {
                hr_ak = Resize(LOWORD(lParam), HIWORD(lParam));
                if (FAILED(hr_ak)) {
                    fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
                    fprintf_s(gpFile_ak, "Resize() Failed.\n");
                    fclose(gpFile_ak);
                    return (hr_ak);
                } else {
                    fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
                    fprintf_s(gpFile_ak, "Resize() Succeeded.\n");
                    fclose(gpFile_ak);
                }
            }
            break;
        }
        case WM_KEYDOWN: {
            switch (wParam) {
                case VK_ESCAPE: {
                    DestroyWindow(hwnd);
                    break;
                }
                case 0x46:
                case 0x66: {
                    ToggleFullScreen();
                    break;
                }
                default: {
                    break;
                }
            }
            break;
        }
        case WM_CHAR: {
            switch (wParam) {
                case 'L':
                case 'l': {
                    if (bLight_ak) {
                        bLight_ak = false;
                    } else {
                        bLight_ak = true;
                    }
                    break;
                }
                case 'x':
                case 'X': {
                    keyPressed_ak = 1;
                    angleForXRotation_ak = 0.0f;
                    angleForYRotation_ak = 0.0f;
                    angleForZRotation_ak = 0.0f;
                    lightPosition_ak[0] = 0.0f;
                    lightPosition_ak[1] = 0.0f;
                    lightPosition_ak[2] = 0.0f;
                    break;
                }
                case 'y':
                case 'Y': {
                    keyPressed_ak = 2;
                    angleForXRotation_ak = 0.0f;
                    angleForYRotation_ak = 0.0f;
                    angleForZRotation_ak = 0.0f;
                    lightPosition_ak[0] = 0.0f;
                    lightPosition_ak[1] = 0.0f;
                    lightPosition_ak[2] = 0.0f;
                    break;
                }
                case 'z':
                case 'Z': {
                    keyPressed_ak = 3;
                    angleForXRotation_ak = 0.0f;
                    angleForYRotation_ak = 0.0f;
                    angleForZRotation_ak = 0.0f;
                    lightPosition_ak[0] = 0.0f;
                    lightPosition_ak[1] = 0.0f;
                    lightPosition_ak[2] = 0.0f;
                    break;
                }
                case 's':
                case 'S': {
                    keyPressed_ak = 0;
                    angleForXRotation_ak = 0.0f;
                    angleForYRotation_ak = 0.0f;
                    angleForZRotation_ak = 0.0f;
                    lightPosition_ak[0] = 0.0f;
                    lightPosition_ak[1] = 0.0f;
                    lightPosition_ak[2] = 0.0f;
                    break;
                }
            }
            break;
        }
        case WM_CLOSE: {
            DestroyWindow(hwnd);
            break;
        }
        case WM_DESTROY: {
            Uninitialize();
            PostQuitMessage(0);
            break;
        }
        default: {
            break;
        }
    }

    return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void) {
    MONITORINFO mi_ak = {sizeof(MONITORINFO)};

    if (gbFullScreen_ak == false) {

        dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);
        if ((dwStyle_ak & WS_OVERLAPPEDWINDOW)) {
            if ((GetWindowPlacement(ghwnd_ak, &wpPrev_ak) &&
                 (GetMonitorInfo(MonitorFromWindow(ghwnd_ak, MONITORINFOF_PRIMARY), &mi_ak)))) {
                SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak & (~WS_OVERLAPPEDWINDOW)));
                SetWindowPos(ghwnd_ak,
                             HWND_TOP,
                             mi_ak.rcMonitor.left,
                             mi_ak.rcMonitor.top,
                             (mi_ak.rcMonitor.right - mi_ak.rcMonitor.left),
                             (mi_ak.rcMonitor.bottom - mi_ak.rcMonitor.top),
                             SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }
        ShowCursor(FALSE);
        gbFullScreen_ak = true;
    } else {
        SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak | WS_OVERLAPPEDWINDOW));
        SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
        SetWindowPos(ghwnd_ak,
                     HWND_TOP,
                     0,
                     0,
                     0,
                     0,
                     (SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOOWNERZORDER));
        ShowCursor(TRUE);
        gbFullScreen_ak = false;
    }
}

HRESULT Initialize(void) {
    void Uninitialize(void);
    HRESULT Resize(int, int);

    HRESULT hr_ak;

    D3D_DRIVER_TYPE d3dDriverType_ak;
    D3D_DRIVER_TYPE d3dDriverTypes[] =
        {
            D3D_DRIVER_TYPE_HARDWARE,
            D3D_DRIVER_TYPE_WARP,
            D3D_DRIVER_TYPE_REFERENCE};

    D3D_FEATURE_LEVEL d3dFeatureLevel_required_ak = D3D_FEATURE_LEVEL_11_0;
    D3D_FEATURE_LEVEL d3dFeatureLevel_acquired_ak = D3D_FEATURE_LEVEL_10_0;

    UINT createDeviceFlags_ak = 0;
    UINT numDriverTypes_ak = 0;
    UINT numFeatureLevels_ak = 1;

    numDriverTypes_ak = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);

    DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
    ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
    dxgiSwapChainDesc.BufferCount = 1;
    dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
    dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
    dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
    dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
    dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    dxgiSwapChainDesc.OutputWindow = ghwnd_ak;
    dxgiSwapChainDesc.SampleDesc.Count = 1;
    dxgiSwapChainDesc.SampleDesc.Quality = 0;
    dxgiSwapChainDesc.Windowed = TRUE;

    for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes_ak; driverTypeIndex++) {
        d3dDriverType_ak = d3dDriverTypes[driverTypeIndex];
        hr_ak = D3D11CreateDeviceAndSwapChain(
            NULL,
            d3dDriverType_ak,
            NULL,
            createDeviceFlags_ak,
            &d3dFeatureLevel_required_ak,
            numFeatureLevels_ak,
            D3D11_SDK_VERSION,
            &dxgiSwapChainDesc,
            &gpIDXGISwapChain_ak,
            &gpID3D11Device_ak,
            &d3dFeatureLevel_acquired_ak,
            &gpID3D11DeviceContext_ak);
        if (SUCCEEDED(hr_ak))
            break;
    }
    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "D3D11CreateDeviceAndSwapChain() Failed.\n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "D3D11CreateDeviceAndSwapChain() Succeeded.\n");

        fprintf_s(gpFile_ak, "The Chosen Driver is of ");
        if (d3dDriverType_ak == D3D_DRIVER_TYPE_HARDWARE) {
            fprintf_s(gpFile_ak, "Hardware Type.\n");
        } else if (d3dDriverType_ak == D3D_DRIVER_TYPE_WARP) {
            fprintf_s(gpFile_ak, "WRAP Type.\n");
        } else if (d3dDriverType_ak == D3D_DRIVER_TYPE_REFERENCE) {
            fprintf_s(gpFile_ak, "Reference Type.\n");
        } else {
            fprintf_s(gpFile_ak, "Unknown Type.\n");
        }

        fprintf_s(gpFile_ak, "The Supported Highest Feature Level Is");

        if (d3dFeatureLevel_acquired_ak == D3D_FEATURE_LEVEL_11_0) {
            fprintf_s(gpFile_ak, "11.0\n");
        }
        if (d3dFeatureLevel_acquired_ak == D3D_FEATURE_LEVEL_10_1) {
            fprintf_s(gpFile_ak, "10.1\n");
        }
        if (d3dFeatureLevel_acquired_ak == D3D_FEATURE_LEVEL_10_0) {
            fprintf_s(gpFile_ak, "10.0\n");
        } else {
            fprintf_s(gpFile_ak, "Unknown\n");
        }

        fclose(gpFile_ak);
    }

    const char *vertexShaderSourceCode_ak =
        "cbuffer ConstantBuffer"
        "{"
        "float4x4 worldMatrix;"
        "float4x4 viewMatrix;"
        "float4x4 projectionMatrix;"
        "float3 la;"
        "float3 ld;"
        "float4 ls;"
        "float4 lightPosition;"
        "float3 ka;"
        "float3 kd;"
        "float4 ks;"
        "float materialShininess;"
        "uint keyPressed;"
        "}"
        "struct vertex_output"
        "{"
        "float4 position: SV_POSITION;"
        "float3 transformed_normals : NORMAL0;"
        "float3 light_direction : NORMAL1;"
        "float3 viewer_vector : NORMAL2;"
        "};"
        "vertex_output main(float4 pos: POSITION, float4 normals: NORMAL)"
        "{"
        "vertex_output output;"
        "if(keyPressed == 1)"
        "{"
        "float4 eyePosition = mul(worldMatrix, pos);"
        "eyePosition = mul(viewMatrix, eyePosition);"
        "output.transformed_normals = mul((float3x3)worldMatrix, (float3)normals);"
        "output.light_direction = (float3)(lightPosition) - eyePosition.xyz;"
        "output.viewer_vector = -eyePosition.xyz;"
        "}"
        "float4 position = mul(worldMatrix, pos);"
        "position = mul(viewMatrix, position);"
        "position = mul(projectionMatrix, position);"
        "output.position = position;"
        "return(output);"
        "}";
    ID3DBlob *pID3DBlob_VertexShaderCode_ak = NULL;
    ID3DBlob *pID3DBlob_Error_ak = NULL;

    hr_ak = D3DCompile(vertexShaderSourceCode_ak,
                        lstrlenA(vertexShaderSourceCode_ak) + 1,
                        "VS",
                        NULL,
                        D3D_COMPILE_STANDARD_FILE_INCLUDE,
                        "main",
                        "vs_5_0",
                        0,
                        0,
                        &pID3DBlob_VertexShaderCode_ak,
                        &pID3DBlob_Error_ak);

    if (FAILED(hr_ak)) {
        if (pID3DBlob_Error_ak != NULL) {
            fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
            fprintf_s(gpFile_ak, "D3DCompile() Failed For Vertex Shader : %s.\n", (char *)pID3DBlob_Error_ak->GetBufferPointer());
            fclose(gpFile_ak);
            pID3DBlob_Error_ak->Release();
            pID3DBlob_Error_ak = NULL;
            return (hr_ak);
        } else {
            fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
            fprintf_s(gpFile_ak, "D3DCompile() Failed For Vertex Shader due to some COM error.\n");
            fclose(gpFile_ak);
        }
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "D3DCompile() Succeeded For Vertex Shader.\n");
        fclose(gpFile_ak);
    }

    hr_ak = gpID3D11Device_ak->CreateVertexShader(pID3DBlob_VertexShaderCode_ak->GetBufferPointer(),
                                                    pID3DBlob_VertexShaderCode_ak->GetBufferSize(),
                                                    NULL,
                                                    &gpID3D11VertexShader_ak);

    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateVertexShader() Failed .\n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateVertexShader() Succeeded.\n");
        fclose(gpFile_ak);
    }

    gpID3D11DeviceContext_ak->VSSetShader(gpID3D11VertexShader_ak, NULL, 0);

    const char *pixelShaderSourceCode_ak =
        "cbuffer ConstantBuffer"
        "{"
        "float4x4 worldMatrix;"
        "float4x4 viewMatrix;"
        "float4x4 projectionMatrix;"
        "float3 la;"
        "float3 ld;"
        "float4 ls;"
        "float4 lightPosition;"
        "float3 ka;"
        "float3 kd;"
        "float4 ks;"
        "float materialShininess;"
        "uint keyPressed;"
        "}"
        "struct vertex_output"
        "{"
        "float4 position: SV_POSITION;"
        "float3 transformed_normals : NORMAL0;"
        "float3 light_direction : NORMAL1;"
        "float3 viewer_vector : NORMAL2;"
        "};"
        "float4 main(vertex_output input): SV_TARGET"
        "{"
        "float3 phong_ads_light;"
        "if(keyPressed == 1)"
        "{"
        "float3 normalized_transformed_normals = normalize(input.transformed_normals);"
        "float3 normalized_light_direction = normalize(input.light_direction);"
        "float3 normalized_viewer_vector = normalize(input.viewer_vector);"
        "float3 ambient = (float3)(la * ka);"
        "float3 diffuse = (float3)(ld * kd) * max(dot(normalized_light_direction, normalized_transformed_normals), 0.0);"
        "float3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);"
        "float3 specular = (float3)(ls * ks) * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), materialShininess);"
        "phong_ads_light = ambient + diffuse + specular;"
        "}"
        "else"
        "{"
        "phong_ads_light = float3(1.0f, 1.0f, 1.0f);"
        "}"
        "float4 color = float4(phong_ads_light, 1.0f);"
        "return(color);"
        "}";

    ID3DBlob *pID3DBlob_PixelShaderCode_ak = NULL;
    pID3DBlob_Error_ak = NULL;

    hr_ak = D3DCompile(pixelShaderSourceCode_ak,
                        lstrlenA(pixelShaderSourceCode_ak) + 1,
                        "PS",
                        NULL,
                        D3D_COMPILE_STANDARD_FILE_INCLUDE,
                        "main",
                        "ps_5_0",
                        0,
                        0,
                        &pID3DBlob_PixelShaderCode_ak,
                        &pID3DBlob_Error_ak);

    if (FAILED(hr_ak)) {
        if (pID3DBlob_Error_ak != NULL) {
            fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
            fprintf_s(gpFile_ak, "D3DCompile() Failed For Pixel Shader : %s.\n", (char *)pID3DBlob_Error_ak->GetBufferPointer());
            fclose(gpFile_ak);
            pID3DBlob_Error_ak->Release();
            pID3DBlob_Error_ak = NULL;
            return (hr_ak);
        } else {
            fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
            fprintf_s(gpFile_ak, "D3DCompile() Failed For Pixel Shader due to some COM error.\n");
            fclose(gpFile_ak);
        }
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "D3DCompile() Succeeded For Pixel Shader\n");
        fclose(gpFile_ak);
    }

    hr_ak = gpID3D11Device_ak->CreatePixelShader(pID3DBlob_PixelShaderCode_ak->GetBufferPointer(),
                                                   pID3DBlob_PixelShaderCode_ak->GetBufferSize(),
                                                   NULL,
                                                   &gpID3D11PixelShader_ak);

    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreatePixelShader() Failed \n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreatePixelShader() Succeeded.\n");
        fclose(gpFile_ak);
    }

    gpID3D11DeviceContext_ak->PSSetShader(gpID3D11PixelShader_ak, NULL, 0);
    pID3DBlob_PixelShaderCode_ak->Release();
    pID3DBlob_PixelShaderCode_ak = NULL;

    D3D11_INPUT_ELEMENT_DESC inputElementDesc_ak[2];

    inputElementDesc_ak[0].SemanticName = "POSITION";
    inputElementDesc_ak[0].SemanticIndex = 0;
    inputElementDesc_ak[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
    inputElementDesc_ak[0].InputSlot = 0;
    inputElementDesc_ak[0].AlignedByteOffset = 0;
    inputElementDesc_ak[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
    inputElementDesc_ak[0].InstanceDataStepRate = 0;

    inputElementDesc_ak[1].SemanticName = "NORMAL";
    inputElementDesc_ak[1].SemanticIndex = 0;
    inputElementDesc_ak[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
    inputElementDesc_ak[1].InputSlot = 1;
    inputElementDesc_ak[1].AlignedByteOffset = 0;
    inputElementDesc_ak[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
    inputElementDesc_ak[1].InstanceDataStepRate = 0;

    hr_ak = gpID3D11Device_ak->CreateInputLayout(inputElementDesc_ak, _ARRAYSIZE(inputElementDesc_ak),
                                                   pID3DBlob_VertexShaderCode_ak->GetBufferPointer(),
                                                   pID3DBlob_VertexShaderCode_ak->GetBufferSize(),
                                                   &gpID3D11InputLayout_ak);

    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "D3D11Device::CreateInput() Failed.\n");
        fclose(gpFile_ak);
        pID3DBlob_VertexShaderCode_ak->Release();
        pID3DBlob_VertexShaderCode_ak = NULL;
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateInput() Succeeded.\n");
        fclose(gpFile_ak);
        pID3DBlob_VertexShaderCode_ak->Release();
        pID3DBlob_VertexShaderCode_ak = NULL;
    }

    getSphereVertexData(sphere_vertices_ak, sphere_normals_ak, sphere_textures_ak, sphere_elements_ak);
    gNumVertices_ak = getNumberOfSphereVertices();
    gNumElements_ak = getNumberOfSphereElements();

    D3D11_BUFFER_DESC bufferDesc_ak;

    ZeroMemory(&bufferDesc_ak, sizeof(D3D11_BUFFER_DESC));

    bufferDesc_ak.Usage = D3D11_USAGE_DYNAMIC;
    bufferDesc_ak.ByteWidth = sizeof(float) * gNumVertices_ak * 3;
    bufferDesc_ak.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    bufferDesc_ak.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

    hr_ak = gpID3D11Device_ak->CreateBuffer(&bufferDesc_ak, NULL,
                                              &gpID3D11Buffer_VertexBuffer_Position_Sphere_ak);

    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateBuffer() for sphere vertices Failed For Vertex Buffer.\n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateBuffer() for sphere vertices Succeeded For Vertex Buffer.\n");
        fclose(gpFile_ak);
    }
    gpID3D11DeviceContext_ak->IASetInputLayout(gpID3D11InputLayout_ak);
    D3D11_MAPPED_SUBRESOURCE mappedSubresource_ak;
    ZeroMemory(&mappedSubresource_ak, sizeof(D3D11_MAPPED_SUBRESOURCE));
    gpID3D11DeviceContext_ak->Map(gpID3D11Buffer_VertexBuffer_Position_Sphere_ak, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource_ak);
    memcpy(mappedSubresource_ak.pData, sphere_vertices_ak, sizeof(float) * gNumVertices_ak * 3);
    gpID3D11DeviceContext_ak->Unmap(gpID3D11Buffer_VertexBuffer_Position_Sphere_ak, NULL);

    ZeroMemory(&bufferDesc_ak, sizeof(D3D11_BUFFER_DESC));

    bufferDesc_ak.Usage = D3D11_USAGE_DYNAMIC;
    bufferDesc_ak.ByteWidth = sizeof(float) * gNumVertices_ak * 3;
    bufferDesc_ak.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    bufferDesc_ak.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

    hr_ak = gpID3D11Device_ak->CreateBuffer(&bufferDesc_ak, NULL,
                                              &gpID3D11Buffer_VertexBuffer_Normal_Sphere_ak);

    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateBuffer() for sphere normals Failed For Vertex Buffer.\n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateBuffer() for sphere normals Succeeded For Vertex Buffer.\n");
        fclose(gpFile_ak);
    }

    ZeroMemory(&mappedSubresource_ak, sizeof(D3D11_MAPPED_SUBRESOURCE));
    gpID3D11DeviceContext_ak->Map(gpID3D11Buffer_VertexBuffer_Normal_Sphere_ak, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource_ak);
    memcpy(mappedSubresource_ak.pData, sphere_normals_ak, sizeof(float) * gNumVertices_ak * 3);
    gpID3D11DeviceContext_ak->Unmap(gpID3D11Buffer_VertexBuffer_Normal_Sphere_ak, NULL);

    // index buffer

    ZeroMemory(&bufferDesc_ak, sizeof(D3D11_BUFFER_DESC));

    bufferDesc_ak.Usage = D3D11_USAGE_DYNAMIC;
    bufferDesc_ak.ByteWidth = sizeof(short) * gNumElements_ak;
    bufferDesc_ak.BindFlags = D3D11_BIND_INDEX_BUFFER;
    bufferDesc_ak.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

    hr_ak = gpID3D11Device_ak->CreateBuffer(&bufferDesc_ak, NULL,
                                              &gpID3D11Buffer_IndexBuffer_ak);

    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateBuffer() for sphere indices Failed For Vertex Buffer.\n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateBuffer() for sphere indices Succeeded For Vertex Buffer.\n");
        fclose(gpFile_ak);
    }

    ZeroMemory(&mappedSubresource_ak, sizeof(D3D11_MAPPED_SUBRESOURCE));
    gpID3D11DeviceContext_ak->Map(gpID3D11Buffer_IndexBuffer_ak, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource_ak);
    memcpy(mappedSubresource_ak.pData, sphere_elements_ak, sizeof(short) * gNumElements_ak);
    gpID3D11DeviceContext_ak->Unmap(gpID3D11Buffer_IndexBuffer_ak, NULL);

    D3D11_BUFFER_DESC bufferDesc_ConstantBuffer_ak;
    ZeroMemory(&bufferDesc_ConstantBuffer_ak, sizeof(D3D11_BUFFER_DESC));
    bufferDesc_ConstantBuffer_ak.Usage = D3D11_USAGE_DEFAULT;
    bufferDesc_ConstantBuffer_ak.ByteWidth = sizeof(CBUFFER);
    bufferDesc_ConstantBuffer_ak.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    hr_ak = gpID3D11Device_ak->CreateBuffer(&bufferDesc_ConstantBuffer_ak, nullptr, &gpID3D11Buffer_ConstantBuffer_ak);

    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateBuffer() Failed for Constant Buffer\n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateBuffer() Succeeded for Constant Buffer.\n");
        fclose(gpFile_ak);
    }

    gpID3D11DeviceContext_ak->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer_ak);
    gpID3D11DeviceContext_ak->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer_ak);

    // create and set Rasterizer State

    D3D11_RASTERIZER_DESC d3d11RasterizedDesc;
    ZeroMemory(&d3d11RasterizedDesc, sizeof(D3D11_RASTERIZER_DESC));

    d3d11RasterizedDesc.AntialiasedLineEnable = FALSE;
    d3d11RasterizedDesc.CullMode = D3D11_CULL_NONE;
    d3d11RasterizedDesc.DepthBias = 0;
    d3d11RasterizedDesc.DepthBiasClamp = 0.0f,
    d3d11RasterizedDesc.DepthClipEnable = TRUE;
    d3d11RasterizedDesc.FillMode = D3D11_FILL_SOLID;
    d3d11RasterizedDesc.FrontCounterClockwise = FALSE;
    d3d11RasterizedDesc.MultisampleEnable = FALSE;
    d3d11RasterizedDesc.ScissorEnable = FALSE;
    d3d11RasterizedDesc.SlopeScaledDepthBias = 0.0f;

    hr_ak = gpID3D11Device_ak->CreateRasterizerState(&d3d11RasterizedDesc,
                                                       &gpID3D11RasterizerState_ak);

    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateRasterizerState() failed for culling\n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateRasterizerState() succeeded for culling\n");
        fclose(gpFile_ak);
    }

    gpID3D11DeviceContext_ak->RSSetState(gpID3D11RasterizerState_ak);

    gClearColor_ak[0] = 0.25f;
    gClearColor_ak[1] = 0.25f;
    gClearColor_ak[2] = 0.25f;
    gClearColor_ak[3] = 1.0f;
    gPerspectiveProjectionMatrix_ak = XMMatrixIdentity();

    hr_ak = Resize(WIN_WIDTH, WIN_HEIGHT);
    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "Resize() Failed.\n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "Resize() Succeeded.\n");
        fclose(gpFile_ak);
    }
    return (S_OK);
}

HRESULT Resize(int width, int height) {
    HRESULT hr_ak = S_OK;
    gHeight_ak = height;
    gWidth_ak = width;
    if (gpID3D11RenderTargetView_ak) {
        gpID3D11RenderTargetView_ak->Release();
        gpID3D11RenderTargetView_ak = NULL;
    }

    gpIDXGISwapChain_ak->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

    ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
    gpIDXGISwapChain_ak->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID *)&pID3D11Texture2D_BackBuffer);

    hr_ak = gpID3D11Device_ak->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView_ak);
    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "CreateRenderTargetView() Failed.\n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "CreateRenderTargetView() Succeeded.\n");
        fclose(gpFile_ak);
    }

    pID3D11Texture2D_BackBuffer->Release();
    pID3D11Texture2D_BackBuffer = NULL;

    // Depth

    if (gpID3D11DepthStencilView_ak) {
        gpID3D11DepthStencilView_ak->Release();
        gpID3D11DepthStencilView_ak = NULL;
    }

    D3D11_TEXTURE2D_DESC d3d11TextureDesc_ak;
    ZeroMemory(&d3d11TextureDesc_ak, sizeof(D3D11_TEXTURE2D_DESC));
    // Necessary for depth
    d3d11TextureDesc_ak.Width = (UINT)width;
    d3d11TextureDesc_ak.Height = (UINT)height;
    d3d11TextureDesc_ak.Format = DXGI_FORMAT_D32_FLOAT;
    d3d11TextureDesc_ak.Usage = D3D11_USAGE_DEFAULT;
    d3d11TextureDesc_ak.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    d3d11TextureDesc_ak.SampleDesc.Count = 1;
    d3d11TextureDesc_ak.SampleDesc.Quality = 0;
    // Extra
    d3d11TextureDesc_ak.ArraySize = 1;
    d3d11TextureDesc_ak.MipLevels = 1;
    d3d11TextureDesc_ak.CPUAccessFlags = 0;
    d3d11TextureDesc_ak.MiscFlags = 0;

    ID3D11Texture2D *pID3D11Texture2D_DepthBuffer_ak = NULL;
    gpID3D11Device_ak->CreateTexture2D(&d3d11TextureDesc_ak, NULL,
                                        &pID3D11Texture2D_DepthBuffer_ak);

    D3D11_DEPTH_STENCIL_VIEW_DESC d3d11DepthStencilViewDesc_ak;
    ZeroMemory(&d3d11DepthStencilViewDesc_ak, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

    d3d11DepthStencilViewDesc_ak.Format = DXGI_FORMAT_D32_FLOAT;
    d3d11DepthStencilViewDesc_ak.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

    hr_ak = gpID3D11Device_ak->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer_ak, &d3d11DepthStencilViewDesc_ak,
                                                        &gpID3D11DepthStencilView_ak);

    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "CreateDepthStencilView() Failed.\n");
        fclose(gpFile_ak);
        return hr_ak;
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "CreateDepthStencilView() Succeeded.\n");
        fclose(gpFile_ak);
    }

    pID3D11Texture2D_DepthBuffer_ak->Release();
    pID3D11Texture2D_DepthBuffer_ak = NULL;

    if (height < 0) {
        height = 1;
    }

    gpID3D11DeviceContext_ak->OMSetRenderTargets(1, &gpID3D11RenderTargetView_ak, gpID3D11DepthStencilView_ak);

    D3D11_VIEWPORT d3dViewPort;
    d3dViewPort.TopLeftX = 0;
    d3dViewPort.TopLeftY = 0;
    d3dViewPort.Width = (float)width;
    d3dViewPort.Height = (float)height;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    gPerspectiveProjectionMatrix_ak = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0), (float)width / (float)height, 0.1f, 100.0f);

    return hr_ak;
}

void Display(void) {
    gpID3D11DeviceContext_ak->ClearRenderTargetView(gpID3D11RenderTargetView_ak, gClearColor_ak);
    gpID3D11DeviceContext_ak->ClearDepthStencilView(gpID3D11DepthStencilView_ak, D3D11_CLEAR_DEPTH, 1.0f, 0);

    UINT stride_ak = sizeof(float) * 3;
    UINT offset_ak = 0;

    gpID3D11DeviceContext_ak->IASetVertexBuffers(0, 1, &gpID3D11Buffer_VertexBuffer_Position_Sphere_ak, &stride_ak, &offset_ak);

    stride_ak = sizeof(float) * 3;
    offset_ak = 0;

    gpID3D11DeviceContext_ak->IASetVertexBuffers(1, 1, &gpID3D11Buffer_VertexBuffer_Normal_Sphere_ak, &stride_ak, &offset_ak);
    gpID3D11DeviceContext_ak->IASetIndexBuffer(gpID3D11Buffer_IndexBuffer_ak, DXGI_FORMAT_R16_UINT, 0);

    gpID3D11DeviceContext_ak->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

    XMMATRIX worldMatrix = XMMatrixIdentity();
    XMMATRIX translationMatrix = XMMatrixIdentity();
    XMMATRIX viewMatrix = XMMatrixIdentity();

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);

    // emerald
    materialAmbient_ak[0] = 0.0215f;
    materialAmbient_ak[1] = 0.1745f;
    materialAmbient_ak[2] = 0.0215f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.07568f;
    materialDiffused_ak[1] = 0.61424f;
    materialDiffused_ak[2] = 0.07568f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.633f;
    materialSpecular_ak[1] = 0.727811f;
    materialSpecular_ak[2] = 0.633f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.6f * 128.0f;

    D3D11_VIEWPORT d3dViewPort;
    d3dViewPort.TopLeftX = gWidth_ak * (-0.40f);
    d3dViewPort.TopLeftY = gHeight_ak * (-0.35f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    CBUFFER constantBuffer;
    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;
        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);
    materialAmbient_ak[0] = 0.135f;
    materialAmbient_ak[1] = 0.2225f;
    materialAmbient_ak[2] = 0.1575f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.54f;
    materialDiffused_ak[1] = 0.89f;
    materialDiffused_ak[2] = 0.63f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.316228f;
    materialSpecular_ak[1] = 0.316228f;
    materialSpecular_ak[2] = 0.316228f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.1 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (-0.40f);
    d3dViewPort.TopLeftY = gHeight_ak * (-0.20f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;

        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);

    materialAmbient_ak[0] = 0.05375f;
    materialAmbient_ak[1] = 0.05f;
    materialAmbient_ak[2] = 0.06625f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.18275f;
    materialDiffused_ak[1] = 0.17f;
    materialDiffused_ak[2] = 0.22525f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.332741f;
    materialSpecular_ak[1] = 0.328634f;
    materialSpecular_ak[2] = 0.346435f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.3 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (-0.40f);
    d3dViewPort.TopLeftY = gHeight_ak * (-0.05f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;

        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);
    materialAmbient_ak[0] = 0.25f;
    materialAmbient_ak[1] = 0.20725f;
    materialAmbient_ak[2] = 0.20725f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 1.0f;
    materialDiffused_ak[1] = 0.829f;
    materialDiffused_ak[2] = 0.829f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.296648f;
    materialSpecular_ak[1] = 0.296648f;
    materialSpecular_ak[2] = 0.296648f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.088 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (-0.40f);
    d3dViewPort.TopLeftY = gHeight_ak * (0.10f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;

        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);
    materialAmbient_ak[0] = 0.1745f;
    materialAmbient_ak[1] = 0.01175f;
    materialAmbient_ak[2] = 0.01175f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.61424f;
    materialDiffused_ak[1] = 0.04136f;
    materialDiffused_ak[2] = 0.04136f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.727811f;
    materialSpecular_ak[1] = 0.626959f;
    materialSpecular_ak[2] = 0.626959f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.6 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (-0.40f);
    d3dViewPort.TopLeftY = gHeight_ak * (0.25f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;

        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);
    materialAmbient_ak[0] = 0.1f;
    materialAmbient_ak[1] = 0.18725f;
    materialAmbient_ak[2] = 0.1745f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.396f;
    materialDiffused_ak[1] = 0.74151f;
    materialDiffused_ak[2] = 0.69102f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.297254f;
    materialSpecular_ak[1] = 0.30829f;
    materialSpecular_ak[2] = 0.306678f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.1 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (-0.40f);
    d3dViewPort.TopLeftY = gHeight_ak * (0.40f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;

        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);

    materialAmbient_ak[0] = 0.329412f;
    materialAmbient_ak[1] = 0.223529f;
    materialAmbient_ak[2] = 0.027451f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.780392f;
    materialDiffused_ak[1] = 0.568627f;
    materialDiffused_ak[2] = 0.113725f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.992157f;
    materialSpecular_ak[1] = 0.941176f;
    materialSpecular_ak[2] = 0.807843f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.21794872 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (-0.15f);
    d3dViewPort.TopLeftY = gHeight_ak * (-0.35f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;
        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);

    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);

    materialAmbient_ak[0] = 0.2125f;
    materialAmbient_ak[1] = 0.1275f;
    materialAmbient_ak[2] = 0.054f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.714f;
    materialDiffused_ak[1] = 0.4284f;
    materialDiffused_ak[2] = 0.18144f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.393548f;
    materialSpecular_ak[1] = 0.271906f;
    materialSpecular_ak[2] = 0.166721f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.2 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (-0.15f);
    d3dViewPort.TopLeftY = gHeight_ak * (-0.20f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;
        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);

    materialAmbient_ak[0] = 0.25f;
    materialAmbient_ak[1] = 0.25f;
    materialAmbient_ak[2] = 0.25f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.4f;
    materialDiffused_ak[1] = 0.4f;
    materialDiffused_ak[2] = 0.4f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.774597f;
    materialSpecular_ak[1] = 0.774597f;
    materialSpecular_ak[2] = 0.774597f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.6 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (-0.15f);
    d3dViewPort.TopLeftY = gHeight_ak * (0.10f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;
        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);
    materialAmbient_ak[0] = 0.19125f;
    materialAmbient_ak[1] = 0.0735f;
    materialAmbient_ak[2] = 0.0225f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.7038f;
    materialDiffused_ak[1] = 0.27048f;
    materialDiffused_ak[2] = 0.0828f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.256777f;
    materialSpecular_ak[1] = 0.137622f;
    materialSpecular_ak[2] = 0.086014f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.1 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (-0.15f);
    d3dViewPort.TopLeftY = gHeight_ak * (-0.05f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;

        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);
    materialAmbient_ak[0] = 0.24725f;
    materialAmbient_ak[1] = 0.1995f;
    materialAmbient_ak[2] = 0.0745f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.75164f;
    materialDiffused_ak[1] = 0.60648f;
    materialDiffused_ak[2] = 0.22648f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.628281f;
    materialSpecular_ak[1] = 0.555802f;
    materialSpecular_ak[2] = 0.366065f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.4 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (-0.15f);
    d3dViewPort.TopLeftY = gHeight_ak * (0.25f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;
        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);
    materialAmbient_ak[0] = 0.19225f;
    materialAmbient_ak[1] = 0.19225f;
    materialAmbient_ak[2] = 0.19225f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.50754f;
    materialDiffused_ak[1] = 0.50754f;
    materialDiffused_ak[2] = 0.50754f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.508273f;
    materialSpecular_ak[1] = 0.508273f;
    materialSpecular_ak[2] = 0.508273f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.4 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (-0.15f);
    d3dViewPort.TopLeftY = gHeight_ak * (0.40f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;

        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);

    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.0f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.01f;
    materialDiffused_ak[1] = 0.01f;
    materialDiffused_ak[2] = 0.01f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.50f;
    materialSpecular_ak[1] = 0.50f;
    materialSpecular_ak[2] = 0.50f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.25 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (0.15f);
    d3dViewPort.TopLeftY = gHeight_ak * (-0.35f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;

        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);
    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.1f;
    materialAmbient_ak[2] = 0.06f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.0f;
    materialDiffused_ak[1] = 0.50980392f;
    materialDiffused_ak[2] = 0.50980392f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.50196078f;
    materialSpecular_ak[1] = 0.50196078f;
    materialSpecular_ak[2] = 0.50196078f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.25 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (0.15f);
    d3dViewPort.TopLeftY = gHeight_ak * (-0.20f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;

        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);

    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.0f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.1f;
    materialDiffused_ak[1] = 0.35f;
    materialDiffused_ak[2] = 0.1f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.45f;
    materialSpecular_ak[1] = 0.55f;
    materialSpecular_ak[2] = 0.45f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.25 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (0.15f);
    d3dViewPort.TopLeftY = gHeight_ak * (-0.05f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;

        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);

    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.0f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.5f;
    materialDiffused_ak[1] = 0.0f;
    materialDiffused_ak[2] = 0.0f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.7f;
    materialSpecular_ak[1] = 0.6f;
    materialSpecular_ak[2] = 0.6f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.25 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (0.15f);
    d3dViewPort.TopLeftY = gHeight_ak * (0.10f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;
        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);

    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.0f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.55f;
    materialDiffused_ak[1] = 0.55f;
    materialDiffused_ak[2] = 0.55f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.70f;
    materialSpecular_ak[1] = 0.70f;
    materialSpecular_ak[2] = 0.70f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.25 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (0.15f);
    d3dViewPort.TopLeftY = gHeight_ak * (0.25f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;
        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);
    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.0f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.5f;
    materialDiffused_ak[1] = 0.5f;
    materialDiffused_ak[2] = 0.0f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.60f;
    materialSpecular_ak[1] = 0.60f;
    materialSpecular_ak[2] = 0.50f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.25 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (0.15f);
    d3dViewPort.TopLeftY = gHeight_ak * (0.40f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;
        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);

    materialAmbient_ak[0] = 0.02f;
    materialAmbient_ak[1] = 0.02f;
    materialAmbient_ak[2] = 0.02f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.01f;
    materialDiffused_ak[1] = 0.01f;
    materialDiffused_ak[2] = 0.01f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.4f;
    materialSpecular_ak[1] = 0.4f;
    materialSpecular_ak[2] = 0.4f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.078125 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (0.40f);
    d3dViewPort.TopLeftY = gHeight_ak * (-0.35f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;
        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);
    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.05f;
    materialAmbient_ak[2] = 0.05f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.4f;
    materialDiffused_ak[1] = 0.5f;
    materialDiffused_ak[2] = 0.5f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.04f;
    materialSpecular_ak[1] = 0.7f;
    materialSpecular_ak[2] = 0.7f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.078125 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (0.40f);
    d3dViewPort.TopLeftY = gHeight_ak * (-0.20f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;
        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);

    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.05f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.4f;
    materialDiffused_ak[1] = 0.5f;
    materialDiffused_ak[2] = 0.4f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.04f;
    materialSpecular_ak[1] = 0.7f;
    materialSpecular_ak[2] = 0.04f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.078125 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (0.40f);
    d3dViewPort.TopLeftY = gHeight_ak * (-0.05f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;
        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);
    materialAmbient_ak[0] = 0.05f;
    materialAmbient_ak[1] = 0.0f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.5f;
    materialDiffused_ak[1] = 0.4f;
    materialDiffused_ak[2] = 0.4f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.7f;
    materialSpecular_ak[1] = 0.04f;
    materialSpecular_ak[2] = 0.04f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.078125 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (0.40f);
    d3dViewPort.TopLeftY = gHeight_ak * (0.10f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;

        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);
    materialAmbient_ak[0] = 0.05f;
    materialAmbient_ak[1] = 0.05f;
    materialAmbient_ak[2] = 0.05f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.5f;
    materialDiffused_ak[1] = 0.5f;
    materialDiffused_ak[2] = 0.5f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.7f;
    materialSpecular_ak[1] = 0.7f;
    materialSpecular_ak[2] = 0.7f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.078125 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (0.40f);
    d3dViewPort.TopLeftY = gHeight_ak * (0.25f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;

        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    worldMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixIdentity();
    viewMatrix = XMMatrixIdentity();
    translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 12.0f);

    materialAmbient_ak[0] = 0.05f;
    materialAmbient_ak[1] = 0.05f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.5f;
    materialDiffused_ak[1] = 0.5f;
    materialDiffused_ak[2] = 0.4f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.7f;
    materialSpecular_ak[1] = 0.7f;
    materialSpecular_ak[2] = 0.04f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.078125 * 128.0f;

    d3dViewPort.TopLeftX = gWidth_ak * (0.40f);
    d3dViewPort.TopLeftY = gHeight_ak * (0.40f);
    d3dViewPort.Width = (float)gWidth_ak;
    d3dViewPort.Height = (float)gHeight_ak;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    constantBuffer.WorldMatrix = translationMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_ak;

    if (bLight_ak) {
        constantBuffer.KeyPressed = 1;
        constantBuffer.La = XMVectorSet(lightAmbient_ak[0], lightAmbient_ak[1], lightAmbient_ak[2], lightAmbient_ak[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_ak[0], lightDiffused_ak[1], lightDiffused_ak[2], lightDiffused_ak[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_ak[0], lightSpecular_ak[1], lightSpecular_ak[2], lightSpecular_ak[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_ak[0], lightPosition_ak[1], lightPosition_ak[2], lightPosition_ak[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_ak[0], materialAmbient_ak[1], materialAmbient_ak[2], materialAmbient_ak[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_ak[0], materialDiffused_ak[1], materialDiffused_ak[2], materialDiffused_ak[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_ak[0], materialSpecular_ak[1], materialSpecular_ak[2], materialSpecular_ak[3]);
        constantBuffer.MaterialShininess = materialShininess_ak;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_ak, 0, NULL, &constantBuffer, 0, 0);
    gpID3D11DeviceContext_ak->DrawIndexed(gNumElements_ak, 0, 0);

    gpIDXGISwapChain_ak->Present(0, 0);
}

void Update() {
    angleForXRotation_ak = angleForXRotation_ak + 0.001f;
    if (angleForXRotation_ak > 2 * XM_PI)
        angleForXRotation_ak = 0.0f;

    if (keyPressed_ak == 1) {
        lightPosition_ak[1] = 100 * sin(angleForXRotation_ak);
        lightPosition_ak[2] = 100 * cos(angleForXRotation_ak);
    }

    angleForYRotation_ak = angleForYRotation_ak + 0.001f;
    if (angleForYRotation_ak > 2 * XM_PI)
        angleForYRotation_ak = 0.0f;
    if (keyPressed_ak == 2) {
        lightPosition_ak[0] = 100 * sin(angleForYRotation_ak);
        lightPosition_ak[2] = 100 * cos(angleForYRotation_ak);
    }

    angleForZRotation_ak = angleForZRotation_ak + 0.001f;
    if (angleForZRotation_ak > 2 * XM_PI)
        angleForZRotation_ak = 0.0f;
    if (keyPressed_ak == 3) {
        lightPosition_ak[0] = 100 * sin(angleForZRotation_ak);
        lightPosition_ak[1] = 100 * cos(angleForZRotation_ak);
    }
}

void Uninitialize(void) {
    if (gbFullScreen_ak == true) {
        dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);

        SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak | WS_OVERLAPPEDWINDOW));
        SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
        SetWindowPos(ghwnd_ak,
                     HWND_TOP,
                     0,
                     0,
                     0,
                     0,
                     SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
        ShowCursor(TRUE);
    }

    if (gpID3D11Buffer_ConstantBuffer_ak) {
        gpID3D11Buffer_ConstantBuffer_ak->Release();
        gpID3D11Buffer_ConstantBuffer_ak = NULL;
    }
    if (gpID3D11InputLayout_ak) {
        gpID3D11InputLayout_ak->Release();
        gpID3D11InputLayout_ak = NULL;
    }

    if (gpID3D11Buffer_IndexBuffer_ak) {
        gpID3D11Buffer_IndexBuffer_ak->Release();
        gpID3D11Buffer_IndexBuffer_ak = NULL;
    }

    if (gpID3D11Buffer_VertexBuffer_Normal_Sphere_ak) {
        gpID3D11Buffer_VertexBuffer_Normal_Sphere_ak->Release();
        gpID3D11Buffer_VertexBuffer_Normal_Sphere_ak = NULL;
    }

    if (gpID3D11Buffer_VertexBuffer_Position_Sphere_ak) {
        gpID3D11Buffer_VertexBuffer_Position_Sphere_ak->Release();
        gpID3D11Buffer_VertexBuffer_Position_Sphere_ak = NULL;
    }

    if (gpID3D11PixelShader_ak) {
        gpID3D11PixelShader_ak->Release();
        gpID3D11PixelShader_ak = NULL;
    }
    if (gpID3D11VertexShader_ak) {
        gpID3D11VertexShader_ak->Release();
        gpID3D11VertexShader_ak = NULL;
    }
    if (gpID3D11RenderTargetView_ak) {
        gpID3D11RenderTargetView_ak->Release();
        gpID3D11RenderTargetView_ak = NULL;
    }
    if (gpIDXGISwapChain_ak) {
        gpIDXGISwapChain_ak->Release();
        gpIDXGISwapChain_ak = NULL;
    }
    if (gpID3D11DeviceContext_ak) {
        gpID3D11DeviceContext_ak->Release();
        gpID3D11DeviceContext_ak = NULL;
    }
    if (gpID3D11Device_ak) {
        gpID3D11Device_ak->Release();
        gpID3D11Device_ak = NULL;
    }

    if (gpFile_ak) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fclose(gpFile_ak);
        gpFile_ak = NULL;
    }
}