#include "Icon.h"
#include <d3d11.h>
#include <d3dcompiler.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#include "Sphere.h"

#pragma warning(disable : 4838)  // to suppress unsigned int to int conversion warning
#include "XNAMath_204\xnamath.h"

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gpFile_PSM = NULL;
HWND ghwnd_PSM = NULL;
DWORD dwStyle_PSM = NULL;
WINDOWPLACEMENT wpPrev_PSM = {sizeof(WINDOWPLACEMENT)};
bool gbFullScreen_PSM = false;

bool gbActiveWindow_PSM = false;
char gszLogFileName_PSM[] = "logs_PSM.txt";

IDXGISwapChain *gpIDXGISwapChain_PSM = NULL;
ID3D11Device *gpID3D11Device_PSM = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext_PSM = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView_PSM = NULL;

ID3D11VertexShader *gpID3D11VertexShader_PSM = NULL;
ID3D11PixelShader *gpID3D11PixelShader_PSM = NULL;

ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Position_Sphere_PSM = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Normal_Sphere_PSM = NULL;
ID3D11Buffer *gpID3D11Buffer_IndexBuffer_PSM = NULL;

ID3D11InputLayout *gpID3D11InputLayout_PSM = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer_PSM = NULL;

ID3D11RasterizerState *gpID3D11RasterizerState_PSM = NULL;

ID3D11DepthStencilView *gpID3D11DepthStencilView_PSM = NULL;

struct CBUFFER {
    XMMATRIX WorldMatrix;
    XMMATRIX ViewMatrix;
    XMMATRIX ProjectionMatrix;

    XMVECTOR La;
    XMVECTOR Ld;
    XMVECTOR Ls;
    XMVECTOR LightPosition;

    XMVECTOR Ka;
    XMVECTOR Kd;
    XMVECTOR Ks;
    float MaterialShininess;

    UINT KeyPressed;
};
bool bLight_PSM = false;

float lightAmbient_PSM[] = {0.0f, 0.0f, 0.0f, 1.0f};
float lightDiffused_PSM[] = {1.0f, 1.0f, 1.0f, 1.0f};
float lightPosition_PSM[] = {100.0f, 100.0f, -100.0f, 1.0f};
float lightSpecular_PSM[] = {1.0f, 1.0f, 1.0f, 1.0f};

float materialAmbient_PSM[] = {0.0f, 0.0f, 0.0f, 1.0f};
float materialDiffused_PSM[] = {1.0f, 1.0f, 1.0f, 1.0f};
float materialSpecular_PSM[] = {1.0f, 1.0f, 1.0f, 1.0f};
float materialShininess_PSM = 50.0f;

XMMATRIX gPerspectiveProjectionMatrix_PSM;

float gClearColor_PSM[4];

float sphere_vertices_PSM[1146];
float sphere_normals_PSM[1146];
float sphere_textures_PSM[764];
unsigned short sphere_elements_PSM[2280];

int gNumVertices_PSM;
int gNumElements_PSM;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
    HRESULT Initialize(void);
    void Display(void);
    void Uninitialize(void);
    void Update(void);
    WNDCLASSEX wndclass_PSM;
    HWND hwnd_PSM;
    MSG msg_PSM;
    TCHAR szAppName_PSM[] = TEXT("MyApp");
    bool bDone_PSM = false;

    if (fopen_s(&gpFile_PSM, gszLogFileName_PSM, "w") != 0) {
        MessageBox(NULL, TEXT("Failure creating file"), TEXT("File IO failed"), MB_ICONSTOP | MB_OK);
        exit(0);
    } else {
        fprintf(gpFile_PSM, "%s\t%s\t%s\t%d File successfully create and program started\n", __DATE__, __TIME__, __FILE__, __LINE__);
        fclose(gpFile_PSM);
    }

    int iScreenWidth = GetSystemMetrics(SM_CXSCREEN);
    int iScreenHeight = GetSystemMetrics(SM_CYSCREEN);

    int iScreenCenterX = iScreenWidth / 2;
    int iScreenCenterY = iScreenHeight / 2;

    int iWindowCenterX = WIN_WIDTH / 2;
    int iWindowsCenterY = WIN_HEIGHT / 2;

    int iWindowX = iScreenCenterX - iWindowCenterX;
    int iWindowY = iScreenCenterY - iWindowsCenterY;

    wndclass_PSM.cbSize = sizeof(WNDCLASSEX);
    wndclass_PSM.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wndclass_PSM.cbClsExtra = 0;
    wndclass_PSM.cbWndExtra = 0;
    wndclass_PSM.hInstance = hInstance;
    wndclass_PSM.lpfnWndProc = WndProc;
    wndclass_PSM.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON_PSM));
    wndclass_PSM.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass_PSM.lpszClassName = szAppName_PSM;
    wndclass_PSM.lpszMenuName = NULL;
    wndclass_PSM.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass_PSM.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON_PSM));

    RegisterClassEx(&wndclass_PSM);

    hwnd_PSM = CreateWindowEx(
        WS_EX_APPWINDOW, szAppName_PSM, TEXT("D3D11 per pixel light on sphere"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, iWindowX, iWindowY, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);
    ghwnd_PSM = hwnd_PSM;

    HRESULT hr_PSM = Initialize();
    if (FAILED(hr_PSM)) {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "Initialize() failed.\n");
        fclose(gpFile_PSM);
        hwnd_PSM = NULL;
    } else {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "Initialize() succeeded.\n");
        fclose(gpFile_PSM);
    }
    ShowWindow(hwnd_PSM, iCmdShow);
    SetForegroundWindow(hwnd_PSM);
    SetFocus(hwnd_PSM);

    while (bDone_PSM == false) {
        if (PeekMessage(&msg_PSM, NULL, 0, 0, PM_REMOVE)) {
            if (msg_PSM.message == WM_QUIT) {
                bDone_PSM = true;
            } else {
                TranslateMessage(&msg_PSM);
                DispatchMessage(&msg_PSM);
            }
        } else {
            if (gbActiveWindow_PSM == true) {
                Display();
                Update();
            }
        }
    }
    Uninitialize();

    return ((int)msg_PSM.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
    void ToggleFullScreen(void);
    HRESULT Resize(int, int);
    void Uninitialize(void);
    HRESULT hr_PSM;
    switch (iMsg) {
        case WM_SETFOCUS: {
            gbActiveWindow_PSM = true;
            break;
        }
        case WM_KILLFOCUS: {
            gbActiveWindow_PSM = false;
            break;
        }
        case WM_ERASEBKGND: {
            return (0);
        }
        case WM_SIZE: {
            if (gpID3D11DeviceContext_PSM) {
                hr_PSM = Resize(LOWORD(lParam), HIWORD(lParam));
                if (FAILED(hr_PSM)) {
                    fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
                    fprintf_s(gpFile_PSM, "Resize() Failed.\n");
                    fclose(gpFile_PSM);
                    return (hr_PSM);
                } else {
                    fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
                    fprintf_s(gpFile_PSM, "Resize() Succeeded.\n");
                    fclose(gpFile_PSM);
                }
            }
            break;
        }
        case WM_KEYDOWN: {
            switch (wParam) {
                case VK_ESCAPE: {
                    DestroyWindow(hwnd);
                    break;
                }
                case 0x46:
                case 0x66: {
                    ToggleFullScreen();
                    break;
                }
                default: {
                    break;
                }
            }
            break;
        }
        case WM_CHAR: {
            switch (wParam) {
                case 'L':
                case 'l': {
                    if (bLight_PSM) {
                        bLight_PSM = false;
                    } else {
                        bLight_PSM = true;
                    }
                    break;
                }
            }
            break;
        }
        case WM_CLOSE: {
            DestroyWindow(hwnd);
            break;
        }
        case WM_DESTROY: {
            Uninitialize();
            PostQuitMessage(0);
            break;
        }
        default: {
            break;
        }
    }

    return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void) {
    MONITORINFO mi_PSM = {sizeof(MONITORINFO)};

    if (gbFullScreen_PSM == false) {
        dwStyle_PSM = GetWindowLong(ghwnd_PSM, GWL_STYLE);
        if ((dwStyle_PSM & WS_OVERLAPPEDWINDOW)) {
            if ((GetWindowPlacement(ghwnd_PSM, &wpPrev_PSM) &&
                 (GetMonitorInfo(MonitorFromWindow(ghwnd_PSM, MONITORINFOF_PRIMARY), &mi_PSM)))) {
                SetWindowLong(ghwnd_PSM, GWL_STYLE, (dwStyle_PSM & (~WS_OVERLAPPEDWINDOW)));
                SetWindowPos(ghwnd_PSM,
                             HWND_TOP,
                             mi_PSM.rcMonitor.left,
                             mi_PSM.rcMonitor.top,
                             (mi_PSM.rcMonitor.right - mi_PSM.rcMonitor.left),
                             (mi_PSM.rcMonitor.bottom - mi_PSM.rcMonitor.top),
                             SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }
        ShowCursor(FALSE);
        gbFullScreen_PSM = true;
    } else {
        SetWindowLong(ghwnd_PSM, GWL_STYLE, (dwStyle_PSM | WS_OVERLAPPEDWINDOW));
        SetWindowPlacement(ghwnd_PSM, &wpPrev_PSM);
        SetWindowPos(ghwnd_PSM,
                     HWND_TOP,
                     0,
                     0,
                     0,
                     0,
                     (SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOOWNERZORDER));
        ShowCursor(TRUE);
        gbFullScreen_PSM = false;
    }
}

HRESULT Initialize(void) {
    void Uninitialize(void);
    HRESULT Resize(int, int);

    HRESULT hr_PSM;

    D3D_DRIVER_TYPE d3dDriverType_PSM;
    D3D_DRIVER_TYPE d3dDriverTypes[] =
        {
            D3D_DRIVER_TYPE_HARDWARE,
            D3D_DRIVER_TYPE_WARP,
            D3D_DRIVER_TYPE_REFERENCE};

    D3D_FEATURE_LEVEL d3dFeatureLevel_required_PSM = D3D_FEATURE_LEVEL_11_0;
    D3D_FEATURE_LEVEL d3dFeatureLevel_acquired_PSM = D3D_FEATURE_LEVEL_10_0;

    UINT createDeviceFlags_PSM = 0;
    UINT numDriverTypes_PSM = 0;
    UINT numFeatureLevels_PSM = 1;

    numDriverTypes_PSM = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);

    DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
    ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
    dxgiSwapChainDesc.BufferCount = 1;
    dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
    dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
    dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
    dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
    dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    dxgiSwapChainDesc.OutputWindow = ghwnd_PSM;
    dxgiSwapChainDesc.SampleDesc.Count = 1;
    dxgiSwapChainDesc.SampleDesc.Quality = 0;
    dxgiSwapChainDesc.Windowed = TRUE;

    for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes_PSM; driverTypeIndex++) {
        d3dDriverType_PSM = d3dDriverTypes[driverTypeIndex];
        hr_PSM = D3D11CreateDeviceAndSwapChain(
            NULL,
            d3dDriverType_PSM,
            NULL,
            createDeviceFlags_PSM,
            &d3dFeatureLevel_required_PSM,
            numFeatureLevels_PSM,
            D3D11_SDK_VERSION,
            &dxgiSwapChainDesc,
            &gpIDXGISwapChain_PSM,
            &gpID3D11Device_PSM,
            &d3dFeatureLevel_acquired_PSM,
            &gpID3D11DeviceContext_PSM);
        if (SUCCEEDED(hr_PSM))
            break;
    }
    if (FAILED(hr_PSM)) {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "D3D11CreateDeviceAndSwapChain() Failed.\n");
        fclose(gpFile_PSM);
        return (hr_PSM);
    } else {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "D3D11CreateDeviceAndSwapChain() Succeeded.\n");

        fprintf_s(gpFile_PSM, "The Chosen Driver is of ");
        if (d3dDriverType_PSM == D3D_DRIVER_TYPE_HARDWARE) {
            fprintf_s(gpFile_PSM, "Hardware Type.\n");
        } else if (d3dDriverType_PSM == D3D_DRIVER_TYPE_WARP) {
            fprintf_s(gpFile_PSM, "WRAP Type.\n");
        } else if (d3dDriverType_PSM == D3D_DRIVER_TYPE_REFERENCE) {
            fprintf_s(gpFile_PSM, "Reference Type.\n");
        } else {
            fprintf_s(gpFile_PSM, "Unknown Type.\n");
        }

        fprintf_s(gpFile_PSM, "The Supported Highest Feature Level Is");

        if (d3dFeatureLevel_acquired_PSM == D3D_FEATURE_LEVEL_11_0) {
            fprintf_s(gpFile_PSM, "11.0\n");
        }
        if (d3dFeatureLevel_acquired_PSM == D3D_FEATURE_LEVEL_10_1) {
            fprintf_s(gpFile_PSM, "10.1\n");
        }
        if (d3dFeatureLevel_acquired_PSM == D3D_FEATURE_LEVEL_10_0) {
            fprintf_s(gpFile_PSM, "10.0\n");
        } else {
            fprintf_s(gpFile_PSM, "Unknown\n");
        }

        fclose(gpFile_PSM);
    }

    const char *vertexShaderSourceCode_PSM =
        "cbuffer ConstantBuffer"
        "{"
        "float4x4 worldMatrix;"
        "float4x4 viewMatrix;"
        "float4x4 projectionMatrix;"
        "float3 la;"
        "float3 ld;"
        "float4 ls;"
        "float4 lightPosition;"
        "float3 ka;"
        "float3 kd;"
        "float4 ks;"
        "float materialShininess;"
        "uint keyPressed;"
        "}"
        "struct vertex_output"
        "{"
        "float4 position: SV_POSITION;"
        "float3 transformed_normals : NORMAL0;"
        "float3 light_direction : NORMAL1;"
        "float3 viewer_vector : NORMAL2;"
        "};"
        "vertex_output main(float4 pos: POSITION, float4 normals: NORMAL)"
        "{"
        "vertex_output output;"
        "if(keyPressed == 1)"
        "{"
        "float4 eyePosition = mul(worldMatrix, pos);"
        "eyePosition = mul(viewMatrix, eyePosition);"
        "output.transformed_normals = mul((float3x3)worldMatrix, (float3)normals);"
        "output.light_direction = (float3)(lightPosition) - eyePosition.xyz;"
        "output.viewer_vector = -eyePosition.xyz;"
        "}"
        "float4 position = mul(worldMatrix, pos);"
        "position = mul(viewMatrix, position);"
        "position = mul(projectionMatrix, position);"
        "output.position = position;"
        "return(output);"
        "}";
    ID3DBlob *pID3DBlob_VertexShaderCode_PSM = NULL;
    ID3DBlob *pID3DBlob_Error_PSM = NULL;

    hr_PSM = D3DCompile(vertexShaderSourceCode_PSM,
                        lstrlenA(vertexShaderSourceCode_PSM) + 1,
                        "VS",
                        NULL,
                        D3D_COMPILE_STANDARD_FILE_INCLUDE,
                        "main",
                        "vs_5_0",
                        0,
                        0,
                        &pID3DBlob_VertexShaderCode_PSM,
                        &pID3DBlob_Error_PSM);

    if (FAILED(hr_PSM)) {
        if (pID3DBlob_Error_PSM != NULL) {
            fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
            fprintf_s(gpFile_PSM, "D3DCompile() Failed For Vertex Shader : %s.\n", (char *)pID3DBlob_Error_PSM->GetBufferPointer());
            fclose(gpFile_PSM);
            pID3DBlob_Error_PSM->Release();
            pID3DBlob_Error_PSM = NULL;
            return (hr_PSM);
        } else {
            fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
            fprintf_s(gpFile_PSM, "D3DCompile() Failed For Vertex Shader due to some COM error.\n");
            fclose(gpFile_PSM);
        }
    } else {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "D3DCompile() Succeeded For Vertex Shader.\n");
        fclose(gpFile_PSM);
    }

    hr_PSM = gpID3D11Device_PSM->CreateVertexShader(pID3DBlob_VertexShaderCode_PSM->GetBufferPointer(),
                                                    pID3DBlob_VertexShaderCode_PSM->GetBufferSize(),
                                                    NULL,
                                                    &gpID3D11VertexShader_PSM);

    if (FAILED(hr_PSM)) {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "ID3D11Device::CreateVertexShader() Failed .\n");
        fclose(gpFile_PSM);
        return (hr_PSM);
    } else {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "ID3D11Device::CreateVertexShader() Succeeded.\n");
        fclose(gpFile_PSM);
    }

    gpID3D11DeviceContext_PSM->VSSetShader(gpID3D11VertexShader_PSM, NULL, 0);

    const char *pixelShaderSourceCode_PSM =
        "cbuffer ConstantBuffer"
        "{"
        "float4x4 worldMatrix;"
        "float4x4 viewMatrix;"
        "float4x4 projectionMatrix;"
        "float3 la;"
        "float3 ld;"
        "float4 ls;"
        "float4 lightPosition;"
        "float3 ka;"
        "float3 kd;"
        "float4 ks;"
        "float materialShininess;"
        "uint keyPressed;"
        "}"
        "struct vertex_output"
        "{"
        "float4 position: SV_POSITION;"
        "float3 transformed_normals : NORMAL0;"
        "float3 light_direction : NORMAL1;"
        "float3 viewer_vector : NORMAL2;"
        "};"
        "float4 main(vertex_output input): SV_TARGET"
        "{"
        "float3 phong_ads_light;"
        "if(keyPressed == 1)"
        "{"
        "float3 normalized_transformed_normals = normalize(input.transformed_normals);"
        "float3 normalized_light_direction = normalize(input.light_direction);"
        "float3 normalized_viewer_vector = normalize(input.viewer_vector);"
        "float3 ambient = (float3)(la * ka);"
        "float3 diffuse = (float3)(ld * kd) * max(dot(normalized_light_direction, normalized_transformed_normals), 0.0);"
        "float3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);"
        "float3 specular = (float3)(ls * ks) * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), materialShininess);"
        "phong_ads_light = ambient + diffuse + specular;"
        "}"
        "else"
        "{"
        "phong_ads_light = float3(1.0f, 1.0f, 1.0f);"
        "}"
        "float4 color = float4(phong_ads_light, 1.0f);"
        "return(color);"
        "}";

    ID3DBlob *pID3DBlob_PixelShaderCode_PSM = NULL;
    pID3DBlob_Error_PSM = NULL;

    hr_PSM = D3DCompile(pixelShaderSourceCode_PSM,
                        lstrlenA(pixelShaderSourceCode_PSM) + 1,
                        "PS",
                        NULL,
                        D3D_COMPILE_STANDARD_FILE_INCLUDE,
                        "main",
                        "ps_5_0",
                        0,
                        0,
                        &pID3DBlob_PixelShaderCode_PSM,
                        &pID3DBlob_Error_PSM);

    if (FAILED(hr_PSM)) {
        if (pID3DBlob_Error_PSM != NULL) {
            fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
            fprintf_s(gpFile_PSM, "D3DCompile() Failed For Pixel Shader : %s.\n", (char *)pID3DBlob_Error_PSM->GetBufferPointer());
            fclose(gpFile_PSM);
            pID3DBlob_Error_PSM->Release();
            pID3DBlob_Error_PSM = NULL;
            return (hr_PSM);
        } else {
            fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
            fprintf_s(gpFile_PSM, "D3DCompile() Failed For Pixel Shader due to some COM error.\n");
            fclose(gpFile_PSM);
        }
    } else {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "D3DCompile() Succeeded For Pixel Shader\n");
        fclose(gpFile_PSM);
    }

    hr_PSM = gpID3D11Device_PSM->CreatePixelShader(pID3DBlob_PixelShaderCode_PSM->GetBufferPointer(),
                                                   pID3DBlob_PixelShaderCode_PSM->GetBufferSize(),
                                                   NULL,
                                                   &gpID3D11PixelShader_PSM);

    if (FAILED(hr_PSM)) {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "ID3D11Device::CreatePixelShader() Failed \n");
        fclose(gpFile_PSM);
        return (hr_PSM);
    } else {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "ID3D11Device::CreatePixelShader() Succeeded.\n");
        fclose(gpFile_PSM);
    }

    gpID3D11DeviceContext_PSM->PSSetShader(gpID3D11PixelShader_PSM, NULL, 0);
    pID3DBlob_PixelShaderCode_PSM->Release();
    pID3DBlob_PixelShaderCode_PSM = NULL;

    D3D11_INPUT_ELEMENT_DESC inputElementDesc_PSM[2];

    inputElementDesc_PSM[0].SemanticName = "POSITION";
    inputElementDesc_PSM[0].SemanticIndex = 0;
    inputElementDesc_PSM[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
    inputElementDesc_PSM[0].InputSlot = 0;
    inputElementDesc_PSM[0].AlignedByteOffset = 0;
    inputElementDesc_PSM[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
    inputElementDesc_PSM[0].InstanceDataStepRate = 0;

    inputElementDesc_PSM[1].SemanticName = "NORMAL";
    inputElementDesc_PSM[1].SemanticIndex = 0;
    inputElementDesc_PSM[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
    inputElementDesc_PSM[1].InputSlot = 1;
    inputElementDesc_PSM[1].AlignedByteOffset = 0;
    inputElementDesc_PSM[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
    inputElementDesc_PSM[1].InstanceDataStepRate = 0;

    hr_PSM = gpID3D11Device_PSM->CreateInputLayout(inputElementDesc_PSM, _ARRAYSIZE(inputElementDesc_PSM),
                                                   pID3DBlob_VertexShaderCode_PSM->GetBufferPointer(),
                                                   pID3DBlob_VertexShaderCode_PSM->GetBufferSize(),
                                                   &gpID3D11InputLayout_PSM);

    if (FAILED(hr_PSM)) {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "D3D11Device::CreateInput() Failed.\n");
        fclose(gpFile_PSM);
        pID3DBlob_VertexShaderCode_PSM->Release();
        pID3DBlob_VertexShaderCode_PSM = NULL;
        return (hr_PSM);
    } else {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "ID3D11Device::CreateInput() Succeeded.\n");
        fclose(gpFile_PSM);
        pID3DBlob_VertexShaderCode_PSM->Release();
        pID3DBlob_VertexShaderCode_PSM = NULL;
    }

    getSphereVertexData(sphere_vertices_PSM, sphere_normals_PSM, sphere_textures_PSM, sphere_elements_PSM);
    gNumVertices_PSM = getNumberOfSphereVertices();
    gNumElements_PSM = getNumberOfSphereElements();

    D3D11_BUFFER_DESC bufferDesc_PSM;

    ZeroMemory(&bufferDesc_PSM, sizeof(D3D11_BUFFER_DESC));

    bufferDesc_PSM.Usage = D3D11_USAGE_DYNAMIC;
    bufferDesc_PSM.ByteWidth = sizeof(float) * gNumVertices_PSM * 3;
    bufferDesc_PSM.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    bufferDesc_PSM.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

    hr_PSM = gpID3D11Device_PSM->CreateBuffer(&bufferDesc_PSM, NULL,
                                              &gpID3D11Buffer_VertexBuffer_Position_Sphere_PSM);

    if (FAILED(hr_PSM)) {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "ID3D11Device::CreateBuffer() for sphere vertices Failed For Vertex Buffer.\n");
        fclose(gpFile_PSM);
        return (hr_PSM);
    } else {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "ID3D11Device::CreateBuffer() for sphere vertices Succeeded For Vertex Buffer.\n");
        fclose(gpFile_PSM);
    }
    gpID3D11DeviceContext_PSM->IASetInputLayout(gpID3D11InputLayout_PSM);
    D3D11_MAPPED_SUBRESOURCE mappedSubresource_PSM;
    ZeroMemory(&mappedSubresource_PSM, sizeof(D3D11_MAPPED_SUBRESOURCE));
    gpID3D11DeviceContext_PSM->Map(gpID3D11Buffer_VertexBuffer_Position_Sphere_PSM, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource_PSM);
    memcpy(mappedSubresource_PSM.pData, sphere_vertices_PSM, sizeof(float) * gNumVertices_PSM * 3);
    gpID3D11DeviceContext_PSM->Unmap(gpID3D11Buffer_VertexBuffer_Position_Sphere_PSM, NULL);

    ZeroMemory(&bufferDesc_PSM, sizeof(D3D11_BUFFER_DESC));

    bufferDesc_PSM.Usage = D3D11_USAGE_DYNAMIC;
    bufferDesc_PSM.ByteWidth = sizeof(float) * gNumVertices_PSM * 3;
    bufferDesc_PSM.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    bufferDesc_PSM.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

    hr_PSM = gpID3D11Device_PSM->CreateBuffer(&bufferDesc_PSM, NULL,
                                              &gpID3D11Buffer_VertexBuffer_Normal_Sphere_PSM);

    if (FAILED(hr_PSM)) {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "ID3D11Device::CreateBuffer() for sphere normals Failed For Vertex Buffer.\n");
        fclose(gpFile_PSM);
        return (hr_PSM);
    } else {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "ID3D11Device::CreateBuffer() for sphere normals Succeeded For Vertex Buffer.\n");
        fclose(gpFile_PSM);
    }

    ZeroMemory(&mappedSubresource_PSM, sizeof(D3D11_MAPPED_SUBRESOURCE));
    gpID3D11DeviceContext_PSM->Map(gpID3D11Buffer_VertexBuffer_Normal_Sphere_PSM, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource_PSM);
    memcpy(mappedSubresource_PSM.pData, sphere_normals_PSM, sizeof(float) * gNumVertices_PSM * 3);
    gpID3D11DeviceContext_PSM->Unmap(gpID3D11Buffer_VertexBuffer_Normal_Sphere_PSM, NULL);

    // index buffer

    ZeroMemory(&bufferDesc_PSM, sizeof(D3D11_BUFFER_DESC));

    bufferDesc_PSM.Usage = D3D11_USAGE_DYNAMIC;
    bufferDesc_PSM.ByteWidth = sizeof(short) * gNumElements_PSM;
    bufferDesc_PSM.BindFlags = D3D11_BIND_INDEX_BUFFER;
    bufferDesc_PSM.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

    hr_PSM = gpID3D11Device_PSM->CreateBuffer(&bufferDesc_PSM, NULL,
                                              &gpID3D11Buffer_IndexBuffer_PSM);

    if (FAILED(hr_PSM)) {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "ID3D11Device::CreateBuffer() for sphere indices Failed For Vertex Buffer.\n");
        fclose(gpFile_PSM);
        return (hr_PSM);
    } else {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "ID3D11Device::CreateBuffer() for sphere indices Succeeded For Vertex Buffer.\n");
        fclose(gpFile_PSM);
    }

    ZeroMemory(&mappedSubresource_PSM, sizeof(D3D11_MAPPED_SUBRESOURCE));
    gpID3D11DeviceContext_PSM->Map(gpID3D11Buffer_IndexBuffer_PSM, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource_PSM);
    memcpy(mappedSubresource_PSM.pData, sphere_elements_PSM, sizeof(short) * gNumElements_PSM);
    gpID3D11DeviceContext_PSM->Unmap(gpID3D11Buffer_IndexBuffer_PSM, NULL);

    D3D11_BUFFER_DESC bufferDesc_ConstantBuffer_PSM;
    ZeroMemory(&bufferDesc_ConstantBuffer_PSM, sizeof(D3D11_BUFFER_DESC));
    bufferDesc_ConstantBuffer_PSM.Usage = D3D11_USAGE_DEFAULT;
    bufferDesc_ConstantBuffer_PSM.ByteWidth = sizeof(CBUFFER);
    bufferDesc_ConstantBuffer_PSM.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    hr_PSM = gpID3D11Device_PSM->CreateBuffer(&bufferDesc_ConstantBuffer_PSM, nullptr, &gpID3D11Buffer_ConstantBuffer_PSM);

    if (FAILED(hr_PSM)) {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "ID3D11Device::CreateBuffer() Failed for Constant Buffer\n");
        fclose(gpFile_PSM);
        return (hr_PSM);
    } else {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "ID3D11Device::CreateBuffer() Succeeded for Constant Buffer.\n");
        fclose(gpFile_PSM);
    }

    gpID3D11DeviceContext_PSM->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer_PSM);
    gpID3D11DeviceContext_PSM->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer_PSM);

    // create and set Rasterizer State

    D3D11_RASTERIZER_DESC d3d11RasterizedDesc;
    ZeroMemory(&d3d11RasterizedDesc, sizeof(D3D11_RASTERIZER_DESC));

    d3d11RasterizedDesc.AntialiasedLineEnable = FALSE;
    d3d11RasterizedDesc.CullMode = D3D11_CULL_NONE;
    d3d11RasterizedDesc.DepthBias = 0;
    d3d11RasterizedDesc.DepthBiasClamp = 0.0f,
    d3d11RasterizedDesc.DepthClipEnable = TRUE;
    d3d11RasterizedDesc.FillMode = D3D11_FILL_SOLID;
    d3d11RasterizedDesc.FrontCounterClockwise = FALSE;
    d3d11RasterizedDesc.MultisampleEnable = FALSE;
    d3d11RasterizedDesc.ScissorEnable = FALSE;
    d3d11RasterizedDesc.SlopeScaledDepthBias = 0.0f;

    hr_PSM = gpID3D11Device_PSM->CreateRasterizerState(&d3d11RasterizedDesc,
                                                       &gpID3D11RasterizerState_PSM);

    if (FAILED(hr_PSM)) {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "ID3D11Device::CreateRasterizerState() failed for culling\n");
        fclose(gpFile_PSM);
        return (hr_PSM);
    } else {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "ID3D11Device::CreateRasterizerState() succeeded for culling\n");
        fclose(gpFile_PSM);
    }

    gpID3D11DeviceContext_PSM->RSSetState(gpID3D11RasterizerState_PSM);

    gClearColor_PSM[0] = 0.0f;
    gClearColor_PSM[1] = 0.0f;
    gClearColor_PSM[2] = 0.0f;
    gClearColor_PSM[3] = 1.0f;
    gPerspectiveProjectionMatrix_PSM = XMMatrixIdentity();

    hr_PSM = Resize(WIN_WIDTH, WIN_HEIGHT);
    if (FAILED(hr_PSM)) {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "Resize() Failed.\n");
        fclose(gpFile_PSM);
        return (hr_PSM);
    } else {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "Resize() Succeeded.\n");
        fclose(gpFile_PSM);
    }
    return (S_OK);
}

HRESULT Resize(int width, int height) {
    HRESULT hr_PSM = S_OK;

    if (gpID3D11RenderTargetView_PSM) {
        gpID3D11RenderTargetView_PSM->Release();
        gpID3D11RenderTargetView_PSM = NULL;
    }

    gpIDXGISwapChain_PSM->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

    ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
    gpIDXGISwapChain_PSM->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID *)&pID3D11Texture2D_BackBuffer);

    hr_PSM = gpID3D11Device_PSM->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView_PSM);
    if (FAILED(hr_PSM)) {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "CreateRenderTargetView() Failed.\n");
        fclose(gpFile_PSM);
        return (hr_PSM);
    } else {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "CreateRenderTargetView() Succeeded.\n");
        fclose(gpFile_PSM);
    }

    pID3D11Texture2D_BackBuffer->Release();
    pID3D11Texture2D_BackBuffer = NULL;

    // Depth

    if (gpID3D11DepthStencilView_PSM) {
        gpID3D11DepthStencilView_PSM->Release();
        gpID3D11DepthStencilView_PSM = NULL;
    }

    D3D11_TEXTURE2D_DESC d3d11TextureDesc_PSM;
    ZeroMemory(&d3d11TextureDesc_PSM, sizeof(D3D11_TEXTURE2D_DESC));
    // Necessary for depth
    d3d11TextureDesc_PSM.Width = (UINT)width;
    d3d11TextureDesc_PSM.Height = (UINT)height;
    d3d11TextureDesc_PSM.Format = DXGI_FORMAT_D32_FLOAT;
    d3d11TextureDesc_PSM.Usage = D3D11_USAGE_DEFAULT;
    d3d11TextureDesc_PSM.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    d3d11TextureDesc_PSM.SampleDesc.Count = 1;
    d3d11TextureDesc_PSM.SampleDesc.Quality = 0;
    // Extra
    d3d11TextureDesc_PSM.ArraySize = 1;
    d3d11TextureDesc_PSM.MipLevels = 1;
    d3d11TextureDesc_PSM.CPUAccessFlags = 0;
    d3d11TextureDesc_PSM.MiscFlags = 0;

    ID3D11Texture2D *pID3D11Texture2D_DepthBuffer_PSM = NULL;
    gpID3D11Device_PSM->CreateTexture2D(&d3d11TextureDesc_PSM, NULL,
                                        &pID3D11Texture2D_DepthBuffer_PSM);

    D3D11_DEPTH_STENCIL_VIEW_DESC d3d11DepthStencilViewDesc_PSM;
    ZeroMemory(&d3d11DepthStencilViewDesc_PSM, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

    d3d11DepthStencilViewDesc_PSM.Format = DXGI_FORMAT_D32_FLOAT;
    d3d11DepthStencilViewDesc_PSM.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

    hr_PSM = gpID3D11Device_PSM->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer_PSM, &d3d11DepthStencilViewDesc_PSM,
                                                        &gpID3D11DepthStencilView_PSM);

    if (FAILED(hr_PSM)) {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "CreateDepthStencilView() Failed.\n");
        fclose(gpFile_PSM);
        return hr_PSM;
    } else {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf_s(gpFile_PSM, "CreateDepthStencilView() Succeeded.\n");
        fclose(gpFile_PSM);
    }

    pID3D11Texture2D_DepthBuffer_PSM->Release();
    pID3D11Texture2D_DepthBuffer_PSM = NULL;

    if (height < 0) {
        height = 1;
    }

    gpID3D11DeviceContext_PSM->OMSetRenderTargets(1, &gpID3D11RenderTargetView_PSM, gpID3D11DepthStencilView_PSM);

    D3D11_VIEWPORT d3dViewPort;
    d3dViewPort.TopLeftX = 0;
    d3dViewPort.TopLeftY = 0;
    d3dViewPort.Width = (float)width;
    d3dViewPort.Height = (float)height;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_PSM->RSSetViewports(1, &d3dViewPort);

    gPerspectiveProjectionMatrix_PSM = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0), (float)width / (float)height, 0.1f, 100.0f);

    return hr_PSM;
}

void Display(void) {
    gpID3D11DeviceContext_PSM->ClearRenderTargetView(gpID3D11RenderTargetView_PSM, gClearColor_PSM);
    gpID3D11DeviceContext_PSM->ClearDepthStencilView(gpID3D11DepthStencilView_PSM, D3D11_CLEAR_DEPTH, 1.0f, 0);

    UINT stride_PSM = sizeof(float) * 3;
    UINT offset_PSM = 0;

    gpID3D11DeviceContext_PSM->IASetVertexBuffers(0, 1, &gpID3D11Buffer_VertexBuffer_Position_Sphere_PSM, &stride_PSM, &offset_PSM);

    stride_PSM = sizeof(float) * 3;
    offset_PSM = 0;

    gpID3D11DeviceContext_PSM->IASetVertexBuffers(1, 1, &gpID3D11Buffer_VertexBuffer_Normal_Sphere_PSM, &stride_PSM, &offset_PSM);
    gpID3D11DeviceContext_PSM->IASetIndexBuffer(gpID3D11Buffer_IndexBuffer_PSM, DXGI_FORMAT_R16_UINT, 0);

    gpID3D11DeviceContext_PSM->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

    XMMATRIX worldMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.5f);
    XMMATRIX viewMatrix = XMMatrixIdentity();
    XMMATRIX wvMatrix = worldMatrix * viewMatrix;

    CBUFFER constantBuffer;
    constantBuffer.WorldMatrix = worldMatrix;
    constantBuffer.ViewMatrix = viewMatrix;
    constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix_PSM;

    if (bLight_PSM) {
        constantBuffer.KeyPressed = 1;

        constantBuffer.La = XMVectorSet(lightAmbient_PSM[0], lightAmbient_PSM[1], lightAmbient_PSM[2], lightAmbient_PSM[3]);
        constantBuffer.Ld = XMVectorSet(lightDiffused_PSM[0], lightDiffused_PSM[1], lightDiffused_PSM[2], lightDiffused_PSM[3]);
        constantBuffer.Ls = XMVectorSet(lightSpecular_PSM[0], lightSpecular_PSM[1], lightSpecular_PSM[2], lightSpecular_PSM[3]);
        constantBuffer.LightPosition = XMVectorSet(lightPosition_PSM[0], lightPosition_PSM[1], lightPosition_PSM[2], lightPosition_PSM[3]);

        constantBuffer.Ka = XMVectorSet(materialAmbient_PSM[0], materialAmbient_PSM[1], materialAmbient_PSM[2], materialAmbient_PSM[3]);
        constantBuffer.Kd = XMVectorSet(materialDiffused_PSM[0], materialDiffused_PSM[1], materialDiffused_PSM[2], materialDiffused_PSM[3]);
        constantBuffer.Ks = XMVectorSet(materialSpecular_PSM[0], materialSpecular_PSM[1], materialSpecular_PSM[2], materialSpecular_PSM[3]);
        constantBuffer.MaterialShininess = materialShininess_PSM;
    } else {
        constantBuffer.KeyPressed = 0;
    }

    gpID3D11DeviceContext_PSM->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_PSM, 0, NULL, &constantBuffer, 0, 0);

    gpID3D11DeviceContext_PSM->DrawIndexed(gNumElements_PSM, 0, 0);

    gpIDXGISwapChain_PSM->Present(0, 0);
}

void Update() {
}

void Uninitialize(void) {
    if (gbFullScreen_PSM == true) {
        dwStyle_PSM = GetWindowLong(ghwnd_PSM, GWL_STYLE);

        SetWindowLong(ghwnd_PSM, GWL_STYLE, (dwStyle_PSM | WS_OVERLAPPEDWINDOW));
        SetWindowPlacement(ghwnd_PSM, &wpPrev_PSM);
        SetWindowPos(ghwnd_PSM,
                     HWND_TOP,
                     0,
                     0,
                     0,
                     0,
                     SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
        ShowCursor(TRUE);
    }

    if (gpID3D11Buffer_ConstantBuffer_PSM) {
        gpID3D11Buffer_ConstantBuffer_PSM->Release();
        gpID3D11Buffer_ConstantBuffer_PSM = NULL;
    }
    if (gpID3D11InputLayout_PSM) {
        gpID3D11InputLayout_PSM->Release();
        gpID3D11InputLayout_PSM = NULL;
    }

    if (gpID3D11Buffer_IndexBuffer_PSM) {
        gpID3D11Buffer_IndexBuffer_PSM->Release();
        gpID3D11Buffer_IndexBuffer_PSM = NULL;
    }

    if (gpID3D11Buffer_VertexBuffer_Normal_Sphere_PSM) {
        gpID3D11Buffer_VertexBuffer_Normal_Sphere_PSM->Release();
        gpID3D11Buffer_VertexBuffer_Normal_Sphere_PSM = NULL;
    }

    if (gpID3D11Buffer_VertexBuffer_Position_Sphere_PSM) {
        gpID3D11Buffer_VertexBuffer_Position_Sphere_PSM->Release();
        gpID3D11Buffer_VertexBuffer_Position_Sphere_PSM = NULL;
    }

    if (gpID3D11PixelShader_PSM) {
        gpID3D11PixelShader_PSM->Release();
        gpID3D11PixelShader_PSM = NULL;
    }
    if (gpID3D11VertexShader_PSM) {
        gpID3D11VertexShader_PSM->Release();
        gpID3D11VertexShader_PSM = NULL;
    }
    if (gpID3D11RenderTargetView_PSM) {
        gpID3D11RenderTargetView_PSM->Release();
        gpID3D11RenderTargetView_PSM = NULL;
    }
    if (gpIDXGISwapChain_PSM) {
        gpIDXGISwapChain_PSM->Release();
        gpIDXGISwapChain_PSM = NULL;
    }
    if (gpID3D11DeviceContext_PSM) {
        gpID3D11DeviceContext_PSM->Release();
        gpID3D11DeviceContext_PSM = NULL;
    }
    if (gpID3D11Device_PSM) {
        gpID3D11Device_PSM->Release();
        gpID3D11Device_PSM = NULL;
    }

    if (gpFile_PSM) {
        fopen_s(&gpFile_PSM, gszLogFileName_PSM, "a+");
        fprintf(gpFile_PSM, "%s\t%s\t%s\t%d Program terminated successfully\n", __DATE__, __TIME__, __FILE__, __LINE__);
        fclose(gpFile_PSM);
        gpFile_PSM = NULL;
    }
}