#include "Icon.h"
#include <d3d11.h>
#include <d3dcompiler.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#pragma warning(disable : 4838)  // to suppress unsigned int to int conversion warning
#include "XNAMath_204\xnamath.h"

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gpFile_ak = NULL;
HWND ghwnd_ak = NULL;
DWORD dwStyle_ak = NULL;
WINDOWPLACEMENT wpPrev_ak = {sizeof(WINDOWPLACEMENT)};
bool gbFullScreen_ak = false;

bool gbActiveWindow_ak = false;
char gszLogFileName_ak[] = "Logs.txt";

IDXGISwapChain *gpIDXGISwapChain_ak = NULL;
ID3D11Device *gpID3D11Device_ak = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext_ak = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView_ak = NULL;

ID3D11VertexShader *gpID3D11VertexShader_ak = NULL;
ID3D11PixelShader *gpID3D11PixelShader_ak = NULL;
ID3D11HullShader *gpID3D11HullShader_ak = NULL;
ID3D11DomainShader *gpID3D11DomainShader_ak = NULL;

ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Position_ak = NULL;

ID3D11InputLayout *gpID3D11InputLayout_ak = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer_HullShader_ak = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer_DomainShader_ak = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer_PixelShader_ak = NULL;

ID3D11RasterizerState *gpID3D11RasterizerState_ak = NULL;
ID3D11DepthStencilView *gpID3D11DepthStencilView_ak = NULL;

struct CBUFFER_HullShader {
    XMVECTOR Hull_Constant_Function_Params;
};
struct CBUFFER_DomainShader {
    XMMATRIX WorldViewProjectionMatrix;
};
struct CBUFFER_PixelShader {
    XMVECTOR LineColor;
};
unsigned int guiNumberOfLineSegments_ak = 1;

XMMATRIX gPerspectiveProjectionMatrix_ak;

float gClearColor_ak[4];

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
    HRESULT Initialize(void);
    void Display(void);
    void Uninitialize(void);
    void Update(void);
    WNDCLASSEX wndclass_ak;
    HWND hwnd_ak;
    MSG msg_ak;
    TCHAR szAppName_ak[] = TEXT("MyApp");
    bool bDone_ak = false;

    if (fopen_s(&gpFile_ak, gszLogFileName_ak, "w") != 0) {
        MessageBox(NULL, TEXT("Failure creating file"), TEXT("File IO failed"), MB_ICONSTOP | MB_OK);
        exit(0);
    } else {
        fclose(gpFile_ak);
    }

    int iScreenWidth = GetSystemMetrics(SM_CXSCREEN);
    int iScreenHeight = GetSystemMetrics(SM_CYSCREEN);

    int iScreenCenterX = iScreenWidth / 2;
    int iScreenCenterY = iScreenHeight / 2;

    int iWindowCenterX = WIN_WIDTH / 2;
    int iWindowsCenterY = WIN_HEIGHT / 2;

    int iWindowX = iScreenCenterX - iWindowCenterX;
    int iWindowY = iScreenCenterY - iWindowsCenterY;

    wndclass_ak.cbSize = sizeof(WNDCLASSEX);
    wndclass_ak.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wndclass_ak.cbClsExtra = 0;
    wndclass_ak.cbWndExtra = 0;
    wndclass_ak.hInstance = hInstance;
    wndclass_ak.lpfnWndProc = WndProc;
    wndclass_ak.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));
    wndclass_ak.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass_ak.lpszClassName = szAppName_ak;
    wndclass_ak.lpszMenuName = NULL;
    wndclass_ak.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass_ak.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));

    RegisterClassEx(&wndclass_ak);

    hwnd_ak = CreateWindowEx(
        WS_EX_APPWINDOW, szAppName_ak, TEXT("D3D11 Tessellation shader"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, iWindowX, iWindowY, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);
    ghwnd_ak = hwnd_ak;

    HRESULT hr_ak = Initialize();
    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "Initialize() failed.\n");
        fclose(gpFile_ak);
        hwnd_ak = NULL;
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "Initialize() succeeded.\n");
        fclose(gpFile_ak);
    }
    ShowWindow(hwnd_ak, iCmdShow);
    SetForegroundWindow(hwnd_ak);
    SetFocus(hwnd_ak);

    while (bDone_ak == false) {
        if (PeekMessage(&msg_ak, NULL, 0, 0, PM_REMOVE)) {
            if (msg_ak.message == WM_QUIT) {
                bDone_ak = true;
            } else {
                TranslateMessage(&msg_ak);
                DispatchMessage(&msg_ak);
            }
        } else {
            if (gbActiveWindow_ak == true) {
                Display();
                Update();
            }
        }
    }
    Uninitialize();

    return ((int)msg_ak.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
    void ToggleFullScreen(void);
    HRESULT Resize(int, int);
    void Uninitialize(void);
    HRESULT hr_ak;
    switch (iMsg) {
        case WM_SETFOCUS: {
            gbActiveWindow_ak = true;
            break;
        }
        case WM_KILLFOCUS: {
            gbActiveWindow_ak = false;
            break;
        }
        case WM_ERASEBKGND: {
            return (0);
        }
        case WM_SIZE: {
            if (gpID3D11DeviceContext_ak) {
                hr_ak = Resize(LOWORD(lParam), HIWORD(lParam));
                if (FAILED(hr_ak)) {
                    fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
                    fprintf_s(gpFile_ak, "Resize() Failed.\n");
                    fclose(gpFile_ak);
                    return (hr_ak);
                } else {
                    fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
                    fprintf_s(gpFile_ak, "Resize() Succeeded.\n");
                    fclose(gpFile_ak);
                }
            }
            break;
        }
        case WM_KEYDOWN: {
            switch (wParam) {
                case VK_ESCAPE: {
                    DestroyWindow(hwnd);
                    break;
                }
                case 0x46:
                case 0x66: {
                    ToggleFullScreen();
                    break;
                }
                case VK_UP: {
                    guiNumberOfLineSegments_ak++;
                    if (guiNumberOfLineSegments_ak >= 30) {
                        guiNumberOfLineSegments_ak = 30;
                    }
                    break;
                }
                case VK_DOWN: {
                    guiNumberOfLineSegments_ak--;
                    if (guiNumberOfLineSegments_ak <= 0) {
                        guiNumberOfLineSegments_ak = 1;
                    }
                    break;
                }
                default: {
                    break;
                }
            }
            break;
        }
        case WM_CLOSE: {
            DestroyWindow(hwnd);
            break;
        }
        case WM_DESTROY: {
            Uninitialize();
            PostQuitMessage(0);
            break;
        }
        default: {
            break;
        }
    }

    return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void) {
    MONITORINFO mi_ak = {sizeof(MONITORINFO)};

    if (gbFullScreen_ak == false) {

        dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);
        if ((dwStyle_ak & WS_OVERLAPPEDWINDOW)) {
            if ((GetWindowPlacement(ghwnd_ak, &wpPrev_ak) &&
                 (GetMonitorInfo(MonitorFromWindow(ghwnd_ak, MONITORINFOF_PRIMARY), &mi_ak)))) {
                SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak & (~WS_OVERLAPPEDWINDOW)));
                SetWindowPos(ghwnd_ak,
                             HWND_TOP,
                             mi_ak.rcMonitor.left,
                             mi_ak.rcMonitor.top,
                             (mi_ak.rcMonitor.right - mi_ak.rcMonitor.left),
                             (mi_ak.rcMonitor.bottom - mi_ak.rcMonitor.top),
                             SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }
        ShowCursor(FALSE);
        gbFullScreen_ak = true;
    } else {
        SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak | WS_OVERLAPPEDWINDOW));
        SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
        SetWindowPos(ghwnd_ak,
                     HWND_TOP,
                     0,
                     0,
                     0,
                     0,
                     (SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOOWNERZORDER));
        ShowCursor(TRUE);
        gbFullScreen_ak = false;
    }
}

HRESULT Initialize(void) {
    void Uninitialize(void);
    HRESULT Resize(int, int);

    HRESULT hr_ak;

    D3D_DRIVER_TYPE d3dDriverType_ak;
    D3D_DRIVER_TYPE d3dDriverTypes[] =
        {
            D3D_DRIVER_TYPE_HARDWARE,
            D3D_DRIVER_TYPE_WARP,
            D3D_DRIVER_TYPE_REFERENCE};

    D3D_FEATURE_LEVEL d3dFeatureLevel_required_ak = D3D_FEATURE_LEVEL_11_0;
    D3D_FEATURE_LEVEL d3dFeatureLevel_acquired_ak = D3D_FEATURE_LEVEL_10_0;

    UINT createDeviceFlags_ak = 0;
    UINT numDriverTypes_ak = 0;
    UINT numFeatureLevels_ak = 1;

    numDriverTypes_ak = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);

    DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
    ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
    dxgiSwapChainDesc.BufferCount = 1;
    dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
    dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
    dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
    dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
    dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    dxgiSwapChainDesc.OutputWindow = ghwnd_ak;
    dxgiSwapChainDesc.SampleDesc.Count = 1;
    dxgiSwapChainDesc.SampleDesc.Quality = 0;
    dxgiSwapChainDesc.Windowed = TRUE;

    for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes_ak; driverTypeIndex++) {
        d3dDriverType_ak = d3dDriverTypes[driverTypeIndex];
        hr_ak = D3D11CreateDeviceAndSwapChain(
            NULL,
            d3dDriverType_ak,
            NULL,
            createDeviceFlags_ak,
            &d3dFeatureLevel_required_ak,
            numFeatureLevels_ak,
            D3D11_SDK_VERSION,
            &dxgiSwapChainDesc,
            &gpIDXGISwapChain_ak,
            &gpID3D11Device_ak,
            &d3dFeatureLevel_acquired_ak,
            &gpID3D11DeviceContext_ak);
        if (SUCCEEDED(hr_ak))
            break;
    }
    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "D3D11CreateDeviceAndSwapChain() Failed.\n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "D3D11CreateDeviceAndSwapChain() Succeeded.\n");

        fprintf_s(gpFile_ak, "The Chosen Driver is of ");
        if (d3dDriverType_ak == D3D_DRIVER_TYPE_HARDWARE) {
            fprintf_s(gpFile_ak, "Hardware Type.\n");
        } else if (d3dDriverType_ak == D3D_DRIVER_TYPE_WARP) {
            fprintf_s(gpFile_ak, "WRAP Type.\n");
        } else if (d3dDriverType_ak == D3D_DRIVER_TYPE_REFERENCE) {
            fprintf_s(gpFile_ak, "Reference Type.\n");
        } else {
            fprintf_s(gpFile_ak, "Unknown Type.\n");
        }

        fprintf_s(gpFile_ak, "The Supported Highest Feature Level Is");

        if (d3dFeatureLevel_acquired_ak == D3D_FEATURE_LEVEL_11_0) {
            fprintf_s(gpFile_ak, "11.0\n");
        }
        if (d3dFeatureLevel_acquired_ak == D3D_FEATURE_LEVEL_10_1) {
            fprintf_s(gpFile_ak, "10.1\n");
        }
        if (d3dFeatureLevel_acquired_ak == D3D_FEATURE_LEVEL_10_0) {
            fprintf_s(gpFile_ak, "10.0\n");
        } else {
            fprintf_s(gpFile_ak, "Unknown\n");
        }

        fclose(gpFile_ak);
    }

    const char *vertexShaderSourceCode_ak =
        "struct vertex_output"
        "{"
        "float4 position: POSITION;"
        "};"
        "vertex_output main(float2 pos: POSITION)"
        "{"
        "vertex_output output;"
        "output.position=float4(pos, 0.0f, 1.0f);"
        "return(output);"
        "}";
    ID3DBlob *pID3DBlob_VertexShaderCode_ak = NULL;
    ID3DBlob *pID3DBlob_Error_ak = NULL;

    hr_ak = D3DCompile(vertexShaderSourceCode_ak,
                        lstrlenA(vertexShaderSourceCode_ak) + 1,
                        "VS",
                        NULL,
                        D3D_COMPILE_STANDARD_FILE_INCLUDE,
                        "main",
                        "vs_5_0",
                        0,
                        0,
                        &pID3DBlob_VertexShaderCode_ak,
                        &pID3DBlob_Error_ak);

    if (FAILED(hr_ak)) {
        if (pID3DBlob_Error_ak != NULL) {
            fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
            fprintf_s(gpFile_ak, "D3DCompile() Failed For Vertex Shader : %s.\n", (char *)pID3DBlob_Error_ak->GetBufferPointer());
            fclose(gpFile_ak);
            pID3DBlob_Error_ak->Release();
            pID3DBlob_Error_ak = NULL;
            return (hr_ak);
        } else {
            fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
            fprintf_s(gpFile_ak, "D3DCompile() Failed For Vertex Shader due to some COM error.\n");
            fclose(gpFile_ak);
        }
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "D3DCompile() Succeeded For Vertex Shader.\n");
        fclose(gpFile_ak);
    }

    hr_ak = gpID3D11Device_ak->CreateVertexShader(pID3DBlob_VertexShaderCode_ak->GetBufferPointer(),
                                                    pID3DBlob_VertexShaderCode_ak->GetBufferSize(),
                                                    NULL,
                                                    &gpID3D11VertexShader_ak);

    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateVertexShader() Failed .\n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateVertexShader() Succeeded.\n");
        fclose(gpFile_ak);
    }

    gpID3D11DeviceContext_ak->VSSetShader(gpID3D11VertexShader_ak, NULL, 0);

    // Hull
    const char *hullShaderSourceCode_ak =
        "cbuffer ConstantBuffer"
        "{"
        "float4 Hull_Constant_Function_Params;"
        "}"
        "struct vertex_output"
        "{"
        "float4 position: POSITION;"
        "};"
        "struct hull_constant_output"
        "{"
        "float edges[2] : SV_TESSFACTOR;"
        "};"
        "hull_constant_output hull_constant_function(void)"
        "{"
        "hull_constant_output output;"
        "float numberOfLineStrips=Hull_Constant_Function_Params[0];"
        "float numberOfLineSegments=Hull_Constant_Function_Params[1];"
        "output.edges[0]=numberOfLineStrips;"
        "output.edges[1]=numberOfLineSegments;"
        "return(output);"
        "}"
        "struct hull_output"
        "{"
        "float4 position : POSITION;"
        "};"
        "[domain(\"isoline\")]"
        "[partitioning(\"integer\")]"
        "[outputtopology(\"line\")]"
        "[outputcontrolpoints(4)]"
        "[patchconstantfunc(\"hull_constant_function\")]"
        "hull_output main(InputPatch<vertex_output,4> input_patch, uint i : SV_OUTPUTCONTROLPOINTID)"
        "{"
        "hull_output output;"
        "output.position = input_patch[i].position;"
        "return(output);"
        "}";

    ID3DBlob *pID3DBlob_HullShaderCode_ak = NULL;
    pID3DBlob_Error_ak = NULL;

    hr_ak = D3DCompile(hullShaderSourceCode_ak,
                        lstrlenA(hullShaderSourceCode_ak) + 1,
                        "HS",
                        NULL,
                        D3D_COMPILE_STANDARD_FILE_INCLUDE,
                        "main",
                        "hs_5_0",
                        0,
                        0,
                        &pID3DBlob_HullShaderCode_ak,
                        &pID3DBlob_Error_ak);

    if (FAILED(hr_ak)) {
        if (pID3DBlob_Error_ak != NULL) {
            fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
            fprintf_s(gpFile_ak, "D3DCompile() Failed For Pixel Shader : %s.\n", (char *)pID3DBlob_Error_ak->GetBufferPointer());
            fclose(gpFile_ak);
            pID3DBlob_Error_ak->Release();
            pID3DBlob_Error_ak = NULL;
            return (hr_ak);
        } else {
            fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
            fprintf_s(gpFile_ak, "D3DCompile() Failed For Pixel Shader due to some COM error.\n");
            fclose(gpFile_ak);
        }
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "D3DCompile() Succeeded For Pixel Shader\n");
        fclose(gpFile_ak);
    }

    hr_ak = gpID3D11Device_ak->CreateHullShader(pID3DBlob_HullShaderCode_ak->GetBufferPointer(),
                                                  pID3DBlob_HullShaderCode_ak->GetBufferSize(),
                                                  NULL,
                                                  &gpID3D11HullShader_ak);

    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreatePixelShader() Failed \n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreatePixelShader() Succeeded.\n");
        fclose(gpFile_ak);
    }

    gpID3D11DeviceContext_ak->HSSetShader(gpID3D11HullShader_ak, NULL, 0);
    pID3DBlob_HullShaderCode_ak->Release();
    pID3DBlob_HullShaderCode_ak = NULL;

    // Domain

    const char *domainShaderSourceCode_ak =
        "cbuffer ConstantBuffer"
        "{"
        "float4x4 WorldViewProjectionMatrix;"
        "}"
        "struct hull_output"
        "{"
        "float4 position:POSITION;"
        "};"
        "struct hull_constant_output"
        "{"
        "float edges[2] : SV_TESSFACTOR;"
        "};"
        "struct domain_output"
        "{"
        "float4 position:SV_POSITION;"
        "};"
        "[domain(\"isoline\")]"
        "domain_output main(hull_constant_output input, OutputPatch<hull_output,4> output_patch, float2 tessCoord : SV_DOMAINLOCATION)"
        "{"
        "domain_output output;"
        "float3 p0 = output_patch[0].position.xyz;"
        "float3 p1 = output_patch[1].position.xyz;"
        "float3 p2 = output_patch[2].position.xyz;"
        "float3 p3 = output_patch[3].position.xyz;"
        "float3 p =	p0 * ((1.0f - tessCoord.x) * (1.0f - tessCoord.x) *	(1.0f - tessCoord.x)) + p1 * 3.0 * (tessCoord.x * (1.0f - tessCoord.x) * (1.0f - tessCoord.x)) + p2 * 3.0 * (tessCoord.x * tessCoord.x * (1.0f - tessCoord.x)) + p3 * (tessCoord.x * tessCoord.x * tessCoord.x);"
        "float4 position = float4(p, 1.0f);"
        "output.position = mul(WorldViewProjectionMatrix, position);"
        "return(output);"
        "}";

    ID3DBlob *pID3DBlob_DomainShaderCode_ak = NULL;
    pID3DBlob_Error_ak = NULL;

    hr_ak = D3DCompile(domainShaderSourceCode_ak,
                        lstrlenA(domainShaderSourceCode_ak) + 1,
                        "DS",
                        NULL,
                        D3D_COMPILE_STANDARD_FILE_INCLUDE,
                        "main",
                        "ds_5_0",
                        0,
                        0,
                        &pID3DBlob_DomainShaderCode_ak,
                        &pID3DBlob_Error_ak);

    if (FAILED(hr_ak)) {
        if (pID3DBlob_Error_ak != NULL) {
            fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
            fprintf_s(gpFile_ak, "D3DCompile() Failed For Pixel Shader : %s.\n", (char *)pID3DBlob_Error_ak->GetBufferPointer());
            fclose(gpFile_ak);
            pID3DBlob_Error_ak->Release();
            pID3DBlob_Error_ak = NULL;
            return (hr_ak);
        } else {
            fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
            fprintf_s(gpFile_ak, "D3DCompile() Failed For Pixel Shader due to some COM error.\n");
            fclose(gpFile_ak);
        }
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "D3DCompile() Succeeded For Pixel Shader\n");
        fclose(gpFile_ak);
    }

    hr_ak = gpID3D11Device_ak->CreateDomainShader(pID3DBlob_DomainShaderCode_ak->GetBufferPointer(),
                                                    pID3DBlob_DomainShaderCode_ak->GetBufferSize(),
                                                    NULL,
                                                    &gpID3D11DomainShader_ak);

    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreatePixelShader() Failed \n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreatePixelShader() Succeeded.\n");
        fclose(gpFile_ak);
    }

    gpID3D11DeviceContext_ak->DSSetShader(gpID3D11DomainShader_ak, NULL, 0);
    pID3DBlob_DomainShaderCode_ak->Release();
    pID3DBlob_DomainShaderCode_ak = NULL;

    //Pixel
    const char *pixelShaderSourceCode_ak =
        "cbuffer ConstantBuffer"
        "{"
        "float4 LineColor;"
        "}"
        "float4 main(void) : SV_TARGET"
        "{"
        "float4 color = LineColor;"
        "return(color);"
        "}";

    ID3DBlob *pID3DBlob_PixelShaderCode_ak = NULL;
    pID3DBlob_Error_ak = NULL;

    hr_ak = D3DCompile(pixelShaderSourceCode_ak,
                        lstrlenA(pixelShaderSourceCode_ak) + 1,
                        "PS",
                        NULL,
                        D3D_COMPILE_STANDARD_FILE_INCLUDE,
                        "main",
                        "ps_5_0",
                        0,
                        0,
                        &pID3DBlob_PixelShaderCode_ak,
                        &pID3DBlob_Error_ak);

    if (FAILED(hr_ak)) {
        if (pID3DBlob_Error_ak != NULL) {
            fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
            fprintf_s(gpFile_ak, "D3DCompile() Failed For Pixel Shader : %s.\n", (char *)pID3DBlob_Error_ak->GetBufferPointer());
            fclose(gpFile_ak);
            pID3DBlob_Error_ak->Release();
            pID3DBlob_Error_ak = NULL;
            return (hr_ak);
        } else {
            fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
            fprintf_s(gpFile_ak, "D3DCompile() Failed For Pixel Shader due to some COM error.\n");
            fclose(gpFile_ak);
        }
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "D3DCompile() Succeeded For Pixel Shader\n");
        fclose(gpFile_ak);
    }

    hr_ak = gpID3D11Device_ak->CreatePixelShader(pID3DBlob_PixelShaderCode_ak->GetBufferPointer(),
                                                   pID3DBlob_PixelShaderCode_ak->GetBufferSize(),
                                                   NULL,
                                                   &gpID3D11PixelShader_ak);

    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreatePixelShader() Failed \n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreatePixelShader() Succeeded.\n");
        fclose(gpFile_ak);
    }

    gpID3D11DeviceContext_ak->PSSetShader(gpID3D11PixelShader_ak, NULL, 0);
    pID3DBlob_PixelShaderCode_ak->Release();
    pID3DBlob_PixelShaderCode_ak = NULL;

    D3D11_INPUT_ELEMENT_DESC inputElementDesc_ak[1];

    inputElementDesc_ak[0].SemanticName = "POSITION";
    inputElementDesc_ak[0].SemanticIndex = 0;
    inputElementDesc_ak[0].Format = DXGI_FORMAT_R32G32_FLOAT;
    inputElementDesc_ak[0].InputSlot = 0;
    inputElementDesc_ak[0].AlignedByteOffset = 0;
    inputElementDesc_ak[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
    inputElementDesc_ak[0].InstanceDataStepRate = 0;

    hr_ak = gpID3D11Device_ak->CreateInputLayout(inputElementDesc_ak, _ARRAYSIZE(inputElementDesc_ak),
                                                   pID3DBlob_VertexShaderCode_ak->GetBufferPointer(),
                                                   pID3DBlob_VertexShaderCode_ak->GetBufferSize(),
                                                   &gpID3D11InputLayout_ak);

    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "D3D11Device::CreateInput() Failed.\n");
        fclose(gpFile_ak);
        pID3DBlob_VertexShaderCode_ak->Release();
        pID3DBlob_VertexShaderCode_ak = NULL;
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateInput() Succeeded.\n");
        fclose(gpFile_ak);
        pID3DBlob_VertexShaderCode_ak->Release();
        pID3DBlob_VertexShaderCode_ak = NULL;
    }

    float vertices_ak[] = {
        -1.0f, -1.0f,
        -0.5f, 1.0f,
        0.5f, -1.0f,
        1.0f, 1.0f};

    D3D11_BUFFER_DESC bufferDesc_ak;
    ZeroMemory(&bufferDesc_ak, sizeof(D3D11_BUFFER_DESC));

    bufferDesc_ak.Usage = D3D11_USAGE_DYNAMIC;
    bufferDesc_ak.ByteWidth = sizeof(float) * _ARRAYSIZE(vertices_ak);
    bufferDesc_ak.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    bufferDesc_ak.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

    hr_ak = gpID3D11Device_ak->CreateBuffer(&bufferDesc_ak, NULL,
                                              &gpID3D11Buffer_VertexBuffer_Position_ak);

    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateBuffer() Failed For Vertex Buffer.\n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateBuffer() Succeeded For Vertex Buffer.\n");
        fclose(gpFile_ak);
    }
    gpID3D11DeviceContext_ak->IASetInputLayout(gpID3D11InputLayout_ak);

    D3D11_MAPPED_SUBRESOURCE mappedSubresource_ak;
    ZeroMemory(&mappedSubresource_ak, sizeof(D3D11_MAPPED_SUBRESOURCE));
    gpID3D11DeviceContext_ak->Map(gpID3D11Buffer_VertexBuffer_Position_ak, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource_ak);
    memcpy(mappedSubresource_ak.pData, vertices_ak, sizeof(vertices_ak));
    gpID3D11DeviceContext_ak->Unmap(gpID3D11Buffer_VertexBuffer_Position_ak, NULL);

    D3D11_BUFFER_DESC bufferDesc_ConstantBuffer_ak;
    ZeroMemory(&bufferDesc_ConstantBuffer_ak, sizeof(D3D11_BUFFER_DESC));
    bufferDesc_ConstantBuffer_ak.Usage = D3D11_USAGE_DEFAULT;
    bufferDesc_ConstantBuffer_ak.ByteWidth = sizeof(CBUFFER_HullShader);
    bufferDesc_ConstantBuffer_ak.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    hr_ak = gpID3D11Device_ak->CreateBuffer(&bufferDesc_ConstantBuffer_ak, nullptr, &gpID3D11Buffer_ConstantBuffer_HullShader_ak);

    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateBuffer() Failed for Constant Buffer\n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateBuffer() Succeeded for Constant Buffer.\n");
        fclose(gpFile_ak);
    }
    gpID3D11DeviceContext_ak->HSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer_HullShader_ak);

    ZeroMemory(&bufferDesc_ConstantBuffer_ak, sizeof(D3D11_BUFFER_DESC));
    bufferDesc_ConstantBuffer_ak.Usage = D3D11_USAGE_DEFAULT;
    bufferDesc_ConstantBuffer_ak.ByteWidth = sizeof(CBUFFER_DomainShader);
    bufferDesc_ConstantBuffer_ak.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    hr_ak = gpID3D11Device_ak->CreateBuffer(&bufferDesc_ConstantBuffer_ak, nullptr, &gpID3D11Buffer_ConstantBuffer_DomainShader_ak);

    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateBuffer() Failed for Constant Buffer\n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateBuffer() Succeeded for Constant Buffer.\n");
        fclose(gpFile_ak);
    }
    gpID3D11DeviceContext_ak->DSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer_DomainShader_ak);

    ZeroMemory(&bufferDesc_ConstantBuffer_ak, sizeof(D3D11_BUFFER_DESC));
    bufferDesc_ConstantBuffer_ak.Usage = D3D11_USAGE_DEFAULT;
    bufferDesc_ConstantBuffer_ak.ByteWidth = sizeof(CBUFFER_PixelShader);
    bufferDesc_ConstantBuffer_ak.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    hr_ak = gpID3D11Device_ak->CreateBuffer(&bufferDesc_ConstantBuffer_ak, nullptr, &gpID3D11Buffer_ConstantBuffer_PixelShader_ak);

    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateBuffer() Failed for Constant Buffer\n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateBuffer() Succeeded for Constant Buffer.\n");
        fclose(gpFile_ak);
    }

    gpID3D11DeviceContext_ak->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer_PixelShader_ak);

    // create and set Rasterizer State

    D3D11_RASTERIZER_DESC d3d11RasterizedDesc;
    ZeroMemory(&d3d11RasterizedDesc, sizeof(D3D11_RASTERIZER_DESC));

    d3d11RasterizedDesc.AntialiasedLineEnable = FALSE;
    d3d11RasterizedDesc.CullMode = D3D11_CULL_NONE;
    d3d11RasterizedDesc.DepthBias = 0;
    d3d11RasterizedDesc.DepthBiasClamp = 0.0f,
    d3d11RasterizedDesc.DepthClipEnable = TRUE;
    d3d11RasterizedDesc.FillMode = D3D11_FILL_SOLID;
    d3d11RasterizedDesc.FrontCounterClockwise = FALSE;
    d3d11RasterizedDesc.MultisampleEnable = FALSE;
    d3d11RasterizedDesc.ScissorEnable = FALSE;
    d3d11RasterizedDesc.SlopeScaledDepthBias = 0.0f;

    hr_ak = gpID3D11Device_ak->CreateRasterizerState(&d3d11RasterizedDesc,
                                                       &gpID3D11RasterizerState_ak);

    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateRasterizerState() failed for culling\n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "ID3D11Device::CreateRasterizerState() succeeded for culling\n");
        fclose(gpFile_ak);
    }

    gpID3D11DeviceContext_ak->RSSetState(gpID3D11RasterizerState_ak);

    gClearColor_ak[0] = 0.0f;
    gClearColor_ak[1] = 0.0f;
    gClearColor_ak[2] = 0.0f;
    gClearColor_ak[3] = 1.0f;
    gPerspectiveProjectionMatrix_ak = XMMatrixIdentity();

    hr_ak = Resize(WIN_WIDTH, WIN_HEIGHT);
    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "Resize() Failed.\n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "Resize() Succeeded.\n");
        fclose(gpFile_ak);
    }
    return (S_OK);
}

HRESULT Resize(int width, int height) {
    HRESULT hr_ak = S_OK;

    if (gpID3D11RenderTargetView_ak) {
        gpID3D11RenderTargetView_ak->Release();
        gpID3D11RenderTargetView_ak = NULL;
    }

    gpIDXGISwapChain_ak->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

    ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
    gpIDXGISwapChain_ak->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID *)&pID3D11Texture2D_BackBuffer);

    hr_ak = gpID3D11Device_ak->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView_ak);
    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "CreateRenderTargetView() Failed.\n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "CreateRenderTargetView() Succeeded.\n");
        fclose(gpFile_ak);
    }

    pID3D11Texture2D_BackBuffer->Release();
    pID3D11Texture2D_BackBuffer = NULL;

    // Depth

    if (gpID3D11DepthStencilView_ak) {
        gpID3D11DepthStencilView_ak->Release();
        gpID3D11DepthStencilView_ak = NULL;
    }

    D3D11_TEXTURE2D_DESC d3d11TextureDesc_ak;
    ZeroMemory(&d3d11TextureDesc_ak, sizeof(D3D11_TEXTURE2D_DESC));
    // Necessary for depth
    d3d11TextureDesc_ak.Width = (UINT)width;
    d3d11TextureDesc_ak.Height = (UINT)height;
    d3d11TextureDesc_ak.Format = DXGI_FORMAT_D32_FLOAT;
    d3d11TextureDesc_ak.Usage = D3D11_USAGE_DEFAULT;
    d3d11TextureDesc_ak.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    d3d11TextureDesc_ak.SampleDesc.Count = 1;
    d3d11TextureDesc_ak.SampleDesc.Quality = 0;
    // Extra
    d3d11TextureDesc_ak.ArraySize = 1;
    d3d11TextureDesc_ak.MipLevels = 1;
    d3d11TextureDesc_ak.CPUAccessFlags = 0;
    d3d11TextureDesc_ak.MiscFlags = 0;

    ID3D11Texture2D *pID3D11Texture2D_DepthBuffer_ak = NULL;
    gpID3D11Device_ak->CreateTexture2D(&d3d11TextureDesc_ak, NULL,
                                        &pID3D11Texture2D_DepthBuffer_ak);

    D3D11_DEPTH_STENCIL_VIEW_DESC d3d11DepthStencilViewDesc_ak;
    ZeroMemory(&d3d11DepthStencilViewDesc_ak, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

    d3d11DepthStencilViewDesc_ak.Format = DXGI_FORMAT_D32_FLOAT;
    d3d11DepthStencilViewDesc_ak.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

    hr_ak = gpID3D11Device_ak->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer_ak, &d3d11DepthStencilViewDesc_ak,
                                                        &gpID3D11DepthStencilView_ak);

    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "CreateDepthStencilView() Failed.\n");
        fclose(gpFile_ak);
        return hr_ak;
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "CreateDepthStencilView() Succeeded.\n");
        fclose(gpFile_ak);
    }

    pID3D11Texture2D_DepthBuffer_ak->Release();
    pID3D11Texture2D_DepthBuffer_ak = NULL;

    if (height < 0) {
        height = 1;
    }

    gpID3D11DeviceContext_ak->OMSetRenderTargets(1, &gpID3D11RenderTargetView_ak, gpID3D11DepthStencilView_ak);

    D3D11_VIEWPORT d3dViewPort;
    d3dViewPort.TopLeftX = 0;
    d3dViewPort.TopLeftY = 0;
    d3dViewPort.Width = (float)width;
    d3dViewPort.Height = (float)height;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    gPerspectiveProjectionMatrix_ak = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0), (float)width / (float)height, 0.1f, 100.0f);

    return hr_ak;
}

void Display(void) {
    gpID3D11DeviceContext_ak->ClearRenderTargetView(gpID3D11RenderTargetView_ak, gClearColor_ak);
    gpID3D11DeviceContext_ak->ClearDepthStencilView(gpID3D11DepthStencilView_ak, D3D11_CLEAR_DEPTH, 1.0f, 0);
    UINT stride_ak = sizeof(float) * 2;
    UINT offset_ak = 0;

    gpID3D11DeviceContext_ak->IASetVertexBuffers(0, 1, &gpID3D11Buffer_VertexBuffer_Position_ak, &stride_ak, &offset_ak);
    gpID3D11DeviceContext_ak->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_4_CONTROL_POINT_PATCHLIST);

    XMMATRIX worldMatrix = XMMatrixIdentity();
    XMMATRIX viewMatrix = XMMatrixIdentity();
    XMMATRIX translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 3.0f);
    worldMatrix = translationMatrix;
    XMMATRIX mvpMatrix = worldMatrix * viewMatrix * gPerspectiveProjectionMatrix_ak;

    CBUFFER_HullShader constantBuffer_HullShader;
    constantBuffer_HullShader.Hull_Constant_Function_Params = XMVectorSet(1.0f, (FLOAT)guiNumberOfLineSegments_ak, 0.0f, 0.0f);
    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_HullShader_ak, 0, NULL, &constantBuffer_HullShader, 0, 0);

    CBUFFER_DomainShader constantBuffer_DomainShader;
    constantBuffer_DomainShader.WorldViewProjectionMatrix = mvpMatrix;
    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_DomainShader_ak, 0, NULL, &constantBuffer_DomainShader, 0, 0);

    CBUFFER_PixelShader constantBuffer_PixelShader;
    constantBuffer_PixelShader.LineColor = XMVectorSet(1.0f, 1.0f, 0.0f, 0.0f);
    gpID3D11DeviceContext_ak->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_PixelShader_ak, 0, NULL, &constantBuffer_PixelShader, 0, 0);

    gpID3D11DeviceContext_ak->Draw(4, 0);

    gpIDXGISwapChain_ak->Present(0, 0);
}

void Update() {
}

void Uninitialize(void) {
    if (gbFullScreen_ak == true) {
        dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);

        SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak | WS_OVERLAPPEDWINDOW));
        SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
        SetWindowPos(ghwnd_ak,
                     HWND_TOP,
                     0,
                     0,
                     0,
                     0,
                     SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
        ShowCursor(TRUE);
    }

    if (gpID3D11Buffer_ConstantBuffer_PixelShader_ak) {
        gpID3D11Buffer_ConstantBuffer_PixelShader_ak->Release();
        gpID3D11Buffer_ConstantBuffer_PixelShader_ak = NULL;
    }

    if (gpID3D11Buffer_ConstantBuffer_DomainShader_ak) {
        gpID3D11Buffer_ConstantBuffer_DomainShader_ak->Release();
        gpID3D11Buffer_ConstantBuffer_DomainShader_ak = NULL;
    }

    if (gpID3D11Buffer_ConstantBuffer_HullShader_ak) {
        gpID3D11Buffer_ConstantBuffer_HullShader_ak->Release();
        gpID3D11Buffer_ConstantBuffer_HullShader_ak = NULL;
    }

    if (gpID3D11InputLayout_ak) {
        gpID3D11InputLayout_ak->Release();
        gpID3D11InputLayout_ak = NULL;
    }
    if (gpID3D11Buffer_VertexBuffer_Position_ak) {
        gpID3D11Buffer_VertexBuffer_Position_ak->Release();
        gpID3D11Buffer_VertexBuffer_Position_ak = NULL;
    }
    if (gpID3D11PixelShader_ak) {
        gpID3D11PixelShader_ak->Release();
        gpID3D11PixelShader_ak = NULL;
    }
    if (gpID3D11DomainShader_ak) {
        gpID3D11DomainShader_ak->Release();
        gpID3D11DomainShader_ak = NULL;
    }
    if (gpID3D11HullShader_ak) {
        gpID3D11HullShader_ak->Release();
        gpID3D11HullShader_ak = NULL;
    }
    if (gpID3D11VertexShader_ak) {
        gpID3D11VertexShader_ak->Release();
        gpID3D11VertexShader_ak = NULL;
    }
    if (gpID3D11RenderTargetView_ak) {
        gpID3D11RenderTargetView_ak->Release();
        gpID3D11RenderTargetView_ak = NULL;
    }
    if (gpIDXGISwapChain_ak) {
        gpIDXGISwapChain_ak->Release();
        gpIDXGISwapChain_ak = NULL;
    }
    if (gpID3D11DeviceContext_ak) {
        gpID3D11DeviceContext_ak->Release();
        gpID3D11DeviceContext_ak = NULL;
    }
    if (gpID3D11Device_ak) {
        gpID3D11Device_ak->Release();
        gpID3D11Device_ak = NULL;
    }

    if (gpFile_ak) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fclose(gpFile_ak);
        gpFile_ak = NULL;
    }
}