#include <d3d11.h>
#include <math.h>
#include <stdio.h>

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")  //directX graphics infrastructure

int main() 
{
    IDXGIFactory *pIdxgiFactory = NULL;
    IDXGIAdapter *pIdxgiAdapter = NULL;
    DXGI_ADAPTER_DESC dxgiAdapterDesc;
    HRESULT hResult;
    char str[255];

    hResult = CreateDXGIFactory(__uuidof(IDXGIFactory), (void **)&pIdxgiFactory);
    if (FAILED(hResult)) {
        printf("DXGI Factory cannot be found");
        goto Cleanup;
    }

    if (pIdxgiFactory->EnumAdapters(0, &pIdxgiAdapter) == DXGI_ERROR_NOT_FOUND) {
        printf("DXGI Adapter cannot be found");
        goto Cleanup;
    }

    ZeroMemory((void *)&dxgiAdapterDesc, sizeof(dxgiAdapterDesc));
    hResult = pIdxgiAdapter->GetDesc(&dxgiAdapterDesc);

    WideCharToMultiByte(CP_ACP, 0, dxgiAdapterDesc.Description, 255, str, 255, NULL, NULL);

    printf("Graphic Card name = %s\n", str);
    printf("Graphic Card VRAM = %I64d GB", (__int64)(ceil(dxgiAdapterDesc.DedicatedVideoMemory / 1024.0 / 1024.0 / 1024.0)));

Cleanup:
    if (pIdxgiAdapter) {
        pIdxgiAdapter->Release();
        pIdxgiAdapter = NULL;
    }

    if (pIdxgiFactory) {
        pIdxgiFactory->Release();
        pIdxgiFactory = NULL;
    }
    return 0;
}