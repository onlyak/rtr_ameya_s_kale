#include "Icon.h"

#include <d3d11.h>
#include <stdio.h>
#include <windows.h>
#pragma comment(lib, "d3d11.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gpFile_ak = NULL;
char gszLogFileName_ak[] = "Logs.txt";

HWND ghwnd_ak = NULL;
DWORD dwStyle_ak = NULL;
WINDOWPLACEMENT wpPrev_ak = {sizeof(WINDOWPLACEMENT)};

bool gbActiveWindow_ak = false;
bool gbFullScreen_ak = false;
float gClearColor_ak[4];

IDXGISwapChain* gpIDXGISwapChain_ak = NULL;
ID3D11Device* gpID3D11Device_ak = NULL;
ID3D11DeviceContext* gpID3D11DeviceContext_ak = NULL;
ID3D11RenderTargetView* gpID3D11RenderTargetView_ak = NULL;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
    HRESULT Initialize(void);
    void Display(void);
    void Uninitialize(void);
    WNDCLASSEX wndclass_ak;
    HWND hwnd_ak;
    MSG msg_ak;
    TCHAR szAppName_ak[] = TEXT("MyApp");
    bool bDone_ak = false;

    if (fopen_s(&gpFile_ak, gszLogFileName_ak, "w") != 0) {
        MessageBox(NULL, TEXT("Failure creating file"), TEXT("File IO failed"), MB_ICONSTOP | MB_OK);
        exit(0);
    } else {
        fprintf(gpFile_ak, "File successfully create and program started\n");
        fclose(gpFile_ak);
    }

    int iScreenWidth = GetSystemMetrics(SM_CXSCREEN);
    int iScreenHeight = GetSystemMetrics(SM_CYSCREEN);

    int iScreenCenterX = iScreenWidth / 2;
    int iScreenCenterY = iScreenHeight / 2;

    int iWindowCenterX = WIN_WIDTH / 2;
    int iWindowsCenterY = WIN_HEIGHT / 2;

    int iWindowX = iScreenCenterX - iWindowCenterX;
    int iWindowY = iScreenCenterY - iWindowsCenterY;

    wndclass_ak.cbSize = sizeof(WNDCLASSEX);
    wndclass_ak.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wndclass_ak.cbClsExtra = 0;
    wndclass_ak.cbWndExtra = 0;
    wndclass_ak.hInstance = hInstance;
    wndclass_ak.lpfnWndProc = WndProc;
    wndclass_ak.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));
    wndclass_ak.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass_ak.lpszClassName = szAppName_ak;
    wndclass_ak.lpszMenuName = NULL;
    wndclass_ak.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass_ak.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));

    RegisterClassEx(&wndclass_ak);

    hwnd_ak = CreateWindowEx(
        WS_EX_APPWINDOW, szAppName_ak, TEXT("D3D11 Blue screen"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, iWindowX, iWindowY, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);
    ghwnd_ak = hwnd_ak;

    ShowWindow(hwnd_ak, iCmdShow);
    SetForegroundWindow(hwnd_ak);
    SetFocus(hwnd_ak);

    HRESULT hr_ak = Initialize();
    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "Initialize() failed.\n");
        fclose(gpFile_ak);
        hwnd_ak = NULL;
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "Initialize() succeeded.\n");
        fclose(gpFile_ak);
    }

    while (bDone_ak == false) {
        if (PeekMessage(&msg_ak, NULL, 0, 0, PM_REMOVE)) {
            if (msg_ak.message == WM_QUIT) {
                bDone_ak = true;
            } else {
                TranslateMessage(&msg_ak);
                DispatchMessage(&msg_ak);
            }
        } else {
            if (gbActiveWindow_ak == true) {
                Display();
            }
        }
    }
    Uninitialize();

    return ((int)msg_ak.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
    void ToggleFullScreen(void);
    HRESULT Resize(int, int);
    void Uninitialize(void);

    HRESULT hr_ak;
    switch (iMsg) {
        case WM_SETFOCUS: {
            gbActiveWindow_ak = true;
            break;
        }
        case WM_KILLFOCUS: {
            gbActiveWindow_ak = false;
            break;
        }
        case WM_ERASEBKGND: {
            return (0);
        }
        case WM_SIZE: {
            if (gpID3D11DeviceContext_ak) {
                hr_ak = Resize(LOWORD(lParam), HIWORD(lParam));
                if (FAILED(hr_ak)) {
                    fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
                    fprintf_s(gpFile_ak, "Resize() Failed.\n");
                    fclose(gpFile_ak);
                    return (hr_ak);
                } else {
                    fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
                    fprintf_s(gpFile_ak, "Resize() Succeeded.\n");
                    fclose(gpFile_ak);
                }
            }
            break;
        }
        case WM_KEYDOWN: {
            switch (wParam) {
                case VK_ESCAPE: {
                    DestroyWindow(hwnd);
                    break;
                }
                case 0x46:
                case 0x66: {
                    ToggleFullScreen();
                    break;
                }
                default: {
                    break;
                }
            }
            break;
        }
        case WM_CLOSE: {
            DestroyWindow(hwnd);
            break;
        }
        case WM_DESTROY: {
            Uninitialize();
            PostQuitMessage(0);
            break;
        }
        default: {
            break;
        }
    }

    return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void) {
    MONITORINFO mi_ak = {sizeof(MONITORINFO)};

    if (gbFullScreen_ak == false) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fclose(gpFile_ak);

        dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);
        if ((dwStyle_ak & WS_OVERLAPPEDWINDOW)) {
            if ((GetWindowPlacement(ghwnd_ak, &wpPrev_ak) &&
                 (GetMonitorInfo(MonitorFromWindow(ghwnd_ak, MONITORINFOF_PRIMARY), &mi_ak)))) {
                SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak & (~WS_OVERLAPPEDWINDOW)));
                SetWindowPos(ghwnd_ak,
                             HWND_TOP,
                             mi_ak.rcMonitor.left,
                             mi_ak.rcMonitor.top,
                             (mi_ak.rcMonitor.right - mi_ak.rcMonitor.left),
                             (mi_ak.rcMonitor.bottom - mi_ak.rcMonitor.top),
                             SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }
        ShowCursor(FALSE);
        gbFullScreen_ak = true;
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fclose(gpFile_ak);
        SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak | WS_OVERLAPPEDWINDOW));
        SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
        SetWindowPos(ghwnd_ak,
                     HWND_TOP,
                     0,
                     0,
                     0,
                     0,
                     (SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOOWNERZORDER));
        ShowCursor(TRUE);
        gbFullScreen_ak = false;
    }
}

HRESULT Initialize(void) {
    void Uninitialize(void);
    HRESULT Resize(int, int);

    HRESULT hr_ak;

    D3D_DRIVER_TYPE d3dDriverType_ak;
    D3D_DRIVER_TYPE d3dDriverTypes_ak[] = {
        D3D_DRIVER_TYPE_HARDWARE,
        D3D_DRIVER_TYPE_WARP,
        D3D_DRIVER_TYPE_REFERENCE};

    D3D_FEATURE_LEVEL dd3dFeatureLevel_required_ak = D3D_FEATURE_LEVEL_11_0;
    D3D_FEATURE_LEVEL d3dFeatureLevel_acquired_ak = D3D_FEATURE_LEVEL_10_0;

    UINT createDeviceFlags_ak = 0;
    UINT cnumDriverTypes_ak = 0;
    UINT numFeatureLevels_ak = 1;

    cnumDriverTypes_ak = sizeof(d3dDriverTypes_ak) / sizeof(d3dDriverTypes_ak[0]);

    DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc_ak;
    ZeroMemory((void*)&dxgiSwapChainDesc_ak, sizeof(DXGI_SWAP_CHAIN_DESC));
    dxgiSwapChainDesc_ak.BufferCount = 1;
    dxgiSwapChainDesc_ak.BufferDesc.Width = WIN_WIDTH;
    dxgiSwapChainDesc_ak.BufferDesc.Height = WIN_HEIGHT;
    dxgiSwapChainDesc_ak.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    dxgiSwapChainDesc_ak.BufferDesc.RefreshRate.Numerator = 60;
    dxgiSwapChainDesc_ak.BufferDesc.RefreshRate.Denominator = 1;
    dxgiSwapChainDesc_ak.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    dxgiSwapChainDesc_ak.OutputWindow = ghwnd_ak;
    dxgiSwapChainDesc_ak.SampleDesc.Count = 1;
    dxgiSwapChainDesc_ak.SampleDesc.Quality = 0;
    dxgiSwapChainDesc_ak.Windowed = TRUE;

    for (UINT driverTypeIndex_ak = 0; driverTypeIndex_ak < cnumDriverTypes_ak; driverTypeIndex_ak++) {
        d3dDriverType_ak = d3dDriverTypes_ak[driverTypeIndex_ak];
        hr_ak = D3D11CreateDeviceAndSwapChain(
            NULL,
            d3dDriverType_ak,
            NULL,
            createDeviceFlags_ak,
            &dd3dFeatureLevel_required_ak,
            numFeatureLevels_ak,
            D3D11_SDK_VERSION,
            &dxgiSwapChainDesc_ak,
            &gpIDXGISwapChain_ak,
            &gpID3D11Device_ak,
            &d3dFeatureLevel_acquired_ak,
            &gpID3D11DeviceContext_ak);
        if (SUCCEEDED(hr_ak)) {
            break;
        }
    }
    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "D3D11CreateDeviceAndSwapChain() Failed.\n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "D3D11CreateDeviceAndSwapChain() Succeeded.\n");

        fprintf_s(gpFile_ak, "The Chosen Driver is Of ");
        if (d3dDriverType_ak == D3D_DRIVER_TYPE_HARDWARE) {
            fprintf_s(gpFile_ak, "Hardware Type.\n");
        } else if (d3dDriverType_ak == D3D_DRIVER_TYPE_WARP) {
            fprintf_s(gpFile_ak, "WRAP Type.\n");
        } else if (d3dDriverType_ak == D3D_DRIVER_TYPE_REFERENCE) {
            fprintf_s(gpFile_ak, "Reference Type.\n");
        } else {
            fprintf_s(gpFile_ak, "Unknown Type.\n");
        }

        fprintf_s(gpFile_ak, "The Supported Highest Feature Level Is");

        if (d3dFeatureLevel_acquired_ak == D3D_FEATURE_LEVEL_11_0) {
            fprintf_s(gpFile_ak, "11.0\n");
        }
        if (d3dFeatureLevel_acquired_ak == D3D_FEATURE_LEVEL_10_1) {
            fprintf_s(gpFile_ak, "10.1\n");
        }
        if (d3dFeatureLevel_acquired_ak == D3D_FEATURE_LEVEL_10_0) {
            fprintf_s(gpFile_ak, "10.0\n");
        } else {
            fprintf_s(gpFile_ak, "Unknown\n");
        }

        fclose(gpFile_ak);
    }

    gClearColor_ak[0] = 0.0f;
    gClearColor_ak[1] = 0.0f;
    gClearColor_ak[2] = 1.0f;
    gClearColor_ak[3] = 1.0f;

    hr_ak = Resize(WIN_WIDTH, WIN_HEIGHT);
    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "Resize() Failed.\n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "Resize() Succeeded.\n");
        fclose(gpFile_ak);
    }
    return (S_OK);
}

HRESULT Resize(int width, int height) {
    HRESULT hr_ak = S_OK;
    if (gpID3D11RenderTargetView_ak) {
        gpID3D11RenderTargetView_ak->Release();
        gpID3D11RenderTargetView_ak = NULL;
    }

    gpIDXGISwapChain_ak->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

    ID3D11Texture2D* pID3D11Texture2D_BackBuffer;
    gpIDXGISwapChain_ak->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_BackBuffer);

    hr_ak = gpID3D11Device_ak->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView_ak);
    if (FAILED(hr_ak)) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "CreateRenderTargetView() Failed.\n");
        fclose(gpFile_ak);
        return (hr_ak);
    } else {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf_s(gpFile_ak, "CreateRenderTargetView() Succeeded.\n");
        fclose(gpFile_ak);
    }

    pID3D11Texture2D_BackBuffer->Release();
    pID3D11Texture2D_BackBuffer = NULL;

    gpID3D11DeviceContext_ak->OMSetRenderTargets(1, &gpID3D11RenderTargetView_ak, NULL);

    D3D11_VIEWPORT d3dViewPort;
    d3dViewPort.TopLeftX = 0;
    d3dViewPort.TopLeftY = 0;
    d3dViewPort.Width = (float)width;
    d3dViewPort.Height = (float)height;
    d3dViewPort.MinDepth = 0.0f;
    d3dViewPort.MaxDepth = 1.0f;
    gpID3D11DeviceContext_ak->RSSetViewports(1, &d3dViewPort);

    return hr_ak;
}

void Display(void) {
    gpID3D11DeviceContext_ak->ClearRenderTargetView(gpID3D11RenderTargetView_ak, gClearColor_ak);

    gpIDXGISwapChain_ak->Present(0, 0);
}

void Uninitialize(void) {
    if (gbFullScreen_ak == true) {
        dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);

        SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak | WS_OVERLAPPEDWINDOW));
        SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
        SetWindowPos(ghwnd_ak,
                     HWND_TOP,
                     0,
                     0,
                     0,
                     0,
                     SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
        ShowCursor(TRUE);
    }

    if (gpID3D11RenderTargetView_ak) {
        gpID3D11RenderTargetView_ak->Release();
        gpID3D11RenderTargetView_ak = NULL;
    }
    if (gpIDXGISwapChain_ak) {
        gpIDXGISwapChain_ak->Release();
        gpIDXGISwapChain_ak = NULL;
    }
    if (gpID3D11DeviceContext_ak) {
        gpID3D11DeviceContext_ak->Release();
        gpID3D11DeviceContext_ak = NULL;
    }
    if (gpID3D11Device_ak) {
        gpID3D11Device_ak->Release();
        gpID3D11Device_ak = NULL;
    }

    if (gpFile_ak) {
        fopen_s(&gpFile_ak, gszLogFileName_ak, "a+");
        fprintf(gpFile_ak, "Program terminated successfully\n");
        fclose(gpFile_ak);
        gpFile_ak = NULL;
    }
}