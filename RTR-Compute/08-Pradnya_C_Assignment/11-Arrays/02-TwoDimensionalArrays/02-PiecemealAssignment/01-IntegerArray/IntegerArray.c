#include <stdio.h>
int main(void)
{
  printf("\nAmeya Kale\n");
  int ak_iArray[3][5];
  int ak_int_size;
  int ak_iArray_size;
  int ak_iArray_num_elements, ak_iArray_num_rows, ak_iArray_num_columns;
  int ak_i, ak_j;

  ak_int_size = sizeof(int);

  ak_iArray_size = sizeof(ak_iArray);
  printf("\nSize Of 2D Integer Array is:- %d\n", ak_iArray_size);

  ak_iArray_num_rows = ak_iArray_size / sizeof(ak_iArray[0]);
  printf("Number of Rows in 2D Integer Array are :- %d\n", ak_iArray_num_rows);

  ak_iArray_num_columns = sizeof(ak_iArray[0]) / ak_int_size;
  printf("Number of Columns in 2D Integer Array are:- %d\n", ak_iArray_num_columns);

  ak_iArray_num_elements = ak_iArray_num_rows * ak_iArray_num_columns;
  printf("Number of Elements in 2D Integer Array are :-%d\n", ak_iArray_num_elements);

  printf("Elements in 2D Array are :-\n");
  ak_iArray[0][0] = 2321;
  ak_iArray[0][1] = 43212;
  ak_iArray[0][2] = 6343;
  ak_iArray[0][3] = 8414;
  ak_iArray[0][4] = 10415;

  ak_iArray[1][0] = 2432;
  ak_iArray[1][1] = 44124;
  ak_iArray[1][2] = 61624;
  ak_iArray[1][3] = 41828;
  ak_iArray[1][4] = 11410;

  ak_iArray[2][0] = 23413;
  ak_iArray[2][1] = 44264;

  ak_iArray[2][2] = 6419;
  ak_iArray[2][3] = 91412;
  ak_iArray[2][4] = 41415;

  for (ak_i = 0; ak_i < ak_iArray_num_rows; ak_i++)
  {
    printf("---- ROW %d ----\n", (ak_i + 1));
    for (ak_j = 0; ak_j < ak_iArray_num_columns; ak_j++)
    {
      printf("ak_iArray[%d][%d] = %d\n", ak_i, ak_j, ak_iArray[ak_i][ak_j]);
    }
    printf("\n");
  }
  return 0;
}
