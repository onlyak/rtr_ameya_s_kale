#include <stdio.h>
#define MAX_STRING_LENGTH 512
int main(void)
{
  printf("\nAmeya Kale\n");
  char ak_strArray[5][10];
  int ak_char_size;
  int ak_strArray_size;
  int ak_strArray_num_elements, ak_strArray_num_rows, ak_strArray_num_columns;
  int ak_i;

  ak_char_size = sizeof(char);
  ak_strArray_size = sizeof(ak_strArray);
  printf("\nSize of 2D Character Array is %d\n", ak_strArray_size);

  ak_strArray_num_rows = ak_strArray_size / sizeof(ak_strArray[0]);
  printf("Number of Rows in 2D Character Array are:- %d\n", ak_strArray_num_rows);

  ak_strArray_num_columns = sizeof(ak_strArray[0]) / ak_char_size;
  printf("Number of Columns in 2D Character Array are:- %d\n", ak_strArray_num_columns);

  ak_strArray_num_elements = ak_strArray_num_rows * ak_strArray_num_columns;
  printf("Maximum Number of Elements in 2D Character Array are :- %d\n", ak_strArray_num_elements);

  ak_strArray[0][0] = 'M';
  ak_strArray[0][1] = 'y';
  ak_strArray[0][2] = '\0';

  ak_strArray[1][0] = 'N';
  ak_strArray[1][1] = 'a';
  ak_strArray[1][2] = 'm';
  ak_strArray[1][3] = 'e';
  ak_strArray[1][4] = '\0';

  ak_strArray[2][0] = 'i';
  ak_strArray[2][1] = 's';
  ak_strArray[2][2] = '\0';

  ak_strArray[3][0] = 'A';
  ak_strArray[3][1] = 'm';
  ak_strArray[3][2] = 'e';
  ak_strArray[3][3] = 'y';
  ak_strArray[3][4] = 'a';
  ak_strArray[3][5] = '\0';

  ak_strArray[4][0] = 'K';
  ak_strArray[4][1] = 'a';
  ak_strArray[4][2] = 'l';
  ak_strArray[4][3] = 'e';
  ak_strArray[4][4] = '\0';

  printf("\nThe Strings in the 2D Character Array are:-\n");
  for (ak_i = 0; ak_i < ak_strArray_num_rows; ak_i++)
  {
    printf("%s ", ak_strArray[ak_i]);
  }
  return 0;
}
