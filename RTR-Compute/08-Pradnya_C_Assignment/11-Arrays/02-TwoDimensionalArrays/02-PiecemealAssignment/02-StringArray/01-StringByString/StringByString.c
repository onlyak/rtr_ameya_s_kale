#include <stdio.h>
#define MAX_STRING_LENGTH 512
int main(void)
{
  printf("\nAmeya Kale\n");
  void ak_MyStrcpy(char[], char[]);
  char ak_strArray[5][10];
  int ak_char_size;
  int ak_strArray_size;
  int ak_strArray_num_elements, ak_strArray_num_rows, ak_strArray_num_columns;
  int ak_i;

  ak_char_size = sizeof(char);
  ak_strArray_size = sizeof(ak_strArray);
  printf("\nSize of 2D Character Array is %d\n", ak_strArray_size);

  ak_strArray_num_rows = ak_strArray_size / sizeof(ak_strArray[0]);
  printf("Number of Rows in 2D Character Array are :-%d\n",ak_strArray_num_rows);

  ak_strArray_num_columns = sizeof(ak_strArray[0]) / ak_char_size;
  printf("Number of Columns in 2D Character Array are :-%d\n",ak_strArray_num_columns);

  ak_strArray_num_elements = ak_strArray_num_rows * ak_strArray_num_columns;
  printf("Maximum Number of Elements in 2D Character Array are :- %d\n",ak_strArray_num_elements);

  ak_MyStrcpy(ak_strArray[0], "My");
  ak_MyStrcpy(ak_strArray[1], "Name");

  ak_MyStrcpy(ak_strArray[2], "Is");
  ak_MyStrcpy(ak_strArray[3], "Ameya");
  ak_MyStrcpy(ak_strArray[4], "Kale");

  printf("The Strings in 2D Character Array are :-\n");

  for (ak_i = 0; ak_i < ak_strArray_num_rows; ak_i++)
   {
    printf("%s ", ak_strArray[ak_i]);
   }
  return 0;
}

void ak_MyStrcpy(char ak_str_destination[], char ak_str_source[])
{

  int ak_MyStrlen(char[]);
  int ak_iStringLength = 0;
  int ak_j;
  ak_iStringLength = ak_MyStrlen(ak_str_source);
  for (ak_j = 0; ak_j < ak_iStringLength; ak_j++)
  {
    ak_str_destination[ak_j] = ak_str_source[ak_j];
  }
  ak_str_destination[ak_j] = '\0';
}

int ak_MyStrlen(char str[])
{
  int ak_j;
  int ak_string_length = 0;

  for (ak_j = 0; ak_j < MAX_STRING_LENGTH; ak_j++)
  {
    if (str[ak_j] == '\0')
      break;
    else
      ak_string_length++;
  }
  return ak_string_length;
}
