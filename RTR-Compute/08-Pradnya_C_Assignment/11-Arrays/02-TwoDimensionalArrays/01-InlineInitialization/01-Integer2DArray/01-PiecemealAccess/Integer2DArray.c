#include <stdio.h>
int main(void)
{
  printf("\nAmeya Kale\n");
  int ak_iArray[5][3] = {{11, 412, 423},
                         {412, 434, 4146},
                         {341, 654, 934},
                         {4432, 842, 12523},
                         {523, 1430, 1435}};
  int ak_int_size;
  int ak_iArray_size;
  int ak_iArray_num_elements, ak_iArray_num_rows, ak_iArray_num_columns;

  ak_int_size = sizeof(int);

  ak_iArray_size = sizeof(ak_iArray);
  printf("\nSize Of 2D Integer Array is:- %d", ak_iArray_size);

  ak_iArray_num_rows = ak_iArray_size / sizeof(ak_iArray[0]);
  printf("\nNumber of Rows in 2D Integer Array are :- %d", ak_iArray_num_rows);

  ak_iArray_num_columns = sizeof(ak_iArray[0]) / ak_int_size;
  printf("\nNumber of Columns in 2D Integer Array are :- %d", ak_iArray_num_columns);

  ak_iArray_num_elements = ak_iArray_num_rows * ak_iArray_num_columns;
  printf("\nNumber of Elements in 2D Integer Array are :- %d", ak_iArray_num_elements);

  printf("\nElements in 2D Array are :-\n");
  printf("ak_iArray[0][0] :- %d\n", ak_iArray[0][0]);
  printf("ak_iArray[0][1] :- %d\n", ak_iArray[0][1]);
  printf("ak_iArray[0][2] :- %d\n", ak_iArray[0][2]);

  printf("ak_iArray[1][0] :- %d\n", ak_iArray[1][0]);
  printf("ak_iArray[1][1] :- %d\n", ak_iArray[1][1]);
  printf("ak_iArray[1][2] :- %d\n", ak_iArray[1][2]);

  printf("ak_iArray[2][0] :- %d\n", ak_iArray[2][0]);
  printf("ak_iArray[2][1] :- %d\n", ak_iArray[2][1]);
  printf("ak_iArray[2][2] :- %d\n", ak_iArray[2][2]);

  printf("ak_iArray[3][0] :- %d\n", ak_iArray[3][0]);
  printf("ak_iArray[3][1] :- %d\n", ak_iArray[3][1]);
  printf("ak_iArray[3][2] :- %d\n", ak_iArray[3][2]);

  printf("ak_iArray[4][0] :- %d\n", ak_iArray[4][0]);
  printf("ak_iArray[4][1] :- %d\n", ak_iArray[4][1]);
  printf("ak_iArray[4][2] :- %d\n", ak_iArray[4][2]);
  return 0;
}
