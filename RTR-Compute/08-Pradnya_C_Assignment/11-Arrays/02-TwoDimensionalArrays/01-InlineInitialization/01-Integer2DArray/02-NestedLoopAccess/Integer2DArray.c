#include <stdio.h>
int main(void)
{
  printf("\nAmeya Kale\n");
  int ak_iArray[5][3] = {{132, 322, 433}, {542, 324, 5436}, {7563, 766, 769}, {434, 832, 5412}, {325, 3210, 13215}}; 
  int ak_int_size;
  int ak_iArray_size;
  int ak_iArray_num_elements, ak_iArray_num_rows, ak_iArray_num_columns;
  int i, j;

  ak_int_size = sizeof(int);

  ak_iArray_size = sizeof(ak_iArray);
  printf("\nSize Of 2D Integer Array is:- %d", ak_iArray_size);

  ak_iArray_num_rows = ak_iArray_size / sizeof(ak_iArray[0]);
  printf("\nNumber of Rows in 2D Integer Array are :- %d", ak_iArray_num_rows);

  ak_iArray_num_columns = sizeof(ak_iArray[0]) / ak_int_size;
  printf("\nNumber of Columns in 2D Integer Array are :- %d", ak_iArray_num_columns);

  ak_iArray_num_elements = ak_iArray_num_rows * ak_iArray_num_columns;
  printf("\nNumber of Elements in 2D Integer Array are :- %d", ak_iArray_num_elements);

  printf("\nElements In The 2D Array :-\n");

  for (i = 0; i < ak_iArray_num_rows; i++)
  {
    printf("-------ROW %d------\n", (i + 1));
    for (j = 0; j < ak_iArray_num_columns; j++)
    {
      printf("ak_iArray[%d][%d] = %d\n", i, j, ak_iArray[i][j]);
    }
    printf("\n");
  }
  return 0;
}
