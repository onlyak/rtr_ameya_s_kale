#include <stdio.h>
#define MAX_STRING_LENGTH 512
int main(void)
{
  printf("\nAmeya Kale\n");
  int ak_MyStrlen(char[]);
  char ak_strArray[10][5] = {"Ameya", "Kale"};
  int ak_char_size;
  int ak_strArray_size;
  int ak_strArray_num_elements, ak_strArray_num_rows, ak_strArray_num_columns;
  int ak_strActual_num_chars = 0;
  int ak_i;
  ak_char_size = sizeof(char);
  ak_strArray_size = sizeof(ak_strArray);
  printf("\nSize Of 2D Character Array is :-%d", ak_strArray_size);
  ak_strArray_num_rows = ak_strArray_size / sizeof(ak_strArray[0]);
  printf("\nNumber of Rows in 2D Character Array are :- %d", ak_strArray_num_rows);

  ak_strArray_num_columns = sizeof(ak_strArray[0]) / ak_char_size;
  printf("\nNumber of Columns in 2D Character Array are :- %d", ak_strArray_num_columns);

  ak_strArray_num_elements = ak_strArray_num_rows * ak_strArray_num_columns;
  printf("\nMaximum Number of Elements in 2D Character Array are :- %d", ak_strArray_num_elements);

  for (ak_i = 0; ak_i < ak_strArray_num_rows; ak_i++)
  {
    ak_strActual_num_chars = ak_strActual_num_chars + ak_MyStrlen(ak_strArray[ak_i]);
  }
  printf("\nActual Number of Elements in 2D Character Array are :-%d", ak_strActual_num_chars);

  printf("\nStrings In The 2D Array :-\n");
  printf("%s ", ak_strArray[0]);
  return 0;
}

int ak_MyStrlen(char str[])
{

  int j;
  int string_length = 0;

  for (j = 0; j < MAX_STRING_LENGTH; j++)
  {
    if (str[j] == '\0')
      break;
    else
      string_length++;
  }
  return string_length;
}
