#include <stdio.h>
#define MAX_STRING_LENGTH 512
int main(void)
{
  printf("\nAmeya Kale\n");
  int ak_MyStrlen(char[]);
  char ak_strArray[2][5] = {"Ameya", "Kale"};
  int ak_iStrLengths[10];
  int ak_strArray_size;
  int ak_strArray_num_rows;
  int ak_i, ak_j;

  ak_strArray_size = sizeof(ak_strArray);
  ak_strArray_num_rows = ak_strArray_size / sizeof(ak_strArray[0]);

  for (ak_i = 0; ak_i < ak_strArray_num_rows; ak_i++)
    ak_iStrLengths[ak_i] = ak_MyStrlen(ak_strArray[ak_i]);

  printf("\nThe String Array:-\n");
  for (ak_i = 0; ak_i < ak_strArray_num_rows; ak_i++)
    printf("%s ", ak_strArray[ak_i]);

  printf("\nStrings in the 2D Array :-\n");
  for (ak_i = 0; ak_i < ak_strArray_num_rows; ak_i++)
  {
    printf("String Number %d :-%s\n\n", (ak_i + 1), ak_strArray[ak_i]);
    for (ak_j = 0; ak_j < ak_iStrLengths[ak_i]; ak_j++)
    {
      printf("Character %d :-%c\n", (ak_j + 1), ak_strArray[ak_i][ak_j]);
    }
    printf("\n");
  }
  return 0;
}

int ak_MyStrlen(char str[])
{

  int j;
  int string_length = 0;

  for (j = 0; j < MAX_STRING_LENGTH; j++)
  {
    if (str[j] == '\0')
      break;
    else
      string_length++;
  }
  return (string_length);
}
