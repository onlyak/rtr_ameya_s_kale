#include <stdio.h>
int main()
{
    printf("\nAmeya Kale\n");
    int ak_iArray_1[5];
    int ak_iArray_2[5][3];
    int ak_iArray_3[5][3][2];

    int ak_numRows2D;
    int ak_numColumns2D;

    int ak_numRows3D;
    int ak_numColumns3D;
    int ak_depth3D;

    printf("\nSize of 1-D Array: %lu", sizeof(ak_iArray_1));
    printf("\nNumber of elements in 1-D Array: %lu", sizeof(ak_iArray_1) / sizeof(int));

    printf("\nSize of 2-D Array: %lu", sizeof(ak_iArray_2));
    printf("\nNumber row in 2-D Array: %lu", sizeof(ak_iArray_2) / sizeof(ak_iArray_2[0]));
    printf("\nNumber column in 2-D Array: %lu", sizeof(ak_iArray_2[0]) / sizeof(ak_iArray_2[0][0]));
    ak_numRows2D = sizeof(ak_iArray_2) / sizeof(ak_iArray_2[0]);
    ak_numColumns2D = sizeof(ak_iArray_2[0]) / sizeof(ak_iArray_2[0][0]);
    printf("\nNumber of elements in 2-D array: %d", ak_numRows2D * ak_numColumns2D);

    printf("\nSize of 3-D Array: %lu", sizeof(ak_iArray_3));
    printf("\nNumber row in 3-D Array: %lu", sizeof(ak_iArray_3) / sizeof(ak_iArray_3[0]));
    printf("\nNumber column in 3-D Array: %lu", sizeof(ak_iArray_3[0]) / sizeof(ak_iArray_3[0][0]));
    printf("\nNumber depth in one row in one column: %lu", sizeof(ak_iArray_3[0][0]) / sizeof(ak_iArray_3[0][0][0]));
    ak_numRows3D = sizeof(ak_iArray_3) / sizeof(ak_iArray_3[0]);
    ak_numColumns3D = sizeof(ak_iArray_3[0]) / sizeof(ak_iArray_3[0][0]);
    ak_depth3D = sizeof(ak_iArray_3[0][0]) / sizeof(ak_iArray_3[0][0][0]);
    printf("\nNumber of elements in 3-D array: %d", ak_numRows2D * ak_numRows3D * ak_depth3D);
    return 0;
}