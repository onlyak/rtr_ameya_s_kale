#include <stdio.h>
int main(void)
{
  printf("\nAmeya Kale\n");
  char ak_chArray_01[] = {'A', 'M', 'E', 'Y', 'A', 'K', 'A', 'L', 'E', '\0'};
  char ak_chArray_02[9] = {'H', 'I', '\0'};
  char ak_chArray_03[] = "Hi";
  char ak_chArray_WithoutNullTerminator[] = {'B', 'y', 'e'};

  printf("\nSize Of ak_chArray_01 :-%lu\n", sizeof(ak_chArray_01));
  printf("Size Of ak_chArray_02 :-%lu\n", sizeof(ak_chArray_02));
  printf("Size Of chArray_04 :-%lu\n", sizeof(ak_chArray_03));

  printf("The Strings Are :-\n");
  printf("ak_chArray_01 :-%s\n", ak_chArray_01);
  printf("ak_chArray_02 :-%s\n", ak_chArray_02);
  printf("ak_chArray_03 :-%s\n", ak_chArray_03);
  printf("Size Of ak_chArray_WithoutNullTerminator :-%lu\n", sizeof(ak_chArray_WithoutNullTerminator));
  printf("ak_chArray_WithoutNullTerminator :-%s\n", ak_chArray_WithoutNullTerminator);
  return 0;
}
