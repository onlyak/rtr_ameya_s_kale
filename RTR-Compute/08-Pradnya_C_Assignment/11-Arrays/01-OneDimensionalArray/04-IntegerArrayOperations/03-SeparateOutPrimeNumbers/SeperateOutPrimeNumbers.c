#include <stdio.h>
#define NUM_ELEMENTS 10
int main(void)
{
  printf("\nAmeya Kale\n");
  int ak_iArray[NUM_ELEMENTS];
  int ak_i, ak_num, ak_j, ak_count = 0;
  printf("\nEnter Integer Elememts:- \n");
  for (ak_i = 0; ak_i < NUM_ELEMENTS; ak_i++)
  {
    scanf("%d", &ak_num);
    if (ak_num < 0)
      ak_num = -1 * ak_num;
    ak_iArray[ak_i] = ak_num;
  }
  printf("\nArray Elements Are:- \n");
  for (ak_i = 0; ak_i < NUM_ELEMENTS; ak_i++)
    printf("%d\n", ak_iArray[ak_i]);

  printf("\nPrime Numbers amongst the array are :-\n");
  for (ak_i = 0; ak_i < NUM_ELEMENTS; ak_i++)
  {
    for (ak_j = 1; ak_j <= ak_iArray[ak_i]; ak_j++)
    {
      if ((ak_iArray[ak_i] % ak_j) == 0)
        ak_count++;
    }

    if (ak_count == 2)
      printf("%d\n", ak_iArray[ak_i]);

    ak_count = 0;
  }
  return 0;
}
