#include <stdio.h>
#define NUM_ELEMENTS 10
int main(void)
{
  printf("\nAmeya Kale\n");
  int ak_iArray[NUM_ELEMENTS];
  int ak_i, ak_num, ak_sum = 0;
  printf("\nEnter Integer Elememts :-\n");
  for (ak_i = 0; ak_i < NUM_ELEMENTS; ak_i++)
  {
    scanf("%d", &ak_num);
    ak_iArray[ak_i] = ak_num;
  }

  for (ak_i = 0; ak_i < NUM_ELEMENTS; ak_i++)
  {
    ak_sum = ak_sum + ak_iArray[ak_i];
  }
  printf("\nSum Of Elements Of Array = %d\n", ak_sum);
  return 0;
}
