#include<stdio.h>
#define MAX_LENGTH 512

void ak_strcat(char ak_destArray[],char ak_orgArray[])
{
    int ak_i,ak_j;
    for(ak_i=strlen(ak_destArray),ak_j=0;ak_j<strlen(ak_orgArray);ak_j++,ak_i++)
    {
        ak_destArray[ak_i]=ak_orgArray[ak_j];
    }
    ak_destArray[ak_i]='\0';
}

int main()
{
    printf("\nAmeya Kale\n");
    char ak_chArray[MAX_LENGTH],ak_catArray[MAX_LENGTH];
    printf("Enter String:-\t");
    gets_s(ak_chArray,MAX_LENGTH);
    printf("Enter String:-\t");
    gets_s(ak_catArray,MAX_LENGTH);
    printf("String entered by me:-\t");    
    printf("%s",ak_catArray);
    printf("\nString entered by me:-\t");
    printf("%s",ak_chArray);
    ak_strcat(ak_catArray,ak_chArray);
    printf("\nConcatenated String :-%s",ak_catArray);
    return 0;
}
