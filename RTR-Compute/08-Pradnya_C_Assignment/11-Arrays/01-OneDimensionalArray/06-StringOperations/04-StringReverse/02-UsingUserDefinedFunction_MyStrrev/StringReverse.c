#include<stdio.h>
#define MAX_LENGTH 512

void ak_strrev(char ak_revstr[],char ak_str[])
{
    int ak_i=0;
    int ak_j;
    int ak_len=strlen(ak_str)-1;

    for(ak_i=0,ak_j=ak_len;ak_i<strlen(ak_str),ak_j>=0;ak_i++,ak_j--)
    {
        ak_revstr[ak_i]=ak_str[ak_j];
    }
    ak_revstr[ak_i]='\0';
}

int main()
{
    printf("\nAmeya Kale\n");
    char ak_chArray[MAX_LENGTH],ak_revArray[MAX_LENGTH];
    printf("Enter String:-\t");
    gets_s(ak_chArray,MAX_LENGTH);
    printf("String entered by me:-\t");
    printf("%s",ak_chArray);
    ak_strrev(ak_revArray,ak_chArray);
    printf("\nReverse String :-%s",ak_revArray);
    return 0;
}
