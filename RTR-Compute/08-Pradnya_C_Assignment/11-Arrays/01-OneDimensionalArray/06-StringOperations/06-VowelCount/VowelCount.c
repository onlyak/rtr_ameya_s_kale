#include<stdio.h>
#define MAX_LENGTH 512

int main()
{
    printf("\nAmeya Kale\n");
    char ak_chArray[MAX_LENGTH];
    int ak_strlen;
    int ak_countA=0,ak_countE=0,ak_countI=0,ak_countO=0,ak_countU=0;
    printf("\nEnter String:-");
    gets_s(ak_chArray,MAX_LENGTH);
    printf("\nString Enterred by me:-\t%s",ak_chArray);
    ak_strlen=strlen(ak_chArray);

    for(int ak_i=0;ak_i<ak_strlen;ak_i++)
    {
        switch(ak_chArray[ak_i])
        {
            case 'A':
            case 'a':
                ak_countA++;
                break;
            
            case 'E':
            case 'e':
                ak_countE++;
                break;

            case 'I':
            case 'i':
                ak_countI++;
                break;
            
            case 'O':
            case 'o':
                ak_countO++;
                break;
            
            case 'U':
            case 'u':
                ak_countU++;
                break;
            
            default:
                break;
        }
    }

    printf("\nVowels in the string entered by me are:-");
    printf("\nOccurence of A:%d",ak_countA);
    printf("\nOccurence of E:%d",ak_countE);
    printf("\nOccurence of I:%d",ak_countI);
    printf("\nOccurence of O:%d",ak_countO);
    printf("\nOccurence of U:%d",ak_countU);
    return 0;
}
