#include<stdio.h>
#define MAX_LENGTH 512

int main()
{
    printf("\nAmeya Kale\n");
    char ak_chArray[MAX_LENGTH],ak_vowelsReplaced[MAX_LENGTH];
    int ak_strlen;
    printf("\nEnter String:-");
    gets_s(ak_chArray,MAX_LENGTH);
    printf("String Enterred by me:-\t%s",ak_chArray);
    ak_strlen=strlen(ak_chArray);
    strcpy(ak_vowelsReplaced,ak_chArray);
    for(int ak_i=0;ak_i<ak_strlen;ak_i++)
    {
        switch(ak_vowelsReplaced[ak_i])
        {
            case 'A':
            case 'a':            
            case 'E':
            case 'e':
            case 'I':
            case 'i':
            case 'O':
            case 'o':
            case 'U':
            case 'u':
                 ak_vowelsReplaced[ak_i]='*';
                 break;
            default:
                break;
        }
    }
    printf("\nString replaced by * is as follows:-\t%s",ak_vowelsReplaced);
    return 0;
}
