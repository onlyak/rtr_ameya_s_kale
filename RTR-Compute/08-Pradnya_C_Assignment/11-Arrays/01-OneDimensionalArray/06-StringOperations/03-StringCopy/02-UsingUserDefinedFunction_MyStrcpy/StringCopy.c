#include<stdio.h>
#define MAX_LENGTH 512

void akStrcopy(char ak_str_destination[], char ak_str_org[])
{
    int ak_i;

    for(ak_i=0;ak_i<strlen(ak_str_org);ak_i++)
    {
        ak_str_destination[ak_i]=ak_str_org[ak_i];
    }
    ak_str_destination[ak_i]='\0';
}
int main()
{
    printf("\nAmeya Kale\n");
    char ak_chArray[MAX_LENGTH],ak_copyArray[MAX_LENGTH];
    printf("Enter String:-\t");
    gets_s(ak_chArray,MAX_LENGTH);
    printf("String entered by me:-\t");
    printf("%s",ak_chArray);
    akStrcopy(ak_copyArray,ak_chArray);
    printf("\nCopied String :-%s",ak_copyArray);
    return 0;
}