#include <stdio.h>
int main(void) 
{
  printf("\nAmeya Kale\n");
  int ak_iArray[] = {9,18,27,36,45,54,63};
  int ak_int_size;
  int ak_iArray_size;
  int ak_iArray_num_elements;

  float ak_fArray[] = {2.2f, 3.3f, 4.4f, 5.5f, 6.6f, 7.7f};
  int ak_float_size;
  int ak_fArray_size;
  int ak_fArray_num_elements;

  char ak_cArray[] = {'A', 'M', 'E', 'Y', 'A', 'K', 'A','L', 'E'};
  int ak_char_size;
  int ak_cArray_size;
  int ak_cArray_num_elements;

  printf("\nIn-line Initialization And Piece-meal Display Of Elements of Integer Array:-\n");
  printf("1st Element = %d\n", ak_iArray[0]);
  printf("2nd Element = %d\n", ak_iArray[1]);
  printf("3rd Element = %d\n", ak_iArray[2]);
  printf("4th Element = %d\n", ak_iArray[3]);
  printf("5th Element = %d\n", ak_iArray[4]);
  printf("6th Element = %d", ak_iArray[5]);
  
  ak_int_size = sizeof(int);
  ak_iArray_size = sizeof(ak_iArray);
  ak_iArray_num_elements = ak_iArray_size / ak_int_size;
  printf("\nSize Of Data :- %d bytes", ak_int_size);
  printf("\nNumber Of Elements :- %d Elements",ak_iArray_num_elements);
  printf("\nSize Of Array :- %d Bytes\n",ak_iArray_num_elements, ak_int_size, ak_iArray_size);

  printf("\nIn-line Initialization And Piece-meal Display Of Elements of Float Array:-");
  printf("\n1st Element = %f\n", ak_fArray[0]);
  printf("2nd Element = %f\n", ak_fArray[1]);
  printf("3rd Element = %f\n", ak_fArray[2]);
  printf("4th Element = %f\n", ak_fArray[3]);
  printf("5th Element = %f\n", ak_fArray[4]);
  printf("6th Element = %f", ak_fArray[5]);

  ak_float_size = sizeof(float);
  ak_fArray_size = sizeof(ak_fArray);
  ak_fArray_num_elements = ak_fArray_size / ak_float_size;
  printf("\nSize Of Data type:- %d bytes", ak_float_size);
  printf("\nNumber Of Elements :- %d Elements",ak_fArray_num_elements);
  printf("\nSize Of Array :- %d Bytes\n",ak_fArray_num_elements, ak_float_size, ak_fArray_size);

  printf("\nIn-line Initialization And Piece-meal Display Of Elements of Character Array:-\n");
  printf("1st Element= %c\n", ak_cArray[0]);
  printf("2nd Element= %c\n", ak_cArray[1]);
  printf("3rd Element= %c\n", ak_cArray[2]);
  printf("4th Element= %c\n", ak_cArray[3]);
  printf("5th Element= %c\n", ak_cArray[4]);
  printf("6th Element= %c\n", ak_cArray[5]);
  printf("7th Element= %c\n", ak_cArray[6]);
  printf("8th Element= %c\n", ak_cArray[7]);
  printf("9th Element= %c", ak_cArray[8]);

  ak_char_size = sizeof(char);
  ak_cArray_size = sizeof(ak_cArray);
  ak_cArray_num_elements = ak_cArray_size / ak_char_size;
  printf("\nSize Of Data :-%d bytes", ak_char_size);
  printf("\nNumber Of Elements :-%d Elements",ak_cArray_num_elements);
  printf("\nSize Of Array:-%d Bytes",ak_cArray_num_elements, ak_char_size, ak_cArray_size);
  return 0;
}
