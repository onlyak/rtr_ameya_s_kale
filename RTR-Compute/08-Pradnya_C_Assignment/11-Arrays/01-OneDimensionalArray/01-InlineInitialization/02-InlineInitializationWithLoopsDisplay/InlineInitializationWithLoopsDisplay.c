#include <stdio.h>
int main(void)
{
  printf("\nAmeya Kale\n");
  int ak_iArray[] = {9,18,27,36,45,54,63};
  int ak_int_size;
  int ak_iArray_size;
  int ak_iArray_num_elements;

  float ak_fArray[] = {2.2f, 3.3f, 4.4f, 5.5f, 6.6f, 7.7f};
  int ak_float_size;
  int ak_fArray_size;
  int ak_fArray_num_elements;

  char ak_cArray[] = {'A', 'M', 'E', 'Y', 'A', 'K', 'A','L', 'E'};
  int ak_char_size;
  int ak_cArray_size;
  int ak_cArray_num_elements;

  int ak_i;
  printf("\nIn-line Initialization And Loop (for) Display Of Elements of Integer Array :-\n");
  ak_int_size = sizeof(int);
  ak_iArray_size = sizeof(ak_iArray);
  ak_iArray_num_elements = ak_iArray_size / ak_int_size;

  for (ak_i = 0; ak_i < ak_iArray_num_elements; ak_i++)
  {
    printf("Element %d :- %d\n", (ak_i + 1), ak_iArray[ak_i]);
  }
  printf("Size Of Data :- %d bytes", ak_int_size);
  printf("\nNumber Of Elements :- %d Elements", ak_iArray_num_elements);
  printf("\nSize Of Array :- %d Bytes\n", ak_iArray_num_elements, ak_int_size, ak_iArray_size);

  printf("\nIn-line Initialization And Loop (while) Display Of Elements of Float Array:-\n");

  ak_float_size = sizeof(float);
  ak_fArray_size = sizeof(ak_fArray);
  ak_fArray_num_elements = ak_fArray_size / ak_float_size;

  ak_i = 0;
  while (ak_i < ak_fArray_num_elements)
  {
    printf("Element %d :-%f\n", (ak_i + 1), ak_fArray[ak_i]);
    ak_i++;
  }

  printf("Size Of Data type:- %d bytes", ak_float_size);
  printf("\nNumber Of Elements :- %d Elements", ak_fArray_num_elements);
  printf("\nSize Of Array :- %d Bytes\n", ak_fArray_num_elements, ak_float_size, ak_fArray_size);

  printf("\nIn-line Initialization And Loop (do while) Display Of Elements of Character Array:-\n");
  ak_char_size = sizeof(char);
  ak_cArray_size = sizeof(ak_cArray);
  ak_cArray_num_elements = ak_cArray_size / ak_char_size;

  ak_i = 0;
  do
  {
    printf("Element %d :- %c\n", (ak_i + 1), ak_cArray[ak_i]);
    ak_i++;
  } while (ak_i < ak_cArray_num_elements);
  printf("Size Of Data :-%d bytes", ak_char_size);
  printf("\nNumber Of Elements :-%d Elements", ak_cArray_num_elements);
  printf("\nSize Of Array:-%d Bytes", ak_cArray_num_elements, ak_char_size, ak_cArray_size);
  return 0;
}
