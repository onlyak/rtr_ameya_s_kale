#include <stdio.h>
#define INT_ARRAY_NUM_ELEMENTS 5
#define FLOAT_ARRAY_NUM_ELEMENTS 3
#define CHAR_ARRAY_NUM_ELEMENTS 4

int main(void) 
{
  printf("\nAmeya Kale\n");
  int ak_iArray[INT_ARRAY_NUM_ELEMENTS];
  float ak_fArray[FLOAT_ARRAY_NUM_ELEMENTS];
  char ak_cArray[CHAR_ARRAY_NUM_ELEMENTS];
  int ak_i, ak_num;
  printf("\nEnter Elements (Int):- \n");
  for (ak_i = 0; ak_i < INT_ARRAY_NUM_ELEMENTS; ak_i++)
      scanf("%d", &ak_iArray[ak_i]);

  printf("\n Enter Elements (Float):- \n");
  for (ak_i = 0; ak_i < FLOAT_ARRAY_NUM_ELEMENTS; ak_i++)
    scanf("%f", &ak_fArray[ak_i]);

  printf("\nEnter Elements (Char):- \n");
  for (ak_i = 0; ak_i < CHAR_ARRAY_NUM_ELEMENTS; ak_i++) {
    ak_cArray[ak_i] = getch();
    printf("%c\n", ak_cArray[ak_i]);
  }
  
  printf("\nInteger Array:-\n");
  for (ak_i = 0; ak_i < INT_ARRAY_NUM_ELEMENTS; ak_i++)
    printf("%d\t", ak_iArray[ak_i]);

  printf("\nFloating-Point Array:- \n");
  for (ak_i = 0; ak_i < FLOAT_ARRAY_NUM_ELEMENTS; ak_i++)
    printf("%f\t", ak_fArray[ak_i]);

  printf("\nCharacter Array:- \n");
  for (ak_i = 0; ak_i < CHAR_ARRAY_NUM_ELEMENTS; ak_i++)
    printf("%c\n", ak_cArray[ak_i]);
  return 0;
}
