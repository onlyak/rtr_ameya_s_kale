#include <stdio.h>
int main(void)
{
  printf("\nAmeya Kale\n");
  int ak_iArrayOne[10];
  int ak_iArrayTwo[10];

  ak_iArrayOne[0] = 9;
  ak_iArrayOne[1] = 18;
  ak_iArrayOne[2] = 27;
  ak_iArrayOne[3] = 36;
  ak_iArrayOne[4] = 45;
  ak_iArrayOne[5] = 54;
  ak_iArrayOne[6] = 63;
  ak_iArrayOne[7] = 72;
  ak_iArrayOne[8] = 81;
  ak_iArrayOne[9] = 90;

  printf("\nPiece-meal Display Of Elements to Integer Array :-\n");
  printf("1st Element Of Array  Element At 0th Index :-%d\n", ak_iArrayOne[0]);
  printf("2nd Element Of Array  Element At 1st Index :-%d\n", ak_iArrayOne[1]);
  printf("3rd Element Of Array  Element At 2nd Index :-%d\n", ak_iArrayOne[2]);
  printf("4th Element Of Array  Element At 3rd Index :-%d\n", ak_iArrayOne[3]);
  printf("5th Element Of Array  Element At 4th Index :-%d\n", ak_iArrayOne[4]);
  printf("6th Element Of Array  Element At 5th Index :-%d\n", ak_iArrayOne[5]);
  printf("7th Element Of Array  Element At 6th Index :-%d\n", ak_iArrayOne[6]);
  printf("8th Element Of Array  Element At 7th Index :-%d\n", ak_iArrayOne[7]);
  printf("9th Element Of Array  Element At 8th Index :-%d\n", ak_iArrayOne[8]);
  printf("10th Element Of Array  Element At 9th Index :-%d\n", ak_iArrayOne[9]);

  printf("\nEnter 1st Element Of Array :- ");
  scanf("%d", &ak_iArrayTwo[0]);
  printf("Enter 2nd Element Of Array :- ");
  scanf("%d", &ak_iArrayTwo[1]);
  printf("Enter 3rd Element Of Array :- ");
  scanf("%d", &ak_iArrayTwo[2]);
  printf("Enter 4th Element Of Array :- ");
  scanf("%d", &ak_iArrayTwo[3]);
  printf("Enter 5th Element Of Array :- ");
  scanf("%d", &ak_iArrayTwo[4]);
  printf("Enter 6th Element Of Array :- ");
  scanf("%d", &ak_iArrayTwo[5]);
  printf("Enter 7th Element Of Array :- ");
  scanf("%d", &ak_iArrayTwo[6]);
  printf("Enter 8th Element Of Array :- ");
  scanf("%d", &ak_iArrayTwo[7]);
  printf("Enter 9th Element Of Array :- ");
  scanf("%d", &ak_iArrayTwo[8]);
  printf("Enter 10th Element Of Array :- ");
  scanf("%d", &ak_iArrayTwo[9]);

  printf("\nPiece-meal Assignment And Display Of Elements to Array :- \n");
  printf("1st Element Of Array  Element At 0th Index :-%d\n", ak_iArrayTwo[0]);
  printf("2nd Element Of Array  Element At 1st Index :-%d\n", ak_iArrayTwo[1]);
  printf("3rd Element Of Array  Element At 2nd Index :-%d\n", ak_iArrayTwo[2]);
  printf("4th Element Of Array  Element At 3rd Index :-%d\n", ak_iArrayTwo[3]);
  printf("5th Element Of Array  Element At 4th Index :-%d\n", ak_iArrayTwo[4]);
  printf("6th Element Of Array  Element At 5th Index :-%d\n", ak_iArrayTwo[5]);
  printf("7th Element Of Array  Element At 6th Index :-%d\n", ak_iArrayTwo[6]);
  printf("8th Element Of Array  Element At 7th Index :-%d\n", ak_iArrayTwo[7]);
  printf("9th Element Of Array  Element At 8th Index :-%d\n", ak_iArrayTwo[8]);
  printf("10th Element Of Array  Element At 9th Index :-%d\n\n", ak_iArrayTwo[9]);
  return 0;
}
