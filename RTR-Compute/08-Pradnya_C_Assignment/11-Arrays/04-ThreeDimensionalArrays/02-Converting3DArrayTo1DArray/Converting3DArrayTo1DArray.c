#include <stdio.h>

#define NUM_ROWS 3
#define NUM_COLUMNS 2
#define DEPTH 2

int main()
{
	printf("\nAmeya Kale\n");
    int ak_iArray_3D[NUM_ROWS][NUM_COLUMNS][DEPTH];
    int ak_iArray_1D[NUM_ROWS * NUM_COLUMNS * DEPTH];
    int ak_row_index = 0;
    int ak_column_index = 0;
    int ak_depth_index = 0;
    printf("\nEnter elements of 3d array: ");
    for(ak_row_index = 0; ak_row_index < NUM_ROWS; ak_row_index++)
    {
        for(ak_column_index = 0; ak_column_index < NUM_COLUMNS; ak_column_index++)
        {
            for(ak_depth_index = 0; ak_depth_index < NUM_COLUMNS; ak_depth_index++)
            {
                printf("\nEnter value for iArray_3D[%d][%d][%d]: ", ak_row_index, ak_column_index, ak_depth_index);
                scanf("%d", &ak_iArray_3D[ak_row_index][ak_column_index][ak_depth_index]);
            }
        }
    }
    printf("\nElements of 3d array are: ");
    for(ak_row_index = 0; ak_row_index < NUM_ROWS; ak_row_index++)
    {
        printf("\nRow %d", ak_row_index);
        for(ak_column_index = 0; ak_column_index < NUM_COLUMNS; ak_column_index++)
        {
            printf("\nColumn %d", ak_column_index);
            for(ak_depth_index = 0; ak_depth_index < NUM_COLUMNS; ak_depth_index++)
            {
                printf("\niArray_3D[%d][%d][%d]: %d", ak_row_index, ak_column_index, ak_depth_index, ak_iArray_3D[ak_row_index][ak_column_index][ak_depth_index]);
            }
        }
    }

    for(ak_row_index = 0; ak_row_index < NUM_ROWS; ak_row_index++)
    {
        for(ak_column_index = 0; ak_column_index < NUM_COLUMNS; ak_column_index++)
        {
            for(ak_depth_index = 0; ak_depth_index < NUM_COLUMNS; ak_depth_index++)
            {
                ak_iArray_1D[(ak_row_index * NUM_COLUMNS * DEPTH) + (ak_column_index * DEPTH) + ak_depth_index] = ak_iArray_3D[ak_row_index][ak_column_index][ak_depth_index];
            }
        }
    }
    printf("\nElements of 1d array are: ");
    for(ak_row_index = 0; ak_row_index < NUM_ROWS * NUM_COLUMNS * DEPTH; ak_row_index++)
    {
        printf("\niArray_1D[%d]: %d\n", ak_row_index, ak_iArray_1D[ak_row_index]);
    }
    return 0;
}