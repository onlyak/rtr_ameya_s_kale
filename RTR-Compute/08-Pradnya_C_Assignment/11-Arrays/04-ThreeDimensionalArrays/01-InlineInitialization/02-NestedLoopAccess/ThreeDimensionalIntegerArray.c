#include <stdio.h>
int main()
{
    int ak_iArray[3][2][2] = {
        {{9, 18}, {27,36 }},
        {{45, 54}, {63, 72}},
        {{81, 90}, {99, 108}}};
	printf("\nAmeya Kale\n");
    int ak_int_size = 0;
    int ak_iArray_size = 0;
    int ak_iArray_num_elements = 0, iArray_width = 0, iArray_height = 0, iArray_depth = 0;
    int ak_index_row = 0;
    int ak_index_column = 0;
    int ak_index_depth = 0;
    ak_int_size = sizeof(int);
    ak_iArray_size = sizeof(ak_iArray);

    iArray_width = ak_iArray_size / sizeof(ak_iArray[0]);
    iArray_height = sizeof(ak_iArray[0]) / sizeof(ak_iArray[0][0]);
    iArray_depth = sizeof(ak_iArray[0][0]) / sizeof(ak_iArray[0][0][0]);
    ak_iArray_num_elements = iArray_width * iArray_height * iArray_depth;

    printf("\nSize of 3D int array = %d", ak_iArray_size);
    printf("\nWidth of 3d int array (number of rows) = %d", iArray_width);
    printf("\nHeight of 3d int array (number of columns) = %d", iArray_height);
    printf("\nDepth of 3d int array = %d", iArray_depth);
    printf("\nNumber of elements 3D int array = %d ", ak_iArray_num_elements);
    printf("Elements on 3d int array are : \n");

    for(ak_index_row = 0; ak_index_row < iArray_width; ak_index_row++)
    {
        printf("\nRow %d", ak_index_row);
        for(ak_index_column = 0; ak_index_column < iArray_height; ak_index_column++)
        {
            printf("\nColumn %d", ak_index_column);
            for(ak_index_depth = 0; ak_index_depth < iArray_depth; ak_index_depth++)
            {
                printf("\niArray[%d][%d][%d] = %d", ak_index_row, ak_index_column, ak_index_depth, ak_iArray[ak_index_row][ak_index_column][ak_index_depth]);

            }
        }
    }
    return 0;
}