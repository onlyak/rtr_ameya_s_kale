#include <stdio.h>
int main()
{
	printf("\nAmeya Kale\n");
    int ak_iArray[3][2][2] = {
        {{9,18}, {27, 36}},
        {{45, 54}, {63, 72}},
        {{81, 90}, {99, 108}}};
    
    int ak_int_size = 0;
    int ak_iArray_size = 0;
    int ak_iArray_num_elements = 0; 
    int ak_iArray_width = 0;
    int ak_iArray_height = 0;
    int ak_iArray_depth = 0;

    ak_int_size = sizeof(int);
    ak_iArray_size = sizeof(ak_iArray);
    ak_iArray_width = ak_iArray_size / sizeof(ak_iArray[0]);
    ak_iArray_height = sizeof(ak_iArray[0]) / sizeof(ak_iArray[0][0]);
    ak_iArray_depth = sizeof(ak_iArray[0][0]) / sizeof(ak_iArray[0][0][0]);
    ak_iArray_num_elements = ak_iArray_width * ak_iArray_height * ak_iArray_depth;

    printf("\nSize of 3D int array = %d", ak_iArray_size);
    printf("\nWidth of 3d int array (number of rows) = %d", ak_iArray_width);
    printf("\nHeight of 3d int array (number of columns) = %d", ak_iArray_height);
    printf("\nDepth of 3d int array = %d", ak_iArray_depth);
    printf("\nNumber of elements 3D int array = %d ", ak_iArray_num_elements);
    printf("\nElements on 3d int array are : \n");
    printf("\nROW 1");
    printf("\nCOLUMN 1");
    printf("\n\tiArray[0][0][0] = %d", ak_iArray[0][0][0]);
    printf("\n\tiArray[0][0][1] = %d", ak_iArray[0][0][1]);
    printf("\nCOLUMN 2");
    printf("\n\tiArray[0][1][0] = %d", ak_iArray[0][1][0]);
    printf("\n\tiArray[0][1][1] = %d", ak_iArray[0][1][1]);
    printf("\nROW 2");
    printf("\nCOLUMN 1");
    printf("\n\tiArray[1][0][0] = %d", ak_iArray[1][0][0]);
    printf("\n\tiArray[1][0][1] = %d", ak_iArray[1][0][1]);
    printf("\nCOLUMN 2");
    printf("\n\tiArray[1][1][0] = %d", ak_iArray[1][1][0]);
    printf("\n\tiArray[1][1][1] = %d", ak_iArray[1][1][1]);
    printf("\nROW 3");
    printf("\nCOLUMN 1");
    printf("\n\tiArray[2][0][0] = %d", ak_iArray[2][0][0]);
    printf("\n\tiArray[2][0][1] = %d", ak_iArray[2][0][1]);
    printf("\nCOLUMN 2");
    printf("\n\tiArray[2][1][0] = %d", ak_iArray[2][1][0]);
    printf("\n\tiArray[2][1][1] = %d", ak_iArray[2][1][1]);
    return 0;
}