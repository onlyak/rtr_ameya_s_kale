#include <stdio.h>
int main(void)
{
	printf("Ameya Kale\n");
	int ak_a = 45;
	printf("Integer Decimal value %d\n", ak_a);
	printf("Integer Octal value %o\n", ak_a);
	printf("Integer Hexadecimal value %x\n", ak_a);
	printf("Integer Hexadecimal value %X\n", ak_a);
	char ak_ch = 'K';
	printf("Character %c\n", ak_ch);
	char ak_str[] = "RTR-2020";
	printf("%s\n", ak_str);
	long ak_num = 43243242L;
	printf("String %ld\n", ak_num);
	unsigned int ak_b = 9;
	printf("Unsigned Int %u\n", ak_b);
	float ak_f_num = 5433.1956f;
	printf("floating point number %%f %f\n", ak_f_num);
	printf("floating point number with %%3.2f %3.2f\n", ak_f_num);
	printf("floating point number with %%4.3f %4.3f\n", ak_f_num);
	double ak_d_pi = 3.1415926535897;
	printf("double precision floating point number(no exponent)%g\n", ak_d_pi);
	printf("double precision floating point number(lowercase)%e\n", ak_d_pi);
	printf("double precision floating point number(uppercase)%E\n", ak_d_pi);
	printf("double hexadecimal value (lowercase)%a\n", ak_d_pi);
	printf("double hexadecimal value (uppercase)%A\n", ak_d_pi);
	return(0);
}
