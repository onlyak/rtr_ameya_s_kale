#include <stdio.h>
void recursive(unsigned int);
int main(int argc, char *argv[], char *envp[])
{
  printf("\nAmeya Kale\n");
  unsigned int ak_num;
  printf("\nEnter number :-  ");
  scanf("%u", &ak_num);
  printf("\nOutput :-\n");
  recursive(ak_num);
  return 0;
}

void recursive(unsigned int ak_n)
{
  printf("ak_n :-  %u\n", ak_n);
  if (ak_n > 0)
  {
    recursive(ak_n - 1);
  }
}
