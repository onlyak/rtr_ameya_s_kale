#include <stdio.h>
int main(int argc, char *argv[], char *envp[])
{
    printf("\nAmeya Kale\n");
    void MyAddition(void);
    int MySubtraction(void);
    void MyMultiplication(int, int);
    int MyDivision(int, int);
    int ak_result_subtraction;
    int ak_a_multiplication, ak_b_multiplication;
    int ak_a_division, ak_b_division, ak_result_division;

    MyAddition();
    ak_result_subtraction = MySubtraction();
    printf("\nSubtraction :-%d", ak_result_subtraction);
    printf("\nEnter value for multiplication:- ");
    scanf("%d", &ak_a_multiplication);
    printf("\nEnter value for multiplication:- ");
    scanf("%d", &ak_b_multiplication);

    MyMultiplication(ak_a_multiplication, ak_b_multiplication);

    printf("\nEnter value for division :- ");
    scanf("%d", &ak_a_division);
    printf("\nEnter value for division :- ");
    scanf("%d", &ak_b_division);
    ak_result_division = MyDivision(ak_a_division, ak_b_division);

    printf("Quotient:- %d\n",ak_result_division);
    return 0;
}
void MyAddition(void)
{
    int ak_a, ak_b, ak_sum;
    printf("\nEnter value for addition:- ");
    scanf("%d", &ak_a);
    printf("\nEnter value for addition:- ");
    scanf("%d", &ak_b);
    ak_sum = ak_a + ak_b;
    printf("Sum :-%d\n",ak_sum);
}
int MySubtraction(void)
{
    int ak_a, ak_b, ak_subtraction;
    printf("\nEnter value for subtraction:- ");
    scanf("%d", &ak_a);
    printf("\nEnter value for subtraction:- ");
    scanf("%d", &ak_b);
    ak_subtraction = ak_a - ak_b;
    return ak_subtraction;
}
void MyMultiplication(int ak_a, int ak_b)
{
    int ak_multiplication;
    ak_multiplication = ak_a * ak_b;
    printf("Multiplication :-%d\n",ak_multiplication);
}
int MyDivision(int ak_a, int ak_b)
{
    int ak_division_quotient;
    if (ak_a > ak_b)
        ak_division_quotient = ak_a / ak_b;
    else
        ak_division_quotient = ak_b / ak_a;
    return ak_division_quotient;
}
