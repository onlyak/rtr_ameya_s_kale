#include <stdio.h>
int main(int argc, char *argv[], char *envp[])
{
    void display_information(void);
    display_information();
    return 0;
}
void display_information(void)
{
    void Function_My(void);
    void Function_Name(void);
    void Function_Is(void);
    void Function_FirstName(void);
    void Function_MiddleName(void);
    void Function_Surname(void);
    void Function_Of(void);

    Function_My();
    Function_Name();
    Function_Is();
    Function_FirstName();
    Function_MiddleName();
    Function_Surname();
    Function_Of();
}
void Function_My(void)
{
    printf("\nMy\n");
}
void Function_Name(void)
{
    printf("name\n");
}
void Function_Is(void)
{
    printf("is\n");
}
void Function_FirstName(void)
{
    printf("Ameya\n");
}
void Function_MiddleName(void)
{
    printf("Sumeet\n");
}
void Function_Surname(void)
{
    printf("Kale\n");
}
void Function_Of(void)
{
    printf("of RTR2020 batch\n");
}
