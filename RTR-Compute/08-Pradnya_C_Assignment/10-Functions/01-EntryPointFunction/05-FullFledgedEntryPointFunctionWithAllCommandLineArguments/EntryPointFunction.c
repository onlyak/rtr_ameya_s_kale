#include <stdio.h>
 int main(int argc, char *argv[], char *envp[])
{
    printf("\nAmeya Kale\n");
    int ak_i;
    printf("Return type int");
    printf("\nThree arguments are passed");
    printf("\nNumber of command line arguments:-%d", argc);
    printf("\nCommand line arguments passed are");
    for (ak_i = 0; ak_i < argc; ak_i++)
    {
        printf("\ncommand line argument number %d:- %s\n", (ak_i + 1), argv[ak_i]);
    }
    printf("\n10 environmental variables passed to this program are");
    for (ak_i = 0; ak_i < 10; ak_i++)
    {
        printf("\nenvironmental variable number %d:-%s\n", (ak_i + 1), envp[ak_i]);
    }
    return 0;
}
