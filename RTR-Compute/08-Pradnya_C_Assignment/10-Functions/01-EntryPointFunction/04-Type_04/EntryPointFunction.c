#include <stdio.h>
int main(int argc, char *argv[])
{
    printf("\nAmeya Kale\n");
    int ak_i;
    printf("Return type is integer");
    printf("\nNumber of command line arguments:-\t%d", argc);
    printf("\nCommand line arguments present in the program are:-");
    for (ak_i = 0; ak_i < argc; ak_i++)
    {
        printf("Command line argument number %d:-%s\n", (ak_i + 1), argv[ak_i]);
    }
    return 0;
}
