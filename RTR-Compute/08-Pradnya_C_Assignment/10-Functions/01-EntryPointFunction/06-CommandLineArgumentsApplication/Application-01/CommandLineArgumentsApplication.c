#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
int main(int argc, char *argv[], char *envp[])
{
    printf("\nAmeya Kale\n");
    int ak_i;
    int ak_num;
    int ak_sum = 0;
    if (argc == 1)
    {
        printf("\nAddition of command line arguments:-");
        exit(0);
    }
    for (ak_i = 1; ak_i < argc; ak_i++)
    {
        ak_num = atoi(argv[ak_i]);
        ak_sum = ak_sum + ak_num;
    }
    printf("Sum:- %d\n", ak_sum);
    return 0;
}
