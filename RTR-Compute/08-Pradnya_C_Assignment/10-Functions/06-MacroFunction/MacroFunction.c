#include <stdio.h>
#define MAX_NUMBER(ak_a, ak_b) ((ak_a > ak_b) ? ak_a : ak_b)
int main(int argc, char *argv[], char *envp[])
{
    printf("\nAmeya Kale\n");
    int ak_iNum_01;
    int ak_iNum_02;
    int ak_iResult;

    float ak_fNum_01;
    float ak_fNum_02;
    float ak_fResult;

    printf("\nEnter a number :-");
    scanf("%d", &ak_iNum_01);
    printf("\nEnter a number :-");
    scanf("%d", &ak_iNum_02);
    ak_iResult = MAX_NUMBER(ak_iNum_01, ak_iNum_02);

    printf("\nResult of func MAX_NUMBER() :-%d", ak_iResult);
    printf("\nEnter a floating point number :");
    scanf("%f", &ak_fNum_01);
    printf("\nEnter a floating point number :");
    scanf("%f", &ak_fNum_02);
    ak_fResult = MAX_NUMBER(ak_fNum_01, ak_fNum_02);
    printf("\nResult of func MAX_NUMBER():- %f\n", ak_fResult);
    return 0;
}
