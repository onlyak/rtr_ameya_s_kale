#include <stdio.h>
int ak_global_count = 0;
int main(void)
{
    printf("\nAmeya Kale\n");
    void change_count(void);
    void change_count_one(void);
    void change_count_two(void);

    change_count();
    change_count_one();
    change_count_two();
    return 0;
}
void change_count(void)
{
    ak_global_count = ak_global_count + 1;
    printf("After func call, ak_global_count :- %d\n", ak_global_count);
}
