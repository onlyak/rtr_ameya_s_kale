#include <stdio.h>
int main(void)
{
    printf("\nAmeya Kale\n");
    void change_count(void);
    extern int ak_global_count;
    printf("\nValue Of ak_global_count before func call :-  %d", ak_global_count);
    change_count();
    printf("\nValue Of ak_global_count after func call :- %d", ak_global_count);
    return 0;
}

int ak_global_count = 0; 
void change_count(void)
{
    ak_global_count = 9;
    printf("\nValue Of ak_global_count in func :-  %d", ak_global_count);
}
