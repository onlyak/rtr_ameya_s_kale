#include <stdio.h>
int ak_global_count = 0;
char ak_global_char;
int main(void)
{
    printf("\nAmeya Kale\n");
    void change_count_one(void);
    void change_count_two(void);
    void change_count_three(void);

    printf("\nDefault value of ak_global_char:-%c\n", ak_global_char);
    printf("\nValue of ak_global_count:-%d\n", ak_global_count);
    change_count_one();
    change_count_two();
    change_count_three();
    return 0;
}
void change_count_one(void)
{
    ak_global_count = 9;
    printf("\nValue of ak_global_count :-%d\n", ak_global_count);
}
void change_count_two(void)
{
    ak_global_count = ak_global_count + 1;
    printf("\nValue of ak_global_count :-%d\n", ak_global_count);
}

void change_count_three(void)
{
    ak_global_count = ak_global_count + 10;
    printf("\nValue of ak_global_count :- %d\n", ak_global_count);
}
