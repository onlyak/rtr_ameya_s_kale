#include<stdio.h>
#include<stdarg.h>

int AkSum(int, ...);
int main()
{
    int ak_answer;
    printf("\nAmeya Kale\n");
    ak_answer=AkSum(6,9,18,27,36,45,54);
    printf("Answer: %d",ak_answer);

    ak_answer=AkSum(0);
    printf("\nAnswer: %d",ak_answer);
    return 0;
}

int AkSum(int ak_num,...)
{
    int ak_sum=0;
    int ak_n;

    va_list ak_list;
        va_start(ak_list,ak_num);
        while(ak_num--)
        {
            ak_n=va_arg(ak_list,int);
            ak_sum+=ak_n;
        }
    va_end(ak_list);
    return ak_sum;
}
