#include<stdio.h>
#include<stdarg.h>

int AkSum(int, ...);
int main()
{
    int ak_answer;
    printf("\nAmeya Kale\n");
    ak_answer=AkSum(4,63,72,81,90);
    printf("Answer: %d",ak_answer);
}

int AkSum(int ak_num,...)
{
    int ak_sum=0;
    int akVa_Calc(int,va_list);

    va_list ak_list;
        va_start(ak_list,ak_num);
        ak_sum=akVa_Calc(ak_num,ak_list);
    va_end(ak_list);
    
    return ak_sum;
}

int akVa_Calc(int ak_num, va_list ak_list)
{
    int ak_sum=0;
    int ak_n;
    while(ak_num--)
    {
        ak_n=va_arg(ak_list,int);
        ak_sum+=ak_n;
    }
    return ak_sum;
}