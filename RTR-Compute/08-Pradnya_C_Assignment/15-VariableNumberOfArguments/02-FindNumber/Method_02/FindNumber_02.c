#include <stdio.h>
#include <stdarg.h>

#define NUM 18
#define LIST 5

void AkFindNum(int, int, ...);

int main()
{
    printf("\nAmeya Kale\n");
    AkFindNum(NUM, LIST, 18,43,534,18,18);
    return 0;
}

void AkFindNum(int ak_searchNo, int ak_num, ...)
{
    int ak_count = 0;
    int ak_n;
    va_list ak_noList;

    va_start(ak_noList, ak_num);
    ak_count = AkVa_Search(NUM, ak_num, ak_noList);
    if (ak_count == 0)
    {
        printf("\nNumber not found");
    }
    else
    {
        printf("\nNumber found %d times", ak_count);
    }
    va_end(ak_noList);
}

int AkVa_Search(int ak_searchNo, int ak_num, va_list ak_list)
{
    int ak_count = 0;
    int ak_n;
    while (ak_num--)
    {
        ak_n = va_arg(ak_list, int);
        if (ak_n == ak_searchNo)
        {
            ak_count++;
        }
    }
    return ak_count;
}