#include <stdio.h>
#include <stdarg.h>

#define NUM 9
#define LIST 9

void AkFindNum(int, int, ...);

int main()
{
    printf("\nAmeya Kale\n");
    AkFindNum(NUM, LIST, 312, 9, 54353, 44, 4234, 9, 763, 654, 54);
    return 0;
}

void AkFindNum(int ak_searchNo, int ak_num,...)
{
    int ak_count = 0;
    int ak_n;
    va_list ak_noList;

    va_start(ak_noList, ak_num);
        while (ak_num--)
        {
            ak_n = va_arg(ak_noList, int);
            if (ak_n == ak_searchNo)
            {
                ak_count++;
            }
        }
        if (ak_count == 0)
        {
            printf("\nNumber not found");
        }
        else
        {
            printf("\nNumber found %d times", ak_count);
        }
    va_end(ak_noList);
}