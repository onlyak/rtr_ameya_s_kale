#include <stdio.h>

#define AK_PI 3.1415926535897932
#define AK_STRING "RTR-2020"
enum
{
	ak_JANUARY = 1,
	ak_FEBRUARY,
	ak_MARCH,
	ak_APRIL,
	ak_MAY,
	ak_JUNE,
	ak_JULY,
	ak_AUGUST,
	ak_SEPTEMBER,
	ak_OCTOBER,
	ak_NOVEMBER,
	ak_DECEMBER
};

enum
{
	ak_SUNDAY,
	ak_MONDAY,
	ak_TUESDAY,
	ak_WEDNESDAY,
	ak_THURSDAY,
	ak_FRIDAY,
	ak_SATURDAY,
};

enum numbers
{
	ak_ONE,
	ak_TWO,
	ak_THREE,
	ak_FOUR,
	ak_FIVE = 5,
	ak_SIX,
	ak_SEVEN,
	ak_EIGHT,
	ak_NINE,
	ak_TEN
};

enum boolean
{
	ak_TRUE = 1,
	ak_FALSE = 0
};

int main(void)
{
	const double ak_eps = 0.000001;
	printf("Ameya Kale\n");
	printf("constant epsilon = %lf\n\n", ak_eps);
	printf("Sunday is day number = %d\n", ak_SUNDAY);
	printf("Monday is day number = %d\n", ak_MONDAY);
	printf("Tuesday is day number = %d\n", ak_TUESDAY);
	printf("Wednesday is day number = %d\n", ak_WEDNESDAY);
	printf("Thursday is day number = %d\n", ak_THURSDAY);
	printf("Friday is day number = %d\n", ak_FRIDAY);
	printf("Saturday is day number = %d\n", ak_SATURDAY);
	printf("One is enum number = %d\n", ak_ONE);
	printf("Two is enum number = %d\n", ak_TWO);
	printf("Three is enum number = %d\n", ak_THREE);
	printf("Four is enum number = %d\n", ak_FOUR);
	printf("Five is enum number = %d\n", ak_FIVE);
	printf("Six is enum number = %d\n", ak_SIX);
	printf("Seven is enum number = %d\n", ak_SEVEN);
	printf("Eight is enum number = %d\n", ak_EIGHT);
	printf("Nine is enum number = %d\n", ak_NINE);
	printf("Ten is enum number = %d\n", ak_TEN);
	printf("January is month number = %d\n", ak_JANUARY);
	printf("February is month number = %d\n", ak_FEBRUARY);
	printf("March is month number = %d\n", ak_MARCH);
	printf("April is month number = %d\n", ak_APRIL);
	printf("May is month number = %d\n", ak_MAY);
	printf("June is month number = %d\n", ak_JUNE);
	printf("July is month number = %d\n", ak_JULY);
	printf("August is month number = %d\n", ak_AUGUST);
	printf("September is month number = %d\n", ak_SEPTEMBER);
	printf("October is month number = %d\n", ak_OCTOBER);
	printf("November is month number = %d\n", ak_NOVEMBER);
	printf("December is month number = %d\n", ak_DECEMBER);
	printf("Value of TRUE is = %d\n", ak_TRUE);
	printf("Value of FALSE is = %d\n", ak_FALSE);
	printf("PI = %.10lf\n", AK_PI);
	printf("Area of circle of radius 4 units = %f\n", (AK_PI * 2.0f * 2.0f)); 
    printf("\n");
    printf(AK_STRING);

    return(0);
}



