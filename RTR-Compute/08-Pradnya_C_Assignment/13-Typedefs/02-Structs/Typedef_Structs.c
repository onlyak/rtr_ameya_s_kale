#include <stdio.h>
#define MAX_LENGTH 100

struct Employee
{
    char ak_name[MAX_LENGTH];
    unsigned int ak_age;
    char ak_gender;
    double ak_salary;
};

struct Data
{
    int ak_i;
    float ak_f;
    double ak_d;
    char ak_c;
};

int main()
{
    printf("\nAmeya Kale\n");
    typedef struct Employee AK_EMP_TYPE;
    typedef struct Data AK_DATA;

    struct Employee ak_emp = {"Ameya", 24, 'M', 5000.0};
    AK_EMP_TYPE ak_typeDefEmp = {"Kale", 24, 'M', 5000.0};

    struct Data ak_data = {90, 99.45f, 72.9972, 'AK'};
    AK_DATA ak_typeDefData = {9, 45.99f, 9972.72, 'KA'};

    printf("\nEmployee Data:-");
    printf("\nName: %s", ak_emp.ak_name);
    printf("\nAge: %d", ak_emp.ak_age);
    printf("\nGender: %c", ak_emp.ak_gender);
    printf("\nSalary: %lf", ak_emp.ak_salary);

    printf("\nEmployee TypeDef Data:-");
    printf("\nName: %s", ak_typeDefEmp.ak_name);
    printf("\nAge: %d", ak_typeDefEmp.ak_age);
    printf("\nGender: %c", ak_typeDefEmp.ak_gender);
    printf("\nSalary: %lf", ak_typeDefEmp.ak_salary);

    printf("\nData:-");
    printf("\nInteger: %d", ak_data.ak_i);
    printf("\nFloat: %f", ak_data.ak_f);
    printf("\nDouble: %lf", ak_data.ak_d);
    printf("\nCharacter: %c", ak_data.ak_c);

    printf("\nTypeDef Data:-");
    printf("\nInteger: %d", ak_typeDefData.ak_i);
    printf("\nFloat: %f", ak_typeDefData.ak_f);
    printf("\nDouble: %lf", ak_typeDefData.ak_d);
    printf("\nCharacter: %c", ak_typeDefData.ak_c);

    return 0;
}