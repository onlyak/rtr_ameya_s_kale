#include<stdio.h>
typedef int AK_INT;
int main()
{
    printf("\nAmeya Kale");
    AK_INT Add(AK_INT,AK_INT);
    typedef int AK_INT;
    typedef float AK_FLOAT;
    typedef char AK_CHAR;
    typedef double AK_DOUBLE;

    //Win32 standard
    typedef unsigned int UINT;
    typedef UINT HANDLE;
    typedef HANDLE HWND;
    typedef HANDLE HINSTANCE;

    AK_INT ak_a=18,ak_i;
    AK_INT ak_iArray[]={10,20,30,40,50,60};
    AK_FLOAT ak_f=18.9f;
    AK_CHAR ak_charArray[]="Hi";
    AK_CHAR ak_charArray_1[][5]={"Ameya","Kale"};
    AK_DOUBLE ak_double=99.989898;

    UINT ak_uint=9693;
    HANDLE ak_handle=999;
    HWND ak_hwnd=9393;
    HINSTANCE ak_hInstance=98989; 

    printf("\nAK_INT: %d",ak_a);

    for(ak_i=0;ak_i<sizeof(ak_iArray)/sizeof(int);ak_i++)
    {
        printf("\nNumerical Array: %d",ak_iArray[ak_i]);
    }

    printf("\nAK_FLOAT: %f",ak_f);
    printf("\nAK_DOUBLE: %lf",ak_double);
    printf("\nAK_CHAR: %s",ak_charArray);
    
    for(ak_i=0;ak_i<sizeof(ak_charArray_1)/sizeof(ak_charArray_1[0]);ak_i++)
    {
        printf("\n%s\t",ak_charArray_1[ak_i]);
    }

    printf("\nUINT: %u",ak_uint);
    printf("\nHANDLE: %u",ak_handle);
    printf("\nHWND: %u",ak_hwnd);
    
    AK_INT ak_x=90;
    AK_INT ak_y=180;
    AK_INT ak_ans=Add(ak_x,ak_y);

    printf("\nAddition: %d",ak_ans);
    return 0;
}

AK_INT Add(AK_INT ak_a,AK_INT ak_b)
{
    return ak_a+ak_b;
}
