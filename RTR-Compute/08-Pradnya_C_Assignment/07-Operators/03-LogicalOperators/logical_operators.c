#include <stdio.h>
int main(void)
{
    int ak_a;
    int ak_b;
    int ak_c;
    int ak_result;
    printf("Enter integer(ak_a):-");
    scanf("%d", &ak_a);
    printf("\nEnter integer(ak_b):-");
    scanf("%d", &ak_b);
    printf("\nEnter integer:-");
    scanf("%d", &ak_c);
    ak_result = (ak_a <= ak_b) && (ak_b != ak_c);
    printf("\nResult of (ak_a <= ak_b) && (ak_b != ak_c) is %d",ak_result);
    ak_result = (ak_b >= ak_a) || (ak_a == ak_c);
    printf("\nResult of (ak_b >= ak_a) || (ak_a == ak_c) is %d",ak_result);
    ak_result = !ak_a;
    printf("\nResult of !ak_a %d", ak_result);
    ak_result = !ak_b;
    printf("\nResult of !ak_b %d",ak_result);
    ak_result = !ak_c;
    printf("\nResult of !ak_c %d",ak_result);
    ak_result = (!(ak_a <= ak_b) && !(ak_b != ak_c));
    printf("\nResult of (!(ak_a <= ak_b) && !(ak_b != ak_c)) %d",ak_result);
    ak_result = !((ak_b >= ak_a) || (ak_a == ak_c));
    printf("\nResult of !((ak_b >= ak_a) || (ak_a == ak_c)) %d",ak_result);
    return 0;
}