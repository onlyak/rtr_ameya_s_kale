#include <stdio.h>
int main(void)
{
       printf("\nAmeya Kale\n");
       int ak_a, ak_b;
       int ak_k, ak_q;
       char ak_ch_result_01, ak_ch_result_02;
       int ak_i_result_01, ak_i_result_02;
       ak_a = 18;
       ak_b = 27;
       printf("\nak_a:-%d",ak_a);
       printf("\nak_b:-%d",ak_b);       
       ak_ch_result_01 = (ak_a > ak_b) ? 'B' : 'C';
       printf("\nResult of (ak_a > ak_b) ? 'B' : 'C' :- %d",ak_ch_result_01);
       ak_i_result_01 = (ak_a > ak_b) ? ak_a : ak_b;
       printf("\nResult of (ak_a > ak_b) ? ak_a : ak_b' :- %d",ak_i_result_01);
       ak_k = 36;
       ak_q = 45;
       printf("\nak_k:-%d",ak_k);
       printf("\nak_q:-%d",ak_q);
       ak_ch_result_02 = (ak_k != ak_q) ? 'K' : 'Q';
       printf("\nResult of (ak_k != ak_q) ? 'K' : 'Q' :- %d",ak_ch_result_02);
       ak_i_result_02 = (ak_k != ak_q) ? ak_k : ak_q;
       printf("\nResult of (ak_k != ak_q) ? ak_k : ak_q :- %d",ak_i_result_02);
       return 0;
}
