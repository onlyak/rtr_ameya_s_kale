#include <stdio.h>
int main(void)
{
    int ak_a;
    int ak_b;
    int ak_result;
    printf("Enter integer (ak_a):-");
    scanf("%d", &ak_a);
    printf("\nEnter integer(ak_b):-");
    scanf("%d", &ak_b);
    printf("\n");
    ak_result = (ak_a < ak_b);
    printf("ak_a < ak_b %d\n",ak_result);
    ak_result = (ak_a > ak_b);
    printf("ak_a > ak_b %d\n",ak_result);
    ak_result = (ak_a <= ak_b);
    printf("ak_a <= ak_b %d\n",ak_result);
    ak_result = (ak_a >= ak_b);
    printf("ak_a >= ak_b %d\n",ak_result);
    ak_result = (ak_a == ak_b);
    printf("ak_a == ak_b %d\n",ak_result);
    ak_result = (ak_a != ak_b);
    printf("ak_a != ak_b %d\n",ak_result);
    return 0;
}