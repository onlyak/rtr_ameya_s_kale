#include <stdio.h>

int main(void)
{
	printf("Ameya Kale\n");
	int ak_a;
	int ak_b;
	int ak_result;	
	printf("\n");
	printf("enter first number:");
	scanf("%d", &ak_a);
	printf("\n");
	printf("enter second number:");
	scanf("%d", &ak_b);
	ak_result = ak_a + ak_b;
	printf("\naddition of a = %d and b = %d gives %d.\n", ak_a, ak_b, ak_result);
	ak_result = ak_a - ak_b;
	printf("subtraction of a = %d and b = %d gives %d.\n", ak_a, ak_b, ak_result);
	ak_result = ak_a * ak_b;
	printf("multiplication of a = %d and b = %d gives %d.\n", ak_a, ak_b, ak_result);
	ak_result = ak_a / ak_b;
	printf("division of a = %d and b = %d gives quotient %d.\n", ak_a, ak_b, ak_result);
	ak_result = ak_a % ak_b;
	printf("division of a = %d and b = %d gives remainder %d.\n", ak_a, ak_b, ak_result);
	return(0);
}
