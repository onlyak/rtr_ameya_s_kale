#include <stdio.h>

int main(void)
{
	printf("Ameya Kale\n");
	int ak_a;
	int ak_b;
	int ak_x;	
	printf("\n");
	printf("enter a number : ");
	scanf("%d", &ak_a);
	printf("\n");
	printf("enter another number : ");
	scanf("%d", &ak_b);
	printf("\n");
	ak_x = ak_a;
	ak_a += ak_b; 
	printf("addition of two numbers %d\n",ak_a);
	ak_x = ak_a;
	ak_a -= ak_b; 
	printf("subtraction of two numbers %d\n",ak_a);
	ak_x = ak_a;
	ak_a *= ak_b; 
	printf("multiplication of two numbers %d\n",ak_a);
	ak_x = ak_a;
	ak_a /= ak_b;  
	printf("division of two numbers yielding quotient %d\n",ak_a);
	ak_x = ak_a;
	ak_a %= ak_b; 
	printf("division of two numbers yielding remainder %d\n",ak_a);
	return(0);
}
