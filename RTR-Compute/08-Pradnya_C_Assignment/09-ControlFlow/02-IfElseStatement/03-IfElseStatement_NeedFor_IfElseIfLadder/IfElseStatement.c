#include <stdio.h>
int main(void)
{
    printf("\nAmeya Kale\n");
    int ak_num;
    printf("\nEnter number:-");
    scanf("%d", &ak_num);
    if (ak_num < 0)
    {
        printf("\nNumber is less than 0");
    }
    else
    {
        if ((ak_num > 0) && (ak_num <= 100))
        {
            printf("\nNumber is greater than 0 and less than or equal to 100");
        }
        else
        {
            if ((ak_num > 100) && (ak_num <= 200))
            {
                printf("\nNumber is greater than 100 and less than or equal to 200");
            }
            else
            {
                if ((ak_num > 200) && (ak_num <= 300))
                {
                    printf("\nNumber is greater than 200 and less than or equal to 300");
                }
                else
                {
                    if ((ak_num > 300) && (ak_num <= 400))
                    {
                        printf("\nNumber is greater than 300 and less than or equal to 400");

                    }
                    else
                    {
                        if ((ak_num > 400) && (ak_num <= 500))
                        {
                             printf("\nNumber is greater than 400 and less than or equal to 500");
                        }
                        else
                        {
                            printf("\nNumber is greater than 500");            
                        }
                    }
                }
            }
        }
    }
    return 0;
}
