#include <stdio.h>
#include <conio.h>
int main(void)
{
    printf("\nAmeya Kale\n");
    int ak_a, ak_b;
    int ak_result;
    char ak_option, ak_option_division;
    printf("\nEnter value:-");
    scanf("%d", &ak_a);
    printf("\nEnter value:-");
    scanf("%d", &ak_b);
    printf("\nEnter option to perform operation:-\t\n");
    printf("'A' or 'a' For Addition : \n");
    printf("'S' or 's' For Subtraction : \n");
    printf("'M' or 'm' For Multiplication : \n");
    printf("'D' or 'd' For Division : \n");
    printf("\nEnter option:-");
    ak_option=getch();
    switch (ak_option)
    {
    case 'A':
    case 'a':
        ak_result = ak_a + ak_b;
        printf("Addition of two numbers:-%d", ak_result);
        break;
    case 'S':
    case 's':
        if (ak_a >= ak_b)
        {
            ak_result = ak_a - ak_b;
            printf("Subtraction of two numbers:-%d", ak_result);
        }
        else
        {
            ak_result = ak_b - ak_a;
            printf("Subtraction of two numbers:-%d", ak_result);
        }
        break;
    case 'M':
    case 'm':
        ak_result = ak_a * ak_b;
        printf("Multiplication of two numbers:-%d", ak_result);
        break;
    case 'D':
    case 'd':
        printf("\nEnter option to perform operation\n");
        printf("'Q' or 'q' or '/' For Quotient Upon Division : \n");
        printf("'R' or 'r' or '%%' For Remainder Upon Division : \n");
        printf("\nEnter Option : ");
        ak_option_division=getch();
        switch (ak_option_division)
        {
        case 'Q':
        case 'q':
        case '/':
            if (ak_a >= ak_b)
            {
                ak_result = ak_a / ak_b;
                printf("Division of two numbers:-%d", ak_result);
            }
            else
            {
                ak_result = ak_b / ak_a;
                printf("Division of two numbers:-%d", ak_result);
            }
            break;
        case 'R':
        case 'r':
        case '%':
            if (ak_a >= ak_b)
            {
                ak_result = ak_a % ak_b;
                printf("Remainder of two numbers:-%d", ak_result);
            }
            else
            {
                ak_result = ak_b % ak_a;
                printf("Remainder of two numbers:-%d", ak_result);
            }
            break;
        default:
            printf("\nInvalid character entered");
            break;
        }
        break;
    default:
        printf("\nInvalid character entered");
        break;
    }
    return 0;
}
