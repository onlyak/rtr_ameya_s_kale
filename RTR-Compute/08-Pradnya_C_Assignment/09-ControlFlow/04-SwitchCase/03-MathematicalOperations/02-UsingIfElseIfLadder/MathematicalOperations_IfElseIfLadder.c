#include <stdio.h>
#include <conio.h>
int main(void)
{
    printf("\nAmeya Kale\n");
    int ak_a, ak_b;
    int ak_result;
    char ak_option, ak_option_division;
    printf("\nEnter value:-");
    scanf("%d", &ak_a);
    printf("\nEnter value:-");
    scanf("%d", &ak_b);
    printf("\nEnter option to perform operation:-\t\n");
    printf("'A' or 'a' For Addition : \n");
    printf("'S' or 's' For Subtraction : \n");
    printf("'M' or 'm' For Multiplication : \n");
    printf("'D' or 'd' For Division : \n\n");
    printf("\nEnter option:-");
    ak_option = getch();
    if (ak_option == 'A' || ak_option == 'a')
    {
        ak_result = ak_a + ak_b;
        printf("Addition of two numbers:-%d", ak_result);
    }
    else if (ak_option == 'S' || ak_option == 's')
    {
        if (ak_a >= ak_b)
        {
            ak_result = ak_a - ak_b;
            printf("Subtraction of two numbers:-%d", ak_result);
        }
        else
        {
            ak_result = ak_b - ak_a;
            printf("Subtraction of two numbers:-%d", ak_result);
        }
    }
    else if (ak_option == 'M' || ak_option == 'm')
    {
        ak_result = ak_a * ak_b;
        printf("Multiplication of two numbers:-%d", ak_result);
    }
    else if (ak_option == 'D' || ak_option == 'd')
    {
        printf("\nEnter option to perform operation\n");
        printf("'Q' or 'q' or '/' For Quotient Upon Division : \n");
        printf("'R' or 'r' or '%%' For Remainder Upon Division : \n");
        printf("\nEnter Option : ");
        ak_option_division = getch();
        if (ak_option_division == 'Q' || ak_option_division == 'q' || ak_option_division == '/')
        {
            if (ak_a >= ak_b)
            {
                ak_result = ak_a / ak_b;
                printf("Division of two numbers:-%d", ak_result);
            }
            else
            {
                ak_result = ak_b / ak_a;
                printf("Division of two numbers:-%d", ak_result);
            }
        }
        else if (ak_option_division == 'R' || ak_option_division == 'r' ||
                 ak_option_division == '%')
        {
            if (ak_a >= ak_b)
            {
                ak_result = ak_a % ak_b;
                printf("Remainder of two numbers:-%d", ak_result);
            }
            else
            {
                ak_result = ak_b % ak_a;
                printf("Remainder of two numbers:-%d", ak_result);
            }
        }
        else
            printf("\nInvalid character entered");
    }
    else
        printf("\nInvalid character entered");
    return 0;
}
