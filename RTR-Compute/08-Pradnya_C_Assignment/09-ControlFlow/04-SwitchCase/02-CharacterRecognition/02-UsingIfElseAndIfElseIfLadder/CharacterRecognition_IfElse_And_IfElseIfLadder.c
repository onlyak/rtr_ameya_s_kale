#include <stdio.h>
#define AK_CHAR_ALPHABET_UPPER_CASE_BEGINNING 65
#define AK_CHAR_ALPHABET_UPPER_CASE_ENDING 90
#define AK_CHAR_ALPHABET_LOWER_CASE_BEGINNING 97
#define AK_CHAR_ALPHABET_LOWER_CASE_ENDING 122
#define AK_CHAR_DIGIT_BEGINNING 48
#define AK_CHAR_DIGIT_ENDING 57
int main(void)
{
    printf("\nAmeya Kale\n");
    char ak_ch;
    int ak_ch_value;
    printf("\nEnter Character:-");
    scanf("%c",&ak_ch);
    if ((ak_ch == 'A' || ak_ch == 'a') || (ak_ch == 'E' || ak_ch == 'e') || (ak_ch == 'I' || ak_ch == 'i') || (ak_ch == 'O' || ak_ch == 'o') || (ak_ch == 'U' || ak_ch == 'u'))
    {
        printf("\nCharacter entered is a vowel");
    }
    else
    {
        ak_ch_value = (int)ak_ch;
        if ((ak_ch_value >= AK_CHAR_ALPHABET_UPPER_CASE_BEGINNING && ak_ch_value <=AK_CHAR_ALPHABET_UPPER_CASE_ENDING) || (ak_ch_value >=AK_CHAR_ALPHABET_LOWER_CASE_BEGINNING && ak_ch_value <=AK_CHAR_ALPHABET_LOWER_CASE_ENDING))
        {
            printf("\nCharacter entered is a consonant");
        }
        else if (ak_ch_value >= AK_CHAR_DIGIT_BEGINNING && ak_ch_value <=AK_CHAR_DIGIT_ENDING)
        {
            printf("\nCharacter entered is a digit");
        }
        else
        {
            printf("\nCharacter entered is a special character");
        }
    }
    return 0;
}
