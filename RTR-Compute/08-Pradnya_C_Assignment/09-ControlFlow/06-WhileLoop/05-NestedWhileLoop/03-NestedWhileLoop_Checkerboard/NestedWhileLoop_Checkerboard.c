#include <stdio.h>
int main(int argc, char *argv[])
{
    printf("\nAmeya Kale\n");
    int ak_i, ak_j, ak_c;
    printf("\n");
    ak_i = 0;
    while (ak_i < 16)
    {
        ak_j = 0;
        while (ak_j < 16)
        {
            ak_c = ((ak_i & 0x8) == 0) ^ ((ak_j & 0x8) == 0);
            if (ak_c == 0)
                printf(" ");
            if (ak_c == 1)
                printf("* ");
            ak_j++;
        }
        printf("\n");
        ak_i++;
    }
    return 0;
}
