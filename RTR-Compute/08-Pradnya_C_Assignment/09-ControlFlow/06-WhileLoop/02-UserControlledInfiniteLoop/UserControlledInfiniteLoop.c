#include <stdio.h>
#include <conio.h>
int main(int argc, char *argv[])
{
    printf("\nAmeya Kale\n");
    char ak_option, ak_ch = '\0';
    printf("\nEnter 'Q' or 'q' to quit the infinite for loop\n");
    printf("Enter 'Y' oy 'y' to initiate user controlled infinite loop:-");
    ak_option = getch();
    if (ak_option == 'Y' || ak_option == 'y')
    {
        while (1)
        {
            printf("In Loop.....\n");
            ak_ch = getch();
            if (ak_ch == 'Q' || ak_ch == 'q')
            {
                printf("\nQuitting");
                break;
            }
        }
        printf("\n");
    }
    else
        printf("You must press 'Y' or 'y' to initiate the user controlled infinite loop");
    return 0;
}
