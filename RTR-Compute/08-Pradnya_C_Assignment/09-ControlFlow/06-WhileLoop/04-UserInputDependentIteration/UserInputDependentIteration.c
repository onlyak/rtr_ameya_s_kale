#include <stdio.h>
int main(int argc, char *argv[])
{
    printf("\nAmeya Kale\n");
    int ak_i_num, ak_num, ak_i;
    printf("\nEnter an integer to begin:- ");
    scanf("%d", &ak_i_num);
    printf("\nHow many digits do you want to print from %d onwards ? :- ", ak_i_num);
    scanf("%d", &ak_num);
    printf("\nPrinting digits %d to %d : \n", ak_i_num, (ak_i_num + ak_num));
    ak_i = ak_i_num;
    while (ak_i <= (ak_i_num + ak_num))
    {
        printf("\t%d\n", ak_i);
        ak_i++;
    }
    return 0;
}
