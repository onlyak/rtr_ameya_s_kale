#include <stdio.h>
int main(int argc, char *argv[])
{
    printf("\nAmeya Kale\n");
    int ak_i, ak_j;
    printf("\nPrinting Digits 2 to 20 and 20 to 100\n");
    ak_i = 2;
    ak_j = 20;
    while (ak_i <= 20, ak_j <= 100)
    {
        printf("\t %d \t %d\n", ak_i, ak_j);
        ak_i++;
        ak_j = ak_j + 10;
    }
    return 0;
}
