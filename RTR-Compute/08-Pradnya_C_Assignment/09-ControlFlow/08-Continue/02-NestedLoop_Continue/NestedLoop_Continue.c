#include <stdio.h>
int main(int argc, char *argv[])
{
    printf("\nAmeya Kale\n");
    int ak_i, ak_j;
    printf("\nOuter loop prints odd numbers between 1 and 10\n");
    printf("\nInner loop prints even numbers between 1 and 6 for every odd number printed by outer loop\n");

    for (ak_i = 1; ak_i <= 10; ak_i++)
    {
        if (ak_i % 2 != 0)
        {
            printf("ak_i = %d\n", ak_i);
            for (ak_j = 1; ak_j <= 6; ak_j++)
            {
                if (ak_j % 2 == 0)
                {
                    printf("\tj = %d\n", ak_j);
                }
                else
                {
                    continue;
                }
            }
            printf("\n");
        }
        else
        {
            continue;
        }
    }
    printf("\n");
    return 0;
}
