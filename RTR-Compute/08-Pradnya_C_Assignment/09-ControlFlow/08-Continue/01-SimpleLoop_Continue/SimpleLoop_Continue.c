#include <stdio.h>
int main(int argc, char *argv[])
{
    printf("\nAmeya Kale\n");
    int ak_i;
    printf("\nPrinting Even Numbers From 0 to 18\n");
    for (ak_i = 0; ak_i <= 18; ak_i++)
    {
        if (ak_i % 2 != 0)
        {
            continue;
        }
        else
        {
            printf("\t%d\n", ak_i);
        }
    }
    return 0;
}
