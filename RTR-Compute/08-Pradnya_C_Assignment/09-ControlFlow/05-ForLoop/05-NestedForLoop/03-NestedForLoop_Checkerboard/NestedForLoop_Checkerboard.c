#include <stdio.h>
int main(void)
{
    printf("\nAmeya Kale\n");
    int ak_i, ak_j, ak_c;
    for (ak_i = 0; ak_i < 16; ak_i++)
    {
        for (ak_j = 0; ak_j < 16; ak_j++)
        {
            ak_c = ((ak_i & 0x8) == 0) ^ ((ak_j & 0x8) == 0);
            if (ak_c == 0)
                printf(" ");
            if (ak_c == 1)
                printf("* ");
        }
        printf("\n");
    }
    return 0;
}
