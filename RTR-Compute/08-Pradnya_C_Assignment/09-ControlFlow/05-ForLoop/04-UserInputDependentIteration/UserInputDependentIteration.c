#include <stdio.h>
int main(void)
{
    printf("\nAmeya Kale\n");
    int ak_i_num, ak_num, ak_i;
    printf("\nEnter An Integer Value From Which Iteration Must Begin :-");
    scanf("%d", &ak_i_num);
    printf("\nHow Many Digits Do You Want To Print From %d Onwards ? :-", ak_i_num);
    scanf("%d", &ak_num);
    printf("\nPrinting Digits %d to %d :-\n", ak_i_num, (ak_i_num + ak_num));
    for (ak_i = ak_i_num; ak_i <= (ak_i_num + ak_num); ak_i++)
    {
        printf("\t%d\n", ak_i);
    }
    return 0;
}
