#include <stdio.h>
int main(void)
{
    printf("\nAmeya Kale\n");
    int ak_i, ak_j;
    printf("\nPrinting Digits 1 to 10 and 10 to 100\n");
    for (ak_i = 1, ak_j = 10; ak_i <= 10, ak_j <= 100; ak_i++, ak_j = ak_j + 10)
    {
        printf("\t %d \t %d\n", ak_i, ak_j);
    }
    return 0;
}
