#include <stdio.h>
int main(void)
{
    printf("\nAmeya Kale\n");
    int ak_i, ak_j;
    printf("\nPrinting Digits 10 to 1 and 100 to 10\n");
    for (ak_i = 10, ak_j = 100; ak_i >= 1, ak_j >= 10; ak_i--, ak_j -= 10)
    {
        printf("\t %d \t %d\n", ak_i, ak_j);
    }
    return 0;
}
