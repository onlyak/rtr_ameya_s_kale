#include <stdio.h>
int main(int argc, char *argv[])
{
    printf("\nAmeya Kale\n");
    int ak_i, ak_j;
    for (ak_i = 1; ak_i <= 9; ak_i++)
    {
        for (ak_j = 1; ak_j <= 9; ak_j++)
        {
            if (ak_j > ak_i)
            {
                break;
            }
            else
            {
                printf("* ");
            }
        }
        printf("\n");
    }
    return 0;
}
