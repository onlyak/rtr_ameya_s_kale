#include <stdio.h>
#include <conio.h>
int main(int argc, char *argv[])
{
    printf("\nAmeya Kale\n");
    int ak_i;
    char ak_ch;
    printf("Printing odd numbers from 1 to 30 for every user input, the loop will break when user will enter character 'q' or 'Q'\n");
    for (ak_i = 1; ak_i <= 30; ak_i=ak_i+2)
    {
        printf("\t%d\n", ak_i);
        ak_ch = getch();
        if (ak_ch == 'Q' || ak_ch == 'q')
        {
            break;
        }
    }
    return 0;
}
