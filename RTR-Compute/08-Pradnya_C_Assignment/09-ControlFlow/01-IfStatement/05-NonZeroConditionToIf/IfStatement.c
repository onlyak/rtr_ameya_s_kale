#include <stdio.h>
int main(void)
{
    printf("\nAmeya Kale\n");
    int ak_a;
    ak_a = 9;
    if (ak_a)
    {
        printf("\nBlock 1 is present with value:-\t%d", ak_a);
    }
    ak_a = -12;
    if (ak_a)
    {
        printf("\nBlock 2 is present with value:-\t%d", ak_a);
    }
    ak_a = 0;
    if (ak_a)
    {
        printf("\nBlock 3 is present with value:-%d", ak_a);
    }
    return 0;
}
