#include <stdio.h>
int main(void)
{
	printf("Ameya Kale\n");
	printf("Going on to next line...using \\n Escape Sequence\n\n");
	printf("Demonstrating \t Horizontal \t Tab \t using \t \\t Escape Sequence !!!\n\n");
	printf("\"This is a double quoted output\" done using \\\" \\\" Escape Sequence\n\n");
	printf("\'This is a single quoted output\' done using \\\' \\\' Escape Sequence\n\n");
	printf("Backspace turned to backspace \b Escape Sequence \\b\n\n");
	printf("\r Demonstrating Carriage Return using \\r Escape Sequence\n");
	printf("Demonstrating \r Carriage Return using \\r Escape Sequence\n");
	printf("Demonstrating Carriage \r Return using \\r Escape Sequence\n\n");
	printf("Demonstrating \x41 using \\xhh Escape Sequence\n\n"); 
	printf("Demonstrating \102 using \\ooo Escape Sequence\n\n");
	return(0);
}
