#include<stdio.h>

struct AkStruct 
{
    int ak_i;
    float ak_f;
    double ak_d;
    char ak_c;
};

union AkUnion
{
    int ak_i;
    float ak_f;
    double ak_d;
    char ak_c;
};

int main()
{
    printf("\nAmeya Kale\n");
    struct AkStruct ak_struct;
    union AkUnion ak_union;

    printf("\nSize of Structure: %lu",sizeof(ak_struct));
    printf("\nSize of Union: %lu",sizeof(ak_union));
    return 0;
}