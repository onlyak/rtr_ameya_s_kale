#include <stdio.h>

union AkUnion {
    int ak_i;
    float ak_f;
    double ak_d;
    char ak_c;
};

int main()
{
    printf("\nAmeya Kale\n");
    union AkUnion ak_u;
    ak_u.ak_d = 72.2727;
    ak_u.ak_c = 'A';
    printf("\nMembers of Union: ");
    printf("\nCharacter: %c", ak_u.ak_c);
    ak_u.ak_i = 3;
    printf("\nInteger: %d", ak_u.ak_i);
    ak_u.ak_f = 6.3f;
    printf("\nFloat: %f", ak_u.ak_f);
    printf("\nDouble: %lf", ak_u.ak_d); 
    return 0;
}
