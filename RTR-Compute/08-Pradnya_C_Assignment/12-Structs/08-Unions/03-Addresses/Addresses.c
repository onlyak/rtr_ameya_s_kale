#include <stdio.h>

struct AkStruct {
    int ak_i;
    float ak_f;
    double ak_d;
    char ak_c;
};

union AkUnion {
    int ak_i;
    float ak_f;
    double ak_d;
    char ak_c;
};

int main()
{
    printf("\nAmeya Kale\n");
    union AkUnion ak_u;
    ak_u.ak_i = 3;
    ak_u.ak_f = 6.3f;
    ak_u.ak_d = 72.2727;
    ak_u.ak_c = 'A';

    printf("\nMembers of Union: ");
    printf("\nCharacter: %c", ak_u.ak_c);
    ak_u.ak_i=10;
    printf("\nInteger: %d", ak_u.ak_i);
    ak_u.ak_f=6.3f;
    printf("\nFloat: %f", ak_u.ak_f);
    printf("\nDouble: %lf", ak_u.ak_d);
    
    printf("\nAddress of Union memebers : ");
    printf("\nInteger address: %p", &ak_u.ak_i);
    printf("\nFloat address: %p", &ak_u.ak_f);
    printf("\nDouble address: %p", &ak_u.ak_d);
    printf("\nCharacter address: %p", &ak_u.ak_c);

    struct AkStruct ak_s;
    ak_s.ak_i = 9;
    ak_s.ak_f = 3.6f;
    ak_s.ak_d = 27.2727;
    ak_s.ak_c = 'B';

    printf("\nMembers of Structure: ");
    printf("\nInteger: %d", ak_s.ak_i);
    printf("\nFloat: %f", ak_s.ak_f);
    printf("\nDouble: %lf", ak_s.ak_d);
    printf("\nCharacter: %c", ak_s.ak_c);

    printf("\nAddress of Structure memebers : ");
    printf("\nInteger address: %p", &ak_s.ak_i);
    printf("\nFloat address: %p", &ak_s.ak_f);
    printf("\nDouble address: %p", &ak_s.ak_d);
    printf("\nCharacter address: %p", &ak_s.ak_c);


    return 0;
}
