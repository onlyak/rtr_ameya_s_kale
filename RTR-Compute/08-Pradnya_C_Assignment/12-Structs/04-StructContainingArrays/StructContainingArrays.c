#include<stdio.h>
#define INT_ARRAY_SIZE 5
#define FLOAT_ARRAY_SIZE 5
#define CHAR_ARRAY_SIZE 26
#define NUM_STRINGS 2
#define MAX_CHARACTERS_PER_STRING 20
#define ALPHA_BEGIN 65

struct Data_1
{
    int ak_iArray[INT_ARRAY_SIZE];
    float ak_fArray[FLOAT_ARRAY_SIZE];
};

struct Data_2
{
    char ak_cArray[CHAR_ARRAY_SIZE];
    char ak_strArray[NUM_STRINGS][MAX_CHARACTERS_PER_STRING];
};

int main()
{
    printf("\nAmeya Kale\n");
    struct Data_1 ak_data1;
    struct Data_2 ak_data2;
    int ak_i;

    ak_data1.ak_fArray[0]=0.9f;
    ak_data1.ak_fArray[1]=1.8f;
    ak_data1.ak_fArray[2]=2.7f;
    ak_data1.ak_fArray[3]=3.6f;
    ak_data1.ak_fArray[4]=4.5f;

    printf("\nEnter %d integers: ",INT_ARRAY_SIZE);
    for(ak_i=0;ak_i<INT_ARRAY_SIZE;ak_i++)
    {
        scanf("%d",&ak_data1.ak_iArray[ak_i]);
    }
    for(ak_i=0;ak_i<CHAR_ARRAY_SIZE;ak_i++)
    {
        ak_data2.ak_cArray[ak_i]=(char)(ak_i+ALPHA_BEGIN);
    }
    strcpy(ak_data2.ak_strArray[0],"Ameya");
    strcpy(ak_data2.ak_strArray[1],"Kale");

    printf("\nMembers of structure one: ");
    printf("\nInteger Array: ");
    for(ak_i=0;ak_i<INT_ARRAY_SIZE;ak_i++)
    {
        printf("%d\t",ak_data1.ak_iArray[ak_i]);
    }
    printf("\nFloat Array: ");
    for(ak_i=0;ak_i<FLOAT_ARRAY_SIZE;ak_i++)
    {
        printf("%f\t",ak_data1.ak_fArray[ak_i]);
    }
    
    printf("\nMembers of structure two: ");
    printf("\nCharacter Array: ");
    for(ak_i=0;ak_i<CHAR_ARRAY_SIZE;ak_i++)
    {
        printf("%c\t",ak_data2.ak_cArray[ak_i]);
    }
    printf("\nString Array: ");
    for(ak_i=0;ak_i<NUM_STRINGS;ak_i++)
    {
        printf("%s\t",ak_data2.ak_strArray[ak_i]);
    }
    return 0;
}