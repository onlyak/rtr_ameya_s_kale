#include <stdio.h>

struct AkData
{
    int ak_i;
    float ak_f;
    double ak_d;
    char ak_c;
};

int main()
{
    struct AkData ak_data;
    ak_data.ak_i = 99;
    ak_data.ak_f = 99.99f;
    ak_data.ak_d = 9999.9999;
    ak_data.ak_c = 73;

    printf("\nStructure values representation: ");
    printf("\n%d", ak_data.ak_i);
    printf("\n%f", ak_data.ak_f);
    printf("\n%lf", ak_data.ak_d);
    printf("\n%c", ak_data.ak_c);

    printf("\nStructure values(address) representation: ");
    printf("\n%p", &ak_data.ak_i);
    printf("\n%p", &ak_data.ak_f);
    printf("\n%p", &ak_data.ak_d);
    printf("\n%p", &ak_data.ak_c);

    printf("\nStarting address of structure: %p", &ak_data);
    return 0;
}
