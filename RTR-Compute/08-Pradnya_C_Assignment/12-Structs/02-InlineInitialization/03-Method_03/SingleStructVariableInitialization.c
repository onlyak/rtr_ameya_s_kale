#include <stdio.h>
int main()
{
    struct MyStructure
    {
        int ak_i;
        float ak_f;
        double ak_d;
    } data={3,9.6f,99.9999};

    printf("\nAmeya Kale\n");
    printf("Data Members:- \n");
    printf("\t\t\t%d\t%f\t%lf", data.ak_i, data.ak_f, data.ak_d);
    printf("\nSize of Data member:");
    printf("\t%d\t", sizeof(data.ak_i));
    printf("%d\t", sizeof(data.ak_f));
    printf("\t%d\t", sizeof(data.ak_d));
    return 0;
}