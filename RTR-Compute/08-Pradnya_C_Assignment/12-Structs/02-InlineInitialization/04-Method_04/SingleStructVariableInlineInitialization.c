#include<stdio.h>

struct MyStruc
{
    int ak_i;
    float ak_f;
    double ak_d;
    char ak_c;
};

int main()
{
    printf("\nAmeya Kale\n");
    struct MyStruc ak_struc_1={99,4.2f,66.9999,'K'};
    struct MyStruc ak_struc_2={'A',9.9f,69.999,96};
    struct MyStruc ak_struc_3={63,'B'};
    struct MyStruc ak_struc_4={81};

    printf("\nData Members of struct 1 :- ");
    printf("%d\t%f\t%lf\t%c",ak_struc_1.ak_i,ak_struc_1.ak_f,ak_struc_1.ak_d,ak_struc_1.ak_c);
    printf("\nData Members of struct 2 :- ");
    printf("%d\t%f\t%lf\t%c",ak_struc_2.ak_i,ak_struc_2.ak_f,ak_struc_2.ak_d,ak_struc_2.ak_c);
    printf("\nData Members of struct 3 :- ");
    printf("%d\t%f\t%lf\t%c",ak_struc_3.ak_i,ak_struc_3.ak_f,ak_struc_3.ak_d,ak_struc_3.ak_c);
    printf("\nData Members of struct 4 :- ");
    printf("%d\t%f\t%lf\t%c",ak_struc_4.ak_i,ak_struc_4.ak_f,ak_struc_4.ak_d,ak_struc_4.ak_c);

}