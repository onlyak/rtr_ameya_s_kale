#include <stdio.h>
int main()
{
    printf("\nAmeya Kale\n");
    int ak_length, ak_breadth, ak_area;
    struct AkRectangle
    {
        struct AkPoint
        {
            int ak_x;
            int ak_y;
        } akPoint_1, akPoint_2;
    } akRect={{63,54},{99,108}};

    ak_length = akRect.akPoint_2.ak_y - akRect.akPoint_1.ak_y;
    if (ak_length < 0)
    {
        ak_length = ak_length * -1;
    }

    ak_breadth = akRect.akPoint_2.ak_x - akRect.akPoint_1.ak_x;
    if (ak_breadth < 0)
    {
        ak_breadth = ak_breadth * -1;
    }
    ak_area = ak_length * ak_breadth;

    printf("\nLength: %d", ak_length);
    printf("\nBreadth: %d", ak_breadth);
    printf("\nArea: %d", ak_area);
    return 0;
}