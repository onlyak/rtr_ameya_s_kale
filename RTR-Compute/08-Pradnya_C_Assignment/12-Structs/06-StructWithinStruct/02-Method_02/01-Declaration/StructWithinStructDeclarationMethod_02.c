#include <stdio.h>

struct AkPoint
{
    int ak_x;
    int ak_y;
};
struct AkRectangle
{
    struct AkPoint akPoint_1, akPoint_2;
};
int main()
{
    printf("\nAmeya Kale\n");
    struct AkRectangle akRect;
    int ak_length, ak_breadth, ak_area;
    printf("\nEnter Leftmost X co-ordinate: ");
    scanf("%d", &akRect.akPoint_1.ak_x);
    printf("\nEnter Bottommost Y co-ordinate: ");
    scanf("%d", &akRect.akPoint_1.ak_y);
    printf("\nEnter Rightmost X co-ordinate: ");
    scanf("%d", &akRect.akPoint_2.ak_x);
    printf("\nEnter Topmost Y co-ordinate: ");
    scanf("%d", &akRect.akPoint_2.ak_y);

    ak_length = akRect.akPoint_2.ak_y - akRect.akPoint_1.ak_y;
    if (ak_length < 0)
    {
        ak_length = ak_length * -1;
    }

    ak_breadth = akRect.akPoint_2.ak_x - akRect.akPoint_1.ak_x;
    if (ak_breadth < 0)
    {
        ak_breadth = ak_breadth * -1;
    }
    ak_area = ak_length * ak_breadth;

    printf("\nLength: %d", ak_length);
    printf("\nBreadth: %d", ak_breadth);
    printf("\nArea: %d", ak_area);
    return 0;
}