#include <stdio.h>

struct AkNumber
{
    int ak_num;
    int ak_numTable[10];
};

struct AkNumTables
{
    struct AkNumber ak_a;
    struct AkNumber ak_b;
    struct AkNumber ak_c;
};

int main()
{
    printf("\nAmeya Kale\n");
    struct AkNumTables ak_table;
    int ak_i;
    ak_table.ak_a.ak_num = 2;
    for (ak_i = 0; ak_i < 10; ak_i++)
    {
        ak_table.ak_a.ak_numTable[ak_i] = (ak_i + 1) * ak_table.ak_a.ak_num;
    }

    printf("\nPrint table of %d:- ", ak_table.ak_a.ak_num);
    for (ak_i = 0; ak_i < 10; ak_i++)
    {
        printf("%d*%d: %d \t", ak_table.ak_a.ak_num, ak_i + 1, ak_table.ak_a.ak_numTable[ak_i]);
    }

    ak_table.ak_b.ak_num = 9;
    for (ak_i = 0; ak_i < 10; ak_i++)
    {
        ak_table.ak_b.ak_numTable[ak_i] = (ak_i + 1) * ak_table.ak_b.ak_num;
    }

    printf("\nPrint table of %d:- ", ak_table.ak_b.ak_num);
    for (ak_i = 0; ak_i < 10; ak_i++)
    {
        printf("%d*%d: %d \t", ak_table.ak_b.ak_num, ak_i + 1, ak_table.ak_b.ak_numTable[ak_i]);
    }

    ak_table.ak_c.ak_num = 7;
    for (ak_i = 0; ak_i < 10; ak_i++)
    {
        ak_table.ak_c.ak_numTable[ak_i] = (ak_i + 1) * ak_table.ak_c.ak_num;
    }

    printf("\nPrint table of %d:- ", ak_table.ak_c.ak_num);
    for (ak_i = 0; ak_i < 10; ak_i++)
    {
        printf("%d*%d: %d \t", ak_table.ak_c.ak_num, ak_i + 1, ak_table.ak_c.ak_numTable[ak_i]);
    }

    return 0;
}