#include<stdio.h>

struct AkNumber
{
    int ak_num;
    int ak_numTable[10];
};

struct AkNumTables
{
    struct AkNumber ak_n;
};

int main()
{
    printf("\nAmeya Kale\n");
    struct AkNumTables ak_tables[10];
    int ak_i,ak_j;
    for(ak_i=0;ak_i<10;ak_i++)
    {
        ak_tables[ak_i].ak_n.ak_num=(ak_i+1);
    }

    for(ak_i=0;ak_i<10;ak_i++)
    {
        printf("\nPrint Table: ");
        for(ak_j=0;ak_j<10;ak_j++)
        {
            ak_tables[ak_i].ak_n.ak_numTable[ak_j]=ak_tables[ak_i].ak_n.ak_num * (ak_j+1);
            printf("%d*%d: %d \t",ak_tables[ak_i].ak_n.ak_num,ak_j+1,ak_tables[ak_i].ak_n.ak_numTable[ak_j]);
        }
    }
    return 0;
}