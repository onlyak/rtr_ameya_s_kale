#include <stdio.h>
struct MyPoint
{
    int ak_x;
    int ak_y;
};
int main()
{
    struct MyPoint ak_PointA, ak_PointB, ak_PointC, ak_PointD;
    printf("\nAmeya Kale\n");
    ak_PointA.ak_x = 0;
    ak_PointA.ak_y = 0;

    ak_PointB.ak_x = 3;
    ak_PointB.ak_y = 6;

    ak_PointC.ak_x = 9;
    ak_PointC.ak_y = 12;

    ak_PointD.ak_x = 15;
    ak_PointD.ak_y = 18;

    printf("\nCo-ordinates (x,y) of Point A:- (%d,%d)", ak_PointA.ak_x, ak_PointA.ak_y);
    printf("\nCo-ordinates (x,y) of Point B:- (%d,%d)", ak_PointB.ak_x, ak_PointB.ak_y);
    printf("\nCo-ordinates (x,y) of Point C:- (%d,%d)", ak_PointC.ak_x, ak_PointC.ak_y);
    printf("\nCo-ordinates (x,y) of Point D:- (%d,%d)", ak_PointD.ak_x, ak_PointD.ak_y);
    return 0;
}