#include <stdio.h>
int main()
{
    struct MyStructure
    {
        int ak_i;
        float ak_f;
        double ak_d;
    } data;

    printf("\nAmeya Kale\n");
    int ak_iSize;
    int ak_fSize;
    int ak_dSize;
    int structure_data_size;

    data.ak_i = 27;
    data.ak_f = 9.9f;
    data.ak_d = 18.1818;
    printf("Data Members:- \n");
    printf("\t\t\t%d\t%f\t%lf", data.ak_i, data.ak_f, data.ak_d);

    printf("\nSize of Data member:");
    printf("\t%d\t", sizeof(data.ak_i));
    printf("%d\t", sizeof(data.ak_f));
    printf("\t%d\t", sizeof(data.ak_d));

    structure_data_size = sizeof(data);
    printf("\nSize of structure:-\t%d", structure_data_size);
    return 0;
}