#include <stdio.h>
int main()
{
    struct MyStructure
    {
        int ak_x;
        int ak_y;
    };
    struct MyStructure myPoint;
    struct Properties
    {
        int ak_quadrant;
        char ak_axisLocation[10];
    };
    struct Properties properties;

    printf("\nAmeya Kale\n");
    printf("Enter x co-ordinate: ");
    scanf("%d", &myPoint.ak_x);
    printf("Enter y co-ordinate: ");
    scanf("%d", &myPoint.ak_y);

    if (myPoint.ak_x == 0 && myPoint.ak_y == 0)
    {
        printf("\nPoint is origin");
    }
    else
    {
        if (myPoint.ak_x == 0)
        {
            if (myPoint.ak_y < 0)
            {
                strcpy(properties.ak_axisLocation, "Negative Y");
            }
            if (myPoint.ak_y > 0)
            {
                strcpy(properties.ak_axisLocation, "Positive Y");
            }

            properties.ak_quadrant = 0;
            printf("\nPoint lies on the %s axis", properties.ak_axisLocation);
        }
        else if (myPoint.ak_y == 0)
        {
            if (myPoint.ak_x < 0)
            {
                strcpy(properties.ak_axisLocation, "Negative X");
            }
            if (myPoint.ak_x > 0)
            {
                strcpy(properties.ak_axisLocation, "Positive X");
            }
            properties.ak_quadrant = 0;
            printf("\nPoint lies on the %s axis", properties.ak_axisLocation);
        }
        else
        {
            properties.ak_axisLocation[0] = '\0';
            if (myPoint.ak_x > 0 && myPoint.ak_y > 0)
            {
                properties.ak_quadrant = 1;
            }
            else if (myPoint.ak_x < 0 && myPoint.ak_y > 0)
            {
                properties.ak_quadrant = 2;
            }
            else if (myPoint.ak_x < 0 && myPoint.ak_y < 0)
            {
                properties.ak_quadrant = 3;
            }
            else
            {
                properties.ak_quadrant = 4;
            }
            printf("Point lies in the %d Quadrant", properties.ak_quadrant);
        }
    }
    return 0;
}
