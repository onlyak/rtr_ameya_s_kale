#include<stdio.h>

struct AkData
{
    int ak_i;
    float ak_f;
    double ak_d;
    char ak_c;
};

int main()
{
    struct AkData Addition(struct AkData,struct AkData,struct AkData);
    struct AkData ak_data_1,ak_data_2,ak_data_3,ak_answerData;
    printf("\nAmeya Kale\n");
    printf("\nData 1: "); 
    printf("Enter Integer: ");
    scanf("%d",&ak_data_1.ak_i);
    printf("Enter Float: ");
    scanf("%f",&ak_data_1.ak_f);
    printf("Enter Double: ");
    scanf("%lf",&ak_data_1.ak_d);
    printf("Enter Character: ");
    ak_data_1.ak_c=getch();
    printf("%c",ak_data_1.ak_c);
    
    printf("\nData 2: "); 
    printf("Enter Integer: ");
    scanf("%d",&ak_data_2.ak_i);
    printf("Enter Float: ");
    scanf("%f",&ak_data_2.ak_f);
    printf("Enter Double: ");
    scanf("%lf",&ak_data_2.ak_d);
    printf("Enter Character: ");
    ak_data_2.ak_c=getch();
    printf("%c",ak_data_2.ak_c);
    
    printf("\nData 3: "); 
    printf("Enter Integer: ");
    scanf("%d",&ak_data_3.ak_i);
    printf("Enter Float: ");
    scanf("%f",&ak_data_3.ak_f);
    printf("Enter Double: ");
    scanf("%lf",&ak_data_3.ak_d);
    printf("Enter Character: ");
    ak_data_3.ak_c=getch();
    printf("%c",ak_data_3.ak_c);

    ak_answerData=Addition(ak_data_1,ak_data_2,ak_data_3);

    printf("\nAnswer of Addition: ");
    printf("\nAddition of int: %d",ak_answerData.ak_i);
    printf("\nAddition of float: %f",ak_answerData.ak_f);
    printf("\nAddition of int: %lf",ak_answerData.ak_d);

    ak_answerData.ak_c=ak_data_1.ak_c;
    printf("\nData 1 char: %c",ak_answerData.ak_c);
    ak_answerData.ak_c=ak_data_2.ak_c;
    printf("\nData 2 char: %c",ak_answerData.ak_c);
    ak_answerData.ak_c=ak_data_3.ak_c;
    printf("\nData 3 char: %c",ak_answerData.ak_c);
    return 0;
}

struct AkData Addition(struct AkData d1,struct AkData d2,struct AkData d3)
{
    struct AkData ak_ans;

    ak_ans.ak_i=d1.ak_i+d2.ak_i+d3.ak_i;
    ak_ans.ak_f=d1.ak_f+d2.ak_f+d3.ak_f;
    ak_ans.ak_d=d1.ak_d+d2.ak_d+d3.ak_d;
    return ak_ans;
};
