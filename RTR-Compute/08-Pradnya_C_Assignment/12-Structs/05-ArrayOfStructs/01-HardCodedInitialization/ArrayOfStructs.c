#include<stdio.h>
#define NAME_LEN 100
#define M_STATUS 10

struct Emp
{
    char ak_name[NAME_LEN];
    int ak_age;
    float ak_salary;
    char ak_sex;
    char ak_mStatus[M_STATUS];
};

int main()
{
    struct Emp ak_empRecord[3];
    char ak_empAmeya[]="Ameya";
    char ak_empRakesh[]="Rakesh";
    char ak_empRohan[]="Rohan";

    int ak_i;

    strcpy(ak_empRecord[0].ak_name,ak_empAmeya);
    ak_empRecord[0].ak_age=24;
    ak_empRecord[0].ak_salary=999.0f;
    ak_empRecord[0].ak_sex='M';
    strcpy(ak_empRecord[0].ak_mStatus,"Unmarried");

    strcpy(ak_empRecord[1].ak_name,ak_empRakesh);
    ak_empRecord[1].ak_age=25;
    ak_empRecord[1].ak_salary=969.0f;
    ak_empRecord[1].ak_sex='M';
    strcpy(ak_empRecord[1].ak_mStatus,"Married");
  
    strcpy(ak_empRecord[2].ak_name,ak_empRohan);
    ak_empRecord[2].ak_age=26;
    ak_empRecord[2].ak_salary=369.0f;
    ak_empRecord[2].ak_sex='M';
    strcpy(ak_empRecord[2].ak_mStatus,"Unmarried");

    printf("\nEmployee Records: ");
    for(ak_i=0;ak_i<3;ak_i++)
    {
        printf("\nName: %s",ak_empRecord[ak_i].ak_name);
        printf("\nAge: %d",ak_empRecord[ak_i].ak_age);
        if(ak_empRecord[ak_i].ak_sex=='M')
        {
            printf("\nSex: Male");
        }
        else
        {
            printf("\nSex: Female");
        }
        printf("\nSalary: %f",ak_empRecord[ak_i].ak_salary);
        printf("\nMarital Status: %s",ak_empRecord[ak_i].ak_mStatus);
    }
    return 0;
}