#include<stdio.h>
#include<string.h>
#include<ctype.h>

#define MAX_STRING_LEN 1024

struct CharacterCount
{
    char ak_ch;
    int ak_chCount;   
} ak_charCountStruct[]={{'A',0},{'B',0},
                         {'C',0},{'D',0},
                         {'E',0},{'F',0},
                         {'G',0},{'H',0},
                         {'I',0},{'J',0},
                         {'K',0},
                         {'L',0},{'M',0},
                         {'N',0},{'O',0},
                         {'P',0},{'Q',0},
                         {'R',0},{'S',0},
                         {'T',0},{'U',0},
                         {'V',0},{'W',0},
                         {'X',0},
                         {'Y',0},{'Z',0}};

#define SIZE_OF_STRUCT sizeof(ak_charCountStruct)
#define SIZE_OF_ONE_STRUCT sizeof(ak_charCountStruct[0])
#define ELEMENTS_OF_ARRAY (SIZE_OF_STRUCT/SIZE_OF_ONE_STRUCT)

int main()
{
    printf("\nAmeya Kale\n");
    char ak_str[MAX_STRING_LEN];
    int ak_i,ak_j,ak_actualStringLength=0;
    printf("\nEnter String: ");
    gets_s(ak_str,MAX_STRING_LEN);

    ak_actualStringLength=strlen(ak_str);

    printf("\nEntered String is: ");
    printf("%s",ak_str);

    for(ak_i=0;ak_i<ak_actualStringLength;ak_i++)
    {
        for(ak_j=0;ak_j<ELEMENTS_OF_ARRAY;ak_j++)
        {
            ak_str[ak_i]=toupper(ak_str[ak_i]);
            if(ak_str[ak_i]==ak_charCountStruct[ak_j].ak_ch)
            {
                ak_charCountStruct[ak_j].ak_chCount++;
            }
        }
    }

    printf("\nNumber of character occurences: ");
    for(ak_i=0;ak_i<ELEMENTS_OF_ARRAY;ak_i++)
    {
        printf("\nCharacter %c : %d",ak_charCountStruct[ak_i].ak_ch,ak_charCountStruct[ak_i].ak_chCount);
    }
    return 0;
}