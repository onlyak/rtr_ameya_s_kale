#include <stdio.h>
int main(void)
{
    printf("Ameya Kale\n");
    int ak_i, ak_j;
    char ak_ch_01, ak_ch_02;
    int ak_a, ak_result_int;
    float ak_f, ak_result_float;
    int ak_i_explicit;
    float ak_f_explicit;
    ak_i = 72;
    ak_ch_01 = ak_i;
    printf("\nCharacter 1 = %d", ak_i);
    printf("\nCharater 1 after typecasting = %c", ak_ch_01);
    ak_ch_02 = 'K';
    ak_j = ak_ch_02;
    printf("\nCharater 2 = %c", ak_ch_02);
    printf("\nCharacter 2 after typecasting = %d", ak_j);
    ak_a = 9;
    ak_f = 3.6f;
    ak_result_float = ak_a + ak_f;
    printf("\nFloating point sum = %f",ak_result_float);
    ak_result_int = ak_a + ak_f;
    printf("\nInteger sum = %d",ak_result_int);
    ak_f_explicit = 36.09031996f;
    ak_i_explicit = (int)ak_f_explicit;
    printf("\nExplicit type casting = %f", ak_f_explicit);
    printf("\nResult after explicit type casting = %d", ak_i_explicit);
    return 0;
}
