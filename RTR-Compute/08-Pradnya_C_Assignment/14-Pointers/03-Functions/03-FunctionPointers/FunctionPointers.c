#include <stdio.h>
int AddInt(int, int);
int SubInt(int, int);
float AddFloat(float, float);

typedef int (*AddIntFnPtr)(int, int);
typedef float (*AddFloatFnPtr)(float, float);

int main()
{
    printf("\nAmeya Kale\n");
    AddIntFnPtr ak_AddInt = NULL;
    AddIntFnPtr ak_ptrFunc = NULL;
    AddFloatFnPtr ak_AddFloat = NULL;

    int ak_intAns = 0;
    float ak_floatAns = 0.0f;

    ak_AddInt = AddInt;
    ak_intAns = ak_AddInt(9, 18);
    printf("\nint + int, Answer: %d",ak_intAns);

    ak_ptrFunc = SubInt;
    ak_intAns = ak_ptrFunc(27, 18);
    printf("\nint - int, Answer: %d",ak_intAns);

    ak_AddFloat = AddFloat;
    ak_floatAns = ak_AddFloat(36.99f, 45.99f);    
    printf("\nfloat + float, Answer: %lf",ak_floatAns);


    return 0;
}

int AddInt(int ak_a, int ak_b)
{
    int ak_c = ak_a + ak_b;
    return ak_c;
}

int SubInt(int ak_a, int ak_b)
{
    int ak_c;
    if (ak_a > ak_b)
    {
        ak_c = ak_b - ak_a;
    }
    else
    {
        ak_c = ak_a - ak_b;
    }
    return ak_c;
}

float AddFloat(float ak_a, float ak_b)
{
    float ak_c = ak_a + ak_b;
    return ak_c;
}