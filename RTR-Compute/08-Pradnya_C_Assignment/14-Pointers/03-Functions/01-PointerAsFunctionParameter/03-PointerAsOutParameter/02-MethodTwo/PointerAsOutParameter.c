#include <stdio.h>
#include <stdlib.h>

void MathOperations(int, int, int *, int *, int *, int *, int *);
int main()
{
    printf("\nAmeya Kale\n");
    int ak_a, ak_b;
    int *ans_add = NULL;
    int *ans_diff = NULL;
    int *ans_prod = NULL;
    int *ans_rem = NULL;
    int *ans_quo = NULL;

    printf("\nEnter integer value: ");
    scanf("%d", &ak_a);
    printf("\nEnter integer value: ");
    scanf("%d", &ak_b);

    ans_add = (int *)malloc(1 * sizeof(int));
    ans_diff = (int *)malloc(1 * sizeof(int));
    ans_prod = (int *)malloc(1 * sizeof(int));
    ans_rem = (int *)malloc(1 * sizeof(int));
    ans_quo = (int *)malloc(1 * sizeof(int));

    if (ans_quo == NULL || ans_add == NULL || ans_diff == NULL || ans_prod == NULL || ans_rem == NULL)
    {
        printf("\nMemory allocation failed");
        exit(0);
    }

    MathOperations(ak_a, ak_b, ans_add, ans_diff, ans_prod, ans_quo, ans_rem);
    printf("\nResult: ");
    printf("\nSum: %d", *ans_add);
    printf("\nDifference: %d", *ans_diff);
    printf("\nProduct: %d", *ans_prod);
    printf("\nQuotient: %d", *ans_quo);
    printf("\nRemainder: %d", *ans_rem);

    if (ans_rem)
    {
        free(ans_rem);
        ans_rem = NULL;
        printf("\nMemory freed");
    }

    if (ans_quo)
    {
        free(ans_quo);
        ans_quo = NULL;
        printf("\nMemory freed");
    }
    if (ans_prod)
    {
        free(ans_prod);
        ans_prod = NULL;
        printf("\nMemory freed");
    }
    if (ans_diff)
    {
        free(ans_diff);
        ans_diff = NULL;
        printf("\nMemory freed");
    }
    if (ans_add)
    {
        free(ans_add);
        ans_add = NULL;
        printf("\nMemory freed");
    }
}

void MathOperations(int ak_x, int ak_y, int *ans_add, int *ans_diff, int *ans_prod, int *ans_quo, int *ans_rem)
{
    *ans_add = ak_x + ak_y;
    *ans_diff = ak_x - ak_y;
    *ans_prod = ak_x * ak_y;
    *ans_quo = ak_x / ak_y;
    *ans_rem = ak_x % ak_y;
}