#include<stdio.h>
void MathOperations(int,int,int*,int*,int*,int*,int*);
int main()
{
    printf("\nAmeya Kale\n");
    int ak_a,ak_b;
    int ak_answerSum,ak_answerDiff,ak_answerProd,ak_answerQuo;
    int ak_answerRem;

    printf("\nEnter value 1:  ");
    scanf("%d",&ak_a);
    printf("\nEnter value 2:  ");
    scanf("%d",&ak_b);  

    MathOperations(ak_a,ak_b,&ak_answerSum,&ak_answerDiff,&ak_answerProd,&ak_answerQuo,&ak_answerRem);
    printf("\nResult: ");
    printf("\nSum: %d",ak_answerSum);
    printf("\nDifference: %d",ak_answerDiff);
    printf("\nProduct: %d",ak_answerProd);
    printf("\nQuotient: %d",ak_answerQuo);
    printf("\nRemainder: %d",ak_answerRem);
    return 0;
}

void MathOperations(int ak_x,int ak_y,int *ak_sum,int *ak_difference,int *ak_product,int *ak_quotient,int *ak_remainder)
{
    *ak_sum=ak_x+ak_y;
    *ak_difference=ak_x-ak_y;
    *ak_product=ak_x*ak_y;
    *ak_quotient=ak_x/ak_y;
    *ak_remainder=ak_x%ak_y;
}