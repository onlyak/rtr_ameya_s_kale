#include<stdio.h>

enum
{
    NEGATIVE = -1,
    ZERO, 
    POSITIVE
};

int AkDiff(int,int,int*);
int main()
{
    int ak_a,ak_b,ak_answer,ak_ret;
    printf("\nAmeya Kale\n");
    printf("\nEnter value 1:  ");
    scanf("%d",&ak_a);
    printf("\nEnter value 2:  ");
    scanf("%d",&ak_b);

    ak_ret=AkDiff(ak_a,ak_b,&ak_answer);
    printf("\nDifference: ");
    if(ak_ret == POSITIVE)
    {
        printf("\nDifference is positive");
    }
    else if(ak_ret == NEGATIVE)
    {
        printf("\nDifference is negative");
    }
    else
    {
        printf("\nDifference is zero");
    }
    return 0;
}

int AkDiff(int ak_x,int ak_y,int *ak_diff)
{
    *ak_diff=ak_x-ak_y;

    if(*ak_diff>0)
    {
        return POSITIVE;
    }
    else if (*ak_diff<0)
    {
        return NEGATIVE;
    }
    else
    {
        return ZERO;
    }
}