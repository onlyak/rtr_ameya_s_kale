#include<stdio.h>
#include<stdlib.h>
void MultiplyArrayNo(int *,int, int);
int main()
{
    printf("\nAmeya Kale\n");
    int *ak_Array=NULL;
    int ak_Elements,ak_i,ak_num;
    printf("\nEnter how many integers you want in the array: ");
    scanf("%d",&ak_Elements);
    ak_Array=(int *)malloc(ak_Elements*sizeof(int));
    if(ak_Array==NULL)
    {
        printf("\nMemory not allocated");
        exit(0);
    }
    printf("\nEnter %d integers: ",ak_Elements);
    for(ak_i=0;ak_i<ak_Elements;ak_i++)
    {
        scanf("%d",&ak_Array[ak_i]);
    }
    printf("\nOriginal Array: ");
    for(ak_i=0;ak_i<ak_Elements;ak_i++)
    {
        printf("\nArray Elements[%d]: %d",ak_i,ak_Array[ak_i]);
    }
    printf("\nMultiplication factor: ");
    scanf("%d",&ak_num);

    MultiplyArrayNo(ak_Array,ak_Elements,ak_num);

    printf("\nOriginal Array after multiplication: ");
    for(ak_i=0;ak_i<ak_Elements;ak_i++)
    {
        printf("\nArray Elements[%d]: %d",ak_i,ak_Array[ak_i]);
    }
    if(ak_Array)
    {
        free(ak_Array);
        ak_Array=NULL;
        printf("\nMemory freed");
    }
    return 0;
}

void MultiplyArrayNo(int *ak_arr, int ak_ele, int ak_num)
{
    int ak_i;
    for(ak_i=0;ak_i<ak_ele;ak_i++)
    {
        ak_arr[ak_i]=ak_arr[ak_i]*ak_num;
    }
}