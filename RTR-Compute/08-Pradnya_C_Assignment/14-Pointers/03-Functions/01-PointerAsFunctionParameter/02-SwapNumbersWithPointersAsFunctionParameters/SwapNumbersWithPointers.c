#include<stdio.h>
void SwapNos(int *,int *);
int main()
{
    printf("\nAmeya Kale\n");
    int ak_a,ak_b;
    printf("\nEnter value 1:  ");
    scanf("%d",&ak_a);
    printf("\nEnter value 2:  ");
    scanf("%d",&ak_b);
    printf("\nBefore Swapping: ");
    printf("\nValue 1: %d",ak_a);
    printf("\nValue 2: %d",ak_b);

    SwapNos(&ak_a,&ak_b);

    printf("\nAfter Swapping: ");
    printf("\nValue 1: %d",ak_a);
    printf("\nValue 2: %d",ak_b);
    return 0;
}

void SwapNos(int *ak_a,int *ak_b)
{
    int ak_temp;
    printf("\nBefore Swapping in function: ");
    printf("\nValue 1: %d",*ak_a);
    printf("\nValue 2: %d",*ak_b);
    ak_temp=*ak_a;
    *ak_a=*ak_b;
    *ak_b=ak_temp;
    printf("\nAfter Swapping in function: ");
    printf("\nValue 1: %d",*ak_a);
    printf("\nValue 2: %d",*ak_b);
}