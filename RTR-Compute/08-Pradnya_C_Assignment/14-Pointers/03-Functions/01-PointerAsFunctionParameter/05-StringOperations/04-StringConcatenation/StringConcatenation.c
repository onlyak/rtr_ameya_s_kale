#include <stdio.h>
#include <stdlib.h>
#define MAX_STR_LEN 512
void MyStrCat(char *, char *);
int MyStrlen(char *);

int main()
{
    printf("\nAmeya Kale\n");
    char *ak_str1 = NULL;
    char *ak_str2 = NULL;
    int ak_str_len = 0;

    ak_str1 = (char *)malloc(MAX_STR_LEN * sizeof(char));
    if (ak_str1 == NULL)
    {
        printf("\nMemory not allocated");
        exit(0);
    }
    printf("\nEnter string: ");
    gets_s(ak_str1, MAX_STR_LEN);
    ak_str_len = MyStrlen(ak_str1);

    ak_str2 = (char *)malloc(ak_str_len * sizeof(char));
    if (ak_str2 == NULL)
    {
        printf("\nMemory not allocated");
        exit(0);
    }
    printf("\nEnter string: ");
    gets_s(ak_str2, MAX_STR_LEN);
    printf("\nBefore Concatenation");
    printf("\nStr 1: %s", ak_str1);
    printf("\nStr 2: %s", ak_str2);

    MyStrCat(ak_str1, ak_str2);

    printf("\nAfter Concatenation");
    printf("\nStr 1: %s", ak_str1);
    printf("\nStr 2: %s", ak_str2);

    if (ak_str1)
    {
        free(ak_str1);
        ak_str1 = NULL;
        printf("\nMemory Freed");
    }

    if (ak_str2)
    {
        free(ak_str2);
        ak_str2 = NULL;
        printf("\nMemory Freed");
    }

    return 0;
}

void MyStrCat(char *ak_dest, char *ak_src)
{
    int MyStrlen(char *);
    int ak_i, ak_j;
    int len_dest = 0, len_src = 0;

    len_dest = MyStrlen(ak_src);
    len_src = MyStrlen(ak_dest);

    for (ak_i = len_dest, ak_j = 0; ak_j < len_src; ak_i++, ak_j++)
    {
        *(ak_dest + ak_i) = *(ak_src + ak_j);
    }
    *(ak_src + ak_i) = '\0';
}

int MyStrlen(char *ak_str)
{
    int ak_i;
    int ak_strlen = 0;
    for (ak_i = 0; ak_i < MAX_STR_LEN; ak_i++)
    {
        if (*(ak_str + ak_i) == '\0')
        {
            break;
        }
        else
        {
            ak_strlen++;
        }
    }
    return ak_strlen;
}
