#include <stdio.h>
#include <stdlib.h>
#define MAX_STR_LEN 512
void MyStrCpy(char *, char *);
int MyStrlen(char *);

int main()
{
    printf("\nAmeya Kale\n");
    char *ak_orgArray = NULL;
    char *ak_copyArray = NULL;
    int ak_str_len = 0;

    ak_orgArray = (char *)malloc(MAX_STR_LEN * sizeof(char));
    if (ak_orgArray == NULL)
    {
        printf("\nMemory not allocated");
        exit(0);
    }
    printf("\nEnter string: ");
    gets_s(ak_orgArray, MAX_STR_LEN);
    ak_str_len = MyStrlen(ak_orgArray);
    ak_copyArray = (char *)malloc(ak_str_len * sizeof(char));
    MyStrCpy(ak_copyArray, ak_orgArray);

    printf("\nOriginal String: %s", ak_orgArray);
    printf("\nCopied String: %s", ak_copyArray);

    if (ak_copyArray)
    {
        free(ak_copyArray);
        ak_copyArray = NULL;
        printf("\nMemory Freed");
    }

    if (ak_orgArray)
    {
        free(ak_orgArray);
        ak_orgArray = NULL;
        printf("\nMemory Freed");
    }

    return 0;
}

void MyStrCpy(char *ak_str1, char *ak_str2)
{
    int MyStrlen(char *);

    int ak_len = 0;
    int ak_i;

    ak_len = MyStrlen(ak_str2);
    for (ak_i = 0; ak_i < ak_len; ak_i++)
    {
        *(ak_str1 + ak_i) = *(ak_str2 + ak_i);
    }
    *(ak_str1 + ak_i) = '\0';
}

int MyStrlen(char *ak_str)
{
    int ak_i;
    int ak_strlen = 0;
    for (ak_i = 0; ak_i < MAX_STR_LEN; ak_i++)
    {
        if (*(ak_str + ak_i) == '\0')
        {
            break;
        }
        else
        {
            ak_strlen++;
        }
    }
    return ak_strlen;
}
