#include <stdio.h>
#include <stdlib.h>
#define MAX_STR_LEN 512
int MyStrlen(char *);

int main()
{
    printf("\nAmeya Kale\n");
    char *ak_chArray = NULL;
    int ak_str_len = 0;

    ak_chArray = (char *)malloc(MAX_STR_LEN * sizeof(char));
    if (ak_chArray == NULL)
    {
        printf("\nMemory not allocated");
        exit(0);
    }
    printf("\nEnter string: ");
    gets_s(ak_chArray, MAX_STR_LEN);
    printf("\nString is: ");
    printf("%s",ak_chArray);
    ak_str_len = MyStrlen(ak_chArray);
    printf("\nLength of string: %d", ak_str_len);

    if (ak_chArray)
    {
        free(ak_chArray);
        ak_chArray = NULL;
    }
    return 0;
}

int MyStrlen(char *ak_str)
{
    int ak_i;
    int ak_strlen = 0;
    for (ak_i = 0; ak_i < MAX_STR_LEN; ak_i++)
    {
        if (*(ak_str+ak_i) == '\0')
        {
            break;
        }
        else
        {
            ak_strlen++;
        }
    }
    return ak_strlen;
}