#include <stdio.h>
#include <stdlib.h>
#define MAX_STR_LEN 512
void MyStrRev(char *, char *);
int MyStrlen(char *);

int main()
{
    printf("\nAmeya Kale\n");
    char *ak_orgArray = NULL;
    char *ak_revArray = NULL;
    int ak_str_len = 0;

    ak_orgArray = (char *)malloc(MAX_STR_LEN * sizeof(char));
    if (ak_orgArray == NULL)
    {
        printf("\nMemory not allocated");
        exit(0);
    }
    printf("\nEnter string: ");
    gets_s(ak_orgArray, MAX_STR_LEN);
    ak_str_len = MyStrlen(ak_orgArray);
    ak_revArray = (char *)malloc(ak_str_len * sizeof(char));
    MyStrRev(ak_revArray, ak_orgArray);

    printf("\nOriginal String: %s", ak_orgArray);
    printf("\nReversed String: %s", ak_revArray);

    if (ak_revArray)
    {
        free(ak_revArray);
        ak_revArray = NULL;
        printf("\nMemory Freed");
    }

    if (ak_orgArray)
    {
        free(ak_orgArray);
        ak_orgArray = NULL;
        printf("\nMemory Freed");
    }

    return 0;
}

void MyStrRev(char *ak_dest, char *ak_src)
{
    int MyStrlen(char *);

    int ak_len = 0;
    int ak_i, ak_j, ak_orgLen;

    ak_len = MyStrlen(ak_src);
    ak_orgLen = ak_len - 1;
    for (ak_i = 0, ak_j = ak_orgLen; ak_i < ak_len, ak_j >= 0; ak_i++, ak_j--)
    {
        *(ak_dest + ak_i) = *(ak_src + ak_j);
    }
    *(ak_src + ak_i) = '\0';
}

int MyStrlen(char *ak_str)
{
    int ak_i;
    int ak_strlen = 0;
    for (ak_i = 0; ak_i < MAX_STR_LEN; ak_i++)
    {
        if (*(ak_str + ak_i) == '\0')
        {
            break;
        }
        else
        {
            ak_strlen++;
        }
    }
    return ak_strlen;
}
