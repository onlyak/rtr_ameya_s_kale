#include <stdio.h>
#include <stdlib.h>
#define MAX_LEN 512
char* ReplaceVowels(char *);

int main()
{
    printf("\nAmeya Kale\n");
    char ak_str[MAX_LEN];
    char *ak_replacedStr=NULL;

    printf("\nEnter String: ");
    gets_s(ak_str,MAX_LEN);

    ak_replacedStr=ReplaceVowels(ak_str);
    if(ak_replacedStr==NULL)
    {
        printf("\nExiting now");
        exit(0);
    }
    printf("\nReplaced String: %s",ak_replacedStr);
    if(ak_replacedStr)
    {
        free(ak_replacedStr);
        ak_replacedStr=NULL;
        printf("\nMemory Freed");
    }
    return 0;
}

char* ReplaceVowels(char *ak_s)
{
    void MyStrCpy(char *,char *);
    int MyStrlen(char *);

    char *ak_newStr=NULL;
    int ak_i;
    ak_newStr=(char *)malloc(MyStrlen(ak_s)*sizeof(char));
    if(ak_newStr==NULL)
    {
        printf("\nMemory not allocated");
        return NULL;
    }
    MyStrCpy(ak_newStr,ak_s);

    for(ak_i=0;ak_i<MyStrlen(ak_newStr);ak_i++)
    {
        switch(ak_newStr[ak_i])
        {
            case 'A':
            case 'a':
            case 'E':
            case 'e':
            case 'I':
            case 'i':
            case 'O':
            case 'o':
            case 'U':
            case 'u':
                ak_newStr[ak_i]='#';
                break;
            default:
                break;            
        }
    }
    return ak_newStr;
}

void MyStrCpy(char *ak_str1, char *ak_str2)
{
    int MyStrlen(char *);

    int ak_len = 0;
    int ak_i;

    ak_len = MyStrlen(ak_str2);
    for (ak_i = 0; ak_i < ak_len; ak_i++)
    {
        *(ak_str1 + ak_i) = *(ak_str2 + ak_i);
    }
    *(ak_str1 + ak_i) = '\0';
}

int MyStrlen(char *ak_str)
{
    int ak_i;
    int ak_strlen = 0;
    for (ak_i = 0; ak_i < MAX_LEN; ak_i++)
    {
        if (*(ak_str + ak_i) == '\0')
        {
            break;
        }
        else
        {
            ak_strlen++;
        }
    }
    return ak_strlen;
}
