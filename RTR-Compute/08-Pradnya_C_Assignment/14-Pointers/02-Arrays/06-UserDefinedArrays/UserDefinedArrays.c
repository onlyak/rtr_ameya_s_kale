#include <stdio.h>
#include <stdlib.h>

#define INT_SIZE sizeof(int)
#define FLOAT_SIZE sizeof(float)
#define DOUBLE_SIZE sizeof(double)
#define CHAR_SIZE sizeof(char)

int main()
{
    printf("\nAmeya Kale\n");
    int *ak_ptrIArray = NULL;
    float *ak_ptrFArray = NULL;
    double *ak_ptrDArray = NULL;
    char *ak_ptrCArray = NULL;
    unsigned int ak_intArrayLen = 0;
    unsigned int ak_floatArrayLen = 0;
    unsigned int ak_doubleArrayLen = 0;
    unsigned int ak_charArrayLen = 0;
    int ak_i;

    printf("\nEnter Size for Int Array: ");
    scanf("%u", &ak_intArrayLen);
    ak_ptrIArray = (int *)malloc(INT_SIZE * ak_intArrayLen);
    if (ak_ptrIArray == NULL)
    {
        printf("\nMemory is not allocated");
        exit(0);
    }
    printf("\nEnter elements in %d integer array: ", ak_intArrayLen);
    for (ak_i = 0; ak_i < ak_intArrayLen; ak_i++)
    {
        scanf("%d", ak_ptrIArray + ak_i);
    }

    printf("\nEnter Size for Float Array: ");
    scanf("%u", &ak_floatArrayLen);
    ak_ptrFArray = (float *)malloc(FLOAT_SIZE * ak_floatArrayLen);
    if (ak_ptrFArray == NULL)
    {
        printf("\nMemory is not allocated");
        exit(0);
    }
    printf("\nEnter elements in %d float array: ", ak_floatArrayLen);
    for (ak_i = 0; ak_i < ak_floatArrayLen; ak_i++)
    {
        scanf("%f", (ak_ptrFArray + ak_i));
    }

    printf("\nEnter Size for Double Array: ");
    scanf("%u", &ak_doubleArrayLen);
    ak_ptrDArray = (double *)malloc(DOUBLE_SIZE * ak_doubleArrayLen);
    if (ak_ptrDArray == NULL)
    {
        printf("\nMemory is not allocated");
        exit(0);
    }
    printf("\nEnter elements in %d double array: ", ak_doubleArrayLen);
    for (ak_i = 0; ak_i < ak_doubleArrayLen; ak_i++)
    {
        scanf("%lf", ak_ptrDArray + ak_i);
    }

    printf("\nEnter Size for Char Array: ");
    scanf("%d", &ak_charArrayLen);
    ak_ptrCArray = (char *)malloc(CHAR_SIZE * ak_charArrayLen);
    if (ak_ptrCArray == NULL)
    {
        printf("\nMemory is not allocated");
        exit(0);
    }
    printf("\nEnter elements in %d char array: ", ak_charArrayLen);
    for (ak_i = 0; ak_i < ak_charArrayLen; ak_i++)
    {
        *(ak_ptrCArray + ak_i) = getch();
        printf("\n%c", *(ak_ptrCArray + ak_i));
    }

    printf("\nInteger Array: ");
    for (ak_i = 0; ak_i < ak_intArrayLen; ak_i++)
    {
        printf("Number: %d Address: %p", ak_ptrIArray[ak_i], &ak_ptrIArray[ak_i]);
    }

    printf("\nFloat Array: ");
    for (ak_i = 0; ak_i < ak_floatArrayLen; ak_i++)
    {
        printf("Number: %f Address: %p", ak_ptrFArray[ak_i], &ak_ptrFArray[ak_i]);
    }

    printf("\nDouble Array: ");
    for (ak_i = 0; ak_i < ak_doubleArrayLen; ak_i++)
    {
        printf("Number: %lf Address: %p", ak_ptrDArray[ak_i], &ak_ptrDArray[ak_i]);
    }

    printf("\nCharacter Array: ");
    for (ak_i = 0; ak_i < ak_charArrayLen; ak_i++)
    {
        printf("Number: %c Address: %p", ak_ptrCArray[ak_i], &ak_ptrCArray[ak_i]);
    }

    if (ak_ptrCArray)
    {
        free(ak_ptrCArray);
        ak_ptrCArray = NULL;
        printf("\nMemory freed");
    }
    if (ak_ptrDArray)
    {
        free(ak_ptrDArray);
        ak_ptrDArray = NULL;
        printf("\nMemory freed");
    }
    if (ak_ptrFArray)
    {
        free(ak_ptrFArray);
        ak_ptrFArray = NULL;
        printf("\nMemory freed");
    }
    if (ak_ptrIArray)
    {
        free(ak_ptrIArray);
        ak_ptrIArray = NULL;
        printf("\nMemory freed");
    }
}