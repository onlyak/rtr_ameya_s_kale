#include<stdio.h>

int main()
{
    printf("\nAmeya Kale\n");
    int ak_iArray[]={144,153,162,171};
    int *ak_intPtr=NULL;
    printf("\nInteger Array Elements and Address:\n");
    printf("ak_iArray[0] : %d\t at address : %p\n", *(ak_iArray + 0),ak_iArray + 0);
    printf("ak_iArray[1] : %d\t at address : %p\n", *(ak_iArray + 1),ak_iArray + 1);
    printf("ak_iArray[2] : %d\t at address : %p\n", *(ak_iArray + 2),ak_iArray + 2);
    printf("ak_iArray[3] : %d\t at address : %p\n", *(ak_iArray + 3),ak_iArray + 3);
    ak_intPtr=ak_iArray;
    printf("\nInteger Array Elements and Address using pointer:\n");
    printf("ak_intPtr[0] : %d\t at address : %p\n", ak_intPtr[0],&ak_intPtr[0]);
    printf("ak_intPtr[1] : %d\t at address : %p\n", ak_intPtr[1],&ak_intPtr[1]);
    printf("ak_intPtr[2] : %d\t at address : %p\n", ak_intPtr[2],&ak_intPtr[2]);
    printf("ak_intPtr[3] : %d\t at address : %p\n", ak_intPtr[3],&ak_intPtr[3]);
    return 0;
}