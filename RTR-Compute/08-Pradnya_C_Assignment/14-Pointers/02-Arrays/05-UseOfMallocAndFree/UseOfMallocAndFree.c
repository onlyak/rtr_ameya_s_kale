#include<stdio.h>
#include<stdlib.h>

int main()
{
    int *ak_ptrArray=NULL;
    unsigned int ak_arrayLength=0;
    int ak_i;

    printf("\nAmeya Kale\n");
    printf("\nEnter number of elements: ");
    scanf("%d",&ak_arrayLength);
    ak_ptrArray=(int *)malloc(sizeof(int)*ak_arrayLength);
    if(ak_ptrArray==NULL)
    {
        printf("\nMemory allocation failed");
        exit(0);
    }
    else
    {
        printf("\nMemory allocation successful");
        printf("\nMemory address allocated from %p to %p : ",ak_ptrArray,(ak_ptrArray+(ak_arrayLength-1)));
    }
    
    printf("\nEnter %d elements for integer array:\n",ak_arrayLength);
    for(ak_i=0;ak_i<ak_arrayLength;ak_i++)
    {
        scanf("%d",ak_ptrArray+ak_i);
    }
    
    printf("\nArray: ");
    for(ak_i=0;ak_i<ak_arrayLength;ak_i++)
    {
        printf("\nNumber: %d Address: %p",ak_ptrArray[ak_i],&ak_ptrArray[ak_i]);
    }

    if(ak_ptrArray)
    {
        free(ak_ptrArray);
        ak_ptrArray=NULL;
        printf("\nMemory has been deallocated");
    }
    return 0;
}