#include<stdio.h>

int main()
{
    printf("\nAmeya Kale\n");
    int ak_i;
    float ak_fArray[9];
    
    for(ak_i=0;ak_i<9;ak_i++)
    {
        ak_fArray[ak_i]=(float)(ak_i+1)*3.3;
    }
    printf("\nElements of array: ");
    for(ak_i=0;ak_i<9;ak_i++)
    {
        printf("\nArray[%d]: %f",ak_i,ak_fArray[ak_i]);
        printf("\tAddress: %p",&ak_fArray[ak_i]);
    }
    return 0;
}