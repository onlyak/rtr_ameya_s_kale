#include<stdio.h>

int main()
{
    printf("\nAmeya Kale\n");
    int ak_i;
    char ak_cArray[9];
    char *ak_ptrArray=NULL;
    
    for(ak_i=0;ak_i<9;ak_i++)
    {
        ak_cArray[ak_i]=(char)(ak_i+65);
    }
    ak_ptrArray=ak_cArray;
    printf("\nElements of array: ");
    for(ak_i=0;ak_i<9;ak_i++)
    {
        printf("\nArray[%d]: %c",ak_i,*(ak_ptrArray+ak_i));
        printf("\tAddress: %p",(ak_ptrArray+ak_i));
    }
    return 0;
}