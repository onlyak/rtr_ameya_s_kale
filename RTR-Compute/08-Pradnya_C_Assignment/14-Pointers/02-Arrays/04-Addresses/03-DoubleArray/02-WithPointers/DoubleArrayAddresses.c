#include<stdio.h>

int main()
{
    printf("\nAmeya Kale\n");
    int ak_i;
    double ak_dArray[9];
    double *ak_ptrArray=NULL;
    
    for(ak_i=0;ak_i<9;ak_i++)
    {
        ak_dArray[ak_i]=(double)(ak_i+1)*2.11111;
    }
    ak_ptrArray=ak_dArray;
    printf("\nElements of array: ");
    for(ak_i=0;ak_i<9;ak_i++)
    {
        printf("\nArray[%d]: %f",ak_i,*(ak_ptrArray+ak_i));
        printf("\tAddress: %p",(ak_ptrArray+ak_i));
    }
    return 0;
}