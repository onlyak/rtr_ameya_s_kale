#include<stdio.h>

int main()
{
    printf("\nAmeya Kale\n");
    int ak_i;
    double ak_dArray[9];
    
    for(ak_i=0;ak_i<9;ak_i++)
    {
        ak_dArray[ak_i]=(double)(ak_i+1)*1.1111;
    }
    printf("\nElements of array: ");
    for(ak_i=0;ak_i<9;ak_i++)
    {
        printf("\nArray[%d]: %lf",ak_i,ak_dArray[ak_i]);
        printf("\tAddress: %p",&ak_dArray[ak_i]);
    }
    return 0;
}