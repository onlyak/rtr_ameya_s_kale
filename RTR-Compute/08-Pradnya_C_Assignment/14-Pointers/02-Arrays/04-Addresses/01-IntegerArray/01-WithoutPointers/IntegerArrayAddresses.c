#include<stdio.h>

int main()
{
    printf("\nAmeya Kale\n");
    int ak_i;
    int ak_iArray[9];
    
    for(ak_i=0;ak_i<9;ak_i++)
    {
        ak_iArray[ak_i]=(ak_i+1)*2;
    }
    printf("\nElements of array: ");
    for(ak_i=0;ak_i<9;ak_i++)
    {
        printf("\nArray[%d]: %d",ak_i,ak_iArray[ak_i]);
        printf("\tAddress: %p",&ak_iArray[ak_i]);
    }
    return 0;
}