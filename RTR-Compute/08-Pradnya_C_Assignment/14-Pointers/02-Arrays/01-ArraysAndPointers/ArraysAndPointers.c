#include<stdio.h>

int main()
{
    printf("\nAmeya Kale\n");
    int ak_iArray[]={9,18,27};
    float ak_fArray[]={9.9f,8.1f,7.2f};
    double ak_dArray[]={36.3636,45.45454,54.545454};
    char ak_cArray[]={'A','K'};
    printf("\nInteger Array Elements and Address: ");
    printf("ak_iArray[0] : %d\t at address : %p\n", *(ak_iArray + 0),ak_iArray + 0);
    printf("ak_iArray[1] : %d\t at address : %p\n", *(ak_iArray + 1),ak_iArray + 1);
    printf("ak_iArray[2] : %d\t at address : %p\n", *(ak_iArray + 2),ak_iArray + 2);

    printf("\nFloat Array Elements and Address: ");
    printf("ak_fArray[0] : %f\t at address : %p\n", *(ak_fArray + 0),ak_iArray + 0);
    printf("ak_fArray[1] : %f\t at address : %p\n", *(ak_fArray + 1),ak_iArray + 1);
    printf("ak_fArray[2] : %f\t at address : %p\n", *(ak_fArray + 2),ak_iArray + 2);

    printf("\nDouble Array Elements and Address: ");
    printf("ak_dArray[0] : %lf\t at address : %p\n", *(ak_dArray + 0),ak_iArray + 0);
    printf("ak_dArray[1] : %lf\t at address : %p\n", *(ak_dArray + 1),ak_iArray + 1);
    printf("ak_dArray[2] : %lf\t at address : %p\n", *(ak_dArray + 2),ak_iArray + 2);

    printf("\nCharacter Array Elements and Address: ");
    printf("ak_cArray[0] : %c\t at address : %p\n", *(ak_cArray + 0),ak_iArray + 0);
    printf("ak_cArray[1] : %c\t at address : %p\n", *(ak_cArray + 1),ak_iArray + 1);
    return 0;
}