#include <stdio.h>
#include <stdlib.h>

void AkMyAlloc(int **ptr, unsigned int nElements);
int main()
{
    printf("\nAmeya Kale\n");
    int *akPtrIntArray = NULL;
    unsigned int nElements;
    int ak_i;

    printf("\nEnter Number of Elements: ");
    scanf("%u", &nElements);

    AkMyAlloc(&akPtrIntArray, nElements);

    printf("\nEnter %u elements to fill the array: ", nElements);
    for (ak_i = 0; ak_i < nElements; ak_i++)
    {
        scanf("%d", &akPtrIntArray[ak_i]);
    }
    printf("\nArray Elements: ");
    for (ak_i = 0; ak_i < nElements; ak_i++)
    {
        printf("%u\t", akPtrIntArray[ak_i]);
    }
    if (akPtrIntArray)
    {
        free(akPtrIntArray);
        akPtrIntArray = NULL;
        printf("\nMemory Freed");
    }
    return 0;
}

void AkMyAlloc(int **ak_ptr, unsigned int no_of_ele)
{
    *ak_ptr = (int *)malloc(no_of_ele * sizeof(int));
    if (*ak_ptr==NULL)
    {
        printf("\nFailed to allocate memory");
        exit(0);
    }
    else
    {
        printf("\nMemory has been allocated");
    }
}