#include<stdio.h>
#include<stdlib.h>

#define NUM_ROWS 4
#define NUM_COLS 4

int main()
{
    printf("\nAmeya Kale\n");
    int *ak_intArray[NUM_ROWS];
    int ak_i,ak_j;
    for(ak_i=0;ak_i<NUM_ROWS;ak_i++)
    {
        ak_intArray[ak_i]=(int *)malloc(NUM_ROWS*sizeof(int));
        if(ak_intArray[ak_i]==NULL)
        {
            printf("\nMemory not allocated");
            exit(0);
        }
        else
        {
            printf("\nMemory Allocated");
        }
    }
    for(ak_i=0;ak_i<NUM_ROWS;ak_i++)
    {
        for(ak_j=0;ak_j<NUM_COLS-ak_i;ak_j++)
        {
            ak_intArray[ak_i][ak_j]=(ak_i+1)*(ak_j+1);
            printf("\nArray[%d][%d]=%d",ak_i,ak_j,ak_intArray[ak_i][ak_j]);
        }
        printf("\n");
    }
    for(ak_i=(NUM_ROWS-1);ak_i>=0;ak_i--)
    {
        free(ak_intArray[ak_i]);
        ak_intArray[ak_i]=NULL;
        printf("\nMemory Freed");
    }
    return 0;
}