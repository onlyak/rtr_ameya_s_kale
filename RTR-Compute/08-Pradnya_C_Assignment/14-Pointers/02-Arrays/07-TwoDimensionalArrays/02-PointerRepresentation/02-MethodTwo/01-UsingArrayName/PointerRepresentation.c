#include<stdio.h>
#include<stdlib.h>

#define NUM_ROWS 3
#define NUM_COLS 4

int main()
{
    printf("\nAmeya Kale\n");
    int ak_intArray[NUM_ROWS][NUM_COLS];
    int ak_i,ak_j;
    for(ak_i=0;ak_i<NUM_ROWS;ak_i++)
    {
        for(ak_j=0;ak_j<NUM_COLS;ak_j++)
        {
            *(*(ak_intArray+ak_i)+ak_j)=(ak_i+1)*(ak_j+1); 
        }
    }
    printf("\nInteger Array Elements with Addresses: ");
    for(ak_i=0;ak_i<NUM_ROWS;ak_i++)
    {
        for(ak_j=0;ak_j<NUM_COLS;ak_j++)
        {
            printf("\nArray[%d][%d]=%d Address: %p",ak_i,ak_j,*(*(ak_intArray+ak_i)+ak_j),&ak_intArray[ak_i][ak_j]);
        }
        printf("\n");
    }
    return 0;
}