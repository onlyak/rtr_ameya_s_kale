#include <stdio.h>
#include <stdlib.h>

#define NUM_ROWS 2
#define NUM_COLS 4

int main()
{
    printf("\nAmeya Kale\n");
    int ak_i, ak_j;
    int **ak_intPArray = NULL;
    ak_intPArray = (int **)malloc(NUM_ROWS * sizeof(int));
    if (ak_intPArray == NULL)
    {
        printf("\nMemory Allocation failed.");
        exit(0);
    }
    else
    {
        printf("\nMemory Allocation Successful");
    }

    for (ak_i = 0; ak_i < NUM_ROWS; ak_i++)
    {
        ak_intPArray[ak_i] = (int *)malloc(NUM_COLS * sizeof(int));
        if (ak_intPArray == NULL)
        {
            printf("\nMemory Allocation failed.");
            exit(0);
        }
        else
        {
            printf("\nMemory Allocation Successful");
        }
    }

    for (ak_i = 0; ak_i < NUM_ROWS; ak_i++)
    {
        for (ak_j = 0; ak_j < NUM_COLS; ak_j++)
        {
            *(*(ak_intPArray + ak_i) + ak_j) = (ak_i + 1) * (ak_j + 1);
        }
    }
    printf("\nInteger Array Elements with Addresses: ");
    for (ak_i = 0; ak_i < NUM_ROWS; ak_i++)
    {
        for (ak_j = 0; ak_j < NUM_COLS; ak_j++)
        {
            printf("\nArray[%d][%d] = %d Address: %p", ak_i, ak_j, *(*(ak_intPArray + ak_i) + ak_j), (*(ak_intPArray + ak_i)));
        }
        printf("\n");
    }

    for (ak_i = (NUM_ROWS - 1); ak_i >= 0; ak_i--)
    {
        if (*(ak_intPArray + ak_i))
        {
            free(*(ak_intPArray + ak_i));
            *(ak_intPArray + ak_i) = NULL;
            printf("\nMemory Freed");
        }
    }
    if (ak_intPArray)
    {
        free(ak_intPArray);
        ak_intPArray = NULL;
        printf("\nMemory Freed");
    }
    return 0;
}