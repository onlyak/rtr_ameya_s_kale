#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf("\nAmeya Kale\n");
    int **ak_intArray = NULL;
    int ak_i, ak_j, nRows, nCols;
    printf("\nEnter number of rows: ");
    scanf("%d", &nRows);
    printf("\nEnter number of cols: ");
    scanf("%d", &nCols);
    ak_intArray = (int **)malloc(nRows * sizeof(int));
    for (ak_i = 0; ak_i < nRows; ak_i++)
    {
        ak_intArray[ak_i] = (int *)malloc(nCols * sizeof(int));
        if (ak_intArray[ak_i] == NULL)
        {
            printf("\nMemory not allocated");
            exit(0);
        }
        else
        {
            printf("\nMemory Allocated");
        }
    }
    for (ak_i = 0; ak_i < nRows; ak_i++)
    {
        for (ak_j = 0; ak_j < nCols; ak_j++)
        {
            ak_intArray[ak_i][ak_j] = (ak_i + 1) * (ak_j + 1);
            printf("\nArray[%d][%d]=%d", ak_i, ak_j, ak_intArray[ak_i][ak_j]);
        }
        printf("\n");
    }
    for (ak_i = (nRows - 1); ak_i >= 0; ak_i--)
    {
        free(ak_intArray[ak_i]);
        ak_intArray[ak_i] = NULL;
        printf("\nMemory Freed");
    }
    return 0;
}
