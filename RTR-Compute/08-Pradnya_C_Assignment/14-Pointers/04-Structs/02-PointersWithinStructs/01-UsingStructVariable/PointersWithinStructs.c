#include <stdio.h>

struct AkData
{
    int *ak_Iptr;
    int ak_i;

    float *ak_Fptr;
    float ak_f;

    double *ak_Dptr;
    double ak_d;
};

int main()
{
    printf("\nAmeya Kale\n");
    struct AkData ak_data;

    ak_data.ak_i = 18;
    ak_data.ak_Iptr = &ak_data.ak_i;

    ak_data.ak_f = 27.27f;
    ak_data.ak_Fptr = &ak_data.ak_f;

    ak_data.ak_d = 36.363636;
    ak_data.ak_Dptr = &ak_data.ak_d;

    printf("\nInteger: %d, Address: %p", *(ak_data.ak_Iptr), ak_data.ak_Iptr);
    printf("\nFloat: %f, Address: %p", *(ak_data.ak_Fptr), ak_data.ak_Fptr);
    printf("\nDouble: %lf, Address: %p", *(ak_data.ak_Dptr), ak_data.ak_Dptr);

    return 0;
}