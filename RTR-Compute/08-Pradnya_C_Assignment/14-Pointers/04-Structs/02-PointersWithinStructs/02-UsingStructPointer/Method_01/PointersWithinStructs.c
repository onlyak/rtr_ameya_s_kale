#include <stdio.h>

struct AkData
{
    int *ak_Iptr;
    int ak_i;

    float *ak_Fptr;
    float ak_f;

    double *ak_Dptr;
    double ak_d;
};

int main()
{
    printf("\nAmeya Kale\n");
    struct AkData *ak_Ptrdata=NULL;

    ak_Ptrdata=(struct AkData *)malloc(sizeof(struct AkData));
    if(ak_Ptrdata==NULL)
    {
        printf("\nMemory Not Allocated");
    }

    (*ak_Ptrdata).ak_i = 27;
    (*ak_Ptrdata).ak_Iptr = &(*ak_Ptrdata).ak_i;

    (*ak_Ptrdata).ak_f = 36.36f;
    (*ak_Ptrdata).ak_Fptr = &(*ak_Ptrdata).ak_f;

    (*ak_Ptrdata).ak_d = 45.454545;
    (*ak_Ptrdata).ak_Dptr = &(*ak_Ptrdata).ak_d;

    printf("\nInteger: %d, Address: %p", *((*ak_Ptrdata).ak_Iptr), (*ak_Ptrdata).ak_Iptr);
    printf("\nFloat: %f, Address: %p", *((*ak_Ptrdata).ak_Fptr), (*ak_Ptrdata).ak_Fptr);
    printf("\nDouble: %lf, Address: %p", *((*ak_Ptrdata).ak_Dptr), (*ak_Ptrdata).ak_Dptr);

    return 0;
}