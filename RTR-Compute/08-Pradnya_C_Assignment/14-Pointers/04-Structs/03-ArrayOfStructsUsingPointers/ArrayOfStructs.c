#include <stdio.h>
#include <ctype.h>

#define NAME 100
#define MARITAL_STATUS 10

struct AkEmployee
{
    char ak_name[NAME];
    int ak_age;
    char ak_sex;
    float ak_salary;
    char ak_maritalStatus;
};

void GetString(char[], int);

int main()
{
    printf("\nAmeya Kale\n");
    struct AkEmployee *EmpPtrRecord = NULL;
    int ak_i, ak_EmpNo;
    printf("\nEnter number of employee to fetch the details: ");
    scanf("%d", &ak_EmpNo);

    EmpPtrRecord = (struct AkEmployee *)malloc(sizeof(struct AkEmployee));
    if (EmpPtrRecord == NULL)
    {
        printf("\nMemory Not Allocated");
    }
    printf("\nEnter Employee Data");
    for (ak_i = 0; ak_i < ak_EmpNo; ak_i++)
    {
        printf("\n\nEmployee Name: ");
        GetString(EmpPtrRecord[ak_i].ak_name, NAME);

        printf("\nEmployee Age: ");
        scanf("%d",&EmpPtrRecord[ak_i].ak_age);

        printf("\nEmployee Sex: (M/F)");
        EmpPtrRecord[ak_i].ak_sex = getch();
        printf("%c",EmpPtrRecord[ak_i].ak_sex);

        printf("\nEmployee Salary: ");
        scanf("%f", &EmpPtrRecord[ak_i].ak_salary);

        printf("\nMarital Status: (Y/N)");
        EmpPtrRecord[ak_i].ak_maritalStatus = getch();
        printf("%c", EmpPtrRecord[ak_i].ak_maritalStatus);
    }

    printf("\nEmployee Records: ");
    for (ak_i = 0; ak_i < ak_EmpNo; ak_i++)
    {
        printf("\nEmployee No: %d", ak_i + 1);
        printf("\nName: %s", EmpPtrRecord[ak_i].ak_name);
        printf("\nAge: %d", EmpPtrRecord[ak_i].ak_age);

        if (EmpPtrRecord[ak_i].ak_sex == 'M')
        {
            printf("\nSex: Male");
        }
        else if (EmpPtrRecord[ak_i].ak_sex == 'F')
        {
            printf("\nSex: Female");
        }
        else
        {
            printf("\nInvalid");
        }

        printf("\nSalary: %f", EmpPtrRecord[ak_i].ak_salary);

        if (EmpPtrRecord[ak_i].ak_maritalStatus == 'Y')
        {
            printf("\nMarital Status: Married");
        }
        else if (EmpPtrRecord[ak_i].ak_maritalStatus == 'N')
        {
            printf("\nMarital Status: Unmarried");
        }
        else
        {
            printf("\nInvalid");
        }
    }

    if (EmpPtrRecord)
    {
        free(EmpPtrRecord);
        EmpPtrRecord = NULL;
        printf("\nMemory Freed");
    }
    return 0;
}

void GetString(char ak_str[], int ak_strSize)
{
    int ak_i;
    char ak_ch = '\0';

    ak_i = 0;
    do
    {
        ak_ch = getch();
        ak_str[ak_i] = ak_ch;
        printf("%c", ak_str[ak_i]);
    } while ((ak_ch != '\r') && (ak_i < ak_strSize));

    if (ak_i == ak_strSize)
    {
        ak_str[ak_i - 1] = '\0';
    }
    else
    {
        ak_str[ak_i] = '\0';
    }
}
