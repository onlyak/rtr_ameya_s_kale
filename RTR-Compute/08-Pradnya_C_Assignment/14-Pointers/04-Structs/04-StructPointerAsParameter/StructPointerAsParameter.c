#include <stdio.h>
#include <stdio.h>

struct AkData
{
    int ak_i;
    float ak_f;
    double ak_d;
};

void ChangeValues(struct AkData *);

int main()
{
    printf("\nAmeya Kale\n");
    struct AkData *ak_Ptrdata = NULL;

    ak_Ptrdata = (struct AkData *)malloc(sizeof(struct AkData));
    if (ak_Ptrdata == NULL)
    {
        printf("\nMemory Not Allocated");
    }

    ak_Ptrdata->ak_i = 9;
    ak_Ptrdata->ak_f = 18.18f;
    ak_Ptrdata->ak_d = 27.272727;

    printf("\nBefore Change Function: ");
    printf("\nInteger: %d", ak_Ptrdata->ak_i);
    printf("\nFloat: %f", ak_Ptrdata->ak_f);
    printf("\nDouble: %lf", ak_Ptrdata->ak_d);

    ChangeValues(ak_Ptrdata);

    printf("\nAfter Change Function: ");
    printf("\nInteger: %d", ak_Ptrdata->ak_i);
    printf("\nFloat: %f", ak_Ptrdata->ak_f);
    printf("\nDouble: %lf", ak_Ptrdata->ak_d);

    if (ak_Ptrdata)
    {
        free(ak_Ptrdata);
        ak_Ptrdata = NULL;
        printf("\nMemory Freed");
    }
    return 0;
}

void ChangeValues(struct AkData *ptrData)
{
    ptrData->ak_i = 18;
    ptrData->ak_f = 27.2727f;
    ptrData->ak_d = 36.363636;
}
