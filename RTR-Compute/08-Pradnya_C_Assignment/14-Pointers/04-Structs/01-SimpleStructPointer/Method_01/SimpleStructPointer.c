#include <stdio.h>

struct AkData
{
    int ak_i;
    float ak_f;
    double ak_d;
};

int main()
{
    printf("\nAmeya Kale\n");
    struct AkData *ak_pData = NULL;
    ak_pData = (struct AkData *)malloc(sizeof(struct AkData));
    if (ak_pData == NULL)
    {
        printf("\nFailed to allocate memory");
    }

    (*ak_pData).ak_i = 9;
    (*ak_pData).ak_f = 18.18f;
    (*ak_pData).ak_d = 27.272727;

    printf("\nData: ");
    printf("\nInteger: %d, Size: %d", (*ak_pData).ak_i, sizeof((*ak_pData).ak_i));
    printf("\nFloat: %f, Size: %d", (*ak_pData).ak_f, sizeof((*ak_pData).ak_f));
    printf("\nDouble: %lf, Size: %d", (*ak_pData).ak_d, sizeof((*ak_pData).ak_d));

    printf("\nSize of structure: %d", sizeof(struct AkData));
    printf("\nSize of pointer: %d", sizeof(struct AkData *));

    if (ak_pData)
    {
        free(ak_pData);
        ak_pData = NULL;
        printf("\nMemory Freed");
    }
    return 0;
}