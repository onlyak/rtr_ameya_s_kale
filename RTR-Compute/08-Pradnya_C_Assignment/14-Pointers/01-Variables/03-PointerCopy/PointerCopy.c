#include <stdio.h>
int main()
{
    printf("\nAmeya Kale\n");
    int ak_num = 36;
    int *ak_ptr = NULL;
    int *ak_copyPtr = NULL;
    ak_ptr=&ak_num;
    printf("\nBefore Copy Pointer: ");
    printf("\nValue of integer: %d", ak_num);
    printf("\nAddress of integer: %p", &ak_num);
    printf("\nValue at address: %d", *(&ak_num));
    printf("\nAddress of pointer: %p", ak_ptr);
    printf("\nValue to which pointer points: %d", *ak_ptr);
    ak_copyPtr=ak_ptr;
    printf("\nAfter Copy Pointer: ");
    printf("\nValue of integer: %d", ak_num);
    printf("\nAddress of integer: %p", &ak_num);
    printf("\nValue at address: %d", *(&ak_num));
    printf("\nAddress of pointer: %p", ak_ptr);
    printf("\nValue to which pointer points: %d", *ak_ptr);
    printf("\nAddress of copy pointer: %p", ak_copyPtr);
    printf("\nValue to which pointer(copy) points: %d", *ak_copyPtr);
    return 0;
}