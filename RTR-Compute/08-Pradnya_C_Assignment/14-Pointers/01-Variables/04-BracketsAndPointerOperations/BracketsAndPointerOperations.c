#include <stdio.h>
int main()
{
    printf("\nAmeya Kale\n");
    int ak_num = 81;
    int *ak_ptr = NULL;
    int ak_ans;
    ak_ptr = &ak_num;
    printf("\nValue of integer: %d", ak_num);
    printf("\nAddress of integer: %p", &ak_num);
    printf("\nValue at address: %d", *(&ak_num));
    printf("\nAddress of pointer: %p", ak_ptr);
    printf("\nValue to which pointer points: %d", *ak_ptr);
    printf("\nAnswer (ak_ptr+10): %p", (ak_ptr + 10));
    printf("\nAnswer *(ak_ptr+10): %d", *(ak_ptr + 10));
    printf("\nAnswer (*ak_ptr+10): %d", *ak_ptr + 10);
    ++*ak_ptr;
    printf("\nAnswer ++*ak_ptr: %d", *ak_ptr);
    *ak_ptr++;
    printf("\nAnswer *ak_ptr++: %d", *ak_ptr);
    ak_ptr = &ak_num;
    (*ak_ptr)++;
    printf("\nAnswer *(ak_ptr)++: %d", *ak_ptr);
    return 0;
}