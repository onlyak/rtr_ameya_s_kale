#include <stdio.h>

struct AkEmployee
{
    char ak_name[100];
    int ak_age;
    float ak_salary;
    char ak_sex;
    char ak_maritalStatus;
};

int main()
{
    printf("\nAmeya Kale\n");
    printf("Size of Int: %d",sizeof(int));
    printf("\t\tSize of Int*: %d",sizeof(int*));

    printf("\nSize of Float: %d",sizeof(float));
    printf("\tSize of Float*: %d",sizeof(float*));

    printf("\nSize of Double: %d",sizeof(double));
    printf("\tSize of Double*: %d",sizeof(double*));

    printf("\nSize of Char: %d",sizeof(char));
    printf("\t\tSize of Char*: %d",sizeof(char*));

    printf("\nSize of Structure: %d",sizeof(struct AkEmployee));
    printf("\tSize of Structure*: %d",sizeof(struct AkEmployee*));
    return 0;
}

