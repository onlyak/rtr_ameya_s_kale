#include <stdio.h>

int main()
{
    printf("\nAmeya Kale\n");
    char ak_num = 'K';
    char *ak_ptr = NULL;
    printf("\nBefore Pointer assignement: ");
    printf("\nValue of integer: %c", ak_num);
    printf("\nAddress of integer: %p", &ak_num);
    printf("\nValue at address: %c", *(&ak_num));

    ak_ptr = &ak_num;
    printf("\nAfter Pointer assignement: ");
    printf("\nValue of integer: %c", ak_num);
    printf("\nAddress of integer: %p", ak_ptr);
    printf("\nValue at address: %c", *ak_ptr);
    return 0;
}