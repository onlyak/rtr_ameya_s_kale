#include<stdio.h>

int main()
{
    printf("\nAmeya Kale\n");
    float ak_num=9.9f;
    float *ak_ptr=NULL;
    printf("\nBefore Pointer assignement: ");
    printf("\nValue of integer: %f",ak_num);
    printf("\nAddress of integer: %p",&ak_num);
    printf("\nValue at address: %f",*(&ak_num));

    ak_ptr=&ak_num;
    printf("\nAfter Pointer assignement: ");
    printf("\nValue of integer: %f",ak_num);
    printf("\nAddress of integer: %p",ak_ptr);
    printf("\nValue at address: %f",*ak_ptr);
    return 0;
}