#include <stdio.h>

int main()
{
    printf("\nAmeya Kale\n");
    double ak_num = 99.9999;
    double *ak_ptr = NULL;
    printf("\nBefore Pointer assignement: ");
    printf("\nValue of integer: %lf", ak_num);
    printf("\nAddress of integer: %p", &ak_num);
    printf("\nValue at address: %lf", *(&ak_num));

    ak_ptr = &ak_num;
    printf("\nAfter Pointer assignement: ");
    printf("\nValue of integer: %lf", ak_num);
    printf("\nAddress of integer: %p", ak_ptr);
    printf("\nValue at address: %lf", *ak_ptr);
    return 0;
}