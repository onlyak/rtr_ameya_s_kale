#include <stdio.h>

int main()
{
    printf("\nAmeya Kale\n");
    int ak_num = 27;
    int* ak_ptr = NULL;
    printf("\nBefore Pointer assignement: ");
    printf("\nValue of integer: %d", ak_num);
    printf("\nAddress of integer: %p", &ak_num);
    printf("\nValue at address: %d", *(&ak_num));

    ak_ptr = &ak_num;
    printf("\nAfter Pointer assignement: ");
    printf("\nValue of integer: %d", ak_num);
    printf("\nAddress of integer: %p", ak_ptr);
    printf("\nValue at address: %d", *ak_ptr);
    return 0;
}