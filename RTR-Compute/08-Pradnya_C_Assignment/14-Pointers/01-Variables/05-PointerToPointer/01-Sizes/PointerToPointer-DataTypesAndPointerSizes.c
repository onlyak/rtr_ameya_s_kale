#include <stdio.h>

struct AkEmployee
{
    char ak_name[100];
    int ak_age;
    float ak_salary;
    char ak_sex;
    char ak_maritalStatus;
};

int main()
{
    printf("\nAmeya Kale\n");
    printf("Size of Datatypes and Pointers");
    printf("\nSize of (int): %d, (int*): %d, (int**): %d", sizeof(int), sizeof(int *), sizeof(int **));
    printf("\nSize of (float): %d, (float*): %d, (float**): %d", sizeof(float), sizeof(float *), sizeof(float **));
    printf("\nSize of (char): %d, (char*): %d, (char**): %d", sizeof(char), sizeof(char *), sizeof(char **));
    printf("\nSize of (double): %d, (double*): %d, (double**): %d", sizeof(double), sizeof(double *), sizeof(double **));
    printf("\nSize of (struct AkEmployee): %d, (struct AkEmployee*): %d, (struct AkEmployee**): %d", sizeof(struct AkEmployee), sizeof(struct AkEmployee *), sizeof(struct AkEmployee **));
    return 0;
}