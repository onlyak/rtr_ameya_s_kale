#include <stdio.h>

int main()
{
    printf("\nAmeya Kale\n");
    int ak_num = 9;
    int *ak_ptr = NULL;
    int **ak_ptrPtr = NULL;

    printf("\nBefore Pointer Assignment");
    printf("\nInteger: %d", ak_num);
    printf("\nAddress: %p", &ak_num);
    printf("\nValue at Address of Integer: %d", *(&ak_num));

    ak_ptr = &ak_num;

    printf("\nAfter Pointer Assignment");
    printf("\nInteger: %d", ak_num);
    printf("\nAddress: %p", ak_ptr);
    printf("\nValue at Address of Integer: %d", *ak_ptr);

    ak_ptrPtr = &ak_ptr;

    printf("\nAfter Double Pointer Assignment");
    printf("\nInteger: %d", ak_num);
    printf("\nAddress: %p", ak_ptr);
    printf("\nValue at Address of Integer: %p", ak_ptrPtr);
    printf("\nValue at Address of Pointer (*ak_ptr): %p", *ak_ptrPtr);
    printf("\nValue at Address of Double Pointer (*ak_ptr)(*ak_ptrPtr) : %d", **ak_ptrPtr);
    return 0;
}