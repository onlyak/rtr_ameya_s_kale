//
//  AppDelegate.m
//  WindowiOS
//
//  Created by ameya kale on 03/07/21.
//

#import "AppDelegate.h"

#import "ViewController.h"

#import "GLESView.h"

@implementation AppDelegate
{
    @private
    UIWindow *window;
    ViewController *viewController;
    GLESView *view;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    CGRect win_rect = [[UIScreen mainScreen]bounds];
    window = [[UIWindow alloc]initWithFrame:win_rect];
    viewController = [[ViewController alloc]init];
    [window setRootViewController:viewController];
    
    view = [[GLESView alloc]initWithFrame:win_rect];
    [viewController setView:view];
    [view release];
    [window makeKeyAndVisible];
    [view startAnimation];
    return YES;
}

-(void)applicationWillResignActive:(UIApplication *)application
{
    [view stopAnimation];
}

-(void)applicationDidEnterBackground:(UIApplication *)application
{
    
}

-(void)applicationWillEnterForeground:(UIApplication *)application
{
    
}

-(void)applicationDidBecomeActive:(UIApplication *)application
{
    [view startAnimation];
}

-(void) applicationWillTerminate:(UIApplication *)application
{
    [view stopAnimation];
}

-(void)dealloc
{
    [view release];
    [viewController release];
    [window release];
    [super dealloc];
}

@end
