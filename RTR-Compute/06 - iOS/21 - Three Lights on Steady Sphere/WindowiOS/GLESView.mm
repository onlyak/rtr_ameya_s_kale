#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "sphere.h"
#import "vmath.h"

using namespace vmath;
GLfloat sphereAngle_ak = 0.0f;
GLfloat lightAmbient_ak[] = { 0.1f, 0.1f, 0.1f };
GLfloat lightDiffused_ak[] = { 1.0f, 1.0f, 1.0f };
GLfloat lightPosition_ak[] = { 100.0f, 100.0f, 100.0f, 1.0f };
GLfloat lightSpecular_ak[] = { 1.0f, 1.0f, 1.0f };
GLfloat materialAmbient_ak[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat materialDiffused_ak[] = { 0.5f, 0.2f, 0.7f, 1.0f };
GLfloat materialSpecular_ak[] = { 0.7f, 0.7f, 0.7f, 1.0f };
GLfloat materialShininess_ak = 50.0f;
int pvpf = 0;
struct Light
{
  GLfloat lightAmbient_ak[3];
  GLfloat lightDiffused_ak[3];
  GLfloat lightPosition_ak[4];
  GLfloat lightSpecular_ak[3];
};
struct Light lights[3];
GLfloat lightAngle0_ak;
GLfloat lightAngle1_ak = 0.0;
GLfloat lightAngle2_ak = 0.0;
bool bLight_ak = 0.0;
bool bAnimate_ak;

@implementation GLESView {
@private
  EAGLContext* eaglContext;

  GLuint defaultFramebuffer;
  GLuint colorRenderbuffer;
  GLuint depthRenderbuffer;

  id displayLink;
  NSInteger animationFrameInterval;
  BOOL isAnimating;

  enum
  {
    ATTRIBUTE_POSITION = 0,
    ATTRIBUTE_COLOR,
    ATTRIBUTE_NORMAL,
    ATTRIBUTE_TEXCOORD
  };
  GLuint gVertexShaderObject_ak;
  GLuint gFragmentShaderObject_ak;
  GLuint gShaderProgramObject_ak;
  GLuint vao_cube_ak;
  GLuint vbo_position_cube_ak;
  GLuint vbo_normal_cube_ak;
  GLuint modelViewMatrixUniform_ak;
  GLuint projectionMatrixUniform_ak;
  GLuint lKeyPressedUniform_ak;
  GLuint ldUniform_ak;
  GLuint kdUniform_ak;
  GLuint lightPositionUniform_ak;
  GLuint perVertexLighting_VertexShaderObject_ak;
  GLuint perVertexLighting_FragmentShaderObject_ak;
  GLuint perVertexLighting_ShaderProgramObject_ak;
  GLuint perVertexLighting_vao_sphere_ak;
  GLuint perVertexLighting_vbo_position_sphere_ak;
  GLuint perVertexLighting_vbo_normal_sphere_ak;
  GLuint perVertexLighting_vbo_element_sphere_ak;
  GLuint perVertexLighting_modelMatrixUniform_ak;
  GLuint perVertexLighting_viewMatrixUniform_ak;
  GLuint perVertexLighting_projectionMatrixUniform_ak;
  GLuint perVertexLighting_laUniform_ak;
  GLuint perVertexLighting_ldUniform_ak;
  GLuint perVertexLighting_lsUniform_ak;
  GLuint perVertexLighting_lightPositionUniform_ak;
  GLuint perVertexLighting_kaUniform_ak;
  GLuint perVertexLighting_kdUniform_ak;
  GLuint perVertexLighting_ksUniform_ak;
  GLuint perVertexLighting_materialShininessUniform_ak;
  GLuint perVertexLighting_lKeyPressedUniform_ak;
  GLuint perFragmentLighting_VertexShaderObject_ak;
  GLuint perFragmentLighting_FragmentShaderObject_ak;
  GLuint perFragmentLighting_ShaderProgramObject_ak;
  GLuint perFragmentLighting_vao_sphere_ak;
  GLuint perFragmentLighting_vbo_position_sphere_ak;
  GLuint perFragmentLighting_vbo_normal_sphere_ak;
  GLuint preFragmentLighting_vbo_element_sphere_ak;
  GLuint perFragmentLighting_modelMatrixUniform_ak;
  GLuint perFragmentLighting_viewMatrixUniform_ak;
  GLuint perFragmentLighting_projectionMatrixUniform_ak;
  GLuint perFragmentLighting_laUniform_ak;
  GLuint perFragmentLighting_ldUniform_ak;
  GLuint perFragmentLighting_lsUniform_ak;
  GLuint perFragmentLighting_lightPositionUniform_ak;
  GLuint perFragmentLighting_kaUniform_ak;
  GLuint perFragmentLighting_kdUniform_ak;
  GLuint perFragmentLighting_ksUniform_ak;
  GLuint perFragmentLighting_materialShininessUniform_ak;
  GLuint perFragmentLighting_lKeyPressedUniform_ak;
  float sphere_vertices_ak[1146];
  float sphere_normals_ak[1146];
  float sphere_textures_ak[764];
  unsigned short sphere_elements_ak[2280];
  int gNumVertices_ak;
  int gNumElements_ak;
  vmath::mat4 perspectiveProjectionMatrix_ak;
}

- (id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    CAEAGLLayer* eaglLayer = (CAEAGLLayer*)[super layer];

    [eaglLayer setOpaque:YES];

    [eaglLayer setDrawableProperties:[NSDictionary
                                       dictionaryWithObjectsAndKeys:
                                         [NSNumber numberWithBool:NO],
                                         kEAGLDrawablePropertyRetainedBacking,
                                         kEAGLColorFormatRGBA8,
                                         kEAGLDrawablePropertyColorFormat,
                                         nil]];

    eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
    if (eaglContext == nil) {
      printf("OpenGL-ES Context Creation Failed.\n");
      return (nil);
    }

    [EAGLContext setCurrentContext:eaglContext];

    glGenFramebuffers(1, &defaultFramebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    glGenRenderbuffers(1, &colorRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
    glFramebufferRenderbuffer(
      GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);

    GLint backingWidth;
    GLint backingHeight;
    glGetRenderbufferParameteriv(
      GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
    glGetRenderbufferParameteriv(
      GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);

    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(
      GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
    glFramebufferRenderbuffer(
      GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    printf("initWithFrame checking if framebuffer is complete\n");
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
      printf("Framebuffer is not complete\n");
      [self unintialize];
      return (nil);
    }

    printf("Renderer : %s\n", glGetString(GL_RENDERER));
    printf("GL Version : %s\n", glGetString(GL_VERSION));
    printf("GLES Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    animationFrameInterval = 60;
    isAnimating = NO;

    Sphere *sphere = new Sphere();
    sphere->getSphereVertexData(sphere_vertices_ak,
                               sphere_normals_ak,
                               sphere_textures_ak,
                               sphere_elements_ak);
    gNumVertices_ak = sphere->getNumberOfSphereVertices();
    gNumElements_ak = sphere->getNumberOfSphereElements();

    lights[0].lightAmbient_ak[0] = 0.0f;
    lights[0].lightAmbient_ak[1] = 0.0f;
    lights[0].lightAmbient_ak[2] = 0.0f;
    lights[0].lightDiffused_ak[0] = 1.0f;
    lights[0].lightDiffused_ak[1] = 0.0f;
    lights[0].lightDiffused_ak[2] = 0.0f;
    lights[0].lightPosition_ak[0] = 0.0f;
    lights[0].lightPosition_ak[1] = 0.0f;
    lights[0].lightPosition_ak[2] = 0.0f;
    lights[0].lightPosition_ak[3] = 1.0f;
    lights[0].lightSpecular_ak[0] = 1.0f;
    lights[0].lightSpecular_ak[1] = 0.0f;
    lights[0].lightSpecular_ak[2] = 0.0f;

    lights[1].lightAmbient_ak[0] = 0.0f;
    lights[1].lightAmbient_ak[1] = 0.0f;
    lights[1].lightAmbient_ak[2] = 0.0f;
    lights[1].lightDiffused_ak[0] = 0.0f;
    lights[1].lightDiffused_ak[1] = 1.0f;
    lights[1].lightDiffused_ak[2] = 0.0f;
    lights[1].lightPosition_ak[0] = 0.0f;
    lights[1].lightPosition_ak[1] = 0.0f;
    lights[1].lightPosition_ak[2] = 0.0f;
    lights[1].lightPosition_ak[3] = 1.0f;
    lights[1].lightSpecular_ak[0] = 0.0f;
    lights[1].lightSpecular_ak[1] = 1.0f;
    lights[1].lightSpecular_ak[2] = 0.0f;

    lights[2].lightAmbient_ak[0] = 0.0f;
    lights[2].lightAmbient_ak[1] = 0.0f;
    lights[2].lightAmbient_ak[2] = 0.0f;
    lights[2].lightDiffused_ak[0] = 0.0f;
    lights[2].lightDiffused_ak[1] = 0.0f;
    lights[2].lightDiffused_ak[2] = 1.0f;
    lights[2].lightPosition_ak[0] = 0.0f;
    lights[2].lightPosition_ak[1] = 0.0f;
    lights[2].lightPosition_ak[2] = 0.0f;
    lights[2].lightPosition_ak[3] = 1.0f;
    lights[2].lightSpecular_ak[0] = 0.0f;
    lights[2].lightSpecular_ak[1] = 0.0f;
    lights[2].lightSpecular_ak[2] = 1.0f;

    perVertexLighting_VertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
      "#version 300 es"
      "\n"
      "precision highp float;"
      "precision highp int;"
      "in vec4 vPosition;\n"
      "in vec3 vNormal;\n"
      "uniform mat4 u_model_matrix;\n"
      "uniform mat4 u_view_matrix;\n"
      "uniform mat4 u_projection_matrix;\n"
      "uniform vec3 u_la[3];\n"
      "uniform vec3 u_ld[3];\n"
      "uniform vec3 u_ls[3];\n"
      "uniform vec4 u_light_position[3];\n"
      "uniform vec3 u_ka;\n"
      "uniform vec3 u_kd;\n"
      "uniform vec3 u_ks;\n"
      "uniform float u_material_shininess;\n"
      "uniform int u_lkey_pressed;\n"
      "out vec3 phong_ads_light;\n"
      "void main(void)\n"
      "{\n"
      "if(u_lkey_pressed == 1)\n"
      "{\n"
      "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;\n"
      "vec3 transformed_normal = normalize(mat3((u_view_matrix * "
      "u_model_matrix)) * vNormal);\n"
      "vec3 view_vector = normalize(-eye_coordinates.xyz);\n"
      "phong_ads_light = vec3(0.0, 0.0, 0.0);\n"
      "for(int i = 0; i < 3; i++)\n"
      "{\n"
      "vec3 light_direction = normalize(vec3(u_light_position[i] - "
      "eye_coordinates));\n"
      "vec3 reflection_vector = reflect(-light_direction, "
      "transformed_normal);\n"
      "vec3 ambient = u_la[i] * u_ka;\n"
      "vec3 diffuse = u_ld[i] * u_kd * max(dot(light_direction, "
      "transformed_normal), 0.0);\n"
      "vec3 specular = u_ls[i] * u_ks * pow(max(dot(reflection_vector, "
      "view_vector), 0.0), u_material_shininess);\n"
      "phong_ads_light = phong_ads_light + ambient + diffuse + specular;\n"
      "}\n"
      "}\n"
      "else\n"
      "{\n"
      "phong_ads_light = vec3(1.0, 1.0, 1.0);\n"
      "}\n"
      "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * "
      "vPosition;\n"
      "}";
    glShaderSource(perVertexLighting_VertexShaderObject_ak,
                   1,
                   (const GLchar**)&vertexShaderSourceCode,
                   NULL);
    glCompileShader(perVertexLighting_VertexShaderObject_ak);
    GLint infoLogLength_ak = 0;
    GLint shaderCompileStatus_ak = 0;
    char* szBuffer_ak = NULL;
    glGetShaderiv(perVertexLighting_VertexShaderObject_ak,
                  GL_COMPILE_STATUS,
                  &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
      glGetShaderiv(perVertexLighting_VertexShaderObject_ak,
                    GL_INFO_LOG_LENGTH,
                    &infoLogLength_ak);
      if (infoLogLength_ak > 0) {
        szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
        if (szBuffer_ak != NULL) {
          GLsizei written_ak;
          glGetShaderInfoLog(perVertexLighting_VertexShaderObject_ak,
                             infoLogLength_ak,
                             &written_ak,
                             szBuffer_ak);
          printf("Vertex shader compilation log: %s\n", szBuffer_ak);
          free(szBuffer_ak);
          [self unintialize];
        }
      }
    }
    perVertexLighting_FragmentShaderObject_ak =
      glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar* fragmentSourceCode_ak =
      "#version 300 es"
      "\n"
      "precision highp float;"
      "precision highp int;"
      "in vec3 phong_ads_light;"
      "out vec4 FragColor;"
      "void main(void)"
      "{"
      "FragColor = vec4(phong_ads_light, 1.0);"
      "}";
    glShaderSource(perVertexLighting_FragmentShaderObject_ak,
                   1,
                   (const GLchar**)&fragmentSourceCode_ak,
                   NULL);
    glCompileShader(perVertexLighting_FragmentShaderObject_ak);
    infoLogLength_ak = 0;
    shaderCompileStatus_ak = 0;
    szBuffer_ak = NULL;
    glGetShaderiv(perVertexLighting_FragmentShaderObject_ak,
                  GL_COMPILE_STATUS,
                  &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
      glGetShaderiv(perVertexLighting_FragmentShaderObject_ak,
                    GL_INFO_LOG_LENGTH,
                    &infoLogLength_ak);
      if (infoLogLength_ak > 0) {
        szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
        if (szBuffer_ak != NULL) {
          GLsizei written_ak;
          glGetShaderInfoLog(perVertexLighting_FragmentShaderObject_ak,
                             infoLogLength_ak,
                             &written_ak,
                             szBuffer_ak);
          printf("Fragment shader compilation log: %s\n", szBuffer_ak);
          free(szBuffer_ak);
          [self unintialize];
        }
      }
    }
    perVertexLighting_ShaderProgramObject_ak = glCreateProgram();
    glAttachShader(perVertexLighting_ShaderProgramObject_ak,
                   perVertexLighting_VertexShaderObject_ak);
    glAttachShader(perVertexLighting_ShaderProgramObject_ak,
                   perVertexLighting_FragmentShaderObject_ak);
    glBindAttribLocation(perVertexLighting_ShaderProgramObject_ak,
                         ATTRIBUTE_POSITION,
                         "vPosition");
    glBindAttribLocation(perVertexLighting_ShaderProgramObject_ak,
                         ATTRIBUTE_NORMAL,
                         "vNormal");
    glLinkProgram(perVertexLighting_ShaderProgramObject_ak);
    GLint shaderProgramLinkStatus = 0;
    glGetProgramiv(perVertexLighting_ShaderProgramObject_ak,
                   GL_LINK_STATUS,
                   &shaderProgramLinkStatus);
    if (shaderProgramLinkStatus == GL_FALSE) {
      glGetProgramiv(perVertexLighting_ShaderProgramObject_ak,
                     GL_INFO_LOG_LENGTH,
                     &infoLogLength_ak);
      if (infoLogLength_ak > 0) {
        szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
        if (szBuffer_ak != NULL) {
          GLsizei written_ak;
          glGetProgramInfoLog(perVertexLighting_ShaderProgramObject_ak,
                              infoLogLength_ak,
                              &written_ak,
                              szBuffer_ak);
          printf("Shader program link log: %s\n", szBuffer_ak);
          free(szBuffer_ak);
          [self unintialize];
        }
      }
    }
    perVertexLighting_modelMatrixUniform_ak = glGetUniformLocation(
      perVertexLighting_ShaderProgramObject_ak, "u_model_matrix");
    perVertexLighting_viewMatrixUniform_ak = glGetUniformLocation(
      perVertexLighting_ShaderProgramObject_ak, "u_view_matrix");
    perVertexLighting_projectionMatrixUniform_ak = glGetUniformLocation(
      perVertexLighting_ShaderProgramObject_ak, "u_projection_matrix");
    perVertexLighting_laUniform_ak =
      glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_la");
    perVertexLighting_ldUniform_ak =
      glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_ld");
    perVertexLighting_lsUniform_ak =
      glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_ls");
    perVertexLighting_lightPositionUniform_ak = glGetUniformLocation(
      perVertexLighting_ShaderProgramObject_ak, "u_light_position");
    perVertexLighting_kaUniform_ak =
      glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_ka");
    perVertexLighting_kdUniform_ak =
      glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_kd");
    perVertexLighting_ksUniform_ak =
      glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_ks");
    perVertexLighting_materialShininessUniform_ak = glGetUniformLocation(
      perVertexLighting_ShaderProgramObject_ak, "u_material_shininess");
    perVertexLighting_lKeyPressedUniform_ak = glGetUniformLocation(
      perVertexLighting_ShaderProgramObject_ak, "u_lkey_pressed");
    glGenVertexArrays(1, &perVertexLighting_vao_sphere_ak);
    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glGenBuffers(1, &perVertexLighting_vbo_position_sphere_ak);
    glBindBuffer(GL_ARRAY_BUFFER, perVertexLighting_vbo_position_sphere_ak);
    glBufferData(GL_ARRAY_BUFFER,
                 sizeof(sphere_vertices_ak),
                 sphere_vertices_ak,
                 GL_STATIC_DRAW);
    glVertexAttribPointer(
      ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glGenBuffers(1, &perVertexLighting_vbo_normal_sphere_ak);
    glBindBuffer(GL_ARRAY_BUFFER, perVertexLighting_vbo_normal_sphere_ak);
    glBufferData(GL_ARRAY_BUFFER,
                 sizeof(sphere_normals_ak),
                 sphere_normals_ak,
                 GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glGenBuffers(1, &perVertexLighting_vbo_element_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                 perVertexLighting_vbo_element_sphere_ak);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 sizeof(sphere_elements_ak),
                 sphere_elements_ak,
                 GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    perFragmentLighting_VertexShaderObject_ak =
      glCreateShader(GL_VERTEX_SHADER);
    const GLchar* perFragmentLighting_vertexShaderSourceCode =
      "#version 300 es"
      "\n"
      "precision highp float;"
      "precision highp int;"
      "in vec4 vPosition;\n"
      "in vec3 vNormal;\n"
      "uniform mat4 u_model_matrix;\n"
      "uniform mat4 u_view_matrix;\n"
      "uniform mat4 u_projection_matrix;\n"
      "uniform int u_lkey_pressed;\n"
      "out vec4 eye_coordinates;"
      "out vec3 transformed_normal;"
      "out vec3 view_vector;"
      "void main(void)\n"
      "{\n"
      "if(u_lkey_pressed == 1)\n"
      "{\n"
      "eye_coordinates = u_view_matrix * u_model_matrix * vPosition;\n"
      "transformed_normal = normalize(mat3((u_view_matrix * u_model_matrix)) * "
      "vNormal);\n"
      "view_vector = normalize(-eye_coordinates.xyz);\n"
      "}\n"
      "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * "
      "vPosition;\n"
      "}";
    glShaderSource(perFragmentLighting_VertexShaderObject_ak,
                   1,
                   (const GLchar**)&perFragmentLighting_vertexShaderSourceCode,
                   NULL);
    glCompileShader(perFragmentLighting_VertexShaderObject_ak);
    infoLogLength_ak = 0;
    shaderCompileStatus_ak = 0;
    szBuffer_ak = NULL;
    glGetShaderiv(perFragmentLighting_VertexShaderObject_ak,
                  GL_COMPILE_STATUS,
                  &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
      glGetShaderiv(perFragmentLighting_VertexShaderObject_ak,
                    GL_INFO_LOG_LENGTH,
                    &infoLogLength_ak);
      if (infoLogLength_ak > 0) {
        szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
        if (szBuffer_ak != NULL) {
          GLsizei written_ak;
          glGetShaderInfoLog(perFragmentLighting_VertexShaderObject_ak,
                             infoLogLength_ak,
                             &written_ak,
                             szBuffer_ak);
          printf("Vertex shader compilation log: %s\n", szBuffer_ak);
          free(szBuffer_ak);
          [super unintialize];
        }
      }
    }
    perFragmentLighting_FragmentShaderObject_ak =
      glCreateShader(GL_FRAGMENT_SHADER);
    perFragmentLighting_FragmentShaderObject_ak =
      glCreateShader(GL_FRAGMENT_SHADER);
    fragmentSourceCode_ak =
      "#version 300 es"
      "\n"
      "precision highp float;"
      "precision highp int;"
      "in vec3 transformed_normal;"
      "in vec3 view_vector;"
      "in vec4 eye_coordinates;"
      "uniform vec3 u_la[3];\n"
      "uniform vec3 u_ld[3];\n"
      "uniform vec3 u_ls[3];\n"
      "uniform vec4 u_light_position[3];\n"
      "uniform vec3 u_ka;\n"
      "uniform vec3 u_kd;\n"
      "uniform vec3 u_ks;\n"
      "uniform float u_material_shininess;\n"
      "uniform int u_lkey_pressed;"
      "out vec4 FragColor;"
      "void main(void)"
      "{"
      "vec3 phong_ads_light = vec3(0.0, 0.0, 0.0);\n"
      "if(u_lkey_pressed == 1)\n"
      "{\n"
      "for(int i = 0; i < 3; i++)\n"
      "{\n"
      "vec3 light_direction = normalize(vec3(u_light_position[i] - "
      "eye_coordinates));\n"
      "vec3 reflection_vector = reflect(-light_direction, "
      "transformed_normal);\n"
      "vec3 ambient = u_la[i] * u_ka;\n"
      "vec3 diffuse = u_ld[i] * u_kd * max(dot(light_direction, "
      "transformed_normal), 0.0);\n"
      "vec3 specular = u_ls[i] * u_ks * pow(max(dot(reflection_vector, "
      "view_vector), 0.0), u_material_shininess);\n"
      "phong_ads_light = phong_ads_light + ambient + diffuse + specular;\n"
      "}\n"
      "}\n"
      "else"
      "{"
      "phong_ads_light = vec3(1.0, 1.0, 1.0);"
      "}"
      "FragColor = vec4(phong_ads_light, 1.0);"
      "}";
    glShaderSource(perFragmentLighting_FragmentShaderObject_ak,
                   1,
                   (const GLchar**)&fragmentSourceCode_ak,
                   NULL);
    glCompileShader(perFragmentLighting_FragmentShaderObject_ak);
    infoLogLength_ak = 0;
    shaderCompileStatus_ak = 0;
    szBuffer_ak = NULL;
    glGetShaderiv(perFragmentLighting_FragmentShaderObject_ak,
                  GL_COMPILE_STATUS,
                  &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
      glGetShaderiv(perFragmentLighting_FragmentShaderObject_ak,
                    GL_INFO_LOG_LENGTH,
                    &infoLogLength_ak);
      if (infoLogLength_ak > 0) {
        szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
        if (szBuffer_ak != NULL) {
          GLsizei written_ak;
          glGetShaderInfoLog(perFragmentLighting_FragmentShaderObject_ak,
                             infoLogLength_ak,
                             &written_ak,
                             szBuffer_ak);
          printf("Fragment shader compilation log: %s\n", szBuffer_ak);
          free(szBuffer_ak);
          [self unintialize];
        }
      }
    }
    perFragmentLighting_ShaderProgramObject_ak = glCreateProgram();
    glAttachShader(perFragmentLighting_ShaderProgramObject_ak,
                   perFragmentLighting_VertexShaderObject_ak);
    glAttachShader(perFragmentLighting_ShaderProgramObject_ak,
                   perFragmentLighting_FragmentShaderObject_ak);
    glBindAttribLocation(perFragmentLighting_ShaderProgramObject_ak,
                         ATTRIBUTE_POSITION,
                         "vPosition");
    glBindAttribLocation(perFragmentLighting_ShaderProgramObject_ak,
                         ATTRIBUTE_NORMAL,
                         "vNormal");
    glLinkProgram(perFragmentLighting_ShaderProgramObject_ak);
    shaderProgramLinkStatus = 0;
    glGetProgramiv(perFragmentLighting_ShaderProgramObject_ak,
                   GL_LINK_STATUS,
                   &shaderProgramLinkStatus);
    if (shaderProgramLinkStatus == GL_FALSE) {
      glGetProgramiv(perFragmentLighting_ShaderProgramObject_ak,
                     GL_INFO_LOG_LENGTH,
                     &infoLogLength_ak);
      if (infoLogLength_ak > 0) {
        szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
        if (szBuffer_ak != NULL) {
          GLsizei written_ak;
          glGetProgramInfoLog(perFragmentLighting_ShaderProgramObject_ak,
                              infoLogLength_ak,
                              &written_ak,
                              szBuffer_ak);
          printf("Shader program link log: %s\n", szBuffer_ak);
          free(szBuffer_ak);
          [self unintialize];
        }
      }
    }
    perFragmentLighting_modelMatrixUniform_ak = glGetUniformLocation(
      perFragmentLighting_ShaderProgramObject_ak, "u_model_matrix");
    perFragmentLighting_viewMatrixUniform_ak = glGetUniformLocation(
      perFragmentLighting_ShaderProgramObject_ak, "u_view_matrix");
    perFragmentLighting_projectionMatrixUniform_ak = glGetUniformLocation(
      perFragmentLighting_ShaderProgramObject_ak, "u_projection_matrix");
    perFragmentLighting_laUniform_ak =
      glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_la");
    perFragmentLighting_ldUniform_ak =
      glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_ld");
    perFragmentLighting_lsUniform_ak =
      glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_ls");
    perFragmentLighting_lightPositionUniform_ak = glGetUniformLocation(
      perFragmentLighting_ShaderProgramObject_ak, "u_light_position");
    perFragmentLighting_kaUniform_ak =
      glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_ka");
    perFragmentLighting_kdUniform_ak =
      glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_kd");
    perFragmentLighting_ksUniform_ak =
      glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_ks");
    perFragmentLighting_materialShininessUniform_ak = glGetUniformLocation(
      perFragmentLighting_ShaderProgramObject_ak, "u_material_shininess");
    perFragmentLighting_lKeyPressedUniform_ak = glGetUniformLocation(
      perFragmentLighting_ShaderProgramObject_ak, "u_lkey_pressed");
    glGenVertexArrays(1, &perFragmentLighting_vao_sphere_ak);
    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glGenBuffers(1, &perFragmentLighting_vbo_position_sphere_ak);
    glBindBuffer(GL_ARRAY_BUFFER, perFragmentLighting_vbo_position_sphere_ak);
    glBufferData(GL_ARRAY_BUFFER,
                 sizeof(sphere_vertices_ak),
                 sphere_vertices_ak,
                 GL_STATIC_DRAW);
    glVertexAttribPointer(
      ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glGenBuffers(1, &perFragmentLighting_vbo_normal_sphere_ak);
    glBindBuffer(GL_ARRAY_BUFFER, perFragmentLighting_vbo_normal_sphere_ak);
    glBufferData(GL_ARRAY_BUFFER,
                 sizeof(sphere_normals_ak),
                 sphere_normals_ak,
                 GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glGenBuffers(1, &preFragmentLighting_vbo_element_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                 preFragmentLighting_vbo_element_sphere_ak);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 sizeof(sphere_elements_ak),
                 sphere_elements_ak,
                 GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    perspectiveProjectionMatrix_ak = vmath::mat4::identity();

    printf("Init done\n");

    UITapGestureRecognizer* singleTapGestureRecognizer =
      [[UITapGestureRecognizer alloc] initWithTarget:self
                                              action:@selector(onSingleTap:)];

    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
    [singleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:singleTapGestureRecognizer];

    UITapGestureRecognizer* doubleTapGestureRecognizer =
      [[UITapGestureRecognizer alloc] initWithTarget:self
                                              action:@selector(onDoubleTap:)];

    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
    [doubleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:doubleTapGestureRecognizer];

    [singleTapGestureRecognizer
      requireGestureRecognizerToFail:doubleTapGestureRecognizer];

    UISwipeGestureRecognizer* swipeGestureRecognizer =
      [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(onSwipe:)];
    [swipeGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:swipeGestureRecognizer];

    UILongPressGestureRecognizer* longPressGestureRecognizer =
      [[UILongPressGestureRecognizer alloc]
        initWithTarget:self
                action:@selector(onLongPress:)];
    [longPressGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:longPressGestureRecognizer];
  }
  return (self);
}

+ (Class)layerClass
{
  return ([CAEAGLLayer class]);
}

/*
 - (void)drawRect:(CGRect)rect {

 [self loadTextureFromBMPFile:@"Smiley":@"bmp"];

 }
 */

- (void)layoutSubviews
{
  glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
  [eaglContext renderbufferStorage:GL_RENDERBUFFER
                      fromDrawable:(CAEAGLLayer*)[self layer]];
  glFramebufferRenderbuffer(
    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);

  GLint width;
  GLint height;
  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
  glGetRenderbufferParameteriv(
    GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);

  glGenRenderbuffers(1, &depthRenderbuffer);
  glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
  glFramebufferRenderbuffer(
    GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);

  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
    printf("Framebuffer is not complete in layoutSubviews");
  }

  if (height < 0) {
    height = 1;
  }

  glViewport(0, 0, (GLsizei)width, (GLsizei)height);
  perspectiveProjectionMatrix_ak =
    vmath::perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
  [self drawView:nil];
}

- (void)drawView:(id)sender
{
  [EAGLContext setCurrentContext:eaglContext];
  glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  switch (pvpf) {
    case 1: {
      glUseProgram(perFragmentLighting_ShaderProgramObject_ak);
      GLfloat lightAmbient_ak[9] = {
        lights[0].lightAmbient_ak[0], lights[0].lightAmbient_ak[1],
        lights[0].lightAmbient_ak[2], lights[1].lightAmbient_ak[0],
        lights[1].lightAmbient_ak[1], lights[1].lightAmbient_ak[2],
        lights[2].lightAmbient_ak[0], lights[2].lightAmbient_ak[1],
        lights[2].lightAmbient_ak[2]
      };
      GLfloat lightDiffused_ak[9] = {
        lights[0].lightDiffused_ak[0], lights[0].lightDiffused_ak[1],
        lights[0].lightDiffused_ak[2], lights[1].lightDiffused_ak[0],
        lights[1].lightDiffused_ak[1], lights[1].lightDiffused_ak[2],
        lights[2].lightDiffused_ak[0], lights[2].lightDiffused_ak[1],
        lights[2].lightDiffused_ak[2]
      };
      GLfloat lightSpecular_ak[9] = {
        lights[0].lightSpecular_ak[0], lights[0].lightSpecular_ak[1],
        lights[0].lightSpecular_ak[2], lights[1].lightSpecular_ak[0],
        lights[1].lightSpecular_ak[1], lights[1].lightSpecular_ak[2],
        lights[2].lightSpecular_ak[0], lights[2].lightSpecular_ak[1],
        lights[2].lightSpecular_ak[2]
      };
      GLfloat lightPosition_ak[12] = {
        lights[0].lightPosition_ak[0], lights[0].lightPosition_ak[1],
        lights[0].lightPosition_ak[2], lights[0].lightPosition_ak[3],
        lights[1].lightPosition_ak[0], lights[1].lightPosition_ak[1],
        lights[1].lightPosition_ak[2], lights[1].lightPosition_ak[3],
        lights[2].lightPosition_ak[0], lights[2].lightPosition_ak[1],
        lights[2].lightPosition_ak[2], lights[2].lightPosition_ak[3]
      };
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 3.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 3.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 3.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 3.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      } else {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 0);
      }
      mat4 modelMatrix_ak = mat4::identity();
      mat4 viewMatrix_ak = mat4::identity();
      mat4 translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -3.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   preFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      glUseProgram(0);
      break;
    }
    case 0: {
      glUseProgram(perVertexLighting_ShaderProgramObject_ak);
      GLfloat lightAmbient_ak[9] = {
        lights[0].lightAmbient_ak[0], lights[0].lightAmbient_ak[1],
        lights[0].lightAmbient_ak[2], lights[1].lightAmbient_ak[0],
        lights[1].lightAmbient_ak[1], lights[1].lightAmbient_ak[2],
        lights[2].lightAmbient_ak[0], lights[2].lightAmbient_ak[1],
        lights[2].lightAmbient_ak[2]
      };
      GLfloat lightDiffused_ak[9] = {
        lights[0].lightDiffused_ak[0], lights[0].lightDiffused_ak[1],
        lights[0].lightDiffused_ak[2], lights[1].lightDiffused_ak[0],
        lights[1].lightDiffused_ak[1], lights[1].lightDiffused_ak[2],
        lights[2].lightDiffused_ak[0], lights[2].lightDiffused_ak[1],
        lights[2].lightDiffused_ak[2]
      };
      GLfloat lightSpecular_ak[9] = {
        lights[0].lightSpecular_ak[0], lights[0].lightSpecular_ak[1],
        lights[0].lightSpecular_ak[2], lights[1].lightSpecular_ak[0],
        lights[1].lightSpecular_ak[1], lights[1].lightSpecular_ak[2],
        lights[2].lightSpecular_ak[0], lights[2].lightSpecular_ak[1],
        lights[2].lightSpecular_ak[2]
      };
      GLfloat lightPosition_ak[12] = {
        lights[0].lightPosition_ak[0], lights[0].lightPosition_ak[1],
        lights[0].lightPosition_ak[2], lights[0].lightPosition_ak[3],
        lights[1].lightPosition_ak[0], lights[1].lightPosition_ak[1],
        lights[1].lightPosition_ak[2], lights[1].lightPosition_ak[3],
        lights[2].lightPosition_ak[0], lights[2].lightPosition_ak[1],
        lights[2].lightPosition_ak[2], lights[2].lightPosition_ak[3]
      };
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 3.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 3.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 3.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 3.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      } else {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 0);
      }
      mat4 modelMatrix_ak = mat4::identity();
      mat4 viewMatrix_ak = mat4::identity();
      mat4 translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -3.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      glUseProgram(0);
      break;
    }
  }

  Update();

  glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
  [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

void
Update(void)
{
  GLfloat radius_ak = 3.0f;
  if (bLight_ak) {
    if (lightAngle0_ak >= 2 * M_PI) {
      lightAngle0_ak = 0.0f;
    } else {
      lightAngle0_ak = lightAngle0_ak + 0.05f;
    }
    lights[0].lightPosition_ak[0] = 0.0f;
    lights[0].lightPosition_ak[1] = radius_ak * sin(lightAngle0_ak);
    lights[0].lightPosition_ak[2] = radius_ak * cos(lightAngle0_ak);
    lights[0].lightPosition_ak[3] = 1.0f;
    if (lightAngle1_ak >= 2 * M_PI) {
      lightAngle1_ak = 0.0f;
    } else {
      lightAngle1_ak = lightAngle1_ak + 0.05f;
    }
    lights[1].lightPosition_ak[0] = radius_ak * sin(lightAngle1_ak);
    lights[1].lightPosition_ak[1] = 0.0;
    lights[1].lightPosition_ak[2] = radius_ak * cos(lightAngle1_ak);
    lights[1].lightPosition_ak[3] = 1.0f;
    if (lightAngle2_ak >= 2 * M_PI) {
      lightAngle2_ak = 0.0f;
    } else {
      lightAngle2_ak = lightAngle2_ak + 0.05f;
    }
    lights[2].lightPosition_ak[0] = radius_ak * sin(lightAngle2_ak);
    lights[2].lightPosition_ak[1] = radius_ak * cos(lightAngle2_ak);
    lights[2].lightPosition_ak[2] = 0.0f;
    lights[2].lightPosition_ak[3] = 1.0f;
  }
}

- (void)startAnimation
{
  if (isAnimating == NO) {
    displayLink = [NSClassFromString(@"CADisplayLink")
      displayLinkWithTarget:self
                   selector:@selector(drawView:)];
    [displayLink setPreferredFramesPerSecond:animationFrameInterval];
    [displayLink addToRunLoop:[NSRunLoop currentRunLoop]
                      forMode:NSDefaultRunLoopMode];

    isAnimating = YES;
  }
}

- (void)stopAnimation
{
  if (isAnimating == YES) {
    [displayLink invalidate];
    displayLink = nil;
    isAnimating = NO;
  }
}

- (void)onSingleTap:(UITapGestureRecognizer*)gr
{
  if (bLight_ak) {
    bLight_ak = false;
  } else {
    bLight_ak = true;
  }
}

- (void)onDoubleTap:(UITapGestureRecognizer*)gr
{
  if (pvpf == 1) {
    pvpf = 0;
  } else {
    pvpf = 1;
  }
}

- (void)onSwipe:(UISwipeGestureRecognizer*)gr
{
  [self unintialize];
  [self release];
  exit(0);
}

- (void)onLongPress:(UILongPressGestureRecognizer*)gr
{}

- (void)unintialize
{
  if (perVertexLighting_vao_sphere_ak) {
    glDeleteVertexArrays(1, &perVertexLighting_vao_sphere_ak);
    perVertexLighting_vao_sphere_ak = 0;
  }
  if (perVertexLighting_vbo_position_sphere_ak) {
    glDeleteVertexArrays(1, &perVertexLighting_vbo_position_sphere_ak);
    perVertexLighting_vbo_position_sphere_ak = 0;
  }
  if (perVertexLighting_vbo_normal_sphere_ak) {
    glDeleteVertexArrays(1, &perVertexLighting_vbo_normal_sphere_ak);
    perVertexLighting_vbo_normal_sphere_ak = 0;
  }
  if (perVertexLighting_ShaderProgramObject_ak) {
    glUseProgram(perVertexLighting_ShaderProgramObject_ak);
    GLsizei shaderCount_ak;
    glGetProgramiv(perVertexLighting_ShaderProgramObject_ak,
                   GL_ATTACHED_SHADERS,
                   &shaderCount_ak);
    GLuint* pShaders_ak = NULL;
    pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
    glGetAttachedShaders(perVertexLighting_ShaderProgramObject_ak,
                         shaderCount_ak,
                         &shaderCount_ak,
                         pShaders_ak);
    for (int i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
      glDetachShader(perVertexLighting_ShaderProgramObject_ak,
                     pShaders_ak[i_ak]);
      glDeleteShader(pShaders_ak[i_ak]);
      pShaders_ak[i_ak] = 0;
    }
    free(pShaders_ak);
    glDeleteProgram(perVertexLighting_ShaderProgramObject_ak);
    perVertexLighting_ShaderProgramObject_ak = 0;
    glUseProgram(0);
  }
  if (perFragmentLighting_vao_sphere_ak) {
    glDeleteVertexArrays(1, &perFragmentLighting_vao_sphere_ak);
    perFragmentLighting_vao_sphere_ak = 0;
  }
  if (perFragmentLighting_vbo_position_sphere_ak) {
    glDeleteVertexArrays(1, &perFragmentLighting_vbo_position_sphere_ak);
    perFragmentLighting_vbo_position_sphere_ak = 0;
  }
  if (perFragmentLighting_vbo_normal_sphere_ak) {
    glDeleteVertexArrays(1, &perFragmentLighting_vbo_normal_sphere_ak);
    perFragmentLighting_vbo_normal_sphere_ak = 0;
  }
  if (perFragmentLighting_ShaderProgramObject_ak) {
    glUseProgram(perFragmentLighting_ShaderProgramObject_ak);
    GLsizei shaderCount_ak;
    glGetProgramiv(perFragmentLighting_ShaderProgramObject_ak,
                   GL_ATTACHED_SHADERS,
                   &shaderCount_ak);
    GLuint* pShaders_ak = NULL;
    pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
    glGetAttachedShaders(perFragmentLighting_ShaderProgramObject_ak,
                         shaderCount_ak,
                         &shaderCount_ak,
                         pShaders_ak);
    for (int i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
      glDetachShader(perFragmentLighting_ShaderProgramObject_ak,
                     pShaders_ak[i_ak]);
      glDeleteShader(pShaders_ak[i_ak]);
      pShaders_ak[i_ak] = 0;
    }
    free(pShaders_ak);
    glDeleteProgram(perFragmentLighting_ShaderProgramObject_ak);
    perFragmentLighting_ShaderProgramObject_ak = 0;
    glUseProgram(0);
  }

  if (depthRenderbuffer) {
    glDeleteRenderbuffers(1, &depthRenderbuffer);
    depthRenderbuffer = 0;
  }

  if (colorRenderbuffer) {
    glDeleteRenderbuffers(1, &colorRenderbuffer);
    colorRenderbuffer = 0;
  }

  if (defaultFramebuffer) {
    glDeleteFramebuffers(1, &defaultFramebuffer);
    defaultFramebuffer = 0;
  }

  if (eaglContext) {
    if ([EAGLContext currentContext] == eaglContext) {
      [EAGLContext setCurrentContext:nil];
    }
  }
}

- (void)dealloc
{
  [self unintialize];
  [super dealloc];
}

@end
