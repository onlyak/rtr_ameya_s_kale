//
//  AppDelegate.m
//  WindowiOS
//
//  Created by ameya kale on 03/07/21.
//

#import "AppDelegate.h"

#import "ViewController.h"

#import "MyView.h"

@implementation AppDelegate
{
    @private
    UIWindow *window;
    ViewController *viewController;
    MyView *view;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    CGRect win_rect = [[UIScreen mainScreen]bounds];
    window = [[UIWindow alloc]initWithFrame:win_rect];
    viewController = [[ViewController alloc]init];
    [window setRootViewController:viewController];
    
    view = [[MyView alloc]initWithFrame:win_rect];
    [viewController setView:view];
    [view release];
    [window makeKeyAndVisible];
    return YES;
}

-(void)applicationWillResignActive:(UIApplication *)application
{
    
}

-(void)applicationDidEnterBackground:(UIApplication *)application
{
    
}

-(void)applicationWillEnterForeground:(UIApplication *)application
{
    
}

-(void)applicationDidBecomeActive:(UIApplication *)application
{
    
}

-(void) applicationWillTerminate:(UIApplication *)application
{
    
}

-(void)dealloc
{
    [view release];
    [viewController release];
    [window release];
    [super dealloc];
}

@end
