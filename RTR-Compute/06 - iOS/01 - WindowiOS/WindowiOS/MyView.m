//
//  MyView.m
//  WindowiOS
//
//  Created by ameya kale on 03/07/21.
//

#import "MyView.h"

@implementation MyView {
    @private
    NSString *centralText;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame: frame];
        if(self)
        {
            centralText=@"Hello World";
            UITapGestureRecognizer *singleTapGestureRecogniser = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
            [singleTapGestureRecogniser setNumberOfTapsRequired:1];
            [singleTapGestureRecogniser setNumberOfTouchesRequired:1];
            [singleTapGestureRecogniser setDelegate:self];
            [self addGestureRecognizer:singleTapGestureRecogniser];
            
            UITapGestureRecognizer *doubleTapGestureRecogniser = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
            [doubleTapGestureRecogniser setNumberOfTapsRequired:2];
            [doubleTapGestureRecogniser setNumberOfTouchesRequired:1];
            [doubleTapGestureRecogniser setDelegate:self];
            [self addGestureRecognizer:doubleTapGestureRecogniser];
            
            [singleTapGestureRecogniser requireGestureRecognizerToFail: doubleTapGestureRecogniser];
            
            UISwipeGestureRecognizer *swipeGestureRecogniser = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
            [swipeGestureRecogniser setDelegate:self];
            [self addGestureRecognizer:swipeGestureRecogniser];
            
            UILongPressGestureRecognizer *longPressRecogniser = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
            [longPressRecogniser setDelegate:self];
            [self addGestureRecognizer:longPressRecogniser];
        }
        return(self);
}
- (void)drawRect:(CGRect)rect {
    // Drawing code
    UIColor *backgroundColor=[UIColor blackColor];
    [backgroundColor set];
    UIRectFill(rect);
    
    NSDictionary *dictionaryForTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                        [UIFont fontWithName:@"Helvetica" size:32],
                         NSFontAttributeName,
                         [UIColor greenColor],
                         NSForegroundColorAttributeName,
                         nil];
                         
        CGSize textSize_PSM=[centralText sizeWithAttributes:dictionaryForTextAttributes];

        CGPoint point;
        point.x=(rect.size.width/2)-(textSize_PSM.width/2);
        point.y=(rect.size.height/2)-(textSize_PSM.height/2)+12;

        [centralText drawAtPoint:point withAttributes:dictionaryForTextAttributes];
    
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    centralText = @"Single Tap";
    [self setNeedsDisplay];
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    centralText = @"Double Tap";
    [self setNeedsDisplay];
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
//    centralText = @"Swipe";
//    [self setNeedsDisplay];
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    centralText = @"Long Press";
    [self setNeedsDisplay];
}

-(void) dealloc
{
    [super dealloc];
}
@end
