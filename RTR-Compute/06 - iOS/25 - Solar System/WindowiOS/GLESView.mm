#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "sphere.h"
#import "vmath.h"
using namespace vmath;

int day_ak = 0;
int year_ak = 0;
int hour_ak = 0;
bool rotationDirection_ak;
GLint objectToBeHandled_ak;


@implementation GLESView {
@private
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink; // CADisplayLink type is not recommended
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    enum {
        ATTRIBUTE_POSITION = 0,
        ATTRIBUTE_COLOR,
        ATTRIBUTE_NORMAL,
        ATTRIBUTE_TEXCOORD
    };
    GLuint gVertexShaderObject_ak;
      GLuint gFragmentShaderObject_ak;
      GLuint gShaderProgramObject_ak;
      GLuint vao_sphere_ak;
      GLuint vbo_position_sphere_ak;
      GLuint vbo_normal_sphere_ak;
      GLuint vbo_element_sphere_ak;
      GLuint mvpMatrixUniform_ak;
      GLuint colorUniform_ak;
      mat4 perspectiveProjectionMatrix_ak;
      float sphere_vertices_ak[1146];
      float sphere_normals_ak[1146];
      float sphere_textures_ak[764];
      unsigned short sphere_elements_ak[2280];
      int gNumVertices_ak;
      int gNumElements_ak;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame: frame];
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)[super layer];
        
        [eaglLayer setOpaque:YES];
        
        [eaglLayer setDrawableProperties :[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO],
                                           kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,
                                           kEAGLDrawablePropertyColorFormat,nil]];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            printf("OpenGL-ES Context Creation Failed.\n");
            return(nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH,&backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT,&backingHeight);
        
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        printf("initWithFrame checking if framebuffer is complete\n");
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Framebuffer is not complete\n");
            [self unintialize];
            return(nil);
        }
        
        printf("Renderer : %s\n", glGetString(GL_RENDERER));
        printf("GL Version : %s\n", glGetString(GL_VERSION));
        printf("GLES Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        animationFrameInterval=60; // default 60 from iOS 8.2
        isAnimating=NO;
        
        //shader block here
        gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
          const GLchar* vertexShaderSourceCode =
            "#version 300 es"
            "\n"
            "in vec4 vPosition;"
            "uniform mat4 u_mvp_matrix;"
            "void main(void)"
            "{"
            "gl_Position = u_mvp_matrix * vPosition;"
            "}";

          glShaderSource(
            gVertexShaderObject_ak, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
          glCompileShader(gVertexShaderObject_ak);

          GLint infoLogLength_ak = 0;
          GLint shaderCompileStatus_ak = 0;
          char* szBuffer_ak = NULL;
          glGetShaderiv(
            gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
          if (shaderCompileStatus_ak == GL_FALSE) {
            glGetShaderiv(
              gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
            if (infoLogLength_ak > 0) {
              szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
              if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetShaderInfoLog(
                  gVertexShaderObject_ak,
                  infoLogLength_ak,
                  &written_ak,
                  szBuffer_ak);
                printf("Vertex shader compilation log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                [self unintialize];
              }
            }
          }

          gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);

          const GLchar* fragmentSourceCode_ak = "#version 300 es"
                                                 "\n"
                                                "precision highp float;"
                                                "out vec4 FragColor;"
                                                 "uniform vec3 u_color;"
                                                 "void main(void)"
                                                 "{"
                                                 "FragColor = vec4(u_color, 1.0);"
                                                 "}";

          glShaderSource(gFragmentShaderObject_ak,
                         1,
                         (const GLchar**)&fragmentSourceCode_ak,
                         NULL);
          glCompileShader(gFragmentShaderObject_ak);

          infoLogLength_ak = 0;
          shaderCompileStatus_ak = 0;
          szBuffer_ak = NULL;
          glGetShaderiv(
            gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
          if (shaderCompileStatus_ak == GL_FALSE) {
            glGetShaderiv(
              gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
            if (infoLogLength_ak > 0) {
              szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
              if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetShaderInfoLog(
                  gFragmentShaderObject_ak,
                  infoLogLength_ak,
                  &written_ak,
                  szBuffer_ak);
                printf(
                  "Fragment shader compilation log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                [self unintialize];
              }
            }
          }

          gShaderProgramObject_ak = glCreateProgram();
          glAttachShader(gShaderProgramObject_ak, gVertexShaderObject_ak);
          glAttachShader(gShaderProgramObject_ak, gFragmentShaderObject_ak);

          glBindAttribLocation(
            gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");

          glLinkProgram(gShaderProgramObject_ak);

          GLint shaderProgramLinkStatus = 0;
          glGetProgramiv(
            gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
          if (shaderProgramLinkStatus == GL_FALSE) {
            glGetProgramiv(
              gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
            if (infoLogLength_ak > 0) {
              szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
              if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetProgramInfoLog(
                  gShaderProgramObject_ak,
                  infoLogLength_ak,
                  &written_ak,
                  szBuffer_ak);
                printf("Shader program link log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                [self unintialize];
              }
            }
          }

          mvpMatrixUniform_ak =
            glGetUniformLocation(gShaderProgramObject_ak, "u_mvp_matrix");
        colorUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_color");
          
        Sphere *sphere = new Sphere();
          sphere->getSphereVertexData(sphere_vertices_ak,
                                     sphere_normals_ak,
                                     sphere_textures_ak,
                                     sphere_elements_ak);
          gNumVertices_ak = sphere->getNumberOfSphereVertices();
          gNumElements_ak = sphere->getNumberOfSphereElements();

          glGenVertexArrays(1, &vao_sphere_ak);
          glBindVertexArray(vao_sphere_ak);

          glGenBuffers(1, &vbo_position_sphere_ak);
          glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere_ak);
          glBufferData(GL_ARRAY_BUFFER,
                       sizeof(sphere_vertices_ak),
                       sphere_vertices_ak,
                       GL_STATIC_DRAW);
          glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
          glEnableVertexAttribArray(ATTRIBUTE_POSITION);
          glBindBuffer(GL_ARRAY_BUFFER, 0);

          glGenBuffers(1, &vbo_normal_sphere_ak);
          glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere_ak);
          glBufferData(GL_ARRAY_BUFFER,
                       sizeof(sphere_normals_ak),
                       sphere_normals_ak,
                       GL_STATIC_DRAW);
          glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
          glEnableVertexAttribArray(ATTRIBUTE_NORMAL);
          glBindBuffer(GL_ARRAY_BUFFER, 0);

          glGenBuffers(1, &vbo_element_sphere_ak);
          glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak);
          glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                       sizeof(sphere_elements_ak),
                       sphere_elements_ak,
                       GL_STATIC_DRAW);
          glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

          glBindVertexArray(0);

          glEnable(GL_DEPTH_TEST);
          glDepthFunc(GL_LEQUAL);

          glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
          perspectiveProjectionMatrix_ak =
            vmath::mat4::identity();
        
        printf("Init done\n");
        
        // gestures event handling
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [swipeGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [longPressGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);
}

+(Class)layerClass{
    return ([CAEAGLLayer class]);
}

-(void)layoutSubviews
{
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)[self layer]];
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);
    
    GLint width;
    GLint height;
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH,&width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT,&height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Framebuffer is not complete in layoutSubviews");
    }
    
    if (height < 0)
    {
        height = 1;
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
        perspectiveProjectionMatrix_ak =
            vmath::perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
    [self drawView: nil];
}

-(void) drawView:(id)sender
{
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      
    glUseProgram(gShaderProgramObject_ak);
      mat4 modelViewMatrix1_ak = mat4::identity();
      mat4 modelViewMatrix2_ak = mat4::identity();
      mat4 modelViewMatrix3_ak = mat4::identity();
      mat4 modelViewProjectionMatrix_ak;
      mat4 translateMatrix_ak;
      mat4 xRotationMatrix_ak;
      mat4 yRotationMatrix_ak;
      mat4 zRotationMatrix_ak;
      mat4 scaleMatrix_ak;
      modelViewProjectionMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      xRotationMatrix_ak = mat4::identity();
      yRotationMatrix_ak = mat4::identity();
      zRotationMatrix_ak = mat4::identity();
      scaleMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -5.0f);
      modelViewMatrix1_ak = translateMatrix_ak;
      xRotationMatrix_ak = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
      modelViewProjectionMatrix_ak =
        perspectiveProjectionMatrix_ak * modelViewMatrix1_ak;
      glUniformMatrix4fv(
        mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix_ak);
      GLfloat sunColor_ak[] = { 1.0f, 1.0f, 0.0f };
      glUniform3fv(colorUniform_ak, 1, sunColor_ak);
      glBindVertexArray(vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      yRotationMatrix_ak = vmath::rotate((GLfloat)year_ak, 0.0f, 1.0f, 0.0f);
      translateMatrix_ak = vmath::translate(1.7f, 0.0f, 0.0f);
      modelViewMatrix2_ak =
        modelViewMatrix1_ak * yRotationMatrix_ak * translateMatrix_ak;
      yRotationMatrix_ak = vmath::rotate((GLfloat)day_ak, 0.0f, 1.0f, 0.0f);
      scaleMatrix_ak = vmath::scale(0.5f, 0.5f, 0.5f);
      modelViewMatrix3_ak =
        modelViewMatrix2_ak * yRotationMatrix_ak * scaleMatrix_ak;
      modelViewProjectionMatrix_ak =
        perspectiveProjectionMatrix_ak * modelViewMatrix3_ak;
      glUniformMatrix4fv(
        mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix_ak);
      GLfloat earthColor[] = { 0.4f, 0.9f, 1.0f };
      glUniform3fv(colorUniform_ak, 1, earthColor);
      glBindVertexArray(vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      yRotationMatrix_ak = vmath::rotate((GLfloat)day_ak, 0.0f, 1.0f, 0.0f);
      translateMatrix_ak = vmath::translate(0.7f, 0.0f, 0.0f);
      modelViewMatrix3_ak =
        modelViewMatrix2_ak * yRotationMatrix_ak * translateMatrix_ak;
      yRotationMatrix_ak = vmath::rotate((GLfloat)hour_ak, 0.0f, 1.0f, 0.0f);
      scaleMatrix_ak = vmath::scale(0.3f, 0.3f, 0.3f);
      modelViewMatrix3_ak =
        modelViewMatrix3_ak * yRotationMatrix_ak * scaleMatrix_ak;
      modelViewProjectionMatrix_ak =
        perspectiveProjectionMatrix_ak * modelViewMatrix3_ak;
      glUniformMatrix4fv(
        mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix_ak);
      GLfloat moonColor[] = { 1.0f, 1.0f, 1.0f };
      glUniform3fv(colorUniform_ak, 1, moonColor);
      glBindVertexArray(vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      modelViewProjectionMatrix_ak =
        perspectiveProjectionMatrix_ak * modelViewMatrix2_ak;
      glUniformMatrix4fv(
        mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix_ak);
      glUseProgram(0);
    
    Update();
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

void
Update(void)
{

}

-(void)startAnimation
{
    if(isAnimating == NO)
    {
        displayLink=[NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        isAnimating = NO;
    }
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    if(rotationDirection_ak == true) {
                rotationDirection_ak = false;
            } else {
                rotationDirection_ak = true;
            }
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    if (objectToBeHandled_ak == 2) {
                objectToBeHandled_ak = 0;
            } else {
                objectToBeHandled_ak += 1;
            }
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    [self unintialize];
    [self release];
    exit(0);
    
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    if(objectToBeHandled_ak == 0) {
                if (rotationDirection_ak == true) {
                    hour_ak = (hour_ak + 1) % 360;
                } else {
                    hour_ak = (hour_ak - 1) % 360;
                }
            }
            if(objectToBeHandled_ak == 1) {
                if (rotationDirection_ak == true) {
                    day_ak = (day_ak + 6) % 360;
                    hour_ak = (hour_ak + 1) % 360;
                } else {
                    day_ak = (day_ak - 6) % 360;
                    hour_ak = (hour_ak - 1) % 360;
                }
            }
            if(objectToBeHandled_ak == 2) {
                if (rotationDirection_ak == true) {
                    year_ak = (year_ak + 3) % 360;
                    day_ak = (day_ak - 6) % 360;
                    hour_ak = (hour_ak + 1) % 360;
                } else {
                    year_ak = (year_ak - 3) % 360;
                    day_ak = (day_ak + 6) % 360;
                    hour_ak = (hour_ak - 1) % 360;
                }
            }
}

-(void)unintialize
{
    if (vao_sphere_ak) {
        glDeleteVertexArrays(1, &vao_sphere_ak);
        vao_sphere_ak = 0;
      }

      if (vbo_position_sphere_ak) {
        glDeleteVertexArrays(1, &vbo_position_sphere_ak);
        vbo_position_sphere_ak = 0;
      }

      if (vbo_normal_sphere_ak) {
        glDeleteVertexArrays(1, &vbo_normal_sphere_ak);
        vbo_normal_sphere_ak = 0;
      }

      if (vbo_element_sphere_ak) {
        glDeleteVertexArrays(1, &vbo_element_sphere_ak);
        vbo_element_sphere_ak = 0;
      }
    
    
    if (gShaderProgramObject_ak) {
        glUseProgram(gShaderProgramObject_ak);
        GLsizei shaderCount_ak;
        glGetProgramiv(gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);
        
        GLuint* pShaders_ak = NULL;
        pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
        glGetAttachedShaders(gShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);
        
        for (int i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
            glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
            glDeleteShader(pShaders_ak[i_ak]);
            pShaders_ak[i_ak] = 0;
        }
        
        free(pShaders_ak);
        glDeleteProgram(gShaderProgramObject_ak);
        gShaderProgramObject_ak = 0;
        
        glUseProgram(0);
    }
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer = 0;
    }
    
    if (eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext){
            [EAGLContext setCurrentContext:nil];
        }
    }
    
}

- (void)dealloc
{
    [self unintialize];
    [super dealloc];
}

@end
