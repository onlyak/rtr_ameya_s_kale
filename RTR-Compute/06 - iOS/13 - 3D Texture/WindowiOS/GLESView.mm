#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"

#import "vmath.h"

using namespace vmath;
GLfloat pyramidAngle_ak = 0.0f;
GLfloat cubeAngle_ak = 0.0f;
GLuint stone_texture_ak;
GLuint kundali_texture_ak;

@implementation GLESView {
@private
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink; // CADisplayLink type is not recommended
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    enum {
        ATTRIBUTE_POSITION = 0,
        ATTRIBUTE_COLOR,
        ATTRIBUTE_NORMAL,
        ATTRIBUTE_TEXCOORD
    };
    GLuint gVertexShaderObject_ak;
     GLuint gFragmentShaderObject_ak;
     GLuint gShaderProgramObject_ak;
     GLuint vao_pyramid_ak;
     GLuint vbo_position_pyramid_ak;
     GLuint vbo_texture_pyramid_ak;
     GLuint vao_cube_ak;
     GLuint vbo_position_cube_ak;
     GLuint vbo_texture_cube_ak;
     GLuint mvpMatrixUniform_ak;
     GLuint textureSamplerUniform_ak;
     vmath::mat4 perspectiveProjectionMatrix_ak;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame: frame];
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)[super layer];
        
        [eaglLayer setOpaque:YES];
        
        [eaglLayer setDrawableProperties :[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO],
                                           kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,
                                           kEAGLDrawablePropertyColorFormat,nil]];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            printf("OpenGL-ES Context Creation Failed.\n");
            return(nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH,&backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT,&backingHeight);
        
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        printf("initWithFrame checking if framebuffer is complete\n");
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Framebuffer is not complete\n");
            [self unintialize];
            return(nil);
        }
        
        printf("Renderer : %s\n", glGetString(GL_RENDERER));
        printf("GL Version : %s\n", glGetString(GL_VERSION));
        printf("GLES Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        animationFrameInterval=60; // default 60 from iOS 8.2
        isAnimating=NO;
        
        //shader block here
        gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
        const GLchar* vertexShaderSourceCode =
        "#version 300 es"
        "\n"
        "in vec4 vPosition;"
            "in vec2 vTexCoord;"
            "uniform mat4 u_mvp_matrix;"
            "out vec2 out_texcoord;"
            "void main(void)"
            "{"
            "gl_Position = u_mvp_matrix * vPosition;"
            "out_texcoord = vTexCoord;"
        "}";
        
        glShaderSource(
                       gVertexShaderObject_ak, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
        glCompileShader(gVertexShaderObject_ak);
        
        GLint infoLogLength_ak = 0;
        GLint shaderCompileStatus_ak = 0;
        char* szBuffer_ak = NULL;
        glGetShaderiv(
                      gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
        if (shaderCompileStatus_ak == GL_FALSE) {
            glGetShaderiv(
                          gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
            if (infoLogLength_ak > 0) {
                szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
                if (szBuffer_ak != NULL) {
                    GLsizei written_ak;
                    glGetShaderInfoLog(
                                       gVertexShaderObject_ak,
                                       infoLogLength_ak,
                                       &written_ak,
                                       szBuffer_ak);
                    printf("Vertex shader compilation log: %s\n", szBuffer_ak);
                    free(szBuffer_ak);
                    [self unintialize];
                }
            }
        }
        
        gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);
        
        const GLchar* fragmentSourceCode_ak = "#version 300 es"
        "\n"
        "precision highp float;"\
        "in vec2 out_texcoord;"
            "uniform sampler2D u_texture_sampler;"
            "out vec4 FragColor;"
            "void main(void)"
            "{"
            "FragColor = texture(u_texture_sampler, out_texcoord);"
        "}";
        
        glShaderSource(gFragmentShaderObject_ak,
                       1,
                       (const GLchar**)&fragmentSourceCode_ak,
                       NULL);
        glCompileShader(gFragmentShaderObject_ak);
        
        infoLogLength_ak = 0;
        shaderCompileStatus_ak = 0;
        szBuffer_ak = NULL;
        glGetShaderiv(
                      gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
        if (shaderCompileStatus_ak == GL_FALSE) {
            glGetShaderiv(
                          gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
            if (infoLogLength_ak > 0) {
                szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
                if (szBuffer_ak != NULL) {
                    GLsizei written_ak;
                    glGetShaderInfoLog(
                                       gFragmentShaderObject_ak,
                                       infoLogLength_ak,
                                       &written_ak,
                                       szBuffer_ak);
                    printf(
                           "Fragment shader compilation log: %s\n", szBuffer_ak);
                    free(szBuffer_ak);
                    [self unintialize];
                }
            }
        }
        
        gShaderProgramObject_ak = glCreateProgram();
        glAttachShader(gShaderProgramObject_ak, gVertexShaderObject_ak);
        glAttachShader(gShaderProgramObject_ak, gFragmentShaderObject_ak);
        glBindAttribLocation(
        gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");
          glBindAttribLocation(
            gShaderProgramObject_ak, ATTRIBUTE_TEXCOORD, "vTexCoord");
        
        glLinkProgram(gShaderProgramObject_ak);
        
        GLint shaderProgramLinkStatus = 0;
        glGetProgramiv(
                       gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
        if (shaderProgramLinkStatus == GL_FALSE) {
            glGetProgramiv(
                           gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
            if (infoLogLength_ak > 0) {
                szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
                if (szBuffer_ak != NULL) {
                    GLsizei written_ak;
                    glGetProgramInfoLog(
                                        gShaderProgramObject_ak,
                                        infoLogLength_ak,
                                        &written_ak,
                                        szBuffer_ak);
                    printf("Shader program link log: %s\n", szBuffer_ak);
                    free(szBuffer_ak);
                    [self unintialize];
                }
            }
        }
        
        mvpMatrixUniform_ak =
            glGetUniformLocation(gShaderProgramObject_ak, "u_mvp_matrix");
          textureSamplerUniform_ak =
            glGetUniformLocation(gShaderProgramObject_ak, "u_texture_sampler");
        
        
        const GLfloat pyramidVertices_ak[] = {
            0.0f, 1.0f, 0.0f, -1.0f, -1.0f, 1.0f,  1.0f,  -1.0f, 1.0f,
            0.0f, 1.0f, 0.0f, 1.0f,  -1.0f, 1.0f,  1.0f,  -1.0f, -1.0f,
            0.0f, 1.0f, 0.0f, 1.0f,  -1.0f, -1.0f, -1.0f, -1.0f, -1.0f,
            0.0f, 1.0f, 0.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f
          };
          const GLfloat pyramidTexCoords_ak[] = { 0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
                                                   0.5f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,
                                                   0.5f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,
                                                   0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f };
          const GLfloat cubeVertices_ak[] = {
            1.0f,  1.0f,  1.0f,  -1.0f, 1.0f,  1.0f,
            -1.0f, -1.0f, 1.0f,  1.0f,  -1.0f, 1.0f,
            1.0f,  1.0f,  -1.0f, 1.0f,  1.0f,  1.0f,
            1.0f,  -1.0f, 1.0f,  1.0f,  -1.0f, -1.0f,
            -1.0f, 1.0f,  -1.0f, 1.0f,  1.0f,  -1.0f,
            1.0f,  -1.0f, -1.0f, -1.0f, -1.0f, -1.0f,
            -1.0f, 1.0f,  1.0f,  -1.0f, 1.0f,  -1.0f,
            -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f,
            1.0f,  1.0f,  -1.0f, -1.0f, 1.0f,  -1.0f,
            -1.0f, 1.0f,  1.0f,  1.0f,  1.0f,  1.0f,
            1.0f,  -1.0f, -1.0f, -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f,  1.0f,  -1.0f, 1.0f
          };
          const GLfloat cubeTexCoords_ak[] = {
            0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
          };
          glGenVertexArrays(1, &vao_pyramid_ak);
          glBindVertexArray(vao_pyramid_ak);
          glGenBuffers(1, &vbo_position_pyramid_ak);
          glBindBuffer(GL_ARRAY_BUFFER, vbo_position_pyramid_ak);
          glBufferData(GL_ARRAY_BUFFER,
                       sizeof(pyramidVertices_ak),
                       pyramidVertices_ak,
                       GL_STATIC_DRAW);
          glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
          glEnableVertexAttribArray(ATTRIBUTE_POSITION);
          glBindBuffer(GL_ARRAY_BUFFER, 0);
          glGenBuffers(1, &vbo_texture_pyramid_ak);
          glBindBuffer(GL_ARRAY_BUFFER, vbo_texture_pyramid_ak);
          glBufferData(GL_ARRAY_BUFFER,
                       sizeof(pyramidTexCoords_ak),
                       pyramidTexCoords_ak,
                       GL_STATIC_DRAW);
          glVertexAttribPointer(
            ATTRIBUTE_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 0, NULL);
          glEnableVertexAttribArray(ATTRIBUTE_TEXCOORD);
          glBindBuffer(GL_ARRAY_BUFFER, 0);
          glBindVertexArray(0);
          glGenVertexArrays(1, &vao_cube_ak);
          glBindVertexArray(vao_cube_ak);
          glGenBuffers(1, &vbo_position_cube_ak);
          glBindBuffer(GL_ARRAY_BUFFER, vbo_position_cube_ak);
          glBufferData(GL_ARRAY_BUFFER,
                       sizeof(cubeVertices_ak),
                       cubeVertices_ak,
                       GL_STATIC_DRAW);
          glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
          glEnableVertexAttribArray(ATTRIBUTE_POSITION);
          glBindBuffer(GL_ARRAY_BUFFER, 0);
          glGenBuffers(1, &vbo_texture_cube_ak);
          glBindBuffer(GL_ARRAY_BUFFER, vbo_texture_cube_ak);
          glBufferData(GL_ARRAY_BUFFER,
                       sizeof(cubeTexCoords_ak),
                       cubeTexCoords_ak,
                       GL_STATIC_DRAW);
          glVertexAttribPointer(
            ATTRIBUTE_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 0, NULL);
          glEnableVertexAttribArray(ATTRIBUTE_TEXCOORD);
          glBindBuffer(GL_ARRAY_BUFFER, 0);
          glBindVertexArray(0);        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        perspectiveProjectionMatrix_ak =
        vmath::mat4::identity();
        stone_texture_ak = [self loadTextureFromBMPFile:@"Stone":@"bmp"];
        kundali_texture_ak = [self loadTextureFromBMPFile:@"Vijay_Kundali":@"bmp"];
        printf("Init done\n");
        
        // gestures event handling
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [swipeGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [longPressGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);
}

+(Class)layerClass{
    return ([CAEAGLLayer class]);
}

/*
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 [self loadTextureFromBMPFile:@"Smiley":@"bmp"];
 
 }
 */
-(GLuint) loadTextureFromBMPFile:(NSString *)imageFileName : (NSString *) extension
{
    //[self loadTextureFromBMPFile:@"Smiley":@"bmp"];
    
    NSString *imageFileNameWithPath_ak = [[NSBundle mainBundle]pathForResource:imageFileName ofType:extension];
    UIImage *bmpImage=[[UIImage alloc]initWithContentsOfFile:imageFileNameWithPath_ak];
    
    // Get CG image representation of NSImage
    CGImageRef cgImage=[bmpImage CGImage];
    
    // Get width and height
    int w=(int)CGImageGetWidth(cgImage);
    int h=(int)CGImageGetHeight(cgImage);
    
    CFDataRef imageData=CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    
    // convert to generic format data
    void* pixels=(void *) CFDataGetBytePtr(imageData);
    GLuint bitmapTexture;
    glGenTextures(1,&bitmapTexture);
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);
    glBindTexture(GL_TEXTURE_2D,bitmapTexture);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 w,
                 h,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 pixels);
    
    glGenerateMipmap(GL_TEXTURE_2D);
    CFRelease(imageData);
    return(bitmapTexture);
}

-(void)layoutSubviews
{
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)[self layer]];
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);
    
    GLint width;
    GLint height;
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH,&width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT,&height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Framebuffer is not complete in layoutSubviews");
    }
    
    if (height < 0)
    {
        height = 1;
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    perspectiveProjectionMatrix_ak =
    vmath::perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
    [self drawView: nil];
}

-(void) drawView:(id)sender
{
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(gShaderProgramObject_ak);
     vmath::mat4 modelViewMatrix_ak;
     vmath::mat4 modelViewProjectionMatrix_ak;
     vmath::mat4 translateMatrix_ak;
     vmath::mat4 xRotationMatrix_ak;
     vmath::mat4 yRotationMatrix_ak;
     vmath::mat4 zRotationMatrix_ak;
     modelViewMatrix_ak = vmath::mat4::identity();
     modelViewProjectionMatrix_ak = vmath::mat4::identity();
     translateMatrix_ak = vmath::mat4::identity();
     xRotationMatrix_ak = vmath::mat4::identity();
     yRotationMatrix_ak = vmath::mat4::identity();
     zRotationMatrix_ak = vmath::mat4::identity();
     translateMatrix_ak = vmath::translate(-1.5f, 0.0f, -6.0f);
     yRotationMatrix_ak = vmath::rotate(pyramidAngle_ak, 0.0f, 1.0f, 0.0f);
     modelViewMatrix_ak = translateMatrix_ak * yRotationMatrix_ak;
     modelViewProjectionMatrix_ak =
       perspectiveProjectionMatrix_ak * modelViewMatrix_ak;
     glUniformMatrix4fv(
       mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix_ak);
     glActiveTexture(GL_TEXTURE0);
     glBindTexture(GL_TEXTURE_2D, stone_texture_ak);
     glUniform1i(textureSamplerUniform_ak, 0);
     glBindVertexArray(vao_pyramid_ak);
     glDrawArrays(GL_TRIANGLES, 0, 12);
     glBindVertexArray(0);
     vmath::mat4 scaleMatrix_ak;
     modelViewMatrix_ak = vmath::mat4::identity();
     modelViewProjectionMatrix_ak = vmath::mat4::identity();
     translateMatrix_ak = vmath::mat4::identity();
     xRotationMatrix_ak = vmath::mat4::identity();
     yRotationMatrix_ak = vmath::mat4::identity();
     zRotationMatrix_ak = vmath::mat4::identity();
     scaleMatrix_ak = vmath::mat4::identity();
     translateMatrix_ak = vmath::translate(1.5f, 0.0f, -6.0f);
     xRotationMatrix_ak = vmath::rotate(cubeAngle_ak, 1.0f, 0.0f, 0.0f);
     yRotationMatrix_ak = vmath::rotate(cubeAngle_ak, 0.0f, 1.0f, 0.0f);
     zRotationMatrix_ak = vmath::rotate(cubeAngle_ak, 0.0f, 0.0f, 1.0f);
     scaleMatrix_ak = vmath::scale(0.75f, 0.75f, 0.75f);
     modelViewMatrix_ak = translateMatrix_ak * scaleMatrix_ak *
                           xRotationMatrix_ak * yRotationMatrix_ak *
                           zRotationMatrix_ak;
     modelViewProjectionMatrix_ak =
       perspectiveProjectionMatrix_ak * modelViewMatrix_ak;
     glUniformMatrix4fv(
       mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix_ak);
     glActiveTexture(GL_TEXTURE0);
     glBindTexture(GL_TEXTURE_2D, kundali_texture_ak);
     glUniform1i(textureSamplerUniform_ak, 0);
     glBindVertexArray(vao_cube_ak);
     glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
     glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
     glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
     glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
     glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
     glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
     glBindVertexArray(0);
     glUseProgram(0);
    
    Update();
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

void Update(void)
{
    cubeAngle_ak = cubeAngle_ak + 0.1f;
    if (cubeAngle_ak > 360.0f) {
        cubeAngle_ak = 0.0f;
    }
    pyramidAngle_ak = pyramidAngle_ak + 0.1f;
    if (pyramidAngle_ak > 360.0f) {
        pyramidAngle_ak = 0.0f;
    }
}

-(void)startAnimation
{
    if(isAnimating == NO)
    {
        displayLink=[NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        isAnimating = NO;
    }
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    [self unintialize];
    [self release];
    exit(0);
    
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
}

-(void)unintialize
{
    if (vao_pyramid_ak) {
        glDeleteVertexArrays(1, &vao_pyramid_ak);
        vao_pyramid_ak = 0;
      }
      if (vbo_position_pyramid_ak) {
        glDeleteVertexArrays(1, &vbo_position_pyramid_ak);
        vbo_position_pyramid_ak = 0;
      }
      if (vbo_texture_pyramid_ak) {
        glDeleteVertexArrays(1, &vbo_texture_pyramid_ak);
        vbo_texture_pyramid_ak = 0;
      }
      if (vao_cube_ak) {
        glDeleteVertexArrays(1, &vao_cube_ak);
        vao_cube_ak = 0;
      }
      if (vbo_position_cube_ak) {
        glDeleteVertexArrays(1, &vbo_position_cube_ak);
        vbo_position_cube_ak = 0;
      }
      if (vbo_texture_cube_ak) {
        glDeleteVertexArrays(1, &vbo_texture_cube_ak);
        vbo_texture_cube_ak = 0;
      }
    if (gShaderProgramObject_ak) {
        glUseProgram(gShaderProgramObject_ak);
        GLsizei shaderCount_ak;
        glGetProgramiv(gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);
        
        GLuint* pShaders_ak = NULL;
        pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
        glGetAttachedShaders(gShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);
        
        for (int i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
            glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
            glDeleteShader(pShaders_ak[i_ak]);
            pShaders_ak[i_ak] = 0;
        }
        
        free(pShaders_ak);
        glDeleteProgram(gShaderProgramObject_ak);
        gShaderProgramObject_ak = 0;
        
        glUseProgram(0);
    }
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer = 0;
    }
    
    if (eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext){
            [EAGLContext setCurrentContext:nil];
        }
    }
    
}

- (void)dealloc
{
    [self unintialize];
    [super dealloc];
}

@end
