
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "sphere.h"
#import "vmath.h"

using namespace vmath;

GLfloat sphereAngle_ak = 0.0f;
GLfloat lightAmbient_ak[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffused_ak[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition_ak[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightSpecular_ak[] = { 1.0f, 1.0f, 1.0f };
GLfloat materialAmbient_ak[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat materialDiffused_ak[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat materialSpecular_ak[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat materialShininess_ak = 50.0f;
struct Light
{
  GLfloat lightAmbient_ak[3];
  GLfloat lightDiffused_ak[3];
  GLfloat lightPosition_ak[4];
  GLfloat lightSpecular_ak[3];
};
struct Light lights[3];
int pvpf = 0;
GLfloat lightAngle0_ak;
GLfloat lightAngle1_ak = 0.0;
GLfloat lightAngle2_ak = 0.0;
GLfloat angleForXRotation_ak = 0.0f;
GLfloat angleForYRotation_ak = 0.0f;
GLfloat angleForZRotation_ak = 0.0f;
bool bLight_ak = 0.0;
bool bAnimate_ak;
GLint keyPressed_ak = 0;
int gWidth = 800;
int gHeight = 600;

@implementation GLESView {
@private
  EAGLContext* eaglContext;

  GLuint defaultFramebuffer;
  GLuint colorRenderbuffer;
  GLuint depthRenderbuffer;

  id displayLink;
  NSInteger animationFrameInterval;
  BOOL isAnimating;

  enum
  {
    ATTRIBUTE_POSITION = 0,
    ATTRIBUTE_COLOR,
    ATTRIBUTE_NORMAL,
    ATTRIBUTE_TEXCOORD
  };
  GLuint gVertexShaderObject_ak;
  GLuint gFragmentShaderObject_ak;
  GLuint gShaderProgramObject_ak;
  GLuint vao_cube_ak;
  GLuint vbo_position_cube_ak;
  GLuint vbo_normal_cube_ak;
  GLuint modelViewMatrixUniform_ak;
  GLuint projectionMatrixUniform_ak;
  GLuint lKeyPressedUniform_ak;
  GLuint ldUniform_ak;
  GLuint kdUniform_ak;
  GLuint lightPositionUniform_ak;
  GLuint perVertexLighting_VertexShaderObject_ak;
  GLuint perVertexLighting_FragmentShaderObject_ak;
  GLuint perVertexLighting_ShaderProgramObject_ak;
  GLuint perVertexLighting_vao_sphere_ak;
  GLuint perVertexLighting_vbo_position_sphere_ak;
  GLuint perVertexLighting_vbo_normal_sphere_ak;
  GLuint perVertexLighting_vbo_element_sphere_ak;
  GLuint perVertexLighting_modelMatrixUniform_ak;
  GLuint perVertexLighting_viewMatrixUniform_ak;
  GLuint perVertexLighting_projectionMatrixUniform_ak;
  GLuint perVertexLighting_laUniform_ak;
  GLuint perVertexLighting_ldUniform_ak;
  GLuint perVertexLighting_lsUniform_ak;
  GLuint perVertexLighting_lightPositionUniform_ak;
  GLuint perVertexLighting_kaUniform_ak;
  GLuint perVertexLighting_kdUniform_ak;
  GLuint perVertexLighting_ksUniform_ak;
  GLuint perVertexLighting_materialShininessUniform_ak;
  GLuint perVertexLighting_lKeyPressedUniform_ak;
  GLuint perFragmentLighting_VertexShaderObject_ak;
  GLuint perFragmentLighting_FragmentShaderObject_ak;
  GLuint perFragmentLighting_ShaderProgramObject_ak;
  GLuint perFragmentLighting_vao_sphere_ak;
  GLuint perFragmentLighting_vbo_position_sphere_ak;
  GLuint perFragmentLighting_vbo_normal_sphere_ak;
  GLuint perFragmentLighting_vbo_element_sphere_ak;
  GLuint perFragmentLighting_modelMatrixUniform_ak;
  GLuint perFragmentLighting_viewMatrixUniform_ak;
  GLuint perFragmentLighting_projectionMatrixUniform_ak;
  GLuint perFragmentLighting_laUniform_ak;
  GLuint perFragmentLighting_ldUniform_ak;
  GLuint perFragmentLighting_lsUniform_ak;
  GLuint perFragmentLighting_lightPositionUniform_ak;
  GLuint perFragmentLighting_kaUniform_ak;
  GLuint perFragmentLighting_kdUniform_ak;
  GLuint perFragmentLighting_ksUniform_ak;
  GLuint perFragmentLighting_materialShininessUniform_ak;
  GLuint perFragmentLighting_lKeyPressedUniform_ak;
    
  float sphere_vertices_ak[1146];
  float sphere_normals_ak[1146];
  float sphere_textures_ak[764];
  unsigned short sphere_elements_ak[2280];
  int gNumVertices_ak;
  int gNumElements_ak;
  vmath::mat4 perspectiveProjectionMatrix_ak;
}

- (id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    CAEAGLLayer* eaglLayer = (CAEAGLLayer*)[super layer];

    [eaglLayer setOpaque:YES];

    [eaglLayer setDrawableProperties:[NSDictionary
                                       dictionaryWithObjectsAndKeys:
                                         [NSNumber numberWithBool:NO],
                                         kEAGLDrawablePropertyRetainedBacking,
                                         kEAGLColorFormatRGBA8,
                                         kEAGLDrawablePropertyColorFormat,
                                         nil]];

    eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
    if (eaglContext == nil) {
      printf("OpenGL-ES Context Creation Failed.\n");
      return (nil);
    }

    [EAGLContext setCurrentContext:eaglContext];

    glGenFramebuffers(1, &defaultFramebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    glGenRenderbuffers(1, &colorRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
    glFramebufferRenderbuffer(
      GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);

    GLint backingWidth;
    GLint backingHeight;
    glGetRenderbufferParameteriv(
      GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
    glGetRenderbufferParameteriv(
      GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);

    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(
      GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
    glFramebufferRenderbuffer(
      GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    printf("initWithFrame checking if framebuffer is complete\n");
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
      printf("Framebuffer is not complete\n");
      [self unintialize];
      return (nil);
    }

    printf("Renderer : %s\n", glGetString(GL_RENDERER));
    printf("GL Version : %s\n", glGetString(GL_VERSION));
    printf("GLES Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    animationFrameInterval = 60;
    isAnimating = NO;

    Sphere *sphere = new Sphere();
    sphere->getSphereVertexData(sphere_vertices_ak,
                               sphere_normals_ak,
                               sphere_textures_ak,
                               sphere_elements_ak);
    gNumVertices_ak = sphere->getNumberOfSphereVertices();
    gNumElements_ak = sphere->getNumberOfSphereElements();

    // shader block here
    lights[0].lightAmbient_ak[0] = 0.0f;
    lights[0].lightAmbient_ak[1] = 0.0f;
    lights[0].lightAmbient_ak[2] = 0.0f;
    lights[0].lightDiffused_ak[0] = 1.0f;
    lights[0].lightDiffused_ak[1] = 0.0f;
    lights[0].lightDiffused_ak[2] = 0.0f;
    lights[0].lightPosition_ak[0] = 0.0f;
    lights[0].lightPosition_ak[1] = 0.0f;
    lights[0].lightPosition_ak[2] = 0.0f;
    lights[0].lightPosition_ak[3] = 1.0f;
    lights[0].lightSpecular_ak[0] = 1.0f;
    lights[0].lightSpecular_ak[1] = 0.0f;
    lights[0].lightSpecular_ak[2] = 0.0f;

    lights[1].lightAmbient_ak[0] = 0.0f;
    lights[1].lightAmbient_ak[1] = 0.0f;
    lights[1].lightAmbient_ak[2] = 0.0f;
    lights[1].lightDiffused_ak[0] = 0.0f;
    lights[1].lightDiffused_ak[1] = 1.0f;
    lights[1].lightDiffused_ak[2] = 0.0f;
    lights[1].lightPosition_ak[0] = 0.0f;
    lights[1].lightPosition_ak[1] = 0.0f;
    lights[1].lightPosition_ak[2] = 0.0f;
    lights[1].lightPosition_ak[3] = 1.0f;
    lights[1].lightSpecular_ak[0] = 0.0f;
    lights[1].lightSpecular_ak[1] = 1.0f;
    lights[1].lightSpecular_ak[2] = 0.0f;

    lights[2].lightAmbient_ak[0] = 0.0f;
    lights[2].lightAmbient_ak[1] = 0.0f;
    lights[2].lightAmbient_ak[2] = 0.0f;
    lights[2].lightDiffused_ak[0] = 0.0f;
    lights[2].lightDiffused_ak[1] = 0.0f;
    lights[2].lightDiffused_ak[2] = 1.0f;
    lights[2].lightPosition_ak[0] = 0.0f;
    lights[2].lightPosition_ak[1] = 0.0f;
    lights[2].lightPosition_ak[2] = 0.0f;
    lights[2].lightPosition_ak[3] = 1.0f;
    lights[2].lightSpecular_ak[0] = 0.0f;
    lights[2].lightSpecular_ak[1] = 0.0f;
    lights[2].lightSpecular_ak[2] = 1.0f;

    perVertexLighting_VertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
      "#version 300 es"
      "\n"
      "precision highp float;"
      "precision highp int;"
      "in vec4 vPosition;"
      "in vec3 vNormal;"
      "uniform mat4 u_model_matrix;"
      "uniform mat4 u_view_matrix;"
      "uniform mat4 u_projection_matrix;"
      "uniform vec3 u_la;"
      "uniform vec3 u_ld;"
      "uniform vec3 u_ls;"
      "uniform vec4 u_light_position;"
      "uniform vec3 u_ka;"
      "uniform vec3 u_kd;"
      "uniform vec3 u_ks;"
      "uniform float u_material_shininess;"
      "uniform int u_lkey_pressed;"
      "out vec3 phong_ads_light;"
      "void main(void)"
      "{"
      "if(u_lkey_pressed == 1)"
      "{"
      "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"
      "vec3 transformed_normal = normalize(mat3((u_view_matrix * "
      "u_model_matrix)) * vNormal);"
      "vec3 light_direction = normalize(vec3(u_light_position - "
      "eye_coordinates));"
      "vec3 reflection_vector = reflect(-light_direction, transformed_normal);"
      "vec3 view_vector = normalize(-eye_coordinates.xyz);"
      "vec3 ambient = u_la * u_ka;"
      "vec3 diffuse = u_ld * u_kd * max(dot(light_direction, "
      "transformed_normal), 0.0);"
      "vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, "
      "view_vector), 0.0), u_material_shininess);"
      "phong_ads_light = ambient + diffuse + specular;"
      "}"
      "else"
      "{"
      "phong_ads_light = vec3(1.0, 1.0, 1.0);"
      "}"
      "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * "
      "vPosition;"
      "}";
    glShaderSource(perVertexLighting_VertexShaderObject_ak,
                   1,
                   (const GLchar**)&vertexShaderSourceCode,
                   NULL);
    glCompileShader(perVertexLighting_VertexShaderObject_ak);
    GLint infoLogLength_ak = 0;
    GLint shaderCompileStatus_ak = 0;
    char* szBuffer_ak = NULL;
    glGetShaderiv(perVertexLighting_VertexShaderObject_ak,
                  GL_COMPILE_STATUS,
                  &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
      glGetShaderiv(perVertexLighting_VertexShaderObject_ak,
                    GL_INFO_LOG_LENGTH,
                    &infoLogLength_ak);
      if (infoLogLength_ak > 0) {
        szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
        if (szBuffer_ak != NULL) {
          GLsizei written_ak;
          glGetShaderInfoLog(perVertexLighting_VertexShaderObject_ak,
                             infoLogLength_ak,
                             &written_ak,
                             szBuffer_ak);
          printf("Vertex shader compilation log: %s\n", szBuffer_ak);
          free(szBuffer_ak);
          [self unintialize];
        }
      }
    }
    perVertexLighting_FragmentShaderObject_ak =
      glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar* fragmentSourceCode_ak =
      "#version 300 es"
      "\n"
      "precision highp float;"
      "precision highp int;"
      "in vec3 phong_ads_light;"
      "out vec4 FragColor;"
      "void main(void)"
      "{"
      "FragColor = vec4(phong_ads_light, 1.0);"
      "}";
    glShaderSource(perVertexLighting_FragmentShaderObject_ak,
                   1,
                   (const GLchar**)&fragmentSourceCode_ak,
                   NULL);
    glCompileShader(perVertexLighting_FragmentShaderObject_ak);
    infoLogLength_ak = 0;
    shaderCompileStatus_ak = 0;
    szBuffer_ak = NULL;
    glGetShaderiv(perVertexLighting_FragmentShaderObject_ak,
                  GL_COMPILE_STATUS,
                  &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
      glGetShaderiv(perVertexLighting_FragmentShaderObject_ak,
                    GL_INFO_LOG_LENGTH,
                    &infoLogLength_ak);
      if (infoLogLength_ak > 0) {
        szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
        if (szBuffer_ak != NULL) {
          GLsizei written_ak;
          glGetShaderInfoLog(perVertexLighting_FragmentShaderObject_ak,
                             infoLogLength_ak,
                             &written_ak,
                             szBuffer_ak);
          printf("Fragment shader compilation log: %s\n", szBuffer_ak);
          free(szBuffer_ak);
          [self unintialize];
        }
      }
    }
    perVertexLighting_ShaderProgramObject_ak = glCreateProgram();
    glAttachShader(perVertexLighting_ShaderProgramObject_ak,
                   perVertexLighting_VertexShaderObject_ak);
    glAttachShader(perVertexLighting_ShaderProgramObject_ak,
                   perVertexLighting_FragmentShaderObject_ak);
    glBindAttribLocation(perVertexLighting_ShaderProgramObject_ak,
                         ATTRIBUTE_POSITION,
                         "vPosition");
    glBindAttribLocation(perVertexLighting_ShaderProgramObject_ak,
                         ATTRIBUTE_NORMAL,
                         "vNormal");
    glLinkProgram(perVertexLighting_ShaderProgramObject_ak);
    GLint shaderProgramLinkStatus = 0;
    glGetProgramiv(perVertexLighting_ShaderProgramObject_ak,
                   GL_LINK_STATUS,
                   &shaderProgramLinkStatus);
    if (shaderProgramLinkStatus == GL_FALSE) {
      glGetProgramiv(perVertexLighting_ShaderProgramObject_ak,
                     GL_INFO_LOG_LENGTH,
                     &infoLogLength_ak);
      if (infoLogLength_ak > 0) {
        szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
        if (szBuffer_ak != NULL) {
          GLsizei written_ak;
          glGetProgramInfoLog(perVertexLighting_ShaderProgramObject_ak,
                              infoLogLength_ak,
                              &written_ak,
                              szBuffer_ak);
          printf("Shader program link log: %s\n", szBuffer_ak);
          free(szBuffer_ak);
          [self unintialize];
        }
      }
    }
    perVertexLighting_modelMatrixUniform_ak = glGetUniformLocation(
      perVertexLighting_ShaderProgramObject_ak, "u_model_matrix");
    perVertexLighting_viewMatrixUniform_ak = glGetUniformLocation(
      perVertexLighting_ShaderProgramObject_ak, "u_view_matrix");
    perVertexLighting_projectionMatrixUniform_ak = glGetUniformLocation(
      perVertexLighting_ShaderProgramObject_ak, "u_projection_matrix");
    perVertexLighting_laUniform_ak =
      glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_la");
    perVertexLighting_ldUniform_ak =
      glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_ld");
    perVertexLighting_lsUniform_ak =
      glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_ls");
    perVertexLighting_lightPositionUniform_ak = glGetUniformLocation(
      perVertexLighting_ShaderProgramObject_ak, "u_light_position");
    perVertexLighting_kaUniform_ak =
      glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_ka");
    perVertexLighting_kdUniform_ak =
      glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_kd");
    perVertexLighting_ksUniform_ak =
      glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_ks");
    perVertexLighting_materialShininessUniform_ak = glGetUniformLocation(
      perVertexLighting_ShaderProgramObject_ak, "u_material_shininess");
    perVertexLighting_lKeyPressedUniform_ak = glGetUniformLocation(
      perVertexLighting_ShaderProgramObject_ak, "u_lkey_pressed");
    glGenVertexArrays(1, &perVertexLighting_vao_sphere_ak);
    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glGenBuffers(1, &perVertexLighting_vbo_position_sphere_ak);
    glBindBuffer(GL_ARRAY_BUFFER, perVertexLighting_vbo_position_sphere_ak);
    glBufferData(GL_ARRAY_BUFFER,
                 sizeof(sphere_vertices_ak),
                 sphere_vertices_ak,
                 GL_STATIC_DRAW);
    glVertexAttribPointer(
      ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glGenBuffers(1, &perVertexLighting_vbo_normal_sphere_ak);
    glBindBuffer(GL_ARRAY_BUFFER, perVertexLighting_vbo_normal_sphere_ak);
    glBufferData(GL_ARRAY_BUFFER,
                 sizeof(sphere_normals_ak),
                 sphere_normals_ak,
                 GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glGenBuffers(1, &perVertexLighting_vbo_element_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                 perVertexLighting_vbo_element_sphere_ak);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 sizeof(sphere_elements_ak),
                 sphere_elements_ak,
                 GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    perFragmentLighting_VertexShaderObject_ak =
      glCreateShader(GL_VERTEX_SHADER);
    const GLchar* perFragmentLighting_vertexShaderSourceCode =
      "#version 300 es"
      "\n"
      "precision highp float;"
      "precision highp int;"
      "in vec4 vPosition;\n"
      "in vec3 vNormal;\n"
      "uniform mat4 u_model_matrix;"
      "uniform mat4 u_view_matrix;"
      "uniform mat4 u_projection_matrix;"
      "uniform vec4 u_light_position;"
      "uniform int u_lkey_pressed;"
      "out vec3 transformed_normal;"
      "out vec3 light_direction;"
      "out vec3 view_vector;"
      "void main(void)"
      "{"
      "if(u_lkey_pressed == 1)"
      "{"
      "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"
      "transformed_normal = mat3((u_view_matrix * u_model_matrix)) * vNormal;"
      "light_direction = vec3(u_light_position - eye_coordinates);"
      "view_vector = -eye_coordinates.xyz;"
      "}"
      "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * "
      "vPosition;"
      "}";
    glShaderSource(perFragmentLighting_VertexShaderObject_ak,
                   1,
                   (const GLchar**)&perFragmentLighting_vertexShaderSourceCode,
                   NULL);
    glCompileShader(perFragmentLighting_VertexShaderObject_ak);
    infoLogLength_ak = 0;
    shaderCompileStatus_ak = 0;
    szBuffer_ak = NULL;
    glGetShaderiv(perFragmentLighting_VertexShaderObject_ak,
                  GL_COMPILE_STATUS,
                  &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
      glGetShaderiv(perFragmentLighting_VertexShaderObject_ak,
                    GL_INFO_LOG_LENGTH,
                    &infoLogLength_ak);
      if (infoLogLength_ak > 0) {
        szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
        if (szBuffer_ak != NULL) {
          GLsizei written_ak;
          glGetShaderInfoLog(perFragmentLighting_VertexShaderObject_ak,
                             infoLogLength_ak,
                             &written_ak,
                             szBuffer_ak);
          printf("Vertex shader compilation log: %s\n", szBuffer_ak);
          free(szBuffer_ak);
          [self unintialize];
        }
      }
    }
    perFragmentLighting_FragmentShaderObject_ak =
      glCreateShader(GL_FRAGMENT_SHADER);
    perFragmentLighting_FragmentShaderObject_ak =
      glCreateShader(GL_FRAGMENT_SHADER);
    fragmentSourceCode_ak =
      "#version 300 es"
      "\n"
      "precision highp float;"
      "precision highp int;"
      "in vec3 transformed_normal;"
      "in vec3 light_direction;"
      "in vec3 view_vector;"
      "uniform vec3 u_la;"
      "uniform vec3 u_ld;"
      "uniform vec3 u_ls;"
      "uniform vec3 u_ka;"
      "uniform vec3 u_kd;"
      "uniform vec3 u_ks;"
      "uniform float u_material_shininess;"
      "uniform int u_lkey_pressed;"
      "out vec4 FragColor;"
      "void main(void)"
      "{"
      "vec3 phong_ads_light;"
      "if(u_lkey_pressed == 1)"
      "{"
      "vec3 normalized_transformed_normal = normalize(transformed_normal);"
      "vec3 normalized_light_direction = normalize(light_direction);"
      "vec3 normalized_view_vector = normalize(view_vector);"
      "vec3 reflection_vector = reflect(-normalized_light_direction, "
      "normalized_transformed_normal);"
      "vec3 ambient = u_la * u_ka;"
      "vec3 diffuse = u_ld * u_kd * max(dot(normalized_light_direction, "
      "normalized_transformed_normal), 0.0);"
      "vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, "
      "normalized_view_vector), 0.0), u_material_shininess);"
      "phong_ads_light = ambient + diffuse + specular;"
      "}"
      "else"
      "{"
      "phong_ads_light = vec3(1.0, 1.0, 1.0);"
      "}"
      "FragColor = vec4(phong_ads_light, 1.0);"
      "}";
    glShaderSource(perFragmentLighting_FragmentShaderObject_ak,
                   1,
                   (const GLchar**)&fragmentSourceCode_ak,
                   NULL);
    glCompileShader(perFragmentLighting_FragmentShaderObject_ak);
    infoLogLength_ak = 0;
    shaderCompileStatus_ak = 0;
    szBuffer_ak = NULL;
    glGetShaderiv(perFragmentLighting_FragmentShaderObject_ak,
                  GL_COMPILE_STATUS,
                  &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
      glGetShaderiv(perFragmentLighting_FragmentShaderObject_ak,
                    GL_INFO_LOG_LENGTH,
                    &infoLogLength_ak);
      if (infoLogLength_ak > 0) {
        szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
        if (szBuffer_ak != NULL) {
          GLsizei written_ak;
          glGetShaderInfoLog(perFragmentLighting_FragmentShaderObject_ak,
                             infoLogLength_ak,
                             &written_ak,
                             szBuffer_ak);
          printf("Fragment shader compilation log: %s\n", szBuffer_ak);
          free(szBuffer_ak);
          [self unintialize];
        }
      }
    }
    perFragmentLighting_ShaderProgramObject_ak = glCreateProgram();
    glAttachShader(perFragmentLighting_ShaderProgramObject_ak,
                   perFragmentLighting_VertexShaderObject_ak);
    glAttachShader(perFragmentLighting_ShaderProgramObject_ak,
                   perFragmentLighting_FragmentShaderObject_ak);
    glBindAttribLocation(perFragmentLighting_ShaderProgramObject_ak,
                         ATTRIBUTE_POSITION,
                         "vPosition");
    glBindAttribLocation(perFragmentLighting_ShaderProgramObject_ak,
                         ATTRIBUTE_NORMAL,
                         "vNormal");
    glLinkProgram(perFragmentLighting_ShaderProgramObject_ak);
    shaderProgramLinkStatus = 0;
    glGetProgramiv(perFragmentLighting_ShaderProgramObject_ak,
                   GL_LINK_STATUS,
                   &shaderProgramLinkStatus);
    if (shaderProgramLinkStatus == GL_FALSE) {
      glGetProgramiv(perFragmentLighting_ShaderProgramObject_ak,
                     GL_INFO_LOG_LENGTH,
                     &infoLogLength_ak);
      if (infoLogLength_ak > 0) {
        szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
        if (szBuffer_ak != NULL) {
          GLsizei written_ak;
          glGetProgramInfoLog(perFragmentLighting_ShaderProgramObject_ak,
                              infoLogLength_ak,
                              &written_ak,
                              szBuffer_ak);
          printf("Shader program link log: %s\n", szBuffer_ak);
          free(szBuffer_ak);
          [self unintialize];
        }
      }
    }
    perFragmentLighting_modelMatrixUniform_ak = glGetUniformLocation(
      perFragmentLighting_ShaderProgramObject_ak, "u_model_matrix");
    perFragmentLighting_viewMatrixUniform_ak = glGetUniformLocation(
      perFragmentLighting_ShaderProgramObject_ak, "u_view_matrix");
    perFragmentLighting_projectionMatrixUniform_ak = glGetUniformLocation(
      perFragmentLighting_ShaderProgramObject_ak, "u_projection_matrix");
    perFragmentLighting_laUniform_ak =
      glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_la");
    perFragmentLighting_ldUniform_ak =
      glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_ld");
    perFragmentLighting_lsUniform_ak =
      glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_ls");
    perFragmentLighting_lightPositionUniform_ak = glGetUniformLocation(
      perFragmentLighting_ShaderProgramObject_ak, "u_light_position");
    perFragmentLighting_kaUniform_ak =
      glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_ka");
    perFragmentLighting_kdUniform_ak =
      glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_kd");
    perFragmentLighting_ksUniform_ak =
      glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_ks");
    perFragmentLighting_materialShininessUniform_ak = glGetUniformLocation(
      perFragmentLighting_ShaderProgramObject_ak, "u_material_shininess");
    perFragmentLighting_lKeyPressedUniform_ak = glGetUniformLocation(
      perFragmentLighting_ShaderProgramObject_ak, "u_lkey_pressed");
    glGenVertexArrays(1, &perFragmentLighting_vao_sphere_ak);
    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glGenBuffers(1, &perFragmentLighting_vbo_position_sphere_ak);
    glBindBuffer(GL_ARRAY_BUFFER, perFragmentLighting_vbo_position_sphere_ak);
    glBufferData(GL_ARRAY_BUFFER,
                 sizeof(sphere_vertices_ak),
                 sphere_vertices_ak,
                 GL_STATIC_DRAW);
    glVertexAttribPointer(
      ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glGenBuffers(1, &perFragmentLighting_vbo_normal_sphere_ak);
    glBindBuffer(GL_ARRAY_BUFFER, perFragmentLighting_vbo_normal_sphere_ak);
    glBufferData(GL_ARRAY_BUFFER,
                 sizeof(sphere_normals_ak),
                 sphere_normals_ak,
                 GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glGenBuffers(1, &perFragmentLighting_vbo_element_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                 perFragmentLighting_vbo_element_sphere_ak);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 sizeof(sphere_elements_ak),
                 sphere_elements_ak,
                 GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    perspectiveProjectionMatrix_ak = vmath::mat4::identity();

    printf("Init done\n");

    UITapGestureRecognizer* singleTapGestureRecognizer =
      [[UITapGestureRecognizer alloc] initWithTarget:self
                                              action:@selector(onSingleTap:)];

    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
    [singleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:singleTapGestureRecognizer];

    UITapGestureRecognizer* doubleTapGestureRecognizer =
      [[UITapGestureRecognizer alloc] initWithTarget:self
                                              action:@selector(onDoubleTap:)];

    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
    [doubleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:doubleTapGestureRecognizer];

    [singleTapGestureRecognizer
      requireGestureRecognizerToFail:doubleTapGestureRecognizer];

    UISwipeGestureRecognizer* swipeGestureRecognizer =
      [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(onSwipe:)];
    [swipeGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:swipeGestureRecognizer];

    UILongPressGestureRecognizer* longPressGestureRecognizer =
      [[UILongPressGestureRecognizer alloc]
        initWithTarget:self
                action:@selector(onLongPress:)];
    [longPressGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:longPressGestureRecognizer];
  }
  return (self);
}

+ (Class)layerClass
{
  return ([CAEAGLLayer class]);
}

/*
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 [self loadTextureFromBMPFile:@"Smiley":@"bmp"];

 }
 */

- (void)layoutSubviews
{
  glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
  [eaglContext renderbufferStorage:GL_RENDERBUFFER
                      fromDrawable:(CAEAGLLayer*)[self layer]];
  glFramebufferRenderbuffer(
    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);

  GLint width;
  GLint height;
  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
  glGetRenderbufferParameteriv(
    GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);

  glGenRenderbuffers(1, &depthRenderbuffer);
  glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
  glFramebufferRenderbuffer(
    GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);

  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
    printf("Framebuffer is not complete in layoutSubviews");
  }

  if (height < 0) {
    height = 1;
  }
  gWidth = width;
  gHeight = height;
  glViewport(0, 0, (GLsizei)width, (GLsizei)height);
  perspectiveProjectionMatrix_ak =
    vmath::perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
  [self drawView:nil];
}

- (void)drawView:(id)sender
{
  [EAGLContext setCurrentContext:eaglContext];
  glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  switch (pvpf) {
    case 1: {
      glUseProgram(perFragmentLighting_ShaderProgramObject_ak);
      mat4 modelMatrix_ak;
      mat4 viewMatrix_ak;
      mat4 translateMatrix_ak;
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
      } else {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 0);
      }
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      GLfloat materialAmbient_ak[4];
      GLfloat materialDiffused_ak[4];
      GLfloat materialSpecular_ak[4];
      GLfloat materialShininess_ak;
      // emerald
      materialAmbient_ak[0] = 0.0215f;
      materialAmbient_ak[1] = 0.1745f;
      materialAmbient_ak[2] = 0.0215f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.07568f;
      materialDiffused_ak[1] = 0.61424f;
      materialDiffused_ak[2] = 0.07568f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.633f;
      materialSpecular_ak[1] = 0.727811f;
      materialSpecular_ak[2] = 0.633f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.6f * 128.0f;
      glViewport(gWidth * (-0.40f), gHeight * (0.35f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // jade
      materialAmbient_ak[0] = 0.135f;
      materialAmbient_ak[1] = 0.2225f;
      materialAmbient_ak[2] = 0.1575f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.54f;
      materialDiffused_ak[1] = 0.89f;
      materialDiffused_ak[2] = 0.63f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.316228f;
      materialSpecular_ak[1] = 0.316228f;
      materialSpecular_ak[2] = 0.316228f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.1 * 128.0f;
      glViewport(gWidth * (-0.40f), gHeight * (0.20f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // obsidian
      materialAmbient_ak[0] = 0.05375f;
      materialAmbient_ak[1] = 0.05f;
      materialAmbient_ak[2] = 0.06625f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.18275f;
      materialDiffused_ak[1] = 0.17f;
      materialDiffused_ak[2] = 0.22525f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.332741f;
      materialSpecular_ak[1] = 0.328634f;
      materialSpecular_ak[2] = 0.346435f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.3 * 128.0f;
      glViewport(gWidth * (-0.40f), gHeight * (0.05f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // pearl
      materialAmbient_ak[0] = 0.25f;
      materialAmbient_ak[1] = 0.20725f;
      materialAmbient_ak[2] = 0.20725f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 1.0f;
      materialDiffused_ak[1] = 0.829f;
      materialDiffused_ak[2] = 0.829f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.296648f;
      materialSpecular_ak[1] = 0.296648f;
      materialSpecular_ak[2] = 0.296648f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.088 * 128.0f;
      glViewport(gWidth * (-0.40f), gHeight * (-0.10), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // ruby
      materialAmbient_ak[0] = 0.1745f;
      materialAmbient_ak[1] = 0.01175f;
      materialAmbient_ak[2] = 0.01175f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.61424f;
      materialDiffused_ak[1] = 0.04136f;
      materialDiffused_ak[2] = 0.04136f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.727811f;
      materialSpecular_ak[1] = 0.626959f;
      materialSpecular_ak[2] = 0.626959f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.6 * 128.0f;
      glViewport(gWidth * (-0.40f), gHeight * (-0.25f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // turquoise
      materialAmbient_ak[0] = 0.1f;
      materialAmbient_ak[1] = 0.18725f;
      materialAmbient_ak[2] = 0.1745f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.396f;
      materialDiffused_ak[1] = 0.74151f;
      materialDiffused_ak[2] = 0.69102f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.297254f;
      materialSpecular_ak[1] = 0.30829f;
      materialSpecular_ak[2] = 0.306678f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.1 * 128.0f;
      glViewport(gWidth * (-0.40f), gHeight * (-0.40f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // brass
      materialAmbient_ak[0] = 0.329412f;
      materialAmbient_ak[1] = 0.223529f;
      materialAmbient_ak[2] = 0.027451f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.780392f;
      materialDiffused_ak[1] = 0.568627f;
      materialDiffused_ak[2] = 0.113725f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.992157f;
      materialSpecular_ak[1] = 0.941176f;
      materialSpecular_ak[2] = 0.807843f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.21794872 * 128.0f;
      glViewport(gWidth * (-0.145f), gHeight * (0.35f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // bronze
      materialAmbient_ak[0] = 0.2125f;
      materialAmbient_ak[1] = 0.1275f;
      materialAmbient_ak[2] = 0.054f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.714f;
      materialDiffused_ak[1] = 0.4284f;
      materialDiffused_ak[2] = 0.18144f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.393548f;
      materialSpecular_ak[1] = 0.271906f;
      materialSpecular_ak[2] = 0.166721f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.2 * 128.0f;
      glViewport(gWidth * (-0.145f), gHeight * (0.20f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // chrome
      materialAmbient_ak[0] = 0.25f;
      materialAmbient_ak[1] = 0.25f;
      materialAmbient_ak[2] = 0.25f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.4f;
      materialDiffused_ak[1] = 0.4f;
      materialDiffused_ak[2] = 0.4f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.774597f;
      materialSpecular_ak[1] = 0.774597f;
      materialSpecular_ak[2] = 0.774597f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.6 * 128.0f;
      glViewport(gWidth * (-0.145f), gHeight * (0.05f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // copper
      materialAmbient_ak[0] = 0.19125f;
      materialAmbient_ak[1] = 0.0735f;
      materialAmbient_ak[2] = 0.0225f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.7038f;
      materialDiffused_ak[1] = 0.27048f;
      materialDiffused_ak[2] = 0.0828f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.256777f;
      materialSpecular_ak[1] = 0.137622f;
      materialSpecular_ak[2] = 0.086014f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.1 * 128.0f;
      glViewport(gWidth * (-0.145f), gHeight * (-0.1f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // gold
      materialAmbient_ak[0] = 0.24725f;
      materialAmbient_ak[1] = 0.1995f;
      materialAmbient_ak[2] = 0.0745f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.75164f;
      materialDiffused_ak[1] = 0.60648f;
      materialDiffused_ak[2] = 0.22648f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.628281f;
      materialSpecular_ak[1] = 0.555802f;
      materialSpecular_ak[2] = 0.366065f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.4 * 128.0f;
      glViewport(gWidth * (-0.145f), gHeight * (-0.25f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // silver
      materialAmbient_ak[0] = 0.19225f;
      materialAmbient_ak[1] = 0.19225f;
      materialAmbient_ak[2] = 0.19225f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.50754f;
      materialDiffused_ak[1] = 0.50754f;
      materialDiffused_ak[2] = 0.50754f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.508273f;
      materialSpecular_ak[1] = 0.508273f;
      materialSpecular_ak[2] = 0.508273f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.4 * 128.0f;
      glViewport(gWidth * (-0.145f), gHeight * (-0.40f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // black
      materialAmbient_ak[0] = 0.0f;
      materialAmbient_ak[1] = 0.0f;
      materialAmbient_ak[2] = 0.0f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.01f;
      materialDiffused_ak[1] = 0.01f;
      materialDiffused_ak[2] = 0.01f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.50f;
      materialSpecular_ak[1] = 0.50f;
      materialSpecular_ak[2] = 0.50f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.25 * 128.0f;
      glViewport(gWidth * (0.145f), gHeight * (0.35f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // cyan
      materialAmbient_ak[0] = 0.0f;
      materialAmbient_ak[1] = 0.1f;
      materialAmbient_ak[2] = 0.06f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.0f;
      materialDiffused_ak[1] = 0.50980392f;
      materialDiffused_ak[2] = 0.50980392f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.50196078f;
      materialSpecular_ak[1] = 0.50196078f;
      materialSpecular_ak[2] = 0.50196078f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.25 * 128.0f;
      glViewport(gWidth * (0.145f), gHeight * (0.20f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // green
      materialAmbient_ak[0] = 0.0f;
      materialAmbient_ak[1] = 0.0f;
      materialAmbient_ak[2] = 0.0f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.1f;
      materialDiffused_ak[1] = 0.35f;
      materialDiffused_ak[2] = 0.1f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.45f;
      materialSpecular_ak[1] = 0.55f;
      materialSpecular_ak[2] = 0.45f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.25 * 128.0f;
      glViewport(gWidth * (0.145f), gHeight * (0.05f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // red
      materialAmbient_ak[0] = 0.0f;
      materialAmbient_ak[1] = 0.0f;
      materialAmbient_ak[2] = 0.0f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.5f;
      materialDiffused_ak[1] = 0.0f;
      materialDiffused_ak[2] = 0.0f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.7f;
      materialSpecular_ak[1] = 0.6f;
      materialSpecular_ak[2] = 0.6f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.25 * 128.0f;
      glViewport(gWidth * (0.145f), gHeight * (-0.10f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // white
      materialAmbient_ak[0] = 0.0f;
      materialAmbient_ak[1] = 0.0f;
      materialAmbient_ak[2] = 0.0f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.55f;
      materialDiffused_ak[1] = 0.55f;
      materialDiffused_ak[2] = 0.55f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.70f;
      materialSpecular_ak[1] = 0.70f;
      materialSpecular_ak[2] = 0.70f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.25 * 128.0f;
      glViewport(gWidth * (0.145f), gHeight * (-0.25f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // yellow plastic
      materialAmbient_ak[0] = 0.0f;
      materialAmbient_ak[1] = 0.0f;
      materialAmbient_ak[2] = 0.0f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.5f;
      materialDiffused_ak[1] = 0.5f;
      materialDiffused_ak[2] = 0.0f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.60f;
      materialSpecular_ak[1] = 0.60f;
      materialSpecular_ak[2] = 0.50f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.25 * 128.0f;
      glViewport(gWidth * (0.145f), gHeight * (-0.40f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // black
      materialAmbient_ak[0] = 0.02f;
      materialAmbient_ak[1] = 0.02f;
      materialAmbient_ak[2] = 0.02f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.01f;
      materialDiffused_ak[1] = 0.01f;
      materialDiffused_ak[2] = 0.01f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.4f;
      materialSpecular_ak[1] = 0.4f;
      materialSpecular_ak[2] = 0.4f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.078125 * 128.0f;
      glViewport(gWidth * (0.40f), gHeight * (0.35f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // cyan
      materialAmbient_ak[0] = 0.0f;
      materialAmbient_ak[1] = 0.05f;
      materialAmbient_ak[2] = 0.05f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.4f;
      materialDiffused_ak[1] = 0.5f;
      materialDiffused_ak[2] = 0.5f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.04f;
      materialSpecular_ak[1] = 0.7f;
      materialSpecular_ak[2] = 0.7f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.078125 * 128.0f;
      glViewport(gWidth * (0.40f), gHeight * (0.20f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // green
      materialAmbient_ak[0] = 0.0f;
      materialAmbient_ak[1] = 0.05f;
      materialAmbient_ak[2] = 0.0f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.4f;
      materialDiffused_ak[1] = 0.5f;
      materialDiffused_ak[2] = 0.4f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.04f;
      materialSpecular_ak[1] = 0.7f;
      materialSpecular_ak[2] = 0.04f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.078125 * 128.0f;
      glViewport(gWidth * (0.40f), gHeight * (0.05f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // red
      materialAmbient_ak[0] = 0.05f;
      materialAmbient_ak[1] = 0.0f;
      materialAmbient_ak[2] = 0.0f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.5f;
      materialDiffused_ak[1] = 0.4f;
      materialDiffused_ak[2] = 0.4f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.7f;
      materialSpecular_ak[1] = 0.04f;
      materialSpecular_ak[2] = 0.04f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.078125 * 128.0f;
      glViewport(gWidth * (0.40f), gHeight * (-0.10f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // white
      materialAmbient_ak[0] = 0.05f;
      materialAmbient_ak[1] = 0.05f;
      materialAmbient_ak[2] = 0.05f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.5f;
      materialDiffused_ak[1] = 0.5f;
      materialDiffused_ak[2] = 0.5f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.7f;
      materialSpecular_ak[1] = 0.7f;
      materialSpecular_ak[2] = 0.7f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.078125 * 128.0f;
      glViewport(gWidth * (0.40f), gHeight * (-0.25f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // yellow rubber
      materialAmbient_ak[0] = 0.05f;
      materialAmbient_ak[1] = 0.05f;
      materialAmbient_ak[2] = 0.0f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.5f;
      materialDiffused_ak[1] = 0.5f;
      materialDiffused_ak[2] = 0.4f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.7f;
      materialSpecular_ak[1] = 0.7f;
      materialSpecular_ak[2] = 0.04f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.078125 * 128.0f;
      glViewport(gWidth * (0.40f), gHeight * (-0.40f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(
          perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         modelMatrix_ak);
      glUniformMatrix4fv(
        perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perFragmentLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perFragmentLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      glUseProgram(0);
      break;
    }
    case 0: {
      glUseProgram(perVertexLighting_ShaderProgramObject_ak);
      mat4 modelMatrix_ak;
      mat4 viewMatrix_ak;
      mat4 translateMatrix_ak;
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
      } else {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 0);
      }
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      GLfloat materialAmbient_ak[4];
      GLfloat materialDiffused_ak[4];
      GLfloat materialSpecular_ak[4];
      GLfloat materialShininess_ak;
      // emerald
      materialAmbient_ak[0] = 0.0215f;
      materialAmbient_ak[1] = 0.1745f;
      materialAmbient_ak[2] = 0.0215f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.07568f;
      materialDiffused_ak[1] = 0.61424f;
      materialDiffused_ak[2] = 0.07568f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.633f;
      materialSpecular_ak[1] = 0.727811f;
      materialSpecular_ak[2] = 0.633f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.6f * 128.0f;
      glViewport(gWidth * (-0.40f), gHeight * (0.35f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // jade
      materialAmbient_ak[0] = 0.135f;
      materialAmbient_ak[1] = 0.2225f;
      materialAmbient_ak[2] = 0.1575f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.54f;
      materialDiffused_ak[1] = 0.89f;
      materialDiffused_ak[2] = 0.63f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.316228f;
      materialSpecular_ak[1] = 0.316228f;
      materialSpecular_ak[2] = 0.316228f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.1 * 128.0f;
      glViewport(gWidth * (-0.40f), gHeight * (0.20f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // obsidian
      materialAmbient_ak[0] = 0.05375f;
      materialAmbient_ak[1] = 0.05f;
      materialAmbient_ak[2] = 0.06625f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.18275f;
      materialDiffused_ak[1] = 0.17f;
      materialDiffused_ak[2] = 0.22525f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.332741f;
      materialSpecular_ak[1] = 0.328634f;
      materialSpecular_ak[2] = 0.346435f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.3 * 128.0f;
      glViewport(gWidth * (-0.40f), gHeight * (0.05f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // pearl
      materialAmbient_ak[0] = 0.25f;
      materialAmbient_ak[1] = 0.20725f;
      materialAmbient_ak[2] = 0.20725f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 1.0f;
      materialDiffused_ak[1] = 0.829f;
      materialDiffused_ak[2] = 0.829f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.296648f;
      materialSpecular_ak[1] = 0.296648f;
      materialSpecular_ak[2] = 0.296648f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.088 * 128.0f;
      glViewport(gWidth * (-0.40f), gHeight * (-0.10), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // ruby
      materialAmbient_ak[0] = 0.1745f;
      materialAmbient_ak[1] = 0.01175f;
      materialAmbient_ak[2] = 0.01175f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.61424f;
      materialDiffused_ak[1] = 0.04136f;
      materialDiffused_ak[2] = 0.04136f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.727811f;
      materialSpecular_ak[1] = 0.626959f;
      materialSpecular_ak[2] = 0.626959f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.6 * 128.0f;
      glViewport(gWidth * (-0.40f), gHeight * (-0.25f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // turquoise
      materialAmbient_ak[0] = 0.1f;
      materialAmbient_ak[1] = 0.18725f;
      materialAmbient_ak[2] = 0.1745f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.396f;
      materialDiffused_ak[1] = 0.74151f;
      materialDiffused_ak[2] = 0.69102f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.297254f;
      materialSpecular_ak[1] = 0.30829f;
      materialSpecular_ak[2] = 0.306678f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.1 * 128.0f;
      glViewport(gWidth * (-0.40f), gHeight * (-0.40f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // brass
      materialAmbient_ak[0] = 0.329412f;
      materialAmbient_ak[1] = 0.223529f;
      materialAmbient_ak[2] = 0.027451f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.780392f;
      materialDiffused_ak[1] = 0.568627f;
      materialDiffused_ak[2] = 0.113725f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.992157f;
      materialSpecular_ak[1] = 0.941176f;
      materialSpecular_ak[2] = 0.807843f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.21794872 * 128.0f;
      glViewport(gWidth * (-0.145f), gHeight * (0.35f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // bronze
      materialAmbient_ak[0] = 0.2125f;
      materialAmbient_ak[1] = 0.1275f;
      materialAmbient_ak[2] = 0.054f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.714f;
      materialDiffused_ak[1] = 0.4284f;
      materialDiffused_ak[2] = 0.18144f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.393548f;
      materialSpecular_ak[1] = 0.271906f;
      materialSpecular_ak[2] = 0.166721f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.2 * 128.0f;
      glViewport(gWidth * (-0.145f), gHeight * (0.20f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // chrome
      materialAmbient_ak[0] = 0.25f;
      materialAmbient_ak[1] = 0.25f;
      materialAmbient_ak[2] = 0.25f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.4f;
      materialDiffused_ak[1] = 0.4f;
      materialDiffused_ak[2] = 0.4f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.774597f;
      materialSpecular_ak[1] = 0.774597f;
      materialSpecular_ak[2] = 0.774597f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.6 * 128.0f;
      glViewport(gWidth * (-0.145f), gHeight * (0.05f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // copper
      materialAmbient_ak[0] = 0.19125f;
      materialAmbient_ak[1] = 0.0735f;
      materialAmbient_ak[2] = 0.0225f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.7038f;
      materialDiffused_ak[1] = 0.27048f;
      materialDiffused_ak[2] = 0.0828f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.256777f;
      materialSpecular_ak[1] = 0.137622f;
      materialSpecular_ak[2] = 0.086014f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.1 * 128.0f;
      glViewport(gWidth * (-0.145f), gHeight * (-0.1f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // gold
      materialAmbient_ak[0] = 0.24725f;
      materialAmbient_ak[1] = 0.1995f;
      materialAmbient_ak[2] = 0.0745f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.75164f;
      materialDiffused_ak[1] = 0.60648f;
      materialDiffused_ak[2] = 0.22648f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.628281f;
      materialSpecular_ak[1] = 0.555802f;
      materialSpecular_ak[2] = 0.366065f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.4 * 128.0f;
      glViewport(gWidth * (-0.145f), gHeight * (-0.25f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // silver
      materialAmbient_ak[0] = 0.19225f;
      materialAmbient_ak[1] = 0.19225f;
      materialAmbient_ak[2] = 0.19225f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.50754f;
      materialDiffused_ak[1] = 0.50754f;
      materialDiffused_ak[2] = 0.50754f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.508273f;
      materialSpecular_ak[1] = 0.508273f;
      materialSpecular_ak[2] = 0.508273f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.4 * 128.0f;
      glViewport(gWidth * (-0.145f), gHeight * (-0.40f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // black
      materialAmbient_ak[0] = 0.0f;
      materialAmbient_ak[1] = 0.0f;
      materialAmbient_ak[2] = 0.0f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.01f;
      materialDiffused_ak[1] = 0.01f;
      materialDiffused_ak[2] = 0.01f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.50f;
      materialSpecular_ak[1] = 0.50f;
      materialSpecular_ak[2] = 0.50f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.25 * 128.0f;
      glViewport(gWidth * (0.145f), gHeight * (0.35f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // cyan
      materialAmbient_ak[0] = 0.0f;
      materialAmbient_ak[1] = 0.1f;
      materialAmbient_ak[2] = 0.06f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.0f;
      materialDiffused_ak[1] = 0.50980392f;
      materialDiffused_ak[2] = 0.50980392f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.50196078f;
      materialSpecular_ak[1] = 0.50196078f;
      materialSpecular_ak[2] = 0.50196078f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.25 * 128.0f;
      glViewport(gWidth * (0.145f), gHeight * (0.20f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // green
      materialAmbient_ak[0] = 0.0f;
      materialAmbient_ak[1] = 0.0f;
      materialAmbient_ak[2] = 0.0f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.1f;
      materialDiffused_ak[1] = 0.35f;
      materialDiffused_ak[2] = 0.1f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.45f;
      materialSpecular_ak[1] = 0.55f;
      materialSpecular_ak[2] = 0.45f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.25 * 128.0f;
      glViewport(gWidth * (0.145f), gHeight * (0.05f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // red
      materialAmbient_ak[0] = 0.0f;
      materialAmbient_ak[1] = 0.0f;
      materialAmbient_ak[2] = 0.0f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.5f;
      materialDiffused_ak[1] = 0.0f;
      materialDiffused_ak[2] = 0.0f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.7f;
      materialSpecular_ak[1] = 0.6f;
      materialSpecular_ak[2] = 0.6f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.25 * 128.0f;
      glViewport(gWidth * (0.145f), gHeight * (-0.10f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // white
      materialAmbient_ak[0] = 0.0f;
      materialAmbient_ak[1] = 0.0f;
      materialAmbient_ak[2] = 0.0f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.55f;
      materialDiffused_ak[1] = 0.55f;
      materialDiffused_ak[2] = 0.55f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.70f;
      materialSpecular_ak[1] = 0.70f;
      materialSpecular_ak[2] = 0.70f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.25 * 128.0f;
      glViewport(gWidth * (0.145f), gHeight * (-0.25f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // yellow plastic
      materialAmbient_ak[0] = 0.0f;
      materialAmbient_ak[1] = 0.0f;
      materialAmbient_ak[2] = 0.0f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.5f;
      materialDiffused_ak[1] = 0.5f;
      materialDiffused_ak[2] = 0.0f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.60f;
      materialSpecular_ak[1] = 0.60f;
      materialSpecular_ak[2] = 0.50f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.25 * 128.0f;
      glViewport(gWidth * (0.145f), gHeight * (-0.40f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // black
      materialAmbient_ak[0] = 0.02f;
      materialAmbient_ak[1] = 0.02f;
      materialAmbient_ak[2] = 0.02f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.01f;
      materialDiffused_ak[1] = 0.01f;
      materialDiffused_ak[2] = 0.01f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.4f;
      materialSpecular_ak[1] = 0.4f;
      materialSpecular_ak[2] = 0.4f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.078125 * 128.0f;
      glViewport(gWidth * (0.40f), gHeight * (0.35f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // cyan
      materialAmbient_ak[0] = 0.0f;
      materialAmbient_ak[1] = 0.05f;
      materialAmbient_ak[2] = 0.05f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.4f;
      materialDiffused_ak[1] = 0.5f;
      materialDiffused_ak[2] = 0.5f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.04f;
      materialSpecular_ak[1] = 0.7f;
      materialSpecular_ak[2] = 0.7f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.078125 * 128.0f;
      glViewport(gWidth * (0.40f), gHeight * (0.20f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // green
      materialAmbient_ak[0] = 0.0f;
      materialAmbient_ak[1] = 0.05f;
      materialAmbient_ak[2] = 0.0f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.4f;
      materialDiffused_ak[1] = 0.5f;
      materialDiffused_ak[2] = 0.4f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.04f;
      materialSpecular_ak[1] = 0.7f;
      materialSpecular_ak[2] = 0.04f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.078125 * 128.0f;
      glViewport(gWidth * (0.40f), gHeight * (0.05f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // red
      materialAmbient_ak[0] = 0.05f;
      materialAmbient_ak[1] = 0.0f;
      materialAmbient_ak[2] = 0.0f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.5f;
      materialDiffused_ak[1] = 0.4f;
      materialDiffused_ak[2] = 0.4f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.7f;
      materialSpecular_ak[1] = 0.04f;
      materialSpecular_ak[2] = 0.04f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.078125 * 128.0f;
      glViewport(gWidth * (0.40f), gHeight * (-0.10f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // white
      materialAmbient_ak[0] = 0.05f;
      materialAmbient_ak[1] = 0.05f;
      materialAmbient_ak[2] = 0.05f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.5f;
      materialDiffused_ak[1] = 0.5f;
      materialDiffused_ak[2] = 0.5f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.7f;
      materialSpecular_ak[1] = 0.7f;
      materialSpecular_ak[2] = 0.7f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.078125 * 128.0f;
      glViewport(gWidth * (0.40f), gHeight * (-0.25f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      // yellow rubber
      materialAmbient_ak[0] = 0.05f;
      materialAmbient_ak[1] = 0.05f;
      materialAmbient_ak[2] = 0.0f;
      materialAmbient_ak[3] = 1.0f;
      materialDiffused_ak[0] = 0.5f;
      materialDiffused_ak[1] = 0.5f;
      materialDiffused_ak[2] = 0.4f;
      materialDiffused_ak[3] = 1.0f;
      materialSpecular_ak[0] = 0.7f;
      materialSpecular_ak[1] = 0.7f;
      materialSpecular_ak[2] = 0.04f;
      materialSpecular_ak[3] = 1.0f;
      materialShininess_ak = 0.078125 * 128.0f;
      glViewport(gWidth * (0.40f), gHeight * (-0.40f), gWidth, gHeight);
      if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(
          perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(
          perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(
          perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak,
                    materialShininess_ak);
      }
      viewMatrix_ak = mat4::identity();
      modelMatrix_ak = mat4::identity();
      translateMatrix_ak = mat4::identity();
      translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);
      modelMatrix_ak = translateMatrix_ak;
      glUniformMatrix4fv(
        perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
      glUniformMatrix4fv(
        perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
      glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak,
                         1,
                         GL_FALSE,
                         perspectiveProjectionMatrix_ak);
      glBindVertexArray(perVertexLighting_vao_sphere_ak);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                   perVertexLighting_vbo_element_sphere_ak);
      glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
      glBindVertexArray(0);
      glUseProgram(0);
      break;
    }
  }

  Update();

  glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
  [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

void
Update(void)
{
  angleForXRotation_ak = angleForXRotation_ak + 0.01f;
  if (angleForXRotation_ak > 2 * M_PI)
    angleForXRotation_ak = 0.0f;
  if (keyPressed_ak == 1) {
    lightPosition_ak[1] = 100 * sin(angleForXRotation_ak);
    lightPosition_ak[2] = 100 * cos(angleForXRotation_ak);
  }
  angleForYRotation_ak = angleForYRotation_ak + 0.01f;
  if (angleForYRotation_ak > 2 * M_PI)
    angleForYRotation_ak = 0.0f;
  if (keyPressed_ak == 2) {
    lightPosition_ak[0] = 100 * sin(angleForYRotation_ak);
    lightPosition_ak[2] = 100 * cos(angleForYRotation_ak);
  }
  angleForZRotation_ak = angleForZRotation_ak + 0.01f;
  if (angleForZRotation_ak > 2 * M_PI)
    angleForZRotation_ak = 0.0f;
  if (keyPressed_ak == 3) {
    lightPosition_ak[0] = 100 * sin(angleForZRotation_ak);
    lightPosition_ak[1] = 100 * cos(angleForZRotation_ak);
  }
}

- (void)startAnimation
{
  if (isAnimating == NO) {
    displayLink = [NSClassFromString(@"CADisplayLink")
      displayLinkWithTarget:self
                   selector:@selector(drawView:)];
    [displayLink setPreferredFramesPerSecond:animationFrameInterval];
    [displayLink addToRunLoop:[NSRunLoop currentRunLoop]
                      forMode:NSDefaultRunLoopMode];

    isAnimating = YES;
  }
}

- (void)stopAnimation
{
  if (isAnimating == YES) {
    [displayLink invalidate];
    displayLink = nil;
    isAnimating = NO;
  }
}

- (void)onSingleTap:(UITapGestureRecognizer*)gr
{
  if (bLight_ak) {
    bLight_ak = false;
  } else {
    bLight_ak = true;
  }
}

- (void)onDoubleTap:(UITapGestureRecognizer*)gr
{
  if (pvpf == 1) {
    pvpf = 0;
  } else {
    pvpf = 1;
  }
}

- (void)onSwipe:(UISwipeGestureRecognizer*)gr
{
  [self unintialize];
  [self release];
  exit(0);
}

- (void)onLongPress:(UILongPressGestureRecognizer*)gr
{
  if (keyPressed_ak < 4) {
    keyPressed_ak = keyPressed_ak + 1;
  } else {
    keyPressed_ak = 0;
  }
  angleForXRotation_ak = 0.0f;
  angleForYRotation_ak = 0.0f;
  angleForZRotation_ak = 0.0f;
  lightPosition_ak[0] = 0.0f;
  lightPosition_ak[1] = 0.0f;
  lightPosition_ak[2] = 0.0f;
}

- (void)unintialize
{
  if (perVertexLighting_vao_sphere_ak) {
    glDeleteVertexArrays(1, &perVertexLighting_vao_sphere_ak);
    perVertexLighting_vao_sphere_ak = 0;
  }
  if (perVertexLighting_vbo_position_sphere_ak) {
    glDeleteVertexArrays(1, &perVertexLighting_vbo_position_sphere_ak);
    perVertexLighting_vbo_position_sphere_ak = 0;
  }
  if (perVertexLighting_vbo_normal_sphere_ak) {
    glDeleteVertexArrays(1, &perVertexLighting_vbo_normal_sphere_ak);
    perVertexLighting_vbo_normal_sphere_ak = 0;
  }
  if (perVertexLighting_ShaderProgramObject_ak) {
    glUseProgram(perVertexLighting_ShaderProgramObject_ak);
    GLsizei shaderCount_ak;
    glGetProgramiv(perVertexLighting_ShaderProgramObject_ak,
                   GL_ATTACHED_SHADERS,
                   &shaderCount_ak);
    GLuint* pShaders_ak = NULL;
    pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
    glGetAttachedShaders(perVertexLighting_ShaderProgramObject_ak,
                         shaderCount_ak,
                         &shaderCount_ak,
                         pShaders_ak);
    for (int i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
      glDetachShader(perVertexLighting_ShaderProgramObject_ak,
                     pShaders_ak[i_ak]);
      glDeleteShader(pShaders_ak[i_ak]);
      pShaders_ak[i_ak] = 0;
    }
    free(pShaders_ak);
    glDeleteProgram(perVertexLighting_ShaderProgramObject_ak);
    perVertexLighting_ShaderProgramObject_ak = 0;
    glUseProgram(0);
  }
  if (perFragmentLighting_vao_sphere_ak) {
    glDeleteVertexArrays(1, &perFragmentLighting_vao_sphere_ak);
    perFragmentLighting_vao_sphere_ak = 0;
  }
  if (perFragmentLighting_vbo_position_sphere_ak) {
    glDeleteVertexArrays(1, &perFragmentLighting_vbo_position_sphere_ak);
    perFragmentLighting_vbo_position_sphere_ak = 0;
  }
  if (perFragmentLighting_vbo_normal_sphere_ak) {
    glDeleteVertexArrays(1, &perFragmentLighting_vbo_normal_sphere_ak);
    perFragmentLighting_vbo_normal_sphere_ak = 0;
  }
  if (perFragmentLighting_ShaderProgramObject_ak) {
    glUseProgram(perFragmentLighting_ShaderProgramObject_ak);
    GLsizei shaderCount_ak;
    glGetProgramiv(perFragmentLighting_ShaderProgramObject_ak,
                   GL_ATTACHED_SHADERS,
                   &shaderCount_ak);
    GLuint* pShaders_ak = NULL;
    pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
    glGetAttachedShaders(perFragmentLighting_ShaderProgramObject_ak,
                         shaderCount_ak,
                         &shaderCount_ak,
                         pShaders_ak);
    for (int i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
      glDetachShader(perFragmentLighting_ShaderProgramObject_ak,
                     pShaders_ak[i_ak]);
      glDeleteShader(pShaders_ak[i_ak]);
      pShaders_ak[i_ak] = 0;
    }
    free(pShaders_ak);
    glDeleteProgram(perFragmentLighting_ShaderProgramObject_ak);
    perFragmentLighting_ShaderProgramObject_ak = 0;
    glUseProgram(0);
  }

  if (depthRenderbuffer) {
    glDeleteRenderbuffers(1, &depthRenderbuffer);
    depthRenderbuffer = 0;
  }

  if (colorRenderbuffer) {
    glDeleteRenderbuffers(1, &colorRenderbuffer);
    colorRenderbuffer = 0;
  }

  if (defaultFramebuffer) {
    glDeleteFramebuffers(1, &defaultFramebuffer);
    defaultFramebuffer = 0;
  }

  if (eaglContext) {
    if ([EAGLContext currentContext] == eaglContext) {
      [EAGLContext setCurrentContext:nil];
    }
  }
}

- (void)dealloc
{
  [self unintialize];
  [super dealloc];
}

@end
