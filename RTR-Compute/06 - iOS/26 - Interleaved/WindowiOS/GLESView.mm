#import "GLESView.h"
#import "vmath.h"
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

using namespace vmath;
GLfloat cubeAngle_ak = 0.0f;
GLuint stone_texture_ak;

bool bLight_ak = false;

GLfloat lightAmbient_ak[] = {0.1f, 0.1f, 0.1f};
GLfloat lightDiffused_ak[] = {1.0f, 1.0f, 1.0f};
GLfloat lightPosition_ak[] = {100.0f, 100.0f, 100.0f, 1.0f};
GLfloat lightSpecular_ak[] = {1.0f, 1.0f, 1.0f};

GLfloat materialAmbient_ak[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat materialDiffused_ak[] = {0.5f, 0.2f, 0.7f, 1.0f};
GLfloat materialSpecular_ak[] = {0.7f, 0.7f, 0.7f, 1.0f};
GLfloat materialShininess_ak = 50.0f;
@implementation GLESView {
@private
  EAGLContext* eaglContext;

  GLuint defaultFramebuffer;
  GLuint colorRenderbuffer;
  GLuint depthRenderbuffer;

  id displayLink;
  NSInteger animationFrameInterval;
  BOOL isAnimating;

  enum
  {
    ATTRIBUTE_POSITION = 0,
    ATTRIBUTE_COLOR,
    ATTRIBUTE_NORMAL,
    ATTRIBUTE_TEXCOORD
  };
  GLuint gVertexShaderObject_ak;
  GLuint gFragmentShaderObject_ak;
  GLuint gShaderProgramObject_ak;
  GLuint vao_cube_ak;
  GLuint vbo_ak;
  GLuint textureSamplerUniform_ak;

  GLuint modelMatrixUniform_ak;
GLuint viewMatrixUniform_ak;
GLuint projectionMatrixUniform_ak;

GLuint laUniform_ak;
GLuint ldUniform_ak;
GLuint lsUniform_ak;
GLuint lightPositionUniform_ak;

GLuint kaUniform_ak;
GLuint kdUniform_ak;
GLuint ksUniform_ak;
GLuint materialShininessUniform_ak;
GLuint lKeyPressedUniform_ak;
  vmath::mat4 perspectiveProjectionMatrix_ak;
}
- (id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    CAEAGLLayer* eaglLayer = (CAEAGLLayer*)[super layer];

    [eaglLayer setOpaque:YES];

    [eaglLayer setDrawableProperties:[NSDictionary
                                       dictionaryWithObjectsAndKeys:
                                         [NSNumber numberWithBool:NO],
                                         kEAGLDrawablePropertyRetainedBacking,
                                         kEAGLColorFormatRGBA8,
                                         kEAGLDrawablePropertyColorFormat,
                                         nil]];

    eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
    if (eaglContext == nil) {
      printf("OpenGL-ES Context Creation Failed.\n");
      return (nil);
    }

    [EAGLContext setCurrentContext:eaglContext];

    glGenFramebuffers(1, &defaultFramebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    glGenRenderbuffers(1, &colorRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
    glFramebufferRenderbuffer(
      GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);

    GLint backingWidth;
    GLint backingHeight;
    glGetRenderbufferParameteriv(
      GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
    glGetRenderbufferParameteriv(
      GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);

    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(
      GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
    glFramebufferRenderbuffer(
      GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    printf("initWithFrame checking if framebuffer is complete\n");
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
      printf("Framebuffer is not complete\n");
      [self unintialize];
      return (nil);
    }

    printf("Renderer : %s\n", glGetString(GL_RENDERER));
    printf("GL Version : %s\n", glGetString(GL_VERSION));
    printf("GLES Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    animationFrameInterval = 60;
    isAnimating = NO;

  gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode_ak =
        "#version 300 es"
        "\n"
        "precision highp float;"
        "precision highp int;"
        "in vec4 vPosition;"
        "in vec4 vColor;"
        "in vec3 vNormal;"
        "in vec2 vTexCoord;"
        "uniform mat4 u_model_matrix;"
        "uniform mat4 u_view_matrix;"
        "uniform mat4 u_projection_matrix;"
        "uniform vec4 u_light_position;"
        "uniform int u_lkey_pressed;"
        "out vec3 transformed_normal;"
        "out vec3 light_direction;"
        "out vec3 view_vector;"
        "out vec2 out_texcoord;"
        "out vec4 out_color;"
        "void main(void)"
        "{"
        "if(u_lkey_pressed == 1)"
        "{"
        "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"
        "transformed_normal = mat3((u_view_matrix * u_model_matrix)) * vNormal;"
        "light_direction = vec3(u_light_position - eye_coordinates);"
        "view_vector = -eye_coordinates.xyz;"
        "}"
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"
        "out_texcoord = vTexCoord;"
        "out_color = vColor;"
        "}";

    glShaderSource(gVertexShaderObject_ak, 1, (const GLchar**)&vertexShaderSourceCode_ak, NULL);
    glCompileShader(gVertexShaderObject_ak);

    GLint infoLogLength_ak = 0;
    GLint shaderCompileStatus_ak = 0;
    char* szBuffer_ak = NULL;
    glGetShaderiv(gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
        glGetShaderiv(gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetShaderInfoLog(gVertexShaderObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                printf( "Vertex shader compilation log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                [self unintialize];
            }
        }
    }

    gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentSourceCode_ak =
        "#version 300 es"
        "\n"
        "precision highp float;"
        "precision highp int;"
        "in vec4 out_color;"
        "in vec2 out_texcoord;"
        "in vec3 transformed_normal;"
        "in vec3 light_direction;"
        "in vec3 view_vector;"
        "uniform vec3 u_la;"
        "uniform vec3 u_ld;"
        "uniform vec3 u_ls;"
        "uniform vec3 u_ka;"
        "uniform vec3 u_kd;"
        "uniform vec3 u_ks;"
        "uniform float u_material_shininess;"
        "uniform int u_lkey_pressed;"
        "uniform sampler2D u_texture_sampler;"
        "out vec4 FragColor;"
        "void main(void)"
        "{"
        "vec3 phong_ads_light;"
        "if(u_lkey_pressed == 1)"
        "{"
        "vec3 normalized_transformed_normal = normalize(transformed_normal);"
        "vec3 normalized_light_direction = normalize(light_direction);"
        "vec3 normalized_view_vector = normalize(view_vector);"
        "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normal);"
        "vec3 ambient = u_la * u_ka;"
        "vec3 diffuse = u_ld * u_kd * max(dot(normalized_light_direction, normalized_transformed_normal), 0.0);"
        "vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, normalized_view_vector), 0.0), u_material_shininess);"
        "phong_ads_light = ambient + diffuse + specular;"
        "}"
        "else"
        "{"
        "phong_ads_light = vec3(1.0, 1.0, 1.0);"
        "}"
        "vec3 tex = vec3(texture(u_texture_sampler, out_texcoord));"
        "FragColor = vec4(vec3(out_color) * phong_ads_light * tex, 1.0f);"
        "}";

    glShaderSource(gFragmentShaderObject_ak, 1, (const GLchar**)&fragmentSourceCode_ak, NULL);
    glCompileShader(gFragmentShaderObject_ak);

    infoLogLength_ak = 0;
    shaderCompileStatus_ak = 0;
    szBuffer_ak = NULL;
    glGetShaderiv(gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
        glGetShaderiv(gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetShaderInfoLog(gFragmentShaderObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                printf( "Fragment shader compilation log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                [self unintialize];
            }
        }
    }

    gShaderProgramObject_ak = glCreateProgram();
    glAttachShader(gShaderProgramObject_ak, gVertexShaderObject_ak);
    glAttachShader(gShaderProgramObject_ak, gFragmentShaderObject_ak);

    glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_NORMAL, "vNormal");
    glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_COLOR, "vColor");
    glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_TEXCOORD, "vTexCoord");

    glLinkProgram(gShaderProgramObject_ak);

    GLint shaderProgramLinkStatus = 0;
    glGetProgramiv(gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
    if (shaderProgramLinkStatus == GL_FALSE) {
        glGetProgramiv(gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetProgramInfoLog(gShaderProgramObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                printf( "Shader program link log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                [self unintialize];
            }
        }
    }

    modelMatrixUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_model_matrix");
    viewMatrixUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_view_matrix");
    projectionMatrixUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_projection_matrix");
    laUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_la");
    ldUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_ld");
    lsUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_ls");
    lightPositionUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_light_position");
    kaUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_ka");
    kdUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_kd");
    ksUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_ks");
    materialShininessUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_material_shininess");
    lKeyPressedUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_lkey_pressed");
    textureSamplerUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_texture_sampler");

    const GLfloat cubePCNT_ak[] = {
        1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
        -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,

        1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
        1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,

        -1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
        1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f,
        1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f,

        -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
        -1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,

        1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
        -1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
        -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,

        1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f,
        -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f};

    glGenVertexArrays(1, &vao_cube_ak);

    glBindVertexArray(vao_cube_ak);
    glGenBuffers(1, &vbo_ak);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_ak);
    glBufferData(GL_ARRAY_BUFFER, 24 * 11 * sizeof(float), cubePCNT_ak, GL_STATIC_DRAW);

    glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(0 * sizeof(float)));
    glEnableVertexAttribArray(ATTRIBUTE_POSITION);

    glVertexAttribPointer(ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(ATTRIBUTE_COLOR);

    glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(ATTRIBUTE_NORMAL);

    glVertexAttribPointer(ATTRIBUTE_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(9 * sizeof(float)));
    glEnableVertexAttribArray(ATTRIBUTE_TEXCOORD);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    perspectiveProjectionMatrix_ak = vmath::mat4::identity();
    stone_texture_ak = [self loadTextureFromBMPFile:@"marble":@"bmp"];
      glEnable(GL_TEXTURE_2D);
    printf("Init done\n");

    UITapGestureRecognizer* singleTapGestureRecognizer =
      [[UITapGestureRecognizer alloc] initWithTarget:self
                                              action:@selector(onSingleTap:)];

    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
    [singleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:singleTapGestureRecognizer];

    UITapGestureRecognizer* doubleTapGestureRecognizer =
      [[UITapGestureRecognizer alloc] initWithTarget:self
                                              action:@selector(onDoubleTap:)];

    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
    [doubleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:doubleTapGestureRecognizer];

    [singleTapGestureRecognizer
      requireGestureRecognizerToFail:doubleTapGestureRecognizer];

    UISwipeGestureRecognizer* swipeGestureRecognizer =
      [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(onSwipe:)];
    [swipeGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:swipeGestureRecognizer];

    UILongPressGestureRecognizer* longPressGestureRecognizer =
      [[UILongPressGestureRecognizer alloc]
        initWithTarget:self
                action:@selector(onLongPress:)];
    [longPressGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:longPressGestureRecognizer];
  }
  return (self);
}
+ (Class)layerClass
{
  return ([CAEAGLLayer class]);
}
/*
 - (void)drawRect:(CGRect)rect {

 [self loadTextureFromBMPFile:@"Smiley":@"bmp"];

 }
 */
- (GLuint)loadTextureFromBMPFile:(NSString*)imageFileName:(NSString*)extension
{

  NSString* imageFileNameWithPath_ak =
    [[NSBundle mainBundle] pathForResource:imageFileName ofType:extension];
  UIImage* bmpImage =
    [[UIImage alloc] initWithContentsOfFile:imageFileNameWithPath_ak];

  CGImageRef cgImage = [bmpImage CGImage];

  int w = (int)CGImageGetWidth(cgImage);
  int h = (int)CGImageGetHeight(cgImage);

  CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));

  void* pixels = (void*)CFDataGetBytePtr(imageData);
  GLuint bitmapTexture;
  glGenTextures(1, &bitmapTexture);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glBindTexture(GL_TEXTURE_2D, bitmapTexture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(
    GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexImage2D(
    GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

  glGenerateMipmap(GL_TEXTURE_2D);
  CFRelease(imageData);
  return (bitmapTexture);
}
- (void)layoutSubviews
{
  glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
  [eaglContext renderbufferStorage:GL_RENDERBUFFER
                      fromDrawable:(CAEAGLLayer*)[self layer]];
  glFramebufferRenderbuffer(
    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);

  GLint width;
  GLint height;
  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
  glGetRenderbufferParameteriv(
    GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);

  glGenRenderbuffers(1, &depthRenderbuffer);
  glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
  glFramebufferRenderbuffer(
    GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);

  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
    printf("Framebuffer is not complete in layoutSubviews");
  }

  if (height < 0) {
    height = 1;
  }

  glViewport(0, 0, (GLsizei)width, (GLsizei)height);
  perspectiveProjectionMatrix_ak =
    vmath::perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
  [self drawView:nil];
}
- (void)drawView:(id)sender
{
  [EAGLContext setCurrentContext:eaglContext];
  glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glUseProgram(gShaderProgramObject_ak);
    if (bLight_ak == true) {
        glUniform1i(lKeyPressedUniform_ak, 1);
        glUniform3fv(laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(materialShininessUniform_ak, materialShininess_ak);
    } else {
        glUniform1i(lKeyPressedUniform_ak, 0);
    }

    mat4 modelMatrix_ak;
    mat4 viewMatrix_ak;
    mat4 modelViewProjectionMatrix_ak;
    mat4 translateMatrix_ak;
    mat4 xRotationMatrix_ak;
    mat4 yRotationMatrix_ak;
    mat4 zRotationMatrix_ak;
    mat4 scaleMatrix_ak;

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    modelViewProjectionMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();
    xRotationMatrix_ak = mat4::identity();
    yRotationMatrix_ak = mat4::identity();
    zRotationMatrix_ak = mat4::identity();
    scaleMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -5.0f);
    xRotationMatrix_ak = vmath::rotate(cubeAngle_ak, 1.0f, 0.0f, 0.0f);
    yRotationMatrix_ak = vmath::rotate(cubeAngle_ak, 0.0f, 1.0f, 0.0f);
    zRotationMatrix_ak = vmath::rotate(cubeAngle_ak, 0.0f, 0.0f, 1.0f);
    scaleMatrix_ak = vmath::scale(0.75f, 0.75f, 0.75f);

    modelMatrix_ak = translateMatrix_ak * scaleMatrix_ak * xRotationMatrix_ak * yRotationMatrix_ak * zRotationMatrix_ak;

    glUniformMatrix4fv(modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, stone_texture_ak);
    glUniform1i(textureSamplerUniform_ak, 0);

    glBindVertexArray(vao_cube_ak);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    glBindVertexArray(0);

    glUseProgram(0);

  Update();

  glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
  [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}
void
Update(void)
{
  cubeAngle_ak = cubeAngle_ak + 0.1f;
  if (cubeAngle_ak > 360.0f) {
    cubeAngle_ak = 0.0f;
  }

}
- (void)startAnimation
{
  if (isAnimating == NO) {
    displayLink = [NSClassFromString(@"CADisplayLink")
      displayLinkWithTarget:self
                   selector:@selector(drawView:)];
    [displayLink setPreferredFramesPerSecond:animationFrameInterval];
    [displayLink addToRunLoop:[NSRunLoop currentRunLoop]
                      forMode:NSDefaultRunLoopMode];

    isAnimating = YES;
  }
}
- (void)stopAnimation
{
  if (isAnimating == YES) {
    [displayLink invalidate];
    displayLink = nil;
    isAnimating = NO;
  }
}
- (void)onSingleTap:(UITapGestureRecognizer*)gr
{
  if (bLight_ak) {
    bLight_ak = false;
  } else {
    bLight_ak = true;
  }
}
- (void)onDoubleTap:(UITapGestureRecognizer*)gr
{}
- (void)onSwipe:(UISwipeGestureRecognizer*)gr
{
  [self unintialize];
  [self release];
  exit(0);
}
- (void)onLongPress:(UILongPressGestureRecognizer*)gr
{}
- (void)unintialize
{

  if (vao_cube_ak) {
    glDeleteVertexArrays(1, &vao_cube_ak);
    vao_cube_ak = 0;
  }
  if (vbo_ak) {
    glDeleteVertexArrays(1, &vbo_ak);
      vbo_ak = 0;
  }

  if (gShaderProgramObject_ak) {
    glUseProgram(gShaderProgramObject_ak);
    GLsizei shaderCount_ak;
    glGetProgramiv(
      gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

    GLuint* pShaders_ak = NULL;
    pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
    glGetAttachedShaders(gShaderProgramObject_ak,
                         shaderCount_ak,
                         &shaderCount_ak,
                         pShaders_ak);

    for (int i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
      glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
      glDeleteShader(pShaders_ak[i_ak]);
      pShaders_ak[i_ak] = 0;
    }

    free(pShaders_ak);
    glDeleteProgram(gShaderProgramObject_ak);
    gShaderProgramObject_ak = 0;

    glUseProgram(0);
  }

  if (depthRenderbuffer) {
    glDeleteRenderbuffers(1, &depthRenderbuffer);
    depthRenderbuffer = 0;
  }

  if (colorRenderbuffer) {
    glDeleteRenderbuffers(1, &colorRenderbuffer);
    colorRenderbuffer = 0;
  }

  if (defaultFramebuffer) {
    glDeleteFramebuffers(1, &defaultFramebuffer);
    defaultFramebuffer = 0;
  }

  if (eaglContext) {
    if ([EAGLContext currentContext] == eaglContext) {
      [EAGLContext setCurrentContext:nil];
    }
  }
}
- (void)dealloc
{
  [self unintialize];
  [super dealloc];
}
@end
