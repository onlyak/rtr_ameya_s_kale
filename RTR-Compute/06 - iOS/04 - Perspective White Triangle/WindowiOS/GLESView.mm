//
//  MyView.m
//  WindowiOS
//
//  Created by ameya kale on 03/07/21.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "GLESView.h"

#import "vmath.h"

using namespace vmath;

enum
{
    ATTRIBUTE_POSITION = 0,
    ATTRIBUTE_COLOR,
    ATTRIBUTE_NORMAL,
    ATRIBUTE_TEXCOORD,
};

@implementation GLESView {
    @private
    EAGLContext *eaglContext;
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint Vao;
    GLuint Vbo_position;
    GLuint mvpMatrixUniform;
    
    GLuint gVertexShaderObject_ak;
    GLuint gFragmentShaderObject_ak;
    GLuint gShaderProgramObject_ak;
    
    vmath::mat4 perspectiveProjectMatrix;

}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame: frame];
        if(self)
        {
            CAEAGLLayer *eaglLayer = (CAEAGLLayer *)[super layer];
            [eaglLayer setOpaque:YES];
            [eaglLayer setDrawableProperties:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], kEAGLDrawablePropertyRetainedBacking,
                                              kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil]];
            
            //context Creation
            
            eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
            if(eaglContext == nil)
            {
                printf("OpenGL context not available");
            }
            
            [EAGLContext setCurrentContext:eaglContext];
            
            glGenFramebuffers(1, &defaultFramebuffer);
            glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
            glGenRenderbuffers(1, &colorRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
            
            // storage for render buffer
            [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
            
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
            
            GLint backingWidth;
            GLint backingHeight;
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
            
            glGenRenderbuffers(1, &depthRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER_WIDTH, depthRenderbuffer);
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
            
            if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            {
                printf("FrameBuffer is not complete.\n");
                [self unitialize];
                return nil;
            }
            
            animationFrameInterval = 60; // from iOS 8.2
            
            isAnimating = NO;
            
            gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
            const GLchar *vertexShaderSourceCode =
            "#version 300 es" \
            "\n"\
            "in vec4 vPosition;" \
            "uniform mat4 u_mvp_matrix;" \
            "void main()" \
            "{" \
                  "gl_Position = u_mvp_matrix * vPosition;" \
            "}";
            
            glShaderSource(gVertexShaderObject_ak,1,(const GLchar **)&vertexShaderSourceCode, NULL);

            // compile Vertex Shader
            glCompileShader(gVertexShaderObject_ak);

            // Error checking for Vertex Shader
            GLint infoLogLength = 0;
            GLint shaderCompiledStatus = 0;
            char *szBuffer = NULL;

            glGetShaderiv(gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
            if(shaderCompiledStatus == GL_FALSE)
            {
                glGetShaderiv(gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
                if(infoLogLength>0)
                {
                    szBuffer = (char *)malloc(infoLogLength);
                    if(szBuffer!=NULL)
                    {
                        GLsizei written;
                        glGetShaderInfoLog(gVertexShaderObject_ak, infoLogLength, &written, szBuffer);
                        printf("Vertex Shader Compilation Log: %s\n",szBuffer);
                        free(szBuffer);
                    }
                }
            }

            //Fragment Shader
            gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);
            const GLchar *vertexFragmentSourceCode =
            "#version 300 es" \
            "\n" \
            "precision highp float;" \
            "out vec4 FragColor;" \
            "void main()"\
            "{" \
                "FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
            "}";

            glShaderSource(gFragmentShaderObject_ak,1,(const GLchar **)&vertexFragmentSourceCode, NULL);

            // compile Fragment Shader
            glCompileShader(gFragmentShaderObject_ak);

            // Error Checking for Fragment Shader
            glGetShaderiv(gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
            if(shaderCompiledStatus == GL_FALSE)
            {
                glGetShaderiv(gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
                if(infoLogLength>0)
                {
                    szBuffer = (char *)malloc(infoLogLength);
                    if(szBuffer!=NULL)
                    {
                        GLsizei written;
                        glGetShaderInfoLog(gFragmentShaderObject_ak, infoLogLength, &written, szBuffer);
                        printf("Fragment Shader Compilation Log: %s\n",szBuffer);
                        free(szBuffer);
                    }
                }
            }

            //Shader Program
            gShaderProgramObject_ak = glCreateProgram();
            glAttachShader(gShaderProgramObject_ak,gVertexShaderObject_ak);
            glAttachShader(gShaderProgramObject_ak,gFragmentShaderObject_ak);

            // Bind the attributes in shader with the enums in your main program
            /* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
            glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");

            glLinkProgram(gShaderProgramObject_ak);

            // Linking Error Checking
            GLint shaderProgramLinkStatus = 0;
            szBuffer = NULL;

            glGetProgramiv(gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
            if(shaderProgramLinkStatus == GL_FALSE)
            {
                glGetProgramiv(gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
                if(infoLogLength>0)
                {
                    szBuffer = (char *)malloc(infoLogLength);
                    if(szBuffer!=NULL)
                    {
                        GLsizei written;
                        glGetProgramInfoLog(gShaderProgramObject_ak, infoLogLength, &written, szBuffer);
                        printf("Shader Program Link Log: %s\n",szBuffer);
                        free(szBuffer);
                    }
                }
            }

            //Get the information of uniform Post linking
            mvpMatrixUniform = glGetUniformLocation(gShaderProgramObject_ak, "u_mvp_matrix");

            //Vertices Array Declaration
            const GLfloat triangleVertices[]=
            {   0.0f, 1.0f, 0.0f,
                -1.0f, -1.0f, 0.0f,
                1.0f, -1.0f, 0.0f
            };

            //Repeat the below steps of Vbo_position and call them in draw method
            glGenVertexArrays(1, &Vao);
            glBindVertexArray(Vao);

            // Push the above vertices to vPosition

            //Steps
            /* 1. Tell OpenGl to create one buffer in your VRAM
                  Give me a symbol to identify. It is known as NamedBuffer
                  In OpenGL terminology, it is called as GL_ARRAY_BUFFER. This is becase vertex has plenty of attributes
                  like color, texture, etc. Also it requires contiguous memory.
                  User identifies this variable as Vbo_position and GPU as GL_ARRAY_BUFFER.
               2. Bind with the above symbol. It doesn't unbind until 'unbind step' is performed eg- Railway track
               3. Insert triangle data into the buffer.
               4. Specify where to insert this data into shader and also how to use it.
               5. Enable the 'in' point.
               6. Unbind
            */
            glGenBuffers(1, &Vbo_position);
            glBindBuffer(GL_ARRAY_BUFFER, Vbo_position);
            glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
            // 3 is specified for 3 pairs for triangle vertices
            /* For Texture, specify 2*/
            // 4th parameter----> Normalized Co-ordinates
            // 5th How many strides to take?
            // 6th From which position
            glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
            glEnableVertexAttribArray(ATTRIBUTE_POSITION);
            glBindBuffer(GL_ARRAY_BUFFER, 0);  //change tracks to link different attributes
            glBindVertexArray(0);

            glClearDepthf(1.0f);
            glEnable(GL_DEPTH_TEST);
            glDepthFunc(GL_LEQUAL);
            
            glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
            
            perspectiveProjectMatrix = vmath::mat4::identity();

            
            // Gestures
            UITapGestureRecognizer *singleTapGestureRecogniser = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
            [singleTapGestureRecogniser setNumberOfTapsRequired:1];
            [singleTapGestureRecogniser setNumberOfTouchesRequired:1];
            [singleTapGestureRecogniser setDelegate:self];
            [self addGestureRecognizer:singleTapGestureRecogniser];
            
            UITapGestureRecognizer *doubleTapGestureRecogniser = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
            [doubleTapGestureRecogniser setNumberOfTapsRequired:2];
            [doubleTapGestureRecogniser setNumberOfTouchesRequired:1];
            [doubleTapGestureRecogniser setDelegate:self];
            [self addGestureRecognizer:doubleTapGestureRecogniser];
            
            [singleTapGestureRecogniser requireGestureRecognizerToFail: doubleTapGestureRecogniser];
            
            UISwipeGestureRecognizer *swipeGestureRecogniser = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
            [swipeGestureRecogniser setDelegate:self];
            [self addGestureRecognizer:swipeGestureRecogniser];
            
            UILongPressGestureRecognizer *longPressRecogniser = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
            [longPressRecogniser setDelegate:self];
            [self addGestureRecognizer:longPressRecogniser];
        }
        return(self);
}
//- (void)drawRect:(CGRect)rect {
//    // Drawing code
//
//}

-(void)layoutSubviews
{
    
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)[self layer]];

    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER_WIDTH, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("FrameBuffer is not complete.\n");
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    perspectiveProjectMatrix = perspective(45.0f, ((GLfloat)width/(GLfloat)height),0.1f, 100.0f);
    
    [self drawView];
}

+(Class)layerClass
{
    return ([CAEAGLLayer class]);
}

-(void)drawView
{
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(gShaderProgramObject_ak);
        
    //OpenGL Draw

    //Set ModelView and ModelViewProjection matrices to identity
    mat4 modelViewMatrix = mat4::identity();
    mat4 modelViewProjectionMatrix = mat4::identity();

    modelViewProjectionMatrix = perspectiveProjectMatrix*modelViewMatrix;

    // Translate call
    mat4 translateMatrix = translate(0.0f,0.0f,-3.0f);
    modelViewMatrix = translateMatrix;
    modelViewProjectionMatrix = perspectiveProjectMatrix*modelViewMatrix;

    // After this line, modelViewProjectionMatrix becomes u_mvpMatrix
    glUniformMatrix4fv(mvpMatrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);

    //Bind Vao
    glBindVertexArray(Vao); //change tracks  //Begin

    glDrawArrays(GL_TRIANGLES, 0, 3);

    // Unbind Vao
    glBindVertexArray(0);  //end

    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void)startAnimation
{
    if(isAnimating == NO)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget: self selector: @selector(drawView)];
        
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
        
    }
}

-(void)stopAnimation
{
    if(isAnimating == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    [self setNeedsDisplay];
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    [self setNeedsDisplay];
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    [self unitialize];
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    [self setNeedsDisplay];
}

-(void) unitialize
{
        if(Vao)
        {
            glDeleteVertexArrays(1, &Vao);
            Vao = 0;
        }

        if(Vbo_position)
        {
            glDeleteVertexArrays(1, &Vbo_position);
            Vbo_position = 0;
        }

        // glDetachShader(gShaderProgramObject_ak, gVertexShaderObject_ak);
        // glDetachShader(gShaderProgramObject_ak, gFragmentShaderObject_ak);

        // glDeleteShader(gVertexShaderObject_ak);
        // gVertexShaderObject_ak = 0;

        // glDeleteShader(gFragmentShaderObject_ak);
        // gFragmentShaderObject_ak = 0;

        // glUseProgram(0);

        if (gShaderProgramObject_ak) {
            glUseProgram(gShaderProgramObject_ak);
            GLsizei shaderCount_ak;
            glGetProgramiv(gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

            GLuint* pShaders_ak = NULL;
            pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
            glGetAttachedShaders(gShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);

            for (GLsizei i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
                glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
                glDeleteShader(pShaders_ak[i_ak]);
                pShaders_ak[i_ak] = 0;
            }

            free(pShaders_ak);
            glDeleteProgram(gShaderProgramObject_ak);
            gShaderProgramObject_ak = 0;

            glUseProgram(0);
        }

    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
    }
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext:nil];
        }
    }
    
    [eaglContext release];
    eaglContext = nil;
}

-(void) dealloc
{
    [self unitialize];
    [super dealloc];
}
@end
