//
//  MyView.m
//  WindowiOS
//
//  Created by ameya kale on 03/07/21.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "GLESView.h"

#import "vmath.h"

using namespace vmath;

#define checkImageWidth_ak 64
#define checkImageHeight_ak 64

enum
{
    ATTRIBUTE_POSITION = 0,
    ATTRIBUTE_COLOR,
    ATTRIBUTE_NORMAL,
    ATTRIBUTE_TEXCOORD,
};

@implementation GLESView {
    @private
    EAGLContext *eaglContext;
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint gVertexShaderObject_ak;
    GLuint gFragmentShaderObject_ak;
    GLuint gShaderProgramObject_ak;

    GLuint mvpMatrixUniform_ak;

    GLuint Vao_cube_ak;
    GLuint Vbo_position_cube_ak;
    GLuint Vbo_color_cube_ak;

    GLuint Vbo_texture_cube_ak;

    GLuint textureSamplerUniform_ak;
    GLuint textureImage_ak;

    GLubyte checkImage_ak[checkImageWidth_ak][checkImageHeight_ak][4];

    mat4 perspectiveProjectMatrix_ak;

}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame: frame];
        if(self)
        {
            CAEAGLLayer *eaglLayer = (CAEAGLLayer *)[super layer];
            [eaglLayer setOpaque:YES];
            [eaglLayer setDrawableProperties:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], kEAGLDrawablePropertyRetainedBacking,
                                              kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil]];
            
            //context Creation
            
            eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
            if(eaglContext == nil)
            {
                printf("OpenGL context not available");
            }
            
            [EAGLContext setCurrentContext:eaglContext];
            
            glGenFramebuffers(1, &defaultFramebuffer);
            glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
            glGenRenderbuffers(1, &colorRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
            
            // storage for render buffer
            [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
            
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
            
            GLint backingWidth;
            GLint backingHeight;
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
            
            glGenRenderbuffers(1, &depthRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER_WIDTH, depthRenderbuffer);
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
            
            if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            {
                printf("FrameBuffer is not complete.\n");
                [self unitialize];
                return nil;
            }
            
            animationFrameInterval = 60; // from iOS 8.2
            
            isAnimating = NO;
            
            gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
            const GLchar *vertexShaderSourceCode =
            "#version 300 es" \
            "\n" \
            "in vec4 vPosition;" \
            "in vec4 vColor;" \
            "out vec4 out_color;" \
            "in vec2 vTexCoord;" \
            "out vec2 out_texCoord;" \
            "uniform mat4 u_mvpMatrix;" \
            "void main(void)" \
            "{" \
            "gl_Position = u_mvpMatrix * vPosition;" \
            "out_color = vColor;" \
            "out_texCoord = vTexCoord;" \
            "}";
            
            glShaderSource(gVertexShaderObject_ak,1,(const GLchar **)&vertexShaderSourceCode, NULL);

            // compile Vertex Shader
            glCompileShader(gVertexShaderObject_ak);

            // Error checking for Vertex Shader
            GLint infoLogLength = 0;
            GLint shaderCompiledStatus = 0;
            char *szBuffer = NULL;

            glGetShaderiv(gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
            if(shaderCompiledStatus == GL_FALSE)
            {
                glGetShaderiv(gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
                if(infoLogLength>0)
                {
                    szBuffer = (char *)malloc(infoLogLength);
                    if(szBuffer!=NULL)
                    {
                        GLsizei written;
                        glGetShaderInfoLog(gVertexShaderObject_ak, infoLogLength, &written, szBuffer);
                        free(szBuffer);
                        szBuffer = NULL;
                    }
                }
            }

            //Fragment Shader
            /* out_color is the output of Vertex Shader */
            gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);
            const GLchar *vertexFragmentSourceCode =
            "#version 300 es" \
            "\n" \
            "precision highp float;" \
            "in vec4 out_color;" \
            "out vec4 FragColor;" \
            "in vec2 out_texCoord;" \
            "uniform sampler2D u_texture_sampler;" \
            "void main(void)" \
            "{" \
            "FragColor = texture(u_texture_sampler, out_texCoord);" \
            "}";

            glShaderSource(gFragmentShaderObject_ak,1,(const GLchar **)&vertexFragmentSourceCode, NULL);

            // compile Fragment Shader
            glCompileShader(gFragmentShaderObject_ak);

            // Error Checking for Fragment Shader
            glGetShaderiv(gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
            if(shaderCompiledStatus == GL_FALSE)
            {
                glGetShaderiv(gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
                if(infoLogLength>0)
                {
                    szBuffer = (char *)malloc(infoLogLength);
                    if(szBuffer!=NULL)
                    {
                        GLsizei written;
                        glGetShaderInfoLog(gFragmentShaderObject_ak, infoLogLength, &written, szBuffer);
                        free(szBuffer);
                        szBuffer = NULL;
                    }
                }
            }

            //Shader Program
            gShaderProgramObject_ak = glCreateProgram();
            glAttachShader(gShaderProgramObject_ak,gVertexShaderObject_ak);
            glAttachShader(gShaderProgramObject_ak,gFragmentShaderObject_ak);

            // Bind the attributes in shader with the enums in your main program
            /* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
            glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");

            // For Texture Attribute
            glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_TEXCOORD, "vTexCoord");

            glLinkProgram(gShaderProgramObject_ak);

            // Linking Error Checking
            GLint shaderProgramLinkStatus = 0;
            szBuffer = NULL;

            glGetProgramiv(gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
            if(shaderProgramLinkStatus == GL_FALSE)
            {
                glGetProgramiv(gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
                if(infoLogLength>0)
                {
                    szBuffer = (char *)malloc(infoLogLength);
                    if(szBuffer!=NULL)
                    {
                        GLsizei written;
                        glGetProgramInfoLog(gShaderProgramObject_ak, infoLogLength, &written, szBuffer);
                        free(szBuffer);
                        szBuffer = NULL;
                    }
                }
            }

            //Get the information of uniform Post linking
            mvpMatrixUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_mvpMatrix");
            textureSamplerUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_texture_sampler");

            [self loadTexture];

            glGenVertexArrays(1, &Vao_cube_ak);
            glBindVertexArray(Vao_cube_ak);

            //Position
            glGenBuffers(1, &Vbo_position_cube_ak);
            glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_cube_ak);
            // 3 is specified for 3 pairs for vertices
            /* For Texture, specify 2*/
            // 4th parameter----> Normalized Co-ordinates
            // 5th How many strides to take?
            // 6th From which position
            glBufferData(GL_ARRAY_BUFFER, 4*3*sizeof(float), NULL, GL_DYNAMIC_DRAW);
            glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
            glEnableVertexAttribArray(ATTRIBUTE_POSITION);
            glBindBuffer(GL_ARRAY_BUFFER, 0);

            //Texture
            glGenBuffers(1, &Vbo_texture_cube_ak);
            glBindBuffer(GL_ARRAY_BUFFER, Vbo_texture_cube_ak);
            glBufferData(GL_ARRAY_BUFFER, 8*sizeof(float), NULL, GL_DYNAMIC_DRAW);
            glVertexAttribPointer(ATTRIBUTE_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 0, NULL);
            glEnableVertexAttribArray(ATTRIBUTE_TEXCOORD);
            glBindBuffer(GL_ARRAY_BUFFER, 0);

            glBindVertexArray(0);

            glClearDepthf(1.0f);
            glEnable(GL_DEPTH_TEST);
            glDepthFunc(GL_LEQUAL);

            glEnable(GL_TEXTURE_2D);
            glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
            perspectiveProjectMatrix_ak = vmath::mat4::identity();

            
            // Gestures
            UITapGestureRecognizer *singleTapGestureRecogniser = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
            [singleTapGestureRecogniser setNumberOfTapsRequired:1];
            [singleTapGestureRecogniser setNumberOfTouchesRequired:1];
            [singleTapGestureRecogniser setDelegate:self];
            [self addGestureRecognizer:singleTapGestureRecogniser];
            
            UITapGestureRecognizer *doubleTapGestureRecogniser = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
            [doubleTapGestureRecogniser setNumberOfTapsRequired:2];
            [doubleTapGestureRecogniser setNumberOfTouchesRequired:1];
            [doubleTapGestureRecogniser setDelegate:self];
            [self addGestureRecognizer:doubleTapGestureRecogniser];
            
            [singleTapGestureRecogniser requireGestureRecognizerToFail: doubleTapGestureRecogniser];
            
            UISwipeGestureRecognizer *swipeGestureRecogniser = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
            [swipeGestureRecogniser setDelegate:self];
            [self addGestureRecognizer:swipeGestureRecogniser];
            
            UILongPressGestureRecognizer *longPressRecogniser = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
            [longPressRecogniser setDelegate:self];
            [self addGestureRecognizer:longPressRecogniser];
        }
        
        return(self);
}

-(void) loadTexture
{
    
    int i, j, c;
    for (i = 0; i < checkImageHeight_ak; i++) {
        for (j = 0; j < checkImageWidth_ak; j++) {
            c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;
            checkImage_ak[i][j][0] = (GLubyte)c;
            checkImage_ak[i][j][1] = (GLubyte)c;
            checkImage_ak[i][j][2] = (GLubyte)c;
            checkImage_ak[i][j][3] = 255;

        }
    }
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glGenTextures(1, &textureImage_ak);
    glBindTexture(GL_TEXTURE_2D, textureImage_ak);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,checkImageWidth_ak,checkImageHeight_ak,0,GL_RGBA,GL_UNSIGNED_BYTE,checkImage_ak);
}

-(void)layoutSubviews
{
    
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)[self layer]];

    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER_WIDTH, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("FrameBuffer is not complete.\n");
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    perspectiveProjectMatrix_ak = perspective(45.0f, ((GLfloat)width/(GLfloat)height),0.1f, 100.0f);
    
    [self drawView];
}

+(Class)layerClass
{
    return ([CAEAGLLayer class]);
}


-(void)drawView
{
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(gShaderProgramObject_ak);
    //Cube
    //Vertices
    GLfloat cubeVertices[] =
    {
        -2.0f,-1.0f, 0.0f ,
        -2.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, -1.0f, 0.0f
    };

    //TexCoord
    GLfloat cubeTexCoord[] =
    {
        0.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f
    };
    
    //OpenGL Draw
    // ModelViewMatrix and Load Identity
    mat4 modelViewMatrix = mat4::identity();
    mat4 modelViewProjectionMatrix = mat4::identity();

    mat4 translateMatrix = translate(0.0f, 0.0f, -6.0f);
    modelViewMatrix = modelViewMatrix * translateMatrix;

    modelViewProjectionMatrix = perspectiveProjectMatrix_ak * modelViewMatrix;
    
    glUniformMatrix4fv(mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix);

    //Texture Cube
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureImage_ak);
    glUniform1i(textureSamplerUniform_ak, 0);

    //Cube Begin
    glBindVertexArray(Vao_cube_ak);

    glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_cube_ak);
    glBufferData(GL_ARRAY_BUFFER, 4 * 3 * sizeof(float), cubeVertices,GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, Vbo_texture_cube_ak);
    glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(float), cubeTexCoord,GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    cubeVertices[0] = 1.0f;
    cubeVertices[1] = -1.0f;
    cubeVertices[2] = 0.0f;
    cubeVertices[3] = 1.0f;
    cubeVertices[4] = 1.0f;
    cubeVertices[5] = 0.0f;
    cubeVertices[6] = 2.41421f;
    cubeVertices[7] = 1.0f;
    cubeVertices[8] = -1.41421f;
    cubeVertices[9] = 2.41421f;
    cubeVertices[10] = -1.0f;
    cubeVertices[11] = -1.41421f;

    glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_cube_ak);
    glBufferData(GL_ARRAY_BUFFER,4 * 3 * sizeof(float), cubeVertices,GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glDrawArrays(GL_TRIANGLE_FAN,0,4);

    glBindTexture(GL_TEXTURE_2D, 0);
    
    glBindVertexArray(0);

    glUseProgram(0);

    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
}

-(void)startAnimation
{
    if(isAnimating == NO)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget: self selector: @selector(drawView)];
        
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
        
    }
}

-(void)stopAnimation
{
    if(isAnimating == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    [self setNeedsDisplay];
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    [self setNeedsDisplay];
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    [self unitialize];
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    [self setNeedsDisplay];
}

-(void) unitialize
{
        if(Vao_cube_ak)
        {
            glDeleteVertexArrays(1, &Vao_cube_ak);
            Vao_cube_ak = 0;
        }

        if(Vbo_position_cube_ak)
        {
            glDeleteVertexArrays(1, &Vbo_position_cube_ak);
            Vbo_position_cube_ak = 0;
        }

        if(Vbo_color_cube_ak)
        {
            glDeleteVertexArrays(1, &Vbo_color_cube_ak);
            Vbo_color_cube_ak = 0;
        }

        if(textureImage_ak)
        {
            glDeleteTextures(1, &textureImage_ak);
            textureImage_ak = 0;
        }
        // glDetachShader(gShaderProgramObject_ak, gVertexShaderObject_ak);
        // glDetachShader(gShaderProgramObject_ak, gFragmentShaderObject_ak);

        // glDeleteShader(gVertexShaderObject_ak);
        // gVertexShaderObject_ak = 0;

        // glDeleteShader(gFragmentShaderObject_ak);
        // gFragmentShaderObject_ak = 0;

        // glUseProgram(0);

        if (gShaderProgramObject_ak) {
            glUseProgram(gShaderProgramObject_ak);
            GLsizei shaderCount_ak;
            glGetProgramiv(gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

            GLuint* pShaders_ak = NULL;
            pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
            glGetAttachedShaders(gShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);

            for (GLsizei i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
                glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
                glDeleteShader(pShaders_ak[i_ak]);
                pShaders_ak[i_ak] = 0;
            }

            free(pShaders_ak);
            glDeleteProgram(gShaderProgramObject_ak);
            gShaderProgramObject_ak = 0;

            glUseProgram(0);
        }

    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
    }
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext:nil];
        }
    }
    
    [eaglContext release];
    eaglContext = nil;
}

-(void) dealloc
{
    [self unitialize];
    [super dealloc];
}
@end
