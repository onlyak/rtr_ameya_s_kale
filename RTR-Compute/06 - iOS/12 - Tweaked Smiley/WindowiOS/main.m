//
//  main.m
//  WindowiOS
//
//  Created by ameya kale on 03/07/21.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    NSString *appDelegateClassName;
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
        appDelegateClassName = NSStringFromClass([AppDelegate class]);
        
    int ret = UIApplicationMain(argc, argv, nil, appDelegateClassName);
    
    [pool release];
    
    return ret;
}
