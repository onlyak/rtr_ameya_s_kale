//
//  MyView.h
//  WindowiOS
//
//  Created by ameya kale on 03/07/21.
//

#import <UIKit/UIKit.h>

@interface GLESView : UIView <UIGestureRecognizerDelegate>
-(void)startAnimation;
-(void)stopAnimation;
@end
