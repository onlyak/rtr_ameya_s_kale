//
//  MyView.m
//  WindowiOS
//
//  Created by ameya kale on 03/07/21.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "GLESView.h"

#import "vmath.h"

using namespace vmath;

enum
{
    ATTRIBUTE_POSITION = 0,
    ATTRIBUTE_COLOR,
    ATTRIBUTE_NORMAL,
    ATRIBUTE_TEXCOORD,
};

float tAngle = 0.0f;
float sAngle = 0.0f;

@implementation GLESView {
    @private
    EAGLContext *eaglContext;
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint gVertexShaderObject_ak;
    GLuint gFragmentShaderObject_ak;
    GLuint gShaderProgramObject_ak;

    GLuint vao_pyramid_ak;
    GLuint vbo_pyramid_color_ak;
    GLuint vbo_pyramid_position_ak;

    GLuint Vao_cube_ak;
    GLuint Vbo_color_cube_ak;
    GLuint Vbo_position_cube_ak;
    GLuint mvpMatrixUniform_ak;

    mat4 perspectiveProjectMatrix;

}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame: frame];
        if(self)
        {
            CAEAGLLayer *eaglLayer = (CAEAGLLayer *)[super layer];
            [eaglLayer setOpaque:YES];
            [eaglLayer setDrawableProperties:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], kEAGLDrawablePropertyRetainedBacking,
                                              kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil]];
            
            //context Creation
            
            eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
            if(eaglContext == nil)
            {
                printf("OpenGL context not available");
            }
            
            [EAGLContext setCurrentContext:eaglContext];
            
            glGenFramebuffers(1, &defaultFramebuffer);
            glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
            glGenRenderbuffers(1, &colorRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
            
            // storage for render buffer
            [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
            
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
            
            GLint backingWidth;
            GLint backingHeight;
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
            
            glGenRenderbuffers(1, &depthRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER_WIDTH, depthRenderbuffer);
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
            
            if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            {
                printf("FrameBuffer is not complete.\n");
                [self unitialize];
                return nil;
            }
            
            animationFrameInterval = 60; // from iOS 8.2
            
            isAnimating = NO;
            
            gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
           const GLchar* vertexShaderSourceCode =
               "#version 300 es" \
               "\n" \
               "precision lowp int;" \
               "in vec4 vPosition;" \
               "in vec4 vColor;" \
               "out vec4 out_color;" \
               "uniform mat4 u_mvpMatrix;" \
               "void main(void)" \
               "{" \
               "gl_Position = u_mvpMatrix * vPosition;" \
               "out_color = vColor;" \
               "}";

           glShaderSource(gVertexShaderObject_ak, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
           glCompileShader(gVertexShaderObject_ak);

           GLint infoLogLength_ak = 0;
           GLint shaderCompileStatus_ak = 0;
           char* szBuffer_ak = NULL;
           glGetShaderiv(gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
           if (shaderCompileStatus_ak == GL_FALSE) {
               glGetShaderiv(gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
               if (infoLogLength_ak > 0) {
                   szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
                   if (szBuffer_ak != NULL) {
                       GLsizei written_ak;                                                                         // for 3rd param and can be null
                       glGetShaderInfoLog(gVertexShaderObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);  // who, length, for extra char length if any, buffer
                       free(szBuffer_ak);
                       [self dealloc];
                   }
               }
           }

           gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);

           const GLchar* fragmentSourceCode_ak =
               "#version 300 es" \
               "\n" \
               "precision highp float;" \
               "in vec4 out_color;" \
               "out vec4 FragColor;" \
               "void main(void)" \
               "{" \
               "FragColor = out_color;" \
               "}";

           glShaderSource(gFragmentShaderObject_ak, 1, (const GLchar**)&fragmentSourceCode_ak, NULL);
           glCompileShader(gFragmentShaderObject_ak);

           infoLogLength_ak = 0;
           shaderCompileStatus_ak = 0;
           szBuffer_ak = NULL;
           glGetShaderiv(gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
           if (shaderCompileStatus_ak == GL_FALSE) {
               glGetShaderiv(gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
               if (infoLogLength_ak > 0) {
                   szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
                   if (szBuffer_ak != NULL) {
                       GLsizei written_ak;                                                                           // for 3rd param and can be null
                       glGetShaderInfoLog(gFragmentShaderObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);  // who, length, for extra char length if any, buffer
                       free(szBuffer_ak);
                       [self dealloc];
                   }
               }
           }

           gShaderProgramObject_ak = glCreateProgram();
           glAttachShader(gShaderProgramObject_ak, gVertexShaderObject_ak);
           glAttachShader(gShaderProgramObject_ak, gFragmentShaderObject_ak);

           glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");
           glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_COLOR, "vColor");

           glLinkProgram(gShaderProgramObject_ak);

           GLint shaderProgramLinkStatus = 0;
           glGetProgramiv(gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
           if (shaderProgramLinkStatus == GL_FALSE) {
               glGetProgramiv(gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
               if (infoLogLength_ak > 0) {
                   szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
                   if (szBuffer_ak != NULL) {
                       GLsizei written_ak;                                                                           // for 3rd param and can be null
                       glGetProgramInfoLog(gShaderProgramObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);  // who, length, for extra char length if any, buffer
                       free(szBuffer_ak);
                       [self dealloc];
                   }
               }
           }

           mvpMatrixUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_mvpMatrix");

           // vertices array declaration
           const GLfloat triangleVertices_ak[] = {
               0.0f, 1.0f, 0.0f,
               -1.0f, -1.0f, 1.0f,
               1.0f, -1.0f, 1.0f,

               0.0f, 1.0f, 0.0f,
               1.0f, -1.0f, 1.0f,
               1.0f, -1.0f, -1.0f,

               0.0f, 1.0f, 0.0f,
               1.0f, -1.0f, -1.0f,
               -1.0f, -1.0f, -1.0f,

               0.0f, 1.0f, 0.0f,
               -1.0f, -1.0f, -1.0f,
               -1.0f, -1.0f, 1.0f
           };

           const GLfloat triangleColors[]=
           {
               1.0f, 0.0f, 0.0f,
               0.0f, 1.0f, 0.0f,
               0.0f, 0.0f, 1.0f,

               1.0f, 0.0f, 0.0f,
               0.0f, 0.0f, 1.0f,
               0.0f, 1.0f, 0.0f,

               1.0f, 0.0f, 0.0f,
               0.0f, 1.0f, 0.0f,
               0.0f, 0.0f, 1.0f,

               1.0f, 0.0f, 0.0f,
               0.0f, 0.0f, 1.0f,
               0.0f, 1.0f, 0.0f
           };


           const GLfloat squareVertices[] =
           {
               1.0f, 1.0f, -1.0f,
               -1.0f, 1.0f, -1.0f,
               -1.0f, 1.0f, 1.0f,
               1.0f, 1.0f, 1.0f,

               1.0f, -1.0f, -1.0f,
               -1.0f, -1.0f, -1.0f,
               -1.0f, -1.0f, 1.0f,
               1.0f, -1.0f, 1.0f,

               1.0f, 1.0f, 1.0f,
               -1.0f, 1.0f, 1.0f,
               -1.0f, -1.0f, 1.0f,
               1.0f, -1.0f, 1.0f,

               1.0f, 1.0f, -1.0f,
               -1.0f, 1.0f, -1.0f,
               -1.0f, -1.0f, -1.0f,
               1.0f, -1.0f, -1.0f,

               1.0f, 1.0f, -1.0f,
               1.0f, 1.0f, 1.0f,
               1.0f, -1.0f, 1.0f,
               1.0f, -1.0f, -1.0f,

               -1.0f, 1.0f, 1.0f,
               -1.0f, 1.0f, -1.0f,
               -1.0f, -1.0f, -1.0f,
               -1.0f, -1.0f, 1.0f,
           };

           //Color
           const GLfloat squareColor[] =
           {
               1.0f, 0.0f, 0.0f,
               1.0f, 0.0f, 0.0f,
               1.0f, 0.0f, 0.0f,
               1.0f, 0.0f, 0.0f,
               
               0.0f, 1.0f, 0.0f,
               0.0f, 1.0f, 0.0f,
               0.0f, 1.0f, 0.0f,
               0.0f, 1.0f, 0.0f,

               0.0f, 0.0f, 1.0f,
               0.0f, 0.0f, 1.0f,
               0.0f, 0.0f, 1.0f,
               0.0f, 0.0f, 1.0f,

               0.0f, 1.0f, 1.0f,
               0.0f, 1.0f, 1.0f,
               0.0f, 1.0f, 1.0f,
               0.0f, 1.0f, 1.0f,

               1.0f, 0.0f, 1.0f,
               1.0f, 0.0f, 1.0f,
               1.0f, 0.0f, 1.0f,
               1.0f, 0.0f, 1.0f,

               1.0f, 1.0f, 0.0f,
               1.0f, 1.0f, 0.0f,
               1.0f, 1.0f, 0.0f,
               1.0f, 1.0f, 0.0f,
           };

           glGenVertexArrays(1, &vao_pyramid_ak);
           glBindVertexArray(vao_pyramid_ak);

           glGenBuffers(1, &vbo_pyramid_position_ak);
           glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_position_ak);
           glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices_ak), triangleVertices_ak, GL_STATIC_DRAW);
           glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
           glEnableVertexAttribArray(ATTRIBUTE_POSITION);
           glBindBuffer(GL_ARRAY_BUFFER, 0);


           glGenBuffers(1, &vbo_pyramid_color_ak);
           glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramid_color_ak);
           glBufferData(GL_ARRAY_BUFFER, sizeof(triangleColors), triangleColors, GL_STATIC_DRAW);
           glVertexAttribPointer(ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
           glEnableVertexAttribArray(ATTRIBUTE_COLOR);

           glBindBuffer(GL_ARRAY_BUFFER, 0);

           glGenVertexArrays(1, &Vao_cube_ak);
           glBindVertexArray(Vao_cube_ak);

           //Position
           glGenBuffers(1, &Vbo_position_cube_ak);
           glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_cube_ak);
           glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
           glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
           glEnableVertexAttribArray(ATTRIBUTE_POSITION);
           glBindBuffer(GL_ARRAY_BUFFER, 0);

           //Color
           glGenBuffers(1, &Vbo_color_cube_ak);
           glBindBuffer(GL_ARRAY_BUFFER, Vbo_color_cube_ak);
           glBufferData(GL_ARRAY_BUFFER, sizeof(squareColor), squareColor, GL_STATIC_DRAW);
           glVertexAttribPointer(ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
           glEnableVertexAttribArray(ATTRIBUTE_COLOR);
           glBindBuffer(GL_ARRAY_BUFFER, 0);

           glBindVertexArray(0);

           glBindVertexArray(0);

           glClearDepthf(1.0f);
           glEnable(GL_DEPTH_TEST);
           glDepthFunc(GL_LEQUAL);

           glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
            perspectiveProjectMatrix = vmath::mat4::identity();

            
            // Gestures
            UITapGestureRecognizer *singleTapGestureRecogniser = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
            [singleTapGestureRecogniser setNumberOfTapsRequired:1];
            [singleTapGestureRecogniser setNumberOfTouchesRequired:1];
            [singleTapGestureRecogniser setDelegate:self];
            [self addGestureRecognizer:singleTapGestureRecogniser];
            
            UITapGestureRecognizer *doubleTapGestureRecogniser = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
            [doubleTapGestureRecogniser setNumberOfTapsRequired:2];
            [doubleTapGestureRecogniser setNumberOfTouchesRequired:1];
            [doubleTapGestureRecogniser setDelegate:self];
            [self addGestureRecognizer:doubleTapGestureRecogniser];
            
            [singleTapGestureRecogniser requireGestureRecognizerToFail: doubleTapGestureRecogniser];
            
            UISwipeGestureRecognizer *swipeGestureRecogniser = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
            [swipeGestureRecogniser setDelegate:self];
            [self addGestureRecognizer:swipeGestureRecogniser];
            
            UILongPressGestureRecognizer *longPressRecogniser = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
            [longPressRecogniser setDelegate:self];
            [self addGestureRecognizer:longPressRecogniser];
        }
        
        return(self);
}
//- (void)drawRect:(CGRect)rect {
//    // Drawing code
//
//}

-(void)layoutSubviews
{
    
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)[self layer]];

    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER_WIDTH, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("FrameBuffer is not complete.\n");
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    perspectiveProjectMatrix = perspective(45.0f, ((GLfloat)width/(GLfloat)height),0.1f, 100.0f);
    
    [self drawView];
}

+(Class)layerClass
{
    return ([CAEAGLLayer class]);
}

-(void)drawView
{
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
    
    glUseProgram(gShaderProgramObject_ak);

    vmath::mat4 modelViewMatrix_ak;
    vmath::mat4 modelViewProjectionMatrix_ak;
    vmath::mat4 translateMatrix_ak;
    vmath::mat4 rotationMatrix_ak;

    modelViewMatrix_ak = vmath::mat4::identity();
    modelViewProjectionMatrix_ak = vmath::mat4::identity();

    translateMatrix_ak = vmath::translate(-1.5f, 0.0f, -6.0f);
    rotationMatrix_ak = vmath::rotate(tAngle, 0.0f, 1.0f, 0.0f);
    modelViewMatrix_ak = translateMatrix_ak * rotationMatrix_ak;

    modelViewProjectionMatrix_ak = perspectiveProjectMatrix * modelViewMatrix_ak;
    glUniformMatrix4fv(mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix_ak);

    glBindVertexArray(vao_pyramid_ak);
    glDrawArrays(GL_TRIANGLES, 0, 12);
    glBindVertexArray(0);

    modelViewMatrix_ak = vmath::mat4::identity();
    modelViewProjectionMatrix_ak = vmath::mat4::identity();
    rotationMatrix_ak = vmath::mat4::identity();
    vmath::mat4 scaleMatrix_ak = vmath::mat4:: identity();


    scaleMatrix_ak = vmath::scale(0.75f, 0.75f, 0.75f);
    translateMatrix_ak = vmath::translate(1.5f, 0.0f, -6.0f);

    rotationMatrix_ak = vmath::rotate(sAngle, 1.0f, 0.0f, 0.0f);
    modelViewMatrix_ak = translateMatrix_ak * rotationMatrix_ak * scaleMatrix_ak;

    rotationMatrix_ak = vmath::rotate(sAngle, 0.0f, 1.0f, 0.0f);
    modelViewMatrix_ak = translateMatrix_ak * rotationMatrix_ak * scaleMatrix_ak;

    rotationMatrix_ak = vmath::rotate(sAngle, 0.0f, 0.0f, 1.0f);
    modelViewMatrix_ak = translateMatrix_ak * rotationMatrix_ak * scaleMatrix_ak;

    modelViewProjectionMatrix_ak = perspectiveProjectMatrix * modelViewMatrix_ak;
    glUniformMatrix4fv(mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix_ak);

    glBindVertexArray(Vao_cube_ak);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    glBindVertexArray(Vao_cube_ak);

    glUseProgram(0);

    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
    tAngle+= 0.1f;
    if(tAngle >= 360.0f)
        tAngle = 0.0f;
    
    sAngle+= 0.1f;
    if(sAngle >= 360.0f)
        sAngle = 0.0f;
}

-(void)startAnimation
{
    if(isAnimating == NO)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget: self selector: @selector(drawView)];
        
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
        
    }
}

-(void)stopAnimation
{
    if(isAnimating == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    [self setNeedsDisplay];
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    [self setNeedsDisplay];
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    [self unitialize];
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    [self setNeedsDisplay];
}

-(void) unitialize
{
        if (vao_pyramid_ak) {
            glDeleteVertexArrays(1, &vao_pyramid_ak);
            vao_pyramid_ak = 0;
        }

        if (vbo_pyramid_position_ak) {
            glDeleteVertexArrays(1, &vbo_pyramid_position_ak);
            vbo_pyramid_position_ak = 0;
        }

        if(Vao_cube_ak)
        {
            glDeleteVertexArrays(1, &Vao_cube_ak);
            Vao_cube_ak = 0;
        }

        if(Vbo_position_cube_ak)
        {
            glDeleteVertexArrays(1, &Vbo_position_cube_ak);
            Vbo_position_cube_ak = 0;
        }

        if(Vbo_color_cube_ak)
        {
            glDeleteVertexArrays(1, &Vbo_color_cube_ak);
            Vbo_color_cube_ak = 0;
        }

        // glDetachShader(gShaderProgramObject_ak, gVertexShaderObject_ak);
        // glDetachShader(gShaderProgramObject_ak, gFragmentShaderObject_ak);

        // glDeleteShader(gVertexShaderObject_ak);
        // gVertexShaderObject_ak = 0;

        // glDeleteShader(gFragmentShaderObject_ak);
        // gFragmentShaderObject_ak = 0;

        // glUseProgram(0);

        if (gShaderProgramObject_ak) {
            glUseProgram(gShaderProgramObject_ak);
            GLsizei shaderCount_ak;
            glGetProgramiv(gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

            GLuint* pShaders_ak = NULL;
            pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
            glGetAttachedShaders(gShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);

            for (GLsizei i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
                glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
                glDeleteShader(pShaders_ak[i_ak]);
                pShaders_ak[i_ak] = 0;
            }

            free(pShaders_ak);
            glDeleteProgram(gShaderProgramObject_ak);
            gShaderProgramObject_ak = 0;

            glUseProgram(0);
        }

    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
    }
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext:nil];
        }
    }
    
    [eaglContext release];
    eaglContext = nil;
}

-(void) dealloc
{
    [self unitialize];
    [super dealloc];
}
@end
