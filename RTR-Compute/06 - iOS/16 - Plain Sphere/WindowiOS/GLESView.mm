
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"

#import "vmath.h"
#import "sphere.h"
using namespace vmath;

Sphere *sphere = new Sphere();

GLuint bLight=0;

@implementation GLESView {
@private
  EAGLContext* eaglContext;

  GLuint defaultFramebuffer;
  GLuint colorRenderbuffer;
  GLuint depthRenderbuffer;

  id displayLink;
  NSInteger animationFrameInterval;
  BOOL isAnimating;
    
  enum {
    ATTRIBUTE_POSITION = 0,
    ATTRIBUTE_COLOR,
    ATTRIBUTE_NORMAL,
    ATTRIBUTE_TEXCOORD
  };
    
    GLuint gVertexShaderObject_ak;
    GLuint gFragmentShaderObject_ak;
    GLuint gShaderProgramObject_ak;

    GLuint Vao_sphere;
    GLuint Vbo_position_sphere;
    GLuint Vbo_element_sphere;
    
    //Uniforms
    GLuint modelViewMatrixUniform;
    GLuint modelViewProjectionMatrixUniform;
    GLuint ldUniform;
    GLuint kdUniform;
    GLuint lightPositionUniform;

    GLuint lKeyPressedUniform;
    
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    unsigned short sphere_elements[2280];

    int numVertices;
    int numElements;

    mat4 perspectiveProjectionMatrix_ak;
}

    -(id)initWithFrame:(CGRect)frame
    {
      self = [super initWithFrame:frame];
      if (self) {
        CAEAGLLayer* eaglLayer = (CAEAGLLayer*)[super layer];

        [eaglLayer setOpaque:YES];

        [eaglLayer setDrawableProperties:[NSDictionary
                                           dictionaryWithObjectsAndKeys:
                                             [NSNumber numberWithBool:NO],
                                             kEAGLDrawablePropertyRetainedBacking,
                                             kEAGLColorFormatRGBA8,
                                             kEAGLDrawablePropertyColorFormat,
                                             nil]];

        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if (eaglContext == nil) {
          printf("OpenGL-ES Context Creation Failed.\n");
          return (nil);
        }

        [EAGLContext setCurrentContext:eaglContext];

        glGenFramebuffers(1, &defaultFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        glFramebufferRenderbuffer(
          GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);

        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(
          GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(
          GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);

        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(
          GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(
          GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
          printf("Framebuffer is not complete\n");
          [self unintialize];
          return (nil);
        }

        animationFrameInterval = 60;
        isAnimating = NO;

      gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
      const GLchar *vertexShaderSourceCode =
      "#version 300 es" \
      "\n" \
      "in vec4 vPosition;" \
      "in vec3 vNormal;"  \
      "uniform mat4 u_model_view_matrix;" \
      "uniform mat4 u_projection_matrix;" \
      "uniform lowp int u_LKeyPressed;" \
      "uniform vec3 u_Ld;" \
      "uniform vec3 u_Kd;" \
      "uniform vec4 u_light_position;" \
      "out vec3 diffuse_light;" \
      "void main(void)" \
      "{" \
          "if (u_LKeyPressed == 1) " \
          "{" \
              "vec4 eyeCoordinates = u_model_view_matrix * vPosition;" \
              "mat3 normalMatrix = mat3(transpose(inverse(u_model_view_matrix)));" \
              "vec3 tnorm = normalize(normalMatrix * vNormal);" \
              "vec3 s = normalize(vec3(u_light_position - eyeCoordinates));" \
              "diffuse_light = u_Ld * u_Kd * max(dot(s, tnorm), 0.0);" \
          "}" \
          "gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;" \
      "}";
      
      glShaderSource(gVertexShaderObject_ak,1,(const GLchar **)&vertexShaderSourceCode, NULL);

      // compile Vertex Shader
      glCompileShader(gVertexShaderObject_ak);

      // Error checking for Vertex Shader
      GLint infoLogLength = 0;
      GLint shaderCompiledStatus = 0;
      char *szBuffer = NULL;

      glGetShaderiv(gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
      if(shaderCompiledStatus == GL_FALSE)
      {
          glGetShaderiv(gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
          if(infoLogLength>0)
          {
              szBuffer = (char *)malloc(infoLogLength);
              if(szBuffer!=NULL)
              {
                  GLsizei written;
                  glGetShaderInfoLog(gVertexShaderObject_ak, infoLogLength, &written, szBuffer);
                  printf("Vertex Shader Compilation Log: %s\n",szBuffer);
                  free(szBuffer);
                  szBuffer = NULL;
              }
          }
      }

      //Fragment Shader
      /* out_color is the output of Vertex Shader */
      gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);
      const GLchar* vertexFragmentSourceCode =
      "#version 300 es" \
      "\n" \
      "precision highp float;" \
      "precision lowp int;" \
      "in vec3 diffuse_light;" \
      "out vec4 FragColor;" \
      "uniform int u_LKeyPressed;" \
      "void main(void)" \
      "{" \
          "vec4 color;" \
          "if (u_LKeyPressed == 1) " \
          "{"
              "color = vec4(diffuse_light, 1.0);"  \
          "}"
          "else" \
          "{" \
              "color = vec4(1.0f,1.0f,1.0f,1.0f);" \
          "}" \
          "FragColor = color;" \
      "}";

      glShaderSource(gFragmentShaderObject_ak,1,(const GLchar **)&vertexFragmentSourceCode, NULL);

      // compile Fragment Shader
      glCompileShader(gFragmentShaderObject_ak);

      // Error Checking for Fragment Shader
      glGetShaderiv(gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
      if(shaderCompiledStatus == GL_FALSE)
      {
          glGetShaderiv(gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
          if(infoLogLength>0)
          {
              szBuffer = (char *)malloc(infoLogLength);
              if(szBuffer!=NULL)
              {
                  GLsizei written;
                  glGetShaderInfoLog(gFragmentShaderObject_ak, infoLogLength, &written, szBuffer);
                  printf("Fragment Shader Compilation Log: %s\n",szBuffer);
                  free(szBuffer);
                  szBuffer = NULL;
              }
          }
      }

      //Shader Program
      gShaderProgramObject_ak = glCreateProgram();
      glAttachShader(gShaderProgramObject_ak,gVertexShaderObject_ak);
      glAttachShader(gShaderProgramObject_ak,gFragmentShaderObject_ak);

      // Bind the attributes in shader with the enums in your main program
      /* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
      glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");

      glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_NORMAL, "vNormal");
      
      glLinkProgram(gShaderProgramObject_ak);

      // Linking Error Checking
      GLint shaderProgramLinkStatus = 0;
      szBuffer = NULL;

      glGetProgramiv(gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
      if(shaderProgramLinkStatus == GL_FALSE)
      {
          glGetProgramiv(gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
          if(infoLogLength>0)
          {
              szBuffer = (char *)malloc(infoLogLength);
              if(szBuffer!=NULL)
              {
                  GLsizei written;
                  glGetProgramInfoLog(gShaderProgramObject_ak, infoLogLength, &written, szBuffer);
                  printf("Shader Program Link Log: %s\n",szBuffer);
                  free(szBuffer);
                  szBuffer = NULL;
              }
          }
      }

      //Get the information of uniform Post linking
      modelViewMatrixUniform = glGetUniformLocation(gShaderProgramObject_ak, "u_model_view_matrix");
      modelViewProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject_ak, "u_projection_matrix");

      ldUniform = glGetUniformLocation(gShaderProgramObject_ak, "u_Ld");
      kdUniform = glGetUniformLocation(gShaderProgramObject_ak, "u_Kd");
      lightPositionUniform = glGetUniformLocation(gShaderProgramObject_ak, "u_light_position");
      lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject_ak, "u_LKeyPressed");


      //Shader attributes for sphere
      sphere->getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
      numVertices = sphere->getNumberOfSphereVertices();
      numElements = sphere->getNumberOfSphereElements();

      // Push the above vertices to vPosition

      //Steps
      /* 1. Tell OpenGl to create one buffer in your VRAM
            Give me a symbol to identify. It is known as NamedBuffer
            In OpenGL terminology, it is called as GL_ARRAY_BUFFER. This is becase vertex has plenty of attributes
            like color, texture, etc. Also it requires contiguous memory.
            User identifies this variable as Vbo_position and GPU as GL_ARRAY_BUFFER.
         2. Bind with the above symbol. It doesn't unbind until 'unbind step' is performed eg- Railway track
         3. Insert triangle data into the buffer.
         4. Specify where to insert this data into shader and also how to use it.
         5. Enable the 'in' point.
         6. Unbind
      */

      //Sphere
      
      // Sphere Vao
      glGenVertexArrays(1, &Vao_sphere);
      glBindVertexArray(Vao_sphere);

      // Sphere position Vbo
      glGenBuffers(1, &Vbo_position_sphere);
      glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_sphere);
      glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
      glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
      glEnableVertexAttribArray(ATTRIBUTE_POSITION);
      glBindBuffer(GL_ARRAY_BUFFER, 0);


      // Sphere element Vbo
      glGenBuffers(1, &Vbo_element_sphere);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Vbo_element_sphere);
      glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

      // unbind vao
      glBindVertexArray(0);

      glClearDepthf(1.0f);
      glEnable(GL_DEPTH_TEST);
      glDepthFunc(GL_LEQUAL);
      //glEnable(GL_CULL_FACE);
      glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

      // Set PerspectiveMatrix to identity matrix
      perspectiveProjectionMatrix_ak = mat4::identity();


    UITapGestureRecognizer* singleTapGestureRecognizer =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                              action:@selector(onSingleTap:)];

    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
    [singleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:singleTapGestureRecognizer];

    UITapGestureRecognizer* doubleTapGestureRecognizer =
      [[UITapGestureRecognizer alloc] initWithTarget:self
                                              action:@selector(onDoubleTap:)];

    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
    [doubleTapGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:doubleTapGestureRecognizer];

    [singleTapGestureRecognizer
      requireGestureRecognizerToFail:doubleTapGestureRecognizer];

    UISwipeGestureRecognizer* swipeGestureRecognizer =
      [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(onSwipe:)];
    [swipeGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:swipeGestureRecognizer];

    UILongPressGestureRecognizer* longPressGestureRecognizer =
      [[UILongPressGestureRecognizer alloc]
        initWithTarget:self
                action:@selector(onLongPress:)];
    [longPressGestureRecognizer setDelegate:self];
    [self addGestureRecognizer:longPressGestureRecognizer];
  }
  return (self);
}

+ (Class)layerClass
{
  return ([CAEAGLLayer class]);
}

- (void)layoutSubviews
{
  glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
  [eaglContext renderbufferStorage:GL_RENDERBUFFER
                      fromDrawable:(CAEAGLLayer*)[self layer]];
  glFramebufferRenderbuffer(
    GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);

  GLint width;
  GLint height;
  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
  glGetRenderbufferParameteriv(
    GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);

  glGenRenderbuffers(1, &depthRenderbuffer);
  glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
  glFramebufferRenderbuffer(
    GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);

  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
    printf("Framebuffer is not complete in layoutSubviews");
  }

  if (height < 0) {
    height = 1;
  }

  glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    perspectiveProjectionMatrix_ak =
    vmath::perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
  [self drawView:nil];
}

- (void)drawView:(id)sender
{
  [EAGLContext setCurrentContext:eaglContext];
  glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(gShaderProgramObject_ak);

        if (bLight == true)
        {
            glUniform1i(lKeyPressedUniform, 1);
            glUniform3f(ldUniform, 1.0f, 1.0f, 1.0f);
            glUniform3f(kdUniform, 0.5f, 0.5f, 0.5f);

            GLfloat lightPosition_ak[] = { 0.0f, 0.0f, 2.0f, 1.0f };
            glUniform4fv(lightPositionUniform, 1, lightPosition_ak);
        }

        else
            glUniform1i(lKeyPressedUniform, 0);
        
        //OpenGL Draw

        //Set ModelView and ModelViewProjection matrices to identity
        mat4 modelMatrix = mat4::identity();

        mat4 translateMatrix = translate(0.0f, 0.0f, -6.0f);
        modelMatrix = translateMatrix;

        glUniformMatrix4fv(modelViewMatrixUniform, 1, GL_FALSE, modelMatrix);

        glUniformMatrix4fv(modelViewProjectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

        //Sphere Begin
        glBindVertexArray(Vao_sphere);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Vbo_element_sphere);
        glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
        
        glBindVertexArray(0); // Sphere end

        glUseProgram(0);

    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
  
}

- (void)startAnimation
{
  if (isAnimating == NO) {
    displayLink = [NSClassFromString(@"CADisplayLink")
      displayLinkWithTarget:self
                   selector:@selector(drawView:)];
    [displayLink setPreferredFramesPerSecond:animationFrameInterval];
    [displayLink addToRunLoop:[NSRunLoop currentRunLoop]
                      forMode:NSDefaultRunLoopMode];

    isAnimating = YES;
  }
}

- (void)stopAnimation
{
  if (isAnimating == YES) {
    [displayLink invalidate];
    displayLink = nil;
    isAnimating = NO;
  }
}

- (void)onSingleTap:(UITapGestureRecognizer*)gr
{
    if (bLight == 0)
        bLight = 1;
    else
        bLight = 0;
}

- (void)onDoubleTap:(UITapGestureRecognizer*)gr
{}

- (void)onSwipe:(UISwipeGestureRecognizer*)gr
{
  [self unintialize];
  [self release];
  exit(0);
}

- (void)onLongPress:(UILongPressGestureRecognizer*)gr
{}

- (void)unintialize
{
    if(Vao_sphere)
    {
        glDeleteVertexArrays(1, &Vao_sphere);
        Vao_sphere = 0;
    }

    if(Vbo_position_sphere)
    {
        glDeleteVertexArrays(1, &Vbo_position_sphere);
        Vbo_position_sphere = 0;
    }

    if(Vbo_element_sphere)
    {
        glDeleteVertexArrays(1, &Vbo_element_sphere);
        Vbo_element_sphere = 0;
    }


    if (gShaderProgramObject_ak) {
        glUseProgram(gShaderProgramObject_ak);
        GLsizei shaderCount_ak;
        glGetProgramiv(gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

        GLuint* pShaders_ak = NULL;
        pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
        glGetAttachedShaders(gShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);

        for (int i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
            glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
            glDeleteShader(pShaders_ak[i_ak]);
            pShaders_ak[i_ak] = 0;
        }

        free(pShaders_ak);
        glDeleteProgram(gShaderProgramObject_ak);
        gShaderProgramObject_ak = 0;

        glUseProgram(0);
  }

  if (depthRenderbuffer) {
    glDeleteRenderbuffers(1, &depthRenderbuffer);
    depthRenderbuffer = 0;
  }

  if (colorRenderbuffer) {
    glDeleteRenderbuffers(1, &colorRenderbuffer);
    colorRenderbuffer = 0;
  }

  if (defaultFramebuffer) {
    glDeleteFramebuffers(1, &defaultFramebuffer);
    defaultFramebuffer = 0;
  }

  if (eaglContext) {
    if ([EAGLContext currentContext] == eaglContext) {
      [EAGLContext setCurrentContext:nil];
    }
  }
}

- (void)dealloc
{
  [self unintialize];
  [super dealloc];
}

@end
