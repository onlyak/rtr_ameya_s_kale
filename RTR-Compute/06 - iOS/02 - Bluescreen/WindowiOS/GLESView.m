//
//  MyView.m
//  WindowiOS
//
//  Created by ameya kale on 03/07/21.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "GLESView.h"

@implementation GLESView {
    @private
    EAGLContext *eaglContext;
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame: frame];
        if(self)
        {
            CAEAGLLayer *eaglLayer = (CAEAGLLayer *)[super layer];
            [eaglLayer setOpaque:YES];
            [eaglLayer setDrawableProperties:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], kEAGLDrawablePropertyRetainedBacking,
                                              kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil]];
            
            //context Creation
            
            eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
            if(eaglContext == nil)
            {
                printf("OpenGL context not available");
            }
            
            [EAGLContext setCurrentContext:eaglContext];
            
            glGenFramebuffers(1, &defaultFramebuffer);
            glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
            glGenRenderbuffers(1, &colorRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
            
            // storage for render buffer
            [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
            
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
            
            GLint backingWidth;
            GLint backingHeight;
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
            
            glGenRenderbuffers(1, &depthRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER_WIDTH, depthRenderbuffer);
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
            
            if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            {
                printf("FrameBuffer is not complete.\n");
                [self unitialize];
                return nil;
            }
            
            animationFrameInterval = 60; // from iOS 8.2
            
            isAnimating = NO;
            
            glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
            
            // Gestures
            UITapGestureRecognizer *singleTapGestureRecogniser = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
            [singleTapGestureRecogniser setNumberOfTapsRequired:1];
            [singleTapGestureRecogniser setNumberOfTouchesRequired:1];
            [singleTapGestureRecogniser setDelegate:self];
            [self addGestureRecognizer:singleTapGestureRecogniser];
            
            UITapGestureRecognizer *doubleTapGestureRecogniser = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
            [doubleTapGestureRecogniser setNumberOfTapsRequired:2];
            [doubleTapGestureRecogniser setNumberOfTouchesRequired:1];
            [doubleTapGestureRecogniser setDelegate:self];
            [self addGestureRecognizer:doubleTapGestureRecogniser];
            
            [singleTapGestureRecogniser requireGestureRecognizerToFail: doubleTapGestureRecogniser];
            
            UISwipeGestureRecognizer *swipeGestureRecogniser = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
            [swipeGestureRecogniser setDelegate:self];
            [self addGestureRecognizer:swipeGestureRecogniser];
            
            UILongPressGestureRecognizer *longPressRecogniser = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
            [longPressRecogniser setDelegate:self];
            [self addGestureRecognizer:longPressRecogniser];
        }
        return(self);
}
//- (void)drawRect:(CGRect)rect {
//    // Drawing code
//
//}

-(void)layoutSubviews
{
    
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)[self layer]];

    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER_WIDTH, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("FrameBuffer is not complete.\n");
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    [self drawView];
}

+(Class)layerClass
{
    return ([CAEAGLLayer class]);
}

-(void)drawView
{
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void)startAnimation
{
    if(isAnimating == NO)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget: self selector: @selector(drawView)];
        
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
        
    }
}

-(void)stopAnimation
{
    if(isAnimating == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
    }
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    [self setNeedsDisplay];
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    [self setNeedsDisplay];
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    [self unitialize];
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    [self setNeedsDisplay];
}

-(void) unitialize
{
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
    }
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext:nil];
        }
    }
    
    [eaglContext release];
    eaglContext = nil;
}

-(void) dealloc
{
    [self unitialize];
    [super dealloc];
}
@end
