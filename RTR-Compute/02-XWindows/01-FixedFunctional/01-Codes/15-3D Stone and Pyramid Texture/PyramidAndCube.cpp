#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glx.h>	
#include <GL/glu.h>

#include <SOIL/SOIL.h>

using namespace std;

GLuint texture_stone;
GLuint kundali_texture_ak;

GLXContext gGlxContext;
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

float Pangle_ak = 0.0f;
float Cangle_ak = 0.0f;

int main()
{
    void CreateWindow();
    void ToggleFullScreen();
    void uninitialize();

    void initialize();
    void resize(int, int);
    void display();
    
    void update();

    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

     //message loop
    XEvent event;
    KeySym keysym;

    bool bDone = false;

    CreateWindow();
    initialize();
    while(bDone==false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
        switch (event.type)
        {
        case MapNotify:
            break;
        case KeyPress:
            keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
            switch (keysym)
            {
            case XK_Escape:
                bDone = true;

            case XK_F:
            case XK_f:
                if (bFullscreen == false)
                {
                    ToggleFullScreen();
                    bFullscreen = true;
                }
                else
                {
                    ToggleFullScreen();
                    bFullscreen = false;
                }
                break;
            default:
                break;
            }
            break;
        case ButtonPress:
            switch (event.xbutton.button)
            {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            default:
                break;
            }
        case MotionNotify:
            break;
        case ConfigureNotify:
            winWidth = event.xconfigure.width;
            winHeight = event.xconfigure.height;
            resize(winWidth,winHeight);
            break;
        case Expose:
            break;
        case DestroyNotify:
            break;
        case 33:
            bDone = true;
        default:
            break;
        }
    }
        display();
        update();
    }
    uninitialize();

    return 0;
}

void CreateWindow()
{
    void uninitialize();

    static int frameBufferAttributes[]={GLX_DOUBLEBUFFER, True,
                                        GLX_RGBA,
                                        GLX_RED_SIZE, 8, 
                                        GLX_BLUE_SIZE, 8,
                                        GLX_ALPHA_SIZE, 8,
                                        GLX_DEPTH_SIZE, 24,
                                        None
                                       };
    

    XSetWindowAttributes winAttributes;
    int defaultScreen;
    int styleMask;

    gpDisplay = XOpenDisplay(NULL);
    if (gpDisplay == NULL)
    {
        printf("ERROR: Unable to Open X Display");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);
    gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));

    if (gpXVisualInfo == NULL)
    {
        printf("ERROR: Unable to Allocate Memory for Visual Info");
        uninitialize();
        exit(1);
    }

    gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

    if (gpXVisualInfo == NULL)
    {
        printf("ERROR: Unable to Get a Visual");
        uninitialize();
        exit(1);
    }

    winAttributes.border_pixel = 0;
    winAttributes.background_pixmap = 0;
    winAttributes.colormap = XCreateColormap(gpDisplay,
                                             RootWindow(gpDisplay, gpXVisualInfo->screen),
                                             gpXVisualInfo->visual,
                                             AllocNone);
    gColormap = winAttributes.colormap;

    winAttributes.background_pixel = BlackPixel(gpDisplay, defaultScreen);

    winAttributes.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay, gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,
                            gpXVisualInfo->depth,
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttributes);

    if (!gWindow)
    {
        printf("ERROR: Failed To Create Main Window");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "3D Pyramid and Cube with Texture");

    Atom windowMAnagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowMAnagerDelete, 1);
    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen()
{
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void initialize(void)
{
	void resize(int, int);
	void uninitialize(void);

    	GLuint loadBitmapAsTexture(const char*);
	
	gGlxContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	if(gGlxContext == NULL)
	{
		printf("Failed to create Rendering Context");
		uninitialize();
		exit(1);
	}
	
	glXMakeCurrent(gpDisplay, gWindow, gGlxContext);

        glEnable(GL_TEXTURE_2D);
    	texture_stone = loadBitmapAsTexture("Stone.bmp");
    	kundali_texture_ak = loadBitmapAsTexture("Vijay_Kundali.bmp");
    	

        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    	glShadeModel(GL_SMOOTH);	
	glClearDepth(1.0f);				
	glEnable(GL_DEPTH_TEST);			
	glDepthFunc(GL_LEQUAL);	
	glHint(GL_PERSPECTIVE_CORRECTION_HINT , GL_NICEST);

	resize(giWindowWidth, giWindowHeight);		
} 

GLuint loadBitmapAsTexture(const char* path)
{
    int width, height;
    unsigned char *imageData = NULL;
    GLuint textureId;

    imageData = SOIL_load_image(path, &width, &height, NULL, SOIL_LOAD_RGB);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 4); //4 --> rgba
    glGenTextures(1, &textureId);
    glBindTexture(GL_TEXTURE_2D, textureId);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    gluBuild2DMipmaps(GL_TEXTURE_2D, 3, width, height, GL_RGB, GL_UNSIGNED_BYTE, (GLvoid *)imageData);	

    SOIL_free_image_data(imageData);

    return textureId;
}

void display(void)
{
		
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(-1.0f, 0.0f, -6.0f);

	glRotatef(Pangle_ak, 0.0f, 1.0f, 0.0f);

	glBindTexture(GL_TEXTURE_2D, texture_stone);
	glBegin(GL_TRIANGLES);
	//glColor3f(1.0f, 0.0f, 0.0f);		// RED
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	//glColor3f(0.0f, 1.0f, 0.0f);		// GREEN
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	//glColor3f(0.0f, 0.0f, 1.0f);		// BLUE
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	// RIGHT FACE
	//glColor3f(1.0f, 0.0f, 0.0f);		// RED
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	//glColor3f(0.0f, 0.0f, 1.0f);		// BLUE
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	//glColor3f(0.0f, 1.0f, 0.0f);		// GREEN
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	// BACK FACE
	//glColor3f(1.0f, 0.0f, 0.0f);		// RED
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	//glColor3f(0.0f, 1.0f, 0.0f);		// GREEN
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//glColor3f(0.0f, 0.0f, 1.0f);		// BLUE
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	// LEFT FACE
	//glColor3f(1.0f, 0.0f, 0.0f);		// RED
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	//glColor3f(0.0f, 0.0f, 1.0f);		// BLUE
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//glColor3f(0.0f, 1.0f, 0.0f);		// GREEN
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glEnd();

	glLoadIdentity();
	glTranslatef(2.0f, 0.0f, -8.0f);

	glRotatef(Cangle_ak, 1.0f, 1.0f, 1.0f);

	glBindTexture(GL_TEXTURE_2D, kundali_texture_ak);
	glBegin(GL_QUADS);
	
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);


	//Bottom Surface
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	// Front Surface
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);


	// Right Surface
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);


	// Back Surface
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);


	// Left Surface
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	
	glEnd();
	
    	glXSwapBuffers(gpDisplay, gWindow); 	
}

void resize(int width, int height)
{
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void update()
{
	Pangle_ak+=0.5f;
	if(Pangle_ak>=360)
		Pangle_ak=0.0f;
	
	Cangle_ak+=0.5f;
	if(Cangle_ak>=360)
		Cangle_ak=0.0f;
}

void uninitialize()
{
    GLXContext currentGlxContext;
    currentGlxContext = glXGetCurrentContext();

    if(currentGlxContext != NULL && currentGlxContext == gGlxContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}

    if(gGlxContext)
	{
		glXDestroyContext(gpDisplay, gGlxContext);
	}

    if (gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if (gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if (gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if (gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }

    if(texture_stone)
    {
        glDeleteTextures(1,&texture_stone);
        texture_stone= 0;
    }
    
     if(kundali_texture_ak)
    {
        glDeleteTextures(1,&kundali_texture_ak);
        kundali_texture_ak= 0;
    }
    
   
}
