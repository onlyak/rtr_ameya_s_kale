#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glx.h>	
#include <GL/glu.h>


using namespace std;

GLXContext gGlxContext;
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

bool gbLight_ak= false;
float angle_ak=0.0f;

GLfloat lightAmbient_ak[]={0.0f,0.0f,0.0f,1.0f}; 
GLfloat lightDiffuse_ak[]={1.0f,1.0f,1.0f,1.0f}; 
GLfloat lightSpecular_ak[] = {1.0f,0.0f,0.0f,1.0f };
GLfloat lightPosition_ak[]={0.0f,0.0f,0.0f,1.0f}; 

GLfloat light_model_ambient_ak[] = { 0.2f,0.2f,0.2f,1.0f };
GLfloat light_model_local_viewer_ak[] = { 0.0f };

GLfloat xRot_ak= 0.0f;
GLfloat yRot_ak = 0.0f;
GLfloat zRot_ak = 0.0f;
GLint keyPressed_ak= 0;

GLUquadric *quadric_ak[24];


int main()
{
    void CreateWindow();
    void ToggleFullScreen();
    void uninitialize();

    void initialize();
    void resize(int, int);
    void display();
    void update();

    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

     //message loop
    XEvent event;
    KeySym keysym;

    bool bDone = false;

    CreateWindow();
    initialize();
    while(bDone==false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
        switch (event.type)
        {
        case MapNotify:
            break;
        case KeyPress:
            keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
            switch (keysym)
            {
            case XK_Escape:
                bDone = true;

            case XK_F:
            case XK_f:
                if (bFullscreen == false)
                {
                    ToggleFullScreen();
                    bFullscreen = true;
                }
                else
                {
                    ToggleFullScreen();
                    bFullscreen = false;
                }
                break;
            case XK_L:
            case XK_l:
                if (gbLight_ak == false)
                {
                    gbLight_ak = true;
                    glEnable(GL_LIGHTING);
                }
                else
                {
                    gbLight_ak = false;
                    glDisable(GL_LIGHTING);
                }
                break;
            case XK_X:
            case XK_x:
		{
		    keyPressed_ak= 1;
		    xRot_ak += 0.0f;
	        }
	    break;

	    case XK_Y:
	    case XK_y:
	     {
		keyPressed_ak= 2;
		yRot_ak = 0.0f;
	     }
	    break;
	    
	    case XK_Z:
	    case XK_z:
	    {
		keyPressed_ak= 3;
		zRot_ak= 0.0f;
	    }
	    break;
            default:
                break;
            }
            break;
        case ButtonPress:
            switch (event.xbutton.button)
            {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            default:
                break;
            }
        case MotionNotify:
            break;
        case ConfigureNotify:
            winWidth = event.xconfigure.width;
            winHeight = event.xconfigure.height;
            resize(winWidth,winHeight);
            break;
        case Expose:
            break;
        case DestroyNotify:
            break;
        case 33:
            bDone = true;
        default:
            break;
        }
    }
        display();
        update();
    }
    uninitialize();

    return 0;
}

void CreateWindow()
{
    void uninitialize();

    static int frameBufferAttributes[]={GLX_DOUBLEBUFFER, True,
                                        GLX_RGBA,
                                        GLX_RED_SIZE, 8, 
                                        GLX_BLUE_SIZE, 8,
                                        GLX_ALPHA_SIZE, 8,
                                        GLX_DEPTH_SIZE, 24,
                                        None
                                       };
    

    XSetWindowAttributes winAttributes;
    int defaultScreen;
    int styleMask;

    gpDisplay = XOpenDisplay(NULL);
    if (gpDisplay == NULL)
    {
        printf("ERROR: Unable to Open X Display");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);
    gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));

    if (gpXVisualInfo == NULL)
    {
        printf("ERROR: Unable to Allocate Memory for Visual Info");
        uninitialize();
        exit(1);
    }

    gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

    if (gpXVisualInfo == NULL)
    {
        printf("ERROR: Unable to Get a Visual");
        uninitialize();
        exit(1);
    }

    winAttributes.border_pixel = 0;
    winAttributes.background_pixmap = 0;
    winAttributes.colormap = XCreateColormap(gpDisplay,
                                             RootWindow(gpDisplay, gpXVisualInfo->screen),
                                             gpXVisualInfo->visual,
                                             AllocNone);
    gColormap = winAttributes.colormap;

    winAttributes.background_pixel = BlackPixel(gpDisplay, defaultScreen);

    winAttributes.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay, gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,
                            gpXVisualInfo->depth,
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttributes);

    if (!gWindow)
    {
        printf("ERROR: Failed To Create Main Window");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "24 Spheres");

    Atom windowMAnagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowMAnagerDelete, 1);
    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen()
{
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void initialize(void)
{
	void resize(int, int);
	void uninitialize(void);

	
	gGlxContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	if(gGlxContext == NULL)
	{
		printf("Failed to create Rendering Context");
		uninitialize();
		exit(1);
	}
	
	glXMakeCurrent(gpDisplay, gWindow, gGlxContext);
    	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    	glShadeModel(GL_SMOOTH);	
	glClearDepth(1.0f);				
	glEnable(GL_DEPTH_TEST);			
	glDepthFunc(GL_LEQUAL);	
	glHint(GL_PERSPECTIVE_CORRECTION_HINT , GL_NICEST);
	
	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);

	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient_ak);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse_ak);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition_ak);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_model_ambient_ak);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, light_model_local_viewer_ak);
	glEnable(GL_LIGHT0);

	for (int i = 0; i < 24; i++)
	{
		quadric_ak[i] = gluNewQuadric();
	}
	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);

	resize(giWindowWidth, giWindowHeight);		
} 

void display(void)
{
	void Spheres24();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

    	if (keyPressed_ak == 1)
	{
		glRotatef(xRot_ak, 1.0f, 0.0f, 0.0f);
		lightPosition_ak[1] = xRot_ak;
	}
	else if (keyPressed_ak == 2)
	{
		glRotatef(yRot_ak, 0.0f, 1.0f, 0.0f);
		lightPosition_ak[2] = yRot_ak;
	}
	else if (keyPressed_ak == 3)
	{
		glRotatef(zRot_ak, 0.0f, 0.0f, 1.0f);
		lightPosition_ak[0] = zRot_ak;
	}

	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition_ak);
	Spheres24();
	
        glXSwapBuffers(gpDisplay, gWindow); 	
}

void resize(int width, int height)
{
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	if (width <= height)
	{
		glOrtho(0.0f,		
			15.5f,			
			0.0f,			
			(15.5f*(GLfloat)height / (GLfloat)width),
			-10.0f,			
			10.0f);			
	}
	else
	{
		glOrtho(0.0f,
			(15.5f*(GLfloat)width / (GLfloat)height),
			0.0f,
			15.5f,
			-10.0f,
			10.0f);
	}

}

void update()
{
    	xRot_ak+=1.0f;
	yRot_ak+=1.0f;
	zRot_ak+=1.0f;
}

void Spheres24()
{	
	GLfloat materialAmbient_ak[4];
	GLfloat materialDiffuse_ak[4];
	GLfloat materialSpecular_ak[4];
	GLfloat materialShininess_ak[1];

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	//sphere 1
	materialAmbient_ak[0] = 0.0215;	
	materialAmbient_ak[1] = 0.1745;	
	materialAmbient_ak[2] = 0.0215;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.07568;	
	materialDiffuse_ak[1] = 0.61424;	
	materialDiffuse_ak[2] = 0.07568;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.633;	
	materialSpecular_ak[1] = 0.727811;	
	materialSpecular_ak[2] = 0.633;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.6 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f, 14.0f, 0.0f);	
	gluSphere(quadric_ak[0], 1.0f, 30, 30);

	//sphere 2
	materialAmbient_ak[0] = 0.135;	
	materialAmbient_ak[1] = 0.2225;	
	materialAmbient_ak[2] = 0.1575;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.54;	
	materialDiffuse_ak[1] = 0.89;	
	materialDiffuse_ak[2] = 0.63;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.316228;	
	materialSpecular_ak[1] = 0.316228;	
	materialSpecular_ak[2] = 0.316228;	 
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.1 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f, 11.5f, 0.0f);	
	gluSphere(quadric_ak[1], 1.0f, 30, 30);

	//sphere 3
	materialAmbient_ak[0] = 0.05375;	
	materialAmbient_ak[1] = 0.05;	
	materialAmbient_ak[2] = 0.06625;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.18275;	
	materialDiffuse_ak[1] = 0.17;	
	materialDiffuse_ak[2] = 0.22525;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.332741;	
	materialSpecular_ak[1] = 0.328634;	
	materialSpecular_ak[2] = 0.346435;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.3 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f, 9.0f, 0.0f);	
	gluSphere(quadric_ak[2], 1.0f, 30, 30);

	//sphere 4
	materialAmbient_ak[0] = 0.25;	
	materialAmbient_ak[1] = 0.20725;	
	materialAmbient_ak[2] = 0.20725;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 1.0;	
	materialDiffuse_ak[1] = 0.829;	
	materialDiffuse_ak[2] = 0.829;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.296648;	
	materialSpecular_ak[1] = 0.296648;	
	materialSpecular_ak[2] = 0.296648;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.088 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f, 6.5f, 0.0f);	
	gluSphere(quadric_ak[3], 1.0f, 30, 30);

	//sphere 5
	materialAmbient_ak[0] = 0.1745;	
	materialAmbient_ak[1] = 0.01175;	
	materialAmbient_ak[2] = 0.01175;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.61424;	
	materialDiffuse_ak[1] = 0.04136;	
	materialDiffuse_ak[2] = 0.04136;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.727811;	
	materialSpecular_ak[1] = 0.626959;	
	materialSpecular_ak[2] = 0.626959;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.6 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f, 4.0f, 0.0f);	
	gluSphere(quadric_ak[4], 1.0f, 30, 30);

	//sphere 6
	materialAmbient_ak[0] = 0.1;		
	materialAmbient_ak[1] = 0.18725;	
	materialAmbient_ak[2] = 0.1745;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.396;	
	materialDiffuse_ak[1] = 0.74151;	
	materialDiffuse_ak[2] = 0.69102;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.297254;	
	materialSpecular_ak[1] = 0.30829;	
	materialSpecular_ak[2] = 0.306678;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.6 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f, 1.5f, 0.0f);	
	gluSphere(quadric_ak[5], 1.0f, 30, 30);
	
	//sphere 7
	materialAmbient_ak[0] = 0.329412;	
	materialAmbient_ak[1] = 0.223529;	
	materialAmbient_ak[2] = 0.027451;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.780392;	
	materialDiffuse_ak[1] = 0.568627;	
	materialDiffuse_ak[2] = 0.113725;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.992157;	
	materialSpecular_ak[1] = 0.941176;	
	materialSpecular_ak[2] = 0.807843;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.21797842 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.0f, 14.0f, 0.0f);	
	gluSphere(quadric_ak[6], 1.0f, 30, 30);

	//sphere 8
	materialAmbient_ak[0] = 0.2125;	
	materialAmbient_ak[1] = 0.1275;	
	materialAmbient_ak[2] = 0.054;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.714;	
	materialDiffuse_ak[1] = 0.4284;	
	materialDiffuse_ak[2] = 0.18144;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.393548;	
	materialSpecular_ak[1] = 0.271906;	
	materialSpecular_ak[2] = 0.166721;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.2 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.0f, 11.5f, 0.0f);	
	gluSphere(quadric_ak[7], 1.0f, 30, 30);

	//sphere 9
	materialAmbient_ak[0] = 0.25;	
	materialAmbient_ak[1] = 0.25;	
	materialAmbient_ak[2] = 0.25;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.4;	
	materialDiffuse_ak[1] = 0.4;	
	materialDiffuse_ak[2] = 0.4;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.774597;	
	materialSpecular_ak[1] = 0.774597;	
	materialSpecular_ak[2] = 0.774597;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.6 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.0f, 9.0f, 0.0f);	
	gluSphere(quadric_ak[8], 1.0f, 30, 30);

	//sphere 10
	materialAmbient_ak[0] = 0.19125;	
	materialAmbient_ak[1] = 0.0735;	
	materialAmbient_ak[2] = 0.0225;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.7038;	
	materialDiffuse_ak[1] = 0.27048;	
	materialDiffuse_ak[2] = 0.0828;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.256777;	
	materialSpecular_ak[1] = 0.137622;	
	materialSpecular_ak[2] = 0.086014;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.1 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.0f, 6.5f, 0.0f);	
	gluSphere(quadric_ak[9], 1.0f, 30, 30);

	//sphere 11
	materialAmbient_ak[0] = 0.24725;	
	materialAmbient_ak[1] = 0.1995;	
	materialAmbient_ak[2] = 0.0745;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.75164;	
	materialDiffuse_ak[1] = 0.60648;	
	materialDiffuse_ak[2] = 0.22648;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.628281;	
	materialSpecular_ak[1] = 0.555802;	
	materialSpecular_ak[2] = 0.366065;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.4 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.0f, 4.0f, 0.0f);	
	gluSphere(quadric_ak[10], 1.0f, 30, 30);

	//sphere 12
	materialAmbient_ak[0] = 0.19225;		
	materialAmbient_ak[1] = 0.19225;	
	materialAmbient_ak[2] = 0.19225;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.50754;	
	materialDiffuse_ak[1] = 0.50754;	
	materialDiffuse_ak[2] = 0.50754;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.508273;	
	materialSpecular_ak[1] = 0.508273;	
	materialSpecular_ak[2] = 0.508273;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.4 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.0f, 1.5f, 0.0f);	
	gluSphere(quadric_ak[11], 1.0f, 30, 30);

	//sphere 13
	materialAmbient_ak[0] = 0.0;	
	materialAmbient_ak[1] = 0.0;	
	materialAmbient_ak[2] = 0.0;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.01;	
	materialDiffuse_ak[1] = 0.01;	
	materialDiffuse_ak[2] = 0.01;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.50;	
	materialSpecular_ak[1] = 0.50;	
	materialSpecular_ak[2] = 0.50;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.5f, 14.0f, 0.0f);	
	gluSphere(quadric_ak[12], 1.0f, 30, 30);

	//sphere 14
	materialAmbient_ak[0] = 0.0;	
	materialAmbient_ak[1] = 0.1;	
	materialAmbient_ak[2] = 0.06;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.0;	
	materialDiffuse_ak[1] = 0.50980392;	
	materialDiffuse_ak[2] = 0.50980392;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.50196078;	
	materialSpecular_ak[1] = 0.50196078;	
	materialSpecular_ak[2] = 0.50196078;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.5f, 11.5f, 0.0f);	
	gluSphere(quadric_ak[13], 1.0f, 30, 30);

	//sphere 15
	materialAmbient_ak[0] = 0.0;	
	materialAmbient_ak[1] = 0.0;	
	materialAmbient_ak[2] = 0.0;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.1;	
	materialDiffuse_ak[1] = 0.35;	
	materialDiffuse_ak[2] = 0.1;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.45;	
	materialSpecular_ak[1] = 0.55;	
	materialSpecular_ak[2] = 0.45;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.5f, 9.0f, 0.0f);	
	gluSphere(quadric_ak[14], 1.0f, 30, 30);

	//sphere 16
	materialAmbient_ak[0] = 0.0;	
	materialAmbient_ak[1] = 0.0;	
	materialAmbient_ak[2] = 0.0;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.5;	
	materialDiffuse_ak[1] = 0.0;	
	materialDiffuse_ak[2] = 0.0;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.7;	
	materialSpecular_ak[1] = 0.6;	
	materialSpecular_ak[2] = 0.6;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.5f, 6.5f, 0.0f);	
	gluSphere(quadric_ak[15], 1.0f, 30, 30);

	//sphere 17
	materialAmbient_ak[0] = 0.0;	
	materialAmbient_ak[1] = 0.0;	
	materialAmbient_ak[2] = 0.0;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.55;	
	materialDiffuse_ak[1] = 0.55;	
	materialDiffuse_ak[2] = 0.55;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.70;	
	materialSpecular_ak[1] = 0.70;	
	materialSpecular_ak[2] = 0.70;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.5f, 4.0f, 0.0f);	
	gluSphere(quadric_ak[16], 1.0f, 30, 30);

	//sphere 18
	materialAmbient_ak[0] = 0.0;		
	materialAmbient_ak[1] = 0.0;	
	materialAmbient_ak[2] = 0.0;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.5;	
	materialDiffuse_ak[1] = 0.5;	
	materialDiffuse_ak[2] = 0.0;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.60;	
	materialSpecular_ak[1] = 0.60;	
	materialSpecular_ak[2] = 0.50;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.5f, 1.5f, 0.0f);	
	gluSphere(quadric_ak[17], 1.0f, 30, 30);

	//sphere 19
	materialAmbient_ak[0] = 0.02;	
	materialAmbient_ak[1] = 0.02;	
	materialAmbient_ak[2] = 0.02;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.01;	
	materialDiffuse_ak[1] = 0.01;	
	materialDiffuse_ak[2] = 0.01;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.4;	
	materialSpecular_ak[1] = 0.4;	
	materialSpecular_ak[2] = 0.4;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(24.0f, 14.0f, 0.0f);	
	gluSphere(quadric_ak[18], 1.0f, 30, 30);

	//sphere 20
	materialAmbient_ak[0] = 0.0;	
	materialAmbient_ak[1] = 0.05;	
	materialAmbient_ak[2] = 0.05;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.4;	
	materialDiffuse_ak[1] = 0.5;	
	materialDiffuse_ak[2] = 0.5;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.04;	
	materialSpecular_ak[1] = 0.7;	
	materialSpecular_ak[2] = 0.7;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(24.0f, 11.5f, 0.0f);	
	gluSphere(quadric_ak[19], 1.0f, 30, 30);

	//sphere 21
	materialAmbient_ak[0] = 0.0;	
	materialAmbient_ak[1] = 0.05;	
	materialAmbient_ak[2] = 0.0;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.4;	
	materialDiffuse_ak[1] = 0.5;	
	materialDiffuse_ak[2] = 0.4;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.04;	
	materialSpecular_ak[1] = 0.7;	
	materialSpecular_ak[2] = 0.04;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(24.0f, 9.0f, 0.0f);	
	gluSphere(quadric_ak[20], 1.0f, 30, 30);

	//sphere 22
	materialAmbient_ak[0] = 0.05;	
	materialAmbient_ak[1] = 0.0;	
	materialAmbient_ak[2] = 0.0;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.5;	
	materialDiffuse_ak[1] = 0.4;	
	materialDiffuse_ak[2] = 0.4;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.7;	
	materialSpecular_ak[1] = 0.04;	
	materialSpecular_ak[2] = 0.04;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(24.0f, 6.5f, 0.0f);	
	gluSphere(quadric_ak[21], 1.0f, 30, 30);

	//sphere 23
	materialAmbient_ak[0] = 0.05;	
	materialAmbient_ak[1] = 0.05;	
	materialAmbient_ak[2] = 0.05;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.5;	
	materialDiffuse_ak[1] = 0.5;	
	materialDiffuse_ak[2] = 0.5;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.7;	
	materialSpecular_ak[1] = 0.7;	
	materialSpecular_ak[2] = 0.7;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(24.0f, 4.0f, 0.0f);	
	gluSphere(quadric_ak[22], 1.0f, 30, 30);

	//sphere 24
	materialAmbient_ak[0] = 0.05;		
	materialAmbient_ak[1] = 0.05;	
	materialAmbient_ak[2] = 0.0;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.5;	
	materialDiffuse_ak[1] = 0.5;	
	materialDiffuse_ak[2] = 0.4;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.7;	
	materialSpecular_ak[1] = 0.7;	
	materialSpecular_ak[2] = 0.04;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(24.0f, 1.5f, 0.0f);	
	gluSphere(quadric_ak[23], 1.0f, 30, 30);

}

void uninitialize()
{
    GLXContext currentGlxContext;
    currentGlxContext = glXGetCurrentContext();

    if(currentGlxContext != NULL && currentGlxContext == gGlxContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}

    if(gGlxContext)
	{
		glXDestroyContext(gpDisplay, gGlxContext);
	}

    if (gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if (gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if (gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if (gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
      
}
