#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glx.h>	
#include <GL/glu.h>


using namespace std;

GLXContext gGlxContext;
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

bool gbLight_ak= false;
float angle_ak=0.0f;

//from right side----> pyramid color red
GLfloat lightAmbient_one_ak[]={0.0f,0.0f,0.0f,1.0f}; 
GLfloat lightDiffuse_one_ak[]={1.0f,0.0f,0.0f,1.0f};  
GLfloat lightSpecular_one_ak[] = {1.0f,0.0f,0.0f,1.0f };
GLfloat lightPosition_one_ak[]={2.0f,0.0f,0.0f,1.0f}; 

//from left side----> pyramid color blue
GLfloat lightAmbient_two_ak[]={0.0f,0.0f,0.0f,1.0f}; 
GLfloat lightDiffuse_two_ak[]={0.0f,0.0f,1.0f,1.0f};  
GLfloat lightSpecular_two_ak[] = {0.0f,0.0f,1.0f,1.0f };
GLfloat lightPosition_two_ak[]={-2.0f,0.0f,0.0f,1.0f}; 

GLfloat materialAmbient_ak[] = { 0.0f,0.0f,0.0f,1.0f };	
GLfloat materialDiffuse_ak[] = { 1.0f,1.0f,1.0f,1.0f };	
GLfloat materialSpecular_ak[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess_ak= 128.0f;


int main()
{
    void CreateWindow();
    void ToggleFullScreen();
    void uninitialize();

    void initialize();
    void resize(int, int);
    void display();
    void update();

    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

     //message loop
    XEvent event;
    KeySym keysym;

    bool bDone = false;

    CreateWindow();
    initialize();
    while(bDone==false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
        switch (event.type)
        {
        case MapNotify:
            break;
        case KeyPress:
            keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
            switch (keysym)
            {
            case XK_Escape:
                bDone = true;

            case XK_F:
            case XK_f:
                if (bFullscreen == false)
                {
                    ToggleFullScreen();
                    bFullscreen = true;
                }
                else
                {
                    ToggleFullScreen();
                    bFullscreen = false;
                }
                break;
            case XK_L:
            case XK_l:
                if (gbLight_ak == false)
                {
                    gbLight_ak = true;
                    glEnable(GL_LIGHTING);
                }
                else
                {
                    gbLight_ak = false;
                    glDisable(GL_LIGHTING);
                }
                break;
            default:
                break;
            }
            break;
        case ButtonPress:
            switch (event.xbutton.button)
            {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            default:
                break;
            }
        case MotionNotify:
            break;
        case ConfigureNotify:
            winWidth = event.xconfigure.width;
            winHeight = event.xconfigure.height;
            resize(winWidth,winHeight);
            break;
        case Expose:
            break;
        case DestroyNotify:
            break;
        case 33:
            bDone = true;
        default:
            break;
        }
    }
        display();
        update();
    }
    uninitialize();

    return 0;
}

void CreateWindow()
{
    void uninitialize();

    static int frameBufferAttributes[]={GLX_DOUBLEBUFFER, True,
                                        GLX_RGBA,
                                        GLX_RED_SIZE, 8, 
                                        GLX_BLUE_SIZE, 8,
                                        GLX_ALPHA_SIZE, 8,
                                        GLX_DEPTH_SIZE, 24,
                                        None
                                       };
    

    XSetWindowAttributes winAttributes;
    int defaultScreen;
    int styleMask;

    gpDisplay = XOpenDisplay(NULL);
    if (gpDisplay == NULL)
    {
        printf("ERROR: Unable to Open X Display");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);
    gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));

    if (gpXVisualInfo == NULL)
    {
        printf("ERROR: Unable to Allocate Memory for Visual Info");
        uninitialize();
        exit(1);
    }

    gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

    if (gpXVisualInfo == NULL)
    {
        printf("ERROR: Unable to Get a Visual");
        uninitialize();
        exit(1);
    }

    winAttributes.border_pixel = 0;
    winAttributes.background_pixmap = 0;
    winAttributes.colormap = XCreateColormap(gpDisplay,
                                             RootWindow(gpDisplay, gpXVisualInfo->screen),
                                             gpXVisualInfo->visual,
                                             AllocNone);
    gColormap = winAttributes.colormap;

    winAttributes.background_pixel = BlackPixel(gpDisplay, defaultScreen);

    winAttributes.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay, gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,
                            gpXVisualInfo->depth,
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttributes);

    if (!gWindow)
    {
        printf("ERROR: Failed To Create Main Window");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "Pyramid With Two Lights");

    Atom windowMAnagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowMAnagerDelete, 1);
    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen()
{
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void initialize(void)
{
	void resize(int, int);
	void uninitialize(void);

	
	gGlxContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	if(gGlxContext == NULL)
	{
		printf("Failed to create Rendering Context");
		uninitialize();
		exit(1);
	}
	
	glXMakeCurrent(gpDisplay, gWindow, gGlxContext);
    	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    	glShadeModel(GL_SMOOTH);	
	glClearDepth(1.0f);				
	glEnable(GL_DEPTH_TEST);			
	glDepthFunc(GL_LEQUAL);	
	glHint(GL_PERSPECTIVE_CORRECTION_HINT , GL_NICEST);
	
	//lighting for right side
	glLightfv(GL_LIGHT0,GL_AMBIENT,lightAmbient_one_ak);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,lightDiffuse_one_ak);
	glLightfv(GL_LIGHT0,GL_POSITION,lightPosition_one_ak);
	glLightfv(GL_LIGHT0,GL_SPECULAR,lightSpecular_one_ak);
	glEnable(GL_LIGHT0);

	//lighting for left side
	glLightfv(GL_LIGHT1,GL_AMBIENT,lightAmbient_two_ak);
	glLightfv(GL_LIGHT1,GL_DIFFUSE,lightDiffuse_two_ak);
	glLightfv(GL_LIGHT1,GL_POSITION,lightPosition_two_ak);
	glLightfv(GL_LIGHT1,GL_SPECULAR,lightSpecular_two_ak);
	glEnable(GL_LIGHT1);

	//material
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);		
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess_ak);

	resize(giWindowWidth, giWindowHeight);		
} 

void display(void)
{

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

    	glTranslatef(0.0f,0.0f,-5.0f);
	glRotatef(angle_ak,0.0f,1.0f,0.0f);

	glBegin(GL_TRIANGLES);
	// front
	glNormal3f(0.0f, 0.447214f,0.894427f);
	glVertex3f(0.0f,1.0,0.0f);
	glVertex3f(-1.0f,-1.0f,1.0f);
	glVertex3f(1.0f,-1.0f,1.0f);

	//right
	glNormal3f(0.894427f,0.447214f,0.0f);
	glVertex3f(0.0f,1.0f,0.0f);
	glVertex3f(1.0f,-1.0f,1.0f);
	glVertex3f(1.0f,-1.0f,-1.0f);

	//back
	glNormal3f(0.0f,0.447214f,-0.894427f);
	glVertex3f(0.0f,1.0f,0.0f);
	glVertex3f(1.0f,-1.0f,-1.0f);
	glVertex3f(-1.0f,-1.0f,-1.0f);

	//left
	glNormal3f(-0.894427f, 0.447214f, 0.0f);
	glVertex3f(0.0f,1.0f,0.0f);
	glVertex3f(-1.0f,-1.0f,-1.0f);
	glVertex3f(-1.0f,-1.0f,1.0f);
	glEnd();
	
        glXSwapBuffers(gpDisplay, gWindow); 	
}

void resize(int width, int height)
{
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void update()
{
    	angle_ak+=0.1f;

	if(angle_ak>=360)
		angle_ak=0.0f;
}

void uninitialize()
{
    GLXContext currentGlxContext;
    currentGlxContext = glXGetCurrentContext();

    if(currentGlxContext != NULL && currentGlxContext == gGlxContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}

    if(gGlxContext)
	{
		glXDestroyContext(gpDisplay, gGlxContext);
	}

    if (gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if (gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if (gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if (gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
      
}
