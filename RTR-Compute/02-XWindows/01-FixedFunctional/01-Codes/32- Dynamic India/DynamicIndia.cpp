#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <math.h>
#include <pthread.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glx.h>	
#include <GL/glu.h>

#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alut.h>

using namespace std;

GLXContext gGlxContext;
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

float PI = 3.14f;
float cd_1 = 0.0f;
float cd_2 = 0.0f;

int a = 0;
float ab = PI;

float x_1 = -4.5f;
float y_1 = 0.0f;
float x_2 = -4.5f;
float y_2 = -4.5f;

float iaf_x = -4.5f;
float iaf_y = 0.0f;

float x_3 = -6.6f;

float x_4 = 0.0f;
float y_4 = 0.0f;

float x_5 = 0.0f;
float y_5 = 0.0f;

float ab_3 = 1.5 * PI;

ALCdevice *alutDevice;
ALCcontext *alutContext;
ALuint wavBuffer, wavSource;
void* oglMain(void *);
void* alutMain(void *);

int main(void)
{	
    pthread_t alutThread, oglThread;

	pthread_create(&oglThread, NULL, oglMain, NULL);
	pthread_create(&alutThread, NULL, alutMain, NULL);

    pthread_join(oglThread,  NULL);
	pthread_join(alutThread, NULL);
	
    return 0;
}


void* oglMain(void *params) {

    void CreateWindow();
    void ToggleFullScreen();
    void uninitialize();

    void initialize();
    void resize(int, int);
    void display();

    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

     //message loop
    XEvent event;
    KeySym keysym;

    bool bDone = false;

    CreateWindow();
    initialize();
    while(bDone==false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
        switch (event.type)
        {
        case MapNotify:
            break;
        case KeyPress:
            keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
            switch (keysym)
            {
            case XK_Escape:
                bDone = true;

            case XK_F:
            case XK_f:
                if (bFullscreen == false)
                {
                    ToggleFullScreen();
                    bFullscreen = true;
                }
                else
                {
                    ToggleFullScreen();
                    bFullscreen = false;
                }
                break;
            default:
                break;
            }
            break;
        case ButtonPress:
            switch (event.xbutton.button)
            {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            default:
                break;
            }
        case MotionNotify:
            break;
        case ConfigureNotify:
            winWidth = event.xconfigure.width;
            winHeight = event.xconfigure.height;
            resize(winWidth,winHeight);
            break;
        case Expose:
            break;
        case DestroyNotify:
            break;
        case 33:
            bDone = true;
        default:
            break;
        }
    }
        display();
    }
    uninitialize();

    return 0;
}

void CreateWindow()
{
    void uninitialize();

    static int frameBufferAttributes[]={GLX_RGBA,
                                        GLX_RED_SIZE, 1, 
                                        GLX_BLUE_SIZE, 1,
                                        GLX_ALPHA_SIZE, 1, 
                                        None
                                       };
    

    XSetWindowAttributes winAttributes;
    int defaultScreen;
    int styleMask;

    gpDisplay = XOpenDisplay(NULL);
    if (gpDisplay == NULL)
    {
        printf("ERROR: Unable to Open X Display");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);
    gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));

    if (gpXVisualInfo == NULL)
    {
        printf("ERROR: Unable to Allocate Memory for Visual Info");
        uninitialize();
        exit(1);
    }

    gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

    if (gpXVisualInfo == NULL)
    {
        printf("ERROR: Unable to Get a Visual");
        uninitialize();
        exit(1);
    }

    winAttributes.border_pixel = 0;
    winAttributes.background_pixmap = 0;
    winAttributes.colormap = XCreateColormap(gpDisplay,
                                             RootWindow(gpDisplay, gpXVisualInfo->screen),
                                             gpXVisualInfo->visual,
                                             AllocNone);
    gColormap = winAttributes.colormap;

    winAttributes.background_pixel = BlackPixel(gpDisplay, defaultScreen);

    winAttributes.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay, gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,
                            gpXVisualInfo->depth,
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttributes);

    if (!gWindow)
    {
        printf("ERROR: Failed To Create Main Window");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "Dynamic India");

    Atom windowMAnagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowMAnagerDelete, 1);
    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen()
{
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void initialize(void)
{
	void resize(int, int);
	void uninitialize(void);

	
	gGlxContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	if(gGlxContext == NULL)
	{
		printf("Failed to create Rendering Context");
		uninitialize();
		exit(1);
	}
	
	glXMakeCurrent(gpDisplay, gWindow, gGlxContext);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	resize(giWindowWidth, giWindowHeight);		
} 

void display(void)
{
		glTranslatef(0.0f, 0.0f, -1.0f);
	void IAF();

	void firstI();
	void N();
	void secondI();
	void D();
	void A();
	void Indianflag();
	void Draw_Aeroplane();
	void lowerPlane();

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);

	firstI();

	if (a == 1)
		N();
	if (a == 2)
		secondI();
	if (a == 3)
		A();
	if (a == 4)
		D();

	if (a == 6)

	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -4.0f);

		if (ab < 1.5f * PI)
		{
			x_1 = 2.0f * cos(ab) - 5.0f;
			y_1 = 2.0f * sin(ab) + 1.5f;
		}
		glTranslatef(x_1, y_1, -4.0f);
		Draw_Aeroplane();
		IAF();

		ab += 0.0001f;
		x_1 += 0.0001f;

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -4.0f);

		static float ab_1 = PI;
		if (ab_1 > 0.5f * PI)
		{
			x_2 = 2.0f * cos(ab_1) - 5.0f;
			y_2 = 2.0f * sin(ab_1) - 2.5f;
		}
		glTranslatef(x_2, y_2, -4.0f);
		Draw_Aeroplane();
		IAF();

		ab_1 -= 0.0001f;
		x_2 += 0.0001f;

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(x_3, -0.5f, -8.0f);
		Draw_Aeroplane();
		IAF();
		//x_3+=0.001f;
		if (x_3 > 4.5f)
			x_3 += 0.0002f;
		else
			x_3 += 0.0001f;

		if (x_3 > 4.5f)
			a = 5;

		if (a == 5)
			lowerPlane();
	}

	if (x_3 > 3.5)
		Indianflag(); 
    	glFlush();		
}

void IAF()
{
	glLineWidth(2.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-0.2f, 0.15f, 0.0f);
	glVertex3f(-0.2f, -0.15f, 0.0f);
	glVertex3f(0.0f, 0.15f, 0.0f);
	glVertex3f(-0.12f, -0.15f, 0.0f);
	glVertex3f(0.0f, 0.15f, 0.0f);
	glVertex3f(0.12f, -0.15f, 0.0f);
	glVertex3f(-0.07f, 0.0f, 0.0f);
	glVertex3f(0.07f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.15f, 0.0f);
	glVertex3f(0.2f, -0.15f, 0.0f);
	glVertex3f(0.2f, 0.15f, 0.0f);
	glVertex3f(0.35f, 0.15f, 0.0f);
	glVertex3f(0.2f, 0.02f, 0.0f);
	glVertex3f(0.3f, 0.02f, 0.0f);

	glEnd();
}

void lowerPlane()
{
	void Draw_Aeroplane();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.0f, 3.0f, -4.0f);

	if (ab_3 <= (2.0f * PI))
	{
		x_1 = 2.5f * cos(ab_3) + 4.5f;
		y_1 = 2.5f * sin(ab_3) + 2.0f;
	}
	glTranslatef(x_1, y_1, -4.0f);
	//Draw_Aeroplane();
	ab_3 += 0.0001f;
	// x_1+=0.0002f;
	// y_1+=0.0002f;

	static float ab_2 = 0.5f * PI;
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.0f, -3.5f, -4.0f);

	if (ab_2 >= 0.0f)
	{
		x_2 = 2.5f * cos(ab_2) + 4.5f;
		y_2 = 2.5f * sin(ab_2) - 3.0f;
	}
	glTranslatef(x_2, y_2, -4.0f);
	//Draw_Aeroplane();
	ab_2 -= 0.0001f;
	// x_2+=0.0002f;
	// y_2+=0.0002f;
}

void firstI()
{
	void N();

	static float x_f_I_ak = -2.3f;

	glLoadIdentity();
	glTranslatef(x_f_I_ak, 0.1f, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.1f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);

	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.15f, 0.1f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.15f, -0.6f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.05f, -0.6f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.05f, 0.1f, 0.0f);

	glEnd();

	if (x_f_I_ak < -1.3f)
		x_f_I_ak += 0.0001f;

	glLoadIdentity();
	glTranslatef(x_f_I_ak, -0.7f, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.1f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);

	glEnd();

	if (x_f_I_ak < -1.3f)
		x_f_I_ak += 0.0001f;

	else
		a = 1;
}

void secondI()
{

	void A();

	static float y_s_I_ak = -2.3f;

	glLoadIdentity();
	glTranslatef(0.7f, y_s_I_ak, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.1f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);

	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.15f, 0.1f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.15f, -0.6f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.05f, -0.6f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.05f, 0.1f, 0.0f);

	glEnd();

	if (y_s_I_ak < 0.1f)
		y_s_I_ak += 0.0001f;

	else
		a = 3;

	glLoadIdentity();
	glTranslatef(0.7f, y_s_I_ak - 0.8f, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.1f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);

	glEnd();

	if (y_s_I_ak < -0.7f)
		y_s_I_ak += 0.0001f;
}

void D()
{

	glLoadIdentity();
	glTranslatef(-0.3f, -0.7f, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(0.0f, cd_1, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);
	glColor3f(cd_1, cd_1, cd_1);
	glVertex3f(0.0f, 0.1f, 0.0f);
	glColor3f(cd_1, cd_2, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.1f, 1.0f, 0.0f);

	glEnd();

	glLoadIdentity();
	glTranslatef(0.05f, 0.1f, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(cd_1, cd_2, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(-0.25f, 0.2f, 0.0f);
	glVertex3f(-0.25f, 0.1f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);

	glEnd();

	glLoadIdentity();
	glTranslatef(0.05f, -0.7f, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(0.0f, cd_1, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(-0.25f, 0.2f, 0.0f);
	glVertex3f(-0.25f, 0.1f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);

	glEnd();

	glLoadIdentity();
	glTranslatef(0.15f, -0.7f, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(0.0f, cd_1, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(0.0f, 0.2f, 0.0f);
	glColor3f(cd_1, cd_2, 0.0f);
	glVertex3f(0.0f, 0.9f, 0.0f);
	glColor3f(cd_1, cd_1, cd_1);
	glVertex3f(0.1f, 0.9f, 0.0f);

	glEnd();

	if (cd_1 <= 1.0f)
		cd_1 += 0.001f;
	if (cd_2 <= 0.5f)
		cd_2 += 0.001f;

	a = 6;
}

void N()
{

	void secondI();

	static float n_f_ak = 2.3f;

	glLoadIdentity();
	glTranslatef(-1.0f, n_f_ak, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);
	glVertex3f(0.0f, 0.1f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.1f, 1.0f, 0.0f);

	glEnd();

	if (n_f_ak > -0.7f)
		n_f_ak -= 0.0001f;

	glLoadIdentity();
	glTranslatef(-0.6f, n_f_ak, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);
	glVertex3f(0.0f, 0.1f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.1f, 1.0f, 0.0f);

	glEnd();

	if (n_f_ak > -0.7f)
		n_f_ak -= 0.0001f;

	glLoadIdentity();
	glTranslatef(-1.0f, n_f_ak, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.3f, 0.1f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.4f, 0.1f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.1f, 1.0f, 0.0f);

	glEnd();

	if (n_f_ak > -0.7f)
		n_f_ak -= 0.0001f;

	else
		a = 2;
}

void A()
{
	static float a_x_ak = 2.3f;

	glLoadIdentity();
	glTranslatef(a_x_ak, -0.7f, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.3f, 0.1f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.4f, 0.1f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.1f, 1.0f, 0.0f);

	glEnd();

	if (a_x_ak > 1.1f)
		a_x_ak -= 0.0001f;

	else
		a = 4;

	glLoadIdentity();

	glTranslatef(a_x_ak + 0.2f, -0.7f, -3.0f);
	glScalef(-1.0f, 1.0f, 1.0f);

	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.3f, 0.1f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.4f, 0.1f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.1f, 1.0f, 0.0f);

	glEnd();

	if (a_x_ak > 1.3f)
		a_x_ak -= 0.0001f;
}

void Indianflag()
{
	glLoadIdentity();
	glTranslatef(1.9f, -0.43f, -5.0f);

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.1f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);

	glEnd();

	glLoadIdentity();
	glTranslatef(1.9f, -0.51f, -5.0f);

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.1f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);

	glEnd();

	glLoadIdentity();
	glTranslatef(1.9f, -0.59f, -5.0f);

	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.1f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);

	glEnd();
}

void Draw_Aeroplane()
{

	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.4f, 0.1f, 0.0f);
	glVertex3f(-0.75f, 0.1f, 0.0f);
	glVertex3f(-0.75f, -0.1f, 0.0f);
	glVertex3f(-0.4f, -0.1f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.45f, 0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.45f, 0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.45f, -0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.45f, -0.18f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.45f, 0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.45f, -0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.8f, -0.18f, 0.0f);

	glEnd();

	glBegin(GL_TRIANGLES);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.45f, 0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.20f, 0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.45f, 0.45f, 0.0f);

	glEnd();

	glBegin(GL_TRIANGLES);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.25f, -0.13f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.25f, -0.40f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.45f, -0.13f, 0.0f);

	glEnd();
}


void resize(int width, int height)
{
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize()
{
    GLXContext currentGlxContext;
    currentGlxContext = glXGetCurrentContext();

    if(currentGlxContext != NULL && currentGlxContext == gGlxContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}

    if(gGlxContext)
	{
		glXDestroyContext(gpDisplay, gGlxContext);
	}

    if (gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if (gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if (gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if (gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}

void* alutMain(void *params)
{
	alutDevice = alcOpenDevice(NULL);
	alutContext = alcCreateContext(alutDevice, NULL);
    alcMakeContextCurrent(alutContext);

	alGenSources((ALuint)1, &wavSource);
	alGenBuffers(1, &wavBuffer);

    ALenum format;
	ALvoid *data;
	ALsizei size, freq;
	ALboolean loop = AL_FALSE;
	alutLoadWAVFile(
        (ALbyte*)"DulhanChali.wav", 
        &format, 
        &data, 
        &size, 
        &freq, 
        &loop
    );
	alBufferData(wavBuffer, format, data, size, freq);

	alSourcei(wavSource, AL_BUFFER, wavBuffer);
	alSourcePlay(wavSource);
    
    ALint source_state;
	alGetSourcei(wavSource, AL_SOURCE_STATE, &source_state);
	while (source_state == AL_PLAYING) {
		alGetSourcei(wavSource, AL_SOURCE_STATE, &source_state);
	}

	if (wavSource) {
        alDeleteSources(1, &wavSource);
    }

    if (wavBuffer) {
        alDeleteBuffers(1, &wavBuffer);
    }
    
	if (alutContext) {
        alutDevice = alcGetContextsDevice(alutContext);
        alcMakeContextCurrent(NULL);
	    alcDestroyContext(alutContext);
	    alcCloseDevice(alutDevice);
    }

    pthread_exit(NULL);
}
