#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glx.h>	
#include <GL/glu.h>

#include <SOIL/SOIL.h>
#include "teapot.h"

using namespace std;

GLXContext gGlxContext;
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

bool gbLight_ak= false;
bool gbTexture_ak = false;
GLuint texture_teapot_ak;
bool gbAnimate_ak = false;
float angle_ak = 0.0f;

GLfloat lightAmbient_ak[]={0.5f,0.5f,0.5f,1.0f }; 
GLfloat lightDiffuse_ak[]={1.0f,1.0f,1.0f,1.0f}; 
GLfloat lightSpecular_ak[] = {1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition_ak[]={100.0f,100.0f,100.0f,1.0f};

GLfloat materialAmbient_ak[] = { 0.0f,0.0f,0.0f,1.0f };	
GLfloat materialDiffuse_ak[] = { 1.0f,1.0f,1.0f,1.0f };	
GLfloat materialSpecular_ak[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess_ak= 50.0f;

int main()
{
    void CreateWindow();
    void ToggleFullScreen();
    void uninitialize();

    void initialize();
    void resize(int, int);
    void display();
    
    void update();

    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

     //message loop
    XEvent event;
    KeySym keysym;

    bool bDone = false;

    CreateWindow();
    initialize();
    while(bDone==false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
        switch (event.type)
        {
        case MapNotify:
            break;
        case KeyPress:
            keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
            switch (keysym)
            {
            case XK_Escape:
                bDone = true;

            case XK_F:
            case XK_f:
                if (bFullscreen == false)
                {
                    ToggleFullScreen();
                    bFullscreen = true;
                }
                else
                {
                    ToggleFullScreen();
                    bFullscreen = false;
                }
                break;
            case XK_L:
	    case XK_l:
		if(gbLight_ak==false)
		{
			gbLight_ak=true;
			glEnable(GL_LIGHTING);
		}
		else
		{
			gbLight_ak=false;
			glDisable(GL_LIGHTING);
		}
		break;
	    case XK_T:
	    case XK_t:
		if(gbTexture_ak==false)
		{
			gbTexture_ak=true;
			glEnable(GL_TEXTURE_2D);
		}
		else
		{
			gbTexture_ak = false;
			glDisable(GL_TEXTURE_2D);
		}
		break;
	    case XK_A:
	    case XK_a:
		if (gbAnimate_ak==false)
		{
			gbAnimate_ak = true;
		}
		else
		{
			gbAnimate_ak = false;
		}
		break;
            default:
                break;
            }
            break;
        case ButtonPress:
            switch (event.xbutton.button)
            {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            default:
                break;
            }
        case MotionNotify:
            break;
        case ConfigureNotify:
            winWidth = event.xconfigure.width;
            winHeight = event.xconfigure.height;
            resize(winWidth,winHeight);
            break;
        case Expose:
            break;
        case DestroyNotify:
            break;
        case 33:
            bDone = true;
        default:
            break;
        }
    }
        display();
        update();
    }
    uninitialize();

    return 0;
}

void CreateWindow()
{
    void uninitialize();

    static int frameBufferAttributes[]={GLX_DOUBLEBUFFER, True,
                                        GLX_RGBA,
                                        GLX_RED_SIZE, 8, 
                                        GLX_BLUE_SIZE, 8,
                                        GLX_ALPHA_SIZE, 8,
                                        GLX_DEPTH_SIZE, 24,
                                        None
                                       };
    

    XSetWindowAttributes winAttributes;
    int defaultScreen;
    int styleMask;

    gpDisplay = XOpenDisplay(NULL);
    if (gpDisplay == NULL)
    {
        printf("ERROR: Unable to Open X Display");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);
    gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));

    if (gpXVisualInfo == NULL)
    {
        printf("ERROR: Unable to Allocate Memory for Visual Info");
        uninitialize();
        exit(1);
    }

    gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

    if (gpXVisualInfo == NULL)
    {
        printf("ERROR: Unable to Get a Visual");
        uninitialize();
        exit(1);
    }

    winAttributes.border_pixel = 0;
    winAttributes.background_pixmap = 0;
    winAttributes.colormap = XCreateColormap(gpDisplay,
                                             RootWindow(gpDisplay, gpXVisualInfo->screen),
                                             gpXVisualInfo->visual,
                                             AllocNone);
    gColormap = winAttributes.colormap;

    winAttributes.background_pixel = BlackPixel(gpDisplay, defaultScreen);

    winAttributes.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay, gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,
                            gpXVisualInfo->depth,
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttributes);

    if (!gWindow)
    {
        printf("ERROR: Failed To Create Main Window");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "Teapot with Light and Texture");

    Atom windowMAnagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowMAnagerDelete, 1);
    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen()
{
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void initialize(void)
{
	void resize(int, int);
	void uninitialize(void);

    	GLuint loadBitmapAsTexture(const char*);
	
	gGlxContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	if(gGlxContext == NULL)
	{
		printf("Failed to create Rendering Context");
		uninitialize();
		exit(1);
	}
	
	glXMakeCurrent(gpDisplay, gWindow, gGlxContext);
        
        texture_teapot_ak = loadBitmapAsTexture("marble.bmp");

        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    	glShadeModel(GL_SMOOTH);	
	glClearDepth(1.0f);				
	glEnable(GL_DEPTH_TEST);			
	glDepthFunc(GL_LEQUAL);	
	glHint(GL_PERSPECTIVE_CORRECTION_HINT , GL_NICEST);
	
	//lighting
	glLightfv(GL_LIGHT0,GL_AMBIENT,lightAmbient_ak);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,lightDiffuse_ak);
	glLightfv(GL_LIGHT0,GL_SPECULAR,lightSpecular_ak);
	glLightfv(GL_LIGHT0,GL_POSITION,lightPosition_ak);
	glEnable(GL_LIGHT0);

	//material
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);		
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess_ak);
	
	resize(giWindowWidth, giWindowHeight);		
} 

GLuint loadBitmapAsTexture(const char* path)
{
    int width, height;
    unsigned char *imageData = NULL;
    GLuint textureId;

    imageData = SOIL_load_image(path, &width, &height, NULL, SOIL_LOAD_RGB);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 4); //4 --> rgba
    glGenTextures(1, &textureId);
    glBindTexture(GL_TEXTURE_2D, textureId);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    gluBuild2DMipmaps(GL_TEXTURE_2D, 3, width, height, GL_RGB, GL_UNSIGNED_BYTE, (GLvoid *)imageData);	

    SOIL_free_image_data(imageData);

    return textureId;
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

    	glBindTexture(GL_TEXTURE_2D, texture_teapot_ak);
	glTranslatef(0.0f,0.0f,-1.0f);
	glRotatef(angle_ak,0.0f,1.0f,0.0f);
	glBegin(GL_TRIANGLES);

	for(int i_ak = 0; i_ak < sizeof(face_indicies)/sizeof(face_indicies[0]); i_ak++)
	{
		for(int j_ak = 0; j_ak < 3; j_ak++)
		{
			int vi_ak = face_indicies[i_ak][j_ak];
			int ni_ak = face_indicies[i_ak][j_ak + 3];
			int ti_ak = face_indicies[i_ak][j_ak + 6];

			glTexCoord2f(textures[ti_ak][0], textures[ti_ak][1]);
			glNormal3f(normals[ni_ak][0], normals[ni_ak][1], normals[ni_ak][2]);
			glVertex3f(vertices[vi_ak][0], vertices[vi_ak][1], vertices[vi_ak][2]);
		}
	}

	glEnd();    

    	glXSwapBuffers(gpDisplay, gWindow); 	
}

void resize(int width, int height)
{
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void update()
{
	if(gbAnimate_ak)
	{
		angle_ak+=0.1f;
		if(angle_ak >= 360.0f)
			angle_ak = 0.0f;
	}
}

void uninitialize()
{
    GLXContext currentGlxContext;
    currentGlxContext = glXGetCurrentContext();

    if(currentGlxContext != NULL && currentGlxContext == gGlxContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}

    if(gGlxContext)
	{
		glXDestroyContext(gpDisplay, gGlxContext);
	}

    if (gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if (gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if (gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if (gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }

    if(texture_teapot_ak)
    {
        glDeleteTextures(1,&texture_teapot_ak);
        texture_teapot_ak = 0;
    }
}
