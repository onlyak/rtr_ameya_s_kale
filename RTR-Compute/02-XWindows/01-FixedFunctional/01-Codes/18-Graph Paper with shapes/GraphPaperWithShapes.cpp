#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glx.h>	
#include <GL/glu.h>

#include <cmath>


using namespace std;

GLXContext gGlxContext;
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

int main()
{
    void CreateWindow();
    void ToggleFullScreen();
    void uninitialize();

    void initialize();
    void resize(int, int);
    void display();

    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

     //message loop
    XEvent event;
    KeySym keysym;

    bool bDone = false;

    CreateWindow();
    initialize();
    while(bDone==false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
        switch (event.type)
        {
        case MapNotify:
            break;
        case KeyPress:
            keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
            switch (keysym)
            {
            case XK_Escape:
                bDone = true;

            case XK_F:
            case XK_f:
                if (bFullscreen == false)
                {
                    ToggleFullScreen();
                    bFullscreen = true;
                }
                else
                {
                    ToggleFullScreen();
                    bFullscreen = false;
                }
                break;
            default:
                break;
            }
            break;
        case ButtonPress:
            switch (event.xbutton.button)
            {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            default:
                break;
            }
        case MotionNotify:
            break;
        case ConfigureNotify:
            winWidth = event.xconfigure.width;
            winHeight = event.xconfigure.height;
            resize(winWidth,winHeight);
            break;
        case Expose:
            break;
        case DestroyNotify:
            break;
        case 33:
            bDone = true;
        default:
            break;
        }
    }
        display();
    }
    uninitialize();

    return 0;
}

void CreateWindow()
{
    void uninitialize();

    static int frameBufferAttributes[]={GLX_DOUBLEBUFFER, True,
                                        GLX_RGBA,
                                        GLX_RED_SIZE, 8, 
                                        GLX_BLUE_SIZE, 8,
                                        GLX_ALPHA_SIZE, 8,
                                        GLX_DEPTH_SIZE, 24,
                                        None
                                       };
    

    XSetWindowAttributes winAttributes;
    int defaultScreen;
    int styleMask;

    gpDisplay = XOpenDisplay(NULL);
    if (gpDisplay == NULL)
    {
        printf("ERROR: Unable to Open X Display");
        uninitialize();
        exit(1);
    }

    defaultScreen = XDefaultScreen(gpDisplay);
    gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));

    if (gpXVisualInfo == NULL)
    {
        printf("ERROR: Unable to Allocate Memory for Visual Info");
        uninitialize();
        exit(1);
    }

    gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

    if (gpXVisualInfo == NULL)
    {
        printf("ERROR: Unable to Get a Visual");
        uninitialize();
        exit(1);
    }

    winAttributes.border_pixel = 0;
    winAttributes.background_pixmap = 0;
    winAttributes.colormap = XCreateColormap(gpDisplay,
                                             RootWindow(gpDisplay, gpXVisualInfo->screen),
                                             gpXVisualInfo->visual,
                                             AllocNone);
    gColormap = winAttributes.colormap;

    winAttributes.background_pixel = BlackPixel(gpDisplay, defaultScreen);

    winAttributes.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay, gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,
                            gpXVisualInfo->depth,
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttributes);

    if (!gWindow)
    {
        printf("ERROR: Failed To Create Main Window");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "Graph Paper With Shapes");

    Atom windowMAnagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowMAnagerDelete, 1);
    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen()
{
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void initialize(void)
{
	void resize(int, int);
	void uninitialize(void);

	
	gGlxContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	if(gGlxContext == NULL)
	{
		printf("Failed to create Rendering Context");
		uninitialize();
		exit(1);
	}
	
	glXMakeCurrent(gpDisplay, gWindow, gGlxContext);
    	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    	glShadeModel(GL_SMOOTH);	
	glClearDepth(1.0f);				
	glEnable(GL_DEPTH_TEST);			
	glDepthFunc(GL_LEQUAL);	
	glHint(GL_PERSPECTIVE_CORRECTION_HINT , GL_NICEST);

	resize(giWindowWidth, giWindowHeight);		
} 

void display(void)
{
	void DrawCircle(float);
	void Triangle(float);
	void Rectangle(float,float);
	void InCircle(float,float,float,float, float,float,float,float,float);
	void OuterCircle(float,float);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

    	glTranslatef(0.0f, 0.0f, -1.3f);
    
    	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);
    	for (float y_axis_ak = -1.0f; y_axis_ak < 0.0f; y_axis_ak += 0.05f)
	{

		glVertex3f(y_axis_ak, 1.0f, 0.0f);
		glVertex3f(y_axis_ak, -1.0f, 0.0f);
	}

    	for (float y_axis_ak = 1.0f; y_axis_ak > 0.0f; y_axis_ak -= 0.05f)
	{

		glVertex3f(y_axis_ak, 1.0f, 0.0f);
		glVertex3f(y_axis_ak, -1.0f, 0.0f);
	}

	glEnd();

	glBegin(GL_LINES);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);

	glEnd();

    	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);

	for (float x_axis_ak = -1.0f; x_axis_ak < 0.0f; x_axis_ak += 0.05f)
	{

        glVertex3f(-1.0f, x_axis_ak, 0.0f);
		glVertex3f(1.0f, x_axis_ak, 0.0f);
	}

    	for (float x_axis_ak = 1.0f; x_axis_ak > 0.0f; x_axis_ak -= 0.05f)
	{

        glVertex3f(-1.0f, x_axis_ak, 0.0f);
		glVertex3f(1.0f, x_axis_ak, 0.0f);
	}

	glEnd();

	glBegin(GL_LINES);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);

	glEnd();
	
	glTranslatef(0.0f, 0.0f, -1.0f);

	Triangle(0.5f);
	InCircle(0.0f, 0.5f, 0.0f,-0.5f, -0.5f, 0.0f,0.5f, -0.5f, 0.0f);
	Rectangle(1.0f,1.0f);
	OuterCircle(1.0f, 1.0f);

    	glXSwapBuffers(gpDisplay, gWindow); 	
}

void resize(int width, int height)
{
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void Triangle(float value_ak)
{
	float count = -1;
	int noCount = 2000;
	GLfloat angle = 0;
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 0.0f);
	
	glVertex3f(0.0f, value_ak, 0.0f);
	glVertex3f(-value_ak, -value_ak, 0.0f);

	glVertex3f(-value_ak, -value_ak, 0.0f);
	glVertex3f(value_ak, -value_ak, 0.0f);

	glVertex3f(value_ak, -value_ak, 0.0f);
	glVertex3f(0.0f, value_ak, 0.0f);

	glEnd();
}

void InCircle(float x1_ak, float y1_ak, float z1_ak,float x2_ak, float y2_ak, float z2,float x3_ak, float y3_ak, float z3_ak)
{
	float count_ak = 0.0f;
	GLfloat angle_ak = 0.0f;

	float dist_a_b_ak = sqrt((x2_ak - x1_ak)*(x2_ak - x1_ak) + (y2_ak - y1_ak)*(y2_ak - y1_ak) + (z2 - z1_ak)*(z2 - z1_ak));
	float dist_b_c_ak = sqrt((x3_ak - x2_ak)*(x3_ak - x2_ak) + (y3_ak - y2_ak)*(y3_ak - y2_ak) + (z3_ak - z2)*(z3_ak - z2));
	float dist_c_a_ak = sqrt((x1_ak - x3_ak)*(x1_ak - x3_ak) + (y1_ak - y3_ak)*(y1_ak - y3_ak) + (z1_ak - z3_ak)*(z1_ak - z3_ak));

	float semiperimeter_ak = (dist_a_b_ak + dist_b_c_ak + dist_c_a_ak) / 2;
	float radius_ak = sqrt((semiperimeter_ak - dist_a_b_ak)*(semiperimeter_ak - dist_b_c_ak)*(semiperimeter_ak - dist_c_a_ak) / semiperimeter_ak);
	float Ox_ak = (x3_ak * dist_a_b_ak + x1_ak * dist_b_c_ak + x2_ak * dist_c_a_ak) / (semiperimeter_ak * 2);
	float Oy_ak = (y3_ak * dist_a_b_ak + y1_ak * dist_b_c_ak + y2_ak * dist_c_a_ak) / (semiperimeter_ak * 2);
	float Oz_ak = (z3_ak * dist_a_b_ak + z1_ak * dist_b_c_ak + z2 * dist_c_a_ak) / (semiperimeter_ak * 2);

	glBegin(GL_LINE_LOOP);
	glColor3f(1.0f,1.0f,0.0f);
	for (count_ak = 0; count_ak <= 2000; count_ak++) 
	{
		angle_ak = 2 * M_PI*count_ak / 2000;
		glVertex3f(cos(angle_ak)*radius_ak + Ox_ak, sin(angle_ak)*radius_ak + Oy_ak, 0.0f + Oz_ak);
	}

	glEnd();

}

void DrawCircle(float radius_ak) {
	float count_ak = 0.0f;
	GLfloat angle_ak = 0.0f;

	glBegin(GL_LINE_LOOP);
	glColor3f(1.0f,1.0f,0.0f);
	for (count_ak = 0; count_ak <= 2000; count_ak++) 
	{
		angle_ak = 2 * M_PI*count_ak / 2000;
		glVertex3f(cos(angle_ak)*radius_ak , sin(angle_ak)*radius_ak, 0.0f );
	}
	glEnd();
}

void Rectangle(float width_ak, float height_ak) 
{
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 0.0f);

	glVertex3f(width_ak/2, height_ak/2, 0.0f);
	glVertex3f(-width_ak / 2, height_ak / 2, 0.0f);
	glVertex3f(-width_ak / 2, height_ak / 2, 0.0f);
	glVertex3f(-width_ak / 2, -height_ak / 2, 0.0f);
	glVertex3f(-width_ak / 2, -height_ak / 2, 0.0f);
	glVertex3f(width_ak / 2, -height_ak / 2, 0.0f);
	glVertex3f(width_ak / 2, -height_ak / 2, 0.0f);
	glVertex3f(width_ak / 2, height_ak / 2, 0.0f);

	glEnd();
}

void OuterCircle(float width_ak, float height_ak) 
{
	float count_ak = 0.0f;
	GLfloat angle_ak = 0.0f;
	float radius_ak = sqrt(width_ak / 2 * width_ak / 2 + height_ak / 2 * height_ak / 2);

	glBegin(GL_LINE_LOOP);
	glColor3f(1.0f, 1.0f, 0.0f);
	for (count_ak = 0; count_ak <= 2000; count_ak++) {
		angle_ak = 2 * M_PI*count_ak / 2000;
		glVertex3f(cos(angle_ak)*radius_ak, sin(angle_ak)*radius_ak, 0.0f);
	}
	glEnd();
}

void uninitialize()
{
    GLXContext currentGlxContext;
    currentGlxContext = glXGetCurrentContext();

    if(currentGlxContext != NULL && currentGlxContext == gGlxContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}

    if(gGlxContext)
	{
		glXDestroyContext(gpDisplay, gGlxContext);
	}

    if (gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if (gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if (gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if (gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
}
