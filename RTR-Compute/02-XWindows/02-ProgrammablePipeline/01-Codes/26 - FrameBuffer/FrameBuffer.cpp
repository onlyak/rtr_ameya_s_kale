#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/glew.h>

#include <GL/gl.h>
#include <GL/glx.h>	
#include <GL/glu.h>

#include <SOIL/SOIL.h>

#include "vmath.h"

using namespace std;
using namespace vmath;

GLXContext gGlxContext_ak;
bool bFullscreen_ak = false;
Display *gpDisplay_ak = NULL;
XVisualInfo *gpXVisualInfo_ak = NULL;
Colormap gColormap_ak;
Window gWindow_ak;
int giWindowWidth_ak = 800;
int giWindowHeight_ak = 600;

enum
{
	ATTRIBUTE_POSITION = 0,
	ATTRIBUTE_COLOR,
	ATTRIBUTE_NORMAL,
	ATTRIBUTE_TEXCOORD,
};

GLuint gVertexShaderObject_ak;
GLuint gFragmentShaderObject_ak;
GLuint gShaderProgramObject_ak;

GLuint mvpMatrixUniform_ak;

GLuint Vao_pyramid;
GLuint Vbo_position_pyramid;
GLuint Vbo_color_pyramid;

GLuint Vao_cube;
GLuint Vbo_position_cube;
GLuint Vbo_color_cube;

GLuint Vbo_texture_pyramid;
GLuint Vbo_texture_cube;

GLuint textureSamplerUniform;
GLuint textureStone;
GLuint textureKundali;

float pAngle = 0.0f;
float cAngle = 0.0f;

GLuint Fbo;
GLuint textureFbo;

mat4 perspectiveProjectMatrix_ak;

FILE *gpFile_ak;

//PP
typedef GLXContext (*glxCreateContextAttribsARBProc) (Display*, GLXFBConfig, GLXContext, Bool, const int*);
glxCreateContextAttribsARBProc glxCreateContextAttribsARB_ak = NULL;
GLXFBConfig gGLXFBConfig_ak;
//PP

int main()
{
    void CreateWindow();
    void ToggleFullScreen();
    void uninitialize();

    void initialize();
    void resize(int, int);
    void display();
    void update();

    int winWidth = giWindowWidth_ak;
    int winHeight = giWindowHeight_ak;

     //message loop
    XEvent event;
    KeySym keysym;

    bool bDone = false;

    CreateWindow();
    initialize();
    while(bDone==false)
    {
        while(XPending(gpDisplay_ak))
        {
            XNextEvent(gpDisplay_ak, &event);
        switch (event.type)
        {
        case MapNotify:
            break;
        case KeyPress:
            keysym = XkbKeycodeToKeysym(gpDisplay_ak, event.xkey.keycode, 0, 0);
            switch (keysym)
            {
            case XK_Escape:
                bDone = true;

            case XK_F:
            case XK_f:
                if (bFullscreen_ak == false)
                {
                    ToggleFullScreen();
                    bFullscreen_ak = true;
                }
                else
                {
                    ToggleFullScreen();
                    bFullscreen_ak = false;
                }
                break;
            default:
                break;
            }
            break;
        case ButtonPress:
            switch (event.xbutton.button)
            {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            default:
                break;
            }
        case MotionNotify:
            break;
        case ConfigureNotify:
            winWidth = event.xconfigure.width;
            winHeight = event.xconfigure.height;
            resize(winWidth,winHeight);
            break;
        case Expose:
            break;
        case DestroyNotify:
            break;
        case 33:
            bDone = true;
        default:
            break;
        }
    }
        update();
        display();
    }
    uninitialize();

    return 0;
}

void CreateWindow()
{
    void uninitialize();

    //FFP
    // static int frameBufferAttributes[]={GLX_DOUBLEBUFFER, True,
    //                                     GLX_RGBA,
    //                                     GLX_RED_SIZE, 8,
    //                                     GLX_GREEN_SIZE, 8, 
    //                                     GLX_BLUE_SIZE, 8,
    //                                     GLX_ALPHA_SIZE, 8,
    //                                     GLX_DEPTH_SIZE, 24,
    //                                     None
    //                                    };
    
    //PP
    static int frameBufferAttributes[]={
                                        GLX_X_RENDERABLE, True,     
                                        GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
                                        GLX_RENDER_TYPE, GLX_RGBA_BIT,
                                        GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
                                        GLX_DOUBLEBUFFER, True,
                                        GLX_RED_SIZE, 8,
                                        GLX_GREEN_SIZE, 8, 
                                        GLX_BLUE_SIZE, 8,
                                        GLX_ALPHA_SIZE, 8,
                                        GLX_DEPTH_SIZE, 24,
                                        GLX_STENCIL_SIZE, 8,
                                        None
                                       };
    //PP

    XSetWindowAttributes winAttributes;
    int defaultScreen;
    int styleMask;

    //PP
    GLXFBConfig *pGLXFBConfig = NULL;
    GLXFBConfig bestGLXFBConfig; 
    XVisualInfo *pTempXVisualInfo;
    int numFBConfigs = 0;
    //PP

    gpDisplay_ak = XOpenDisplay(NULL);
    if (gpDisplay_ak == NULL)
    {
        printf("ERROR: Unable to Open X Display");
        uninitialize();
        exit(1);
    }

    //FFP
    // defaultScreen = XDefaultScreen(gpDisplay);
    // gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));
    // if (gpXVisualInfo == NULL)
    // {
    //     printf("ERROR: Unable to Allocate Memory for Visual Info");
    //     uninitialize();
    //     exit(1);
    // }
    // gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
    // if (gpXVisualInfo == NULL)
    // {
    //     printf("ERROR: Unable to Get a Visual");
    //     uninitialize();
    //     exit(1);
    // }

    //PP
    pGLXFBConfig = glXChooseFBConfig(gpDisplay_ak, DefaultScreen(gpDisplay_ak), frameBufferAttributes, &numFBConfigs);

    int bestFrameBufferConfig = -1;
    int worstFrameBufferConfig = -1;
    int bestNoOfSamples = -1;
    int worstNoOfSamples = 999;

    for (int i = 0; i < numFBConfigs; i++)
    {
        pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay_ak, pGLXFBConfig[i]);
        if(pTempXVisualInfo)
        {
            int sampleBuffers, samples;
            glXGetFBConfigAttrib(gpDisplay_ak, pGLXFBConfig[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);
            glXGetFBConfigAttrib(gpDisplay_ak, pGLXFBConfig[i], GLX_SAMPLES, &samples);

            if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNoOfSamples)
            {
                bestFrameBufferConfig = i;
                bestNoOfSamples = samples;
            }

            if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNoOfSamples)
            {
                worstFrameBufferConfig = i;
                worstNoOfSamples = samples;
            }
        }
        XFree(pTempXVisualInfo);
    }

    bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
    gGLXFBConfig_ak = bestGLXFBConfig;
    XFree(pGLXFBConfig);
    gpXVisualInfo_ak = glXGetVisualFromFBConfig(gpDisplay_ak,bestGLXFBConfig);
    //PP
    
    winAttributes.border_pixel = 0;
    winAttributes.background_pixmap = 0;
    winAttributes.colormap = XCreateColormap(gpDisplay_ak,
                                             RootWindow(gpDisplay_ak, gpXVisualInfo_ak->screen),
                                             gpXVisualInfo_ak->visual,
                                             AllocNone);
    gColormap_ak = winAttributes.colormap;

    winAttributes.background_pixel = BlackPixel(gpDisplay_ak, defaultScreen);

    winAttributes.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow_ak = XCreateWindow(gpDisplay_ak,
                            RootWindow(gpDisplay_ak, gpXVisualInfo_ak->screen),
                            0,
                            0,
                            giWindowWidth_ak,
                            giWindowHeight_ak,
                            0,
                            gpXVisualInfo_ak->depth,
                            InputOutput,
                            gpXVisualInfo_ak->visual,
                            styleMask,
                            &winAttributes);

    if (!gWindow_ak)
    {
        printf("ERROR: Failed To Create Main Window");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay_ak, gWindow_ak, "PP: Framebuffer");

    Atom windowMAnagerDelete = XInternAtom(gpDisplay_ak, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay_ak, gWindow_ak, &windowMAnagerDelete, 1);
    XMapWindow(gpDisplay_ak, gWindow_ak);
}

void ToggleFullScreen()
{
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    wm_state = XInternAtom(gpDisplay_ak, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow_ak;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen_ak ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay_ak, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay_ak,
               RootWindow(gpDisplay_ak, gpXVisualInfo_ak->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void initialize(void)
{
	void resize(int, int);
	void uninitialize(void);
    GLuint loadBitmapAsTexture(const char*);

    gpFile_ak = fopen("Log.txt", "w");
    if (gpFile_ak)
    {
        fprintf(gpFile_ak, "Log file created successfully \n");
    }
    else
    {
        fprintf(stderr, "Log file can't be created \n exiting \n");
        exit(EXIT_FAILURE);
    }

    glxCreateContextAttribsARB_ak = (glxCreateContextAttribsARBProc) glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");
    const int intAttribs[] = 
    {
        GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
        GLX_CONTEXT_MINOR_VERSION_ARB, 5,
        GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
        0
    };

    gGlxContext_ak = glxCreateContextAttribsARB_ak(gpDisplay_ak,gGLXFBConfig_ak, 0, True, intAttribs);
    if(!gGlxContext_ak)
    {
		int attribs[] = 
		{
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			0
		};
		gGlxContext_ak = glxCreateContextAttribsARB_ak(gpDisplay_ak, gGLXFBConfig_ak, 0, True, attribs);
	}
	
	Bool isDirectContext = glXIsDirect(gpDisplay_ak, gGlxContext_ak);
	if( isDirectContext == True)
	{
        printf("Rendering Context is Direct Hardware Context\n");
	}
	else
	{
        printf("Rendering Context is Direct Software Context\n");
	}
    

    //FFP
	// gGlxContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	// if(gGlxContext == NULL)
	// {
	// 	printf("Failed to create Rendering Context");
	// 	uninitialize();
	// 	exit(1);
	// }
	
	glXMakeCurrent(gpDisplay_ak, gWindow_ak, gGlxContext_ak);

    //PP
    GLenum glew_error = glewInit();
    if(glew_error != GLEW_OK)
	{
		glXDestroyContext(gpDisplay_ak, gGlxContext_ak);
		gGlxContext_ak = NULL;
		
		XCloseDisplay(gpDisplay_ak);
		gpDisplay_ak = NULL;
	}
    //PP

	GLint number_of_extensions;
	glGetIntegerv(GL_NUM_EXTENSIONS, &number_of_extensions);
	
	fprintf(gpFile_ak, "\nOpenGL Vendor : %s \n",glGetString(GL_VENDOR));
	fprintf(gpFile_ak, "\nOpenGL Renderer : %s \n",glGetString(GL_RENDERER));
	fprintf(gpFile_ak, "\nOpenGL Version : %s \n",glGetString(GL_VERSION));
	fprintf(gpFile_ak, "\nOpenGL GLSL Version : %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	GLint numExten_ak;

	glGetIntegerv(GL_NUM_EXTENSIONS,&numExten_ak);
	for (int i=0; i<numExten_ak; i++)
	{
		fprintf(gpFile_ak,"\nOpenGL Enabled Extensions:%s\n",glGetStringi(GL_EXTENSIONS,i));
	}

    gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
	const GLchar *vertexShaderSourceCode = 
	"#version 430 core" \
	"\n" \
	"in vec4 vPosition;" \
	"in vec4 vColor;" \
	"out vec4 out_color;" \
	"in vec2 vTexCoord;" \
	"out vec2 out_texCoord;" \
	"uniform mat4 u_mvpMatrix;" \
	"void main(void)" \
	"{" \
	"gl_Position = u_mvpMatrix * vPosition;" \
	"out_color = vColor;" \
	"out_texCoord = vTexCoord;" \
	"}";
	
	glShaderSource(gVertexShaderObject_ak,1,(const GLchar **)&vertexShaderSourceCode, NULL);

	// compile Vertex Shader
	glCompileShader(gVertexShaderObject_ak);

	// Error checking for Vertex Shader
	GLint infoLogLength = 0; 
	GLint shaderCompiledStatus = 0;
	char *szBuffer = NULL;

	glGetShaderiv(gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if(shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength>0)
		{
			szBuffer = (char *)malloc(infoLogLength);
			if(szBuffer!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_ak, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Vertex Shader Compilation Log: %s\n",szBuffer);
				free(szBuffer);
				szBuffer = NULL;

			}
		}
	}

	//Fragment Shader
	/* out_color is the output of Vertex Shader */
	gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar *vertexFragmentSourceCode = 
	"#version 430" \
	"\n" \
	"in vec4 out_color;" \
	"out vec4 FragColor;" \
	"in vec2 out_texCoord;" \
	"uniform sampler2D u_texture_sampler;" \
	"void main(void)" \
	"{" \
	"FragColor = texture(u_texture_sampler, out_texCoord);" \
	"}";

	glShaderSource(gFragmentShaderObject_ak,1,(const GLchar **)&vertexFragmentSourceCode, NULL);

	// compile Fragment Shader
	glCompileShader(gFragmentShaderObject_ak);

	// Error Checking for Fragment Shader
	glGetShaderiv(gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if(shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength>0)
		{
			szBuffer = (char *)malloc(infoLogLength);
			if(szBuffer!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_ak, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Fragment Shader Compilation Log: %s\n",szBuffer);
				free(szBuffer);
				szBuffer = NULL;

			}
		}
	}

	//Shader Program
	gShaderProgramObject_ak = glCreateProgram();
	glAttachShader(gShaderProgramObject_ak,gVertexShaderObject_ak);
	glAttachShader(gShaderProgramObject_ak,gFragmentShaderObject_ak);

	// Bind the attributes in shader with the enums in your main program
	/* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
	glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");

	// For Texture Attribute
	glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_TEXCOORD, "vTexCoord");

	glLinkProgram(gShaderProgramObject_ak);

	// Linking Error Checking
	GLint shaderProgramLinkStatus = 0;
	szBuffer = NULL;

	glGetProgramiv(gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
	if(shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength>0)
		{
			szBuffer = (char *)malloc(infoLogLength);
			if(szBuffer!=NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_ak, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Shader Program Link Log: %s\n",szBuffer);
				free(szBuffer);
				szBuffer = NULL;

			}
		}
	}

	//Get the information of uniform Post linking
	mvpMatrixUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_mvpMatrix");
	textureSamplerUniform = glGetUniformLocation(gShaderProgramObject_ak, "u_texture_sampler");

	//Pyramid
	//Vertices Array Declaration
	const GLfloat pyramidVertices[]=
	{   
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,

		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,

		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f
	};

	//TexCoord Array

	const GLfloat pyramidTexCoord[]=
	{	
        0.5f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		0.5f, 1.0f,
		1.0f, 0.0f,
		0.0f, 0.0f,
		0.5f, 1.0f,
		1.0f, 0.0f,
		0.0f, 0.0f,
		0.5f, 1.0f,
		1.0f, 0.0f,
		0.0f, 0.0f,
		0.5f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f		
	};

	//Cube
	//Vertices
	const GLfloat cubeVertices[] = 
	{
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		1.0f, 1.0f, -1.0f,													
		-1.0f, 1.0f, -1.0f,		
		-1.0f, -1.0f, -1.0f, 
		1.0f, -1.0f, -1.0f,

		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,

		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,	
	};

	//TexCoord
	const GLfloat cubeTexCoord[] =
	{
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f
	};


	//Pyramid
	//Repeat the below steps of Vbo_position and call them in draw method
	glGenVertexArrays(1, &Vao_pyramid);
	glBindVertexArray(Vao_pyramid);

	// Push the above vertices to vPosition

	//Steps
	/* 1. Tell OpenGl to create one buffer in your VRAM
	      Give me a symbol to identify. It is known as NamedBuffer
	      In OpenGL terminology, it is called as GL_ARRAY_BUFFER. This is becase vertex has plenty of attributes 
		  like color, texture, etc. Also it requires contiguous memory.
		  User identifies this variable as Vbo_position and GPU as GL_ARRAY_BUFFER. 
	   2. Bind with the above symbol. It doesn't unbind until 'unbind step' is performed eg- Railway track
	   3. Insert triangle data into the buffer.
	   4. Specify where to insert this data into shader and also how to use it.
	   5. Enable the 'in' point. 
	   6. Unbind 
	*/
    // Pyramid
	glGenBuffers(1, &Vbo_position_pyramid);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_pyramid);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
	// 3 is specified for 3 pairs for vertices
	/* For Texture, specify 2*/
    // 4th parameter----> Normalized Co-ordinates
	// 5th How many strides to take?
	// 6th From which position	
	glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);  //change tracks to link different attributes

	// Push texture to vTexCoord
	glGenBuffers(1, &Vbo_texture_pyramid);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_texture_pyramid);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidTexCoord), pyramidTexCoord, GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_TEXCOORD);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//Cube
	glGenVertexArrays(1, &Vao_cube);
	glBindVertexArray(Vao_cube);

	//Position
	glGenBuffers(1, &Vbo_position_cube);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_cube);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Texture
	glGenBuffers(1, &Vbo_texture_cube);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_texture_cube);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTexCoord), cubeTexCoord, GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_TEXCOORD);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);


	//Frame Buffer

	GLuint depthBuffer;

	glGenFramebuffers(1, &Fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, Fbo);

	glGenTextures(1, &textureFbo);
	glBindTexture(GL_TEXTURE_2D, textureFbo);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 800, 600, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureFbo, 0);

	glGenRenderbuffers(1, &depthBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);

	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, 800, 600);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);

	glBindTexture(GL_TEXTURE_2D, 0);

	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE)
		fprintf(gpFile_ak, "---------------------------------------------------------------------------.\n");


	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_TEXTURE_2D);
	//loadTexture(&textureStone, MAKEINTRESOURCE(STONE_BITMAP));
	//loadTexture(&textureKundali, MAKEINTRESOURCE(KUNDALI_BITMAP));
	textureStone = loadBitmapAsTexture("Stone.bmp");

	// Set PerspectiveMatrix to identity matrix 
	perspectiveProjectMatrix_ak = mat4::identity();


	resize(giWindowWidth_ak, giWindowHeight_ak);		
} 

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
   	glBindFramebuffer(GL_FRAMEBUFFER, Fbo);

	glClearColor(0.5f, 0.5f, 0.5f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject_ak);
	//OpenGL Draw

	//Set ModelView and ModelViewProjection matrices to identity
	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();
	mat4 rotationMatrix = mat4:: identity();

	modelViewProjectionMatrix = perspectiveProjectMatrix_ak*modelViewMatrix;

	// Translate call
	mat4 translateMatrix = translate(0.0f,0.0f,-6.0f);
	rotationMatrix = rotate(pAngle, 0.0f, 1.0f, 0.0f);
	modelViewMatrix = translateMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectMatrix_ak*modelViewMatrix;

	// After this line, modelViewProjectionMatrix becomes u_mvpMatrix
	glUniformMatrix4fv(mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix);

	//Texture Stone
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureStone);
	glUniform1i(textureSamplerUniform, 0);

	//Bind Vao Pyramid
	glBindVertexArray(Vao_pyramid); //change tracks  //Begin

	glDrawArrays(GL_TRIANGLES, 0, 12);
	
	// Unbind Vao Pyramid
	glBindVertexArray(0);  //end

	glUseProgram(0);

	glBindTexture(GL_TEXTURE_2D, 0);

	//unbind Fbo
	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	glClearColor(0.0f, 0.0f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject_ak);
	// ModelViewMatrix and Load Identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	rotationMatrix = mat4 :: identity();

	mat4 scaleMatrix = mat4:: identity();

	translateMatrix = translate(0.0f, 0.0f, -6.0f);

	//Scale
	scaleMatrix = scale(0.75f, 0.75f, 0.75f);

	rotationMatrix = rotate(cAngle, 1.0f, 0.0f, 0.0f);
	modelViewMatrix = translateMatrix * scaleMatrix * rotationMatrix;

	rotationMatrix = rotate(cAngle, 0.0f, 1.0f, 0.0f);
	modelViewMatrix = modelViewMatrix * rotationMatrix;

	rotationMatrix = rotate(cAngle, 0.0f, 0.0f, 1.0f);
	modelViewMatrix = modelViewMatrix * rotationMatrix;

	modelViewProjectionMatrix = perspectiveProjectMatrix_ak * modelViewMatrix;	
	
	glUniformMatrix4fv(mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix);

	//Texture Stone
	glActiveTexture(GL_TEXTURE0);
	//glBindTexture(GL_TEXTURE_2D, textureKundali);
	glBindTexture(GL_TEXTURE_2D, textureFbo);
	glUniform1i(textureSamplerUniform, 0);

	//Cube Begin
	glBindVertexArray(Vao_cube);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

	glBindVertexArray(0);

	glUseProgram(0);
    
    glXSwapBuffers(gpDisplay_ak, gWindow_ak); 	
}

void resize(int width, int height)
{
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	perspectiveProjectMatrix_ak = perspective(45.0f, ((GLfloat)width/(GLfloat)height),0.1f, 100.0f);

}

void update()
{
    pAngle+= 0.5f;
	if(pAngle >= 360.0f)
		pAngle = 0.0f;
	
	cAngle+= 0.5f;
	if(cAngle >= 360.0f)
		cAngle = 0.0f;
}

GLuint loadBitmapAsTexture(const char* path)
{
    int width, height;
    unsigned char *imageData = NULL;
    GLuint textureId;

    imageData = SOIL_load_image(path, &width, &height, NULL, SOIL_LOAD_RGB);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 4); //4 --> rgba
    glGenTextures(1, &textureId);
    glBindTexture(GL_TEXTURE_2D, textureId);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, imageData);

    glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);

    SOIL_free_image_data(imageData);

    return textureId;
}

void uninitialize()
{
    GLXContext currentGlxContext;
    currentGlxContext = glXGetCurrentContext();

    if (gShaderProgramObject_ak) 
    {
        glUseProgram(gShaderProgramObject_ak);
        GLsizei shaderCount_ak;
        glGetProgramiv(gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

        GLuint* pShaders_ak = NULL;
        pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
        glGetAttachedShaders(gShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);

        for (GLsizei i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
            glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
            glDeleteShader(pShaders_ak[i_ak]);
            pShaders_ak[i_ak] = 0;
        }

        free(pShaders_ak);
        glDeleteProgram(gShaderProgramObject_ak);
        gShaderProgramObject_ak = 0;

        glUseProgram(0);
    }

    if(currentGlxContext != NULL && currentGlxContext == gGlxContext_ak)
	{
		glXMakeCurrent(gpDisplay_ak, 0, 0);
	}

    if(gGlxContext_ak)
	{
		glXDestroyContext(gpDisplay_ak, gGlxContext_ak);
	}

    if (gWindow_ak)
    {
        XDestroyWindow(gpDisplay_ak, gWindow_ak);
    }

    if (gColormap_ak)
    {
        XFreeColormap(gpDisplay_ak, gColormap_ak);
    }

    if (gpXVisualInfo_ak)
    {
        free(gpXVisualInfo_ak);
        gpXVisualInfo_ak = NULL;
    }

    if (gpDisplay_ak)
    {
        XCloseDisplay(gpDisplay_ak);
        gpDisplay_ak = NULL;
    }
	    if(gpFile_ak)
    {
		fprintf(gpFile_ak, "Log file is successfully closed.\n");
		fclose(gpFile_ak);
		gpFile_ak = NULL;
    }
}
