#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/glew.h>

#include <GL/gl.h>
#include <GL/glx.h>	
#include <GL/glu.h>

#include "vmath.h"

using namespace std;
using namespace vmath;

GLXContext gGlxContext;
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

enum
{
	ATTRIBUTE_POSITION = 0,
	ATTRIBUTE_COLOR,
	ATTRIBUTE_NORMAL,
	ATRIBUTE_TEXCOORD,
};

GLuint gVertexShaderObject_ak;
GLuint gFragmentShaderObject_ak;
GLuint gShaderProgramObject_ak;

GLuint mvpMatrixUniform_ak;
GLuint Vao_ak;
GLuint Vbo_position_ak;
GLuint Vbo_color_ak;

mat4 perspectiveProjectMatrix_ak;

FILE *gpFile_ak;

//PP
typedef GLXContext (*glxCreateContextAttribsARBProc) (Display*, GLXFBConfig, GLXContext, Bool, const int*);
glxCreateContextAttribsARBProc glxCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;
//PP

int main()
{
    void CreateWindow();
    void ToggleFullScreen();
    void uninitialize();

    void initialize();
    void resize(int, int);
    void display();

    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

     //message loop
    XEvent event;
    KeySym keysym;

    bool bDone = false;

    CreateWindow();
    initialize();
    while(bDone==false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
        switch (event.type)
        {
        case MapNotify:
            break;
        case KeyPress:
            keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
            switch (keysym)
            {
            case XK_Escape:
                bDone = true;

            case XK_F:
            case XK_f:
                if (bFullscreen == false)
                {
                    ToggleFullScreen();
                    bFullscreen = true;
                }
                else
                {
                    ToggleFullScreen();
                    bFullscreen = false;
                }
                break;
            default:
                break;
            }
            break;
        case ButtonPress:
            switch (event.xbutton.button)
            {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            default:
                break;
            }
        case MotionNotify:
            break;
        case ConfigureNotify:
            winWidth = event.xconfigure.width;
            winHeight = event.xconfigure.height;
            resize(winWidth,winHeight);
            break;
        case Expose:
            break;
        case DestroyNotify:
            break;
        case 33:
            bDone = true;
        default:
            break;
        }
    }
        display();
    }
    uninitialize();

    return 0;
}

void CreateWindow()
{
    void uninitialize();

    //FFP
    // static int frameBufferAttributes[]={GLX_DOUBLEBUFFER, True,
    //                                     GLX_RGBA,
    //                                     GLX_RED_SIZE, 8,
    //                                     GLX_GREEN_SIZE, 8, 
    //                                     GLX_BLUE_SIZE, 8,
    //                                     GLX_ALPHA_SIZE, 8,
    //                                     GLX_DEPTH_SIZE, 24,
    //                                     None
    //                                    };
    
    //PP
    static int frameBufferAttributes[]={
                                        GLX_X_RENDERABLE, True,     
                                        GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
                                        GLX_RENDER_TYPE, GLX_RGBA_BIT,
                                        GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
                                        GLX_DOUBLEBUFFER, True,
                                        GLX_RED_SIZE, 8,
                                        GLX_GREEN_SIZE, 8, 
                                        GLX_BLUE_SIZE, 8,
                                        GLX_ALPHA_SIZE, 8,
                                        GLX_DEPTH_SIZE, 24,
                                        GLX_STENCIL_SIZE, 8,
                                        None
                                       };
    //PP

    XSetWindowAttributes winAttributes;
    int defaultScreen;
    int styleMask;

    //PP
    GLXFBConfig *pGLXFBConfig = NULL;
    GLXFBConfig bestGLXFBConfig; 
    XVisualInfo *pTempXVisualInfo;
    int numFBConfigs = 0;
    //PP

    gpDisplay = XOpenDisplay(NULL);
    if (gpDisplay == NULL)
    {
        printf("ERROR: Unable to Open X Display");
        uninitialize();
        exit(1);
    }

    //FFP
    // defaultScreen = XDefaultScreen(gpDisplay);
    // gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));
    // if (gpXVisualInfo == NULL)
    // {
    //     printf("ERROR: Unable to Allocate Memory for Visual Info");
    //     uninitialize();
    //     exit(1);
    // }
    // gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
    // if (gpXVisualInfo == NULL)
    // {
    //     printf("ERROR: Unable to Get a Visual");
    //     uninitialize();
    //     exit(1);
    // }

    //PP
    pGLXFBConfig = glXChooseFBConfig(gpDisplay, DefaultScreen(gpDisplay), frameBufferAttributes, &numFBConfigs);

    int bestFrameBufferConfig = -1;
    int worstFrameBufferConfig = -1;
    int bestNoOfSamples = -1;
    int worstNoOfSamples = 999;

    for (int i = 0; i < numFBConfigs; i++)
    {
        pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfig[i]);
        if(pTempXVisualInfo)
        {
            int sampleBuffers, samples;
            glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);
            glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLES, &samples);

            if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNoOfSamples)
            {
                bestFrameBufferConfig = i;
                bestNoOfSamples = samples;
            }

            if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNoOfSamples)
            {
                worstFrameBufferConfig = i;
                worstNoOfSamples = samples;
            }
        }
        XFree(pTempXVisualInfo);
    }

    bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
    gGLXFBConfig = bestGLXFBConfig;
    XFree(pGLXFBConfig);
    gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);
    //PP
    
    winAttributes.border_pixel = 0;
    winAttributes.background_pixmap = 0;
    winAttributes.colormap = XCreateColormap(gpDisplay,
                                             RootWindow(gpDisplay, gpXVisualInfo->screen),
                                             gpXVisualInfo->visual,
                                             AllocNone);
    gColormap = winAttributes.colormap;

    winAttributes.background_pixel = BlackPixel(gpDisplay, defaultScreen);

    winAttributes.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay, gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,
                            gpXVisualInfo->depth,
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttributes);

    if (!gWindow)
    {
        printf("ERROR: Failed To Create Main Window");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "PP: MultiColored Rectangle");

    Atom windowMAnagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowMAnagerDelete, 1);
    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen()
{
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void initialize(void)
{
	void resize(int, int);
	void uninitialize(void);

    gpFile_ak = fopen("Log.txt", "w");
    if (gpFile_ak)
    {
        fprintf(gpFile_ak, "Log file created successfully \n");
    }
    else
    {
        fprintf(stderr, "Log file can't be created \n exiting \n");
        exit(EXIT_FAILURE);
    }

    glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc) glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");
    const int intAttribs[] = 
    {
        GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
        GLX_CONTEXT_MINOR_VERSION_ARB, 5,
        GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
        0
    };

    gGlxContext = glxCreateContextAttribsARB(gpDisplay,gGLXFBConfig, 0, True, intAttribs);
    if(!gGlxContext)
    {
		int attribs[] = 
		{
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			0
		};
		gGlxContext = glxCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, attribs);
	}
	
	Bool isDirectContext = glXIsDirect(gpDisplay, gGlxContext);
	if( isDirectContext == True)
	{
        printf("Rendering Context is Direct Hardware Context\n");
	}
	else
	{
        printf("Rendering Context is Direct Software Context\n");
	}
    

    //FFP
	// gGlxContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	// if(gGlxContext == NULL)
	// {
	// 	printf("Failed to create Rendering Context");
	// 	uninitialize();
	// 	exit(1);
	// }
	
	glXMakeCurrent(gpDisplay, gWindow, gGlxContext);

    //PP
    GLenum glew_error = glewInit();
    if(glew_error != GLEW_OK)
	{
		glXDestroyContext(gpDisplay, gGlxContext);
		gGlxContext = NULL;
		
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
    //PP

    GLint number_of_extensions;
	glGetIntegerv(GL_NUM_EXTENSIONS, &number_of_extensions);
	
	fprintf(gpFile_ak, "\nOpenGL Vendor : %s \n",glGetString(GL_VENDOR));
	fprintf(gpFile_ak, "\nOpenGL Renderer : %s \n",glGetString(GL_RENDERER));
	fprintf(gpFile_ak, "\nOpenGL Version : %s \n",glGetString(GL_VERSION));
	fprintf(gpFile_ak, "\nOpenGL GLSL Version : %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	GLint numExten_ak;

	glGetIntegerv(GL_NUM_EXTENSIONS,&numExten_ak);
	for (int i=0; i<numExten_ak; i++)
	{
		fprintf(gpFile_ak,"\nOpenGL Enabled Extensions:%s\n",glGetStringi(GL_EXTENSIONS,i));
	}

    gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
	const GLchar *vertexShaderSourceCode = 
	"#version 430 core" \
	"\n" \
	"in vec4 vPosition;" \
	"in vec4 vColor;" \
	"out vec4 out_color;" \
	"uniform mat4 u_mvpMatrix;" \
	"void main(void)" \
	"{" \
	"gl_Position = u_mvpMatrix * vPosition;" \
	"out_color = vColor;" \
	"}";
	
	glShaderSource(gVertexShaderObject_ak,1,(const GLchar **)&vertexShaderSourceCode, NULL);

	// compile Vertex Shader
	glCompileShader(gVertexShaderObject_ak);

	// Error checking for Vertex Shader
	GLint infoLogLength = 0; 
	GLint shaderCompiledStatus = 0;
	char *szBuffer = NULL;

	glGetShaderiv(gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if(shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength>0)
		{
			szBuffer = (char *)malloc(infoLogLength);
			if(szBuffer!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_ak, infoLogLength, &written, szBuffer);
			}
		}
	}

	//Fragment Shader
	/* out_color is the output of Vertex Shader */
	gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar *vertexFragmentSourceCode = 
	"#version 430" \
	"\n" \
	"in vec4 out_color;" \
	"out vec4 FragColor;" \
	"void main(void)" \
	"{" \
	"FragColor = out_color;" \
	"}";

	glShaderSource(gFragmentShaderObject_ak,1,(const GLchar **)&vertexFragmentSourceCode, NULL);

	// compile Fragment Shader
	glCompileShader(gFragmentShaderObject_ak);

	// Error Checking for Fragment Shader
	glGetShaderiv(gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if(shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength>0)
		{
			szBuffer = (char *)malloc(infoLogLength);
			if(szBuffer!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_ak, infoLogLength, &written, szBuffer);
			}
		}
	}

	//Shader Program
	gShaderProgramObject_ak = glCreateProgram();
	glAttachShader(gShaderProgramObject_ak,gVertexShaderObject_ak);
	glAttachShader(gShaderProgramObject_ak,gFragmentShaderObject_ak);

	// Bind the attributes in shader with the enums in your main program
	/* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
	glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");

	// For Color Attribute
	glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_COLOR, "vColor");

	glLinkProgram(gShaderProgramObject_ak);

	// Linking Error Checking
	GLint shaderProgramLinkStatus = 0;
	szBuffer = NULL;

	glGetProgramiv(gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
	if(shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength>0)
		{
			szBuffer = (char *)malloc(infoLogLength);
			if(szBuffer!=NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_ak, infoLogLength, &written, szBuffer);
			}
		}
	}

	//Get the information of uniform Post linking
	mvpMatrixUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_mvpMatrix");

	//Vertices Array Declaration
	const GLfloat triangleVertices[]=
	{   	
		1.0f, 1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f
	};

	//Color Array

	const GLfloat triangleColors[]=
	{	
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f
	};

	//Repeat the below steps of Vbo_position and call them in draw method
	glGenVertexArrays(1, &Vao_ak);
	glBindVertexArray(Vao_ak);

	// Push the above vertices to vPosition

	//Steps
	/* 1. Tell OpenGl to create one buffer in your VRAM
	      Give me a symbol to identify. It is known as NamedBuffer
	      In OpenGL terminology, it is called as GL_ARRAY_BUFFER. This is becase vertex has plenty of attributes 
		  like color, texture, etc. Also it requires contiguous memory.
		  User identifies this variable as Vbo_position and GPU as GL_ARRAY_BUFFER. 
	   2. Bind with the above symbol. It doesn't unbind until 'unbind step' is performed eg- Railway track
	   3. Insert triangle data into the buffer.
	   4. Specify where to insert this data into shader and also how to use it.
	   5. Enable the 'in' point. 
	   6. Unbind 
	*/
	glGenBuffers(1, &Vbo_position_ak);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_ak);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
	// 3 is specified for 3 pairs for triangle vertices
	/* For Texture, specify 2*/
    // 4th parameter----> Normalized Co-ordinates
	// 5th How many strides to take?
	// 6th From which position	
	glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);  //change tracks to link different attributes

	// Push color to vColor

	glGenBuffers(1, &Vbo_color_ak);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_color_ak);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleColors), triangleColors, GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);


    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glShadeModel(GL_SMOOTH);	
	glClearDepth(1.0f);				
	glEnable(GL_DEPTH_TEST);			
	glDepthFunc(GL_LEQUAL);	
	glHint(GL_PERSPECTIVE_CORRECTION_HINT , GL_NICEST);

	perspectiveProjectMatrix_ak = mat4::identity();

	resize(giWindowWidth, giWindowHeight);		
} 

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(gShaderProgramObject_ak);
	
	//OpenGL Draw

	//Set ModelView and ModelViewProjection matrices to identity
	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();

	modelViewProjectionMatrix = perspectiveProjectMatrix_ak*modelViewMatrix;

	// Translate call
	mat4 translateMatrix = translate(0.0f,0.0f,-3.0f);
	modelViewMatrix = translateMatrix;
	modelViewProjectionMatrix = perspectiveProjectMatrix_ak*modelViewMatrix;

	// After this line, modelViewProjectionMatrix becomes u_mvpMatrix
	glUniformMatrix4fv(mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix);

	//Bind Vao
	glBindVertexArray(Vao_ak); //change tracks  //Begin

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	// Unbind Vao
	glBindVertexArray(0);  //end

	glUseProgram(0);    
    
    glXSwapBuffers(gpDisplay, gWindow); 	
}

void resize(int width, int height)
{
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    perspectiveProjectMatrix_ak = perspective(45.0f, ((GLfloat)width/(GLfloat)height),0.1f, 100.0f);

}

void uninitialize()
{
    GLXContext currentGlxContext;
    currentGlxContext = glXGetCurrentContext();

    if(Vao_ak)
	{
		glDeleteVertexArrays(1, &Vao_ak);
		Vao_ak = 0;
	}

	if(Vbo_position_ak)
	{
		glDeleteVertexArrays(1, &Vbo_position_ak);
		Vbo_position_ak = 0;
	}

	if(Vbo_color_ak)
	{
		glDeleteVertexArrays(1, &Vbo_color_ak);
		Vbo_color_ak = 0;
	}

    if(currentGlxContext != NULL && currentGlxContext == gGlxContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}

    if(gGlxContext)
	{
		glXDestroyContext(gpDisplay, gGlxContext);
	}

    if (gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if (gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if (gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if (gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
        if(gpFile_ak)
    {
		fprintf(gpFile_ak, "Log file is successfully closed.\n");
		fclose(gpFile_ak);
		gpFile_ak = NULL;
    }
}
