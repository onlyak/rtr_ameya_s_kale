#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/glew.h>

#include <GL/gl.h>
#include <GL/glx.h>	
#include <GL/glu.h>

#include "vmath.h"

#include "Sphere.h"

using namespace std;
using namespace vmath;

FILE *gpFile_ak;

GLXContext gGlxContext;
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

enum
{
	ATTRIBUTE_POSITION = 0,
	ATTRIBUTE_COLOR,
	ATTRIBUTE_NORMAL,
	ATRIBUTE_TEXCOORD,
};

//Per Vertex
GLuint gVertexShaderObject_ak_pv;
GLuint gFragmentShaderObject_ak_pv;

//Per Fragment
GLuint gVertexShaderObject_ak_pf;
GLuint gFragmentShaderObject_ak_pf;

GLuint gShaderProgramObject_ak_pv;
GLuint gShaderProgramObject_ak_pf;

GLuint Vao_sphere;
GLuint Vbo_position_sphere;
GLuint Vbo_element_sphere;
GLuint Vbo_normal_sphere;

struct Light
{
	GLfloat lightAmbient_ak[3];
	GLfloat lightDiffuse_ak[3];
	GLfloat lightSpecular_ak[3];
	GLfloat lightPosition_ak[4];
};
struct Light lights[3];

GLfloat materialAmbient_ak[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat materialDiffuse_ak[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular_ak[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess_ak = 50.0f;

//Uniforms
/********** 3 Uniforms**************/
GLuint ldUniform;
GLuint kdUniform;
GLuint lightPositionUniform;

/************ 9 Uniforms******************/
GLuint laUniform; // light component of ambient light
GLuint kaUniform; 

GLuint lsUniform; 
GLuint ksUniform;

GLuint shininessUniform;  //single float value

GLuint modelUniform;
GLuint viewUniform;
GLuint projectionUniform;

GLuint lKeyPressedUniform;
/*****************************/

bool bLight;

mat4 perspectiveProjectMatrix;

bool isLkey = false;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint numVertices;
GLuint numElements;

int pvpf = 1;
void commonUniformLocation(GLuint);

float angle_ak = 0.0f;

//PP
typedef GLXContext (*glxCreateContextAttribsARBProc) (Display*, GLXFBConfig, GLXContext, Bool, const int*);
glxCreateContextAttribsARBProc glxCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;
//PP

int main()
{
    void CreateWindow();
    void ToggleFullScreen();
    void uninitialize();

    void initialize();
    void resize(int, int);
    void display();
    void update();

    void commonUniformLocation(GLuint);


    int winWidth = giWindowWidth;
    int winHeight = giWindowHeight;

     //message loop
    XEvent event;
    KeySym keysym;

    bool bDone = false;

    CreateWindow();
    initialize();
    while(bDone==false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
        switch (event.type)
        {
        case MapNotify:
            break;
        case KeyPress:
            keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
            switch (keysym)
            {
            case XK_Escape:
                bDone = true;

            case XK_F11:
                if (bFullscreen == false)
                {
                    ToggleFullScreen();
                    bFullscreen = true;
                }
                else
                {
                    ToggleFullScreen();
                    bFullscreen = false;
                }
                break;
            case XK_F:
            case XK_f:
                commonUniformLocation(gShaderProgramObject_ak_pf);
                pvpf = 0;
                break;
		    case XK_V:
		    case XK_v:
                commonUniformLocation(gShaderProgramObject_ak_pv);
                pvpf = 1;
                break;
		    case XK_L:
		    case XK_l:
			if (isLkey == false)
			{
				bLight = true;
				isLkey = true;
			}
			else
			{
				bLight = false;
				isLkey = false;
			}
			break;
            default:
                break;
            }
            break;
        case ButtonPress:
            switch (event.xbutton.button)
            {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            default:
                break;
            }
        case MotionNotify:
            break;
        case ConfigureNotify:
            winWidth = event.xconfigure.width;
            winHeight = event.xconfigure.height;
            resize(winWidth,winHeight);
            break;
        case Expose:
            break;
        case DestroyNotify:
            break;
        case 33:
            bDone = true;
        default:
            break;
        }
    }
        update();
        display();
    }
    uninitialize();

    return 0;
}

void CreateWindow()
{
    void uninitialize();

    //FFP
    // static int frameBufferAttributes[]={GLX_DOUBLEBUFFER, True,
    //                                     GLX_RGBA,
    //                                     GLX_RED_SIZE, 8,
    //                                     GLX_GREEN_SIZE, 8, 
    //                                     GLX_BLUE_SIZE, 8,
    //                                     GLX_ALPHA_SIZE, 8,
    //                                     GLX_DEPTH_SIZE, 24,
    //                                     None
    //                                    };
    
    //PP
    static int frameBufferAttributes[]={
                                        GLX_X_RENDERABLE, True,     
                                        GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
                                        GLX_RENDER_TYPE, GLX_RGBA_BIT,
                                        GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
                                        GLX_DOUBLEBUFFER, True,
                                        GLX_RGBA,
                                        GLX_RED_SIZE, 8,
                                        GLX_GREEN_SIZE, 8, 
                                        GLX_BLUE_SIZE, 8,
                                        GLX_ALPHA_SIZE, 8,
                                        GLX_DEPTH_SIZE, 24,
                                        GLX_STENCIL_SIZE, 8,
                                        GLX_DOUBLEBUFFER, True,
                                        None
                                       };
    //PP

    XSetWindowAttributes winAttributes;
    int defaultScreen;
    int styleMask;

    //PP
    GLXFBConfig *pGLXFBConfig = NULL;
    GLXFBConfig bestGLXFBConfig; 
    XVisualInfo *pTempXVisualInfo;
    int numFBConfigs = 0;
    //PP

    gpDisplay = XOpenDisplay(NULL);
    if (gpDisplay == NULL)
    {
        printf("ERROR: Unable to Open X Display");
        uninitialize();
        exit(1);
    }

    //FFP
    // defaultScreen = XDefaultScreen(gpDisplay);
    // gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));
    // if (gpXVisualInfo == NULL)
    // {
    //     printf("ERROR: Unable to Allocate Memory for Visual Info");
    //     uninitialize();
    //     exit(1);
    // }
    // gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
    // if (gpXVisualInfo == NULL)
    // {
    //     printf("ERROR: Unable to Get a Visual");
    //     uninitialize();
    //     exit(1);
    // }

    //PP
    pGLXFBConfig = glXChooseFBConfig(gpDisplay, DefaultScreen(gpDisplay), frameBufferAttributes, &numFBConfigs);

    int bestFrameBufferConfig = -1;
    int worstFrameBufferConfig = -1;
    int bestNoOfSamples = -1;
    int worstNoOfSamples = 999;

    for (int i = 0; i < numFBConfigs; i++)
    {
        pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfig[i]);
        if(pTempXVisualInfo)
        {
            int sampleBuffers, samples;
            glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);
            glXGetFBConfigAttrib(gpDisplay, pGLXFBConfig[i], GLX_SAMPLES, &samples);

            if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNoOfSamples)
            {
                bestFrameBufferConfig = i;
                bestNoOfSamples = samples;
            }

            if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNoOfSamples)
            {
                worstFrameBufferConfig = i;
                worstNoOfSamples = samples;
            }
        }
        XFree(pTempXVisualInfo);
    }

    bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
    gGLXFBConfig = bestGLXFBConfig;
    XFree(pGLXFBConfig);
    gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);
    //PP
    
    winAttributes.border_pixel = 0;
    winAttributes.background_pixmap = 0;
    winAttributes.colormap = XCreateColormap(gpDisplay,
                                             RootWindow(gpDisplay, gpXVisualInfo->screen),
                                             gpXVisualInfo->visual,
                                             AllocNone);
    gColormap = winAttributes.colormap;

    winAttributes.background_pixel = BlackPixel(gpDisplay, defaultScreen);

    winAttributes.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow = XCreateWindow(gpDisplay,
                            RootWindow(gpDisplay, gpXVisualInfo->screen),
                            0,
                            0,
                            giWindowWidth,
                            giWindowHeight,
                            0,
                            gpXVisualInfo->depth,
                            InputOutput,
                            gpXVisualInfo->visual,
                            styleMask,
                            &winAttributes);

    if (!gWindow)
    {
        printf("ERROR: Failed To Create Main Window");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay, gWindow, "PP: 3D Plain Cube");

    Atom windowMAnagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay, gWindow, &windowMAnagerDelete, 1);
    XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen()
{
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay,
               RootWindow(gpDisplay, gpXVisualInfo->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void initialize(void)
{
	void resize(int, int);
	void uninitialize(void);

    gpFile_ak = fopen("Log.txt", "w");
    if (gpFile_ak)
    {
        fprintf(gpFile_ak, "Log file created successfully \n");
    }
    else
    {
        fprintf(stderr, "Log file can't be created \n exiting \n");
        exit(EXIT_FAILURE);
    }

    glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc) glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");
    const int intAttribs[] = 
    {
        GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
        GLX_CONTEXT_MINOR_VERSION_ARB, 5,
        GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
        0
    };

    gGlxContext = glxCreateContextAttribsARB(gpDisplay,gGLXFBConfig, 0, True, intAttribs);
    if(!gGlxContext)
    {
		int attribs[] = 
		{
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			0
		};
		gGlxContext = glxCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, attribs);
	}
	
	Bool isDirectContext = glXIsDirect(gpDisplay, gGlxContext);
	if( isDirectContext == True)
	{
        printf("Rendering Context is Direct Hardware Context\n");
	}
	else
	{
        printf("Rendering Context is Direct Software Context\n");
	}
    

    //FFP
	// gGlxContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	// if(gGlxContext == NULL)
	// {
	// 	printf("Failed to create Rendering Context");
	// 	uninitialize();
	// 	exit(1);
	// }
	
	glXMakeCurrent(gpDisplay, gWindow, gGlxContext);

    //PP
    GLenum glew_error = glewInit();
    if(glew_error != GLEW_OK)
	{
		glXDestroyContext(gpDisplay, gGlxContext);
		gGlxContext = NULL;
		
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
    //PP

	GLint number_of_extensions;
	glGetIntegerv(GL_NUM_EXTENSIONS, &number_of_extensions);
	
	fprintf(gpFile_ak, "\nOpenGL Vendor : %s \n",glGetString(GL_VENDOR));
	fprintf(gpFile_ak, "\nOpenGL Renderer : %s \n",glGetString(GL_RENDERER));
	fprintf(gpFile_ak, "\nOpenGL Version : %s \n",glGetString(GL_VERSION));
	fprintf(gpFile_ak, "\nOpenGL GLSL Version : %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	GLint numExten_ak;

	glGetIntegerv(GL_NUM_EXTENSIONS,&numExten_ak);
	for (int i=0; i<numExten_ak; i++)
	{
		fprintf(gpFile_ak,"\nOpenGL Enabled Extensions:%s\n",glGetStringi(GL_EXTENSIONS,i));
	}

    //Shader attributes for sphere
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	numVertices = getNumberOfSphereVertices();
	numElements = getNumberOfSphereElements();

	//Per Vertex Shader
	//********** Vertex Shader runs per vertex **********
	/* 1. When we specify core, we tell OpenGL to use Programmable Pipeline. i.e Core Profile
	   2. in/out are glsl language specifier. Known to shader language only.
	      in---> loads incoming data from main program. It loads data only once.
	   3. vec4--> [x,y,z,w]
	   4. uniform---> load incoming data from main program. It loads data multiple times
	   5. gl_Position---> in-built variable of Vertex Shader
	*/
	// out_color is sent as an input to Fragment Shader

	/*******Per Vertex Shader start***********/
	//Per Vertex Vertex Shader
	gVertexShaderObject_ak_pv = glCreateShader(GL_VERTEX_SHADER);
	const GLchar *vertexShaderSourceCode_pv = 
	"#version 430 core" \
	"\n" \
	"in vec4 vPosition;" \
	"in vec3 vNormal;"  \
	"uniform mat4 u_model_matrix;" \
	"uniform mat4 u_projection_matrix;" \
	"uniform mat4 u_view_matrix;" \
	"uniform int u_LKeyPressed;" \
	"uniform vec3 u_Ld[3];" \
	"uniform vec3 u_Kd;" \
	"uniform vec4 u_light_position[3];" \
	"uniform vec3 u_La[3];" \
	"uniform vec3 u_Ls[3];" \
	"uniform vec3 u_Ka;" \
	"uniform vec3 u_Ks;" \
	"uniform float u_Shininess;" \
	"out vec3 phong_ads_light;" \
	"void main(void)" \
	"{" \
		"if (u_LKeyPressed == 1) " \
		"{" \
			"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
			"vec3 transformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
			"vec3 view_vector = normalize(-eyeCoordinates.xyz);" \
			"phong_ads_light = vec3(0.0,0.0,0.0);"
			"vec3 light_direction[3];"
			"vec3 reflection_vector[3];"
			"vec3 ambient[3];"
			"vec3 diffuse[3];"
			"vec3 specular[3];"
			"for(int i =0; i<3 ; i++)"
			"{"
			"light_direction[i] = normalize(vec3(u_light_position[i] - eyeCoordinates));" \
		    "reflection_vector[i] = reflect(-light_direction[i], transformed_normal);" \
			"ambient[i] = u_La[i] * u_Ka;" \
			"diffuse[i] = u_Ld[i] * u_Kd * max(dot(light_direction[i],transformed_normal),0.0);" \
		    "specular[i] = u_Ls[i] * u_Ks * pow(max(dot(reflection_vector[i],view_vector),0.0),u_Shininess);" \
			"phong_ads_light = phong_ads_light + ambient[i] + diffuse[i] + specular[i];" \
			"}"
		"}" \
		"else" \
		"{" \
		    "phong_ads_light = vec3(1.0f,1.0f,1.0f);" \
		"}"
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
	"}";
	
	glShaderSource(gVertexShaderObject_ak_pv,1,(const GLchar **)&vertexShaderSourceCode_pv, NULL);

	// compile Vertex Shader
	glCompileShader(gVertexShaderObject_ak_pv);

	// Error checking for Vertex Shader
	GLint infoLogLength = 0; 
	GLint shaderCompiledStatus = 0;
	char *szBuffer = NULL;

	glGetShaderiv(gVertexShaderObject_ak_pv, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if(shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_ak_pv, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength>0)
		{
			szBuffer = (char *)malloc(infoLogLength);
			if(szBuffer!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_ak_pv, infoLogLength, &written, szBuffer);
			}
		}
	}

	//Per Vertex Fragment Shader
	/* out_color is the output of Vertex Shader */
	gFragmentShaderObject_ak_pv = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* vertexFragmentSourceCode_pv =
	"#version 430 core" \
	"\n" \
	"in vec3 phong_ads_light;" \
	"out vec4 FragColor;" \
	"void main(void)" \
	"{" \
		"FragColor = vec4(phong_ads_light, 1.0f);"  \
	"}";

	glShaderSource(gFragmentShaderObject_ak_pv,1,(const GLchar **)&vertexFragmentSourceCode_pv, NULL);

	// compile Fragment Shader
	glCompileShader(gFragmentShaderObject_ak_pv);

	// Error Checking for Fragment Shader
	glGetShaderiv(gFragmentShaderObject_ak_pv, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if(shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_ak_pv, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength>0)
		{
			szBuffer = (char *)malloc(infoLogLength);
			if(szBuffer!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_ak_pv, infoLogLength, &written, szBuffer);
			}
		}
	}

	//Shader Program
	gShaderProgramObject_ak_pv = glCreateProgram();
	glAttachShader(gShaderProgramObject_ak_pv,gVertexShaderObject_ak_pv);
	glAttachShader(gShaderProgramObject_ak_pv,gFragmentShaderObject_ak_pv);

	// Bind the attributes in shader with the enums in your main program
	/* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
	glBindAttribLocation(gShaderProgramObject_ak_pv, ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject_ak_pv, ATTRIBUTE_NORMAL, "vNormal");
	
	glLinkProgram(gShaderProgramObject_ak_pv);

	// Linking Error Checking
	GLint shaderProgramLinkStatus = 0;
	szBuffer = NULL;

	glGetProgramiv(gShaderProgramObject_ak_pv, GL_LINK_STATUS, &shaderProgramLinkStatus);
	if (shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_ak_pv, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			szBuffer = (char*)malloc(infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_ak_pv, infoLogLength, &written, szBuffer);
			}
		}
	}

	if(pvpf == 1)
		commonUniformLocation(gShaderProgramObject_ak_pv);


	/*******Per Vertex Shader End***********/

	/************Per Fragment Shader Start********************************/
	gVertexShaderObject_ak_pf = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* vertexShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;"  \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform int u_LKeyPressed;" \
		"uniform vec4 u_light_position[3];" \
		"out vec3 transformed_normal;" \
		"out vec3 light_direction[3];" \
		"out vec3 view_vector;" \
		"void main(void)" \
		"{" \
		"if (u_LKeyPressed == 1) " \
		"{" \
		"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normal = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"view_vector = -eyeCoordinates.xyz;" \
		"for(int i = 0; i<3; i++)"
			"{"
				"light_direction[i] = vec3(u_light_position[i] - eyeCoordinates);" \
			"}"
		"}" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject_ak_pf, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile Vertex Shader
	glCompileShader(gVertexShaderObject_ak_pf);

	// Error checking for Vertex Shader
	infoLogLength = 0;
	shaderCompiledStatus = 0;
	szBuffer = NULL;

	glGetShaderiv(gVertexShaderObject_ak_pf, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if (shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_ak_pf, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			szBuffer = (char*)malloc(infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_ak_pf, infoLogLength, &written, szBuffer);

			}
		}
	}

	//Fragment Shader
	/* out_color is the output of Vertex Shader */
	gFragmentShaderObject_ak_pf = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* vertexFragmentSourceCode =
		"#version 430 core" \
		"\n" \
		"out vec4 FragColor;" \
		"in vec3 transformed_normal;" \
		"in vec3 light_direction[3];" \
		"in vec3 view_vector;" \
		"uniform vec3 u_Ld[3];" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_La[3];" \
		"uniform vec3 u_Ls[3];" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_Shininess;" \
		"uniform int u_LKeyPressed;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color;" \
		"if(u_LKeyPressed == 1)" \
		"{" \
		"vec3 norm_transformed_normal = normalize(transformed_normal);" \
		"vec3 norm_view_vector = normalize(view_vector);" \
		"vec3 norm_light_direction[3];"
		"vec3 reflection_vector[3];"
		"vec3 ambient[3];"
		"vec3 diffuse[3];"
		"vec3 specular[3];"
		"for(int i=0;i<3;i++)"
		"{"
		"norm_light_direction[i] = normalize(light_direction[i]);" \
		"reflection_vector[i] = reflect(-norm_light_direction[i], norm_transformed_normal);" \
		"ambient[i] = u_La[i] * u_Ka;" \
		"diffuse[i] = u_Ld[i] * u_Kd * max(dot(norm_light_direction[i],norm_transformed_normal),0.0);" \
		"specular[i] = u_Ls[i] * u_Ks * pow(max(dot(reflection_vector[i],norm_view_vector),0.0),u_Shininess);" \
		"phong_ads_color = phong_ads_color + ambient[i] + diffuse[i] + specular[i];" \
		"}"
		"}" \
		"else" \
		"{" \
		"phong_ads_color = vec3(1.0f,1.0f,1.0f);" \
		"}" \

		"FragColor = vec4(phong_ads_color, 1.0f);" \
		"}";

	glShaderSource(gFragmentShaderObject_ak_pf, 1, (const GLchar**)&vertexFragmentSourceCode, NULL);

	// compile Fragment Shader
	glCompileShader(gFragmentShaderObject_ak_pf);

	infoLogLength = 0;
	shaderCompiledStatus = 0;
	szBuffer = NULL;

	// Error Checking for Fragment Shader
	glGetShaderiv(gFragmentShaderObject_ak_pf, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if (shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_ak_pf, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			szBuffer = (char*)malloc(infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_ak_pf, infoLogLength, &written, szBuffer);

			}
		}
	}

	//Shader Program
	gShaderProgramObject_ak_pf = glCreateProgram();
	glAttachShader(gShaderProgramObject_ak_pf, gVertexShaderObject_ak_pf);
	glAttachShader(gShaderProgramObject_ak_pf, gFragmentShaderObject_ak_pf);

	// Bind the attributes in shader with the enums in your main program
	/* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
	glBindAttribLocation(gShaderProgramObject_ak_pf, ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject_ak_pf, ATTRIBUTE_NORMAL, "vNormal");

	glLinkProgram(gShaderProgramObject_ak_pf);

	// Linking Error Checking
	shaderProgramLinkStatus = 0;
	szBuffer = NULL;

	glGetProgramiv(gShaderProgramObject_ak_pf, GL_LINK_STATUS, &shaderProgramLinkStatus);
	if (shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_ak_pf, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			szBuffer = (char*)malloc(infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_ak_pf, infoLogLength, &written, szBuffer);
			}
		}
	}

	if (pvpf == 0)
		commonUniformLocation(gShaderProgramObject_ak_pf);

	/*******************Per Fragment Shader Ends**************************/

	

	// Push the above vertices to vPosition

	//Steps
	/* 1. Tell OpenGl to create one buffer in your VRAM
	      Give me a symbol to identify. It is known as NamedBuffer
	      In OpenGL terminology, it is called as GL_ARRAY_BUFFER. This is becase vertex has plenty of attributes 
		  like color, texture, etc. Also it requires contiguous memory.
		  User identifies this variable as Vbo_position and GPU as GL_ARRAY_BUFFER. 
	   2. Bind with the above symbol. It doesn't unbind until 'unbind step' is performed eg- Railway track
	   3. Insert triangle data into the buffer.
	   4. Specify where to insert this data into shader and also how to use it.
	   5. Enable the 'in' point. 
	   6. Unbind 
	*/

	//Sphere
	
	// Sphere Vao
	glGenVertexArrays(1, &Vao_sphere);
	glBindVertexArray(Vao_sphere);

	// Sphere position Vbo
	glGenBuffers(1, &Vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	// Sphere element Vbo
	glGenBuffers(1, &Vbo_element_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Vbo_element_sphere);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// Sphere normals Vbo
	glGenBuffers(1, &Vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_normal_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_NORMAL);

	// unbind vao
	glBindVertexArray(0);

	lights[0].lightAmbient_ak[0] = 0.0f;
	lights[0].lightAmbient_ak[1] = 0.0f;
	lights[0].lightAmbient_ak[2] = 0.0f;

	lights[1].lightAmbient_ak[0] = 0.0f;
	lights[1].lightAmbient_ak[1] = 0.0f;
	lights[1].lightAmbient_ak[2] = 0.0f;

	lights[2].lightAmbient_ak[0] = 0.0f;
	lights[2].lightAmbient_ak[1] = 0.0f;
	lights[2].lightAmbient_ak[2] = 0.0f;

	lights[0].lightDiffuse_ak[0] = 1.0f;
	lights[0].lightDiffuse_ak[1] = 0.0f;
	lights[0].lightDiffuse_ak[2] = 0.0f;

	lights[1].lightDiffuse_ak[0] = 0.0f;
	lights[1].lightDiffuse_ak[1] = 1.0f;
	lights[1].lightDiffuse_ak[2] = 0.0f;

	lights[2].lightDiffuse_ak[0] = 0.0f;
	lights[2].lightDiffuse_ak[1] = 0.0f;
	lights[2].lightDiffuse_ak[2] = 1.0f;

	lights[0].lightSpecular_ak[0] = 1.0f;
	lights[0].lightSpecular_ak[1] = 0.0f;
	lights[0].lightSpecular_ak[2] = 0.0f;

	lights[1].lightSpecular_ak[0] = 0.0f;
	lights[1].lightSpecular_ak[1] = 1.0f;
	lights[1].lightSpecular_ak[2] = 0.0f;

	lights[2].lightSpecular_ak[0] = 0.0f;
	lights[2].lightSpecular_ak[1] = 0.0f;
	lights[2].lightSpecular_ak[2] = 1.0f;

	lights[0].lightPosition_ak[0] = 0.0f;
	lights[0].lightPosition_ak[1] = 0.0f;
	lights[0].lightPosition_ak[2] = 0.0f;
	lights[0].lightPosition_ak[3] = 1.0f;

	lights[1].lightPosition_ak[0] = 0.0f;
	lights[1].lightPosition_ak[1] = 0.0f;
	lights[1].lightPosition_ak[2] = 0.0f;
	lights[1].lightPosition_ak[3] = 1.0f;

	lights[2].lightPosition_ak[0] = 0.0f;
	lights[2].lightPosition_ak[1] = 0.0f;
	lights[2].lightPosition_ak[2] = 0.0f;
	lights[2].lightPosition_ak[3] = 1.0f;
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glShadeModel(GL_SMOOTH);	
	glClearDepth(1.0f);				
	glEnable(GL_DEPTH_TEST);			
	glDepthFunc(GL_LEQUAL);	
	glHint(GL_PERSPECTIVE_CORRECTION_HINT , GL_NICEST);

    perspectiveProjectMatrix = mat4::identity();

	resize(giWindowWidth, giWindowHeight);		
} 

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
	GLfloat lightAmbient[9] = {
	lights[0].lightAmbient_ak[0],
	lights[0].lightAmbient_ak[1],
	lights[0].lightAmbient_ak[2],

	lights[1].lightAmbient_ak[0],
	lights[1].lightAmbient_ak[1],
	lights[1].lightAmbient_ak[2],

	lights[2].lightAmbient_ak[0],
	lights[2].lightAmbient_ak[1],
	lights[2].lightAmbient_ak[2]

	};

	GLfloat lightDiffuse[9] = {

	lights[0].lightDiffuse_ak[0],
	lights[0].lightDiffuse_ak[1],
	lights[0].lightDiffuse_ak[2],

	lights[1].lightDiffuse_ak[0],
	lights[1].lightDiffuse_ak[1],
	lights[1].lightDiffuse_ak[2],

	lights[2].lightDiffuse_ak[0],
	lights[2].lightDiffuse_ak[1],
	lights[2].lightDiffuse_ak[2]

	};

	GLfloat lightSpecular[9] = {

	lights[0].lightSpecular_ak[0],
	lights[0].lightSpecular_ak[1],
	lights[0].lightSpecular_ak[2],

	lights[1].lightSpecular_ak[0],
	lights[1].lightSpecular_ak[1],
	lights[1].lightSpecular_ak[2],


	lights[2].lightSpecular_ak[0],
	lights[2].lightSpecular_ak[1],
	lights[2].lightSpecular_ak[2]

	};

	GLfloat lightPosition[12] = {
		lights[0].lightPosition_ak[0],
		lights[0].lightPosition_ak[1],
		lights[0].lightPosition_ak[2],
		lights[0].lightPosition_ak[3],

		lights[1].lightPosition_ak[0],
		lights[1].lightPosition_ak[1],
		lights[1].lightPosition_ak[2],
		lights[1].lightPosition_ak[3],

		lights[2].lightPosition_ak[0],
		lights[2].lightPosition_ak[1],
		lights[2].lightPosition_ak[2],
		lights[2].lightPosition_ak[3]
	};


	if(pvpf==1)
		glUseProgram(gShaderProgramObject_ak_pv);
	else
		glUseProgram(gShaderProgramObject_ak_pf);


	if (bLight == true)
	{
		glUniform1i(lKeyPressedUniform, 1);
		glUniform3fv(laUniform,3, lightAmbient);
		glUniform3fv(ldUniform,3, lightDiffuse);
		glUniform3fv(lsUniform,3, lightSpecular);
		glUniform4fv(lightPositionUniform, 3, lightPosition);

		glUniform3fv(kaUniform, 1, materialAmbient_ak);
		glUniform3fv(kdUniform,1, materialDiffuse_ak);
		glUniform3fv(ksUniform,1, materialSpecular_ak);
		glUniform1f(shininessUniform, materialShininess_ak);

	}
	else
		glUniform1i(lKeyPressedUniform, 0);


	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 translationMatrix = mat4::identity();

	translationMatrix = translate(0.0f, 0.0f, -4.0f);
	modelMatrix = translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, perspectiveProjectMatrix);
	
	//Sphere Begin
	glBindVertexArray(Vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
	
	glBindVertexArray(0); // Sphere end

	glUseProgram(0);
    glXSwapBuffers(gpDisplay, gWindow); 	
}

void resize(int width, int height)
{
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	perspectiveProjectMatrix = perspective(45.0f, ((GLfloat)width/(GLfloat)height),0.1f, 100.0f);

}

void update()
{
    angle_ak += 0.5f;
	if (angle_ak > 360)
		angle_ak = 0.0f;

	lights[0].lightPosition_ak[1] = 10 * sin(angle_ak);
	lights[0].lightPosition_ak[2] = 10 * cos(angle_ak);

	lights[1].lightPosition_ak[0] = 10 * sin(angle_ak);
	lights[1].lightPosition_ak[2] = 10 * cos(angle_ak);

	lights[2].lightPosition_ak[0] = 10 * sin(angle_ak);
	lights[2].lightPosition_ak[1] = 10 * cos(angle_ak);
}

void uninitialize()
{
    GLXContext currentGlxContext;
    currentGlxContext = glXGetCurrentContext();
	
	if(Vao_sphere)
	{
		glDeleteVertexArrays(1, &Vao_sphere);
		Vao_sphere = 0;
	}

	if(Vbo_position_sphere)
	{
		glDeleteVertexArrays(1, &Vbo_position_sphere);
		Vbo_position_sphere = 0;
	}

	if(Vbo_element_sphere)
	{
		glDeleteVertexArrays(1, &Vbo_element_sphere);
		Vbo_element_sphere = 0;
	}

	if (Vbo_normal_sphere)
	{
		glDeleteVertexArrays(1, &Vbo_normal_sphere);
		Vbo_normal_sphere = 0;
	}


	// glDetachShader(gShaderProgramObject_ak, gVertexShaderObject_ak);
	// glDetachShader(gShaderProgramObject_ak, gFragmentShaderObject_ak);

	// glDeleteShader(gVertexShaderObject_ak);
	// gVertexShaderObject_ak = 0;

	// glDeleteShader(gFragmentShaderObject_ak);
	// gFragmentShaderObject_ak = 0;

	// glUseProgram(0);

	// Safe Shader Release
	if (gShaderProgramObject_ak_pv) {
        glUseProgram(gShaderProgramObject_ak_pv);
        GLsizei shaderCount_ak;
        glGetProgramiv(gShaderProgramObject_ak_pv, GL_ATTACHED_SHADERS, &shaderCount_ak);

        GLuint* pShaders_ak = NULL;
        pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
        glGetAttachedShaders(gShaderProgramObject_ak_pv, shaderCount_ak, &shaderCount_ak, pShaders_ak);

        for (GLsizei i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
            glDetachShader(gShaderProgramObject_ak_pv, pShaders_ak[i_ak]);
            glDeleteShader(pShaders_ak[i_ak]);
            pShaders_ak[i_ak] = 0;
        }

        free(pShaders_ak);
        glDeleteProgram(gShaderProgramObject_ak_pv);
        gShaderProgramObject_ak_pv = 0;

        glUseProgram(0);
    }

    if(currentGlxContext != NULL && currentGlxContext == gGlxContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}

    if(gGlxContext)
	{
		glXDestroyContext(gpDisplay, gGlxContext);
	}

    if (gWindow)
    {
        XDestroyWindow(gpDisplay, gWindow);
    }

    if (gColormap)
    {
        XFreeColormap(gpDisplay, gColormap);
    }

    if (gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo = NULL;
    }

    if (gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay = NULL;
    }
	    if(gpFile_ak)
    {
		fprintf(gpFile_ak, "Log file is successfully closed.\n");
		fclose(gpFile_ak);
		gpFile_ak = NULL;
    }
	    if(gpFile_ak)
    {
		fprintf(gpFile_ak, "Log file is successfully closed.\n");
		fclose(gpFile_ak);
		gpFile_ak = NULL;
    }
	    if(gpFile_ak)
    {
		fprintf(gpFile_ak, "Log file is successfully closed.\n");
		fclose(gpFile_ak);
		gpFile_ak = NULL;
    }
}

void commonUniformLocation(GLuint shaderObject)
{
	modelUniform = glGetUniformLocation(shaderObject, "u_model_matrix");
	viewUniform = glGetUniformLocation(shaderObject, "u_view_matrix");
	projectionUniform = glGetUniformLocation(shaderObject, "u_projection_matrix");

	ldUniform = glGetUniformLocation(shaderObject, "u_Ld");
	kdUniform = glGetUniformLocation(shaderObject, "u_Kd");
	lightPositionUniform = glGetUniformLocation(shaderObject, "u_light_position");

	laUniform = glGetUniformLocation(shaderObject, "u_La");
	kaUniform = glGetUniformLocation(shaderObject, "u_Ka");

	lsUniform = glGetUniformLocation(shaderObject, "u_Ls");
	ksUniform = glGetUniformLocation(shaderObject, "u_Ks");

	shininessUniform = glGetUniformLocation(shaderObject, "u_Shininess");

	lKeyPressedUniform = glGetUniformLocation(shaderObject, "u_LKeyPressed");
}
