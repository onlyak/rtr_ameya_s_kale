#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/glew.h>

#include <GL/gl.h>
#include <GL/glx.h>	
#include <GL/glu.h>

#include "vmath.h"

using namespace std;
using namespace vmath;

GLXContext gGlxContext_ak;
bool bFullscreen_ak = false;
Display *gpDisplay_ak = NULL;
XVisualInfo *gpXVisualInfo_ak = NULL;
Colormap gColormap_ak;
Window gWindow_ak;
int giWindowWidth_ak = 800;
int giWindowHeight_ak = 600;

enum
{
	ATTRIBUTE_POSITION = 0,
	ATTRIBUTE_COLOR,
	ATTRIBUTE_NORMAL,
	ATRIBUTE_TEXCOORD,
};

GLuint gVertexShaderObject_ak;
GLuint gFragmentShaderObject_ak;
GLuint gShaderProgramObject_ak;

GLuint vao_cube_ak;
GLuint vbo_ak;
GLuint vbo_normal_cube_ak;

GLuint modelMatrixUniform_ak;
GLuint viewMatrixUniform_ak;
GLuint projectionMatrixUniform_ak;

GLuint laUniform_ak;
GLuint ldUniform_ak;
GLuint lsUniform_ak;
GLuint lightPositionUniform_ak;

GLuint kaUniform_ak;
GLuint kdUniform_ak;
GLuint ksUniform_ak;
GLuint materialShininessUniform_ak;
GLuint textureSamplerUniform_ak;
GLuint lKeyPressedUniform_ak;

bool bAnimate_ak = true;
bool bLight_ak = false;

GLfloat lightAmbient_ak[] = {0.1f, 0.1f, 0.1f};
GLfloat lightDiffused_ak[] = {1.0f, 1.0f, 1.0f};
GLfloat lightPosition_ak[] = {100.0f, 100.0f, 100.0f, 1.0f};
GLfloat lightSpecular_ak[] = {1.0f, 1.0f, 1.0f};

GLfloat materialAmbient_ak[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat materialDiffused_ak[] = {0.5f, 0.2f, 0.7f, 1.0f};
GLfloat materialSpecular_ak[] = {0.7f, 0.7f, 0.7f, 1.0f};
GLfloat materialShininess_ak = 50.0f;

GLuint stone_texture_ak;

bool bAnimate_ak = true;
bool bLight_ak = false;

GLfloat lightAmbient_ak[] = {0.1f, 0.1f, 0.1f};
GLfloat lightDiffused_ak[] = {1.0f, 1.0f, 1.0f};
GLfloat lightPosition_ak[] = {100.0f, 100.0f, 100.0f, 1.0f};
GLfloat lightSpecular_ak[] = {1.0f, 1.0f, 1.0f};

GLfloat materialAmbient_ak[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat materialDiffused_ak[] = {0.5f, 0.2f, 0.7f, 1.0f};
GLfloat materialSpecular_ak[] = {0.7f, 0.7f, 0.7f, 1.0f};
GLfloat materialShininess_ak = 50.0f;

GLuint stone_texture_ak;

GLfloat cubeAngle_ak = 0.0f;

mat4 perspectiveProjectMatrix_ak;

FILE *gpFile_ak;

//PP
typedef GLXContext (*glxCreateContextAttribsARBProc) (Display*, GLXFBConfig, GLXContext, Bool, const int*);
glxCreateContextAttribsARBProc glxCreateContextAttribsARB_ak = NULL;
GLXFBConfig gGLXFBConfig_ak;
//PP

int main()
{
    void CreateWindow();
    void ToggleFullScreen();
    void uninitialize();

    void initialize();
    void resize(int, int);
    void display();
    void update();

    int winWidth = giWindowWidth_ak;
    int winHeight = giWindowHeight_ak;

     //message loop
    XEvent event;
    KeySym keysym;

    bool bDone = false;

    CreateWindow();
    initialize();
    while(bDone==false)
    {
        while(XPending(gpDisplay_ak))
        {
            XNextEvent(gpDisplay_ak, &event);
        switch (event.type)
        {
        case MapNotify:
            break;
        case KeyPress:
            keysym = XkbKeycodeToKeysym(gpDisplay_ak, event.xkey.keycode, 0, 0);
            switch (keysym)
            {
            case XK_Escape:
                bDone = true;

            case XK_F:
            case XK_f:
                if (bFullscreen_ak == false)
                {
                    ToggleFullScreen();
                    bFullscreen_ak = true;
                }
                else
                {
                    ToggleFullScreen();
                    bFullscreen_ak = false;
                }
                break;
                case XK_L:
                case XK_l: {
                    if (bLight_ak) {
                        bLight_ak = false;
                    } else {
                        bLight_ak = true;
                    }
                    break;
                }
                case XK_A:
                case XK_a: {
                    if (bAnimate_ak) {
                        bAnimate_ak = false;
                    } else {
                        bAnimate_ak = true;
                    }
                    break;
                }
            default:
                break;
            }
            break;
        case ButtonPress:
            switch (event.xbutton.button)
            {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            default:
                break;
            }
        case MotionNotify:
            break;
        case ConfigureNotify:
            winWidth = event.xconfigure.width;
            winHeight = event.xconfigure.height;
            resize(winWidth,winHeight);
            break;
        case Expose:
            break;
        case DestroyNotify:
            break;
        case 33:
            bDone = true;
        default:
            break;
        }
    }
        update();
        display();
    }
    uninitialize();

    return 0;
}

void CreateWindow()
{
    void uninitialize();

    //FFP
    // static int frameBufferAttributes[]={GLX_DOUBLEBUFFER, True,
    //                                     GLX_RGBA,
    //                                     GLX_RED_SIZE, 8,
    //                                     GLX_GREEN_SIZE, 8, 
    //                                     GLX_BLUE_SIZE, 8,
    //                                     GLX_ALPHA_SIZE, 8,
    //                                     GLX_DEPTH_SIZE, 24,
    //                                     None
    //                                    };
    
    //PP
    static int frameBufferAttributes[]={
                                        GLX_X_RENDERABLE, True,     
                                        GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
                                        GLX_RENDER_TYPE, GLX_RGBA_BIT,
                                        GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
                                        GLX_DOUBLEBUFFER, True,
                                        GLX_RED_SIZE, 8,
                                        GLX_GREEN_SIZE, 8, 
                                        GLX_BLUE_SIZE, 8,
                                        GLX_ALPHA_SIZE, 8,
                                        GLX_DEPTH_SIZE, 24,
                                        GLX_STENCIL_SIZE, 8,
                                        None
                                       };
    //PP

    XSetWindowAttributes winAttributes;
    int defaultScreen;
    int styleMask;

    //PP
    GLXFBConfig *pGLXFBConfig = NULL;
    GLXFBConfig bestGLXFBConfig; 
    XVisualInfo *pTempXVisualInfo;
    int numFBConfigs = 0;
    //PP

    gpDisplay_ak = XOpenDisplay(NULL);
    if (gpDisplay_ak == NULL)
    {
        printf("ERROR: Unable to Open X Display");
        uninitialize();
        exit(1);
    }

    //FFP
    // defaultScreen = XDefaultScreen(gpDisplay);
    // gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));
    // if (gpXVisualInfo == NULL)
    // {
    //     printf("ERROR: Unable to Allocate Memory for Visual Info");
    //     uninitialize();
    //     exit(1);
    // }
    // gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
    // if (gpXVisualInfo == NULL)
    // {
    //     printf("ERROR: Unable to Get a Visual");
    //     uninitialize();
    //     exit(1);
    // }

    //PP
    pGLXFBConfig = glXChooseFBConfig(gpDisplay_ak, DefaultScreen(gpDisplay_ak), frameBufferAttributes, &numFBConfigs);

    int bestFrameBufferConfig = -1;
    int worstFrameBufferConfig = -1;
    int bestNoOfSamples = -1;
    int worstNoOfSamples = 999;

    for (int i = 0; i < numFBConfigs; i++)
    {
        pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay_ak, pGLXFBConfig[i]);
        if(pTempXVisualInfo)
        {
            int sampleBuffers, samples;
            glXGetFBConfigAttrib(gpDisplay_ak, pGLXFBConfig[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);
            glXGetFBConfigAttrib(gpDisplay_ak, pGLXFBConfig[i], GLX_SAMPLES, &samples);

            if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNoOfSamples)
            {
                bestFrameBufferConfig = i;
                bestNoOfSamples = samples;
            }

            if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNoOfSamples)
            {
                worstFrameBufferConfig = i;
                worstNoOfSamples = samples;
            }
        }
        XFree(pTempXVisualInfo);
    }

    bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
    gGLXFBConfig_ak = bestGLXFBConfig;
    XFree(pGLXFBConfig);
    gpXVisualInfo_ak = glXGetVisualFromFBConfig(gpDisplay_ak,bestGLXFBConfig);
    //PP
    
    winAttributes.border_pixel = 0;
    winAttributes.background_pixmap = 0;
    winAttributes.colormap = XCreateColormap(gpDisplay_ak,
                                             RootWindow(gpDisplay_ak, gpXVisualInfo_ak->screen),
                                             gpXVisualInfo_ak->visual,
                                             AllocNone);
    gColormap_ak = winAttributes.colormap;

    winAttributes.background_pixel = BlackPixel(gpDisplay_ak, defaultScreen);

    winAttributes.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow_ak = XCreateWindow(gpDisplay_ak,
                            RootWindow(gpDisplay_ak, gpXVisualInfo_ak->screen),
                            0,
                            0,
                            giWindowWidth_ak,
                            giWindowHeight_ak,
                            0,
                            gpXVisualInfo_ak->depth,
                            InputOutput,
                            gpXVisualInfo_ak->visual,
                            styleMask,
                            &winAttributes);

    if (!gWindow_ak)
    {
        printf("ERROR: Failed To Create Main Window");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay_ak, gWindow_ak, "Interleaved");

    Atom windowMAnagerDelete = XInternAtom(gpDisplay_ak, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay_ak, gWindow_ak, &windowMAnagerDelete, 1);
    XMapWindow(gpDisplay_ak, gWindow_ak);
}

void ToggleFullScreen()
{
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    wm_state = XInternAtom(gpDisplay_ak, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow_ak;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen_ak ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay_ak, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay_ak,
               RootWindow(gpDisplay_ak, gpXVisualInfo_ak->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void initialize(void)
{
	void resize(int, int);
	void uninitialize(void);

    gpFile_ak = fopen("Log.txt", "w");
    if (gpFile_ak)
    {
        fprintf(gpFile_ak, "Log file created successfully \n");
    }
    else
    {
        fprintf(stderr, "Log file can't be created \n exiting \n");
        exit(EXIT_FAILURE);
    }

    glxCreateContextAttribsARB_ak = (glxCreateContextAttribsARBProc) glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");
    const int intAttribs[] = 
    {
        GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
        GLX_CONTEXT_MINOR_VERSION_ARB, 5,
        GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
        0
    };

    gGlxContext_ak = glxCreateContextAttribsARB_ak(gpDisplay_ak,gGLXFBConfig_ak, 0, True, intAttribs);
    if(!gGlxContext_ak)
    {
		int attribs[] = 
		{
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			0
		};
		gGlxContext_ak = glxCreateContextAttribsARB_ak(gpDisplay_ak, gGLXFBConfig_ak, 0, True, attribs);
	}
	
	Bool isDirectContext = glXIsDirect(gpDisplay_ak, gGlxContext_ak);
	if( isDirectContext == True)
	{
        printf("Rendering Context is Direct Hardware Context\n");
	}
	else
	{
        printf("Rendering Context is Direct Software Context\n");
	}
    

    //FFP
	// gGlxContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	// if(gGlxContext == NULL)
	// {
	// 	printf("Failed to create Rendering Context");
	// 	uninitialize();
	// 	exit(1);
	// }
	
	glXMakeCurrent(gpDisplay_ak, gWindow_ak, gGlxContext_ak);

    //PP
    GLenum glew_error = glewInit();
    if(glew_error != GLEW_OK)
	{
		glXDestroyContext(gpDisplay_ak, gGlxContext_ak);
		gGlxContext_ak = NULL;
		
		XCloseDisplay(gpDisplay_ak);
		gpDisplay_ak = NULL;
	}
    //PP

	GLint number_of_extensions;
	glGetIntegerv(GL_NUM_EXTENSIONS, &number_of_extensions);
	
	fprintf(gpFile_ak, "\nOpenGL Vendor : %s \n",glGetString(GL_VENDOR));
	fprintf(gpFile_ak, "\nOpenGL Renderer : %s \n",glGetString(GL_RENDERER));
	fprintf(gpFile_ak, "\nOpenGL Version : %s \n",glGetString(GL_VERSION));
	fprintf(gpFile_ak, "\nOpenGL GLSL Version : %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	GLint numExten_ak;

	glGetIntegerv(GL_NUM_EXTENSIONS,&numExten_ak);
	for (int i=0; i<numExten_ak; i++)
	{
		fprintf(gpFile_ak,"\nOpenGL Enabled Extensions:%s\n",glGetStringi(GL_EXTENSIONS,i));
	}

    gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode_ak =
        "#version 460 core"
        "\n"
        "in vec4 vPosition;"
        "in vec4 vColor;"
        "in vec3 vNormal;"
        "in vec2 vTexCoord;"
        "uniform mat4 u_model_matrix;"
        "uniform mat4 u_view_matrix;"
        "uniform mat4 u_projection_matrix;"
        "uniform vec4 u_light_position;"
        "uniform int u_lkey_pressed;"
        "out vec3 transformed_normal;"
        "out vec3 light_direction;"
        "out vec3 view_vector;"
        "out vec2 out_texcoord;"
        "out vec4 out_color;"
        "void main(void)"
        "{"
        "if(u_lkey_pressed == 1)"
        "{"
        "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"
        "transformed_normal = mat3((u_view_matrix * u_model_matrix)) * vNormal;"
        "light_direction = vec3(u_light_position - eye_coordinates);"
        "view_vector = -eye_coordinates.xyz;"
        "}"
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"
        "out_texcoord = vTexCoord;"
        "out_color = vColor;"
        "}";

    glShaderSource(gVertexShaderObject_ak, 1, (const GLchar**)&vertexShaderSourceCode_ak, NULL);
    glCompileShader(gVertexShaderObject_ak);

    GLint infoLogLength_ak = 0;
    GLint shaderCompileStatus_ak = 0;
    char* szBuffer_ak = NULL;
    glGetShaderiv(gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
        glGetShaderiv(gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetShaderInfoLog(gVertexShaderObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Vertex shader compilation log: %s\n", szBuffer_ak);
                free(szBuffer_ak);

            }
        }
    }

    gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentSourceCode_ak =
        "#version 460 core"
        "\n"
        "in vec4 out_color;"
        "in vec2 out_texcoord;"
        "in vec3 transformed_normal;"
        "in vec3 light_direction;"
        "in vec3 view_vector;"
        "uniform vec3 u_la;"
        "uniform vec3 u_ld;"
        "uniform vec3 u_ls;"
        "uniform vec3 u_ka;"
        "uniform vec3 u_kd;"
        "uniform vec3 u_ks;"
        "uniform float u_material_shininess;"
        "uniform int u_lkey_pressed;"
        "uniform sampler2D u_texture_sampler;"
        "out vec4 FragColor;"
        "void main(void)"
        "{"
        "vec3 phong_ads_light;"
        "if(u_lkey_pressed == 1)"
        "{"
        "vec3 normalized_transformed_normal = normalize(transformed_normal);"
        "vec3 normalized_light_direction = normalize(light_direction);"
        "vec3 normalized_view_vector = normalize(view_vector);"
        "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normal);"
        "vec3 ambient = u_la * u_ka;"
        "vec3 diffuse = u_ld * u_kd * max(dot(normalized_light_direction, normalized_transformed_normal), 0.0);"
        "vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, normalized_view_vector), 0.0), u_material_shininess);"
        "phong_ads_light = ambient + diffuse + specular;"
        "}"
        "else"
        "{"
        "phong_ads_light = vec3(1.0, 1.0, 1.0);"
        "}"
        "vec3 tex = vec3(texture(u_texture_sampler, out_texcoord));"
        "FragColor = vec4(vec3(out_color) * phong_ads_light * tex, 1.0f);"
        "}";

    glShaderSource(gFragmentShaderObject_ak, 1, (const GLchar**)&fragmentSourceCode_ak, NULL);
    glCompileShader(gFragmentShaderObject_ak);

    infoLogLength_ak = 0;
    shaderCompileStatus_ak = 0;
    szBuffer_ak = NULL;
    glGetShaderiv(gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
        glGetShaderiv(gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetShaderInfoLog(gFragmentShaderObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Fragment shader compilation log: %s\n", szBuffer_ak);
                free(szBuffer_ak);

            }
        }
    }

    gShaderProgramObject_ak = glCreateProgram();
    glAttachShader(gShaderProgramObject_ak, gVertexShaderObject_ak);
    glAttachShader(gShaderProgramObject_ak, gFragmentShaderObject_ak);

    glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_NORMAL, "vNormal");
    glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_COLOR, "vColor");
    glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_TEXCOORD, "vTexCoord");

    glLinkProgram(gShaderProgramObject_ak);

    GLint shaderProgramLinkStatus = 0;
    glGetProgramiv(gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
    if (shaderProgramLinkStatus == GL_FALSE) {
        glGetProgramiv(gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetProgramInfoLog(gShaderProgramObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Shader program link log: %s\n", szBuffer_ak);
                free(szBuffer_ak);

            }
        }
    }

    modelMatrixUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_model_matrix");
    viewMatrixUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_view_matrix");
    projectionMatrixUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_projection_matrix");
    laUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_la");
    ldUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_ld");
    lsUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_ls");
    lightPositionUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_light_position");
    kaUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_ka");
    kdUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_kd");
    ksUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_ks");
    materialShininessUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_material_shininess");
    lKeyPressedUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_lkey_pressed");
    textureSamplerUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_texture_sampler");

    const GLfloat cubePCNT_ak[] = {
        1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
        -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,

        1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
        1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,

        -1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
        1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f,
        1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f,

        -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
        -1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,

        1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
        -1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
        -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,

        1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f,
        -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f};

    glGenVertexArrays(1, &vao_cube_ak);

    glBindVertexArray(vao_cube_ak);
    glGenBuffers(1, &vbo_ak);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_ak);
    glBufferData(GL_ARRAY_BUFFER, 24 * 11 * sizeof(float), cubePCNT_ak, GL_STATIC_DRAW);

    glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(0 * sizeof(float)));
    glEnableVertexAttribArray(ATTRIBUTE_POSITION);

    glVertexAttribPointer(ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(ATTRIBUTE_COLOR);

    glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(ATTRIBUTE_NORMAL);

    glVertexAttribPointer(ATTRIBUTE_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(9 * sizeof(float)));
    glEnableVertexAttribArray(ATTRIBUTE_TEXCOORD);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    bool res = LoadGLTexture(&stone_texture_ak, MAKEINTRESOURCE(STONE_BITMAP));
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    // glEnable(GL_CULL_FACE);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    perspectiveProjectionMatrix_ak = mat4::identity();

    bAnimate_ak = false;
    bLight_ak = false;
    perspectiveProjectMatrix_ak = mat4::identity();

	resize(giWindowWidth_ak, giWindowHeight_ak);		
} 

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(gShaderProgramObject_ak);
	
	glUseProgram(gShaderProgramObject_ak);
    if (bLight_ak == true) {
        glUniform1i(lKeyPressedUniform_ak, 1);
        glUniform3fv(laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(materialShininessUniform_ak, materialShininess_ak);
    } else {
        glUniform1i(lKeyPressedUniform_ak, 0);
    }

    mat4 modelMatrix_ak;
    mat4 viewMatrix_ak;
    mat4 modelViewProjectionMatrix_ak;
    mat4 translateMatrix_ak;
    mat4 xRotationMatrix_ak;
    mat4 yRotationMatrix_ak;
    mat4 zRotationMatrix_ak;
    mat4 scaleMatrix_ak;

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    modelViewProjectionMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();
    xRotationMatrix_ak = mat4::identity();
    yRotationMatrix_ak = mat4::identity();
    zRotationMatrix_ak = mat4::identity();
    scaleMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -5.0f);
    xRotationMatrix_ak = vmath::rotate(cubeAngle_ak, 1.0f, 0.0f, 0.0f);
    yRotationMatrix_ak = vmath::rotate(cubeAngle_ak, 0.0f, 1.0f, 0.0f);
    zRotationMatrix_ak = vmath::rotate(cubeAngle_ak, 0.0f, 0.0f, 1.0f);
    scaleMatrix_ak = vmath::scale(0.75f, 0.75f, 0.75f);

    modelMatrix_ak = translateMatrix_ak * scaleMatrix_ak * xRotationMatrix_ak * yRotationMatrix_ak * zRotationMatrix_ak;

    glUniformMatrix4fv(modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, stone_texture_ak);
    glUniform1i(textureSamplerUniform_ak, 0);

    glBindVertexArray(vao_cube_ak);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    glBindVertexArray(0);

    glUseProgram(0);
    glXSwapBuffers(gpDisplay_ak, gWindow_ak); 	
}

void resize(int width, int height)
{
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	perspectiveProjectMatrix_ak = perspective(45.0f, ((GLfloat)width/(GLfloat)height),0.1f, 100.0f);

}

void update()
{
    cubeAngle_ak = cubeAngle_ak + 0.1f;
    if (cubeAngle_ak > 360.0f) {
        cubeAngle_ak = 0.0f;
    }
}

void uninitialize()
{
    GLXContext currentGlxContext;
    currentGlxContext = glXGetCurrentContext();

    if(Vao_pyramid_ak)
	{
		glDeleteVertexArrays(1, &Vao_pyramid_ak);
		Vao_pyramid_ak = 0;
	}

	if(Vbo_position_pyramid_ak)
	{
		glDeleteVertexArrays(1, &Vbo_position_pyramid_ak);
		Vbo_position_pyramid_ak = 0;
	}

	if(Vbo_color_pyramid_ak)
	{
		glDeleteVertexArrays(1, &Vbo_color_pyramid_ak);
		Vbo_color_pyramid_ak = 0;
	}

	if(Vao_cube_ak)
	{
		glDeleteVertexArrays(1, &Vao_cube_ak);
		Vao_cube_ak = 0;
	}

	if(Vbo_position_cube_ak)
	{
		glDeleteVertexArrays(1, &Vbo_position_cube_ak);
		Vbo_position_cube_ak = 0;
	}

	if(Vbo_color_cube_ak)
	{
		glDeleteVertexArrays(1, &Vbo_color_cube_ak);
		Vbo_color_cube_ak = 0;
	}


    if (gShaderProgramObject_ak) 
    {
        glUseProgram(gShaderProgramObject_ak);
        GLsizei shaderCount_ak;
        glGetProgramiv(gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

        GLuint* pShaders_ak = NULL;
        pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
        glGetAttachedShaders(gShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);

        for (GLsizei i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
            glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
            glDeleteShader(pShaders_ak[i_ak]);
            pShaders_ak[i_ak] = 0;
        }

        free(pShaders_ak);
        glDeleteProgram(gShaderProgramObject_ak);
        gShaderProgramObject_ak = 0;

        glUseProgram(0);
    }

    if(currentGlxContext != NULL && currentGlxContext == gGlxContext_ak)
	{
		glXMakeCurrent(gpDisplay_ak, 0, 0);
	}

    if(gGlxContext_ak)
	{
		glXDestroyContext(gpDisplay_ak, gGlxContext_ak);
	}

    if (gWindow_ak)
    {
        XDestroyWindow(gpDisplay_ak, gWindow_ak);
    }

    if (gColormap_ak)
    {
        XFreeColormap(gpDisplay_ak, gColormap_ak);
    }

    if (gpXVisualInfo_ak)
    {
        free(gpXVisualInfo_ak);
        gpXVisualInfo_ak = NULL;
    }

    if (gpDisplay_ak)
    {
        XCloseDisplay(gpDisplay_ak);
        gpDisplay_ak = NULL;
    }
	    if(gpFile_ak)
    {
		fprintf(gpFile_ak, "Log file is successfully closed.\n");
		fclose(gpFile_ak);
		gpFile_ak = NULL;
    }
}
