#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/glew.h>

#include <GL/gl.h>
#include <GL/glx.h>	
#include <GL/glu.h>

#include "vmath.h"

#include "Sphere.h"

using namespace std;
using namespace vmath;

GLXContext gGlxContext_ak;
bool bFullscreen_ak = false;
Display *gpDisplay_ak = NULL;
XVisualInfo *gpXVisualInfo_ak = NULL;
Colormap gColormap_ak;
Window gWindow_ak;
int giWindowWidth_ak = 800;
int giWindowHeight_ak = 600;

enum
{
	ATTRIBUTE_POSITION = 0,
	ATTRIBUTE_COLOR,
	ATTRIBUTE_NORMAL,
	ATRIBUTE_TEXCOORD,
};

GLuint gVertexShaderObject_ak;
GLuint gFragmentShaderObject_ak;
GLuint gShaderProgramObject_ak;

GLuint Vao_sphere_ak;
GLuint Vbo_position_sphere_ak;
GLuint Vbo_element_sphere_ak;
GLuint Vbo_normal_sphere_ak;

GLfloat lightAmbient_ak[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse_ak[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular_ak[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition_ak[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat materialAmbient_ak[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat materialDiffuse_ak[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular_ak[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess_ak = 50.0f;

//Uniforms
/********** 3 Uniforms**************/
GLuint ldUniform_ak;
GLuint kdUniform_ak;
GLuint lightPositionUniform_ak;

/************ 9 Uniforms******************/
GLuint laUniform_ak; // light component of ambient light
GLuint kaUniform_ak; 

GLuint lsUniform_ak; 
GLuint ksUniform_ak;

GLuint shininessUniform_ak;  //single float value

GLuint modelUniform_ak;
GLuint viewUniform_ak;
GLuint projectionUniform_ak;

GLuint lKeyPressedUniform_ak;
/*****************************/

bool bLight_ak;

mat4 perspectiveProjectMatrix_ak;

bool isLkey_ak = false;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint numVertices;
GLuint numElements;

FILE *gpFile_ak;

//PP
typedef GLXContext (*glxCreateContextAttribsARBProc) (Display*, GLXFBConfig, GLXContext, Bool, const int*);
glxCreateContextAttribsARBProc glxCreateContextAttribsARB_ak = NULL;
GLXFBConfig gGLXFBConfig_ak;
//PP

int main()
{
    void CreateWindow();
    void ToggleFullScreen();
    void uninitialize();

    void initialize();
    void resize(int, int);
    void display();
    void update();

    int winWidth = giWindowWidth_ak;
    int winHeight = giWindowHeight_ak;

     //message loop
    XEvent event;
    KeySym keysym;

    bool bDone = false;

    CreateWindow();
    initialize();
    while(bDone==false)
    {
        while(XPending(gpDisplay_ak))
        {
            XNextEvent(gpDisplay_ak, &event);
        switch (event.type)
        {
        case MapNotify:
            break;
        case KeyPress:
            keysym = XkbKeycodeToKeysym(gpDisplay_ak, event.xkey.keycode, 0, 0);
            switch (keysym)
            {
            case XK_Escape:
                bDone = true;

            case XK_F:
            case XK_f:
                if (bFullscreen_ak == false)
                {
                    ToggleFullScreen();
                    bFullscreen_ak = true;
                }
                else
                {
                    ToggleFullScreen();
                    bFullscreen_ak = false;
                }
                break;
            case XK_L:
            case XK_l:
			if (isLkey_ak == false)
			{
				bLight_ak = true;
				isLkey_ak = true;
			}
			else
			{
				bLight_ak = false;
				isLkey_ak = false;
			}
			break;
            default:
                break;
            }
            break;
        case ButtonPress:
            switch (event.xbutton.button)
            {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            default:
                break;
            }
        case MotionNotify:
            break;
        case ConfigureNotify:
            winWidth = event.xconfigure.width;
            winHeight = event.xconfigure.height;
            resize(winWidth,winHeight);
            break;
        case Expose:
            break;
        case DestroyNotify:
            break;
        case 33:
            bDone = true;
        default:
            break;
        }
    }
        update();
        display();
    }
    uninitialize();

    return 0;
}

void CreateWindow()
{
    void uninitialize();

    //FFP
    // static int frameBufferAttributes[]={GLX_DOUBLEBUFFER, True,
    //                                     GLX_RGBA,
    //                                     GLX_RED_SIZE, 8,
    //                                     GLX_GREEN_SIZE, 8, 
    //                                     GLX_BLUE_SIZE, 8,
    //                                     GLX_ALPHA_SIZE, 8,
    //                                     GLX_DEPTH_SIZE, 24,
    //                                     None
    //                                    };
    
    //PP
    static int frameBufferAttributes[]={
                                        GLX_X_RENDERABLE, True,     
                                        GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
                                        GLX_RENDER_TYPE, GLX_RGBA_BIT,
                                        GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
                                        GLX_DOUBLEBUFFER, True,
                                        GLX_RGBA,
                                        GLX_RED_SIZE, 8,
                                        GLX_GREEN_SIZE, 8, 
                                        GLX_BLUE_SIZE, 8,
                                        GLX_ALPHA_SIZE, 8,
                                        GLX_DEPTH_SIZE, 24,
                                        GLX_STENCIL_SIZE, 8,
                                        GLX_DOUBLEBUFFER, True,
                                        None
                                       };
    //PP

    XSetWindowAttributes winAttributes;
    int defaultScreen;
    int styleMask;

    //PP
    GLXFBConfig *pGLXFBConfig = NULL;
    GLXFBConfig bestGLXFBConfig; 
    XVisualInfo *pTempXVisualInfo;
    int numFBConfigs = 0;
    //PP

    gpDisplay_ak = XOpenDisplay(NULL);
    if (gpDisplay_ak == NULL)
    {
        printf("ERROR: Unable to Open X Display");
        uninitialize();
        exit(1);
    }

    //FFP
    // defaultScreen = XDefaultScreen(gpDisplay);
    // gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));
    // if (gpXVisualInfo == NULL)
    // {
    //     printf("ERROR: Unable to Allocate Memory for Visual Info");
    //     uninitialize();
    //     exit(1);
    // }
    // gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
    // if (gpXVisualInfo == NULL)
    // {
    //     printf("ERROR: Unable to Get a Visual");
    //     uninitialize();
    //     exit(1);
    // }

    //PP
    pGLXFBConfig = glXChooseFBConfig(gpDisplay_ak, DefaultScreen(gpDisplay_ak), frameBufferAttributes, &numFBConfigs);

    int bestFrameBufferConfig = -1;
    int worstFrameBufferConfig = -1;
    int bestNoOfSamples = -1;
    int worstNoOfSamples = 999;

    for (int i = 0; i < numFBConfigs; i++)
    {
        pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay_ak, pGLXFBConfig[i]);
        if(pTempXVisualInfo)
        {
            int sampleBuffers, samples;
            glXGetFBConfigAttrib(gpDisplay_ak, pGLXFBConfig[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);
            glXGetFBConfigAttrib(gpDisplay_ak, pGLXFBConfig[i], GLX_SAMPLES, &samples);

            if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNoOfSamples)
            {
                bestFrameBufferConfig = i;
                bestNoOfSamples = samples;
            }

            if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNoOfSamples)
            {
                worstFrameBufferConfig = i;
                worstNoOfSamples = samples;
            }
        }
        XFree(pTempXVisualInfo);
    }

    bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
    gGLXFBConfig_ak = bestGLXFBConfig;
    XFree(pGLXFBConfig);
    gpXVisualInfo_ak = glXGetVisualFromFBConfig(gpDisplay_ak,bestGLXFBConfig);
    //PP
    
    winAttributes.border_pixel = 0;
    winAttributes.background_pixmap = 0;
    winAttributes.colormap = XCreateColormap(gpDisplay_ak,
                                             RootWindow(gpDisplay_ak, gpXVisualInfo_ak->screen),
                                             gpXVisualInfo_ak->visual,
                                             AllocNone);
    gColormap_ak = winAttributes.colormap;

    winAttributes.background_pixel = BlackPixel(gpDisplay_ak, defaultScreen);

    winAttributes.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow_ak = XCreateWindow(gpDisplay_ak,
                            RootWindow(gpDisplay_ak, gpXVisualInfo_ak->screen),
                            0,
                            0,
                            giWindowWidth_ak,
                            giWindowHeight_ak,
                            0,
                            gpXVisualInfo_ak->depth,
                            InputOutput,
                            gpXVisualInfo_ak->visual,
                            styleMask,
                            &winAttributes);

    if (!gWindow_ak)
    {
        printf("ERROR: Failed To Create Main Window");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay_ak, gWindow_ak, "PP: 3D Plain Cube");

    Atom windowMAnagerDelete = XInternAtom(gpDisplay_ak, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay_ak, gWindow_ak, &windowMAnagerDelete, 1);
    XMapWindow(gpDisplay_ak, gWindow_ak);
}

void ToggleFullScreen()
{
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    wm_state = XInternAtom(gpDisplay_ak, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow_ak;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen_ak ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay_ak, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay_ak,
               RootWindow(gpDisplay_ak, gpXVisualInfo_ak->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void initialize(void)
{
	void resize(int, int);
	void uninitialize(void);

    gpFile_ak = fopen("Log.txt", "w");
    if (gpFile_ak)
    {
        fprintf(gpFile_ak, "Log file created successfully \n");
    }
    else
    {
        fprintf(stderr, "Log file can't be created \n exiting \n");
        exit(EXIT_FAILURE);
    }

    glxCreateContextAttribsARB_ak = (glxCreateContextAttribsARBProc) glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");
    const int intAttribs[] = 
    {
        GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
        GLX_CONTEXT_MINOR_VERSION_ARB, 5,
        GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
        0
    };

    gGlxContext_ak = glxCreateContextAttribsARB_ak(gpDisplay_ak,gGLXFBConfig_ak, 0, True, intAttribs);
    if(!gGlxContext_ak)
    {
		int attribs[] = 
		{
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			0
		};
		gGlxContext_ak = glxCreateContextAttribsARB_ak(gpDisplay_ak, gGLXFBConfig_ak, 0, True, attribs);
	}
	
	Bool isDirectContext = glXIsDirect(gpDisplay_ak, gGlxContext_ak);
	if( isDirectContext == True)
	{
        printf("Rendering Context is Direct Hardware Context\n");
	}
	else
	{
        printf("Rendering Context is Direct Software Context\n");
	}
    

    //FFP
	// gGlxContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	// if(gGlxContext == NULL)
	// {
	// 	printf("Failed to create Rendering Context");
	// 	uninitialize();
	// 	exit(1);
	// }
	
	glXMakeCurrent(gpDisplay_ak, gWindow_ak, gGlxContext_ak);

    //PP
    GLenum glew_error = glewInit();
    if(glew_error != GLEW_OK)
	{
		glXDestroyContext(gpDisplay_ak, gGlxContext_ak);
		gGlxContext_ak = NULL;
		
		XCloseDisplay(gpDisplay_ak);
		gpDisplay_ak = NULL;
	}
    //PP

    GLint number_of_extensions;
	glGetIntegerv(GL_NUM_EXTENSIONS, &number_of_extensions);
	
	fprintf(gpFile_ak, "\nOpenGL Vendor : %s \n",glGetString(GL_VENDOR));
	fprintf(gpFile_ak, "\nOpenGL Renderer : %s \n",glGetString(GL_RENDERER));
	fprintf(gpFile_ak, "\nOpenGL Version : %s \n",glGetString(GL_VERSION));
	fprintf(gpFile_ak, "\nOpenGL GLSL Version : %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	GLint numExten_ak;

	glGetIntegerv(GL_NUM_EXTENSIONS,&numExten_ak);
	for (int i=0; i<numExten_ak; i++)
	{
		fprintf(gpFile_ak,"\nOpenGL Enabled Extensions:%s\n",glGetStringi(GL_EXTENSIONS,i));
	}

    gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
	const GLchar *vertexShaderSourceCode = 
	"#version 430 core" \
	"\n" \
	"in vec4 vPosition;" \
	"in vec3 vNormal;"  \
	"uniform mat4 u_model_matrix;" \
	"uniform mat4 u_projection_matrix;" \
	"uniform mat4 u_view_matrix;" \
	"uniform int u_LKeyPressed;" \
	"uniform vec4 u_light_position;" \
	"out vec3 transformed_normal;" \
	"out vec3 light_direction;" \
	"out vec3 view_vector;" \
	"void main(void)" \
	"{" \
		"if (u_LKeyPressed == 1) " \
		"{" \
			"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
			"transformed_normal = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
			"light_direction = vec3(u_light_position - eyeCoordinates);" \
		    "view_vector = -eyeCoordinates.xyz;" \
		"}" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
	"}";
	
	glShaderSource(gVertexShaderObject_ak,1,(const GLchar **)&vertexShaderSourceCode, NULL);

	// compile Vertex Shader
	glCompileShader(gVertexShaderObject_ak);

	// Error checking for Vertex Shader
	GLint infoLogLength = 0; 
	GLint shaderCompiledStatus = 0;
	char *szBuffer = NULL;

	glGetShaderiv(gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if(shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength>0)
		{
			szBuffer = (char *)malloc(infoLogLength);
			if(szBuffer!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_ak, infoLogLength, &written, szBuffer);
			}
		}
	}

	//Fragment Shader
	/* out_color is the output of Vertex Shader */
	gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* vertexFragmentSourceCode =
		"#version 430 core" \
		"\n" \
		"out vec4 FragColor;" \
		"in vec3 transformed_normal;" \
		"in vec3 light_direction;" \
		"in vec3 view_vector;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_La;" \
		"uniform vec3 u_Ls;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_Shininess;" \
		"uniform int u_LKeyPressed;" \
		"void main(void)" \
		"{" \
			"vec3 phong_ads_color;" \
			"if(u_LKeyPressed == 1)" \
			"{" \
				"vec3 norm_transformed_normal = normalize(transformed_normal);" \
				"vec3 norm_light_direction = normalize(light_direction);" \
				"vec3 norm_view_vector = normalize(view_vector);" \
				"vec3 reflection_vector = reflect(-norm_light_direction, norm_transformed_normal);" \
				"vec3 ambient = u_La * u_Ka;" \
				"vec3 diffuse = u_Ld * u_Kd * max(dot(norm_light_direction,norm_transformed_normal),0.0);" \
				"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector,norm_view_vector),0.0),u_Shininess);" \
				"phong_ads_color = ambient + diffuse + specular;" \
			"}" \
			"else" \
			"{" \
				"phong_ads_color = vec3(1.0f,1.0f,1.0f);" \
			"}" \

			"FragColor = vec4(phong_ads_color, 1.0f);" \
		"}";

	glShaderSource(gFragmentShaderObject_ak,1,(const GLchar **)&vertexFragmentSourceCode, NULL);

	// compile Fragment Shader
	glCompileShader(gFragmentShaderObject_ak);

	// Error Checking for Fragment Shader
	glGetShaderiv(gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if(shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength>0)
		{
			szBuffer = (char *)malloc(infoLogLength);
			if(szBuffer!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_ak, infoLogLength, &written, szBuffer);
			}
		}
	}

	//Shader Program
	gShaderProgramObject_ak = glCreateProgram();
	glAttachShader(gShaderProgramObject_ak,gVertexShaderObject_ak);
	glAttachShader(gShaderProgramObject_ak,gFragmentShaderObject_ak);

	// Bind the attributes in shader with the enums in your main program
	/* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
	glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_NORMAL, "vNormal");
	
	glLinkProgram(gShaderProgramObject_ak);

	// Linking Error Checking
	GLint shaderProgramLinkStatus = 0;
	szBuffer = NULL;

	glGetProgramiv(gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
	if(shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength>0)
		{
			szBuffer = (char *)malloc(infoLogLength);
			if(szBuffer!=NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_ak, infoLogLength, &written, szBuffer);
			}
		}
	}

	//Get the information of uniform Post linking
	modelUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_model_matrix");
	viewUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_view_matrix");
	projectionUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_projection_matrix");

	ldUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_Ld");
	kdUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_Kd");
	lightPositionUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_light_position");
	
	laUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_La");
	kaUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_Ka");

	lsUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_Ls");
	ksUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_Ks"); 

	shininessUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_Shininess");

	lKeyPressedUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_LKeyPressed");

	//Shader attributes for sphere
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	numVertices = getNumberOfSphereVertices();
	numElements = getNumberOfSphereElements();

	// Push the above vertices to vPosition

	//Steps
	/* 1. Tell OpenGl to create one buffer in your VRAM
	      Give me a symbol to identify. It is known as NamedBuffer
	      In OpenGL terminology, it is called as GL_ARRAY_BUFFER. This is becase vertex has plenty of attributes 
		  like color, texture, etc. Also it requires contiguous memory.
		  User identifies this variable as Vbo_position and GPU as GL_ARRAY_BUFFER. 
	   2. Bind with the above symbol. It doesn't unbind until 'unbind step' is performed eg- Railway track
	   3. Insert triangle data into the buffer.
	   4. Specify where to insert this data into shader and also how to use it.
	   5. Enable the 'in' point. 
	   6. Unbind 
	*/

	//Sphere
	
	// Sphere Vao
	glGenVertexArrays(1, &Vao_sphere_ak);
	glBindVertexArray(Vao_sphere_ak);

	// Sphere position Vbo
	glGenBuffers(1, &Vbo_position_sphere_ak);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_sphere_ak);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	// Sphere element Vbo
	glGenBuffers(1, &Vbo_element_sphere_ak);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Vbo_element_sphere_ak);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// Sphere normals Vbo
	glGenBuffers(1, &Vbo_normal_sphere_ak);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_normal_sphere_ak);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_NORMAL);

	// unbind vao
	glBindVertexArray(0);

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glShadeModel(GL_SMOOTH);	
	glClearDepth(1.0f);				
	glEnable(GL_DEPTH_TEST);			
	glDepthFunc(GL_LEQUAL);	
	glHint(GL_PERSPECTIVE_CORRECTION_HINT , GL_NICEST);

    perspectiveProjectMatrix_ak = mat4::identity();

	resize(giWindowWidth_ak, giWindowHeight_ak);		
} 

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
	glUseProgram(gShaderProgramObject_ak);

	if (bLight_ak == true)
	{
		glUniform1i(lKeyPressedUniform_ak, 1);
		glUniform3fv(laUniform_ak,1,lightAmbient_ak);
		glUniform3fv(ldUniform_ak,1,lightDiffuse_ak);
		glUniform3fv(lsUniform_ak,1,lightSpecular_ak);
		glUniform4fv(lightPositionUniform_ak, 1, lightPosition_ak);

		glUniform3fv(kaUniform_ak, 1, materialAmbient_ak);
		glUniform3fv(kdUniform_ak,1, materialDiffuse_ak);
		glUniform3fv(ksUniform_ak,1, materialSpecular_ak);
		glUniform1f(shininessUniform_ak, materialShininess_ak);

	}
	else
		glUniform1i(lKeyPressedUniform_ak, 0);


	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 translationMatrix = mat4::identity();

	translationMatrix = translate(0.0f, 0.0f, -4.0f);
	modelMatrix = translationMatrix;

	glUniformMatrix4fv(modelUniform_ak, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewUniform_ak, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionUniform_ak, 1, GL_FALSE, perspectiveProjectMatrix_ak);
	
	//Sphere Begin
	glBindVertexArray(Vao_sphere_ak);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Vbo_element_sphere_ak);
	glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
	
	glBindVertexArray(0); // Sphere end

	glUseProgram(0);

    glXSwapBuffers(gpDisplay_ak, gWindow_ak); 	
}

void resize(int width, int height)
{
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	perspectiveProjectMatrix_ak = perspective(45.0f, ((GLfloat)width/(GLfloat)height),0.1f, 100.0f);

}

void update()
{

}

void uninitialize()
{
    GLXContext currentGlxContext;
    currentGlxContext = glXGetCurrentContext();
	
	if(Vao_sphere_ak)
	{
		glDeleteVertexArrays(1, &Vao_sphere_ak);
		Vao_sphere_ak = 0;
	}

	if(Vbo_position_sphere_ak)
	{
		glDeleteVertexArrays(1, &Vbo_position_sphere_ak);
		Vbo_position_sphere_ak = 0;
	}

	if(Vbo_element_sphere_ak)
	{
		glDeleteVertexArrays(1, &Vbo_element_sphere_ak);
		Vbo_element_sphere_ak = 0;
	}


    if (gShaderProgramObject_ak) 
    {
        glUseProgram(gShaderProgramObject_ak);
        GLsizei shaderCount_ak;
        glGetProgramiv(gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

        GLuint* pShaders_ak = NULL;
        pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
        glGetAttachedShaders(gShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);

        for (GLsizei i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
            glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
            glDeleteShader(pShaders_ak[i_ak]);
            pShaders_ak[i_ak] = 0;
        }

        free(pShaders_ak);
        glDeleteProgram(gShaderProgramObject_ak);
        gShaderProgramObject_ak = 0;

        glUseProgram(0);
    }

    if(currentGlxContext != NULL && currentGlxContext == gGlxContext_ak)
	{
		glXMakeCurrent(gpDisplay_ak, 0, 0);
	}

    if(gGlxContext_ak)
	{
		glXDestroyContext(gpDisplay_ak, gGlxContext_ak);
	}

    if (gWindow_ak)
    {
        XDestroyWindow(gpDisplay_ak, gWindow_ak);
    }

    if (gColormap_ak)
    {
        XFreeColormap(gpDisplay_ak, gColormap_ak);
    }

    if (gpXVisualInfo_ak)
    {
        free(gpXVisualInfo_ak);
        gpXVisualInfo_ak = NULL;
    }

    if (gpDisplay_ak)
    {
        XCloseDisplay(gpDisplay_ak);
        gpDisplay_ak = NULL;
    }
        if(gpFile_ak)
    {
		fprintf(gpFile_ak, "Log file is successfully closed.\n");
		fclose(gpFile_ak);
		gpFile_ak = NULL;
    }
}
