#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/glew.h>

#include <GL/gl.h>
#include <GL/glx.h>	
#include <GL/glu.h>

#include "vmath.h"

using namespace std;
using namespace vmath;

GLXContext gGlxContext_ak;
bool bFullscreen_ak = false;
Display *gpDisplay_ak = NULL;
XVisualInfo *gpXVisualInfo_ak = NULL;
Colormap gColormap_ak;
Window gWindow_ak;
int giWindowWidth_ak = 800;
int giWindowHeight_ak = 600;

enum
{
	ATTRIBUTE_POSITION = 0,
	ATTRIBUTE_COLOR,
	ATTRIBUTE_NORMAL,
	ATRIBUTE_TEXCOORD,
};

GLuint gVertexShaderObject_ak;
GLuint gFragmentShaderObject_ak;
GLuint gShaderProgramObject_ak;

GLuint mvpMatrixUniform;
GLuint Vao;
GLuint Vbo_position;

GLuint tessellationControlShaderObject_ak;
GLuint tessellationEvaluationShaderObject_ak;
GLuint numberOfSegmentsUniform_ak;
GLuint numberOfStripsUniform_ak;
GLuint lineColorUniform_ak;

mat4 perspectiveProjectMatrix;
unsigned int UISegments_ak;

FILE *gpFile_ak;

//PP
typedef GLXContext (*glxCreateContextAttribsARBProc) (Display*, GLXFBConfig, GLXContext, Bool, const int*);
glxCreateContextAttribsARBProc glxCreateContextAttribsARB_ak = NULL;
GLXFBConfig gGLXFBConfig_ak;
//PP

int main()
{
    void CreateWindow();
    void ToggleFullScreen();
    void uninitialize();

    void initialize();
    void resize(int, int);
    void display();
    void update();

    int winWidth = giWindowWidth_ak;
    int winHeight = giWindowHeight_ak;

     //message loop
    XEvent event;
    KeySym keysym;

    bool bDone = false;

    CreateWindow();
    initialize();
    while(bDone==false)
    {
        while(XPending(gpDisplay_ak))
        {
            XNextEvent(gpDisplay_ak, &event);
        switch (event.type)
        {
        case MapNotify:
            break;
        case KeyPress:
            keysym = XkbKeycodeToKeysym(gpDisplay_ak, event.xkey.keycode, 0, 0);
            switch (keysym)
            {
            case XK_Escape:
                bDone = true;

            case XK_F:
            case XK_f:
                if (bFullscreen_ak == false)
                {
                    ToggleFullScreen();
                    bFullscreen_ak = true;
                }
                else
                {
                    ToggleFullScreen();
                    bFullscreen_ak = false;
                }
                break;
            default:
                break;
            }
            break;
        case ButtonPress:
            switch (event.xbutton.button)
            {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            default:
                break;
            }
        case MotionNotify:
            break;
        case ConfigureNotify:
            winWidth = event.xconfigure.width;
            winHeight = event.xconfigure.height;
            resize(winWidth,winHeight);
            break;
        case Expose:
            break;
        case DestroyNotify:
            break;
        case 33:
            bDone = true;
        default:
            break;
        }
    }
        update();
        display();
    }
    uninitialize();

    return 0;
}

void CreateWindow()
{
    void uninitialize();

    //FFP
    // static int frameBufferAttributes[]={GLX_DOUBLEBUFFER, True,
    //                                     GLX_RGBA,
    //                                     GLX_RED_SIZE, 8,
    //                                     GLX_GREEN_SIZE, 8, 
    //                                     GLX_BLUE_SIZE, 8,
    //                                     GLX_ALPHA_SIZE, 8,
    //                                     GLX_DEPTH_SIZE, 24,
    //                                     None
    //                                    };
    
    //PP
    static int frameBufferAttributes[]={
                                        GLX_X_RENDERABLE, True,     
                                        GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
                                        GLX_RENDER_TYPE, GLX_RGBA_BIT,
                                        GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
                                        GLX_DOUBLEBUFFER, True,
                                        GLX_RED_SIZE, 8,
                                        GLX_GREEN_SIZE, 8, 
                                        GLX_BLUE_SIZE, 8,
                                        GLX_ALPHA_SIZE, 8,
                                        GLX_DEPTH_SIZE, 24,
                                        GLX_STENCIL_SIZE, 8,
                                        None
                                       };
    //PP

    XSetWindowAttributes winAttributes;
    int defaultScreen;
    int styleMask;

    //PP
    GLXFBConfig *pGLXFBConfig = NULL;
    GLXFBConfig bestGLXFBConfig; 
    XVisualInfo *pTempXVisualInfo;
    int numFBConfigs = 0;
    //PP

    gpDisplay_ak = XOpenDisplay(NULL);
    if (gpDisplay_ak == NULL)
    {
        printf("ERROR: Unable to Open X Display");
        uninitialize();
        exit(1);
    }

    //FFP
    // defaultScreen = XDefaultScreen(gpDisplay);
    // gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));
    // if (gpXVisualInfo == NULL)
    // {
    //     printf("ERROR: Unable to Allocate Memory for Visual Info");
    //     uninitialize();
    //     exit(1);
    // }
    // gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
    // if (gpXVisualInfo == NULL)
    // {
    //     printf("ERROR: Unable to Get a Visual");
    //     uninitialize();
    //     exit(1);
    // }

    //PP
    pGLXFBConfig = glXChooseFBConfig(gpDisplay_ak, DefaultScreen(gpDisplay_ak), frameBufferAttributes, &numFBConfigs);

    int bestFrameBufferConfig = -1;
    int worstFrameBufferConfig = -1;
    int bestNoOfSamples = -1;
    int worstNoOfSamples = 999;

    for (int i = 0; i < numFBConfigs; i++)
    {
        pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay_ak, pGLXFBConfig[i]);
        if(pTempXVisualInfo)
        {
            int sampleBuffers, samples;
            glXGetFBConfigAttrib(gpDisplay_ak, pGLXFBConfig[i], GLX_SAMPLE_BUFFERS, &sampleBuffers);
            glXGetFBConfigAttrib(gpDisplay_ak, pGLXFBConfig[i], GLX_SAMPLES, &samples);

            if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNoOfSamples)
            {
                bestFrameBufferConfig = i;
                bestNoOfSamples = samples;
            }

            if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNoOfSamples)
            {
                worstFrameBufferConfig = i;
                worstNoOfSamples = samples;
            }
        }
        XFree(pTempXVisualInfo);
    }

    bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
    gGLXFBConfig_ak = bestGLXFBConfig;
    XFree(pGLXFBConfig);
    gpXVisualInfo_ak = glXGetVisualFromFBConfig(gpDisplay_ak,bestGLXFBConfig);
    //PP
    
    winAttributes.border_pixel = 0;
    winAttributes.background_pixmap = 0;
    winAttributes.colormap = XCreateColormap(gpDisplay_ak,
                                             RootWindow(gpDisplay_ak, gpXVisualInfo_ak->screen),
                                             gpXVisualInfo_ak->visual,
                                             AllocNone);
    gColormap_ak = winAttributes.colormap;

    winAttributes.background_pixel = BlackPixel(gpDisplay_ak, defaultScreen);

    winAttributes.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow_ak = XCreateWindow(gpDisplay_ak,
                            RootWindow(gpDisplay_ak, gpXVisualInfo_ak->screen),
                            0,
                            0,
                            giWindowWidth_ak,
                            giWindowHeight_ak,
                            0,
                            gpXVisualInfo_ak->depth,
                            InputOutput,
                            gpXVisualInfo_ak->visual,
                            styleMask,
                            &winAttributes);

    if (!gWindow_ak)
    {
        printf("ERROR: Failed To Create Main Window");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay_ak, gWindow_ak, "Tessellation shader");

    Atom windowMAnagerDelete = XInternAtom(gpDisplay_ak, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(gpDisplay_ak, gWindow_ak, &windowMAnagerDelete, 1);
    XMapWindow(gpDisplay_ak, gWindow_ak);
}

void ToggleFullScreen()
{
    Atom wm_state;
    Atom fullscreen;
    XEvent xev = {0};

    wm_state = XInternAtom(gpDisplay_ak, "_NET_WM_STATE", False);
    memset(&xev, 0, sizeof(xev));

    xev.type = ClientMessage;
    xev.xclient.window = gWindow_ak;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = bFullscreen_ak ? 0 : 1;

    fullscreen = XInternAtom(gpDisplay_ak, "_NET_WM_STATE_FULLSCREEN", False);
    xev.xclient.data.l[1] = fullscreen;

    XSendEvent(gpDisplay_ak,
               RootWindow(gpDisplay_ak, gpXVisualInfo_ak->screen),
               False,
               StructureNotifyMask,
               &xev);
}

void initialize(void)
{
	void resize(int, int);
	void uninitialize(void);

    gpFile_ak = fopen("Log.txt", "w");
    if (gpFile_ak)
    {
        fprintf(gpFile_ak, "Log file created successfully \n");
    }
    else
    {
        fprintf(stderr, "Log file can't be created \n exiting \n");
        exit(EXIT_FAILURE);
    }

    glxCreateContextAttribsARB_ak = (glxCreateContextAttribsARBProc) glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");
    const int intAttribs[] = 
    {
        GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
        GLX_CONTEXT_MINOR_VERSION_ARB, 5,
        GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
        0
    };

    gGlxContext_ak = glxCreateContextAttribsARB_ak(gpDisplay_ak,gGLXFBConfig_ak, 0, True, intAttribs);
    if(!gGlxContext_ak)
    {
		int attribs[] = 
		{
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			0
		};
		gGlxContext_ak = glxCreateContextAttribsARB_ak(gpDisplay_ak, gGLXFBConfig_ak, 0, True, attribs);
	}
	
	Bool isDirectContext = glXIsDirect(gpDisplay_ak, gGlxContext_ak);
	if( isDirectContext == True)
	{
        printf("Rendering Context is Direct Hardware Context\n");
	}
	else
	{
        printf("Rendering Context is Direct Software Context\n");
	}
    

    //FFP
	// gGlxContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	// if(gGlxContext == NULL)
	// {
	// 	printf("Failed to create Rendering Context");
	// 	uninitialize();
	// 	exit(1);
	// }
	
	glXMakeCurrent(gpDisplay_ak, gWindow_ak, gGlxContext_ak);

    //PP
    GLenum glew_error = glewInit();
    if(glew_error != GLEW_OK)
	{
		glXDestroyContext(gpDisplay_ak, gGlxContext_ak);
		gGlxContext_ak = NULL;
		
		XCloseDisplay(gpDisplay_ak);
		gpDisplay_ak = NULL;
	}
    //PP

	GLint number_of_extensions;
	glGetIntegerv(GL_NUM_EXTENSIONS, &number_of_extensions);
	
	fprintf(gpFile_ak, "\nOpenGL Vendor : %s \n",glGetString(GL_VENDOR));
	fprintf(gpFile_ak, "\nOpenGL Renderer : %s \n",glGetString(GL_RENDERER));
	fprintf(gpFile_ak, "\nOpenGL Version : %s \n",glGetString(GL_VERSION));
	fprintf(gpFile_ak, "\nOpenGL GLSL Version : %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	GLint numExten_ak;

	glGetIntegerv(GL_NUM_EXTENSIONS,&numExten_ak);
	for (int i=0; i<numExten_ak; i++)
	{
		fprintf(gpFile_ak,"\nOpenGL Enabled Extensions:%s\n",glGetStringi(GL_EXTENSIONS,i));
	}

    gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
	const GLchar *vertexShaderSourceCode = 
	"#version 460 core" \
	"\n" \
	"in vec2 vPosition;" \
	"void main(void)" \
	"{" \
	"gl_Position = vec4(vPosition, 0.0, 1.0);" \
	"}";
	
	glShaderSource(gVertexShaderObject_ak,1,(const GLchar **)&vertexShaderSourceCode, NULL);

	// compile Vertex Shader
	glCompileShader(gVertexShaderObject_ak);

	// Error checking for Vertex Shader
	GLint infoLogLength = 0; 
	GLint shaderCompiledStatus = 0;
	char *szBuffer = NULL;

	glGetShaderiv(gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if(shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength>0)
		{
			szBuffer = (char *)malloc(infoLogLength);
			if(szBuffer!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_ak, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Vertex Shader Compilation Log: %s\n",szBuffer);
				free(szBuffer);
				szBuffer = NULL;

			}
		}
	}

	tessellationControlShaderObject_ak = glCreateShader(GL_TESS_CONTROL_SHADER);
	const GLchar* tessellationControlSourceCode =
		"#version 460 core" \
		"\n" \
		"layout(vertices = 4)out;"
		"uniform int numberOfSegments;"
		"uniform int numberOfStrips;"
		"void main(void)" \
		"{" \
			"gl_out[gl_InvocationID].gl_Position=gl_in[gl_InvocationID].gl_Position;"\
			"gl_TessLevelOuter[0]=float(numberOfStrips);"\
			"gl_TessLevelOuter[1]=float(numberOfSegments);"\
		"}";

	glShaderSource(tessellationControlShaderObject_ak, 1, (const GLchar**)&tessellationControlSourceCode, NULL);

	// compile Vertex Shader
	glCompileShader(tessellationControlShaderObject_ak);

	// Error checking for Vertex Shader
	infoLogLength = 0;
	shaderCompiledStatus = 0;
	szBuffer = NULL;

	glGetShaderiv(tessellationControlShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if (shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(tessellationControlShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			szBuffer = (char*)malloc(infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(tessellationControlShaderObject_ak, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Tessellation Control Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				szBuffer = NULL;

			}
		}
	}

	tessellationEvaluationShaderObject_ak = glCreateShader(GL_TESS_EVALUATION_SHADER);
	const GLchar* tessellationEvaluationSourceCode =
		"#version 460 core" \
		"\n" \
		"layout(isolines)in;"
		"uniform mat4 u_mvpMatrix;"
		"void main(void)" \
		"{" \
			"float tessCoord = gl_TessCoord.x;"
			"vec3 p0 = gl_in[0].gl_Position.xyz;"
			"vec3 p1 = gl_in[1].gl_Position.xyz;"
			"vec3 p2 = gl_in[2].gl_Position.xyz;"
			"vec3 p3 = gl_in[3].gl_Position.xyz;"
			"vec3 p = p0 * (1.0-tessCoord) * (1.0-tessCoord) * (1.0-tessCoord) + p1 * 3.0 * tessCoord * (1.0-tessCoord) * (1.0-tessCoord) + p2 * tessCoord * tessCoord * (1.0-tessCoord) + p3 * tessCoord * tessCoord * tessCoord;"
			"gl_Position = u_mvpMatrix * vec4(p, 1.0);"
		"}";

	glShaderSource(tessellationEvaluationShaderObject_ak, 1, (const GLchar**)&tessellationEvaluationSourceCode, NULL);

	// compile Vertex Shader
	glCompileShader(tessellationEvaluationShaderObject_ak);

	// Error checking for Vertex Shader
	infoLogLength = 0;
	shaderCompiledStatus = 0;
	szBuffer = NULL;

	glGetShaderiv(tessellationEvaluationShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if (shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(tessellationEvaluationShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			szBuffer = (char*)malloc(infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(tessellationEvaluationShaderObject_ak, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Tessellation Evaluation Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				szBuffer = NULL;

			}
		}
	}

	//Fragment Shader
	gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar *vertexFragmentSourceCode = 
	"#version 460" \
	"\n" \
	"out vec4 FragColor;" \
	"uniform vec4 lineColor;"
	"void main(void)" \
	"{" \
	"FragColor = lineColor;" \
	"}";

	glShaderSource(gFragmentShaderObject_ak,1,(const GLchar **)&vertexFragmentSourceCode, NULL);

	// compile Fragment Shader
	glCompileShader(gFragmentShaderObject_ak);

	// Error Checking for Fragment Shader
	glGetShaderiv(gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if(shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength>0)
		{
			szBuffer = (char *)malloc(infoLogLength);
			if(szBuffer!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_ak, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Fragment Shader Compilation Log: %s\n",szBuffer);
				free(szBuffer);
				szBuffer = NULL;

			}
		}
	}

	//Shader Program
	gShaderProgramObject_ak = glCreateProgram();
	glAttachShader(gShaderProgramObject_ak, gVertexShaderObject_ak);  
	glAttachShader(gShaderProgramObject_ak, tessellationControlShaderObject_ak);
	glAttachShader(gShaderProgramObject_ak, tessellationEvaluationShaderObject_ak);
	glAttachShader(gShaderProgramObject_ak, gFragmentShaderObject_ak);

	// Bind the attributes in shader with the enums in your main program
	/* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
	glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");

	glLinkProgram(gShaderProgramObject_ak);

	// Linking Error Checking
	GLint shaderProgramLinkStatus = 0;
	szBuffer = NULL;

	glGetProgramiv(gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
	if(shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength>0)
		{
			szBuffer = (char *)malloc(infoLogLength);
			if(szBuffer!=NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_ak, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Shader Program Link Log: %s\n",szBuffer);
				free(szBuffer);
				szBuffer = NULL;

			}
		}
	}

	//Get the information of uniform Post linking
	mvpMatrixUniform = glGetUniformLocation(gShaderProgramObject_ak, "u_mvpMatrix");
	numberOfSegmentsUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "numberOfSegments");
	numberOfStripsUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "numberOfStrips");
	lineColorUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "lineColor");

	//Vertices Array Declaration
	const GLfloat vertices[]=
	{   
		-1.0f, -1.0f,
		-0.5f, 1.0f,
		0.5f, -1.0f,
		1.0f, 1.0f
	};

	//Repeat the below steps of Vbo_position and call them in draw method
	glGenVertexArrays(1, &Vao);
	glBindVertexArray(Vao);

	// Push the above vertices to vPosition

	//Steps
	/* 1. Tell OpenGl to create one buffer in your VRAM
	      Give me a symbol to identify. It is known as NamedBuffer
	      In OpenGL terminology, it is called as GL_ARRAY_BUFFER. This is becase vertex has plenty of attributes 
		  like color, texture, etc. Also it requires contiguous memory.
		  User identifies this variable as Vbo_position and GPU as GL_ARRAY_BUFFER. 
	   2. Bind with the above symbol. It doesn't unbind until 'unbind step' is performed eg- Railway track
	   3. Insert triangle data into the buffer.
	   4. Specify where to insert this data into shader and also how to use it.
	   5. Enable the 'in' point. 
	   6. Unbind 
	*/
	glGenBuffers(1, &Vbo_position);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	// 3 is specified for 3 pairs for triangle vertices
	/* For Texture, specify 2*/
    // 4th parameter----> Normalized Co-ordinates
	// 5th How many strides to take?
	// 6th From which position	
	glVertexAttribPointer(ATTRIBUTE_POSITION, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);  //change tracks to link different attributes
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);
	glClearColor(0.0f, 0.0f, 1.0f, 0.0f);

	// Set OrthographicMatrix to identity matrix 
	perspectiveProjectMatrix = mat4::identity();

	UISegments_ak = 1;

	resize(giWindowWidth_ak, giWindowHeight_ak);		
} 

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(gShaderProgramObject_ak);
	
	//OpenGL Draw

	//Set ModelView and ModelViewProjection matrices to identity
	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();

	modelViewProjectionMatrix = perspectiveProjectMatrix*modelViewMatrix;

	// Translate call
	mat4 translateMatrix = translate(0.0f,0.0f,-4.0f);
	modelViewMatrix = translateMatrix;
	modelViewProjectionMatrix = perspectiveProjectMatrix*modelViewMatrix;

	// After this line, modelViewProjectionMatrix becomes u_mvpMatrix
	glUniformMatrix4fv(mvpMatrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniform1i(numberOfSegmentsUniform_ak, UISegments_ak);
	glUniform1i(numberOfStripsUniform_ak, 1);
	glUniform4fv(lineColorUniform_ak, 1, vec4(1.0f, 1.0f, 0.0f, 1.0f));

	//Bind Vao
	glBindVertexArray(Vao); //change tracks  //Begin

	glPatchParameteri(GL_PATCH_VERTICES, 4);

	glDrawArrays(GL_PATCHES, 0, 4);

	// Unbind Vao
	glBindVertexArray(0);  //end

	glUseProgram(0);
    
    glXSwapBuffers(gpDisplay_ak, gWindow_ak); 	
}

void resize(int width, int height)
{
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	perspectiveProjectMatrix = perspective(45.0f, ((GLfloat)width/(GLfloat)height),0.1f, 100.0f);

}

void update()
{}

void uninitialize()
{
    GLXContext currentGlxContext;
    currentGlxContext = glXGetCurrentContext();

    if (gShaderProgramObject_ak) 
    {
        glUseProgram(gShaderProgramObject_ak);
        GLsizei shaderCount_ak;
        glGetProgramiv(gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

        GLuint* pShaders_ak = NULL;
        pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
        glGetAttachedShaders(gShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);

        for (GLsizei i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
            glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
            glDeleteShader(pShaders_ak[i_ak]);
            pShaders_ak[i_ak] = 0;
        }

        free(pShaders_ak);
        glDeleteProgram(gShaderProgramObject_ak);
        gShaderProgramObject_ak = 0;

        glUseProgram(0);
    }

    if(currentGlxContext != NULL && currentGlxContext == gGlxContext_ak)
	{
		glXMakeCurrent(gpDisplay_ak, 0, 0);
	}

    if(gGlxContext_ak)
	{
		glXDestroyContext(gpDisplay_ak, gGlxContext_ak);
	}

    if (gWindow_ak)
    {
        XDestroyWindow(gpDisplay_ak, gWindow_ak);
    }

    if (gColormap_ak)
    {
        XFreeColormap(gpDisplay_ak, gColormap_ak);
    }

    if (gpXVisualInfo_ak)
    {
        free(gpXVisualInfo_ak);
        gpXVisualInfo_ak = NULL;
    }

    if (gpDisplay_ak)
    {
        XCloseDisplay(gpDisplay_ak);
        gpDisplay_ak = NULL;
    }
	    if(gpFile_ak)
    {
		fprintf(gpFile_ak, "Log file is successfully closed.\n");
		fclose(gpFile_ak);
		gpFile_ak = NULL;
    }
}
