package com.example.two2dshapesrotation;

import android.content.Context;
import android.opengl.GLSurfaceView; 
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{

    private final Context context_ak;

    private GestureDetector gestureDetector;

    private int vertexShaderObject_ak;
    private int fragmentShaderObject_ak;
    private int shaderProgramObject_ak;

    private int[] vaoTriangle_ak = new int[1];
    private int[] vboPositionTriangle_ak = new int[1];
    private int[] vboColorTriangle_ak = new int[1];

    private int[] vaoSquare_ak = new int[1];
    private int[] vboPositionSquare_ak = new int[1];
    private int[] vboColorSquare_ak = new int[1];

    private int mvpUniform_ak;

    private float perspectiveProjectMatrix_ak[] = new float[16];

    private float angleTriangle = 0.0f;
    private float angleSquare = 0.0f;

    public GLESView(Context drawingContext){
        
        super(drawingContext);
        context_ak = drawingContext;
        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        gestureDetector = new GestureDetector (context_ak, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config){
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("ASK: OpenGL-ES:- "+version);
        String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("ASK: GLSL Version:- "+glslVersion);

        initialize(gl);
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height){
        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused){
        draw();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        int eventAction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);
        return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent e){
        System.out.println("ASK: "+"Double Tap");
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e){
        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e){
        System.out.println("ASK: "+"Single Tap");
        return true;
    }

    @Override
    public boolean onDown(MotionEvent e){
        return true;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float veloxityX, float veloxityY){
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e){
        System.out.println("ASK: "+"Long Press");
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY){
        System.out.println("ASK: "+"Scroll");
        System.exit(0);
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e){

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e){
        return true;
    }

    private void initialize(GL10 gl){
        
        /************************Vertex Shader************************************/

        vertexShaderObject_ak = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
        final String vertexShaderSourceCode = String.format(
        "#version 320 es" +
        "\n"+
        "in vec4 vPosition;" +
        "out vec4 out_color;" +
        "in vec4 vColor;" +
        "uniform mat4 u_mvp_matrix;" +
        "void main()" +
        "{" +
              "gl_Position = u_mvp_matrix * vPosition;" +
              "out_color = vColor;"+
        "}" 
        );

        GLES32.glShaderSource(vertexShaderObject_ak, vertexShaderSourceCode);

        GLES32.glCompileShader(vertexShaderObject_ak);
        int[] iShaderCompiledStatus = new int[1];
        int[] infoLogLength = new int[1];
        String szInfoLog = null;
        GLES32.glGetShaderiv(vertexShaderObject_ak, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
        if(iShaderCompiledStatus[0] == GLES32.GL_FALSE){
            GLES32.glGetShaderiv(vertexShaderObject_ak, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);
            if(infoLogLength[0] > 0){
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject_ak);
                System.out.println("ASK: Vertex Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        /************************Fragment Shader************************************/

        fragmentShaderObject_ak = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        final String fragmentShaderSourceCode =  String.format(
        "#version 320 es" +
        "\n" +
        "precision highp float;" +
        "out vec4 FragColor;" +
        "in vec4 out_color;" +
        "void main()"+
        "{" +
            "FragColor = out_color;" +
        "}"
        );

        GLES32.glShaderSource(fragmentShaderObject_ak, fragmentShaderSourceCode);

        GLES32.glCompileShader(fragmentShaderObject_ak);
        iShaderCompiledStatus[0] = 0;
        infoLogLength[0] = 0;
        szInfoLog = null;
        GLES32.glGetShaderiv(fragmentShaderObject_ak, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
        if(iShaderCompiledStatus[0] == GLES32.GL_FALSE){
            GLES32.glGetShaderiv(fragmentShaderObject_ak, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
            if(infoLogLength[0] > 0){
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject_ak);
                System.out.println("ASK: Fragment Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        shaderProgramObject_ak = GLES32.glCreateProgram();

        GLES32.glAttachShader(shaderProgramObject_ak, vertexShaderObject_ak);
        GLES32.glAttachShader(shaderProgramObject_ak, fragmentShaderObject_ak);

        GLES32.glBindAttribLocation(shaderProgramObject_ak, GLESMacros.ATTRIBUTE_VERTEX, "vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject_ak, GLESMacros.ATTRIBUTE_COLOR, "vColor");

        GLES32.glLinkProgram(shaderProgramObject_ak);

        int[] iShaderProgramLinkStatus = new int[1];
        infoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetProgramiv(shaderProgramObject_ak, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
        if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE){
            GLES32.glGetProgramiv(shaderProgramObject_ak, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);
            if(infoLogLength[0] > 0){
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject_ak);
                System.out.println("ASK: Shader Program Link Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
        }

      }

      mvpUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_mvp_matrix");

      final float triangleVertices[] = new float[]{
        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 0.0f,
        1.0f, -1.0f, 0.0f
      };

      final float triangleColors[] = new float[]{
        1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f
      };

      final float squareVertices[] = new float[]{
		1.0f, 1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f
      };

      final float squareColors[] = new float[]{
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f
      };

      //Triangle Starts
      GLES32.glGenVertexArrays(1, vaoTriangle_ak, 0);
      GLES32.glBindVertexArray(vaoTriangle_ak[0]);

      GLES32.glGenBuffers(1,vboPositionTriangle_ak,0);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPositionTriangle_ak[0]);

      ByteBuffer byteBufferPositionTriangle = ByteBuffer.allocateDirect(triangleVertices.length * 4);
      byteBufferPositionTriangle.order(ByteOrder.nativeOrder());
      FloatBuffer verticesBufferTriangle = byteBufferPositionTriangle.asFloatBuffer();
      verticesBufferTriangle.put(triangleVertices);
      verticesBufferTriangle.position(0);

      GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, triangleVertices.length * 4, verticesBufferTriangle, GLES32.GL_STATIC_DRAW);
      GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_VERTEX, 3, GLES32.GL_FLOAT, false, 0, 0);
      GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_VERTEX);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

      GLES32.glGenBuffers(1,vboColorTriangle_ak,0);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColorTriangle_ak[0]);

      ByteBuffer byteBufferColorTriangle = ByteBuffer.allocateDirect(triangleColors.length * 4);
      byteBufferColorTriangle.order(ByteOrder.nativeOrder());
      FloatBuffer colorBufferTriangle = byteBufferColorTriangle.asFloatBuffer();
      colorBufferTriangle.put(triangleColors);
      colorBufferTriangle.position(0);

      GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, triangleColors.length * 4, colorBufferTriangle, GLES32.GL_STATIC_DRAW);
      GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
      GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_COLOR);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
      GLES32.glBindVertexArray(0);
      //Triangle Ends

      //Square Starts
      GLES32.glGenVertexArrays(1, vaoSquare_ak, 0);
      GLES32.glBindVertexArray(vaoSquare_ak[0]);

      GLES32.glGenBuffers(1,vboPositionSquare_ak,0);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPositionSquare_ak[0]);

      ByteBuffer byteBufferPositionSquare = ByteBuffer.allocateDirect(squareVertices.length * 4);
      byteBufferPositionSquare.order(ByteOrder.nativeOrder());
      FloatBuffer verticesBufferSquare = byteBufferPositionSquare.asFloatBuffer();
      verticesBufferSquare.put(squareVertices);
      verticesBufferSquare.position(0);

      GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, squareVertices.length * 4, verticesBufferSquare, GLES32.GL_STATIC_DRAW);
      GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_VERTEX, 3, GLES32.GL_FLOAT, false, 0, 0);
      GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_VERTEX);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

      GLES32.glGenBuffers(1,vboColorSquare_ak,0);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColorSquare_ak[0]);

      ByteBuffer byteBufferColorSquare = ByteBuffer.allocateDirect(squareColors.length * 4);
      byteBufferColorSquare.order(ByteOrder.nativeOrder());
      FloatBuffer colorBufferSquare = byteBufferColorSquare.asFloatBuffer();
      colorBufferSquare.put(squareColors);
      colorBufferSquare.position(0);

      GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, squareColors.length * 4, colorBufferSquare, GLES32.GL_STATIC_DRAW);
      GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
      GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_COLOR);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
      GLES32.glBindVertexArray(0);
      //Square Ends


      GLES32.glEnable(GLES32.GL_DEPTH_TEST);
      GLES32.glDepthFunc(GLES32.GL_LEQUAL);
    
      GLES32.glClearColor(0.0f,0.0f,0.0f,0.0f);

      Matrix.setIdentityM(perspectiveProjectMatrix_ak,0);
    }

    private void resize(int width, int height){
        GLES32.glViewport(0,0,width,height);
        Matrix.perspectiveM(perspectiveProjectMatrix_ak, 0, 45.0f, (float)width/height, 0.1f, 100.f);

    }

    public void draw(){
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject_ak);

        float modelViewMatrix_ak[] = new float[16];
        float modelViewProjectionMatrix_ak[] = new float[16];
        float rotationMatrix_ak[] = new float[16];

        Matrix.setIdentityM(modelViewMatrix_ak, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix_ak, 0);
        Matrix.setIdentityM(rotationMatrix_ak, 0);


        Matrix.translateM(modelViewMatrix_ak, 0, -1.5f, 0.0f, -6.0f);
        Matrix.rotateM(rotationMatrix_ak, 0, angleTriangle, 0.0f, 1.0f, 0.0f);
        Matrix.multiplyMM(modelViewMatrix_ak, 0, modelViewMatrix_ak, 0, rotationMatrix_ak, 0);
        Matrix.multiplyMM(modelViewProjectionMatrix_ak, 0, perspectiveProjectMatrix_ak, 0, modelViewMatrix_ak, 0);
        GLES32.glUniformMatrix4fv(mvpUniform_ak, 1, false, modelViewProjectionMatrix_ak, 0);
        
        GLES32.glBindVertexArray(vaoTriangle_ak[0]);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 3);
        GLES32.glBindVertexArray(0);

        Matrix.setIdentityM(modelViewMatrix_ak, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix_ak, 0);
        Matrix.setIdentityM(rotationMatrix_ak, 0);
     
        Matrix.translateM(modelViewMatrix_ak, 0, 1.5f, 0.0f, -6.0f);
        Matrix.rotateM(rotationMatrix_ak, 0, angleSquare, 1.0f, 0.0f, 0.0f);
        Matrix.multiplyMM(modelViewMatrix_ak, 0, modelViewMatrix_ak, 0, rotationMatrix_ak, 0);
        Matrix.multiplyMM(modelViewProjectionMatrix_ak, 0, perspectiveProjectMatrix_ak, 0, modelViewMatrix_ak, 0);
        GLES32.glUniformMatrix4fv(mvpUniform_ak, 1, false, modelViewProjectionMatrix_ak, 0);
        
        GLES32.glBindVertexArray(vaoSquare_ak[0]);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
        GLES32.glBindVertexArray(0);

        GLES32.glUseProgram(0);

        update();

        requestRender();
    }

    private void update(){
        angleTriangle +=0.5f;
        if(angleTriangle >=360)
            angleTriangle = 0.0f;

        angleSquare +=0.5f;
        if(angleSquare >=360)
            angleSquare = 0.0f;
    }

    void uninitialize(){
        if(vaoTriangle_ak[0] != 0){
            GLES32.glDeleteVertexArrays(1, vaoTriangle_ak, 0);
            vaoTriangle_ak[0] = 0;
        }

        if(vboPositionTriangle_ak[0] != 0){
            GLES32.glDeleteVertexArrays(1, vboPositionTriangle_ak, 0);
            vboPositionTriangle_ak[0] = 0;
        }

        if(vboColorTriangle_ak[0] != 0){
            GLES32.glDeleteVertexArrays(1, vboColorTriangle_ak, 0);
            vboColorTriangle_ak[0] = 0;
        }

        if(vboPositionSquare_ak[0] != 0){
            GLES32.glDeleteVertexArrays(1, vboPositionSquare_ak, 0);
            vboPositionSquare_ak[0] = 0;
        }

        if(vboColorSquare_ak[0] != 0){
            GLES32.glDeleteVertexArrays(1, vboColorSquare_ak, 0);
            vboColorSquare_ak[0] = 0;
        }

        if(shaderProgramObject_ak != 0){
            if(vertexShaderObject_ak != 0){
                GLES32.glDetachShader(shaderProgramObject_ak, vertexShaderObject_ak);
                GLES32.glDeleteShader(vertexShaderObject_ak);
                vertexShaderObject_ak = 0;
            }

            if(fragmentShaderObject_ak != 0){
                GLES32.glDetachShader(shaderProgramObject_ak, fragmentShaderObject_ak);
                GLES32.glDeleteShader(fragmentShaderObject_ak);
                fragmentShaderObject_ak = 0;
            }
        }

        if(shaderProgramObject_ak != 0){
            GLES32.glDeleteProgram(shaderProgramObject_ak);
            shaderProgramObject_ak = 0;
        }

    }
}