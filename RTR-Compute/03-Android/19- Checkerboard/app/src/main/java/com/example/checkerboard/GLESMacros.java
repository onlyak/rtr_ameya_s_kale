package com.example.checkerboard;

public class GLESMacros{
    public static final int ATTRIBUTE_VERTEX = 0;
    public static final int ATTRIBUTE_COLOR = 1;
    public static final int ATTRIBUTE_NORMAL = 2;
    public static final int ATTRIBUTE_TEXTURE0 = 3;
}