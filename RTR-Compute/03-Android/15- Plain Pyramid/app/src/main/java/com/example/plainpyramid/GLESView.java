package com.example.plainpyramid;

import android.content.Context;
import android.opengl.GLSurfaceView; 
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{

    private final Context context_ak;

    private GestureDetector gestureDetector;

    private int vertexShaderObject_ak;
    private int fragmentShaderObject_ak;
    private int shaderProgramObject_ak;

    private int[] vaoPyramid_ak = new int[1];
    private int[] vboPositionPyramid_ak = new int[1];
    private int[] vboColorPyramid_ak = new int[1];

    private int mvpUniform_ak;

    private float perspectiveProjectMatrix_ak[] = new float[16];

    private float anglePyramid = 0.0f;

    public GLESView(Context drawingContext){
        
        super(drawingContext);
        context_ak = drawingContext;
        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        gestureDetector = new GestureDetector (context_ak, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config){
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("ASK: OpenGL-ES:- "+version);
        String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("ASK: GLSL Version:- "+glslVersion);

        initialize(gl);
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height){
        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused){
        draw();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        int eventAction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);
        return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent e){
        System.out.println("ASK: "+"Double Tap");
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e){
        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e){
        System.out.println("ASK: "+"Single Tap");
        return true;
    }

    @Override
    public boolean onDown(MotionEvent e){
        return true;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float veloxityX, float veloxityY){
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e){
        System.out.println("ASK: "+"Long Press");
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY){
        System.out.println("ASK: "+"Scroll");
        System.exit(0);
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e){

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e){
        return true;
    }

    private void initialize(GL10 gl){
        
        /************************Vertex Shader************************************/

        vertexShaderObject_ak = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
        final String vertexShaderSourceCode = String.format(
        "#version 320 es" +
        "\n"+
        "in vec4 vPosition;" +
        "out vec4 out_color;" +
        "in vec4 vColor;" +
        "uniform mat4 u_mvp_matrix;" +
        "void main()" +
        "{" +
              "gl_Position = u_mvp_matrix * vPosition;" +
              "out_color = vColor;"+
        "}" 
        );

        GLES32.glShaderSource(vertexShaderObject_ak, vertexShaderSourceCode);

        GLES32.glCompileShader(vertexShaderObject_ak);
        int[] iShaderCompiledStatus = new int[1];
        int[] infoLogLength = new int[1];
        String szInfoLog = null;
        GLES32.glGetShaderiv(vertexShaderObject_ak, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
        if(iShaderCompiledStatus[0] == GLES32.GL_FALSE){
            GLES32.glGetShaderiv(vertexShaderObject_ak, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);
            if(infoLogLength[0] > 0){
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject_ak);
                System.out.println("ASK: Vertex Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        /************************Fragment Shader************************************/

        fragmentShaderObject_ak = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        final String fragmentShaderSourceCode =  String.format(
        "#version 320 es" +
        "\n" +
        "precision highp float;" +
        "out vec4 FragColor;" +
        "in vec4 out_color;" +
        "void main()"+
        "{" +
            "FragColor = out_color;" +
        "}"
        );

        GLES32.glShaderSource(fragmentShaderObject_ak, fragmentShaderSourceCode);

        GLES32.glCompileShader(fragmentShaderObject_ak);
        iShaderCompiledStatus[0] = 0;
        infoLogLength[0] = 0;
        szInfoLog = null;
        GLES32.glGetShaderiv(fragmentShaderObject_ak, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
        if(iShaderCompiledStatus[0] == GLES32.GL_FALSE){
            GLES32.glGetShaderiv(fragmentShaderObject_ak, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
            if(infoLogLength[0] > 0){
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject_ak);
                System.out.println("ASK: Fragment Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        shaderProgramObject_ak = GLES32.glCreateProgram();

        GLES32.glAttachShader(shaderProgramObject_ak, vertexShaderObject_ak);
        GLES32.glAttachShader(shaderProgramObject_ak, fragmentShaderObject_ak);

        GLES32.glBindAttribLocation(shaderProgramObject_ak, GLESMacros.ATTRIBUTE_VERTEX, "vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject_ak, GLESMacros.ATTRIBUTE_COLOR, "vColor");

        GLES32.glLinkProgram(shaderProgramObject_ak);

        int[] iShaderProgramLinkStatus = new int[1];
        infoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetProgramiv(shaderProgramObject_ak, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
        if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE){
            GLES32.glGetProgramiv(shaderProgramObject_ak, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);
            if(infoLogLength[0] > 0){
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject_ak);
                System.out.println("ASK: Shader Program Link Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
        }

      }

      mvpUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_mvp_matrix");

      final float pyramidVertices[] = new float[]{
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,

		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,

		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f
      };

      final float pyramidColors[] = new float[]{
        1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

        1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

        1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,

        1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f
      };

      //Pyramid Starts
      GLES32.glGenVertexArrays(1, vaoPyramid_ak, 0);
      GLES32.glBindVertexArray(vaoPyramid_ak[0]);

      GLES32.glGenBuffers(1,vboPositionPyramid_ak,0);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPositionPyramid_ak[0]);

      ByteBuffer byteBufferPositionPyramid = ByteBuffer.allocateDirect(pyramidVertices.length * 4);
      byteBufferPositionPyramid.order(ByteOrder.nativeOrder());
      FloatBuffer verticesBufferPyramid = byteBufferPositionPyramid.asFloatBuffer();
      verticesBufferPyramid.put(pyramidVertices);
      verticesBufferPyramid.position(0);

      GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidVertices.length * 4, verticesBufferPyramid, GLES32.GL_STATIC_DRAW);
      GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_VERTEX, 3, GLES32.GL_FLOAT, false, 0, 0);
      GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_VERTEX);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

      GLES32.glGenBuffers(1,vboColorPyramid_ak,0);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColorPyramid_ak[0]);

      ByteBuffer byteBufferColorPyramid = ByteBuffer.allocateDirect(pyramidColors.length * 4);
      byteBufferColorPyramid.order(ByteOrder.nativeOrder());
      FloatBuffer colorBufferPyramid = byteBufferColorPyramid.asFloatBuffer();
      colorBufferPyramid.put(pyramidColors);
      colorBufferPyramid.position(0);

      GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidColors.length * 4, colorBufferPyramid, GLES32.GL_STATIC_DRAW);
      GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
      GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_COLOR);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
      GLES32.glBindVertexArray(0);
      //Pyramid Ends

      GLES32.glEnable(GLES32.GL_DEPTH_TEST);
      GLES32.glDepthFunc(GLES32.GL_LEQUAL);
      GLES32.glClearDepthf(1.0f);
    
      GLES32.glClearColor(0.0f,0.0f,0.0f,0.0f);

      Matrix.setIdentityM(perspectiveProjectMatrix_ak,0);
    }

    private void resize(int width, int height){
        GLES32.glViewport(0,0,width,height);
        Matrix.perspectiveM(perspectiveProjectMatrix_ak, 0, 45.0f, (float)width/height, 0.1f, 100.f);

    }

    public void draw(){
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject_ak);

        float modelViewMatrix_ak[] = new float[16];
        float modelViewProjectionMatrix_ak[] = new float[16];
        float rotationMatrix_ak[] = new float[16];

        Matrix.setIdentityM(modelViewMatrix_ak, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix_ak, 0);
        Matrix.setIdentityM(rotationMatrix_ak, 0);


        Matrix.translateM(modelViewMatrix_ak, 0, 0f, 0.0f, -6.0f);
        Matrix.rotateM(rotationMatrix_ak, 0, anglePyramid, 0.0f, 1.0f, 0.0f);
        Matrix.multiplyMM(modelViewMatrix_ak, 0, modelViewMatrix_ak, 0, rotationMatrix_ak, 0);
        Matrix.multiplyMM(modelViewProjectionMatrix_ak, 0, perspectiveProjectMatrix_ak, 0, modelViewMatrix_ak, 0);
        GLES32.glUniformMatrix4fv(mvpUniform_ak, 1, false, modelViewProjectionMatrix_ak, 0);
        
        GLES32.glBindVertexArray(vaoPyramid_ak[0]);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12);
        GLES32.glBindVertexArray(0);

        GLES32.glUseProgram(0);

        update();

        requestRender();
    }

    private void update(){
        anglePyramid +=0.5f;
        if(anglePyramid >=360)
            anglePyramid = 0.0f;

    }

    void uninitialize(){
        if(vaoPyramid_ak[0] != 0){
            GLES32.glDeleteVertexArrays(1, vaoPyramid_ak, 0);
            vaoPyramid_ak[0] = 0;
        }

        if(vboPositionPyramid_ak[0] != 0){
            GLES32.glDeleteVertexArrays(1, vboPositionPyramid_ak, 0);
            vboPositionPyramid_ak[0] = 0;
        }

        if(vboColorPyramid_ak[0] != 0){
            GLES32.glDeleteVertexArrays(1, vboColorPyramid_ak, 0);
            vboColorPyramid_ak[0] = 0;
        }

        if(shaderProgramObject_ak != 0){
            if(vertexShaderObject_ak != 0){
                GLES32.glDetachShader(shaderProgramObject_ak, vertexShaderObject_ak);
                GLES32.glDeleteShader(vertexShaderObject_ak);
                vertexShaderObject_ak = 0;
            }

            if(fragmentShaderObject_ak != 0){
                GLES32.glDetachShader(shaderProgramObject_ak, fragmentShaderObject_ak);
                GLES32.glDeleteShader(fragmentShaderObject_ak);
                fragmentShaderObject_ak = 0;
            }
        }

        if(shaderProgramObject_ak != 0){
            GLES32.glDeleteProgram(shaderProgramObject_ak);
            shaderProgramObject_ak = 0;
        }

    }
}