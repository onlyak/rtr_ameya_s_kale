
package com.example.solarsystem;

import android.content.Context;
import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener {
	private final Context context_ak;
	private GestureDetector gestureDetector_ak;

	private int vertexShaderObject_ak;
	private int fragmentShaderObject_ak;
	private int shaderProgramObject_ak;
	private int colorUniform_ak;

	private int modelViewMatrixUniform_ak;
	private int viewMatrixUniform_ak;
	private int projectionMatrixUniform_ak;

	private int lKeyPressedUniform_ak;

	private int laUniform_ak;
	private int ldUniform_ak;
	private int lsUniform_ak;
	private int lightPositionUniform_ak;

	private int kaUniform_ak;
	private int kdUniform_ak;
	private int ksUniform_ak;
	private int materialShininessUniform_ak;

	private boolean rotationDirection_ak = false;
	private int objectToBeHandled_ak = 0;
	
	private int day_ak = 0;
	private int year_ak = 0;
	private int hour_ak = 0;


	private float perspectiveProjectionMatrix_ak[] = new float[16];

	private int[] vao_sphere_ak = new int[1];
	private int[] vbo_position_sphere_ak = new int[1];
	private int[] vbo_normal_sphere_ak = new int[1];
	private int[] vbo_element_sphere_ak = new int[1];

	private float sphere_vertices_ak[] = new float[1146];
	private float sphere_normals_ak[] = new float[1146];
	private float sphere_textures_ak[] = new float[764];
	private short sphere_elements_ak[] = new short[2280];

	private int numVertices_ak = 0;
	private int numElements_ak = 0;

	GLESView(Context drawingContext_ak) {
		super(drawingContext_ak);
		context_ak = drawingContext_ak;
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		gestureDetector_ak = new GestureDetector(context_ak, this, null, false);
		gestureDetector_ak.setOnDoubleTapListener(this);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		String glesVersion_ak = gl.glGetString(GL10.GL_VERSION);
		String glslVersion_ak = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		initialize(gl);
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height) {
		resize(width, height);
	}

	@Override
	public void onDrawFrame(GL10 unused) {
		display();
		update();
	}

	@Override
	public boolean onTouchEvent(MotionEvent e) {
		int eventAction_ak = e.getAction();
		if (!gestureDetector_ak.onTouchEvent(e)) {
			super.onTouchEvent(e);
		}
		return (true);
	}

	@Override
	public boolean onDoubleTap(MotionEvent e) {
		if (objectToBeHandled_ak == 2) {
			objectToBeHandled_ak = 0;
		} else {
			objectToBeHandled_ak += 1;
		}
		return (true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e) {
		return (true);
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e) {
		if(rotationDirection_ak == true) {
			rotationDirection_ak = false;
		} else {
			rotationDirection_ak = true;
		}
		return (true);
	}

	@Override
	public boolean onDown(MotionEvent e) {
		return (true);
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
		return (true);
	}

	@Override
	public void onLongPress(MotionEvent e) {
		if(objectToBeHandled_ak == 0) {
			if (rotationDirection_ak == true) {
				hour_ak = (hour_ak + 1) % 360;
			} else {
				hour_ak = (hour_ak - 1) % 360;
			}
		}
		if(objectToBeHandled_ak == 1) {
			if (rotationDirection_ak == true) {
				day_ak = (day_ak + 6) % 360;
				hour_ak = (hour_ak + 1) % 360;
			} else {
				day_ak = (day_ak - 6) % 360;
				hour_ak = (hour_ak - 1) % 360;
			}
		}
		if(objectToBeHandled_ak == 2) {
			if (rotationDirection_ak == true) {
				year_ak = (year_ak + 3) % 360;
				day_ak = (day_ak - 6) % 360;
				hour_ak = (hour_ak + 1) % 360;
			} else {
				year_ak = (year_ak - 3) % 360;
				day_ak = (day_ak + 6) % 360;
				hour_ak = (hour_ak - 1) % 360;
			}
		}
		
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
		uninitialize();
		System.exit(0);
		return (true);
	}

	@Override
	public void onShowPress(MotionEvent e) {
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		return (true);
	}

	private void initialize(GL10 gl) {
		vertexShaderObject_ak = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		final String vertexShaderSourceCode_ak = String.format(
			"#version 320 es" +
			"\n" +
			"in vec4 vPosition;" +
			"uniform mat4 u_mvp_matrix;" +
			"void main(void)" +
			"{" +
			"gl_Position = u_mvp_matrix * vPosition;" +
			"}"
		);

		GLES32.glShaderSource(vertexShaderObject_ak, vertexShaderSourceCode_ak);

		GLES32.glCompileShader(vertexShaderObject_ak);
		int[] iShaderCompileStatus_ak = new int[1];
		int[] iInfoLogLength_ak = new int[1];
		String szInfoLog_ak = null;
		GLES32.glGetShaderiv(vertexShaderObject_ak, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus_ak, 0);
		if (iShaderCompileStatus_ak[0] == GLES32.GL_FALSE) {
			GLES32.glGetShaderiv(vertexShaderObject_ak, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength_ak, 0);
			if (iInfoLogLength_ak[0] > 0) {
				szInfoLog_ak = GLES32.glGetShaderInfoLog(vertexShaderObject_ak);
				uninitialize();
				System.exit(0);
			}
		}
		fragmentShaderObject_ak = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		final String fragementShaderSourceCode_ak = String.format(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			"precision lowp int;" +
			"uniform vec3 u_color;" +
			"out vec4 FragColor;" +
			"void main(void)" +
			"{" +
			"FragColor = vec4(u_color, 1.0);" +
			"}"
		);

		GLES32.glShaderSource(fragmentShaderObject_ak, fragementShaderSourceCode_ak);

		GLES32.glCompileShader(fragmentShaderObject_ak);
		iShaderCompileStatus_ak[0] = 0;
		iInfoLogLength_ak[0] = 0;
		szInfoLog_ak = null;

		GLES32.glGetShaderiv(fragmentShaderObject_ak, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus_ak, 0);
		if (iShaderCompileStatus_ak[0] == GLES32.GL_FALSE) {
			GLES32.glGetShaderiv(fragmentShaderObject_ak, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength_ak, 0);
			if (iInfoLogLength_ak[0] > 0) {
				szInfoLog_ak = GLES32.glGetShaderInfoLog(fragmentShaderObject_ak);
				uninitialize();
				System.exit(0);
			}
		}

		shaderProgramObject_ak = GLES32.glCreateProgram();

		GLES32.glAttachShader(shaderProgramObject_ak, vertexShaderObject_ak);
		GLES32.glAttachShader(shaderProgramObject_ak, fragmentShaderObject_ak);

		GLES32.glBindAttribLocation(shaderProgramObject_ak, GLESMacros.ATTRIBUTE_POSITION, "vPosition");
		GLES32.glLinkProgram(shaderProgramObject_ak);

		int[] iShaderProgramLinkStatus_ak = new int[1];
		iInfoLogLength_ak[0] = 0;
		szInfoLog_ak = null;

		GLES32.glGetProgramiv(shaderProgramObject_ak, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus_ak, 0);
		if (iShaderProgramLinkStatus_ak[0] == GLES32.GL_FALSE) {
			GLES32.glGetProgramiv(shaderProgramObject_ak, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength_ak, 0);
			if (iInfoLogLength_ak[0] > 0) {
				szInfoLog_ak = GLES32.glGetProgramInfoLog(shaderProgramObject_ak);
				uninitialize();
				System.exit(0);
			}
		}
		modelViewMatrixUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_mvp_matrix");
		colorUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_color");

		Sphere sphere_ak = new Sphere();

		sphere_ak.getSphereVertexData(sphere_vertices_ak, sphere_normals_ak, sphere_textures_ak, sphere_elements_ak);
		numVertices_ak = sphere_ak.getNumberOfSphereVertices();
		numElements_ak = sphere_ak.getNumberOfSphereElements();

		GLES32.glGenVertexArrays(1, vao_sphere_ak, 0);
		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glGenBuffers(1, vbo_position_sphere_ak, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_sphere_ak[0]);

		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_vertices_ak.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(sphere_vertices_ak);
		verticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_vertices_ak.length * 4, verticesBuffer, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);

		GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_POSITION);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glGenBuffers(1, vbo_normal_sphere_ak, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_normal_sphere_ak[0]);

		byteBuffer = ByteBuffer.allocateDirect(sphere_normals_ak.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer normalBuffer = byteBuffer.asFloatBuffer();
		normalBuffer = byteBuffer.asFloatBuffer();
		normalBuffer.put(sphere_normals_ak);
		normalBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_normals_ak.length * 4, normalBuffer, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 0, 0);

		GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_NORMAL);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glGenBuffers(1, vbo_element_sphere_ak, 0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);

		byteBuffer = ByteBuffer.allocateDirect(sphere_elements_ak.length * 2);
		byteBuffer.order(ByteOrder.nativeOrder());
		ShortBuffer elementsBuffer = byteBuffer.asShortBuffer();
		elementsBuffer.put(sphere_elements_ak);
		elementsBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER, sphere_elements_ak.length * 2, elementsBuffer, GLES32.GL_STATIC_DRAW);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);

		GLES32.glClearDepthf(1.0f);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		// GLES32.glEnable(GLES32.GL_CULL_FACE);

		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		rotationDirection_ak = false;
		Matrix.setIdentityM(perspectiveProjectionMatrix_ak, 0);

	}

	private void resize(int width, int height) {
		GLES32.glViewport(0, 0, width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix_ak, 0, 45.0f, (float) width / height, 0.1f, 100.f);
	}

	private void display() {
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		GLES32.glUseProgram(shaderProgramObject_ak);

		float modelViewMatrix1_ak[] = new float[16];
		float modelViewMatrix2_ak[] = new float[16];
		float modelViewMatrix3_ak[] = new float[16];
		float modelViewProjectionMatrix_ak[] = new float[16];
		float translationMatrix_ak[] = new float[16];
		float xRotationMatrix_ak[] = new float[16];
		float yRotationMatrix_ak[] = new float[16];
		float zRotationMatrix_ak[] = new float[16];
		float scaleMatrix_ak[] = new float[16];


		Matrix.setIdentityM(modelViewMatrix1_ak, 0);
		Matrix.setIdentityM(modelViewMatrix2_ak, 0);
		Matrix.setIdentityM(modelViewMatrix3_ak, 0);
		Matrix.setIdentityM(translationMatrix_ak, 0);
		Matrix.setIdentityM(xRotationMatrix_ak, 0);
		Matrix.setIdentityM(yRotationMatrix_ak, 0);
		Matrix.setIdentityM(zRotationMatrix_ak, 0);
		Matrix.setIdentityM(scaleMatrix_ak, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix_ak, 0);

		// GLES32.glPolygonMode(GLES32.GL_FRONT_AND_BACK, GLES32.GL_FILL);
		

		Matrix.translateM(translationMatrix_ak, 0, 0.0f, 0.0f, -5.0f);
		Matrix.multiplyMM(modelViewMatrix1_ak, 0, modelViewMatrix1_ak, 0, translationMatrix_ak, 0);
		Matrix.multiplyMM(modelViewProjectionMatrix_ak, 0, perspectiveProjectionMatrix_ak, 0, modelViewMatrix1_ak, 0);
		GLES32.glUniformMatrix4fv(modelViewMatrixUniform_ak, 1, false, modelViewProjectionMatrix_ak, 0);
		
	
		Matrix.setIdentityM(translationMatrix_ak, 0);
		Matrix.setIdentityM(xRotationMatrix_ak, 0);
		Matrix.setIdentityM(yRotationMatrix_ak, 0);
		Matrix.setIdentityM(zRotationMatrix_ak, 0);
		Matrix.setIdentityM(scaleMatrix_ak, 0);
		
		float sunColor_ak[] = new float[]{ 1.0f, 1.0f, 0.0f };
		GLES32.glUniform3fv(colorUniform_ak, 1, sunColor_ak, 0);
		GLES32.glBindVertexArray(vao_sphere_ak[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);
		GLES32.glBindVertexArray(0);

		// earth 
		Matrix.setRotateM(yRotationMatrix_ak, 0, year_ak, 0.0f, 1.0f, 0.0f);
		Matrix.translateM(translationMatrix_ak, 0, 1.7f, 0.0f, 0.0f);
		Matrix.multiplyMM(modelViewMatrix2_ak , 0, modelViewMatrix1_ak , 0, yRotationMatrix_ak, 0);
		Matrix.multiplyMM(modelViewMatrix2_ak , 0, modelViewMatrix2_ak , 0, translationMatrix_ak, 0);

		Matrix.setRotateM(yRotationMatrix_ak, 0, day_ak, 0.0f, 1.0f, 0.0f);
		Matrix.scaleM(scaleMatrix_ak, 0, 0.5f, 0.5f, 0.5f);
		Matrix.multiplyMM(modelViewMatrix3_ak , 0, modelViewMatrix2_ak , 0, yRotationMatrix_ak, 0);
		Matrix.multiplyMM(modelViewMatrix3_ak , 0, modelViewMatrix3_ak , 0, scaleMatrix_ak, 0);

		Matrix.multiplyMM(modelViewProjectionMatrix_ak, 0, perspectiveProjectionMatrix_ak, 0, modelViewMatrix3_ak, 0);
		GLES32.glUniformMatrix4fv(modelViewMatrixUniform_ak, 1, false, modelViewProjectionMatrix_ak, 0);
		// GLES32.glPolygonMode(GLES32.GL_FRONT_AND_BACK, GLES32.GL_LINE);

		float earthColor_ak[] = new float[]{ 0.4f, 0.9f, 1.0f };
		GLES32.glUniform3fv(colorUniform_ak, 1, earthColor_ak, 0);

		GLES32.glBindVertexArray(vao_sphere_ak[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(translationMatrix_ak, 0);
		Matrix.setIdentityM(xRotationMatrix_ak, 0);
		Matrix.setIdentityM(yRotationMatrix_ak, 0);
		Matrix.setIdentityM(zRotationMatrix_ak, 0);
		Matrix.setIdentityM(scaleMatrix_ak, 0);
		// moon
		Matrix.setRotateM(yRotationMatrix_ak, 0, day_ak, 0.0f, 1.0f, 0.0f);
		Matrix.translateM(translationMatrix_ak, 0, 0.7f, 0.0f, 0.0f);

		Matrix.multiplyMM(modelViewMatrix3_ak , 0, modelViewMatrix2_ak , 0, yRotationMatrix_ak, 0);
		Matrix.multiplyMM(modelViewMatrix3_ak , 0, modelViewMatrix3_ak , 0, translationMatrix_ak, 0);

		Matrix.setRotateM(yRotationMatrix_ak, 0, hour_ak, 0.0f, 1.0f, 0.0f);
		Matrix.scaleM(scaleMatrix_ak, 0, 0.3f, 0.3f, 0.3f);
		Matrix.multiplyMM(modelViewMatrix3_ak , 0, modelViewMatrix3_ak , 0, yRotationMatrix_ak, 0);
		Matrix.multiplyMM(modelViewMatrix3_ak , 0, modelViewMatrix3_ak , 0, scaleMatrix_ak, 0);
		Matrix.multiplyMM(modelViewProjectionMatrix_ak, 0, perspectiveProjectionMatrix_ak, 0, modelViewMatrix3_ak, 0);
		GLES32.glUniformMatrix4fv(modelViewMatrixUniform_ak, 1, false, modelViewProjectionMatrix_ak, 0);

		float moonColor_ak[] = new float[]{ 1.0f, 1.0f, 1.0f };
		GLES32.glUniform3fv(colorUniform_ak, 1, moonColor_ak, 0);
		GLES32.glBindVertexArray(vao_sphere_ak[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(translationMatrix_ak, 0);
		Matrix.setIdentityM(xRotationMatrix_ak, 0);
		Matrix.setIdentityM(yRotationMatrix_ak, 0);
		Matrix.setIdentityM(zRotationMatrix_ak, 0);
		Matrix.setIdentityM(scaleMatrix_ak, 0);
		
		Matrix.multiplyMM(modelViewProjectionMatrix_ak, 0, perspectiveProjectionMatrix_ak, 0, modelViewMatrix2_ak, 0);
		GLES32.glUniformMatrix4fv(modelViewMatrixUniform_ak, 1, false, modelViewProjectionMatrix_ak, 0);
		

		GLES32.glUseProgram(0);

		requestRender();
	}

	private void update() {
	}

	private void uninitialize() {

		if (vao_sphere_ak[0] != 0) {
			GLES32.glDeleteVertexArrays(1, vao_sphere_ak, 0);
			vao_sphere_ak[0] = 0;
		}

		if (vbo_position_sphere_ak[0] != 0) {
			GLES32.glDeleteBuffers(1, vbo_position_sphere_ak, 0);
			vbo_position_sphere_ak[0] = 0;
		}

		if (vbo_normal_sphere_ak[0] != 0) {
			GLES32.glDeleteBuffers(1, vbo_normal_sphere_ak, 0);
			vbo_normal_sphere_ak[0] = 0;
		}

		if (vbo_element_sphere_ak[0] != 0) {
			GLES32.glDeleteBuffers(1, vbo_element_sphere_ak, 0);
			vbo_element_sphere_ak[0] = 0;
		}

		if (shaderProgramObject_ak != 0) {

			if (vertexShaderObject_ak != 0) {
				GLES32.glDetachShader(shaderProgramObject_ak, vertexShaderObject_ak);
				GLES32.glDeleteShader(vertexShaderObject_ak);
				vertexShaderObject_ak = 0;
			}

			if (fragmentShaderObject_ak != 0) {
				GLES32.glDetachShader(shaderProgramObject_ak, fragmentShaderObject_ak);
				GLES32.glDeleteShader(fragmentShaderObject_ak);
				fragmentShaderObject_ak = 0;
			}
		}

		if (shaderProgramObject_ak != 0) {
			GLES32.glDeleteProgram(shaderProgramObject_ak);
			shaderProgramObject_ak = 0;
		}
	}
}
