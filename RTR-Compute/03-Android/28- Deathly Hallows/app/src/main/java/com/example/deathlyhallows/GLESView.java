package com.example.deathlyhallows;

import android.content.Context;
import android.opengl.GLSurfaceView; 
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{

    private final Context context_ak;

    private GestureDetector gestureDetector;

    private int vertexShaderObject_ak;
    private int fragmentShaderObject_ak;
    private int shaderProgramObject_ak;

    private int[] vao_ak = new int[1];
    private int[] vboPosition_ak = new int[1];
    private int[] vboColor_ak = new int[1];
    private int mvpUniform_ak;

    private float perspectiveProjectMatrix_ak[] = new float[16];

    private  FloatBuffer verticesBuffer;
    private  FloatBuffer colorBuffer;

    private ByteBuffer byteBufferColor;
    private ByteBuffer byteBuffer;

    private float CRangle_ak = 0.0f;
    private float XCircle_ak = 3.0f;
    private float YCircle_ak = -3.0f;

    private float angle_ak = 0.0f;
	private float Xtriangle_ak = -2.0f;
	private float Ytriangle_ak = -2.0f;
	private float TriangleRotation_ak = 1.0f;

    private float XWand_ak = 0.0f;
    private float YWand_ak = 2.0f;

    public GLESView(Context drawingContext){
        
        super(drawingContext);
        context_ak = drawingContext;
        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        gestureDetector = new GestureDetector (context_ak, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config){
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("ASK: OpenGL-ES:- "+version);
        String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("ASK: GLSL Version:- "+glslVersion);

        initialize(gl);
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height){
        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused){
        draw();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        int eventAction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);
        return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent e){
        System.out.println("ASK: "+"Double Tap");
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e){
        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e){
        System.out.println("ASK: "+"Single Tap");
        return true;
    }

    @Override
    public boolean onDown(MotionEvent e){
        return true;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float veloxityX, float veloxityY){
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e){
        System.out.println("ASK: "+"Long Press");
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY){
        System.out.println("ASK: "+"Scroll");
        System.exit(0);
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e){

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e){
        return true;
    }

    private void initialize(GL10 gl){
        
        /************************Vertex Shader************************************/

        vertexShaderObject_ak = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
        final String vertexShaderSourceCode = String.format(
        "#version 320 es" +
        "\n"+
        "in vec4 vPosition;" +
        "out vec4 out_color;" +
        "in vec4 vColor;" +
        "uniform mat4 u_mvp_matrix;" +
        "void main()" +
        "{" +
              "gl_Position = u_mvp_matrix * vPosition;" +
              "out_color = vColor;"+
        "}" 
        );

        GLES32.glShaderSource(vertexShaderObject_ak, vertexShaderSourceCode);

        GLES32.glCompileShader(vertexShaderObject_ak);
        int[] iShaderCompiledStatus = new int[1];
        int[] infoLogLength = new int[1];
        String szInfoLog = null;
        GLES32.glGetShaderiv(vertexShaderObject_ak, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
        if(iShaderCompiledStatus[0] == GLES32.GL_FALSE){
            GLES32.glGetShaderiv(vertexShaderObject_ak, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);
            if(infoLogLength[0] > 0){
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject_ak);
                System.out.println("ASK: Vertex Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        /************************Fragment Shader************************************/

        fragmentShaderObject_ak = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        final String fragmentShaderSourceCode =  String.format(
        "#version 320 es" +
        "\n" +
        "precision highp float;" +
        "out vec4 FragColor;" +
        "in vec4 out_color;" +
        "void main()"+
        "{" +
            "FragColor = out_color;" +
        "}"
        );

        GLES32.glShaderSource(fragmentShaderObject_ak, fragmentShaderSourceCode);

        GLES32.glCompileShader(fragmentShaderObject_ak);
        iShaderCompiledStatus[0] = 0;
        infoLogLength[0] = 0;
        szInfoLog = null;
        GLES32.glGetShaderiv(fragmentShaderObject_ak, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
        if(iShaderCompiledStatus[0] == GLES32.GL_FALSE){
            GLES32.glGetShaderiv(fragmentShaderObject_ak, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
            if(infoLogLength[0] > 0){
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject_ak);
                System.out.println("ASK: Fragment Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        shaderProgramObject_ak = GLES32.glCreateProgram();

        GLES32.glAttachShader(shaderProgramObject_ak, vertexShaderObject_ak);
        GLES32.glAttachShader(shaderProgramObject_ak, fragmentShaderObject_ak);

        GLES32.glBindAttribLocation(shaderProgramObject_ak, GLESMacros.ATTRIBUTE_VERTEX, "vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject_ak, GLESMacros.ATTRIBUTE_COLOR, "vColor");

        GLES32.glLinkProgram(shaderProgramObject_ak);

        int[] iShaderProgramLinkStatus = new int[1];
        infoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetProgramiv(shaderProgramObject_ak, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
        if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE){
            GLES32.glGetProgramiv(shaderProgramObject_ak, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);
            if(infoLogLength[0] > 0){
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject_ak);
                System.out.println("ASK: Shader Program Link Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
        }

      }

      mvpUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_mvp_matrix");

      final float squareVertices[] = new float[]{
		1.0f, 1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f
      };

      final float squareColors[] = new float[]{
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f
      };


      GLES32.glGenVertexArrays(1, vao_ak, 0);
      GLES32.glBindVertexArray(vao_ak[0]);

      GLES32.glGenBuffers(1,vboPosition_ak,0);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPosition_ak[0]);

      GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 6 * 4, null, GLES32.GL_DYNAMIC_DRAW);
      GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_VERTEX, 3, GLES32.GL_FLOAT, false, 0, 0);
      GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_VERTEX);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

      GLES32.glGenBuffers(1,vboColor_ak,0);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColor_ak[0]);

      GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 6 * 4, null, GLES32.GL_DYNAMIC_DRAW);
      GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false, 0, 0);
      GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_COLOR);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
      GLES32.glBindVertexArray(0);

      GLES32.glEnable(GLES32.GL_DEPTH_TEST);
      GLES32.glDepthFunc(GLES32.GL_LEQUAL);
    
      GLES32.glClearColor(0.25f,0.25f,0.25f,0.0f);

      Matrix.setIdentityM(perspectiveProjectMatrix_ak,0);
    }

    private void resize(int width, int height){
        GLES32.glViewport(0,0,width,height);
        Matrix.perspectiveM(perspectiveProjectMatrix_ak, 0, 45.0f, (float)width/height, 0.1f, 100.f);

    }

    public void draw(){
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject_ak);

        float modelViewMatrix_ak[] = new float[16];
        float modelViewProjectionMatrix_ak[] = new float[16];
        float rotationMatrix_ak[] = new float[16];

        int a = 1;

        Matrix.setIdentityM(modelViewMatrix_ak, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix_ak, 0);
        Matrix.setIdentityM(rotationMatrix_ak, 0);

        Matrix.translateM(modelViewMatrix_ak, 0, Xtriangle_ak, Ytriangle_ak, -2.5f);
        Matrix.rotateM(rotationMatrix_ak, 0, angle_ak, 0.0f, 1.0f, 0.0f);
        Matrix.multiplyMM(modelViewMatrix_ak, 0, modelViewMatrix_ak, 0, rotationMatrix_ak, 0);

        Matrix.multiplyMM(modelViewProjectionMatrix_ak, 0, perspectiveProjectMatrix_ak, 0, modelViewMatrix_ak, 0);

        GLES32.glUniformMatrix4fv(mvpUniform_ak, 1, false, modelViewProjectionMatrix_ak, 0);

        GLES32.glBindVertexArray(vao_ak[0]);

        triangle(0.5f);

        angle_ak += 1.0f;

        if (Xtriangle_ak <= 0.0f)
        {
            Xtriangle_ak = Xtriangle_ak + 0.001f;
        }
        else
        {
            a = 0;
            angle_ak = 0.0f;
        }
    
        if (Ytriangle_ak <= 0.0f)
            Ytriangle_ak = Ytriangle_ak + 0.001f;
        if (angle_ak >= 360.0f)
            angle_ak = 0.0f;

        if(a == 0){
            Matrix.setIdentityM(modelViewMatrix_ak, 0);
            Matrix.setIdentityM(modelViewProjectionMatrix_ak, 0);
            Matrix.setIdentityM(rotationMatrix_ak, 0);
        
            Matrix.translateM(modelViewMatrix_ak, 0, XCircle_ak, YCircle_ak, -2.5f);
            Matrix.rotateM(rotationMatrix_ak, 0, CRangle_ak, 0.0f, 1.0f, 0.0f);
            Matrix.multiplyMM(modelViewMatrix_ak, 0, modelViewMatrix_ak, 0, rotationMatrix_ak, 0);
            Matrix.multiplyMM(modelViewProjectionMatrix_ak, 0, perspectiveProjectMatrix_ak, 0, modelViewMatrix_ak, 0);
        
            GLES32.glUniformMatrix4fv(mvpUniform_ak, 1, false, modelViewProjectionMatrix_ak, 0);
        
            inCircle(0.0f, 0.5f, 0.0f, -0.5f, -0.5f, 0.0f, 0.5f, -0.5f, 0.0f);
        
            CRangle_ak += 1.0f;
        
            if (XCircle_ak >= 0.0f)
                XCircle_ak = XCircle_ak - 0.001f;
            else
            {
                a = 2;
                CRangle_ak = 0.0f;
            }
        
            if (YCircle_ak <= 0.0f)
                YCircle_ak = YCircle_ak + 0.001f;
            if (CRangle_ak >= 360.0f)
                CRangle_ak = 0.0f;

            
            if (a == 2)
            {
                Matrix.setIdentityM(modelViewMatrix_ak, 0);
                Matrix.setIdentityM(modelViewProjectionMatrix_ak, 0);
                Matrix.setIdentityM(rotationMatrix_ak, 0);
    
                Matrix.translateM(modelViewMatrix_ak, 0, XWand_ak, YWand_ak, -2.5f);
                Matrix.multiplyMM(modelViewMatrix_ak, 0, modelViewMatrix_ak, 0, modelViewMatrix_ak, 0);
                Matrix.multiplyMM(modelViewProjectionMatrix_ak, 0, perspectiveProjectMatrix_ak, 0, modelViewMatrix_ak, 0);

                GLES32.glUniformMatrix4fv(mvpUniform_ak, 1, false, modelViewProjectionMatrix_ak, 0);
    
                wand(0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f);
    
                if (YWand_ak >= 0.0f)
                    YWand_ak -= 0.001f;
            }

        }
            
        GLES32.glBindVertexArray(0);

        GLES32.glUseProgram(0);

        requestRender();
    }


    void triangle(float value){

        float lineVertices[] = new float[] {
            1.0f, 1.0f, 0.0f, 
            1.0f, -1.0f, 0.0f
        };
        float lineColor[] = new float[] {
            1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f
        };


        lineVertices[0] = 0.0f;
        lineVertices[1] = value;
        lineVertices[2] = 0.0f;
        lineVertices[3] = -value;
        lineVertices[4] = -value;
        lineVertices[5] = 0.0f;

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPosition_ak[0]);
        byteBuffer = ByteBuffer.allocateDirect(6 * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer = byteBuffer.asFloatBuffer();
        verticesBuffer.put(lineVertices);
        verticesBuffer.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 6 * 4, verticesBuffer, GLES32.GL_DYNAMIC_DRAW);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColor_ak[0]);
        byteBufferColor = ByteBuffer.allocateDirect(6 * 4);
        byteBufferColor.order(ByteOrder.nativeOrder());
        colorBuffer = byteBuffer.asFloatBuffer();
        colorBuffer.put(lineColor);
        colorBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 6 * 4, colorBuffer, GLES32.GL_DYNAMIC_DRAW);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        lineVertices[0] = -value;
        lineVertices[1] = -value;
        lineVertices[2] = 0.0f;
        lineVertices[3] = value;
        lineVertices[4] = -value;
        lineVertices[5] = 0.0f;
    
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPosition_ak[0]);
        byteBuffer = ByteBuffer.allocateDirect(6 * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer = byteBuffer.asFloatBuffer();
        verticesBuffer.put(lineVertices);
        verticesBuffer.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 6 * 4, verticesBuffer, GLES32.GL_DYNAMIC_DRAW);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColor_ak[0]);
        byteBufferColor = ByteBuffer.allocateDirect(6 * 4);
        byteBufferColor.order(ByteOrder.nativeOrder());
        colorBuffer = byteBuffer.asFloatBuffer();
        colorBuffer.put(lineColor);
        colorBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 6 * 4, colorBuffer, GLES32.GL_DYNAMIC_DRAW);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        lineVertices[0] = value;
        lineVertices[1] = -value;
        lineVertices[2] = 0.0f;
        lineVertices[3] = 0.0f;
        lineVertices[4] = value;
        lineVertices[5] = 0.0f;


        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPosition_ak[0]);
        byteBuffer = ByteBuffer.allocateDirect(6 * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer = byteBuffer.asFloatBuffer();
        verticesBuffer.put(lineVertices);
        verticesBuffer.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 6 * 4, verticesBuffer, GLES32.GL_DYNAMIC_DRAW);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColor_ak[0]);
        byteBufferColor = ByteBuffer.allocateDirect(6 * 4);
        byteBufferColor.order(ByteOrder.nativeOrder());
        colorBuffer = byteBuffer.asFloatBuffer();
        colorBuffer.put(lineColor);
        colorBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 6 * 4, colorBuffer, GLES32.GL_DYNAMIC_DRAW);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
    
    }

    void inCircle(float x1_ak, float y1_ak, float z1_ak, float x2_ak, float y2_ak, float z2, float x3_ak, float y3_ak, float z3_ak){
        float lineVertices[] = new float[] {
            0.0f, 0.0f, 0.0f 
        };
        float lineColor[] = new float[] {
            1.0f, 1.0f, 1.0f
        };
        
        float count_ak = 0.0f;
        float angle_ak = 0.0f;
    
        float dist_a_b_ak = (float)Math.sqrt((x2_ak - x1_ak) * (x2_ak - x1_ak) + (y2_ak - y1_ak) * (y2_ak - y1_ak) + (z2 - z1_ak) * (z2 - z1_ak));
        float dist_b_c_ak = (float)Math.sqrt((x3_ak - x2_ak) * (x3_ak - x2_ak) + (y3_ak - y2_ak) * (y3_ak - y2_ak) + (z3_ak - z2) * (z3_ak - z2));
        float dist_c_a_ak = (float)Math.sqrt((x1_ak - x3_ak) * (x1_ak - x3_ak) + (y1_ak - y3_ak) * (y1_ak - y3_ak) + (z1_ak - z3_ak) * (z1_ak - z3_ak));
    
        float semiperimeter_ak = (dist_a_b_ak + dist_b_c_ak + dist_c_a_ak) / 2;
        float radius_ak =(float) Math.sqrt((semiperimeter_ak - dist_a_b_ak) * (semiperimeter_ak - dist_b_c_ak) * (semiperimeter_ak - dist_c_a_ak) / semiperimeter_ak);
        float Ox_ak = (x3_ak * dist_a_b_ak + x1_ak * dist_b_c_ak + x2_ak * dist_c_a_ak) / (semiperimeter_ak * 2);
        float Oy_ak = (y3_ak * dist_a_b_ak + y1_ak * dist_b_c_ak + y2_ak * dist_c_a_ak) / (semiperimeter_ak * 2);
        float Oz_ak = (z3_ak * dist_a_b_ak + z1_ak * dist_b_c_ak + z2 * dist_c_a_ak) / (semiperimeter_ak * 2);

        for (count_ak = 0; count_ak <= 2000; count_ak++)
        {
    
            lineVertices[0] =(float)(Math.cos(angle_ak) * radius_ak + Ox_ak);
            lineVertices[1] = (float)(Math.sin(angle_ak) * radius_ak + Oy_ak);
            lineVertices[2] = (float)(0.0f + Oz_ak);
    
            angle_ak = (float) (2 * 3.14 * count_ak / 2000);

            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPosition_ak[0]);
            byteBuffer = ByteBuffer.allocateDirect(3 * 4);
            byteBuffer.order(ByteOrder.nativeOrder());
            verticesBuffer = byteBuffer.asFloatBuffer();
            verticesBuffer.put(lineVertices);
            verticesBuffer.position(0);
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 3 * 4, verticesBuffer, GLES32.GL_DYNAMIC_DRAW);
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
    
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColor_ak[0]);
            byteBufferColor = ByteBuffer.allocateDirect(3 * 4);
            byteBufferColor.order(ByteOrder.nativeOrder());
            colorBuffer = byteBuffer.asFloatBuffer();
            colorBuffer.put(lineColor);
            colorBuffer.position(0);
    
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 3 * 4, colorBuffer, GLES32.GL_DYNAMIC_DRAW);
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
    
            GLES32.glDrawArrays(GLES32.GL_POINTS, 0, 1);
       }
    }

    void wand(float x1_ak, float y1_ak, float z1_ak, float x2_ak, float y2_ak, float z2_ak){
        
        float lineVertices[] = new float[] {
           x1_ak, y1_ak, z1_ak,
           x2_ak, y2_ak, z2_ak
        };

        float lineColor[] = new float[] {
            1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f
        };

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPosition_ak[0]);
        byteBuffer = ByteBuffer.allocateDirect(6 * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer = byteBuffer.asFloatBuffer();
        verticesBuffer.put(lineVertices);
        verticesBuffer.position(0);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 6 * 4, verticesBuffer, GLES32.GL_DYNAMIC_DRAW);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColor_ak[0]);
        byteBufferColor = ByteBuffer.allocateDirect(6 * 4);
        byteBufferColor.order(ByteOrder.nativeOrder());
        colorBuffer = byteBuffer.asFloatBuffer();
        colorBuffer.put(lineColor);
        colorBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 6 * 4, colorBuffer, GLES32.GL_DYNAMIC_DRAW);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

    }

    void uninitialize(){
        if(vao_ak[0] != 0){
            GLES32.glDeleteVertexArrays(1, vao_ak, 0);
            vao_ak[0] = 0;
        }

        if(vboPosition_ak[0] != 0){
            GLES32.glDeleteVertexArrays(1, vboPosition_ak, 0);
            vboPosition_ak[0] = 0;
        }

        if(vboColor_ak[0] != 0){
            GLES32.glDeleteVertexArrays(1, vboColor_ak, 0);
            vboColor_ak[0] = 0;
        }

        if(shaderProgramObject_ak != 0){
            if(vertexShaderObject_ak != 0){
                GLES32.glDetachShader(shaderProgramObject_ak, vertexShaderObject_ak);
                GLES32.glDeleteShader(vertexShaderObject_ak);
                vertexShaderObject_ak = 0;
            }

            if(fragmentShaderObject_ak != 0){
                GLES32.glDetachShader(shaderProgramObject_ak, fragmentShaderObject_ak);
                GLES32.glDeleteShader(fragmentShaderObject_ak);
                fragmentShaderObject_ak = 0;
            }
        }

        if(shaderProgramObject_ak != 0){
            GLES32.glDeleteProgram(shaderProgramObject_ak);
            shaderProgramObject_ak = 0;
        }

    }
}