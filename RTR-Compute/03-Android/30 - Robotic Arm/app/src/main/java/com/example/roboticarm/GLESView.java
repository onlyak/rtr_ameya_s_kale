
package com.example.roboticarm;

import android.content.Context;
import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener {
	private final Context context;
	private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int modelViewMatrixUniform;
	private int viewMatrixUniform;
	private int projectionMatrixUniform;

	private int lKeyPressedUniform;

	private int laUniform;
	private int ldUniform;
	private int lsUniform;
	private int lightPositionUniform;

	private int kaUniform;
	private int kdUniform;
	private int ksUniform;
	private int materialShininessUniform;

	private boolean rotationDirection = false;
	private int shoulder = 0;
	private int elbow = 0;

	private float perspectiveProjectionMatrix[] = new float[16];

	private int[] vao_sphere = new int[1];
	private int[] vbo_position_sphere = new int[1];
	private int[] vbo_normal_sphere = new int[1];
	private int[] vbo_element_sphere = new int[1];

	private float sphere_vertices[] = new float[1146];
	private float sphere_normals[] = new float[1146];
	private float sphere_textures[] = new float[764];
	private short sphere_elements[] = new short[2280];

	private int numVertices = 0;
	private int numElements = 0;

	GLESView(Context drawingContext) {
		super(drawingContext);
		context = drawingContext;
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		initialize(gl);
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height) {
		resize(width, height);
	}

	@Override
	public void onDrawFrame(GL10 unused) {
		display();
		update();
	}

	@Override
	public boolean onTouchEvent(MotionEvent e) {
		int eventAction = e.getAction();
		if (!gestureDetector.onTouchEvent(e)) {
			super.onTouchEvent(e);
		}
		return (true);
	}

	@Override
	public boolean onDoubleTap(MotionEvent e) {
		if (rotationDirection == true) {
			rotationDirection = false;
		} else {
			rotationDirection = true;
		}
		return (true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e) {
		return (true);
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e) {
		if (rotationDirection == true) {
			elbow = (elbow + 3) % 360;
		} else {
			elbow = (elbow - 3) % 360;
		}
		return (true);
	}

	@Override
	public boolean onDown(MotionEvent e) {
		return (true);
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
		return (true);
	}

	@Override
	public void onLongPress(MotionEvent e) {
		if (rotationDirection == true) {
			shoulder = (shoulder + 3) % 360;
		} else {
			shoulder = (shoulder - 3) % 360;
		}
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
		uninitialize();
		System.exit(0);
		return (true);
	}

	@Override
	public void onShowPress(MotionEvent e) {
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		return (true);
	}

	private void initialize(GL10 gl) {
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		final String vertexShaderSourceCode = String.format(
			"#version 320 es" +
			"\n" +
			"in vec4 vPosition;" +
			"uniform mat4 u_mvp_matrix;" +
			"void main(void)" +
			"{" +
			"gl_Position = u_mvp_matrix * vPosition;" +
			"}"
		);

		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);

		GLES32.glCompileShader(vertexShaderObject);
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		if (iShaderCompileStatus[0] == GLES32.GL_FALSE) {
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0) {
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				uninitialize();
				System.exit(0);
			}
		}
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		final String fragementShaderSourceCode = String.format(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			"precision lowp int;" +
			"out vec4 FragColor;" +
			"void main(void)" +
			"{" +
			"FragColor = vec4(0.5, 0.35, 0.05, 1.0);" +
			"}"
		);

		GLES32.glShaderSource(fragmentShaderObject, fragementShaderSourceCode);

		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		if (iShaderCompileStatus[0] == GLES32.GL_FALSE) {
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0) {
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				uninitialize();
				System.exit(0);
			}
		}

		shaderProgramObject = GLES32.glCreateProgram();

		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.ATTRIBUTE_POSITION, "vPosition");
		GLES32.glLinkProgram(shaderProgramObject);

		int[] iShaderProgramLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
		if (iShaderProgramLinkStatus[0] == GLES32.GL_FALSE) {
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0) {
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				uninitialize();
				System.exit(0);
			}
		}

		modelViewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");

		Sphere sphere = new Sphere();

		sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
		numVertices = sphere.getNumberOfSphereVertices();
		numElements = sphere.getNumberOfSphereElements();

		GLES32.glGenVertexArrays(1, vao_sphere, 0);
		GLES32.glBindVertexArray(vao_sphere[0]);

		GLES32.glGenBuffers(1, vbo_position_sphere, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_sphere[0]);

		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_vertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(sphere_vertices);
		verticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_vertices.length * 4, verticesBuffer, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);

		GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_POSITION);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glGenBuffers(1, vbo_normal_sphere, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_normal_sphere[0]);

		byteBuffer = ByteBuffer.allocateDirect(sphere_normals.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer normalBuffer = byteBuffer.asFloatBuffer();
		normalBuffer = byteBuffer.asFloatBuffer();
		normalBuffer.put(sphere_normals);
		normalBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_normals.length * 4, normalBuffer, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 0, 0);

		GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_NORMAL);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glGenBuffers(1, vbo_element_sphere, 0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere[0]);

		byteBuffer = ByteBuffer.allocateDirect(sphere_elements.length * 2);
		byteBuffer.order(ByteOrder.nativeOrder());
		ShortBuffer elementsBuffer = byteBuffer.asShortBuffer();
		elementsBuffer.put(sphere_elements);
		elementsBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER, sphere_elements.length * 2, elementsBuffer, GLES32.GL_STATIC_DRAW);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);

		GLES32.glClearDepthf(1.0f);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		rotationDirection = false;
		Matrix.setIdentityM(perspectiveProjectionMatrix, 0);

	}

	private void resize(int width, int height) {
		GLES32.glViewport(0, 0, width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float) width / height, 0.1f, 100.f);
	}

	private void display() {
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		GLES32.glUseProgram(shaderProgramObject);

		float modelViewMatrix1[] = new float[16];
		float modelViewMatrix2[] = new float[16];
		float modelViewMatrix3[] = new float[16];
		float modelViewMatrix4[] = new float[16];
		float modelViewProjectionMatrix[] = new float[16];
		float translationMatrix[] = new float[16];
		float xRotationMatrix[] = new float[16];
		float yRotationMatrix[] = new float[16];
		float zRotationMatrix[] = new float[16];
		float scaleMatrix[] = new float[16];


		Matrix.setIdentityM(modelViewMatrix1, 0);
		Matrix.setIdentityM(modelViewMatrix2, 0);
		Matrix.setIdentityM(modelViewMatrix3, 0);
		Matrix.setIdentityM(modelViewMatrix4, 0);
		Matrix.setIdentityM(translationMatrix, 0);
		Matrix.setIdentityM(xRotationMatrix, 0);
		Matrix.setIdentityM(yRotationMatrix, 0);
		Matrix.setIdentityM(zRotationMatrix, 0);
		Matrix.setIdentityM(scaleMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);

		Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -3.0f);
		Matrix.multiplyMM(modelViewMatrix1, 0, modelViewMatrix1, 0, translationMatrix, 0);
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix1, 0);
		GLES32.glUniformMatrix4fv(modelViewMatrixUniform, 1, false, modelViewProjectionMatrix, 0);
		
		Matrix.setRotateM(zRotationMatrix, 0, shoulder, 0.0f, 0.0f, 1.0f);
		Matrix.translateM(translationMatrix, 0, 1.0f, 0.0f, 0.0f);
		Matrix.multiplyMM(modelViewMatrix2 , 0, modelViewMatrix1 , 0, zRotationMatrix, 0);
		Matrix.multiplyMM(modelViewMatrix2 , 0, modelViewMatrix2 , 0, translationMatrix, 0);
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix2, 0);
		GLES32.glUniformMatrix4fv(modelViewMatrixUniform, 1, false, modelViewProjectionMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 2.0f, 0.5f, 1.0f);
		Matrix.multiplyMM(modelViewMatrix3 , 0, modelViewMatrix2 , 0, scaleMatrix, 0);
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix3, 0);
		GLES32.glUniformMatrix4fv(modelViewMatrixUniform, 1, false, modelViewProjectionMatrix, 0);

		GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		GLES32.glBindVertexArray(0);

		Matrix.translateM(translationMatrix, 0, 1.0f, 0.0f, 0.0f);
		Matrix.multiplyMM(modelViewMatrix3 , 0, modelViewMatrix2 , 0, translationMatrix, 0);
		Matrix.setRotateM(zRotationMatrix, 0, elbow, 0.0f, 0.0f, 1.0f);
		Matrix.translateM(translationMatrix, 0, 1.0f, 0.0f, 0.0f);
		Matrix.multiplyMM(modelViewMatrix3  , 0, modelViewMatrix3  , 0, zRotationMatrix , 0);
		Matrix.multiplyMM(modelViewMatrix3  , 0, modelViewMatrix3  , 0, translationMatrix , 0);
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix3, 0);
		GLES32.glUniformMatrix4fv(modelViewMatrixUniform, 1, false, modelViewProjectionMatrix, 0);

		Matrix.setIdentityM(scaleMatrix, 0);

		Matrix.scaleM(scaleMatrix, 0, 4.0f, 1.0f, 2.0f);
		Matrix.multiplyMM(modelViewMatrix4 , 0, modelViewMatrix3 , 0, scaleMatrix, 0);
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix4, 0);
		GLES32.glUniformMatrix4fv(modelViewMatrixUniform, 1, false, modelViewProjectionMatrix, 0);

		GLES32.glBindVertexArray(vao_sphere[0]);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
		GLES32.glBindVertexArray(0);

		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix3, 0);
		GLES32.glUniformMatrix4fv(modelViewMatrixUniform, 1, false, modelViewProjectionMatrix, 0);

		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix2, 0);
		GLES32.glUniformMatrix4fv(modelViewMatrixUniform, 1, false, modelViewProjectionMatrix, 0);

		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix1, 0);
		GLES32.glUniformMatrix4fv(modelViewMatrixUniform, 1, false, modelViewProjectionMatrix, 0);


		GLES32.glUseProgram(0);

		requestRender();
	}

	private void update() {
	}

	private void uninitialize() {

		if (vao_sphere[0] != 0) {
			GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
			vao_sphere[0] = 0;
		}

		if (vbo_position_sphere[0] != 0) {
			GLES32.glDeleteBuffers(1, vbo_position_sphere, 0);
			vbo_position_sphere[0] = 0;
		}

		if (vbo_normal_sphere[0] != 0) {
			GLES32.glDeleteBuffers(1, vbo_normal_sphere, 0);
			vbo_normal_sphere[0] = 0;
		}

		if (vbo_element_sphere[0] != 0) {
			GLES32.glDeleteBuffers(1, vbo_element_sphere, 0);
			vbo_element_sphere[0] = 0;
		}

		if (shaderProgramObject != 0) {

			if (vertexShaderObject != 0) {
				GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
				GLES32.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}

			if (fragmentShaderObject != 0) {
				GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
				GLES32.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}

		if (shaderProgramObject != 0) {
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}
	}
}
