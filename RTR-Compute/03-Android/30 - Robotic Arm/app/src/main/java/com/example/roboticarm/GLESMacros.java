package com.example.roboticarm;

public class GLESMacros {
    public static final int ATTRIBUTE_POSITION_PSM  = 0;
	public static final int ATTRIBUTE_COLOR_PSM = 1;
	public static final int ATTRIBUTE_NORMAL_PSM = 2;
	public static final int ATTRIBUTE_TEXCOORD0_PSM = 3;
}
