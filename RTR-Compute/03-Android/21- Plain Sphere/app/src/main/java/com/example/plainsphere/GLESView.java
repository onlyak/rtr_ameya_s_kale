package com.example.plainsphere;

import android.content.Context;
import android.opengl.GLSurfaceView; 
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{

    private final Context context_ak;

    private GestureDetector gestureDetector;

    private int vertexShaderObject_ak;
    private int fragmentShaderObject_ak;
    private int shaderProgramObject_ak;

    private int[] vaoSphere_ak = new int[1];
    private int[] vboPositionSphere_ak = new int[1];
    private int[] vboElementSphere_ak = new int[1];

    private int numVertices;
    private int numElements;

    private int modelViewMatrixUniform_ak;
    private int modelViewProjectionMatrixUniform_ak;
    private int ldUniform_ak;
    private int kdUniform_ak;
    private int lightPositionUniform_ak;
    private int lKeyPressedUniform_ak;

    private boolean bLight_ak = false;

    private float perspectiveProjectMatrix_ak[] = new float[16];

    private float angleCube = 0.0f;
    

    public GLESView(Context drawingContext){
        
        super(drawingContext);
        context_ak = drawingContext;
        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        gestureDetector = new GestureDetector (context_ak, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config){
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("ASK: OpenGL-ES:- "+version);
        String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("ASK: GLSL Version:- "+glslVersion);

        initialize(gl);
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height){
        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused){
        draw();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        int eventAction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);
        return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent e){
        System.out.println("ASK: "+"Double Tap");
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e){
        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e){
        System.out.println("ASK: "+"Single Tap");
        return true;
    }

    @Override
    public boolean onDown(MotionEvent e){
        return true;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float veloxityX, float veloxityY){
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e){
        System.out.println("ASK: "+"Long Press");
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY){
        System.out.println("ASK: "+"Scroll");
        System.exit(0);
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e){

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e){
        return true;
    }

    private void initialize(GL10 gl){
        
        /************************Vertex Shader************************************/

        vertexShaderObject_ak = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
        final String vertexShaderSourceCode = String.format(
        "#version 320 es" +
        "\n"+
        "in vec4 vPosition;" +
        "in vec3 vNormal;" +
        "uniform mat4 u_model_view_matrix;" +
        "uniform mat4 u_projection_matrix;" +
        "uniform lowp int u_LKeyPressed;"+
        "uniform vec3 u_Ld;"+
        "uniform vec3 u_Kd;"+
        "uniform vec4 u_light_position;"+
        "out vec3 diffuse_light;" +
        "void main()" +
        "{" +
		"if (u_LKeyPressed == 1) " +
            "{" +
                "vec4 eyeCoordinates = u_model_view_matrix * vPosition;" +
                "mat3 normalMatrix = mat3(transpose(inverse(u_model_view_matrix)));" +
                "vec3 tnorm = normalize(normalMatrix * vNormal);" +
                "vec3 s = normalize(vec3(u_light_position - eyeCoordinates));" +
                "diffuse_light = u_Ld * u_Kd * max(dot(s, tnorm), 0.0);" +
            "}" +
		"gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;" +
	   "}"
        );

        GLES32.glShaderSource(vertexShaderObject_ak, vertexShaderSourceCode);

        GLES32.glCompileShader(vertexShaderObject_ak);
        int[] iShaderCompiledStatus = new int[1];
        int[] infoLogLength = new int[1];
        String szInfoLog = null;
        GLES32.glGetShaderiv(vertexShaderObject_ak, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
        if(iShaderCompiledStatus[0] == GLES32.GL_FALSE){
            GLES32.glGetShaderiv(vertexShaderObject_ak, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);
            if(infoLogLength[0] > 0){
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject_ak);
                System.out.println("ASK: Vertex Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        /************************Fragment Shader************************************/

        fragmentShaderObject_ak = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        final String fragmentShaderSourceCode =  String.format(
        "#version 320 es" +
        "\n" +
        "precision highp float;" +
        "precision lowp int;" +
        "out vec4 FragColor;" +
        "in vec3 diffuse_light;" +
        "uniform int u_LKeyPressed;" +
        "void main()"+
        "{" +
            "vec4 color;" +
            "if (u_LKeyPressed == 1) " +
            "{"+
                "color = vec4(diffuse_light, 1.0);"  +
            "}"+
            "else" +
            "{" +
                "color = vec4(1.0f,1.0f,1.0f,1.0f);" +
            "}" +
		    "FragColor = color;" +
	    "}"
        );

        GLES32.glShaderSource(fragmentShaderObject_ak, fragmentShaderSourceCode);

        GLES32.glCompileShader(fragmentShaderObject_ak);
        iShaderCompiledStatus[0] = 0;
        infoLogLength[0] = 0;
        szInfoLog = null;
        GLES32.glGetShaderiv(fragmentShaderObject_ak, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
        if(iShaderCompiledStatus[0] == GLES32.GL_FALSE){
            GLES32.glGetShaderiv(fragmentShaderObject_ak, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
            if(infoLogLength[0] > 0){
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject_ak);
                System.out.println("ASK: Fragment Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        shaderProgramObject_ak = GLES32.glCreateProgram();

        GLES32.glAttachShader(shaderProgramObject_ak, vertexShaderObject_ak);
        GLES32.glAttachShader(shaderProgramObject_ak, fragmentShaderObject_ak);

        GLES32.glBindAttribLocation(shaderProgramObject_ak, GLESMacros.ATTRIBUTE_VERTEX, "vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject_ak, GLESMacros.ATTRIBUTE_NORMAL, "vNormal");

        GLES32.glLinkProgram(shaderProgramObject_ak);

        int[] iShaderProgramLinkStatus = new int[1];
        infoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetProgramiv(shaderProgramObject_ak, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
        if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE){
            GLES32.glGetProgramiv(shaderProgramObject_ak, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);
            if(infoLogLength[0] > 0){
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject_ak);
                System.out.println("ASK: Shader Program Link Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
        }

      }

      modelViewMatrixUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_model_view_matrix");
	  modelViewProjectionMatrixUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_projection_matrix");
	  ldUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_Ld");
	  kdUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_Kd");
	  lightPositionUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_light_position");
	  lKeyPressedUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_LKeyPressed");


      Sphere sphere=new Sphere();

      float sphere_vertices[]=new float[1146];
      float sphere_normals[]=new float[1146];
      float sphere_textures[]=new float[764];
      short sphere_elements[]=new short[2280];
      sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
      numVertices = sphere.getNumberOfSphereVertices();
      numElements = sphere.getNumberOfSphereElements();

      //Sphere Starts
      GLES32.glGenVertexArrays(1, vaoSphere_ak, 0);
      GLES32.glBindVertexArray(vaoSphere_ak[0]);

      //Sphere Position
      GLES32.glGenBuffers(1,vboPositionSphere_ak,0);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPositionSphere_ak[0]);

      ByteBuffer byteBufferPositionSphere = ByteBuffer.allocateDirect(sphere_vertices.length * 4);
      byteBufferPositionSphere.order(ByteOrder.nativeOrder());
      FloatBuffer verticesBufferSphere = byteBufferPositionSphere.asFloatBuffer();
      verticesBufferSphere.put(sphere_vertices);
      verticesBufferSphere.position(0);

      GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_vertices.length * 4, verticesBufferSphere, GLES32.GL_STATIC_DRAW);
      GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_VERTEX, 3, GLES32.GL_FLOAT, false, 0, 0);
      GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_VERTEX);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

      //Sphere Elements
      GLES32.glGenBuffers(1,vboElementSphere_ak,0);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboElementSphere_ak[0]);

      ByteBuffer byteBufferElementsSphere = ByteBuffer.allocateDirect(sphere_elements.length * 2);
      byteBufferElementsSphere.order(ByteOrder.nativeOrder());
      ShortBuffer elementBufferSphere = byteBufferElementsSphere.asShortBuffer();
      elementBufferSphere.put(sphere_elements);
      elementBufferSphere.position(0);

      GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_elements.length * 2, elementBufferSphere, GLES32.GL_STATIC_DRAW);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
      GLES32.glBindVertexArray(0);

      //Sphere Ends

      GLES32.glEnable(GLES32.GL_DEPTH_TEST);
      GLES32.glDepthFunc(GLES32.GL_LEQUAL);
      GLES32.glClearDepthf(1.0f);
    
      GLES32.glClearColor(0.0f,0.0f,0.0f,0.0f);

      Matrix.setIdentityM(perspectiveProjectMatrix_ak,0);
    }

    private void resize(int width, int height){
        GLES32.glViewport(0,0,width,height);
        Matrix.perspectiveM(perspectiveProjectMatrix_ak, 0, 45.0f, (float)width/height, 0.1f, 100.f);

    }

    public void draw(){
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject_ak);

        float modelViewMatrix_ak[] = new float[16];
        float modelViewProjectionMatrix_ak[] = new float[16];
        float rotationMatrix_ak[] = new float[16];

        Matrix.setIdentityM(modelViewMatrix_ak, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix_ak, 0);
        Matrix.setIdentityM(rotationMatrix_ak, 0);

        Matrix.translateM(modelViewMatrix_ak, 0, 0.0f, 0.0f, -1.0f);
        Matrix.multiplyMM(modelViewMatrix_ak, 0, modelViewMatrix_ak, 0, modelViewMatrix_ak, 0);
        
        GLES32.glUniformMatrix4fv(modelViewMatrixUniform_ak, 1, false, modelViewMatrix_ak, 0);
        GLES32.glUniformMatrix4fv(modelViewProjectionMatrixUniform_ak, 1, false, perspectiveProjectMatrix_ak, 0);

        GLES32.glUniform3f(ldUniform_ak, 1.0f, 1.0f, 1.0f);
		GLES32.glUniform3f(kdUniform_ak, 0.5f, 0.5f, 0.5f);
		GLES32.glUniform3f(lightPositionUniform_ak, 0.0f, 0.0f, 2.0f);

        if(bLight_ak)
            GLES32.glUniform1i(lKeyPressedUniform_ak, 1);
        else
            GLES32.glUniform1i(lKeyPressedUniform_ak, 0);

        
        GLES32.glBindVertexArray(vaoSphere_ak[0]);
        
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vboElementSphere_ak[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        GLES32.glBindVertexArray(0);

        GLES32.glUseProgram(0);

        update();

        requestRender();
    }

    private void update(){

    }

    void uninitialize(){
        if(vboElementSphere_ak[0] != 0){
            GLES32.glDeleteVertexArrays(1, vboElementSphere_ak, 0);
            vboElementSphere_ak[0] = 0;
        }

        if(vboPositionSphere_ak[0] != 0){
            GLES32.glDeleteVertexArrays(1, vboPositionSphere_ak, 0);
            vboPositionSphere_ak[0] = 0;
        }

        if(shaderProgramObject_ak != 0){
            if(vertexShaderObject_ak != 0){
                GLES32.glDetachShader(shaderProgramObject_ak, vertexShaderObject_ak);
                GLES32.glDeleteShader(vertexShaderObject_ak);
                vertexShaderObject_ak = 0;
            }

            if(fragmentShaderObject_ak != 0){
                GLES32.glDetachShader(shaderProgramObject_ak, fragmentShaderObject_ak);
                GLES32.glDeleteShader(fragmentShaderObject_ak);
                fragmentShaderObject_ak = 0;
            }
        }

        if(shaderProgramObject_ak != 0){
            GLES32.glDeleteProgram(shaderProgramObject_ak);
            shaderProgramObject_ak = 0;
        }

    }
}