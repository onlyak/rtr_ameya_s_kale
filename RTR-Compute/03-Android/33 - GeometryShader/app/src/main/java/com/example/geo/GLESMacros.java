package com.example.geo;

public class GLESMacros {
    public static final int ATTRIBUTE_POSITION  = 0;
	public static final int ATTRIBUTE_COLOR = 1;
	public static final int ATTRIBUTE_NORMAL = 2;
	public static final int ATTRIBUTE_TEXCOORD0 = 3;
}
