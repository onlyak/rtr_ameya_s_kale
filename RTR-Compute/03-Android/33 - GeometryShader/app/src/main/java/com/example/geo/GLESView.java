
package com.example.geo;

import android.content.Context;
import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

public class GLESView extends GLSurfaceView
		implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener {
	private final Context context_ak;
	private GestureDetector gestureDetector_ak;

	private int vertexShaderObject_ak;
	private int gGeometryShaderObject_ak;
	private int fragmentShaderObject_ak;
	private int shaderProgramObject_ak;

	private int[] vao_ak = new int[1];
	private int[] vbo_ak = new int[1];
	private int mvpUniform_ak;

	private float perspectiveProjectionMatrix_ak[] = new float[16];

	GLESView(Context drawingContext_ak) {
		super(drawingContext_ak);
		context_ak = drawingContext_ak;
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		gestureDetector_ak = new GestureDetector(context_ak, this, null, false);
		gestureDetector_ak.setOnDoubleTapListener(this);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		String glesVersion_ak = gl.glGetString(GL10.GL_VERSION);
		String glslVersion_ak = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		initialize(gl);
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height) {
		resize(width, height);
	}

	@Override
	public void onDrawFrame(GL10 unused) {
		display();
	}

	@Override
	public boolean onTouchEvent(MotionEvent e) {
		int eventAction_ak = e.getAction();
		if (!gestureDetector_ak.onTouchEvent(e)) {
			super.onTouchEvent(e);
		}
		return (true);
	}

	@Override
	public boolean onDoubleTap(MotionEvent e) {
		return (true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e) {

		return (true);
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e) {
		return (true);
	}

	@Override
	public boolean onDown(MotionEvent e) {
		return (true);
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
		return (true);
	}

	@Override
	public void onLongPress(MotionEvent e) {
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
		uninitialize();
		System.exit(0);
		return (true);
	}

	@Override
	public void onShowPress(MotionEvent e) {
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		return (true);
	}

	private void initialize(GL10 gl) {
		vertexShaderObject_ak = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		final String vertexShaderSourceCode_ak = String.format(
				"#version 320 es" +
						"\n" +
						"in vec2 vPosition;\n" +
						"void main(void)\n" +
						"{\n" +
						"gl_Position = vec4(vPosition, 0.0, 1.0);" +
						"}");

		GLES32.glShaderSource(vertexShaderObject_ak, vertexShaderSourceCode_ak);

		GLES32.glCompileShader(vertexShaderObject_ak);
		int[] iShaderCompileStatus_ak = new int[1];
		int[] iInfoLogLength_ak = new int[1];
		String szInfoLog_ak = null;
		GLES32.glGetShaderiv(vertexShaderObject_ak, GLES32.GL_COMPILE_STATUS,
				iShaderCompileStatus_ak, 0);
		if (iShaderCompileStatus_ak[0] == GLES32.GL_FALSE) {
			GLES32.glGetShaderiv(vertexShaderObject_ak, GLES32.GL_INFO_LOG_LENGTH,
					iInfoLogLength_ak, 0);
			if (iInfoLogLength_ak[0] > 0) {
				szInfoLog_ak = GLES32.glGetShaderInfoLog(vertexShaderObject_ak);
				uninitialize();
				System.exit(0);
			}
		}

		gGeometryShaderObject_ak = GLES32.glCreateShader(GLES32.GL_GEOMETRY_SHADER);
		final String geometryShaderSourceCode = String.format(
				"#version 320 es" +
						"\n" +
						"layout(triangles)in;" +
						"layout(triangle_strip,max_vertices=9)out;" +
						"uniform mat4 u_mvp_matrix;" +
						"void main(void)" +
						"{" +
						"for(int i=0; i<3; i++) {" +
						"gl_Position = u_mvp_matrix * (gl_in[i].gl_Position + vec4(0.0, 1.0, 0.0, 0.0));"
						+
						"EmitVertex();" +
						"gl_Position = u_mvp_matrix * (gl_in[i].gl_Position + vec4(-1.0, -1.0, 0.0, 0.0));"
						+
						"EmitVertex();" +
						"gl_Position = u_mvp_matrix * (gl_in[i].gl_Position + vec4(1.0, -1.0, 0.0, 0.0));"
						+
						"EmitVertex();" +
						"EndPrimitive();" +
						"}" +
						"}");

		GLES32.glShaderSource(gGeometryShaderObject_ak,
				geometryShaderSourceCode);
		GLES32.glCompileShader(gGeometryShaderObject_ak);

		iShaderCompileStatus_ak[0] = 0;
		iInfoLogLength_ak[0] = 0;
		szInfoLog_ak = null;

		GLES32.glGetShaderiv(gGeometryShaderObject_ak, GLES32.GL_COMPILE_STATUS,
				iShaderCompileStatus_ak, 0);
		if (iShaderCompileStatus_ak[0] == GLES32.GL_FALSE) {
			GLES32.glGetShaderiv(gGeometryShaderObject_ak, GLES32.GL_INFO_LOG_LENGTH,
					iInfoLogLength_ak, 0);
			if (iInfoLogLength_ak[0] > 0) {
				szInfoLog_ak = GLES32.glGetShaderInfoLog(gGeometryShaderObject_ak);
				uninitialize();
				System.exit(0);
			}
		}

		fragmentShaderObject_ak = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		final String fragementShaderSourceCode_ak = String.format(
				"#version 320 es" +
						"\n" +
						"precision highp float;\n" +
						"out vec4 fragColor;\n" +
						"void main(void)" +
						"{" +
						"fragColor = vec4(1.0,1.0,1.0,1.0);" +
						"}");

		GLES32.glShaderSource(fragmentShaderObject_ak, fragementShaderSourceCode_ak);

		GLES32.glCompileShader(fragmentShaderObject_ak);

		iShaderCompileStatus_ak[0] = 0;
		iInfoLogLength_ak[0] = 0;
		szInfoLog_ak = null;

		GLES32.glGetShaderiv(fragmentShaderObject_ak, GLES32.GL_COMPILE_STATUS,
				iShaderCompileStatus_ak, 0);
		if (iShaderCompileStatus_ak[0] == GLES32.GL_FALSE) {
			GLES32.glGetShaderiv(fragmentShaderObject_ak, GLES32.GL_INFO_LOG_LENGTH,
					iInfoLogLength_ak, 0);
			if (iInfoLogLength_ak[0] > 0) {
				szInfoLog_ak = GLES32.glGetShaderInfoLog(fragmentShaderObject_ak);
				uninitialize();
				System.exit(0);
			}
		}

		shaderProgramObject_ak = GLES32.glCreateProgram();

		GLES32.glAttachShader(shaderProgramObject_ak, vertexShaderObject_ak);
		GLES32.glAttachShader(shaderProgramObject_ak, gGeometryShaderObject_ak);
		GLES32.glAttachShader(shaderProgramObject_ak, fragmentShaderObject_ak);

		GLES32.glBindAttribLocation(shaderProgramObject_ak, GLESMacros.ATTRIBUTE_POSITION,
				"vPosition");

		GLES32.glLinkProgram(shaderProgramObject_ak);

		int[] iShaderProgramLinkStatus_ak = new int[1];
		iInfoLogLength_ak[0] = 0;
		szInfoLog_ak = null;

		GLES32.glGetProgramiv(shaderProgramObject_ak, GLES32.GL_LINK_STATUS,
				iShaderProgramLinkStatus_ak, 0);
		if (iShaderProgramLinkStatus_ak[0] == GLES32.GL_FALSE) {
			GLES32.glGetProgramiv(shaderProgramObject_ak, GLES32.GL_INFO_LOG_LENGTH,
					iInfoLogLength_ak, 0);
			if (iInfoLogLength_ak[0] > 0) {
				szInfoLog_ak = GLES32.glGetProgramInfoLog(shaderProgramObject_ak);
				uninitialize();
				System.exit(0);
			}
		}

		mvpUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_mvp_matrix");

		final float triangleVertices_ak[] = new float[] {
				0.0f, 1.0f, 0.0f,
				-1.0f, -1.0f, 0.0f,
				1.0f, -1.0f, 0.0f
		};

		GLES32.glGenVertexArrays(1, vao_ak, 0);
		GLES32.glBindVertexArray(vao_ak[0]);
		GLES32.glGenBuffers(1, vbo_ak, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_ak[0]);

		ByteBuffer byteBuffer_ak = ByteBuffer.allocateDirect(triangleVertices_ak.length * 4);
		byteBuffer_ak.order(ByteOrder.nativeOrder());
		FloatBuffer verticesBuffer_ak = byteBuffer_ak.asFloatBuffer();
		verticesBuffer_ak.put(triangleVertices_ak);
		verticesBuffer_ak.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, triangleVertices_ak.length * 4,
				verticesBuffer_ak,
				GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false,
				0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		GLES32.glBindVertexArray(0);

		GLES32.glClearDepthf(1.0f);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		GLES32.glEnable(GLES32.GL_CULL_FACE);

		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		Matrix.setIdentityM(perspectiveProjectionMatrix_ak, 0);
	}

	private void resize(int width, int height) {
		GLES32.glViewport(0, 0, width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix_ak, 0, 45.0f, (float) width / height, 0.1f,
				100.f);
	}

	private void display() {
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		GLES32.glUseProgram(shaderProgramObject_ak);

		float modelViewMatrix_ak[] = new float[16];
		float modelViewProjectionMatrix_ak[] = new float[16];
		float lineColor_ak[] = new float[] {
				1.0f, 1.0f, 0.0f, 1.0f
		};

		Matrix.setIdentityM(modelViewMatrix_ak, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix_ak, 0);

		Matrix.translateM(modelViewMatrix_ak, 0, 0.0f, 0.0f, -6.0f);

		Matrix.multiplyMM(modelViewProjectionMatrix_ak, 0, perspectiveProjectionMatrix_ak, 0,
				modelViewMatrix_ak, 0);
		GLES32.glUniformMatrix4fv(mvpUniform_ak, 1, false, modelViewProjectionMatrix_ak, 0);
		GLES32.glBindVertexArray(vao_ak[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 3);
		GLES32.glBindVertexArray(0);

		GLES32.glUseProgram(0);

		requestRender();
	}

	private void uninitialize() {

		if (vao_ak[0] != 0) {
			GLES32.glDeleteVertexArrays(1, vao_ak, 0);
			vao_ak[0] = 0;
		}

		if (vbo_ak[0] != 0) {
			GLES32.glDeleteBuffers(1, vbo_ak, 0);
			vbo_ak[0] = 0;
		}

		if (shaderProgramObject_ak != 0) {

			if (vertexShaderObject_ak != 0) {
				GLES32.glDetachShader(shaderProgramObject_ak, vertexShaderObject_ak);
				GLES32.glDeleteShader(vertexShaderObject_ak);
				vertexShaderObject_ak = 0;
			}

			if (fragmentShaderObject_ak != 0) {
				GLES32.glDetachShader(shaderProgramObject_ak, fragmentShaderObject_ak);
				GLES32.glDeleteShader(fragmentShaderObject_ak);
				fragmentShaderObject_ak = 0;
			}
		}

		if (shaderProgramObject_ak != 0) {
			GLES32.glDeleteProgram(shaderProgramObject_ak);
			shaderProgramObject_ak = 0;
		}
	}
}
