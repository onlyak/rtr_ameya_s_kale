package com.example.threelightsonsphere;

public class LightStruct{
    public float[] lightAmbient_ak;
    public float[] lightDiffuse_ak;
    public float[] lightSpecular_ak;
    public float[] lightPosition_ak;

    public LightStruct(){
        this.lightAmbient_ak = new float[3];
        this.lightDiffuse_ak = new float[3];
        this.lightSpecular_ak = new float[3];
        this.lightPosition_ak = new float[4];
    }
};
