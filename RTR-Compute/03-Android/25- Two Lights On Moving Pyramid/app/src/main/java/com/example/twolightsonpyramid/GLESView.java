package com.example.twolightsonpyramid;

import android.content.Context;
import android.opengl.GLSurfaceView; 
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{

    private final Context context_ak;

    private GestureDetector gestureDetector;

    public LightStruct[] lights = new LightStruct[2];

    private int vertexShaderObject_ak;
    private int fragmentShaderObject_ak;
    private int shaderProgramObject_ak;

    private int[] vaoPyramid_ak = new int[1];
    private int[] vboPositionPyramid_ak = new int[1];
    private int[] vboNormalPyramid_ak = new int[1];

    private float[] materialAmbient_ak = new float[] { 0.0f,0.0f,0.0f,1.0f };
    private float[] materialDiffuse_ak = new float[] { 1.0f,1.0f,1.0f,1.0f };
    private float[] materialSpecular_ak = new float[]  { 1.0f,1.0f,1.0f,1.0f };
    private float materialShininess_ak = 50.0f;

    private int ldUniform_ak;
    private int kdUniform_ak;
    private int lightPositionUniform_ak;

    private int laUniform_ak;
    private int kaUniform_ak;
    
    private int lsUniform_ak;
    private int ksUniform_ak;

    private int shininessUniform_ak;
    private int singleTapLightUniform_ak;

    private int modelUniform_ak;
    private int viewUniform_ak;
    private int projectionUniform_ak;

    private boolean bLight_ak = false;

    private float perspectiveProjectMatrix_ak[] = new float[16];

    private float anglePyramid = 0.0f;

    public GLESView(Context drawingContext){
        
        super(drawingContext);
        context_ak = drawingContext;
        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        gestureDetector = new GestureDetector (context_ak, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config){
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("ASK: OpenGL-ES:- "+version);
        String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("ASK: GLSL Version:- "+glslVersion);

        initialize(gl);
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height){
        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused){
        draw();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        int eventAction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);
        return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent e){
        System.out.println("ASK: "+"Double Tap");
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e){
        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e){
        System.out.println("ASK: "+"Single Tap");
        if(bLight_ak == true){
            bLight_ak = false;
        }
        else{
            bLight_ak = true;
        }
        return true;
    }

    @Override
    public boolean onDown(MotionEvent e){
        return true;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float veloxityX, float veloxityY){
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e){
        System.out.println("ASK: "+"Long Press");
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY){
        System.out.println("ASK: "+"Scroll");
        System.exit(0);
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e){

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e){
        return true;
    }

    private void initialize(GL10 gl){
        
        /************************Vertex Shader************************************/

        vertexShaderObject_ak = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
        final String vertexShaderSourceCode = String.format(
        "#version 320 es" +
        "\n"+
        "precision highp float;"+
        "in vec4 vPosition;" +
        "in vec3 vNormal;"  +
        "uniform mat4 u_model_matrix;" +
        "uniform mat4 u_projection_matrix;" +
        "uniform mat4 u_view_matrix;" +
        "uniform lowp int u_single_tap_light;" +
        "uniform vec3 u_Ld[2];" +
        "uniform vec3 u_Kd;" +
        "uniform vec4 u_light_position[2];" +
        "uniform vec3 u_La[2];" +
        "uniform vec3 u_Ls[2];" +
        "uniform vec3 u_Ka;" +
        "uniform vec3 u_Ks;" +
        "uniform float u_shininess;" +
        "out vec3 phong_ads_light;" +
        "void main(void)" +
        "{" +
            "if (u_single_tap_light == 1) " +
            "{" +
                "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" +
                "vec3 transformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" +
                "vec3 view_vector = normalize(-eyeCoordinates.xyz);" +
                "phong_ads_light = vec3(0.0,0.0,0.0);"+
                "vec3 light_direction[2];"+
                "vec3 reflection_vector[2];"+
                "vec3 ambient[2];"+
                "vec3 diffuse[2];"+
                "vec3 specular[2];"+
                "for(int i =0; i<2 ; i++)"+
                "{"+
                    "light_direction[i] = normalize(vec3(u_light_position[i] - eyeCoordinates));" +
                    "reflection_vector[i] = reflect(-light_direction[i], transformed_normal);" +
                    "ambient[i] = u_La[i] * u_Ka;" +
                    "diffuse[i] = u_Ld[i] * u_Kd * max(dot(light_direction[i],transformed_normal),0.0);" +
                    "specular[i] = u_Ls[i] * u_Ks * pow(max(dot(reflection_vector[i],view_vector),0.0),u_shininess);" +
                    "phong_ads_light = phong_ads_light + ambient[i] + diffuse[i] + specular[i];" +
                "}"+
            "}" +
            "else" +
            "{" +
                "phong_ads_light = vec3(1.0f,1.0f,1.0f);" +
            "}"+
            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
        "}"
        );

        GLES32.glShaderSource(vertexShaderObject_ak, vertexShaderSourceCode);

        GLES32.glCompileShader(vertexShaderObject_ak);
        int[] iShaderCompiledStatus = new int[1];
        int[] infoLogLength = new int[1];
        String szInfoLog = null;
        GLES32.glGetShaderiv(vertexShaderObject_ak, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
        if(iShaderCompiledStatus[0] == GLES32.GL_FALSE){
            GLES32.glGetShaderiv(vertexShaderObject_ak, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);
            if(infoLogLength[0] > 0){
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject_ak);
                System.out.println("ASK: Vertex Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        /************************Fragment Shader************************************/

        fragmentShaderObject_ak = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        final String fragmentShaderSourceCode =  String.format(
        "#version 320 es" +
        "\n" +
        "precision highp float;" +
        "out vec4 FragColor;" +
        "in vec3 phong_ads_light;" +
        "void main()"+
        "{" +
            "FragColor = vec4(phong_ads_light, 1.0f);" +
        "}"
        );

        GLES32.glShaderSource(fragmentShaderObject_ak, fragmentShaderSourceCode);

        GLES32.glCompileShader(fragmentShaderObject_ak);
        iShaderCompiledStatus[0] = 0;
        infoLogLength[0] = 0;
        szInfoLog = null;
        GLES32.glGetShaderiv(fragmentShaderObject_ak, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
        if(iShaderCompiledStatus[0] == GLES32.GL_FALSE){
            GLES32.glGetShaderiv(fragmentShaderObject_ak, GLES32.GL_INFO_LOG_LENGTH, infoLogLength,0);
            if(infoLogLength[0] > 0){
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject_ak);
                System.out.println("ASK: Fragment Shader Compilation Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        shaderProgramObject_ak = GLES32.glCreateProgram();

        GLES32.glAttachShader(shaderProgramObject_ak, vertexShaderObject_ak);
        GLES32.glAttachShader(shaderProgramObject_ak, fragmentShaderObject_ak);

        GLES32.glBindAttribLocation(shaderProgramObject_ak, GLESMacros.ATTRIBUTE_VERTEX, "vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject_ak, GLESMacros.ATTRIBUTE_NORMAL, "vNormal");

        GLES32.glLinkProgram(shaderProgramObject_ak);

        int[] iShaderProgramLinkStatus = new int[1];
        infoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetProgramiv(shaderProgramObject_ak, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
        if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE){
            GLES32.glGetProgramiv(shaderProgramObject_ak, GLES32.GL_INFO_LOG_LENGTH, infoLogLength, 0);
            if(infoLogLength[0] > 0){
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject_ak);
                System.out.println("ASK: Shader Program Link Log = "+szInfoLog);
                uninitialize();
                System.exit(0);
        }

      }

      modelUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_model_matrix");
      viewUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_view_matrix");
      projectionUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_projection_matrix");
	  ldUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_Ld");
	  kdUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_Kd");
	  laUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_La");
	  kaUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_Ka");
	  lsUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_Ls");
	  ksUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_Ks");
      shininessUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_shininess");
	  lightPositionUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_light_position");
	  singleTapLightUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_single_tap_light");

      final float pyramidVertices[] = new float[]{
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,

		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,

		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f
      };

      final float pyramidNormals[] = new float[]{
        0.0f, 0.447214f,0.894427f,
        0.0f, 0.447214f,0.894427f,
        0.0f, 0.447214f,0.894427f,

        0.894427f,0.447214f,0.0f,
        0.894427f,0.447214f,0.0f,
        0.894427f,0.447214f,0.0f,

        0.0f,0.447214f,-0.894427f,
        0.0f,0.447214f,-0.894427f,
        0.0f,0.447214f,-0.894427f,

        -0.894427f, 0.447214f, 0.0f,
        -0.894427f, 0.447214f, 0.0f,
        -0.894427f, 0.447214f, 0.0f
      };

      //Pyramid Starts
      GLES32.glGenVertexArrays(1, vaoPyramid_ak, 0);
      GLES32.glBindVertexArray(vaoPyramid_ak[0]);

      GLES32.glGenBuffers(1,vboPositionPyramid_ak,0);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPositionPyramid_ak[0]);

      ByteBuffer byteBufferPositionPyramid = ByteBuffer.allocateDirect(pyramidVertices.length * 4);
      byteBufferPositionPyramid.order(ByteOrder.nativeOrder());
      FloatBuffer verticesBufferPyramid = byteBufferPositionPyramid.asFloatBuffer();
      verticesBufferPyramid.put(pyramidVertices);
      verticesBufferPyramid.position(0);

      GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidVertices.length * 4, verticesBufferPyramid, GLES32.GL_STATIC_DRAW);
      GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_VERTEX, 3, GLES32.GL_FLOAT, false, 0, 0);
      GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_VERTEX);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

      GLES32.glGenBuffers(1,vboNormalPyramid_ak,0);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboNormalPyramid_ak[0]);

      ByteBuffer byteBufferNormalPyramid = ByteBuffer.allocateDirect(pyramidNormals.length * 4);
      byteBufferNormalPyramid.order(ByteOrder.nativeOrder());
      FloatBuffer normalBufferPyramid = byteBufferNormalPyramid.asFloatBuffer();
      normalBufferPyramid.put(pyramidNormals);
      normalBufferPyramid.position(0);

      GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, pyramidNormals.length * 4, normalBufferPyramid, GLES32.GL_STATIC_DRAW);
      GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 0, 0);
      GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_NORMAL);
      GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
      GLES32.glBindVertexArray(0);
      //Pyramid Ends

      lights[0] = new LightStruct();
      lights[1] = new LightStruct();

      GLES32.glEnable(GLES32.GL_DEPTH_TEST);
      GLES32.glDepthFunc(GLES32.GL_LEQUAL);
      GLES32.glClearDepthf(1.0f);
    
      GLES32.glClearColor(0.0f,0.0f,0.0f,0.0f);

      Matrix.setIdentityM(perspectiveProjectMatrix_ak,0);
    }

    private void resize(int width, int height){
        GLES32.glViewport(0,0,width,height);
        Matrix.perspectiveM(perspectiveProjectMatrix_ak, 0, 45.0f, (float)width/height, 0.1f, 100.f);

    }

    public void draw(){
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        float[] lightAmbient = new float[] {
            lights[0].lightAmbient_ak[0] = 0.0f,
            lights[0].lightAmbient_ak[1] = 0.0f,
            lights[0].lightAmbient_ak[2] = 0.0f,

            lights[1].lightAmbient_ak[0] = 0.0f,
            lights[1].lightAmbient_ak[1] = 0.0f,
            lights[1].lightAmbient_ak[2] = 0.0f,
            };
        
        float[] lightDiffuse = new float[] {
			lights[0].lightDiffuse_ak[0] = 1.0f,
			lights[0].lightDiffuse_ak[1] = 0.0f,
			lights[0].lightDiffuse_ak[2] = 0.0f,

			lights[1].lightDiffuse_ak[0] = 0.0f,
			lights[1].lightDiffuse_ak[1] = 0.0f,
			lights[1].lightDiffuse_ak[2] = 1.0f,
    
            };
    
        float[] lightSpecular = new float[] {
			lights[0].lightSpecular_ak[0] = 1.0f,
			lights[0].lightSpecular_ak[1] = 0.0f,
			lights[0].lightSpecular_ak[2] = 0.0f,

			lights[1].lightSpecular_ak[0] = 0.0f,
			lights[1].lightSpecular_ak[1] = 0.0f,
			lights[1].lightSpecular_ak[2] = 1.0f,
            };
        
        float[] lightPosition = new float[] {
			lights[0].lightPosition_ak[0] = 2.0f,
			lights[0].lightPosition_ak[1] = 0.0f,
			lights[0].lightPosition_ak[2] = 0.0f,
			lights[0].lightPosition_ak[3] = 1.0f,

			lights[1].lightPosition_ak[0] = -2.0f,
			lights[1].lightPosition_ak[1] = 0.0f,
			lights[1].lightPosition_ak[2] = 0.0f,
			lights[1].lightPosition_ak[3] = 1.0f
            };

        GLES32.glUseProgram(shaderProgramObject_ak);

        
        GLES32.glUniform1i(singleTapLightUniform_ak, 1);

        GLES32.glUniform3fv(laUniform_ak,2,lightAmbient,0);
        GLES32.glUniform3fv(ldUniform_ak,2,lightDiffuse,0);
        GLES32.glUniform3fv(lsUniform_ak,2,lightSpecular,0);
        GLES32.glUniform4fv(lightPositionUniform_ak,2,lightPosition,0);

        GLES32.glUniform3fv(kaUniform_ak,1, materialAmbient_ak,0);
        GLES32.glUniform3fv(kdUniform_ak,1, materialDiffuse_ak,0);
        GLES32.glUniform3fv(ksUniform_ak,1, materialSpecular_ak,0);

        GLES32.glUniform1f(shininessUniform_ak,materialShininess_ak);
            
        if(bLight_ak == true){
            GLES32.glUniform1i(singleTapLightUniform_ak, 1);
        }
        else{
                GLES32.glUniform1i(singleTapLightUniform_ak, 0);
        }
        

        float modelMatrix_ak[] = new float[16];
        float viewMatrix_ak[] = new float[16];
        float rotationMatrix_ak[] = new float[16];

        Matrix.setIdentityM(modelMatrix_ak, 0);
        Matrix.setIdentityM(viewMatrix_ak, 0);
        Matrix.setIdentityM(rotationMatrix_ak, 0);

        Matrix.translateM(modelMatrix_ak, 0, 0.0f, 0.0f, -4.0f);
        Matrix.setRotateM(rotationMatrix_ak, 0, anglePyramid, 0.0f, 1.0f, 0.0f);
        Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, rotationMatrix_ak, 0);
       
        GLES32.glUniformMatrix4fv(modelUniform_ak, 1, false, modelMatrix_ak, 0);
        GLES32.glUniformMatrix4fv(viewUniform_ak, 1, false, viewMatrix_ak, 0);
        GLES32.glUniformMatrix4fv(projectionUniform_ak, 1, false, perspectiveProjectMatrix_ak, 0);
        
        GLES32.glBindVertexArray(vaoPyramid_ak[0]);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12);
        GLES32.glBindVertexArray(0);

        GLES32.glUseProgram(0);

        update();

        requestRender();
    }

    private void update(){
        anglePyramid +=0.5f;
        if(anglePyramid >=360)
            anglePyramid = 0.0f;

    }

    void uninitialize(){
        if(vaoPyramid_ak[0] != 0){
            GLES32.glDeleteVertexArrays(1, vaoPyramid_ak, 0);
            vaoPyramid_ak[0] = 0;
        }

        if(vboPositionPyramid_ak[0] != 0){
            GLES32.glDeleteVertexArrays(1, vboPositionPyramid_ak, 0);
            vboPositionPyramid_ak[0] = 0;
        }

        if(vboNormalPyramid_ak[0] != 0){
            GLES32.glDeleteVertexArrays(1, vboNormalPyramid_ak, 0);
            vboNormalPyramid_ak[0] = 0;
        }

        if(shaderProgramObject_ak != 0){
            if(vertexShaderObject_ak != 0){
                GLES32.glDetachShader(shaderProgramObject_ak, vertexShaderObject_ak);
                GLES32.glDeleteShader(vertexShaderObject_ak);
                vertexShaderObject_ak = 0;
            }

            if(fragmentShaderObject_ak != 0){
                GLES32.glDetachShader(shaderProgramObject_ak, fragmentShaderObject_ak);
                GLES32.glDeleteShader(fragmentShaderObject_ak);
                fragmentShaderObject_ak = 0;
            }
        }

        if(shaderProgramObject_ak != 0){
            GLES32.glDeleteProgram(shaderProgramObject_ak);
            shaderProgramObject_ak = 0;
        }

    }
}