
package com.example.interleaved;

import android.content.Context;
import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import android.opengl.Matrix;

public class GLESView extends GLSurfaceView
		implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener {
	private final Context context_ak;
	private GestureDetector gestureDetector_ak;

	private int vertexShaderObject_ak;
	private int fragmentShaderObject_ak;
	private int shaderProgramObject_ak;

	private int modelMatrixUniform_ak;
	private int viewMatrixUniform_ak;
	private int projectionMatrixUniform_ak;

	private int lKeyPressedUniform_ak;

	private int laUniform_ak;
	private int ldUniform_ak;
	private int lsUniform_ak;
	private int lightPositionUniform_ak;

	private int kaUniform_ak;
	private int kdUniform_ak;
	private int ksUniform_ak;
	private int materialShininessUniform_ak;

	private boolean bLight_ak = false;
	
	private float perspectiveProjectionMatrix_ak[] = new float[16];

	private int[] vao_ak = new int[1];
	private int[] vbo_ak = new int[1];

	private float lightAmbient_ak[] = new float[] {
			0.1f, 0.1f, 0.1f
	};
	private float lightDiffused_ak[] = new float[] {
			1.0f, 1.0f, 1.0f
	};
	private float lightPosition_ak[] = new float[] {
			100.0f, 100.0f, 100.0f, 1.0f
	};
	private float lightSpecular_ak[] = new float[] {
			1.0f, 1.0f, 1.0f
	};

	private float materialAmbient_ak[] = new float[] {
			0.0f, 0.0f, 0.0f, 1.0f
	};
	private float materialDiffused_ak[] = new float[] {
			0.5f, 0.2f, 0.7f, 1.0f
	};
	private float materialSpecular_ak[] = new float[] {
			0.7f, 0.7f, 0.7f, 1.0f
	};
	private float materialShininess_ak = 50.0f;

	private int textureSamplerUniform_ak;
    private int[] stone_texture_ak = new int[1];
	private float cubeAngle_ak = 0.0f;

	GLESView(Context drawingContext_ak) {
		super(drawingContext_ak);
		context_ak = drawingContext_ak;
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		gestureDetector_ak = new GestureDetector(context_ak, this, null, false);
		gestureDetector_ak.setOnDoubleTapListener(this);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		String glesVersion_ak = gl.glGetString(GL10.GL_VERSION);
		String glslVersion_ak = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		initialize(gl);
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height) {
		resize(width, height);
	}

	@Override
	public void onDrawFrame(GL10 unused) {
		display();
		update();
	}

	@Override
	public boolean onTouchEvent(MotionEvent e) {
		int eventAction_ak = e.getAction();
		if (!gestureDetector_ak.onTouchEvent(e)) {
			super.onTouchEvent(e);
		}
		return (true);
	}

	@Override
	public boolean onDoubleTap(MotionEvent e) {
		return (true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e) {
		return (true);
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e) {
		if (bLight_ak == true) {
			bLight_ak = false;
		} else {
			bLight_ak = true;
		}
		return (true);
	}

	@Override
	public boolean onDown(MotionEvent e) {
		return (true);
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
		return (true);
	}

	@Override
	public void onLongPress(MotionEvent e) {
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
		uninitialize();
		System.exit(0);
		return (true);
	}

	@Override
	public void onShowPress(MotionEvent e) {
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		return (true);
	}

	private void initialize(GL10 gl) {
		vertexShaderObject_ak = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		final String vertexShaderSourceCode_ak = String.format(
			"#version 320 es" +
			"\n" +
			"precision mediump float;" +
			"precision highp int;" +
			"in vec4 vPosition;" +
			"in vec4 vColor;\n" +
			"in vec3 vNormal;" +
			"in vec2 vTexCoord;\n" +
			"uniform mat4 u_model_matrix;" +
			"uniform mat4 u_view_matrix;" +
			"uniform mat4 u_projection_matrix;" +
			"uniform vec4 u_light_position;" +
			"uniform int u_lkey_pressed;" +
			"out vec3 transformed_normal;" +
			"out vec3 light_direction;" +
			"out vec3 view_vector;" +
			"out vec4 out_color;\n" +
			"out vec2 out_texcoord;\n" +
			"void main(void)" +
			"{" +
			"if(u_lkey_pressed == 1)" +
			"{" +
			"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" +
			"transformed_normal = mat3((u_view_matrix * u_model_matrix)) * vNormal;" +
			"light_direction = vec3(u_light_position - eye_coordinates);" +
			"view_vector = -eye_coordinates.xyz;" +
			"}" +
			"out_texcoord = vTexCoord;\n" +
			"out_color = vColor;\n" +
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
			"}");

		GLES32.glShaderSource(vertexShaderObject_ak, vertexShaderSourceCode_ak);

		GLES32.glCompileShader(vertexShaderObject_ak);
		int[] iShaderCompileStatus_ak = new int[1];
		int[] iInfoLogLength_ak = new int[1];
		String szInfoLog_ak = null;
		GLES32.glGetShaderiv(vertexShaderObject_ak, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus_ak, 0);
		if (iShaderCompileStatus_ak[0] == GLES32.GL_FALSE) {
			GLES32.glGetShaderiv(vertexShaderObject_ak, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength_ak, 0);
			if (iInfoLogLength_ak[0] > 0) {
				szInfoLog_ak = GLES32.glGetShaderInfoLog(vertexShaderObject_ak);
				uninitialize();
				System.exit(0);
			}
		}
		fragmentShaderObject_ak = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		final String fragementShaderSourceCode_ak = String.format(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			"precision highp int;" +
			"in vec4 out_color;\n" +
			"in vec2 out_texcoord;\n" +
			"in vec3 transformed_normal;" +
			"in vec3 light_direction;" +
			"in vec3 view_vector;" +
			"uniform vec3 u_la;" +
			"uniform vec3 u_ld;" +
			"uniform vec3 u_ls;" +
			"uniform vec3 u_ka;" +
			"uniform vec3 u_kd;" +
			"uniform vec3 u_ks;" +
			"uniform float u_material_shininess;" +
			"uniform int u_lkey_pressed;" +
			"uniform sampler2D u_texture_sampler;"+
			"out vec4 FragColor;" +
			"void main(void)" +
			"{" +
			"vec3 phong_ads_light;" +
			"if(u_lkey_pressed == 1)" +
			"{" +
			"vec3 normalized_transformed_normal = normalize(transformed_normal);" +
			"vec3 normalized_light_direction = normalize(light_direction);" +
			"vec3 normalized_view_vector = normalize(view_vector);" +
			"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normal);" +
			"vec3 ambient = u_la * u_ka;" +
			"vec3 diffuse = u_ld * u_kd * max(dot(normalized_light_direction, normalized_transformed_normal), 0.0);" +
			"vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, normalized_view_vector), 0.0), u_material_shininess);" +
			"phong_ads_light = ambient + diffuse + specular;" +
			"}" +
			"else" +
			"{" +
			"phong_ads_light = vec3(1.0, 1.0, 1.0);" +
			"}" +
			"vec3 tex = vec3(texture(u_texture_sampler, out_texcoord));"+
			"FragColor = vec4(vec3(out_color) * phong_ads_light * tex, 1.0f);"+
			"}");

		GLES32.glShaderSource(fragmentShaderObject_ak, fragementShaderSourceCode_ak);

		GLES32.glCompileShader(fragmentShaderObject_ak);
		iShaderCompileStatus_ak[0] = 0;
		iInfoLogLength_ak[0] = 0;
		szInfoLog_ak = null;

		GLES32.glGetShaderiv(fragmentShaderObject_ak, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus_ak, 0);
		if (iShaderCompileStatus_ak[0] == GLES32.GL_FALSE) {
			GLES32.glGetShaderiv(fragmentShaderObject_ak, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength_ak, 0);
			if (iInfoLogLength_ak[0] > 0) {
				szInfoLog_ak = GLES32.glGetShaderInfoLog(fragmentShaderObject_ak);
				uninitialize();
				System.exit(0);
			}
		}

		shaderProgramObject_ak = GLES32.glCreateProgram();

		GLES32.glAttachShader(shaderProgramObject_ak, vertexShaderObject_ak);
		GLES32.glAttachShader(shaderProgramObject_ak, fragmentShaderObject_ak);

		GLES32.glBindAttribLocation(shaderProgramObject_ak, GLESMacros.ATTRIBUTE_POSITION, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject_ak, GLESMacros.ATTRIBUTE_TEXCOORD0, "vTexCoord");
		GLES32.glBindAttribLocation(shaderProgramObject_ak, GLESMacros.ATTRIBUTE_POSITION, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject_ak, GLESMacros.ATTRIBUTE_NORMAL, "vNormal");
		GLES32.glLinkProgram(shaderProgramObject_ak);

		int[] iShaderProgramLinkStatus_ak = new int[1];
		iInfoLogLength_ak[0] = 0;
		szInfoLog_ak = null;

		GLES32.glGetProgramiv(shaderProgramObject_ak, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus_ak, 0);
		if (iShaderProgramLinkStatus_ak[0] == GLES32.GL_FALSE) {
			GLES32.glGetProgramiv(shaderProgramObject_ak, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength_ak, 0);
			if (iInfoLogLength_ak[0] > 0) {
				szInfoLog_ak = GLES32.glGetProgramInfoLog(shaderProgramObject_ak);
				uninitialize();
				System.exit(0);
			}
		}
		modelMatrixUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_model_matrix");
		viewMatrixUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_view_matrix");
		projectionMatrixUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_projection_matrix");
		laUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_la");
		ldUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_ld");
		lsUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_ls");
		lightPositionUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_light_position");
		kaUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_ka");
		kdUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_kd");
		ksUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_ks");
		materialShininessUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_material_shininess");
		lKeyPressedUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_lkey_pressed");
		textureSamplerUniform_ak = GLES32.glGetUniformLocation(shaderProgramObject_ak, "u_texture_sampler");


		final float cubePCNT_ak[] = new float[] {
			1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
        -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,

        1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
        1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,

        -1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
        1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f,
        1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f,

        -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
        -1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,

        1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
        -1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
        -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,

        1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f,
        -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f};


		GLES32.glGenVertexArrays(1, vao_ak, 0);
		GLES32.glBindVertexArray(vao_ak[0]);

		GLES32.glGenBuffers(1, vbo_ak, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_ak[0]);

		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(cubePCNT_ak.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(cubePCNT_ak);
		verticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, cubePCNT_ak.length * 4, verticesBuffer, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false,  11 * 4, (0 * 4));
		GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_POSITION);

		
		GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_COLOR, 3, GLES32.GL_FLOAT, false,  11 * 4, (3 * 4));
		GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_COLOR);

		
		GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false,  11 * 4, (6 * 4));
		GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_NORMAL);

		
		GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_TEXCOORD0, 3, GLES32.GL_FLOAT, false,  11 * 4, (9 * 4));
		GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_TEXCOORD0);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);

		GLES32.glClearDepthf(1.0f);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		stone_texture_ak = loadTexture(R.raw.marble);
		bLight_ak = false;
		Matrix.setIdentityM(perspectiveProjectionMatrix_ak, 0);

	}

	private int[] loadTexture(int fileResourceId) {
        BitmapFactory.Options options_ak = new BitmapFactory.Options();
        options_ak.inScaled = false;
        Bitmap bitmap_ak = BitmapFactory.decodeResource(context_ak.getResources(), fileResourceId, options_ak);
		
		int[] texture = new int[1];
		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1); 
		GLES32.glGenTextures(1, texture, 0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);

		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);

        GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap_ak, 0);
        GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);

        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D,0);
		return texture;
    }

	private void resize(int width, int height) {
		GLES32.glViewport(0, 0, width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix_ak, 0, 45.0f, (float) width / height, 0.1f, 100.f);
	}

	private void display() {
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		GLES32.glUseProgram(shaderProgramObject_ak);

		if (bLight_ak == true) {
			GLES32.glUniform1i(lKeyPressedUniform_ak, 1);
			GLES32.glUniform3fv(laUniform_ak, 1, lightAmbient_ak, 0);
			GLES32.glUniform3fv(ldUniform_ak, 1, lightDiffused_ak, 0);
			GLES32.glUniform3fv(lsUniform_ak, 1, lightSpecular_ak, 0);
			GLES32.glUniform4fv(lightPositionUniform_ak, 1, lightPosition_ak, 0);
			GLES32.glUniform3fv(kaUniform_ak, 1, materialAmbient_ak, 0);
			GLES32.glUniform3fv(kdUniform_ak, 1, materialDiffused_ak, 0);
			GLES32.glUniform3fv(ksUniform_ak, 1, materialSpecular_ak, 0);
			GLES32.glUniform1f(materialShininessUniform_ak, materialShininess_ak);

		} else {
			GLES32.glUniform1i(lKeyPressedUniform_ak, 0);
		}

		float modelMatrix_ak[] = new float[16];
		float viewMatrix_ak[] = new float[16];
		float translationMatrix_ak[] = new float[16];
		float xRotationMatrix_ak[] = new float[16];
		float yRotationMatrix_ak[] = new float[16];
		float zRotationMatrix_ak[] = new float[16];
		float scaleMatrix_ak[] = new float[16];

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translationMatrix_ak, 0);
		Matrix.setIdentityM(translationMatrix_ak, 0);
		Matrix.setIdentityM(xRotationMatrix_ak, 0);
		Matrix.setIdentityM(yRotationMatrix_ak, 0);
		Matrix.setIdentityM(zRotationMatrix_ak, 0);
		Matrix.setIdentityM(scaleMatrix_ak, 0);

		Matrix.translateM(translationMatrix_ak, 0, 0.0f, 0.0f, -6.0f);
		Matrix.setRotateM(xRotationMatrix_ak, 0, cubeAngle_ak, 1.0f, 0.0f, 0.0f);
		Matrix.setRotateM(yRotationMatrix_ak, 0, cubeAngle_ak, 0.0f, 1.0f, 0.0f);
		Matrix.setRotateM(zRotationMatrix_ak, 0, cubeAngle_ak, 0.0f, 0.0f, 1.0f);
		Matrix.scaleM(scaleMatrix_ak, 0, 0.75f, 0.75f, 0.75f);

		Matrix.multiplyMM(modelMatrix_ak, 0, translationMatrix_ak, 0, scaleMatrix_ak, 0);
		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, xRotationMatrix_ak, 0);
		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, yRotationMatrix_ak, 0);
		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, zRotationMatrix_ak, 0);
	
		GLES32.glUniformMatrix4fv(modelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);

		GLES32.glUniformMatrix4fv(viewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);

		GLES32.glUniformMatrix4fv(projectionMatrixUniform_ak, 1, false, perspectiveProjectionMatrix_ak, 0);


		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, stone_texture_ak[0]);
		GLES32.glUniform1i(textureSamplerUniform_ak, 0);
		GLES32.glBindVertexArray(vao_ak[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);
		GLES32.glBindVertexArray(0);

		GLES32.glUseProgram(0);

		requestRender();
	}

	private void update() {
		cubeAngle_ak = cubeAngle_ak + 0.1f;
		if (cubeAngle_ak > 360.0f) {
			cubeAngle_ak = 0.0f;
		}
	}

	private void uninitialize() {

		if (vao_ak[0] != 0) {
			GLES32.glDeleteVertexArrays(1, vao_ak, 0);
			vao_ak[0] = 0;
		}

		if (vbo_ak[0] != 0) {
			GLES32.glDeleteBuffers(1, vbo_ak, 0);
			vbo_ak[0] = 0;
		}

		if(stone_texture_ak[0] != 0){
            GLES32.glDeleteTextures(1, stone_texture_ak, 0);
            stone_texture_ak[0] = 0;
        }

		if (shaderProgramObject_ak != 0) {

			if (vertexShaderObject_ak != 0) {
				GLES32.glDetachShader(shaderProgramObject_ak, vertexShaderObject_ak);
				GLES32.glDeleteShader(vertexShaderObject_ak);
				vertexShaderObject_ak = 0;
			}

			if (fragmentShaderObject_ak != 0) {
				GLES32.glDetachShader(shaderProgramObject_ak, fragmentShaderObject_ak);
				GLES32.glDeleteShader(fragmentShaderObject_ak);
				fragmentShaderObject_ak = 0;
			}
		}

		if (shaderProgramObject_ak != 0) {
			GLES32.glDeleteProgram(shaderProgramObject_ak);
			shaderProgramObject_ak = 0;
		}
	}
}
