
package com.example.materials;

import android.content.Context;
import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLES32;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import android.opengl.Matrix;

public class GLESView extends GLSurfaceView
		implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener {
	private final Context context_ak;
	private GestureDetector gestureDetector_ak;

	private int pvVertexShaderObject_ak;
	private int pvFragmentShaderObject_ak;
	private int pvShaderProgramObject_ak;

	private int pvModelMatrixUniform_ak;
	private int pvViewMatrixUniform_ak;
	private int pvProjectionMatrixUniform_ak;
	private int gWidth = 800;
	private int	gHeight = 600;
	private int pvLKeyPressedUniform_ak;

	private int pvLaUniform_ak;
	private int pvLdUniform_ak;
	private int pvLsUniform_ak;
	private int pvLightPositionUniform_ak;

	private int pvKaUniform_ak;
	private int pvKdUniform_ak;
	private int pvKsUniform_ak;
	private int pvMaterialShininessUniform_ak;

	private int pfVertexShaderObject_ak;
	private int pfFragmentShaderObject_ak;
	private int pfShaderProgramObject_ak;

	private int pfModelMatrixUniform_ak;
	private int pfViewMatrixUniform_ak;
	private int pfProjectionMatrixUniform_ak;

	private int pfLKeyPressedUniform_ak;

	private int pfLaUniform_ak;
	private int pfLdUniform_ak;
	private int pfLsUniform_ak;
	private int pfLightPositionUniform_ak;

	private int pfKaUniform_ak;
	private int pfKdUniform_ak;
	private int pfKsUniform_ak;
	private int pfMaterialShininessUniform_ak;

	private boolean bLight_ak = false;

	private float perspectiveProjectionMatrix_ak[] = new float[16];

	private int[] vao_sphere_ak = new int[1];
	private int[] vbo_position_sphere_ak = new int[1];
	private int[] vbo_normal_sphere_ak = new int[1];
	private int[] vbo_element_sphere_ak = new int[1];

	private float lightAmbient_ak[] = new float[] {
			0.1f, 0.1f, 0.1f
	};
	private float lightDiffused_ak[] = new float[] {
			1.0f, 1.0f, 1.0f
	};
	private float lightPosition_ak[] = new float[] {
			100.0f, 100.0f, 100.0f, 1.0f
	};
	private float lightSpecular_ak[] = new float[] {
			1.0f, 1.0f, 1.0f
	};

	private float materialAmbient_ak[] = new float[] {
			0.0f, 0.0f, 0.0f, 1.0f
	};
	private float materialDiffused_ak[] = new float[] {
			0.5f, 0.2f, 0.7f, 1.0f
	};
	private float materialSpecular_ak[] = new float[] {
			0.7f, 0.7f, 0.7f, 1.0f
	};
	private float materialShininess_ak = 50.0f;

	private float sphere_vertices_ak[] = new float[1146];
	private float sphere_normals_ak[] = new float[1146];
	private float sphere_textures_ak[] = new float[764];
	private short sphere_elements_ak[] = new short[2280];

	private int numVertices_ak = 0;
	private int numElements_ak = 0;
	private int programToBeUsed_ak = 1;

	private float angleForXRotation_ak = 0.0f;
	private float angleForYRotation_ak = 0.0f;
	private float angleForZRotation_ak = 0.0f;

	private int keyPressed_ak = 0;

	GLESView(Context drawingContext_ak) {
		super(drawingContext_ak);
		context_ak = drawingContext_ak;
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		gestureDetector_ak = new GestureDetector(context_ak, this, null, false);
		gestureDetector_ak.setOnDoubleTapListener(this);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		String glesVersion_ak = gl.glGetString(GL10.GL_VERSION);
		String glslVersion_ak = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		initialize(gl);
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height) {
		resize(width, height);
	}

	@Override
	public void onDrawFrame(GL10 unused) {
		display();
		update();
	}

	@Override
	public boolean onTouchEvent(MotionEvent e) {
		int eventAction_ak = e.getAction();
		if (!gestureDetector_ak.onTouchEvent(e)) {
			super.onTouchEvent(e);
		}
		return (true);
	}

	@Override
	public boolean onDoubleTap(MotionEvent e) {
		if (programToBeUsed_ak == 0) {
			programToBeUsed_ak = 1;
		} else {
			programToBeUsed_ak = 0;
		}
		return (true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e) {
		return (true);
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e) {
		if (bLight_ak == true) {
			bLight_ak = false;
		} else {
			bLight_ak = true;
		}
		return (true);
	}

	@Override
	public boolean onDown(MotionEvent e) {
		return (true);
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
		return (true);
	}

	@Override
	public void onLongPress(MotionEvent e) {
		if (keyPressed_ak < 4) {
			keyPressed_ak = keyPressed_ak + 1;
		} else {
			keyPressed_ak = 0;
		}
		angleForXRotation_ak = 0.0f;
		angleForYRotation_ak = 0.0f;
		angleForZRotation_ak = 0.0f;
		lightPosition_ak[0] = 0.0f;
		lightPosition_ak[1] = 0.0f;
		lightPosition_ak[2] = 0.0f;
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
		uninitialize();
		System.exit(0);
		return (true);
	}

	@Override
	public void onShowPress(MotionEvent e) {
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		return (true);
	}

	private void initialize(GL10 gl) {
		pvVertexShaderObject_ak = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		final String pvVertexShaderSourceCode_ak = String.format(
			"#version 320 es" +
			"\n" +
			"precision lowp float;" +
			"precision lowp int;" +
			"in vec4 vPosition;" +
			"in vec3 vNormal;" +
			"uniform mat4 u_model_matrix;" +
			"uniform mat4 u_view_matrix;" +
			"uniform mat4 u_projection_matrix;" +
			"uniform vec3 u_la;" +
			"uniform vec3 u_ld;" +
			"uniform vec3 u_ls;" +
			"uniform vec4 u_light_position;" +
			"uniform vec3 u_ka;" +
			"uniform vec3 u_kd;" +
			"uniform vec3 u_ks;" +
			"uniform float u_material_shininess;" +
			"uniform int u_lkey_pressed;" +
			"out vec3 phong_ads_light;" +
			"void main(void)" +
			"{" +
			"if(u_lkey_pressed == 1)" +
			"{" +
			"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" +
			"vec3 transformed_normal = normalize(mat3((u_view_matrix * u_model_matrix)) * vNormal);"
						+
			"vec3 light_direction = normalize(vec3(u_light_position - eye_coordinates));"
						+
			"vec3 reflection_vector = reflect(-light_direction, transformed_normal);" +
			"vec3 view_vector = normalize(-eye_coordinates.xyz);" +
			"vec3 ambient = u_la * u_ka;" +
			"vec3 diffuse = u_ld * u_kd * max(dot(light_direction, transformed_normal), 0.0);"
						+
			"vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, view_vector), 0.0), u_material_shininess);"
						+
			"phong_ads_light = ambient + diffuse + specular;" +
			"}" +
			"else" +
			"{" +
			"phong_ads_light = vec3(1.0, 1.0, 1.0);" +
			"}" +
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
			"}");

		GLES32.glShaderSource(pvVertexShaderObject_ak, pvVertexShaderSourceCode_ak);

		GLES32.glCompileShader(pvVertexShaderObject_ak);
		int[] iShaderCompileStatus_ak = new int[1];
		int[] iInfoLogLength_ak = new int[1];
		String szInfoLog_ak = null;
		GLES32.glGetShaderiv(pvVertexShaderObject_ak, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus_ak, 0);
		if (iShaderCompileStatus_ak[0] == GLES32.GL_FALSE) {
			GLES32.glGetShaderiv(pvVertexShaderObject_ak, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength_ak, 0);
			if (iInfoLogLength_ak[0] > 0) {
				szInfoLog_ak = GLES32.glGetShaderInfoLog(pvVertexShaderObject_ak);
				uninitialize();
				System.exit(0);
			}
		}
		pvFragmentShaderObject_ak = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		final String pvFragementShaderSourceCode_ak = String.format(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			"precision lowp int;" +
			"in vec3 phong_ads_light;" +
			"out vec4 FragColor;" +
			"void main(void)" +
			"{" +
			"FragColor = vec4(phong_ads_light, 1.0);" +
			"}");

		GLES32.glShaderSource(pvFragmentShaderObject_ak, pvFragementShaderSourceCode_ak);

		GLES32.glCompileShader(pvFragmentShaderObject_ak);
		iShaderCompileStatus_ak[0] = 0;
		iInfoLogLength_ak[0] = 0;
		szInfoLog_ak = null;

		GLES32.glGetShaderiv(pvFragmentShaderObject_ak, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus_ak, 0);
		if (iShaderCompileStatus_ak[0] == GLES32.GL_FALSE) {
			GLES32.glGetShaderiv(pvFragmentShaderObject_ak, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength_ak, 0);
			if (iInfoLogLength_ak[0] > 0) {
				szInfoLog_ak = GLES32.glGetShaderInfoLog(pvFragmentShaderObject_ak);
				uninitialize();
				System.exit(0);
			}
		}

		pvShaderProgramObject_ak = GLES32.glCreateProgram();

		GLES32.glAttachShader(pvShaderProgramObject_ak, pvVertexShaderObject_ak);
		GLES32.glAttachShader(pvShaderProgramObject_ak, pvFragmentShaderObject_ak);

		GLES32.glBindAttribLocation(pvShaderProgramObject_ak, GLESMacros.ATTRIBUTE_POSITION, "vPosition");
		GLES32.glBindAttribLocation(pvShaderProgramObject_ak, GLESMacros.ATTRIBUTE_NORMAL, "vNormal");
		GLES32.glLinkProgram(pvShaderProgramObject_ak);

		int[] iShaderProgramLinkStatus_ak = new int[1];
		iInfoLogLength_ak[0] = 0;
		szInfoLog_ak = null;

		GLES32.glGetProgramiv(pvShaderProgramObject_ak, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus_ak, 0);
		if (iShaderProgramLinkStatus_ak[0] == GLES32.GL_FALSE) {
			GLES32.glGetProgramiv(pvShaderProgramObject_ak, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength_ak, 0);
			if (iInfoLogLength_ak[0] > 0) {
				szInfoLog_ak = GLES32.glGetProgramInfoLog(pvShaderProgramObject_ak);
				uninitialize();
				System.exit(0);
			}
		}
		pvModelMatrixUniform_ak = GLES32.glGetUniformLocation(pvShaderProgramObject_ak, "u_model_matrix");
		pvViewMatrixUniform_ak = GLES32.glGetUniformLocation(pvShaderProgramObject_ak, "u_view_matrix");
		pvProjectionMatrixUniform_ak = GLES32.glGetUniformLocation(pvShaderProgramObject_ak, "u_projection_matrix");
		pvLaUniform_ak = GLES32.glGetUniformLocation(pvShaderProgramObject_ak, "u_la");
		pvLdUniform_ak = GLES32.glGetUniformLocation(pvShaderProgramObject_ak, "u_ld");
		pvLsUniform_ak = GLES32.glGetUniformLocation(pvShaderProgramObject_ak, "u_ls");
		pvLightPositionUniform_ak = GLES32.glGetUniformLocation(pvShaderProgramObject_ak, "u_light_position");
		pvKaUniform_ak = GLES32.glGetUniformLocation(pvShaderProgramObject_ak, "u_ka");
		pvKdUniform_ak = GLES32.glGetUniformLocation(pvShaderProgramObject_ak, "u_kd");
		pvKsUniform_ak = GLES32.glGetUniformLocation(pvShaderProgramObject_ak, "u_ks");
		pvMaterialShininessUniform_ak = GLES32.glGetUniformLocation(pvShaderProgramObject_ak, "u_material_shininess");
		pvLKeyPressedUniform_ak = GLES32.glGetUniformLocation(pvShaderProgramObject_ak, "u_lkey_pressed");
		pfVertexShaderObject_ak = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		final String vertexShaderSourceCode_ak = String.format(
			"#version 320 es" +
			"\n" +
			"precision lowp float;" +
			"precision lowp int;" +
			"in vec4 vPosition;" +
			"in vec3 vNormal;" +
			"uniform mat4 u_model_matrix;" +
			"uniform mat4 u_view_matrix;" +
			"uniform mat4 u_projection_matrix;" +
			"uniform vec4 u_light_position;" +
			"uniform int u_lkey_pressed;" +
			"out vec3 transformed_normal;" +
			"out vec3 light_direction;" +
			"out vec3 view_vector;" +
			"void main(void)" +
			"{" +
			"if(u_lkey_pressed == 1)" +
			"{" +
			"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" +
			"transformed_normal = mat3((u_view_matrix * u_model_matrix)) * vNormal;" +
			"light_direction = vec3(u_light_position - eye_coordinates);" +
			"view_vector = -eye_coordinates.xyz;" +
			"}" +
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
			"}");

		GLES32.glShaderSource(pfVertexShaderObject_ak, vertexShaderSourceCode_ak);

		GLES32.glCompileShader(pfVertexShaderObject_ak);
		iShaderCompileStatus_ak = new int[1];
		iInfoLogLength_ak = new int[1];
		szInfoLog_ak = null;
		GLES32.glGetShaderiv(pfVertexShaderObject_ak, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus_ak, 0);
		if (iShaderCompileStatus_ak[0] == GLES32.GL_FALSE) {
			GLES32.glGetShaderiv(pfVertexShaderObject_ak, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength_ak, 0);
			if (iInfoLogLength_ak[0] > 0) {
				szInfoLog_ak = GLES32.glGetShaderInfoLog(pfVertexShaderObject_ak);
				uninitialize();
				System.exit(0);
			}
		}
		pfFragmentShaderObject_ak = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		final String fragementShaderSourceCode_ak = String.format(
			"#version 320 es" +
			"\n" +
			"precision highp float;" +
			"precision lowp int;" +
			"in vec3 transformed_normal;" +
			"in vec3 light_direction;" +
			"in vec3 view_vector;" +
			"uniform vec3 u_la;" +
			"uniform vec3 u_ld;" +
			"uniform vec3 u_ls;" +
			"uniform vec3 u_ka;" +
			"uniform vec3 u_kd;" +
			"uniform vec3 u_ks;" +
			"uniform float u_material_shininess;" +
			"uniform int u_lkey_pressed;" +
			"out vec4 FragColor;" +
			"void main(void)" +
			"{" +
			"vec3 phong_ads_light;" +
			"if(u_lkey_pressed == 1)" +
			"{" +
			"vec3 normalized_transformed_normal = normalize(transformed_normal);" +
			"vec3 normalized_light_direction = normalize(light_direction);" +
			"vec3 normalized_view_vector = normalize(view_vector);" +
			"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normal);" +
			"vec3 ambient = u_la * u_ka;" +
			"vec3 diffuse = u_ld * u_kd * max(dot(normalized_light_direction, normalized_transformed_normal), 0.0);" +
			"vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, normalized_view_vector), 0.0), u_material_shininess);" +
			"phong_ads_light = ambient + diffuse + specular;" +
			"}" +
			"else" +
			"{" +
			"phong_ads_light = vec3(1.0, 1.0, 1.0);" +
			"}" +
			"FragColor = vec4(phong_ads_light, 1.0);" +
			"}");

		GLES32.glShaderSource(pfFragmentShaderObject_ak, fragementShaderSourceCode_ak);

		GLES32.glCompileShader(pfFragmentShaderObject_ak);
		iShaderCompileStatus_ak[0] = 0;
		iInfoLogLength_ak[0] = 0;
		szInfoLog_ak = null;

		GLES32.glGetShaderiv(pfFragmentShaderObject_ak, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus_ak, 0);
		if (iShaderCompileStatus_ak[0] == GLES32.GL_FALSE) {
			GLES32.glGetShaderiv(pfFragmentShaderObject_ak, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength_ak, 0);
			if (iInfoLogLength_ak[0] > 0) {
				szInfoLog_ak = GLES32.glGetShaderInfoLog(pfFragmentShaderObject_ak);
				uninitialize();
				System.exit(0);
			}
		}

		pfShaderProgramObject_ak = GLES32.glCreateProgram();

		GLES32.glAttachShader(pfShaderProgramObject_ak, pfVertexShaderObject_ak);
		GLES32.glAttachShader(pfShaderProgramObject_ak, pfFragmentShaderObject_ak);

		GLES32.glBindAttribLocation(pfShaderProgramObject_ak, GLESMacros.ATTRIBUTE_POSITION, "vPosition");
		GLES32.glBindAttribLocation(pfShaderProgramObject_ak, GLESMacros.ATTRIBUTE_NORMAL, "vNormal");
		GLES32.glLinkProgram(pfShaderProgramObject_ak);

		iShaderProgramLinkStatus_ak = new int[1];
		iInfoLogLength_ak[0] = 0;
		szInfoLog_ak = null;

		GLES32.glGetProgramiv(pfShaderProgramObject_ak, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus_ak, 0);
		if (iShaderProgramLinkStatus_ak[0] == GLES32.GL_FALSE) {
			GLES32.glGetProgramiv(pfShaderProgramObject_ak, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength_ak, 0);
			if (iInfoLogLength_ak[0] > 0) {
				szInfoLog_ak = GLES32.glGetProgramInfoLog(pfShaderProgramObject_ak);
				uninitialize();
				System.exit(0);
			}
		}

		pfModelMatrixUniform_ak = GLES32.glGetUniformLocation(pfShaderProgramObject_ak, "u_model_matrix");
		pfViewMatrixUniform_ak = GLES32.glGetUniformLocation(pfShaderProgramObject_ak, "u_view_matrix");
		pfProjectionMatrixUniform_ak = GLES32.glGetUniformLocation(pfShaderProgramObject_ak, "u_projection_matrix");
		pfLaUniform_ak = GLES32.glGetUniformLocation(pfShaderProgramObject_ak, "u_la");
		pfLdUniform_ak = GLES32.glGetUniformLocation(pfShaderProgramObject_ak, "u_ld");
		pfLsUniform_ak = GLES32.glGetUniformLocation(pfShaderProgramObject_ak, "u_ls");
		pfLightPositionUniform_ak = GLES32.glGetUniformLocation(pfShaderProgramObject_ak, "u_light_position");
		pfKaUniform_ak = GLES32.glGetUniformLocation(pfShaderProgramObject_ak, "u_ka");
		pfKdUniform_ak = GLES32.glGetUniformLocation(pfShaderProgramObject_ak, "u_kd");
		pfKsUniform_ak = GLES32.glGetUniformLocation(pfShaderProgramObject_ak, "u_ks");
		pfMaterialShininessUniform_ak = GLES32.glGetUniformLocation(pfShaderProgramObject_ak, "u_material_shininess");
		pfLKeyPressedUniform_ak = GLES32.glGetUniformLocation(pfShaderProgramObject_ak, "u_lkey_pressed");

		Sphere sphere_ak = new Sphere();

		sphere_ak.getSphereVertexData(sphere_vertices_ak, sphere_normals_ak, sphere_textures_ak, sphere_elements_ak);
		numVertices_ak = sphere_ak.getNumberOfSphereVertices();
		numElements_ak = sphere_ak.getNumberOfSphereElements();

		GLES32.glGenVertexArrays(1, vao_sphere_ak, 0);
		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glGenBuffers(1, vbo_position_sphere_ak, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_sphere_ak[0]);

		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_vertices_ak.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(sphere_vertices_ak);
		verticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_vertices_ak.length * 4, verticesBuffer, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);

		GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_POSITION);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glGenBuffers(1, vbo_normal_sphere_ak, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_normal_sphere_ak[0]);

		byteBuffer = ByteBuffer.allocateDirect(sphere_normals_ak.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer normalBuffer = byteBuffer.asFloatBuffer();
		normalBuffer = byteBuffer.asFloatBuffer();
		normalBuffer.put(sphere_normals_ak);
		normalBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, sphere_normals_ak.length * 4, normalBuffer, GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.ATTRIBUTE_NORMAL, 3, GLES32.GL_FLOAT, false, 0, 0);

		GLES32.glEnableVertexAttribArray(GLESMacros.ATTRIBUTE_NORMAL);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glGenBuffers(1, vbo_element_sphere_ak, 0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);

		byteBuffer = ByteBuffer.allocateDirect(sphere_elements_ak.length * 2);
		byteBuffer.order(ByteOrder.nativeOrder());
		ShortBuffer elementsBuffer = byteBuffer.asShortBuffer();
		elementsBuffer.put(sphere_elements_ak);
		elementsBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER, sphere_elements_ak.length * 2, elementsBuffer, GLES32.GL_STATIC_DRAW);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);

		GLES32.glClearDepthf(1.0f);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		// GLES32.glEnable(GLES32.GL_CULL_FACE);

		GLES32.glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
		bLight_ak = false;
		Matrix.setIdentityM(perspectiveProjectionMatrix_ak, 0);

	}

	private void resize(int width, int height) {
		gHeight = height;
		gWidth = width;
		GLES32.glViewport(0, 0, width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix_ak, 0, 45.0f, (float) width / height, 0.1f, 100.f);
	}

	private void display() {
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		float modelMatrix_ak[] = new float[16];
		float viewMatrix_ak[] = new float[16];
		float translateMatrix_ak[] = new float[16];

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

		 // emerald
		 materialAmbient_ak[0] = 0.0215f;
		 materialAmbient_ak[1] = 0.1745f;
		 materialAmbient_ak[2] = 0.0215f;
		 materialAmbient_ak[3] = 1.0f;
	 
		 materialDiffused_ak[0] = 0.07568f;
		 materialDiffused_ak[1] = 0.61424f;
		 materialDiffused_ak[2] = 0.07568f;
		 materialDiffused_ak[3] = 1.0f;
	 
		 materialSpecular_ak[0] = 0.633f;
		 materialSpecular_ak[1] = 0.727811f;
		 materialSpecular_ak[2] = 0.633f;
		 materialSpecular_ak[3] = 1.0f;
	
		 materialShininess_ak = 0.6f * 128.0f;
		 GLES32.glViewport((int)(gWidth * (-0.40f)), (int)(gHeight * (0.35f)), gWidth, gHeight);
	
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

		// jade
		materialAmbient_ak[0] = 0.135f;
		materialAmbient_ak[1] = 0.2225f;
		materialAmbient_ak[2] = 0.1575f;
		materialAmbient_ak[3] = 1.0f;
	
		materialDiffused_ak[0] = 0.54f;
		materialDiffused_ak[1] = 0.89f;
		materialDiffused_ak[2] = 0.63f;
		materialDiffused_ak[3] = 1.0f;
	
		materialSpecular_ak[0] = 0.316228f;
		materialSpecular_ak[1] = 0.316228f;
		materialSpecular_ak[2] = 0.316228f;
		materialSpecular_ak[3] = 1.0f;
	
		materialShininess_ak = 0.1f * 128.0f;
		GLES32.glViewport((int)(gWidth * (-0.40f)), (int)(gHeight * (0.20f)), gWidth, gHeight);
	
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		// 3

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

	 // obsidian
	 materialAmbient_ak[0] = 0.05375f;
	 materialAmbient_ak[1] = 0.05f;
	 materialAmbient_ak[2] = 0.06625f;
	 materialAmbient_ak[3] = 1.0f;
 
	 materialDiffused_ak[0] = 0.18275f;
	 materialDiffused_ak[1] = 0.17f;
	 materialDiffused_ak[2] = 0.22525f;
	 materialDiffused_ak[3] = 1.0f;
 
	 materialSpecular_ak[0] = 0.332741f;
	 materialSpecular_ak[1] = 0.328634f;
	 materialSpecular_ak[2] = 0.346435f;
	 materialSpecular_ak[3] = 1.0f;
 
	 materialShininess_ak = 0.3f * 128.0f;
	 GLES32.glViewport((int)(gWidth * (-0.40f)), (int)(gHeight * (0.05f)), gWidth, gHeight);
 
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		// 4

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

	  // pearl
	  materialAmbient_ak[0] = 0.25f;
	  materialAmbient_ak[1] = 0.20725f;
	  materialAmbient_ak[2] = 0.20725f;
	  materialAmbient_ak[3] = 1.0f;
  
	  materialDiffused_ak[0] = 1.0f;
	  materialDiffused_ak[1] = 0.829f;
	  materialDiffused_ak[2] = 0.829f;
	  materialDiffused_ak[3] = 1.0f;
  
	  materialSpecular_ak[0] = 0.296648f;
	  materialSpecular_ak[1] = 0.296648f;
	  materialSpecular_ak[2] = 0.296648f;
	  materialSpecular_ak[3] = 1.0f;
  
	  materialShininess_ak = 0.088f * 128.0f;
	  GLES32.glViewport((int)(gWidth * (-0.40f)), (int)(gHeight * (-0.10)), gWidth, gHeight);
  
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		// 5

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

	  // ruby
	  materialAmbient_ak[0] = 0.1745f;
	  materialAmbient_ak[1] = 0.01175f;
	  materialAmbient_ak[2] = 0.01175f;
	  materialAmbient_ak[3] = 1.0f;
  
	  materialDiffused_ak[0] = 0.61424f;
	  materialDiffused_ak[1] = 0.04136f;
	  materialDiffused_ak[2] = 0.04136f;
	  materialDiffused_ak[3] = 1.0f;
  
	  materialSpecular_ak[0] = 0.727811f;
	  materialSpecular_ak[1] = 0.626959f;
	  materialSpecular_ak[2] = 0.626959f;
	  materialSpecular_ak[3] = 1.0f;
  
	  materialShininess_ak = 0.6f * 128.0f;
	  GLES32.glViewport((int)(gWidth * (-0.40f)), (int)(gHeight * (-0.25f)), gWidth, gHeight);
  
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		// 5

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

	  // turquoise
	  materialAmbient_ak[0] = 0.1f;
	  materialAmbient_ak[1] = 0.18725f;
	  materialAmbient_ak[2] = 0.1745f;
	  materialAmbient_ak[3] = 1.0f;
  
	  materialDiffused_ak[0] = 0.396f;
	  materialDiffused_ak[1] = 0.74151f;
	  materialDiffused_ak[2] = 0.69102f;
	  materialDiffused_ak[3] = 1.0f;
  
	  materialSpecular_ak[0] = 0.297254f;
	  materialSpecular_ak[1] = 0.30829f;
	  materialSpecular_ak[2] = 0.306678f;
	  materialSpecular_ak[3] = 1.0f;
  
	  materialShininess_ak = 0.1f * 128.0f;
	  GLES32.glViewport((int)(gWidth * (-0.40f)), (int)(gHeight * (-0.40f)), gWidth, gHeight);
  
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		// 7

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

	 // brass
	 materialAmbient_ak[0] = 0.329412f;
	 materialAmbient_ak[1] = 0.223529f;
	 materialAmbient_ak[2] = 0.027451f;
	 materialAmbient_ak[3] = 1.0f;
 
	 materialDiffused_ak[0] = 0.780392f;
	 materialDiffused_ak[1] = 0.568627f;
	 materialDiffused_ak[2] = 0.113725f;
	 materialDiffused_ak[3] = 1.0f;
 
	 materialSpecular_ak[0] = 0.992157f;
	 materialSpecular_ak[1] = 0.941176f;
	 materialSpecular_ak[2] = 0.807843f;
	 materialSpecular_ak[3] = 1.0f;
 
	 materialShininess_ak = 0.21794872f * 128.0f;
	 GLES32.glViewport((int)(gWidth * (-0.145f)), (int)(gHeight * (0.35f)), gWidth, gHeight);
 
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}
		// 8

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

	  // bronze
	  materialAmbient_ak[0] = 0.2125f;
	  materialAmbient_ak[1] = 0.1275f;
	  materialAmbient_ak[2] = 0.054f;
	  materialAmbient_ak[3] = 1.0f;
  
	  materialDiffused_ak[0] = 0.714f;
	  materialDiffused_ak[1] = 0.4284f;
	  materialDiffused_ak[2] = 0.18144f;
	  materialDiffused_ak[3] = 1.0f;
  
	  materialSpecular_ak[0] = 0.393548f;
	  materialSpecular_ak[1] = 0.271906f;
	  materialSpecular_ak[2] = 0.166721f;
	  materialSpecular_ak[3] = 1.0f;
  
	  materialShininess_ak = 0.2f * 128.0f;
	  GLES32.glViewport((int)(gWidth * (-0.145f)), (int)(gHeight * (0.20f)), gWidth, gHeight);
  
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		// 9

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

	 // chrome
	 materialAmbient_ak[0] = 0.25f;
	 materialAmbient_ak[1] = 0.25f;
	 materialAmbient_ak[2] = 0.25f;
	 materialAmbient_ak[3] = 1.0f;
 
	 materialDiffused_ak[0] = 0.4f;
	 materialDiffused_ak[1] = 0.4f;
	 materialDiffused_ak[2] = 0.4f;
	 materialDiffused_ak[3] = 1.0f;
 
	 materialSpecular_ak[0] = 0.774597f;
	 materialSpecular_ak[1] = 0.774597f;
	 materialSpecular_ak[2] = 0.774597f;
	 materialSpecular_ak[3] = 1.0f;
 
	 materialShininess_ak = 0.6f * 128.0f;
	 GLES32.glViewport((int)(gWidth * (-0.145f)), (int)(gHeight * (0.05f)), gWidth, gHeight);
 
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		// 10
		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

	  // copper
	  materialAmbient_ak[0] = 0.19125f;
	  materialAmbient_ak[1] = 0.0735f;
	  materialAmbient_ak[2] = 0.0225f;
	  materialAmbient_ak[3] = 1.0f;
  
	  materialDiffused_ak[0] = 0.7038f;
	  materialDiffused_ak[1] = 0.27048f;
	  materialDiffused_ak[2] = 0.0828f;
	  materialDiffused_ak[3] = 1.0f;
  
	  materialSpecular_ak[0] = 0.256777f;
	  materialSpecular_ak[1] = 0.137622f;
	  materialSpecular_ak[2] = 0.086014f;
	  materialSpecular_ak[3] = 1.0f;
  
	  materialShininess_ak = 0.1f * 128.0f;
	  GLES32.glViewport((int)(gWidth * (-0.145f)), (int)(gHeight * (-0.1f)), gWidth, gHeight);
  
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		//11 
		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

		  // gold
		  materialAmbient_ak[0] = 0.24725f;
		  materialAmbient_ak[1] = 0.1995f;
		  materialAmbient_ak[2] = 0.0745f;
		  materialAmbient_ak[3] = 1.0f;
	  
		  materialDiffused_ak[0] = 0.75164f;
		  materialDiffused_ak[1] = 0.60648f;
		  materialDiffused_ak[2] = 0.22648f;
		  materialDiffused_ak[3] = 1.0f;
	  
		  materialSpecular_ak[0] = 0.628281f;
		  materialSpecular_ak[1] = 0.555802f;
		  materialSpecular_ak[2] = 0.366065f;
		  materialSpecular_ak[3] = 1.0f;
	  
		  materialShininess_ak = 0.4f * 128.0f;
		  GLES32.glViewport((int)(gWidth * (-0.145f)), (int)(gHeight * (-0.25f)), gWidth, gHeight);
	  
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		// 12

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

		// silver
		materialAmbient_ak[0] = 0.19225f;
		materialAmbient_ak[1] = 0.19225f;
		materialAmbient_ak[2] = 0.19225f;
		materialAmbient_ak[3] = 1.0f;
	
		materialDiffused_ak[0] = 0.50754f;
		materialDiffused_ak[1] = 0.50754f;
		materialDiffused_ak[2] = 0.50754f;
		materialDiffused_ak[3] = 1.0f;
	
		materialSpecular_ak[0] = 0.508273f;
		materialSpecular_ak[1] = 0.508273f;
		materialSpecular_ak[2] = 0.508273f;
		materialSpecular_ak[3] = 1.0f;
	
		materialShininess_ak = 0.4f * 128.0f;
		GLES32.glViewport((int)(gWidth * (-0.145f)), (int)(gHeight * (-0.40f)), gWidth, gHeight);
	
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		// 13

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

		 // black
		 materialAmbient_ak[0] = 0.0f;
		 materialAmbient_ak[1] = 0.0f;
		 materialAmbient_ak[2] = 0.0f;
		 materialAmbient_ak[3] = 1.0f;
	 
		 materialDiffused_ak[0] = 0.01f;
		 materialDiffused_ak[1] = 0.01f;
		 materialDiffused_ak[2] = 0.01f;
		 materialDiffused_ak[3] = 1.0f;
	 
		 materialSpecular_ak[0] = 0.50f;
		 materialSpecular_ak[1] = 0.50f;
		 materialSpecular_ak[2] = 0.50f;
		 materialSpecular_ak[3] = 1.0f;
	 
		 materialShininess_ak = 0.25f * 128.0f;
		 GLES32.glViewport((int)(gWidth * (0.145f)), (int)(gHeight * (0.35f)), gWidth, gHeight);
	 
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		// 14

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

		// cyan
    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.1f;
    materialAmbient_ak[2] = 0.06f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.0f;
    materialDiffused_ak[1] = 0.50980392f;
    materialDiffused_ak[2] = 0.50980392f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.50196078f;
    materialSpecular_ak[1] = 0.50196078f;
    materialSpecular_ak[2] = 0.50196078f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.25f * 128.0f;
    GLES32.glViewport((int)(gWidth * (0.145f)), (int)(gHeight * (0.20f)), gWidth, gHeight);

		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		// 15

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

		// green
		materialAmbient_ak[0] = 0.0f;
		materialAmbient_ak[1] = 0.0f;
		materialAmbient_ak[2] = 0.0f;
		materialAmbient_ak[3] = 1.0f;
	
		materialDiffused_ak[0] = 0.1f;
		materialDiffused_ak[1] = 0.35f;
		materialDiffused_ak[2] = 0.1f;
		materialDiffused_ak[3] = 1.0f;
	
		materialSpecular_ak[0] = 0.45f;
		materialSpecular_ak[1] = 0.55f;
		materialSpecular_ak[2] = 0.45f;
		materialSpecular_ak[3] = 1.0f;
	
		materialShininess_ak = 0.25f * 128.0f;
		GLES32.glViewport((int)(gWidth * (0.145f)), (int)(gHeight * (0.05f)), gWidth, gHeight);
	
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		// 16

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

		// red
		materialAmbient_ak[0] = 0.0f;
		materialAmbient_ak[1] = 0.0f;
		materialAmbient_ak[2] = 0.0f;
		materialAmbient_ak[3] = 1.0f;
	
		materialDiffused_ak[0] = 0.5f;
		materialDiffused_ak[1] = 0.0f;
		materialDiffused_ak[2] = 0.0f;
		materialDiffused_ak[3] = 1.0f;
	
		materialSpecular_ak[0] = 0.7f;
		materialSpecular_ak[1] = 0.6f;
		materialSpecular_ak[2] = 0.6f;
		materialSpecular_ak[3] = 1.0f;
	
		materialShininess_ak = 0.25f * 128.0f;
		GLES32.glViewport((int)(gWidth * (0.145f)), (int)(gHeight * (-0.10f)), gWidth, gHeight);
	
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		// 17

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

		// white
		materialAmbient_ak[0] = 0.0f;
		materialAmbient_ak[1] = 0.0f;
		materialAmbient_ak[2] = 0.0f;
		materialAmbient_ak[3] = 1.0f;
	
		materialDiffused_ak[0] = 0.55f;
		materialDiffused_ak[1] = 0.55f;
		materialDiffused_ak[2] = 0.55f;
		materialDiffused_ak[3] = 1.0f;
	
		materialSpecular_ak[0] = 0.70f;
		materialSpecular_ak[1] = 0.70f;
		materialSpecular_ak[2] = 0.70f;
		materialSpecular_ak[3] = 1.0f;
	
		materialShininess_ak = 0.25f * 128.0f;
		GLES32.glViewport((int)(gWidth * (0.145f)), (int)(gHeight * (-0.25f)), gWidth, gHeight);
	
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		// 18

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

		// yellow plastic
		materialAmbient_ak[0] = 0.0f;
		materialAmbient_ak[1] = 0.0f;
		materialAmbient_ak[2] = 0.0f;
		materialAmbient_ak[3] = 1.0f;
	
		materialDiffused_ak[0] = 0.5f;
		materialDiffused_ak[1] = 0.5f;
		materialDiffused_ak[2] = 0.0f;
		materialDiffused_ak[3] = 1.0f;
	
		materialSpecular_ak[0] = 0.60f;
		materialSpecular_ak[1] = 0.60f;
		materialSpecular_ak[2] = 0.50f;
		materialSpecular_ak[3] = 1.0f;
	
		materialShininess_ak = 0.25f * 128.0f;
		GLES32.glViewport((int)(gWidth * (0.145f)), (int)(gHeight * (-0.40f)), gWidth, gHeight);
	
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		// 19

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

		 // black
		 materialAmbient_ak[0] = 0.02f;
		 materialAmbient_ak[1] = 0.02f;
		 materialAmbient_ak[2] = 0.02f;
		 materialAmbient_ak[3] = 1.0f;
	 
		 materialDiffused_ak[0] = 0.01f;
		 materialDiffused_ak[1] = 0.01f;
		 materialDiffused_ak[2] = 0.01f;
		 materialDiffused_ak[3] = 1.0f;
	 
		 materialSpecular_ak[0] = 0.4f;
		 materialSpecular_ak[1] = 0.4f;
		 materialSpecular_ak[2] = 0.4f;
		 materialSpecular_ak[3] = 1.0f;
	 
		 materialShininess_ak = 0.078125f * 128.0f;
		 GLES32.glViewport((int)(gWidth * (0.40f)), (int)(gHeight * (0.35f)), gWidth, gHeight);
	 
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		//20

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

		// cyan
		materialAmbient_ak[0] = 0.0f;
		materialAmbient_ak[1] = 0.05f;
		materialAmbient_ak[2] = 0.05f;
		materialAmbient_ak[3] = 1.0f;
	
		materialDiffused_ak[0] = 0.4f;
		materialDiffused_ak[1] = 0.5f;
		materialDiffused_ak[2] = 0.5f;
		materialDiffused_ak[3] = 1.0f;
	
		materialSpecular_ak[0] = 0.04f;
		materialSpecular_ak[1] = 0.7f;
		materialSpecular_ak[2] = 0.7f;
		materialSpecular_ak[3] = 1.0f;
	
		materialShininess_ak = 0.078125f * 128.0f;
		GLES32.glViewport((int)(gWidth * (0.40f)), (int)(gHeight * (0.20f)), gWidth, gHeight);
	
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		// 21

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

		// green
		materialAmbient_ak[0] = 0.0f;
		materialAmbient_ak[1] = 0.05f;
		materialAmbient_ak[2] = 0.0f;
		materialAmbient_ak[3] = 1.0f;
	
		materialDiffused_ak[0] = 0.4f;
		materialDiffused_ak[1] = 0.5f;
		materialDiffused_ak[2] = 0.4f;
		materialDiffused_ak[3] = 1.0f;
	
		materialSpecular_ak[0] = 0.04f;
		materialSpecular_ak[1] = 0.7f;
		materialSpecular_ak[2] = 0.04f;
		materialSpecular_ak[3] = 1.0f;
	
		materialShininess_ak = 0.078125f * 128.0f;
		GLES32.glViewport((int)(gWidth * (0.40f)), (int)(gHeight * (0.05f)), gWidth, gHeight);
	
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		// 22

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

		// red
		materialAmbient_ak[0] = 0.05f;
		materialAmbient_ak[1] = 0.0f;
		materialAmbient_ak[2] = 0.0f;
		materialAmbient_ak[3] = 1.0f;
	
		materialDiffused_ak[0] = 0.5f;
		materialDiffused_ak[1] = 0.4f;
		materialDiffused_ak[2] = 0.4f;
		materialDiffused_ak[3] = 1.0f;
	
		materialSpecular_ak[0] = 0.7f;
		materialSpecular_ak[1] = 0.04f;
		materialSpecular_ak[2] = 0.04f;
		materialSpecular_ak[3] = 1.0f;
	
		materialShininess_ak = 0.078125f * 128.0f;
		GLES32.glViewport((int)(gWidth * (0.40f)), (int)(gHeight * (-0.10f)), gWidth, gHeight);
	
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		// 23

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

		 // white
		 materialAmbient_ak[0] = 0.05f;
		 materialAmbient_ak[1] = 0.05f;
		 materialAmbient_ak[2] = 0.05f;
		 materialAmbient_ak[3] = 1.0f;
	 
		 materialDiffused_ak[0] = 0.5f;
		 materialDiffused_ak[1] = 0.5f;
		 materialDiffused_ak[2] = 0.5f;
		 materialDiffused_ak[3] = 1.0f;
	 
		 materialSpecular_ak[0] = 0.7f;
		 materialSpecular_ak[1] = 0.7f;
		 materialSpecular_ak[2] = 0.7f;
		 materialSpecular_ak[3] = 1.0f;
	 
		 materialShininess_ak = 0.078125f * 128.0f;
		 GLES32.glViewport((int)(gWidth * (0.40f)), (int)(gHeight * (-0.25f)), gWidth, gHeight);
	 
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		// 24

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

		// yellow rubber
		materialAmbient_ak[0] = 0.05f;
		materialAmbient_ak[1] = 0.05f;
		materialAmbient_ak[2] = 0.0f;
		materialAmbient_ak[3] = 1.0f;
	
		materialDiffused_ak[0] = 0.5f;
		materialDiffused_ak[1] = 0.5f;
		materialDiffused_ak[2] = 0.4f;
		materialDiffused_ak[3] = 1.0f;
	
		materialSpecular_ak[0] = 0.7f;
		materialSpecular_ak[1] = 0.7f;
		materialSpecular_ak[2] = 0.04f;
		materialSpecular_ak[3] = 1.0f;
	
		materialShininess_ak = 0.078125f * 128.0f;
		GLES32.glViewport((int)(gWidth * (0.40f)), (int)(gHeight * (-0.40f)), gWidth, gHeight);
	
		if (programToBeUsed_ak == 1) {
			GLES32.glUseProgram(pfShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pfLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pfLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pfLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pfLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pfKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pfKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pfKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pfMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pfLKeyPressedUniform_ak, 0);
			}
		} else {
			GLES32.glUseProgram(pvShaderProgramObject_ak);
			if (bLight_ak == true) {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 1);
				GLES32.glUniform3fv(pvLaUniform_ak, 1, lightAmbient_ak, 0);
				GLES32.glUniform3fv(pvLdUniform_ak, 1, lightDiffused_ak, 0);
				GLES32.glUniform3fv(pvLsUniform_ak, 1, lightSpecular_ak, 0);
				GLES32.glUniform4fv(pvLightPositionUniform_ak, 1, lightPosition_ak, 0);
				GLES32.glUniform3fv(pvKaUniform_ak, 1, materialAmbient_ak, 0);
				GLES32.glUniform3fv(pvKdUniform_ak, 1, materialDiffused_ak, 0);
				GLES32.glUniform3fv(pvKsUniform_ak, 1, materialSpecular_ak, 0);
				GLES32.glUniform1f(pvMaterialShininessUniform_ak, materialShininess_ak);

			} else {
				GLES32.glUniform1i(pvLKeyPressedUniform_ak, 0);
			}
		}

		Matrix.setIdentityM(modelMatrix_ak, 0);
		Matrix.setIdentityM(viewMatrix_ak, 0);
		Matrix.setIdentityM(translateMatrix_ak, 0);

		Matrix.translateM(translateMatrix_ak, 0, 0.0f, 0.0f, -12.0f);

		Matrix.multiplyMM(modelMatrix_ak, 0, modelMatrix_ak, 0, translateMatrix_ak, 0);

		if (programToBeUsed_ak == 1) {
			GLES32.glUniformMatrix4fv(pfModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pfProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		} else {
			GLES32.glUniformMatrix4fv(pvModelMatrixUniform_ak, 1, false, modelMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvViewMatrixUniform_ak, 1, false, viewMatrix_ak, 0);
			GLES32.glUniformMatrix4fv(pvProjectionMatrixUniform_ak, 1, false,
					perspectiveProjectionMatrix_ak, 0);
		}

		GLES32.glBindVertexArray(vao_sphere_ak[0]);

		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements_ak, GLES32.GL_UNSIGNED_SHORT, 0);

		GLES32.glBindVertexArray(0);

		GLES32.glUseProgram(0);

		requestRender();
	}

	private void update() {
		angleForXRotation_ak = angleForXRotation_ak + 0.01f;
	if (angleForXRotation_ak > 2 * Math.PI)
		angleForXRotation_ak = 0.0f;

	if (keyPressed_ak == 1) {
		lightPosition_ak[1] = (float)(100 * Math.sin(angleForXRotation_ak));
		lightPosition_ak[2] = (float)(100 * Math.cos(angleForXRotation_ak));
	}

	angleForYRotation_ak = angleForYRotation_ak + 0.01f;
	if (angleForYRotation_ak > 2 * Math.PI)
		angleForYRotation_ak = 0.0f;
	if (keyPressed_ak == 2) {
		lightPosition_ak[0] = (float)(100 * Math.sin(angleForYRotation_ak));
		lightPosition_ak[2] = (float)(100 * Math.cos(angleForYRotation_ak));
	}

	angleForZRotation_ak = angleForZRotation_ak + 0.01f;
	if (angleForZRotation_ak > 2 * Math.PI)
		angleForZRotation_ak = 0.0f;
	if (keyPressed_ak == 3) {
		lightPosition_ak[0] = (float)(100 * Math.sin(angleForZRotation_ak));
		lightPosition_ak[1] = (float)(100 * Math.cos(angleForZRotation_ak));
	}
	}

	private void uninitialize() {

		if (vao_sphere_ak[0] != 0) {
			GLES32.glDeleteVertexArrays(1, vao_sphere_ak, 0);
			vao_sphere_ak[0] = 0;
		}

		if (vbo_position_sphere_ak[0] != 0) {
			GLES32.glDeleteBuffers(1, vbo_position_sphere_ak, 0);
			vbo_position_sphere_ak[0] = 0;
		}

		if (vbo_normal_sphere_ak[0] != 0) {
			GLES32.glDeleteBuffers(1, vbo_normal_sphere_ak, 0);
			vbo_normal_sphere_ak[0] = 0;
		}

		if (vbo_element_sphere_ak[0] != 0) {
			GLES32.glDeleteBuffers(1, vbo_element_sphere_ak, 0);
			vbo_element_sphere_ak[0] = 0;
		}

		if (pfShaderProgramObject_ak != 0) {

			if (pfVertexShaderObject_ak != 0) {
				GLES32.glDetachShader(pfShaderProgramObject_ak, pfVertexShaderObject_ak);
				GLES32.glDeleteShader(pfVertexShaderObject_ak);
				pfVertexShaderObject_ak = 0;
			}

			if (pfFragmentShaderObject_ak != 0) {
				GLES32.glDetachShader(pfShaderProgramObject_ak, pfFragmentShaderObject_ak);
				GLES32.glDeleteShader(pfFragmentShaderObject_ak);
				pfFragmentShaderObject_ak = 0;
			}
		}

		if (pfShaderProgramObject_ak != 0) {
			GLES32.glDeleteProgram(pfShaderProgramObject_ak);
			pfShaderProgramObject_ak = 0;
		}

		if (pvShaderProgramObject_ak != 0) {

			if (pvVertexShaderObject_ak != 0) {
				GLES32.glDetachShader(pvShaderProgramObject_ak, pvVertexShaderObject_ak);
				GLES32.glDeleteShader(pvVertexShaderObject_ak);
				pvVertexShaderObject_ak = 0;
			}

			if (pvFragmentShaderObject_ak != 0) {
				GLES32.glDetachShader(pvShaderProgramObject_ak, pvFragmentShaderObject_ak);
				GLES32.glDeleteShader(pvFragmentShaderObject_ak);
				pvFragmentShaderObject_ak = 0;
			}
		}

		if (pvShaderProgramObject_ak != 0) {
			GLES32.glDeleteProgram(pvShaderProgramObject_ak);
			pvShaderProgramObject_ak = 0;
		}
	}
}
