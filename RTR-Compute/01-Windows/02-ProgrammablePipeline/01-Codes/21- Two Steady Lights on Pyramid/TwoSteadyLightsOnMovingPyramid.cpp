#include <windows.h>
#include <stdio.h> 
#include "Icon.h"

#include <gl\glew.h>

#include <gl\GL.h>

#include "vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

enum
{
	ATTRIBUTE_POSITION = 0,
	ATTRIBUTE_COLOR,
	ATTRIBUTE_NORMAL,
	ATRIBUTE_TEXCOORD,
};

using namespace vmath;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gpFile_ak = NULL;

HWND ghwnd_ak = NULL;
HDC ghdc_ak = NULL;
HGLRC ghrc_ak = NULL;

DWORD dwStyle_ak;
WINDOWPLACEMENT wpPrev_ak = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow_ak = false;
bool gbEscapeKeyIsPressed_ak = false;
bool gbFullscreen_ak = false;

GLuint gVertexShaderObject_ak;
GLuint gFragmentShaderObject_ak;
GLuint gShaderProgramObject_ak;

GLuint mvpMatrixUniform_ak;

GLuint Vao_pyramid_ak;
GLuint Vbo_position_pyramid_ak;
GLuint Vbo_normals_pyramid_ak;

//Uniforms
GLuint ldUniform_ak;
GLuint kdUniform_ak;
GLuint lightPositionUniform_ak;

GLuint laUniform_ak;
GLuint kaUniform_ak;

GLuint lsUniform_ak;
GLuint ksUniform_ak;

GLuint shininessUniform_ak;

GLuint modelUniform_ak;
GLuint viewUniform_ak;
GLuint projectionUniform_ak;

GLuint lKeyPressedUniform_ak;

struct Light
{
	GLfloat lightAmbient_ak[3];
	GLfloat lightDiffuse_ak[3];
	GLfloat lightSpecular_ak[3];
	GLfloat lightPosition_ak[4];
};
struct Light light[2];

GLfloat materialAmbient_ak[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat materialDiffuse_ak[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular_ak[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess_ak = 128.0f;

float pAngle = 0.0f;

bool isLkey = false;
bool bLight;



mat4 perspectiveProjectMatrix;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update();

	WNDCLASSEX wndclass_ak;
	HWND hwnd_ak;
	MSG msg_ak;
	TCHAR szClassName_ak[] = TEXT("OpenGLPP");
	bool bDone_ak = false;

	if (fopen_s(&gpFile_ak, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile_ak, "Log File Is Successfully Opened.\n");
	}

	int iScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	int iScreenHeight = GetSystemMetrics(SM_CYSCREEN);

	int iScreenCenterX = iScreenWidth / 2;
	int iScreenCenterY = iScreenHeight / 2;

	int iWindowCenterX = WIN_WIDTH / 2;
	int iWindowsCenterY = WIN_HEIGHT / 2;

	int iWindowX = iScreenCenterX - iWindowCenterX;
	int iWindowY = iScreenCenterY - iWindowsCenterY;

	wndclass_ak.cbSize = sizeof(WNDCLASSEX);
	wndclass_ak.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass_ak.cbClsExtra = 0;
	wndclass_ak.cbWndExtra = 0;
	wndclass_ak.hInstance = hInstance;
	wndclass_ak.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass_ak.hIcon = LoadIcon(NULL, MAKEINTRESOURCE(MY_ICON));
	wndclass_ak.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass_ak.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));
	wndclass_ak.lpfnWndProc = WndProc;
	wndclass_ak.lpszClassName = szClassName_ak;
	wndclass_ak.lpszMenuName = NULL;

	RegisterClassEx(&wndclass_ak);

	hwnd_ak = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName_ak,
		TEXT("PP: Two lights on moving Pyramid"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		iWindowX,
		iWindowY,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd_ak = hwnd_ak;

	ShowWindow(hwnd_ak, iCmdShow);
	SetForegroundWindow(hwnd_ak);
	SetFocus(hwnd_ak);

	initialize();

	while (bDone_ak == false)
	{
		if (PeekMessage(&msg_ak, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_ak.message == WM_QUIT)
				bDone_ak = true;
			else
			{
				TranslateMessage(&msg_ak);
				DispatchMessage(&msg_ak);
			}
		}
		else
		{
			update();
			display();

			if (gbActiveWindow_ak == true)
			{
				if (gbEscapeKeyIsPressed_ak == true)
					bDone_ak = true;
			}
		}
	}

	uninitialize();

	return((int)msg_ak.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow_ak = true;
		else
			gbActiveWindow_ak = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed_ak = true;
			break;
		case 0x46:
			if (gbFullscreen_ak == false)
			{
				ToggleFullscreen();
				gbFullscreen_ak = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen_ak = false;
			}
			break;
		case 0x4C:
			if (isLkey == false)
			{
				bLight = true;
				isLkey = true;
			}
			else
			{
				bLight = false;
				isLkey = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if (gbFullscreen_ak == false)
	{
		dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);
		if (dwStyle_ak & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd_ak, &wpPrev_ak) && GetMonitorInfo(MonitorFromWindow(ghwnd_ak, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd_ak, GWL_STYLE, dwStyle_ak & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd_ak, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		SetWindowLong(ghwnd_ak, GWL_STYLE, dwStyle_ak | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
		SetWindowPos(ghwnd_ak, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	void uninitialize(void);
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc_ak = GetDC(ghwnd_ak);

	iPixelFormatIndex = ChoosePixelFormat(ghdc_ak, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	if (SetPixelFormat(ghdc_ak, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	ghrc_ak = wglCreateContext(ghdc_ak);
	if (ghrc_ak == NULL)
	{
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	if (wglMakeCurrent(ghdc_ak, ghrc_ak) == false)
	{
		wglDeleteContext(ghrc_ak);
		ghrc_ak = NULL;
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc_ak);
		ghrc_ak = NULL;
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	fprintf(gpFile_ak, "\nOpenGL Vendor : %s \n",glGetString(GL_VENDOR));
	fprintf(gpFile_ak, "\nOpenGL Renderer : %s \n",glGetString(GL_RENDERER));
	fprintf(gpFile_ak, "\nOpenGL Version : %s \n",glGetString(GL_VERSION));
	fprintf(gpFile_ak, "\nOpenGL GLSL Version : %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	GLint numExten_ak;

	glGetIntegerv(GL_NUM_EXTENSIONS,&numExten_ak);
	for (int i=0; i<numExten_ak; i++)
	{
		fprintf(gpFile_ak,"\nOpenGL Enabled Extensions:%s\n",glGetStringi(GL_EXTENSIONS,i));
	}

	//Vertex Shader
	//********** Vertex Shader runs per vertex **********
	/* 1. When we specify core, we tell OpenGL to use Programmable Pipeline. i.e Core Profile
	   2. in/out are glsl language specifier. Known to shader language only.
	      in---> loads incoming data from main program. It loads data only once.
	   3. vec4--> [x,y,z,w]
	   4. uniform---> load incoming data from main program. It loads data multiple times
	   5. gl_Position---> in-built variable of Vertex Shader
	*/
	// out_color is sent as an input to Fragment Shader
	gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* vertexShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;"
		"uniform mat4 u_model_matrix;"
		"uniform mat4 u_view_matrix;"
		"uniform mat4 u_projection_matrix;"
		"uniform int u_LKeyPressed;"
		"uniform vec3 u_Ld[2];"
		"uniform vec3 u_Kd;"
		"uniform vec3 u_La[2];"
		"uniform vec3 u_Ka;"
		"uniform vec3 u_Ls[2];"
		"uniform vec3 u_Ks;"
		"uniform vec4 u_light_position[2];"
		"uniform float u_shininess;"
		"out vec3 phong_ads_light;"
		"void main(void)" \
		"{" \

		"if(u_LKeyPressed==1 )"
		"{"
			"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;"
			"vec3 transformedNormal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"
			"vec3 view_vector = normalize(-eyeCoordinates.xyz);"
			"phong_ads_light = vec3(0.0f,0.0f,0.0f);"
		    "vec3 light_direction[2];"
		    "vec3 reflection_vector[2];"
			"vec3 ambient[2];"
			"vec3 diffuse[2];"
			"vec3 specular[2];"
			"for(int i=0;i<2;i++)"
			"{" 
				"light_direction[i] = normalize(vec3(u_light_position[i] - eyeCoordinates));"
				"reflection_vector[i] = reflect(-light_direction[i], transformedNormal);"
				"ambient[i]= u_La[i] * u_Ka;"
				"diffuse[i] = u_Ld[i] * u_Kd * max(dot(light_direction[i], transformedNormal),0.0);"
				"specular[i]= u_Ls[i] * u_Ks * pow(max(dot(reflection_vector[i],view_vector),0.0),u_shininess);"
				
				"phong_ads_light = phong_ads_light + ambient[i] + diffuse[i] + specular[i];"

			"}"
		"}"
		"else"
		"{"
			"phong_ads_light = vec3(1.0f,1.0f,1.0f);"
		"}"

		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"
	"}";
	
	glShaderSource(gVertexShaderObject_ak,1,(const GLchar **)&vertexShaderSourceCode, NULL);

	// compile Vertex Shader
	glCompileShader(gVertexShaderObject_ak);

	// Error checking for Vertex Shader
	GLint infoLogLength = 0; 
	GLint shaderCompiledStatus = 0;
	char *szBuffer = NULL;

	glGetShaderiv(gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if(shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength>0)
		{
			szBuffer = (char *)malloc(infoLogLength);
			if(szBuffer!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_ak, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Vertex Shader Compilation Log: %s\n",szBuffer);
				free(szBuffer);
				szBuffer = NULL;
				DestroyWindow(ghwnd_ak);
			}
		}
	}

	//Fragment Shader
	/* out_color is the output of Vertex Shader */
	gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar *vertexFragmentSourceCode = 
	"#version 460 core" \
	"\n" \
	"out vec4 FragColor;" \
	"in vec3 phong_ads_light;"
	"void main(void)" \
	"{" \
		"FragColor = vec4(phong_ads_light,1.0f);" \
	"}";

	glShaderSource(gFragmentShaderObject_ak,1,(const GLchar **)&vertexFragmentSourceCode, NULL);

	// compile Fragment Shader
	glCompileShader(gFragmentShaderObject_ak);

	// Error Checking for Fragment Shader
	glGetShaderiv(gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if(shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength>0)
		{
			szBuffer = (char *)malloc(infoLogLength);
			if(szBuffer!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_ak, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Fragment Shader Compilation Log: %s\n",szBuffer);
				free(szBuffer);
				szBuffer = NULL;
				DestroyWindow(ghwnd_ak);
			}
		}
	}

	//Shader Program
	gShaderProgramObject_ak = glCreateProgram();
	glAttachShader(gShaderProgramObject_ak,gVertexShaderObject_ak);
	glAttachShader(gShaderProgramObject_ak,gFragmentShaderObject_ak);

	// Bind the attributes in shader with the enums in your main program
	/* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
	glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_NORMAL, "vNormal");

	// Linking
	glLinkProgram(gShaderProgramObject_ak);

	// Linking Error Checking
	GLint shaderProgramLinkStatus = 0;
	szBuffer = NULL;

	glGetProgramiv(gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
	if(shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength>0)
		{
			szBuffer = (char *)malloc(infoLogLength);
			if(szBuffer!=NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_ak, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Shader Program Link Log: %s\n",szBuffer);
				free(szBuffer);
				szBuffer = NULL;
				DestroyWindow(ghwnd_ak);
			}
		}
	}

	//Get the information of uniform Post linking
	modelUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_model_matrix");
	viewUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_view_matrix");
	projectionUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_projection_matrix");

	ldUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_Ld");
	kdUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_Kd");
	lightPositionUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_light_position");

	laUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_La");
	kaUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_Ka");

	lsUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_Ls");
	ksUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_Ks");

	shininessUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_shininess");

	lKeyPressedUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_LKeyPressed");

	//Pyramid
	//Vertices Array Declaration
	const GLfloat pyramidVertices[]=
	{   
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,

		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,

		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f
	};

	//Normals 
	const GLfloat pyramidNormals[] =
	{
		 0.0f, 0.447214f,0.894427f,
		 0.0f, 0.447214f,0.894427f,
		 0.0f, 0.447214f,0.894427f,

		 0.894427f,0.447214f,0.0f,
		 0.894427f,0.447214f,0.0f,
		 0.894427f,0.447214f,0.0f,

		 0.0f,0.447214f,-0.894427f,
		 0.0f,0.447214f,-0.894427f,
		 0.0f,0.447214f,-0.894427f,

		 -0.894427f, 0.447214f, 0.0f,
		 -0.894427f, 0.447214f, 0.0f,
		 -0.894427f, 0.447214f, 0.0f
	};

	//Pyramid
	//Repeat the below steps of Vbo_position and call them in draw method
	glGenVertexArrays(1, &Vao_pyramid_ak);
	glBindVertexArray(Vao_pyramid_ak);

	// Push the above vertices to vPosition

	//Steps
	/* 1. Tell OpenGl to create one buffer in your VRAM
	      Give me a symbol to identify. It is known as NamedBuffer
	      In OpenGL terminology, it is called as GL_ARRAY_BUFFER. This is becase vertex has plenty of attributes 
		  like color, texture, etc. Also it requires contiguous memory.
		  User identifies this variable as Vbo_position and GPU as GL_ARRAY_BUFFER. 
	   2. Bind with the above symbol. It doesn't unbind until 'unbind step' is performed eg- Railway track
	   3. Insert triangle data into the buffer.
	   4. Specify where to insert this data into shader and also how to use it.
	   5. Enable the 'in' point. 
	   6. Unbind 
	*/
    // Pyramid
	//Position
	glGenBuffers(1, &Vbo_position_pyramid_ak);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_pyramid_ak);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
	// 3 is specified for 3 pairs for vertices
	/* For Texture, specify 2*/
    // 4th parameter----> Normalized Co-ordinates
	// 5th How many strides to take?
	// 6th From which position	
	glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);  //change tracks to link different attributes

	//Normals
	glGenBuffers(1, &Vbo_normals_pyramid_ak);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_normals_pyramid_ak);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormals), pyramidNormals, GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbind Vao
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);          
	glClearColor(0.25f, 0.25f, 0.25f, 0.0f);

	// Set PerspectiveMatrix to identity matrix 
	perspectiveProjectMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}


void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject_ak);

	GLfloat lightAmbient[8] = {
				light[0].lightAmbient_ak[0] = 0.0f,
				light[0].lightAmbient_ak[1] = 0.0f,
				light[0].lightAmbient_ak[2] = 0.0f,

				light[1].lightAmbient_ak[0] = 0.0f,
				light[1].lightAmbient_ak[1] = 0.0f,
				light[1].lightAmbient_ak[2] = 0.0f,
	};

	GLfloat lightDiffuse[8] = {
			light[0].lightDiffuse_ak[0] = 1.0f,
			light[0].lightDiffuse_ak[1] = 0.0f,
			light[0].lightDiffuse_ak[2] = 0.0f,

			light[1].lightDiffuse_ak[0] = 0.0f,
			light[1].lightDiffuse_ak[1] = 0.0f,
			light[1].lightDiffuse_ak[2] = 1.0f,
	};

	GLfloat lightSpecular[8] = {
			light[0].lightSpecular_ak[0] = 1.0f,
			light[0].lightSpecular_ak[1] = 0.0f,
			light[0].lightSpecular_ak[2] = 0.0f,

			light[1].lightSpecular_ak[0] = 0.0f,
			light[1].lightSpecular_ak[1] = 0.0f,
			light[1].lightSpecular_ak[2] = 1.0f,
	};

	GLfloat lightPosition[8] = {
			light[0].lightPosition_ak[0] = 2.0f,
			light[0].lightPosition_ak[1] = 0.0f,
			light[0].lightPosition_ak[2] = 0.0f,
			light[0].lightPosition_ak[3] = 1.0f,

			light[1].lightPosition_ak[0] = -2.0f,
			light[1].lightPosition_ak[1] = 0.0f,
			light[1].lightPosition_ak[2] = 0.0f,
			light[1].lightPosition_ak[3] = 1.0f
	};

	if (bLight == true)
	{
			glUniform1i(lKeyPressedUniform_ak, 1);
			glUniform3fv(laUniform_ak, 2, lightAmbient);
			glUniform3fv(ldUniform_ak, 2, lightDiffuse);
			glUniform3fv(lsUniform_ak, 2, lightSpecular);
			glUniform4fv(lightPositionUniform_ak, 2, lightPosition);	


		glUniform3fv(kaUniform_ak, 1, materialAmbient_ak);
		glUniform3fv(kdUniform_ak, 1, materialDiffuse_ak);
		glUniform3fv(ksUniform_ak, 1, materialSpecular_ak);
		glUniform1f(shininessUniform_ak, materialShininess_ak);

	}
	else
		glUniform1i(lKeyPressedUniform_ak, 0);


	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 translationMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();


	translationMatrix = translate(0.0f, 0.0f, -5.0f);
	rotationMatrix = rotate(pAngle, 0.0f, 1.0f, 0.0f);
	modelMatrix = translationMatrix * rotationMatrix;

	glUniformMatrix4fv(modelUniform_ak, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewUniform_ak, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionUniform_ak, 1, GL_FALSE, perspectiveProjectMatrix);

	//Pyramid Begin
	glBindVertexArray(Vao_pyramid_ak);
	glDrawArrays(GL_TRIANGLES, 0, 12);

	glBindVertexArray(0); // Pyramid end

	glUseProgram(0);

	SwapBuffers(ghdc_ak);
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	perspectiveProjectMatrix = perspective(45.0f, ((GLfloat)width/(GLfloat)height),0.1f, 100.0f);
}

void update()
{
	pAngle+= 0.1f;
	if(pAngle >= 360.0f)
		pAngle = 0.0f;

}

void uninitialize(void)
{
	if (gbFullscreen_ak == true)
	{
		dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);
		SetWindowLong(ghwnd_ak, GWL_STYLE, dwStyle_ak | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
		SetWindowPos(ghwnd_ak, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	if(Vao_pyramid_ak)
	{
		glDeleteVertexArrays(1, &Vao_pyramid_ak);
		Vao_pyramid_ak = 0;
	}

	if(Vbo_position_pyramid_ak)
	{
		glDeleteVertexArrays(1, &Vbo_position_pyramid_ak);
		Vbo_position_pyramid_ak = 0;
	}

	// glDetachShader(gShaderProgramObject_ak, gVertexShaderObject_ak);
	// glDetachShader(gShaderProgramObject_ak, gFragmentShaderObject_ak);

	// glDeleteShader(gVertexShaderObject_ak);
	// gVertexShaderObject_ak = 0;

	// glDeleteShader(gFragmentShaderObject_ak);
	// gFragmentShaderObject_ak = 0;

	// glUseProgram(0);

	// Safe Shader Release
	if (gShaderProgramObject_ak) {
        glUseProgram(gShaderProgramObject_ak);
        GLsizei shaderCount_ak;
        glGetProgramiv(gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

        GLuint* pShaders_ak = NULL;
        pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
        glGetAttachedShaders(gShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);

        for (GLsizei i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
            glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
            glDeleteShader(pShaders_ak[i_ak]);
            pShaders_ak[i_ak] = 0;
        }

        free(pShaders_ak);
        glDeleteProgram(gShaderProgramObject_ak);
        gShaderProgramObject_ak = 0;

        glUseProgram(0);
    }

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc_ak);
	ghrc_ak = NULL;

	ReleaseDC(ghwnd_ak, ghdc_ak);
	ghdc_ak = NULL;

	if (gpFile_ak)
	{
		fprintf(gpFile_ak, "Log File Is Successfully Closed.\n");
		fclose(gpFile_ak);
		gpFile_ak = NULL;
	}
}