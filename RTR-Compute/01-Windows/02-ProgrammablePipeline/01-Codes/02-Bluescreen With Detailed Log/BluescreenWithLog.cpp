#include <windows.h>
#include <stdio.h> 

#include <gl\glew.h>

#include <gl\GL.h>

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gpFile_ak = NULL;

HWND ghwnd_ak = NULL;
HDC ghdc_ak = NULL;
HGLRC ghrc_ak = NULL;

DWORD dwStyle_ak;
WINDOWPLACEMENT wpPrev_ak = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow_ak = false;
bool gbEscapeKeyIsPressed_ak = false;
bool gbFullscreen_ak = false;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);

	WNDCLASSEX wndclass_ak;
	HWND hwnd_ak;
	MSG msg_ak;
	TCHAR szClassName_ak[] = TEXT("OpenGLPP");
	bool bDone_ak = false;

	if (fopen_s(&gpFile_ak, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile_ak, "Log File Is Successfully Opened.\n");
	}

	wndclass_ak.cbSize = sizeof(WNDCLASSEX);
	wndclass_ak.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass_ak.cbClsExtra = 0;
	wndclass_ak.cbWndExtra = 0;
	wndclass_ak.hInstance = hInstance;
	wndclass_ak.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass_ak.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass_ak.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass_ak.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass_ak.lpfnWndProc = WndProc;
	wndclass_ak.lpszClassName = szClassName_ak;
	wndclass_ak.lpszMenuName = NULL;

	RegisterClassEx(&wndclass_ak);

	hwnd_ak = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName_ak,
		TEXT("PP: Bluescreen with Detailed Log"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd_ak = hwnd_ak;

	ShowWindow(hwnd_ak, iCmdShow);
	SetForegroundWindow(hwnd_ak);
	SetFocus(hwnd_ak);

	initialize();

	while (bDone_ak == false)
	{
		if (PeekMessage(&msg_ak, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_ak.message == WM_QUIT)
				bDone_ak = true;
			else
			{
				TranslateMessage(&msg_ak);
				DispatchMessage(&msg_ak);
			}
		}
		else
		{
			display();

			if (gbActiveWindow_ak == true)
			{
				if (gbEscapeKeyIsPressed_ak == true)
					bDone_ak = true;
			}
		}
	}

	uninitialize();

	return((int)msg_ak.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow_ak = true;
		else
			gbActiveWindow_ak = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed_ak = true;
			break;
		case 0x46:
			if (gbFullscreen_ak == false)
			{
				ToggleFullscreen();
				gbFullscreen_ak = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen_ak = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if (gbFullscreen_ak == false)
	{
		dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);
		if (dwStyle_ak & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd_ak, &wpPrev_ak) && GetMonitorInfo(MonitorFromWindow(ghwnd_ak, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd_ak, GWL_STYLE, dwStyle_ak & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd_ak, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		SetWindowLong(ghwnd_ak, GWL_STYLE, dwStyle_ak | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
		SetWindowPos(ghwnd_ak, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	void uninitialize(void);
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc_ak = GetDC(ghwnd_ak);

	iPixelFormatIndex = ChoosePixelFormat(ghdc_ak, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	if (SetPixelFormat(ghdc_ak, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	ghrc_ak = wglCreateContext(ghdc_ak);
	if (ghrc_ak == NULL)
	{
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	if (wglMakeCurrent(ghdc_ak, ghrc_ak) == false)
	{
		wglDeleteContext(ghrc_ak);
		ghrc_ak = NULL;
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc_ak);
		ghrc_ak = NULL;
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	fprintf(gpFile_ak, "\nOpenGL Vendor : %s \n",glGetString(GL_VENDOR));
	fprintf(gpFile_ak, "\nOpenGL Renderer : %s \n",glGetString(GL_RENDERER));
	fprintf(gpFile_ak, "\nOpenGL Version : %s \n",glGetString(GL_VERSION));
	fprintf(gpFile_ak, "\nOpenGL GLSL Version : %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	GLint numExten_ak;

	glGetIntegerv(GL_NUM_EXTENSIONS,&numExten_ak);
	for (int i=0; i<numExten_ak; i++)
	{
		fprintf(gpFile_ak,"\nOpenGL Enabled Extensions:%s\n",glGetStringi(GL_EXTENSIONS,i));
	}

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 1.0f, 0.0f);

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//OpenGL Draw

	SwapBuffers(ghdc_ak);
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void uninitialize(void)
{
	if (gbFullscreen_ak == true)
	{
		dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);
		SetWindowLong(ghwnd_ak, GWL_STYLE, dwStyle_ak | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
		SetWindowPos(ghwnd_ak, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc_ak);
	ghrc_ak = NULL;

	ReleaseDC(ghwnd_ak, ghdc_ak);
	ghdc_ak = NULL;

	if (gpFile_ak)
	{
		fprintf(gpFile_ak, "Log File Is Successfully Closed.\n");
		fclose(gpFile_ak);
		gpFile_ak = NULL;
	}
}