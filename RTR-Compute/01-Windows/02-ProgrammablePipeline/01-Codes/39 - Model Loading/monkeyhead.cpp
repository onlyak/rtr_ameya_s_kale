#include<windows.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<gl\glew.h>
#include<gl\GL.h>
#include"vmath.h"
#include"Icon.h"

using namespace vmath;

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define BUFFER_SIZE 256
#define S_EQUAL 0
#define NR_POINT_COORDS 3
#define NR_TEXTURE_COORDS 2
#define NR_NORMAL_COORDS 3
#define NR_FACE_TOKENS 3
#define NR_TRIANGLE_VERTICES 3


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

DWORD ak_dwStyle;
WINDOWPLACEMENT ak_gwpPrev = { sizeof(WINDOWPLACEMENT) };
bool ak_gbFullScreen = false;
HWND ak_ghwnd = NULL;
bool ak_gbActiveWindow = false;
HDC ak_ghdc = NULL;
HGLRC ak_ghrc = NULL;
FILE *ak_gpFile = NULL;

GLuint ak_gVertexShaderObject;
GLuint ak_gFragmentShadeerObject;
GLuint ak_gShaderProgramObject;

enum
{
	ak__ATTRIBUTE_POSITION = 0,
	ak__ATTRIBUTE_COLOR,
	ak__ATTRIBUTE_TEXCOORD,
	ak__ATTRIBUTE_NORMAL
};

GLuint ak_gVboPosition;
GLuint ak_gVboTexture;
GLuint ak_gVboElement;
GLuint ak_gMvpMatrixUniform;
mat4 ak_gPerspectiveProjectionMatrix;
GLuint ak_gVao;							
GLfloat ak_gAnglePyramid = 0.0f;
GLfloat ak_gAngleCube = 0.0f;

typedef struct vec_2d_int
{
	GLint** pp_arr;
	size_t size;
}vec_2d_int_t;

typedef struct vec_2d_float
{
	GLfloat** pp_arr;
	size_t size;
}vec_2d_float_t;

vec_2d_float_t* gp_vertices;
vec_2d_float_t* gp_texture;
vec_2d_float_t* gp_normals;
vec_2d_int_t* gp_face_tri;
vec_2d_int_t* gp_face_texture;
vec_2d_int_t* gp_face_normals;

FILE* g_fp_meshFile = NULL;
FILE* g_fp_logFile = NULL;
char g_line[BUFFER_SIZE];
GLuint ak_stone_texture;
GLuint ak_gtextureSamplerUniform;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	
	void Initialize(void);
	void Display(void);
	void Uninitialize(void);
	
	
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("OGL");
	int ak_DesktopWidth, ak_DesktopHeight;
	int ak_WndXPos, ak_WndYPos;
	bool ak_bDone = false;
	bool ak_gbEscapeKeyIsPressed = false;
	
	if(fopen_s(&ak_gpFile, "Logs.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Cannot open desired file"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
		
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackak_ound = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));
	
	RegisterClassEx(&wndclass);
	
	ak_DesktopWidth = GetSystemMetrics(SM_CXSCREEN);
	ak_DesktopHeight = GetSystemMetrics(SM_CYSCREEN);
	
	ak_DesktopWidth = ak_DesktopWidth / 2;
	ak_DesktopHeight = ak_DesktopHeight / 2;
	
	ak_WndXPos = ak_DesktopWidth - 400;
	
	ak_WndYPos = ak_DesktopHeight - 300;
	
	
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
				szAppName,
				TEXT("MonkeyHead"),
				WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
				ak_WndXPos,
				ak_WndYPos,
				WIN_WIDTH,
				WIN_HEIGHT,
				NULL,
				NULL,
				hInstance,
				NULL);
				
	ak_ghwnd = hwnd;
	
	Initialize();
	
	ShowWindow(hwnd, iCmdShow);
	
	SetForeak_oundWindow(hwnd);
	SetFocus(hwnd);
	
	while(ak_bDone == false)
	{
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if(msg.message == WM_QUIT)
			ak_bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			
			if(ak_gbActiveWindow == true)
			{
				Display();
			}
		}
		
	}
	
	Uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	
	void ToggleFullScreen(void);
	void Resize(int, int);
	void Uninitialize(void);
	

	switch(iMsg)
	{
		case WM_SETFOCUS : 
			ak_gbActiveWindow = true;
			break;
			
		case WM_KILLFOCUS :
			ak_gbActiveWindow = false;
			break;
			
		case WM_ERASEBKGND :
			return(0);
		
			
		case WM_SIZE :
			Resize(LOWORD(lParam), HIWORD(lParam));
			break;
		
		case WM_KEYDOWN : 
			switch(wParam)
			{
				case VK_ESCAPE :
					DestroyWindow(hwnd);
					break;
				
				case 0x46 : 
				case 0x66 :
					ToggleFullScreen();
					break;
				
				default : 
					break;
			}
			break;
			
		case WM_CLOSE :
			DestroyWindow(hwnd);
			break;
		
		case WM_DESTROY :
			Uninitialize();
			PostQuitMessage(0);
			break;
		
	}
	
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen()
{
	MONITORINFO mi = { sizeof(MONITORINFO) };
	
	if(ak_gbFullScreen == false)
	{
		ak_dwStyle = GetWindowLong(ak_ghwnd, GWL_STYLE);
		if(ak_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if( GetWindowPlacement(ak_ghwnd, &ak_gwpPrev) && GetMonitorInfo(MonitorFromWindow(ak_ghwnd, MONITORINFOF_PRIMARY), &mi) )
			{
				SetWindowLong( ak_ghwnd, GWL_STYLE, (ak_dwStyle & ~ WS_OVERLAPPEDWINDOW) );
				SetWindowPos( ak_ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
					(mi.rcMonitor.right - mi.rcMonitor.left), (mi.rcMonitor.bottom - mi.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED );
			}
		}
		ShowCursor(false);
		ak_gbFullScreen = true;
	}
	else
	{
		SetWindowLong( ak_ghwnd, GWL_STYLE, (ak_dwStyle | WS_OVERLAPPEDWINDOW) );
		SetWindowPlacement(ak_ghwnd, &ak_gwpPrev);
		SetWindowPos(ak_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(true);
		ak_gbFullScreen = false;
	}
}

void Initialize()
{
	
	void Resize(int, int);
	void Uninitialize(void);
	void LoadMeshData(void);
	bool LoadGLTexture(GLuint*, TCHAR[]);
	
	PIXELFORMATDESCRIPTOR ak_pfd;
	int ak_iPixelFormatIndex;
	
	ak_ghdc = GetDC(ak_ghwnd);
	
	ZeroMemory(&ak_pfd, sizeof(PIXELFORMATDESCRIPTOR));
	ak_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	ak_pfd.nVersion = 1;
	ak_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	ak_pfd.iPixelType = PFD_TYPE_RGBA;
	ak_pfd.cColorBits = 32;
	ak_pfd.cRedBits = 8;
	ak_pfd.cak_eenBits = 8;
	ak_pfd.cBlueBits = 8;
	ak_pfd.cAlphaBits = 8;
	ak_pfd.cDepthBits = 32;

	ak_iPixelFormatIndex = ChoosePixelFormat(ak_ghdc, &ak_pfd);
	if(ak_iPixelFormatIndex == 0)
	{
		fprintf(ak_gpFile, "ChoosePixelFormat() failed\n");
		Uninitialize();
	}
	
	if(SetPixelFormat(ak_ghdc, ak_iPixelFormatIndex, &ak_pfd) == FALSE)
	{
		fprintf(ak_gpFile, "SetPixelFormat() failed\n");
		Uninitialize();
	}
	
	ak_ghrc = wglCreateContext(ak_ghdc);
	if(ak_ghrc == NULL)
	{
		fprintf(ak_gpFile, "wglCreateContext() failed\n");
		ReleaseDC(ak_ghwnd, ak_ghdc);
		ak_ghdc = NULL;
	}
	
	if(wglMakeCurrent(ak_ghdc, ak_ghrc) == FALSE)
	{
		fprintf(ak_gpFile, "wglMakeCurrent() failed\n");
		wglDeleteContext(ak_ghrc);
		ak_ghrc = NULL;
		ReleaseDC(ak_ghwnd, ak_ghdc);
		ak_ghdc = NULL;
	}

	GLenum ak_glew_error = glewInit();
	if (ak_glew_error != GLEW_OK)
	{ 
		wglDeleteContext(ak_ghrc);
		ak_ghrc = NULL;
		ReleaseDC(ak_ghwnd, ak_ghdc);
		ak_ghdc = NULL;
	}
	
	ak_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar* ak_vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec2 vTexCoord;" \
		"uniform mat4 u_mvpMatrix;" \
		"out vec2 out_texcoord;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvpMatrix * vPosition;" \
		"out_texcoord = vTexCoord;" \
		"}";
	
	glShaderSource(ak_gVertexShaderObject, 1, (const GLchar **) &ak_vertexShaderSourceCode, NULL);

	
	glCompileShader(ak_gVertexShaderObject);
	 compilation
	GLint ak_iInfoLength = 0;
	GLint ak_iShaderCompileStatus = 0;
	char* ak_szInfoLog = NULL;

	glGetShaderiv(ak_gVertexShaderObject, GL_COMPILE_STATUS, &ak_iShaderCompileStatus);
	if (ak_iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(ak_gVertexShaderObject, GL_INFO_LOG_LENGTH, &ak_iInfoLength);
		if (ak_iInfoLength > 0)
		{
			ak_szInfoLog = (char*)malloc(sizeof(char) * sizeof(ak_iInfoLength));
			if (ak_szInfoLog != NULL)
			{
				GLsizei ak_written;
				glGetShaderInfoLog(ak_gVertexShaderObject, ak_iInfoLength, &ak_written, ak_szInfoLog);
				fprintf(ak_gpFile, "\n Vertex Shader Compilation Log : %s", ak_szInfoLog);
				free(ak_szInfoLog);
				Uninitialize();
			}
		}
	}
	ak_gFragmentShadeerObject = glCreateShader(GL_FRAGMENT_SHADER);

	 shader
	const GLchar* ak_fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec2 out_texcoord;"
		"uniform sampler2D u_texture_sampler;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
		"}";

	glShaderSource(ak_gFragmentShadeerObject, 1, (const GLchar **) &ak_fragmentShaderSourceCode, NULL);

	
	glCompileShader(ak_gFragmentShadeerObject);
	 compiation
	glGetShaderiv(ak_gFragmentShadeerObject, GL_COMPILE_STATUS, &ak_iShaderCompileStatus);
	if (ak_iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(ak_gFragmentShadeerObject, GL_INFO_LOG_LENGTH, &ak_iInfoLength);
		if (ak_iInfoLength > 0)
		{
			ak_szInfoLog = (char*)malloc(sizeof(char) * sizeof(ak_iInfoLength));
			if (ak_szInfoLog != NULL)
			{
				GLsizei ak_written;
				glGetShaderInfoLog(ak_gFragmentShadeerObject, ak_iInfoLength, &ak_written, ak_szInfoLog);
				fprintf(ak_gpFile, "\n Fragment Shader Compilation Log : %s", ak_szInfoLog);
				free(ak_szInfoLog);
				Uninitialize();
			}
		}
	}

	ak_gShaderProgramObject = glCreateProak_am();

	 to shader proak_am
	glAttachShader(ak_gShaderProgramObject, ak_gVertexShaderObject);

	 to shader proak_am
	glAttachShader(ak_gShaderProgramObject, ak_gFragmentShadeerObject);

	glBindAttribLocation(ak_gShaderProgramObject, ak__ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(ak_gShaderProgramObject, ak__ATTRIBUTE_TEXCOORD, "vTexCoord");

	
	glLinkProak_am(ak_gShaderProgramObject);
	 linking
	GLint ak_iShaderProak_amLinkStatus = 0;
	glGetProak_amiv(ak_gShaderProgramObject, GL_LINK_STATUS, &ak_iShaderProak_amLinkStatus);
	if (ak_iShaderProak_amLinkStatus == GL_FALSE)
	{
		glGetProak_amiv(ak_gShaderProgramObject, GL_INFO_LOG_LENGTH, &ak_iInfoLength);
		if (ak_iInfoLength > 0)
		{
			ak_szInfoLog = (char*)malloc(sizeof(ak_iInfoLength) * sizeof(char));
			if (ak_szInfoLog != NULL)
			{
				GLsizei ak_written;
				glGetProak_amInfoLog(ak_gShaderProgramObject, ak_iInfoLength, &ak_written, ak_szInfoLog);
				Uninitialize();
			}
		}
	}

	ak_gMvpMatrixUniform = glGetUniformLocation(ak_gShaderProgramObject, "u_mvpMatrix");
	ak_gtextureSamplerUniform = glGetUniformLocation(ak_gShaderProgramObject, "u_texture_sampler");

	
	LoadMeshData();
	
	GLfloat* vertices;
	int index = 0;
	int vi;
	vertices = (GLfloat*)malloc(gp_face_tri->size * 9 * sizeof(GLfloat));
	for (int i = 0; i != gp_face_tri->size; ++i)
	{
		for (int j = 0; j != NR_TRIANGLE_VERTICES; j++)
		{
			
			vi = gp_face_tri->pp_arr[i][j] - 1;
			vertices[index + 0] = gp_vertices->pp_arr[vi][0];
			vertices[index + 1] = gp_vertices->pp_arr[vi][1];
			vertices[index + 2] = gp_vertices->pp_arr[vi][2];
			fprintf(ak_gpFile, "\n vertices[%d] : %f \t vertices[%d] : %f \t vertices[%d] : %f", index + 0, vertices[index + 0],
					index + 1, vertices[index + 1], index + 2, vertices[index + 2]);
					
			index = index + 3;
		}
		
	}
	GLfloat* textures = NULL;
	index = 0;
	textures = (GLfloat*)malloc(gp_face_texture->size * 9 * sizeof(GLfloat));
	for (int i = 0; i != gp_face_texture->size; ++i)
	{
		for (int j = 0; j != NR_TEXTURE_COORDS; j++)
		{
			vi = gp_face_texture->pp_arr[i][j] - 1;
			textures[index + 0] = gp_texture->pp_arr[vi][0];
			textures[index + 1] = gp_texture->pp_arr[vi][1];
		}
		
	}
	
	glGenVertexArrays(1, &ak_gVao);
	glBindVertexArray(ak_gVao);

	glGenBuffers(1, &ak_gVboPosition);
	glBindBuffer(GL_ARRAY_BUFFER, ak_gVboPosition);
	glBufferData(GL_ARRAY_BUFFER, gp_face_tri->size * 9 * sizeof(GLfloat), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(ak__ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ak__ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	glGenBuffers(1, &ak_gVboTexture);
	glBindBuffer(GL_ARRAY_BUFFER, ak_gVboTexture);
	glBufferData(GL_ARRAY_BUFFER, gp_face_texture->size * 9 * sizeof(GLfloat), textures, GL_STATIC_DRAW);
	glVertexAttribPointer(ak__ATTRIBUTE_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ak__ATTRIBUTE_TEXCOORD);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);



	free(vertices);
	vertices = NULL;
	free(textures);
	textures = NULL;
	
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	LoadGLTexture(&ak_stone_texture, MAKEINTRESOURCE(ak_TEXTURE));
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	ak_gPerspectiveProjectionMatrix = mat4::identity();
	Resize(WIN_WIDTH, WIN_HEIGHT);
}

bool LoadGLTexture(GLuint* texture, TCHAR resourceID[])
{
	
	bool bResult = false;
	HBITMAP hBitmap = NULL;
	BITMAP bmp;

	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), resourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap)
	{
		bResult = true;
		GetObject(hBitmap, sizeof(bmp), &bmp);

		 OpenGL actual code
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);
		 parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);	
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);		

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp.bmWidth, bmp.bmHeight, 0, GL_Bak_, GL_UNSIGNED_BYTE, bmp.bmBits);
		glGenerateMipmap(GL_TEXTURE_2D);

		DeleteObject(hBitmap);

	}

	return(bResult);

}

void Resize(int width, int height)
{
	if(height == 0)
		height = 1;
	
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	ak_gPerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat) width / (GLfloat) height, 0.1f, 100.0f);

}

void LoadMeshData()
{
	int lineCount = 0;
	vec_2d_int_t* create_vec_2d_int(void);
	vec_2d_float_t* create_vec_2d_float(void);

	void push_back_vec_2d_int(vec_2d_int_t *, int *);
	void push_back_vec_2d_float(vec_2d_float_t*, float*);
	
	void* xcalloc(int, size_t);

	if (fopen_s(&g_fp_meshFile, "monkey.obj", "r") != 0)
	{
		fprintf(ak_gpFile, "\n error while reading obj file");
		DestroyWindow(ak_ghwnd);
	}
	gp_vertices = create_vec_2d_float();
	gp_texture = create_vec_2d_float();
	gp_normals = create_vec_2d_float();
	
	gp_face_tri = create_vec_2d_int();
	gp_face_texture = create_vec_2d_int();
	gp_face_normals = create_vec_2d_int();

	const char* sep_space = " ";
	const char* sep_fslash = "/";
	char* firstToken = NULL;
	char* token = NULL;
	char* faceTokens[NR_FACE_TOKENS];
	int nrTokens;
	char* tokenVertexIndex = NULL;
	char* tokenTextureIndex = NULL;
	char* tokenNormalIndex = NULL;

	while (fgets(g_line, BUFFER_SIZE, g_fp_meshFile) != NULL)
	{
		firstToken = strtok(g_line, sep_space);

		if (strcmp(firstToken, "v") == S_EQUAL)
		{
			GLfloat* pvec_point_coord = (GLfloat*)xcalloc(NR_POINT_COORDS, sizeof(GLfloat));
			for (int i = 0; i != NR_POINT_COORDS; i++)
			{
				pvec_point_coord[i] = atof(strtok(NULL, sep_space));
				
			}
			push_back_vec_2d_float(gp_vertices, pvec_point_coord);
		}

		else if (strcmp(firstToken, "vt") == S_EQUAL)
		{
			GLfloat* pvec_texture_coord = (GLfloat*)xcalloc(NR_TEXTURE_COORDS, sizeof(GLfloat));
			for (int i = 0; i != NR_TEXTURE_COORDS; i++)
			{
				pvec_texture_coord[i] = atof(strtok(NULL, sep_space));
			}
			push_back_vec_2d_float(gp_texture, pvec_texture_coord);
		}

		else if (strcmp(firstToken, "vn") == S_EQUAL)
		{
			GLfloat* pvec_normal_coord = (GLfloat*)xcalloc(NR_NORMAL_COORDS, sizeof(GLfloat));
			for (int i = 0; i != NR_NORMAL_COORDS; i++)
			{
				pvec_normal_coord[i] = atof(strtok(NULL, sep_space));
			}
			push_back_vec_2d_float(gp_normals, pvec_normal_coord);
		}

		else if (strcmp(firstToken, "f") == S_EQUAL)
		{
			GLint* pvecVertexIndices = (GLint*)xcalloc(3, sizeof(GLint));
			GLint* pvecTextureIndices = (GLint*)xcalloc(3, sizeof(GLint));
			GLint* pvecNormalIndices = (GLint*)xcalloc(3, sizeof(GLint));
			memset((void*)faceTokens, 0, NR_FACE_TOKENS);

			nrTokens = 0;
			while (token = strtok(NULL, sep_space))
			{
				if (strlen(token) < 3)
					break;
				faceTokens[nrTokens] = token;
				nrTokens++;
			}
		

			for (int i = 0; i != NR_FACE_TOKENS; ++i)
			{
				tokenVertexIndex = strtok(faceTokens[i], sep_fslash);
 				tokenTextureIndex = strtok(NULL, sep_fslash);
				tokenNormalIndex = strtok(NULL, sep_fslash);
				int tokenIndex = atoi(tokenVertexIndex);
				int textureIndex = atoi(tokenTextureIndex);
				int normalIndex = atoi(tokenNormalIndex);
				pvecVertexIndices[i] = tokenIndex;
				pvecTextureIndices[i] = textureIndex;
				pvecNormalIndices[i] = normalIndex;
				
			}
			push_back_vec_2d_int(gp_face_tri, pvecVertexIndices);
			push_back_vec_2d_int(gp_face_texture, pvecTextureIndices);
			push_back_vec_2d_int(gp_face_normals, pvecNormalIndices);
		}
		
		memset((void*)g_line, (int)'\0', BUFFER_SIZE);
		
		lineCount++;
	}

	fclose(g_fp_meshFile);
	g_fp_meshFile = NULL;
	
}

void Display(void)
{
	
	void Update(void);
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glUseProgram(ak_gShaderProgramObject);

=	mat4 ak_modelViewMatrix = mat4::identity();
	mat4 ak_modelViewProjectionMatrix = mat4::identity();
	mat4 ak_rotateMatrix = mat4::identity();
	mat4 ak_translateMatrix = mat4::identity();

	ak_translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
	ak_rotateMatrix = vmath::rotate(ak_gAnglePyramid, 0.0f, 1.0f, 0.0f);
	ak_modelViewMatrix = ak_translateMatrix * ak_rotateMatrix;

	ak_modelViewProjectionMatrix = ak_gPerspectiveProjectionMatrix * ak_modelViewMatrix;
	glUniformMatrix4fv(ak_gMvpMatrixUniform, 1, GL_FALSE, ak_modelViewProjectionMatrix);
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, ak_stone_texture);
	glUniform1i(ak_gtextureSamplerUniform, 0);
	
	
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glBindVertexArray(ak_gVao);
	glDrawArrays(GL_TRIANGLES, 0, gp_face_tri->size * 9);
	glBindVertexArray(0);
	#pragma endregion pyramid



	 shader proak_am
	glUseProgram(0);

	SwapBuffers(ak_ghdc);
}

vec_2d_int_t* create_vec_2d_int(void)
{
	void* xcalloc(int, size_t);

	return((vec_2d_int_t*)xcalloc(1, sizeof(vec_2d_int_t)));
}

vec_2d_float_t *create_vec_2d_float(void)
{	
	void* xcalloc(int, size_t);

	return((vec_2d_float_t*)xcalloc(1, sizeof(vec_2d_float_t)));
}

void push_back_vec_2d_int(vec_2d_int_t* pVec, GLint* pArr)
{
	void* xrealloc(void *, size_t);

	pVec->pp_arr = (GLint**)xrealloc(pVec->pp_arr, (pVec->size + 1) * sizeof(int**));
	pVec->size++;
	pVec->pp_arr[pVec->size - 1] = pArr;
}

void push_back_vec_2d_float(vec_2d_float_t* pVec, GLfloat* pArr)
{
	void* xrealloc(void*, size_t);

	pVec->pp_arr = (GLfloat**)xrealloc(pVec->pp_arr, (pVec->size + 1) * sizeof(GLfloat**));
	pVec->size++;
	pVec->pp_arr[pVec->size - 1] = pArr;
}

void clean_vec_2d_int(vec_2d_int_t** ppVec)
{
	vec_2d_int_t* pVec = *ppVec;
	for (size_t i = 0; i != pVec->size; i++)
	{
		free(pVec->pp_arr[i]);
	}
	free(pVec->pp_arr);
	free(pVec);
	ppVec = NULL;
}

void clean_vec_2d_float(vec_2d_float_t** ppVec)
{
	vec_2d_float_t* pVec = *ppVec;
	for (size_t i = 0; i != pVec->size; i++)
	{
		free(pVec->pp_arr[i]);
	}
	free(pVec);
	ppVec = NULL;
}

void* xcalloc(int nrElements, size_t sizePerElement)
{

	void* p = calloc(nrElements, sizePerElement);
	if (!p)
	{
		fprintf(ak_gpFile, "\n Error in xcalloc, calloc fatal memory error");
		DestroyWindow(ak_ghwnd);
	}
	return(p);
}

void* xrealloc(void* p, size_t newSize)
{
	void* ptr = realloc(p, newSize);
	if (!ptr)
	{
		fprintf(ak_gpFile, "\n Error in xrealloc, realloc out of memory error");
		DestroyWindow(ak_ghwnd);
	}
	return(ptr);
}

void Uninitialize(void)
{
	void clean_vec_2d_float(vec_2d_float_t **ppVec);
	void clean_vec_2d_int(vec_2d_int_t **ppVec);

	if(ak_gbFullScreen == true)
	{
		ak_dwStyle = GetWindowLong(ak_ghwnd, GWL_STYLE);
		SetWindowLong(ak_ghwnd, GWL_STYLE, (ak_dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ak_ghwnd, &ak_gwpPrev);
		SetWindowPos(ak_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(true);
		
	}
	 and vbo
	if (ak_gVao)
	{
		glDeleteVertexArrays(1, &ak_gVao);
		ak_gVao = 0;
	}


	if (ak_gVboTexture)
	{
		glDeleteBuffers(1, &ak_gVboTexture);
		ak_gVboTexture = 0;
	}
	 and vbo
	

	
	
	glDetachShader(ak_gShaderProgramObject, ak_gVertexShaderObject);
	
	glDetachShader(ak_gShaderProgramObject, ak_gFragmentShadeerObject);

	
	glDeleteShader(ak_gVertexShaderObject);
	ak_gVertexShaderObject = 0;


	 object
	glDeleteShader(ak_gFragmentShadeerObject);
	ak_gFragmentShadeerObject = 0;

	glUseProgram(0);

	clean_vec_2d_float(&gp_vertices);
	clean_vec_2d_float(&gp_normals);
	clean_vec_2d_float(&gp_texture);

	clean_vec_2d_int(&gp_face_tri);
	clean_vec_2d_int(&gp_face_texture);
	clean_vec_2d_int(&gp_face_normals);

	if(wglGetCurrentContext() == ak_ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
		
	if(ak_ghrc)
	{
		wglDeleteContext(ak_ghrc);
		ak_ghrc = NULL;
	}
		
	if(ak_ghdc)
	{
		ReleaseDC(ak_ghwnd, ak_ghdc);
		ak_ghdc = NULL;
	}
		
	if(ak_gpFile)
	{
		fclose(ak_gpFile);
		ak_gpFile = NULL;
	}
}


