#include "Icon.h"
#include <gl/glew.h>
#include <gl/GL.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#include "vmath.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum {
    ATTRIBUTE_POSITION = 0,
    ATTRIBUTE_COLOR,
    ATTRIBUTE_NORMAL,
    ATTRIBUTE_TEXCOORD
};

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "OpenGL32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gpFile_ak = NULL;
HWND ghwnd_ak = NULL;
DWORD dwStyle_ak = NULL;
WINDOWPLACEMENT wpPrev_ak = {sizeof(WINDOWPLACEMENT)};
bool gbFullScreen_ak = false;

HDC ghdc_ak = NULL;
HGLRC ghrc_ak = NULL;
bool gbActiveWindow_ak = false;

GLuint gVertexShaderObject_ak;
GLuint gFragmentShaderObject_ak;
GLuint gShaderProgramObject_ak;

GLuint vao_cube_ak;
GLuint vbo_ak;
GLuint vbo_normal_cube_ak;

GLuint modelMatrixUniform_ak;
GLuint viewMatrixUniform_ak;
GLuint projectionMatrixUniform_ak;

GLuint laUniform_ak;
GLuint ldUniform_ak;
GLuint lsUniform_ak;
GLuint lightPositionUniform_ak;

GLuint kaUniform_ak;
GLuint kdUniform_ak;
GLuint ksUniform_ak;
GLuint materialShininessUniform_ak;
GLuint textureSamplerUniform_ak;
GLuint lKeyPressedUniform_ak;

bool bAnimate_ak = true;
bool bLight_ak = false;

GLfloat lightAmbient_ak[] = {0.1f, 0.1f, 0.1f};
GLfloat lightDiffused_ak[] = {1.0f, 1.0f, 1.0f};
GLfloat lightPosition_ak[] = {100.0f, 100.0f, 100.0f, 1.0f};
GLfloat lightSpecular_ak[] = {1.0f, 1.0f, 1.0f};

GLfloat materialAmbient_ak[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat materialDiffused_ak[] = {0.5f, 0.2f, 0.7f, 1.0f};
GLfloat materialSpecular_ak[] = {0.7f, 0.7f, 0.7f, 1.0f};
GLfloat materialShininess_ak = 50.0f;

GLuint stone_texture_ak;

GLfloat cubeAngle_ak = 0.0f;

mat4 perspectiveProjectionMatrix_ak;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) 
{
    void Initialize(void);
    void Display(void);
    void Update(void);
    WNDCLASSEX wndclass_ak;
    HWND hwnd_ak;
    MSG msg_ak;
    TCHAR szAppName_ak[] = TEXT("MyApp");
    bool bDone_ak = false;

    if (fopen_s(&gpFile_ak, "Log.txt", "w") != 0) {
        MessageBox(NULL, TEXT("Failure creating file"), TEXT("File IO failed"), MB_ICONSTOP | MB_OK);
        exit(0);
    } else {
        fprintf(gpFile_ak, "File successfully create and program started\n");
    }

    int iScreenWidth = GetSystemMetrics(SM_CXSCREEN);
    int iScreenHeight = GetSystemMetrics(SM_CYSCREEN);
    int iScreenCenterX = iScreenWidth / 2;
    int iScreenCenterY = iScreenHeight / 2;
    int iWindowCenterX = WIN_WIDTH / 2;
    int iWindowsCenterY = WIN_HEIGHT / 2;
    int iWindowX = iScreenCenterX - iWindowCenterX;
    int iWindowY = iScreenCenterY - iWindowsCenterY;

    wndclass_ak.cbSize = sizeof(WNDCLASSEX);
    wndclass_ak.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wndclass_ak.cbClsExtra = 0;
    wndclass_ak.cbWndExtra = 0;
    wndclass_ak.hInstance = hInstance;
    wndclass_ak.lpfnWndProc = WndProc;
    wndclass_ak.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));
    wndclass_ak.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass_ak.lpszClassName = szAppName_ak;
    wndclass_ak.lpszMenuName = NULL;
    wndclass_ak.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass_ak.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));

    RegisterClassEx(&wndclass_ak);

    hwnd_ak = CreateWindowEx(WS_EX_APPWINDOW, szAppName_ak, TEXT("Interleaved"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, iWindowX, iWindowY, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);
    ghwnd_ak = hwnd_ak;

    Initialize();
    ShowWindow(hwnd_ak, iCmdShow);
    SetForegroundWindow(hwnd_ak);
    SetFocus(hwnd_ak);

    while (bDone_ak == false) {
        if (PeekMessage(&msg_ak, NULL, 0, 0, PM_REMOVE)) {
            if (msg_ak.message == WM_QUIT) {
                bDone_ak = true;
            } else {
                TranslateMessage(&msg_ak);
                DispatchMessage(&msg_ak);
            }
        } else {
            if (gbActiveWindow_ak == true) {
                Display();
                if (bAnimate_ak) {
                    Update();
                }
            }
        }
    }
    return ((int)msg_ak.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) 
{
    void ToggleFullScreen(void);
    void Resize(int, int);
    void Uninitialize(void);
    switch (iMsg) {
        case WM_SETFOCUS: {
            gbActiveWindow_ak = true;
            break;
        }
        case WM_KILLFOCUS: {
            gbActiveWindow_ak = false;
            break;
        }
        case WM_ERASEBKGND: {
            return (0);
        }
        case WM_SIZE: {
            Resize(LOWORD(lParam), HIWORD(lParam));
            break;
        }
        case WM_KEYDOWN: {
            switch (wParam) {
                case VK_ESCAPE: {
                    DestroyWindow(hwnd);
                    break;
                }
                case 0x46:
                case 0x66: {
                    ToggleFullScreen();
                    break;
                }
                default: {
                    break;
                }
            }
            break;
        }
        case WM_CHAR: {
            switch (wParam) {
                case 'L':
                case 'l': {
                    if (bLight_ak) {
                        bLight_ak = false;
                    } else {
                        bLight_ak = true;
                    }
                    break;
                }
                case 'A':
                case 'a': {
                    if (bAnimate_ak) {
                        bAnimate_ak = false;
                    } else {
                        bAnimate_ak = true;
                    }
                    break;
                }
            }
            break;
        }
        case WM_CLOSE: {
            DestroyWindow(hwnd);
            break;
        }
        case WM_DESTROY: {
            Uninitialize();
            PostQuitMessage(0);
            break;
        }
        default: {
            break;
        }
    }
    return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void) 
{
    MONITORINFO mi_ak = {sizeof(MONITORINFO)};
    if (gbFullScreen_ak == false) {

        dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);
        if ((dwStyle_ak & WS_OVERLAPPEDWINDOW)) {
            if ((GetWindowPlacement(ghwnd_ak, &wpPrev_ak) &&
                 (GetMonitorInfo(MonitorFromWindow(ghwnd_ak, MONITORINFOF_PRIMARY), &mi_ak)))) {
                SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak & (~WS_OVERLAPPEDWINDOW)));
                SetWindowPos(ghwnd_ak,
                             HWND_TOP,
                             mi_ak.rcMonitor.left,
                             mi_ak.rcMonitor.top,
                             (mi_ak.rcMonitor.right - mi_ak.rcMonitor.left),
                             (mi_ak.rcMonitor.bottom - mi_ak.rcMonitor.top),
                             SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }
        ShowCursor(FALSE);
        gbFullScreen_ak = true;
    } else {
        SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak | WS_OVERLAPPEDWINDOW));
        SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
        SetWindowPos(ghwnd_ak,
                     HWND_TOP,
                     0,
                     0,
                     0,
                     0,
                     (SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOOWNERZORDER));
        ShowCursor(TRUE);
        gbFullScreen_ak = false;
    }
}

void Initialize(void) 
{
    void Resize(int, int);
    void Uninitialize(void);
    bool LoadGLTexture(GLuint*, TCHAR[]);
    PIXELFORMATDESCRIPTOR pFD_ak;
    int iPixelFormatIndex_ak;

    ghdc_ak = GetDC(ghwnd_ak);

    ZeroMemory(&pFD_ak, sizeof(PIXELFORMATDESCRIPTOR));
    pFD_ak.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pFD_ak.nVersion = 1;
    pFD_ak.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pFD_ak.iPixelType = PFD_TYPE_RGBA;
    pFD_ak.cColorBits = 32;
    pFD_ak.cRedBits = 8;
    pFD_ak.cGreenBits = 8;
    pFD_ak.cBlueBits = 8;
    pFD_ak.cAlphaBits = 8;
    pFD_ak.cDepthBits = 32;

    iPixelFormatIndex_ak = ChoosePixelFormat(ghdc_ak, &pFD_ak);
    if (iPixelFormatIndex_ak == 0) {
        DestroyWindow(ghwnd_ak);
    }

    if (SetPixelFormat(ghdc_ak, iPixelFormatIndex_ak, &pFD_ak) == FALSE) {
        DestroyWindow(ghwnd_ak);
    }

    ghrc_ak = wglCreateContext(ghdc_ak);
    if (ghrc_ak == NULL) {
        DestroyWindow(ghwnd_ak);
    }

    if (wglMakeCurrent(ghdc_ak, ghrc_ak) == FALSE) {
        DestroyWindow(ghwnd_ak);
    }

    GLenum glew_error_ak = glewInit();
    if (glew_error_ak != GLEW_OK) {
        wglDeleteContext(ghrc_ak);
        ghrc_ak = NULL;
        ReleaseDC(ghwnd_ak, ghdc_ak);
        ghdc_ak = NULL;
    }

    fprintf(gpFile_ak, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(gpFile_ak, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(gpFile_ak, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(gpFile_ak, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    GLint numExt_ak;

    glGetIntegerv(GL_NUM_EXTENSIONS, &numExt_ak);
    for (int i = 0; i < numExt_ak; i++) {
        fprintf(gpFile_ak, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
    }

    gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode_ak =
        "#version 460 core"
        "\n"
        "in vec4 vPosition;"
        "in vec4 vColor;"
        "in vec3 vNormal;"
        "in vec2 vTexCoord;"
        "uniform mat4 u_model_matrix;"
        "uniform mat4 u_view_matrix;"
        "uniform mat4 u_projection_matrix;"
        "uniform vec4 u_light_position;"
        "uniform int u_lkey_pressed;"
        "out vec3 transformed_normal;"
        "out vec3 light_direction;"
        "out vec3 view_vector;"
        "out vec2 out_texcoord;"
        "out vec4 out_color;"
        "void main(void)"
        "{"
        "if(u_lkey_pressed == 1)"
        "{"
        "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"
        "transformed_normal = mat3((u_view_matrix * u_model_matrix)) * vNormal;"
        "light_direction = vec3(u_light_position - eye_coordinates);"
        "view_vector = -eye_coordinates.xyz;"
        "}"
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"
        "out_texcoord = vTexCoord;"
        "out_color = vColor;"
        "}";

    glShaderSource(gVertexShaderObject_ak, 1, (const GLchar**)&vertexShaderSourceCode_ak, NULL);
    glCompileShader(gVertexShaderObject_ak);

    GLint infoLogLength_ak = 0;
    GLint shaderCompileStatus_ak = 0;
    char* szBuffer_ak = NULL;
    glGetShaderiv(gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
        glGetShaderiv(gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetShaderInfoLog(gVertexShaderObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Vertex shader compilation log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                DestroyWindow(ghwnd_ak);
            }
        }
    }

    gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentSourceCode_ak =
        "#version 460 core"
        "\n"
        "in vec4 out_color;"
        "in vec2 out_texcoord;"
        "in vec3 transformed_normal;"
        "in vec3 light_direction;"
        "in vec3 view_vector;"
        "uniform vec3 u_la;"
        "uniform vec3 u_ld;"
        "uniform vec3 u_ls;"
        "uniform vec3 u_ka;"
        "uniform vec3 u_kd;"
        "uniform vec3 u_ks;"
        "uniform float u_material_shininess;"
        "uniform int u_lkey_pressed;"
        "uniform sampler2D u_texture_sampler;"
        "out vec4 FragColor;"
        "void main(void)"
        "{"
        "vec3 phong_ads_light;"
        "if(u_lkey_pressed == 1)"
        "{"
        "vec3 normalized_transformed_normal = normalize(transformed_normal);"
        "vec3 normalized_light_direction = normalize(light_direction);"
        "vec3 normalized_view_vector = normalize(view_vector);"
        "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normal);"
        "vec3 ambient = u_la * u_ka;"
        "vec3 diffuse = u_ld * u_kd * max(dot(normalized_light_direction, normalized_transformed_normal), 0.0);"
        "vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, normalized_view_vector), 0.0), u_material_shininess);"
        "phong_ads_light = ambient + diffuse + specular;"
        "}"
        "else"
        "{"
        "phong_ads_light = vec3(1.0, 1.0, 1.0);"
        "}"
        "vec3 tex = vec3(texture(u_texture_sampler, out_texcoord));"
        "FragColor = vec4(vec3(out_color) * phong_ads_light * tex, 1.0f);"
        "}";

    glShaderSource(gFragmentShaderObject_ak, 1, (const GLchar**)&fragmentSourceCode_ak, NULL);
    glCompileShader(gFragmentShaderObject_ak);

    infoLogLength_ak = 0;
    shaderCompileStatus_ak = 0;
    szBuffer_ak = NULL;
    glGetShaderiv(gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
        glGetShaderiv(gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetShaderInfoLog(gFragmentShaderObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Fragment shader compilation log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                DestroyWindow(ghwnd_ak);
            }
        }
    }

    gShaderProgramObject_ak = glCreateProgram();
    glAttachShader(gShaderProgramObject_ak, gVertexShaderObject_ak);
    glAttachShader(gShaderProgramObject_ak, gFragmentShaderObject_ak);

    glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_NORMAL, "vNormal");
    glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_COLOR, "vColor");
    glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_TEXCOORD, "vTexCoord");

    glLinkProgram(gShaderProgramObject_ak);

    GLint shaderProgramLinkStatus = 0;
    glGetProgramiv(gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
    if (shaderProgramLinkStatus == GL_FALSE) {
        glGetProgramiv(gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetProgramInfoLog(gShaderProgramObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Shader program link log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                DestroyWindow(ghwnd_ak);
            }
        }
    }

    modelMatrixUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_model_matrix");
    viewMatrixUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_view_matrix");
    projectionMatrixUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_projection_matrix");
    laUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_la");
    ldUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_ld");
    lsUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_ls");
    lightPositionUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_light_position");
    kaUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_ka");
    kdUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_kd");
    ksUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_ks");
    materialShininessUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_material_shininess");
    lKeyPressedUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_lkey_pressed");
    textureSamplerUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_texture_sampler");

    const GLfloat cubePCNT_ak[] = {
        1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
        -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,

        1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
        1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,

        -1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
        1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f,
        1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f,

        -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
        -1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,

        1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
        -1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
        -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,

        1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f,
        -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f};

    glGenVertexArrays(1, &vao_cube_ak);

    glBindVertexArray(vao_cube_ak);
    glGenBuffers(1, &vbo_ak);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_ak);
    glBufferData(GL_ARRAY_BUFFER, 24 * 11 * sizeof(float), cubePCNT_ak, GL_STATIC_DRAW);

    glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(0 * sizeof(float)));
    glEnableVertexAttribArray(ATTRIBUTE_POSITION);

    glVertexAttribPointer(ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(ATTRIBUTE_COLOR);

    glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(ATTRIBUTE_NORMAL);

    glVertexAttribPointer(ATTRIBUTE_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(9 * sizeof(float)));
    glEnableVertexAttribArray(ATTRIBUTE_TEXCOORD);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    bool res = LoadGLTexture(&stone_texture_ak, MAKEINTRESOURCE(STONE_BITMAP));
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    // glEnable(GL_CULL_FACE);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    perspectiveProjectionMatrix_ak = mat4::identity();

    bAnimate_ak = false;
    bLight_ak = false;

    Resize(WIN_WIDTH, WIN_HEIGHT);
}

bool LoadGLTexture(GLuint* texture, TCHAR resourceId[]) {
    bool bResult_ak = false;
    HBITMAP hBitmap_ak;
    BITMAP bmp_ak;
    hBitmap_ak = (HBITMAP)LoadImage(GetModuleHandle(NULL), resourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
    if (hBitmap_ak) {
        bResult_ak = true;
        GetObject(hBitmap_ak, sizeof(bmp_ak), &bmp_ak);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glGenTextures(1, texture);
        glBindTexture(GL_TEXTURE_2D, *texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bmp_ak.bmWidth, bmp_ak.bmHeight, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp_ak.bmBits);
        glGenerateMipmap(GL_TEXTURE_2D);
        DeleteObject(hBitmap_ak);
    }
    return bResult_ak;
}

void Resize(int width, int height) {
    if (height == 0) {
        height = 1;
    }
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    perspectiveProjectionMatrix_ak = vmath::perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
}

void Display(void) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(gShaderProgramObject_ak);
    if (bLight_ak == true) {
        glUniform1i(lKeyPressedUniform_ak, 1);
        glUniform3fv(laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(materialShininessUniform_ak, materialShininess_ak);
    } else {
        glUniform1i(lKeyPressedUniform_ak, 0);
    }

    mat4 modelMatrix_ak;
    mat4 viewMatrix_ak;
    mat4 modelViewProjectionMatrix_ak;
    mat4 translateMatrix_ak;
    mat4 xRotationMatrix_ak;
    mat4 yRotationMatrix_ak;
    mat4 zRotationMatrix_ak;
    mat4 scaleMatrix_ak;

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    modelViewProjectionMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();
    xRotationMatrix_ak = mat4::identity();
    yRotationMatrix_ak = mat4::identity();
    zRotationMatrix_ak = mat4::identity();
    scaleMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -5.0f);
    xRotationMatrix_ak = vmath::rotate(cubeAngle_ak, 1.0f, 0.0f, 0.0f);
    yRotationMatrix_ak = vmath::rotate(cubeAngle_ak, 0.0f, 1.0f, 0.0f);
    zRotationMatrix_ak = vmath::rotate(cubeAngle_ak, 0.0f, 0.0f, 1.0f);
    scaleMatrix_ak = vmath::scale(0.75f, 0.75f, 0.75f);

    modelMatrix_ak = translateMatrix_ak * scaleMatrix_ak * xRotationMatrix_ak * yRotationMatrix_ak * zRotationMatrix_ak;

    glUniformMatrix4fv(modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, stone_texture_ak);
    glUniform1i(textureSamplerUniform_ak, 0);

    glBindVertexArray(vao_cube_ak);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    glBindVertexArray(0);

    glUseProgram(0);
    SwapBuffers(ghdc_ak);
}

void Update(void) {
    cubeAngle_ak = cubeAngle_ak + 0.1f;
    if (cubeAngle_ak > 360.0f) {
        cubeAngle_ak = 0.0f;
    }
}

void Uninitialize(void) {
    if (gbFullScreen_ak == true) {
        dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);
        SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak | WS_OVERLAPPEDWINDOW));
        SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
        SetWindowPos(ghwnd_ak,
                     HWND_TOP,
                     0,
                     0,
                     0,
                     0,
                     SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
        ShowCursor(TRUE);
    }

    if (vao_cube_ak) {
        glDeleteVertexArrays(1, &vao_cube_ak);
        vao_cube_ak = 0;
    }

    if (vbo_ak) {
        glDeleteVertexArrays(1, &vbo_ak);
        vbo_ak = 0;
    }

    glDeleteTextures(1, &stone_texture_ak);

    if (gShaderProgramObject_ak) {
        glUseProgram(gShaderProgramObject_ak);
        GLsizei shaderCount_ak;
        glGetProgramiv(gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

        GLuint* pShaders_ak = NULL;
        pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
        glGetAttachedShaders(gShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);

        for (int i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
            glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
            glDeleteShader(pShaders_ak[i_ak]);
            pShaders_ak[i_ak] = 0;
        }

        free(pShaders_ak);
        glDeleteProgram(gShaderProgramObject_ak);
        gShaderProgramObject_ak = 0;

        glUseProgram(0);
    }

    if (wglGetCurrentContext() == ghrc_ak) {
        wglMakeCurrent(NULL, NULL);
    }

    if (ghrc_ak) {
        wglDeleteContext(ghrc_ak);
        ghrc_ak = NULL;
    }

    if (ghdc_ak) {
        ReleaseDC(ghwnd_ak, ghdc_ak);
        ghdc_ak = NULL;
    }

    if (gpFile_ak) {
        fprintf(gpFile_ak, "Program terminated successfully\n");
        fclose(gpFile_ak);
        gpFile_ak = NULL;
    }
}
