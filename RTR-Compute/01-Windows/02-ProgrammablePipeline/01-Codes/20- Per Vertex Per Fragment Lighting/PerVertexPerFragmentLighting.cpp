#include <windows.h>
#include <stdio.h> 
#include "Icon.h"

#include <gl\glew.h>

#include <gl\GL.h>

#include "vmath.h"

#include "Sphere.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#pragma comment(lib, "Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

enum
{
	ATTRIBUTE_POSITION = 0,
	ATTRIBUTE_COLOR,
	ATTRIBUTE_NORMAL,
	ATRIBUTE_TEXCOORD,
};

using namespace vmath;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gpFile_ak = NULL;

HWND ghwnd_ak = NULL;
HDC ghdc_ak = NULL;
HGLRC ghrc_ak = NULL;

DWORD dwStyle_ak;
WINDOWPLACEMENT wpPrev_ak = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow_ak = false;
bool gbEscapeKeyIsPressed_ak = false;
bool gbFullscreen_ak = false;

//Per Vertex
GLuint gVertexShaderObject_ak_pv;
GLuint gFragmentShaderObject_ak_pv;

//Per Fragment
GLuint gVertexShaderObject_ak_pf;
GLuint gFragmentShaderObject_ak_pf;

GLuint gShaderProgramObject_ak_pv;
GLuint gShaderProgramObject_ak_pf;

GLuint Vao_sphere;
GLuint Vbo_position_sphere;
GLuint Vbo_element_sphere;
GLuint Vbo_normal_sphere;

GLfloat lightAmbient_ak[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse_ak[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular_ak[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition_ak[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat materialAmbient_ak[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat materialDiffuse_ak[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular_ak[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess_ak = 50.0f;

//Uniforms
/********** 3 Uniforms**************/
GLuint ldUniform;
GLuint kdUniform;
GLuint lightPositionUniform;

/************ 9 Uniforms******************/
GLuint laUniform; // light component of ambient light
GLuint kaUniform; 

GLuint lsUniform; 
GLuint ksUniform;

GLuint shininessUniform;  //single float value

GLuint modelUniform;
GLuint viewUniform;
GLuint projectionUniform;

GLuint lKeyPressedUniform;
/*****************************/

bool bLight;

mat4 perspectiveProjectMatrix;

bool isLkey = false;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint numVertices;
GLuint numElements;

int pvpf = 1;
void commonUniformLocation(GLuint);



int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update();

	WNDCLASSEX wndclass_ak;
	HWND hwnd_ak;
	MSG msg_ak;
	TCHAR szClassName_ak[] = TEXT("OpenGLPP");
	bool bDone_ak = false;

	if (fopen_s(&gpFile_ak, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile_ak, "Log File Is Successfully Opened.\n");
	}

	int iScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	int iScreenHeight = GetSystemMetrics(SM_CYSCREEN);

	int iScreenCenterX = iScreenWidth / 2;
	int iScreenCenterY = iScreenHeight / 2;

	int iWindowCenterX = WIN_WIDTH / 2;
	int iWindowsCenterY = WIN_HEIGHT / 2;

	int iWindowX = iScreenCenterX - iWindowCenterX;
	int iWindowY = iScreenCenterY - iWindowsCenterY;

	wndclass_ak.cbSize = sizeof(WNDCLASSEX);
	wndclass_ak.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass_ak.cbClsExtra = 0;
	wndclass_ak.cbWndExtra = 0;
	wndclass_ak.hInstance = hInstance;
	wndclass_ak.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass_ak.hIcon = LoadIcon(NULL, MAKEINTRESOURCE(MY_ICON));
	wndclass_ak.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass_ak.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));
	wndclass_ak.lpfnWndProc = WndProc;
	wndclass_ak.lpszClassName = szClassName_ak;
	wndclass_ak.lpszMenuName = NULL;

	RegisterClassEx(&wndclass_ak);

	hwnd_ak = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName_ak,
		TEXT("PP: Per Vertex Per Fragment Lighting"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		iWindowX,
		iWindowY,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd_ak = hwnd_ak;

	ShowWindow(hwnd_ak, iCmdShow);
	SetForegroundWindow(hwnd_ak);
	SetFocus(hwnd_ak);

	initialize();

	while (bDone_ak == false)
	{
		if (PeekMessage(&msg_ak, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_ak.message == WM_QUIT)
				bDone_ak = true;
			else
			{
				TranslateMessage(&msg_ak);
				DispatchMessage(&msg_ak);
			}
		}
		else
		{
			update();
			display();

			if (gbActiveWindow_ak == true)
			{
				if (gbEscapeKeyIsPressed_ak == true)
					bDone_ak = true;
			}
		}
	}

	uninitialize();

	return((int)msg_ak.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	void commonUniformLocation(GLuint);


	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow_ak = true;
		else
			gbActiveWindow_ak = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed_ak = true;
			break;
		case VK_F11:
			if (gbFullscreen_ak == false)
			{
				ToggleFullscreen();
				gbFullscreen_ak = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen_ak = false;
			}
			break;
		case 0x46:
			commonUniformLocation(gShaderProgramObject_ak_pf);
			pvpf = 0;
			break;
		case 0x56:
			commonUniformLocation(gShaderProgramObject_ak_pv);
			pvpf = 1;
			break;
		case 0x4C:
			if (isLkey == false)
			{
				bLight = true;
				isLkey = true;
			}
			else
			{
				bLight = false;
				isLkey = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if (gbFullscreen_ak == false)
	{
		dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);
		if (dwStyle_ak & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd_ak, &wpPrev_ak) && GetMonitorInfo(MonitorFromWindow(ghwnd_ak, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd_ak, GWL_STYLE, dwStyle_ak & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd_ak, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		SetWindowLong(ghwnd_ak, GWL_STYLE, dwStyle_ak | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
		SetWindowPos(ghwnd_ak, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	void uninitialize(void);
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc_ak = GetDC(ghwnd_ak);

	iPixelFormatIndex = ChoosePixelFormat(ghdc_ak, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	if (SetPixelFormat(ghdc_ak, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	ghrc_ak = wglCreateContext(ghdc_ak);
	if (ghrc_ak == NULL)
	{
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	if (wglMakeCurrent(ghdc_ak, ghrc_ak) == false)
	{
		wglDeleteContext(ghrc_ak);
		ghrc_ak = NULL;
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc_ak);
		ghrc_ak = NULL;
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	fprintf(gpFile_ak, "\nOpenGL Vendor : %s \n",glGetString(GL_VENDOR));
	fprintf(gpFile_ak, "\nOpenGL Renderer : %s \n",glGetString(GL_RENDERER));
	fprintf(gpFile_ak, "\nOpenGL Version : %s \n",glGetString(GL_VERSION));
	fprintf(gpFile_ak, "\nOpenGL GLSL Version : %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	GLint numExten_ak;

	glGetIntegerv(GL_NUM_EXTENSIONS,&numExten_ak);
	for (int i=0; i<numExten_ak; i++)
	{
		fprintf(gpFile_ak,"\nOpenGL Enabled Extensions:%s\n",glGetStringi(GL_EXTENSIONS,i));
	}

	//Shader attributes for sphere
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	numVertices = getNumberOfSphereVertices();
	numElements = getNumberOfSphereElements();

	//Per Vertex Shader
	//********** Vertex Shader runs per vertex **********
	/* 1. When we specify core, we tell OpenGL to use Programmable Pipeline. i.e Core Profile
	   2. in/out are glsl language specifier. Known to shader language only.
	      in---> loads incoming data from main program. It loads data only once.
	   3. vec4--> [x,y,z,w]
	   4. uniform---> load incoming data from main program. It loads data multiple times
	   5. gl_Position---> in-built variable of Vertex Shader
	*/
	// out_color is sent as an input to Fragment Shader

	/*******Per Vertex Shader start***********/
	//Per Vertex Vertex Shader
	gVertexShaderObject_ak_pv = glCreateShader(GL_VERTEX_SHADER);
	const GLchar *vertexShaderSourceCode_pv = 
	"#version 460 core" \
	"\n" \
	"in vec4 vPosition;" \
	"in vec3 vNormal;"  \
	"uniform mat4 u_model_matrix;" \
	"uniform mat4 u_projection_matrix;" \
	"uniform mat4 u_view_matrix;" \
	"uniform int u_LKeyPressed;" \
	"uniform vec3 u_Ld;" \
	"uniform vec3 u_Kd;" \
	"uniform vec4 u_light_position;" \
	"uniform vec3 u_La;" \
	"uniform vec3 u_Ls;" \
	"uniform vec3 u_Ka;" \
	"uniform vec3 u_Ks;" \
	"uniform float u_Shininess;" \
	"out vec3 phong_ads_light;" \
	"void main(void)" \
	"{" \
		"if (u_LKeyPressed == 1) " \
		"{" \
			"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
			"vec3 transformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
			"vec3 light_direction = normalize(vec3(u_light_position - eyeCoordinates));" \
		    "vec3 reflection_vector = reflect(-light_direction, transformed_normal);" \
		    "vec3 view_vector = normalize(-eyeCoordinates.xyz);" \
			"vec3 ambient = u_La * u_Ka;" \
			"vec3 diffuse = u_Ld * u_Kd * max(dot(light_direction,transformed_normal),0.0);" \
		    "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector,view_vector),0.0),u_Shininess);" \
			"phong_ads_light = ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		    "phong_ads_light = vec3(1.0f,1.0f,1.0f);" \
		"}"
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
	"}";
	
	glShaderSource(gVertexShaderObject_ak_pv,1,(const GLchar **)&vertexShaderSourceCode_pv, NULL);

	// compile Vertex Shader
	glCompileShader(gVertexShaderObject_ak_pv);

	// Error checking for Vertex Shader
	GLint infoLogLength = 0; 
	GLint shaderCompiledStatus = 0;
	char *szBuffer = NULL;

	glGetShaderiv(gVertexShaderObject_ak_pv, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if(shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_ak_pv, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength>0)
		{
			szBuffer = (char *)malloc(infoLogLength);
			if(szBuffer!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_ak_pv, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Vertex Shader Compilation Log: %s\n",szBuffer);
				free(szBuffer);
				szBuffer = NULL;
				DestroyWindow(ghwnd_ak);
			}
		}
	}

	//Per Vertex Fragment Shader
	/* out_color is the output of Vertex Shader */
	gFragmentShaderObject_ak_pv = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* vertexFragmentSourceCode_pv =
	"#version 460 core" \
	"\n" \
	"in vec3 phong_ads_light;" \
	"out vec4 FragColor;" \
	"void main(void)" \
	"{" \
		"FragColor = vec4(phong_ads_light, 1.0f);"  \
	"}";

	glShaderSource(gFragmentShaderObject_ak_pv,1,(const GLchar **)&vertexFragmentSourceCode_pv, NULL);

	// compile Fragment Shader
	glCompileShader(gFragmentShaderObject_ak_pv);

	// Error Checking for Fragment Shader
	glGetShaderiv(gFragmentShaderObject_ak_pv, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if(shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_ak_pv, GL_INFO_LOG_LENGTH, &infoLogLength);
		if(infoLogLength>0)
		{
			szBuffer = (char *)malloc(infoLogLength);
			if(szBuffer!=NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_ak_pv, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Fragment Shader Compilation Log: %s\n",szBuffer);
				free(szBuffer);
				szBuffer = NULL;
				DestroyWindow(ghwnd_ak);
			}
		}
	}

	//Shader Program
	gShaderProgramObject_ak_pv = glCreateProgram();
	glAttachShader(gShaderProgramObject_ak_pv,gVertexShaderObject_ak_pv);
	glAttachShader(gShaderProgramObject_ak_pv,gFragmentShaderObject_ak_pv);

	// Bind the attributes in shader with the enums in your main program
	/* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
	glBindAttribLocation(gShaderProgramObject_ak_pv, ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject_ak_pv, ATTRIBUTE_NORMAL, "vNormal");
	
	glLinkProgram(gShaderProgramObject_ak_pv);

	// Linking Error Checking
	GLint shaderProgramLinkStatus = 0;
	szBuffer = NULL;

	glGetProgramiv(gShaderProgramObject_ak_pv, GL_LINK_STATUS, &shaderProgramLinkStatus);
	if (shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_ak_pv, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			szBuffer = (char*)malloc(infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_ak_pv, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Shader Program Link Log: %s\n", szBuffer);
				free(szBuffer);
				szBuffer = NULL;
				DestroyWindow(ghwnd_ak);
			}
		}
	}

	if(pvpf == 1)
		commonUniformLocation(gShaderProgramObject_ak_pv);


	/*******Per Vertex Shader End***********/

	/************Per Fragment Shader Start********************************/
	gVertexShaderObject_ak_pf = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* vertexShaderSourceCode =
		"#version 460 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;"  \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform int u_LKeyPressed;" \
		"uniform vec4 u_light_position;" \
		"out vec3 transformed_normal;" \
		"out vec3 light_direction;" \
		"out vec3 view_vector;" \
		"void main(void)" \
		"{" \
		"if (u_LKeyPressed == 1) " \
		"{" \
		"vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normal = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"light_direction = vec3(u_light_position - eyeCoordinates);" \
		"view_vector = -eyeCoordinates.xyz;" \
		"}" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject_ak_pf, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile Vertex Shader
	glCompileShader(gVertexShaderObject_ak_pf);

	// Error checking for Vertex Shader
	infoLogLength = 0;
	shaderCompiledStatus = 0;
	szBuffer = NULL;

	glGetShaderiv(gVertexShaderObject_ak_pf, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if (shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_ak_pf, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			szBuffer = (char*)malloc(infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_ak_pf, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Vertex Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				szBuffer = NULL;
				DestroyWindow(ghwnd_ak);
			}
		}
	}

	//Fragment Shader
	/* out_color is the output of Vertex Shader */
	gFragmentShaderObject_ak_pf = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* vertexFragmentSourceCode =
		"#version 460 core" \
		"\n" \
		"out vec4 FragColor;" \
		"in vec3 transformed_normal;" \
		"in vec3 light_direction;" \
		"in vec3 view_vector;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_La;" \
		"uniform vec3 u_Ls;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_Shininess;" \
		"uniform int u_LKeyPressed;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color;" \
		"if(u_LKeyPressed == 1)" \
		"{" \
		"vec3 norm_transformed_normal = normalize(transformed_normal);" \
		"vec3 norm_light_direction = normalize(light_direction);" \
		"vec3 norm_view_vector = normalize(view_vector);" \
		"vec3 reflection_vector = reflect(-norm_light_direction, norm_transformed_normal);" \
		"vec3 ambient = u_La * u_Ka;" \
		"vec3 diffuse = u_Ld * u_Kd * max(dot(norm_light_direction,norm_transformed_normal),0.0);" \
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector,norm_view_vector),0.0),u_Shininess);" \
		"phong_ads_color = ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color = vec3(1.0f,1.0f,1.0f);" \
		"}" \

		"FragColor = vec4(phong_ads_color, 1.0f);" \
		"}";

	glShaderSource(gFragmentShaderObject_ak_pf, 1, (const GLchar**)&vertexFragmentSourceCode, NULL);

	// compile Fragment Shader
	glCompileShader(gFragmentShaderObject_ak_pf);

	infoLogLength = 0;
	shaderCompiledStatus = 0;
	szBuffer = NULL;

	// Error Checking for Fragment Shader
	glGetShaderiv(gFragmentShaderObject_ak_pf, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if (shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_ak_pf, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			szBuffer = (char*)malloc(infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_ak_pf, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Fragment Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				szBuffer = NULL;
				DestroyWindow(ghwnd_ak);
			}
		}
	}

	//Shader Program
	gShaderProgramObject_ak_pf = glCreateProgram();
	glAttachShader(gShaderProgramObject_ak_pf, gVertexShaderObject_ak_pf);
	glAttachShader(gShaderProgramObject_ak_pf, gFragmentShaderObject_ak_pf);

	// Bind the attributes in shader with the enums in your main program
	/* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
	glBindAttribLocation(gShaderProgramObject_ak_pf, ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject_ak_pf, ATTRIBUTE_NORMAL, "vNormal");

	glLinkProgram(gShaderProgramObject_ak_pf);

	// Linking Error Checking
	shaderProgramLinkStatus = 0;
	szBuffer = NULL;

	glGetProgramiv(gShaderProgramObject_ak_pf, GL_LINK_STATUS, &shaderProgramLinkStatus);
	if (shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_ak_pf, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			szBuffer = (char*)malloc(infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_ak_pf, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Shader Program Link Log: %s\n", szBuffer);
				free(szBuffer);
				szBuffer = NULL;
				DestroyWindow(ghwnd_ak);
			}
		}
	}

	if (pvpf == 0)
		commonUniformLocation(gShaderProgramObject_ak_pf);

	/*******************Per Fragment Shader Ends**************************/

	

	// Push the above vertices to vPosition

	//Steps
	/* 1. Tell OpenGl to create one buffer in your VRAM
	      Give me a symbol to identify. It is known as NamedBuffer
	      In OpenGL terminology, it is called as GL_ARRAY_BUFFER. This is becase vertex has plenty of attributes 
		  like color, texture, etc. Also it requires contiguous memory.
		  User identifies this variable as Vbo_position and GPU as GL_ARRAY_BUFFER. 
	   2. Bind with the above symbol. It doesn't unbind until 'unbind step' is performed eg- Railway track
	   3. Insert triangle data into the buffer.
	   4. Specify where to insert this data into shader and also how to use it.
	   5. Enable the 'in' point. 
	   6. Unbind 
	*/

	//Sphere
	
	// Sphere Vao
	glGenVertexArrays(1, &Vao_sphere);
	glBindVertexArray(Vao_sphere);

	// Sphere position Vbo
	glGenBuffers(1, &Vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	// Sphere element Vbo
	glGenBuffers(1, &Vbo_element_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Vbo_element_sphere);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// Sphere normals Vbo
	glGenBuffers(1, &Vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_normal_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_NORMAL);

	// unbind vao
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);          
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Set PerspectiveMatrix to identity matrix 
	perspectiveProjectMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if(pvpf==1)
		glUseProgram(gShaderProgramObject_ak_pv);
	else
		glUseProgram(gShaderProgramObject_ak_pf);


	if (bLight == true)
	{
		glUniform1i(lKeyPressedUniform, 1);
		glUniform3fv(laUniform,1,lightAmbient_ak);
		glUniform3fv(ldUniform,1,lightDiffuse_ak);
		glUniform3fv(lsUniform,1,lightSpecular_ak);
		glUniform4fv(lightPositionUniform, 1, lightPosition_ak);

		glUniform3fv(kaUniform, 1, materialAmbient_ak);
		glUniform3fv(kdUniform,1, materialDiffuse_ak);
		glUniform3fv(ksUniform,1, materialSpecular_ak);
		glUniform1f(shininessUniform, materialShininess_ak);

	}
	else
		glUniform1i(lKeyPressedUniform, 0);


	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 translationMatrix = mat4::identity();

	translationMatrix = translate(0.0f, 0.0f, -4.0f);
	modelMatrix = translationMatrix;

	glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, perspectiveProjectMatrix);
	
	//Sphere Begin
	glBindVertexArray(Vao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
	
	glBindVertexArray(0); // Sphere end

	glUseProgram(0);
	
	SwapBuffers(ghdc_ak);
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	perspectiveProjectMatrix = perspective(45.0f, ((GLfloat)width/(GLfloat)height),0.1f, 100.0f);
}

void update()
{
	
}

void uninitialize(void)
{
	if (gbFullscreen_ak == true)
	{
		dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);
		SetWindowLong(ghwnd_ak, GWL_STYLE, dwStyle_ak | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
		SetWindowPos(ghwnd_ak, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	if(Vao_sphere)
	{
		glDeleteVertexArrays(1, &Vao_sphere);
		Vao_sphere = 0;
	}

	if(Vbo_position_sphere)
	{
		glDeleteVertexArrays(1, &Vbo_position_sphere);
		Vbo_position_sphere = 0;
	}

	if(Vbo_element_sphere)
	{
		glDeleteVertexArrays(1, &Vbo_element_sphere);
		Vbo_element_sphere = 0;
	}

	if (Vbo_normal_sphere)
	{
		glDeleteVertexArrays(1, &Vbo_normal_sphere);
		Vbo_normal_sphere = 0;
	}


	// glDetachShader(gShaderProgramObject_ak, gVertexShaderObject_ak);
	// glDetachShader(gShaderProgramObject_ak, gFragmentShaderObject_ak);

	// glDeleteShader(gVertexShaderObject_ak);
	// gVertexShaderObject_ak = 0;

	// glDeleteShader(gFragmentShaderObject_ak);
	// gFragmentShaderObject_ak = 0;

	// glUseProgram(0);

	// Safe Shader Release
	if (gShaderProgramObject_ak_pv) {
        glUseProgram(gShaderProgramObject_ak_pv);
        GLsizei shaderCount_ak;
        glGetProgramiv(gShaderProgramObject_ak_pv, GL_ATTACHED_SHADERS, &shaderCount_ak);

        GLuint* pShaders_ak = NULL;
        pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
        glGetAttachedShaders(gShaderProgramObject_ak_pv, shaderCount_ak, &shaderCount_ak, pShaders_ak);

        for (GLsizei i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
            glDetachShader(gShaderProgramObject_ak_pv, pShaders_ak[i_ak]);
            glDeleteShader(pShaders_ak[i_ak]);
            pShaders_ak[i_ak] = 0;
        }

        free(pShaders_ak);
        glDeleteProgram(gShaderProgramObject_ak_pv);
        gShaderProgramObject_ak_pv = 0;

        glUseProgram(0);
    }

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc_ak);
	ghrc_ak = NULL;

	ReleaseDC(ghwnd_ak, ghdc_ak);
	ghdc_ak = NULL;

	if (gpFile_ak)
	{
		fprintf(gpFile_ak, "Log File Is Successfully Closed.\n");
		fclose(gpFile_ak);
		gpFile_ak = NULL;
	}
}

void commonUniformLocation(GLuint shaderObject)
{
	modelUniform = glGetUniformLocation(shaderObject, "u_model_matrix");
	viewUniform = glGetUniformLocation(shaderObject, "u_view_matrix");
	projectionUniform = glGetUniformLocation(shaderObject, "u_projection_matrix");

	ldUniform = glGetUniformLocation(shaderObject, "u_Ld");
	kdUniform = glGetUniformLocation(shaderObject, "u_Kd");
	lightPositionUniform = glGetUniformLocation(shaderObject, "u_light_position");

	laUniform = glGetUniformLocation(shaderObject, "u_La");
	kaUniform = glGetUniformLocation(shaderObject, "u_Ka");

	lsUniform = glGetUniformLocation(shaderObject, "u_Ls");
	ksUniform = glGetUniformLocation(shaderObject, "u_Ks");

	shininessUniform = glGetUniformLocation(shaderObject, "u_Shininess");

	lKeyPressedUniform = glGetUniformLocation(shaderObject, "u_LKeyPressed");
}