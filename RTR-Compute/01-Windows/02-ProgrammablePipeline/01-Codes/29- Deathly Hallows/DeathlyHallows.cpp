#include <windows.h>
#include <stdio.h> 
#include "Icon.h"

#include <gl\glew.h>

#include <gl\GL.h>

#include "vmath.h"
#include <cmath>

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

enum
{
	ATTRIBUTE_POSITION = 0,
	ATTRIBUTE_COLOR,
	ATTRIBUTE_NORMAL,
	ATRIBUTE_TEXCOORD,
};

using namespace vmath;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gpFile_ak = NULL;

HWND ghwnd_ak = NULL;
HDC ghdc_ak = NULL;
HGLRC ghrc_ak = NULL;

DWORD dwStyle_ak;
WINDOWPLACEMENT wpPrev_ak = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow_ak = false;
bool gbEscapeKeyIsPressed_ak = false;
bool gbFullscreen_ak = false;

GLuint gVertexShaderObject_ak;
GLuint gFragmentShaderObject_ak;
GLuint gShaderProgramObject_ak;

GLuint mvpMatrixUniform_ak;
GLuint Vao_ak;
GLuint Vbo_position_ak;
GLuint Vbo_color_ak;

mat4 perspectiveProjectMatrix_ak;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);

	WNDCLASSEX wndclass_ak;
	HWND hwnd_ak;
	MSG msg_ak;
	TCHAR szClassName_ak[] = TEXT("OpenGLPP");
	bool bDone_ak = false;

	if (fopen_s(&gpFile_ak, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile_ak, "Log File Is Successfully Opened.\n");
	}

	int iScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	int iScreenHeight = GetSystemMetrics(SM_CYSCREEN);

	int iScreenCenterX = iScreenWidth / 2;
	int iScreenCenterY = iScreenHeight / 2;

	int iWindowCenterX = WIN_WIDTH / 2;
	int iWindowsCenterY = WIN_HEIGHT / 2;

	int iWindowX = iScreenCenterX - iWindowCenterX;
	int iWindowY = iScreenCenterY - iWindowsCenterY;

	wndclass_ak.cbSize = sizeof(WNDCLASSEX);
	wndclass_ak.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass_ak.cbClsExtra = 0;
	wndclass_ak.cbWndExtra = 0;
	wndclass_ak.hInstance = hInstance;
	wndclass_ak.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass_ak.hIcon = LoadIcon(NULL, MAKEINTRESOURCE(MY_ICON));
	wndclass_ak.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass_ak.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));
	wndclass_ak.lpfnWndProc = WndProc;
	wndclass_ak.lpszClassName = szClassName_ak;
	wndclass_ak.lpszMenuName = NULL;

	RegisterClassEx(&wndclass_ak);

	hwnd_ak = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName_ak,
		TEXT("PP: Dynamic Deathly Hallows"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		iWindowX,
		iWindowY,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd_ak = hwnd_ak;

	ShowWindow(hwnd_ak, iCmdShow);
	SetForegroundWindow(hwnd_ak);
	SetFocus(hwnd_ak);

	initialize();

	while (bDone_ak == false)
	{
		if (PeekMessage(&msg_ak, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_ak.message == WM_QUIT)
				bDone_ak = true;
			else
			{
				TranslateMessage(&msg_ak);
				DispatchMessage(&msg_ak);
			}
		}
		else
		{
			display();

			if (gbActiveWindow_ak == true)
			{
				if (gbEscapeKeyIsPressed_ak == true)
					bDone_ak = true;
			}
		}
	}

	uninitialize();

	return((int)msg_ak.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow_ak = true;
		else
			gbActiveWindow_ak = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed_ak = true;
			break;
		case 0x46:
			if (gbFullscreen_ak == false)
			{
				ToggleFullscreen();
				gbFullscreen_ak = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen_ak = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if (gbFullscreen_ak == false)
	{
		dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);
		if (dwStyle_ak & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd_ak, &wpPrev_ak) && GetMonitorInfo(MonitorFromWindow(ghwnd_ak, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd_ak, GWL_STYLE, dwStyle_ak & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd_ak, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		SetWindowLong(ghwnd_ak, GWL_STYLE, dwStyle_ak | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
		SetWindowPos(ghwnd_ak, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	void uninitialize(void);
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc_ak = GetDC(ghwnd_ak);

	iPixelFormatIndex = ChoosePixelFormat(ghdc_ak, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	if (SetPixelFormat(ghdc_ak, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	ghrc_ak = wglCreateContext(ghdc_ak);
	if (ghrc_ak == NULL)
	{
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	if (wglMakeCurrent(ghdc_ak, ghrc_ak) == false)
	{
		wglDeleteContext(ghrc_ak);
		ghrc_ak = NULL;
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc_ak);
		ghrc_ak = NULL;
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	fprintf(gpFile_ak, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
	fprintf(gpFile_ak, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile_ak, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile_ak, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	GLint numExten_ak;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExten_ak);
	for (int i = 0; i < numExten_ak; i++)
	{
		fprintf(gpFile_ak, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
	}

	//Vertex Shader
	//********** Vertex Shader runs per vertex **********
	/* 1. When we specify core, we tell OpenGL to use Programmable Pipeline. i.e Core Profile
	   2. in/out are glsl language specifier. Known to shader language only.
		  in---> loads incoming data from main program. It loads data only once.
	   3. vec4--> [x,y,z,w]
	   4. uniform---> load incoming data from main program. It loads data multiple times
	   5. gl_Position---> in-built variable of Vertex Shader
	*/
	// out_color is sent as an input to Fragment Shader
	gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* vertexShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"out vec4 out_color;" \
		"uniform mat4 u_mvpMatrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvpMatrix * vPosition;" \
		"out_color = vColor;" \
		"}";

	glShaderSource(gVertexShaderObject_ak, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile Vertex Shader
	glCompileShader(gVertexShaderObject_ak);

	// Error checking for Vertex Shader
	GLint infoLogLength = 0;
	GLint shaderCompiledStatus = 0;
	char* szBuffer = NULL;

	glGetShaderiv(gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if (shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			szBuffer = (char*)malloc(infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_ak, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Vertex Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				szBuffer = NULL;
				DestroyWindow(ghwnd_ak);
			}
		}
	}

	//Fragment Shader
	/* out_color is the output of Vertex Shader */
	gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* vertexFragmentSourceCode =
		"#version 430" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = out_color;" \
		"}";

	glShaderSource(gFragmentShaderObject_ak, 1, (const GLchar**)&vertexFragmentSourceCode, NULL);

	// compile Fragment Shader
	glCompileShader(gFragmentShaderObject_ak);

	// Error Checking for Fragment Shader
	glGetShaderiv(gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if (shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			szBuffer = (char*)malloc(infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_ak, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Fragment Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				szBuffer = NULL;
				DestroyWindow(ghwnd_ak);
			}
		}
	}

	//Shader Program
	gShaderProgramObject_ak = glCreateProgram();
	glAttachShader(gShaderProgramObject_ak, gVertexShaderObject_ak);
	glAttachShader(gShaderProgramObject_ak, gFragmentShaderObject_ak);

	// Bind the attributes in shader with the enums in your main program
	/* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
	glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");

	// For Color Attribute
	glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_COLOR, "vColor");

	glLinkProgram(gShaderProgramObject_ak);

	// Linking Error Checking
	GLint shaderProgramLinkStatus = 0;
	szBuffer = NULL;

	glGetProgramiv(gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
	if (shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			szBuffer = (char*)malloc(infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_ak, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Shader Program Link Log: %s\n", szBuffer);
				free(szBuffer);
				szBuffer = NULL;
				DestroyWindow(ghwnd_ak);
			}
		}
	}

	//Get the information of uniform Post linking
	mvpMatrixUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_mvpMatrix");

	//Vertices Array Declaration
	const GLfloat triangleVertices[] =
	{ 0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f
	};

	//Color Array

	const GLfloat triangleColors[] =
	{
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f
	};

	//Repeat the below steps of Vbo_position and call them in draw method
	glGenVertexArrays(1, &Vao_ak);
	glBindVertexArray(Vao_ak);

	// Push the above vertices to vPosition

	//Steps
	/* 1. Tell OpenGl to create one buffer in your VRAM
		  Give me a symbol to identify. It is known as NamedBuffer
		  In OpenGL terminology, it is called as GL_ARRAY_BUFFER. This is becase vertex has plenty of attributes
		  like color, texture, etc. Also it requires contiguous memory.
		  User identifies this variable as Vbo_position and GPU as GL_ARRAY_BUFFER.
	   2. Bind with the above symbol. It doesn't unbind until 'unbind step' is performed eg- Railway track
	   3. Insert triangle data into the buffer.
	   4. Specify where to insert this data into shader and also how to use it.
	   5. Enable the 'in' point.
	   6. Unbind
	*/
	glGenBuffers(1, &Vbo_position_ak);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_ak);
	glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), NULL, GL_DYNAMIC_DRAW);
	// 3 is specified for 3 pairs for triangle vertices
	/* For Texture, specify 2*/
	// 4th parameter----> Normalized Co-ordinates
	// 5th How many strides to take?
	// 6th From which position	
	glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);  //change tracks to link different attributes

	// Push color to vColor

	glGenBuffers(1, &Vbo_color_ak);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_color_ak);
	glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Set OrthographicMatrix to identity matrix 
	perspectiveProjectMatrix_ak = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	void triangle(float);
	void inCircle(float, float, float, float, float, float, float, float, float);
	void wand(float, float, float, float, float, float);

	static GLfloat angle_ak = 0.0f;
	static float Xtriangle_ak = -2.0f;
	static float Ytriangle_ak = -2.0f;
	static float TriangleRotation_ak = 1.0f;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject_ak);

	//OpenGL Draw

	//Set ModelView and ModelViewProjection matrices to identity
	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();

	modelViewProjectionMatrix = perspectiveProjectMatrix_ak * modelViewMatrix;

	int a = 1;

	// Translate call
	mat4 translateMatrix = translate(Xtriangle_ak, Ytriangle_ak, -2.5f);
	mat4 rotateMatrix = rotate(angle_ak, 0.0f, 1.0f, 0.0f);
	modelViewProjectionMatrix = perspectiveProjectMatrix_ak * translateMatrix * rotateMatrix;

	// After this line, modelViewProjectionMatrix becomes u_mvpMatrix
	glUniformMatrix4fv(mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix);

	//Bind Vao
	glBindVertexArray(Vao_ak); //change tracks  //Begin

	triangle(0.5f);
	angle_ak += 0.1f;

	if (Xtriangle_ak <= 0.0f)
	{
		Xtriangle_ak = Xtriangle_ak + 0.001f;
	}
	else
	{
		a = 0;
		angle_ak = 0.0f;
	}

	if (Ytriangle_ak <= 0.0f)
		Ytriangle_ak = Ytriangle_ak + 0.001f;
	if (angle_ak >= 360.0f)
		angle_ak = 0.0f;

	if (a == 0)
	{
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translateMatrix = mat4::identity();

		modelViewProjectionMatrix = perspectiveProjectMatrix_ak * modelViewMatrix;

		static GLfloat CRangle_ak = 0.0f;
		static float XCircle_ak = 3.0f;
		static float YCircle_ak = -3.0f;

		translateMatrix = translate(XCircle_ak, YCircle_ak, -2.5f);
		rotateMatrix = rotate(CRangle_ak, 0.0f, 1.0f, 0.0f);
		modelViewProjectionMatrix = perspectiveProjectMatrix_ak * translateMatrix * rotateMatrix;

		// After this line, modelViewProjectionMatrix becomes u_mvpMatrix
		glUniformMatrix4fv(mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix);

		inCircle(0.0f, 0.5f, 0.0f, -0.5f, -0.5f, 0.0f, 0.5f, -0.5f, 0.0f);

		CRangle_ak += 0.1f;

		if (XCircle_ak >= 0.0f)
			XCircle_ak = XCircle_ak - 0.001f;
		else
		{
			a = 2;
			CRangle_ak = 0.0f;
		}

		if (YCircle_ak <= 0.0f)
			YCircle_ak = YCircle_ak + 0.001f;
		if (CRangle_ak >= 360.0f)
			CRangle_ak = 0.0f;

		if (a == 2)
		{
			modelViewMatrix = mat4::identity();
			modelViewProjectionMatrix = mat4::identity();
			translateMatrix = mat4::identity();

			modelViewProjectionMatrix = perspectiveProjectMatrix_ak * modelViewMatrix;

			static float XWand_ak = 0.0f;
			static float YWand_ak = 2.0f;

			mat4 translateMatrix = translate(XWand_ak, YWand_ak, -2.5f);
			modelViewProjectionMatrix = perspectiveProjectMatrix_ak * translateMatrix;

			// After this line, modelViewProjectionMatrix becomes u_mvpMatrix
			glUniformMatrix4fv(mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix);

			wand(0.0f, 0.5f, 0.0f, 0.0f, -0.5f, 0.0f);

			if (YWand_ak >= 0.0f)
				YWand_ak -= 0.001f;
		}
	}
	// Unbind Vao
	glBindVertexArray(0);  //end

	glUseProgram(0);

	SwapBuffers(ghdc_ak);
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	perspectiveProjectMatrix_ak = perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
}

void triangle(float value)
{
	GLfloat lineVertices[6] = {
	1.0f, 1.0f, 0.0f,
	1.0f, -1.0f, 0.0f
	};
	GLfloat lineColor[6] = {
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f
	};

	lineVertices[0] = 0.0f;
	lineVertices[1] = value;
	lineVertices[2] = 0.0f;
	lineVertices[3] = -value;
	lineVertices[4] = -value;
	lineVertices[5] = 0.0f;

	glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_ak);
	glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineVertices, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, Vbo_color_ak);
	glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineColor, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDrawArrays(GL_LINES, 0, 2);

	lineVertices[0] = -value;
	lineVertices[1] = -value;
	lineVertices[2] = 0.0f;
	lineVertices[3] = value;
	lineVertices[4] = -value;
	lineVertices[5] = 0.0f;

	glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_ak);
	glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineVertices, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, Vbo_color_ak);
	glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineColor, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDrawArrays(GL_LINES, 0, 2);

	lineVertices[0] = value;
	lineVertices[1] = -value;
	lineVertices[2] = 0.0f;
	lineVertices[3] = 0.0f;
	lineVertices[4] = value;
	lineVertices[5] = 0.0f;

	glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_ak);
	glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineVertices, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, Vbo_color_ak);
	glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineColor, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDrawArrays(GL_LINES, 0, 2);
}

void inCircle(float x1_ak, float y1_ak, float z1_ak, float x2_ak, float y2_ak, float z2, float x3_ak, float y3_ak, float z3_ak)
{
	GLfloat lineVertices[3] = {
		0.0f, 0.0f, 0.0f
	};
	GLfloat lineColor[3] = {
		1.0f, 1.0f, 1.0f,
	};

	float count_ak = 0.0f;
	GLfloat angle_ak = 0.0f;

	float dist_a_b_ak = sqrt((x2_ak - x1_ak) * (x2_ak - x1_ak) + (y2_ak - y1_ak) * (y2_ak - y1_ak) + (z2 - z1_ak) * (z2 - z1_ak));
	float dist_b_c_ak = sqrt((x3_ak - x2_ak) * (x3_ak - x2_ak) + (y3_ak - y2_ak) * (y3_ak - y2_ak) + (z3_ak - z2) * (z3_ak - z2));
	float dist_c_a_ak = sqrt((x1_ak - x3_ak) * (x1_ak - x3_ak) + (y1_ak - y3_ak) * (y1_ak - y3_ak) + (z1_ak - z3_ak) * (z1_ak - z3_ak));

	float semiperimeter_ak = (dist_a_b_ak + dist_b_c_ak + dist_c_a_ak) / 2;
	float radius_ak = sqrt((semiperimeter_ak - dist_a_b_ak) * (semiperimeter_ak - dist_b_c_ak) * (semiperimeter_ak - dist_c_a_ak) / semiperimeter_ak);
	float Ox_ak = (x3_ak * dist_a_b_ak + x1_ak * dist_b_c_ak + x2_ak * dist_c_a_ak) / (semiperimeter_ak * 2);
	float Oy_ak = (y3_ak * dist_a_b_ak + y1_ak * dist_b_c_ak + y2_ak * dist_c_a_ak) / (semiperimeter_ak * 2);
	float Oz_ak = (z3_ak * dist_a_b_ak + z1_ak * dist_b_c_ak + z2 * dist_c_a_ak) / (semiperimeter_ak * 2);

	for (count_ak = 0; count_ak <= 2000; count_ak++)
	{

		lineVertices[0] = cos(angle_ak) * radius_ak + Ox_ak;
		lineVertices[1] = sin(angle_ak) * radius_ak + Oy_ak;
		lineVertices[2] = 0.0f + Oz_ak;

		angle_ak = 2 * M_PI * count_ak / 2000;

		glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_ak);
		glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(float), lineVertices, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindBuffer(GL_ARRAY_BUFFER, Vbo_color_ak);
		glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(float), lineColor, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glDrawArrays(GL_POINTS, 0, 1);

	}

}

void wand(float x1_ak, float y1_ak, float z1_ak, float x2_ak, float y2_ak, float z2_ak)
{
	GLfloat lineVertices[6] = {
	x1_ak, y1_ak, z1_ak,
	x2_ak, y2_ak, z2_ak
	};
	GLfloat lineColor[6] = {
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f
	};


	glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_ak);
	glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineVertices, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, Vbo_color_ak);
	glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineColor, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDrawArrays(GL_LINES, 0, 2);
}

void uninitialize(void)
{
	if (gbFullscreen_ak == true)
	{
		dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);
		SetWindowLong(ghwnd_ak, GWL_STYLE, dwStyle_ak | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
		SetWindowPos(ghwnd_ak, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	if (Vao_ak)
	{
		glDeleteVertexArrays(1, &Vao_ak);
		Vao_ak = 0;
	}

	if (Vbo_position_ak)
	{
		glDeleteVertexArrays(1, &Vbo_position_ak);
		Vbo_position_ak = 0;
	}

	if (Vbo_color_ak)
	{
		glDeleteVertexArrays(1, &Vbo_color_ak);
		Vbo_color_ak = 0;
	}

	// glDetachShader(gShaderProgramObject_ak, gVertexShaderObject_ak);
	// glDetachShader(gShaderProgramObject_ak, gFragmentShaderObject_ak);

	// glDeleteShader(gVertexShaderObject_ak);
	// gVertexShaderObject_ak = 0;

	// glDeleteShader(gFragmentShaderObject_ak);
	// gFragmentShaderObject_ak = 0;

	// glUseProgram(0);

	// Safe Shader Release
	if (gShaderProgramObject_ak) {
		glUseProgram(gShaderProgramObject_ak);
		GLsizei shaderCount_ak;
		glGetProgramiv(gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

		GLuint* pShaders_ak = NULL;
		pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
		glGetAttachedShaders(gShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);

		for (GLsizei i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
			glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
			glDeleteShader(pShaders_ak[i_ak]);
			pShaders_ak[i_ak] = 0;
		}

		free(pShaders_ak);
		glDeleteProgram(gShaderProgramObject_ak);
		gShaderProgramObject_ak = 0;

		glUseProgram(0);
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc_ak);
	ghrc_ak = NULL;

	ReleaseDC(ghwnd_ak, ghdc_ak);
	ghdc_ak = NULL;

	if (gpFile_ak)
	{
		fprintf(gpFile_ak, "Log File Is Successfully Closed.\n");
		fclose(gpFile_ak);
		gpFile_ak = NULL;
	}
}