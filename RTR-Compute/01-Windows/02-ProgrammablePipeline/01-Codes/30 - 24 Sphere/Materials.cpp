#include "Icon.h"
#include "PerVertexLighting.h"
#include "PerFragmentLighting.h"

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gpFile_ak = NULL;
HWND ghwnd_ak = NULL;
DWORD dwStyle_ak = NULL;
WINDOWPLACEMENT wpPrev_ak = { sizeof(WINDOWPLACEMENT) };
bool gbFullScreen_ak = false;

HDC ghdc_ak = NULL;
HGLRC ghrc_ak = NULL;
bool gbActiveWindow_ak = false;

mat4 perspectiveProjectionMatrix_ak = mat4::identity();;

float sphere_vertices_ak[1146];
float sphere_normals_ak[1146];
float sphere_textures_ak[764];
unsigned short sphere_elements_ak[2280];

int gNumVertices_ak;
int gNumElements_ak;

bool bLight_ak = false;

GLfloat lightAmbient_ak[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffused_ak[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition_ak[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightSpecular_ak[] = { 1.0f, 1.0f, 1.0f };

GLfloat angleForXRotation_ak = 0.0f;
GLfloat angleForYRotation_ak = 0.0f;
GLfloat angleForZRotation_ak = 0.0f;

GLint keyPressed_ak = 0;

int gWidth = 800;
int	gHeight = 600;

int  programToBeExecuted = 0;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
	void Initialize(void);
	void Display(void);
	void Update(void);
	WNDCLASSEX wndclass_ak;
	HWND hwnd_ak;
	MSG msg_ak;
	TCHAR szAppName_ak[] = TEXT("MyApp");
	bool bDone_ak = false;

	if (fopen_s(&gpFile_ak, "Logs.txt", "w") != 0) {
		MessageBox(NULL, TEXT("Failure creating file"), TEXT("File IO failed"), MB_ICONSTOP | MB_OK);
		exit(0);
	}
	
	int iScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	int iScreenHeight = GetSystemMetrics(SM_CYSCREEN);

	int iScreenCenterX = iScreenWidth / 2;
	int iScreenCenterY = iScreenHeight / 2;

	int iWindowCenterX = WIN_WIDTH_ak / 2;
	int iWindowsCenterY = WIN_HEIGHT_ak / 2;

	int iWindowX = iScreenCenterX - iWindowCenterX;
	int iWindowY = iScreenCenterY - iWindowsCenterY;

	wndclass_ak.cbSize = sizeof(WNDCLASSEX);
	wndclass_ak.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass_ak.cbClsExtra = 0;
	wndclass_ak.cbWndExtra = 0;
	wndclass_ak.hInstance = hInstance;
	wndclass_ak.lpfnWndProc = WndProc;
	wndclass_ak.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));
	wndclass_ak.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass_ak.lpszClassName = szAppName_ak;
	wndclass_ak.lpszMenuName = NULL;
	wndclass_ak.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass_ak.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));

	RegisterClassEx(&wndclass_ak);

	hwnd_ak = CreateWindowEx(
		WS_EX_APPWINDOW, szAppName_ak, TEXT("24 Materials"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, iWindowX, iWindowY, WIN_WIDTH_ak, WIN_HEIGHT_ak, NULL, NULL, hInstance, NULL);
	ghwnd_ak = hwnd_ak;

	Initialize();
	ShowWindow(hwnd_ak, iCmdShow);
	SetForegroundWindow(hwnd_ak);
	SetFocus(hwnd_ak);

	while (bDone_ak == false) {
		if (PeekMessage(&msg_ak, NULL, 0, 0, PM_REMOVE)) {
			if (msg_ak.message == WM_QUIT) {
				bDone_ak = true;
			}
			else {
				TranslateMessage(&msg_ak);
				DispatchMessage(&msg_ak);
			}
		}
		else {
			if (gbActiveWindow_ak == true) {
				Display();
				Update();
			}
		}
	}

	return ((int)msg_ak.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	void ToggleFullScreen(void);
	void Resize(int, int);
	void Uninitialize(void);

	switch (iMsg) {
	case WM_SETFOCUS: {
		gbActiveWindow_ak = true;
		break;
	}
	case WM_KILLFOCUS: {
		gbActiveWindow_ak = false;
		break;
	}
	case WM_ERASEBKGND: {
		return (0);
	}
	case WM_SIZE: {
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	}
	case WM_KEYDOWN: {
		switch (wParam) {
		case VK_ESCAPE: {
			ToggleFullScreen();
			break;
		}
		default: {
			break;
		}
		}
		break;
	}
	case WM_CHAR: {
		switch (wParam) {
		case 'L':
		case 'l': {
			if (bLight_ak) {
				bLight_ak = false;
			}
			else {
				bLight_ak = true;
			}
			break;
		}
		case 'f':
		case 'F': {
			programToBeExecuted = 1;
			break;
		}
		case 'v':
		case 'V': {
			programToBeExecuted = 0;
			break;
		}
		case 'q':
		case 'Q': {
			DestroyWindow(hwnd);
			break;
		}
		case 'x':
		case 'X':
		{
			keyPressed_ak = 1;
			angleForXRotation_ak = 0.0f;
			angleForYRotation_ak = 0.0f;
			angleForZRotation_ak = 0.0f;
			lightPosition_ak[0] = 0.0f;
			lightPosition_ak[1] = 0.0f;
			lightPosition_ak[2] = 0.0f;
			break;
		}
		case 'y':
		case 'Y':
		{
			keyPressed_ak = 2;
			angleForXRotation_ak = 0.0f;
			angleForYRotation_ak = 0.0f;
			angleForZRotation_ak = 0.0f;
			lightPosition_ak[0] = 0.0f;
			lightPosition_ak[1] = 0.0f;
			lightPosition_ak[2] = 0.0f;
			break;
		}
		case 'z':
		case 'Z':
		{
			keyPressed_ak = 3;
			angleForXRotation_ak = 0.0f;
			angleForYRotation_ak = 0.0f;
			angleForZRotation_ak = 0.0f;
			lightPosition_ak[0] = 0.0f;
			lightPosition_ak[1] = 0.0f;
			lightPosition_ak[2] = 0.0f;
			break;
		}
		case 's':
		case 'S':
		{
			keyPressed_ak = 0;
			angleForXRotation_ak = 0.0f;
			angleForYRotation_ak = 0.0f;
			angleForZRotation_ak = 0.0f;
			lightPosition_ak[0] = 0.0f;
			lightPosition_ak[1] = 0.0f;
			lightPosition_ak[2] = 0.0f;
			break;
		}
		}
		break;
	}
	case WM_CLOSE: {
		DestroyWindow(hwnd);
		break;
	}
	case WM_DESTROY: {
		Uninitialize();
		PostQuitMessage(0);
		break;
	}
	default: {
		break;
	}
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void) {
	MONITORINFO mi_ak = { sizeof(MONITORINFO) };

	if (gbFullScreen_ak == false) {

		dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);
		if ((dwStyle_ak & WS_OVERLAPPEDWINDOW)) {
			if ((GetWindowPlacement(ghwnd_ak, &wpPrev_ak) &&
				(GetMonitorInfo(MonitorFromWindow(ghwnd_ak, MONITORINFOF_PRIMARY), &mi_ak)))) {
				SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak & (~WS_OVERLAPPEDWINDOW)));
				SetWindowPos(ghwnd_ak,
					HWND_TOP,
					mi_ak.rcMonitor.left,
					mi_ak.rcMonitor.top,
					(mi_ak.rcMonitor.right - mi_ak.rcMonitor.left),
					(mi_ak.rcMonitor.bottom - mi_ak.rcMonitor.top),
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen_ak = true;
	}
	else {
		SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
		SetWindowPos(ghwnd_ak,
			HWND_TOP,
			0,
			0,
			0,
			0,
			(SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOOWNERZORDER));
		ShowCursor(TRUE);
		gbFullScreen_ak = false;
	}
}

void Initialize(void) {
	void Resize(int, int);
	void Uninitialize(void);
	PIXELFORMATDESCRIPTOR pFD_ak;
	int iPixelFormatIndex_ak;

	ghdc_ak = GetDC(ghwnd_ak);

	ZeroMemory(&pFD_ak, sizeof(PIXELFORMATDESCRIPTOR));
	pFD_ak.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pFD_ak.nVersion = 1;
	pFD_ak.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pFD_ak.iPixelType = PFD_TYPE_RGBA;
	pFD_ak.cColorBits = 32;
	pFD_ak.cRedBits = 8;
	pFD_ak.cGreenBits = 8;
	pFD_ak.cBlueBits = 8;
	pFD_ak.cAlphaBits = 8;
	pFD_ak.cDepthBits = 32;

	iPixelFormatIndex_ak = ChoosePixelFormat(ghdc_ak, &pFD_ak);
	if (iPixelFormatIndex_ak == 0) {
		DestroyWindow(ghwnd_ak);
	}

	if (SetPixelFormat(ghdc_ak, iPixelFormatIndex_ak, &pFD_ak) == FALSE) {
		DestroyWindow(ghwnd_ak);
	}

	ghrc_ak = wglCreateContext(ghdc_ak);
	if (ghrc_ak == NULL) {
		DestroyWindow(ghwnd_ak);
	}

	if (wglMakeCurrent(ghdc_ak, ghrc_ak) == FALSE) {
		DestroyWindow(ghwnd_ak);
	}

	GLenum glew_error_ak = glewInit();
	if (glew_error_ak != GLEW_OK) {
		wglDeleteContext(ghrc_ak);
		ghrc_ak = NULL;
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	fprintf(gpFile_ak, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
	fprintf(gpFile_ak, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile_ak, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile_ak, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	GLint numExt_ak;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExt_ak);
	for (int i = 0; i < numExt_ak; i++) {
		fprintf(gpFile_ak, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
	}

	getSphereVertexData(sphere_vertices_ak, sphere_normals_ak, sphere_textures_ak, sphere_elements_ak);
	gNumVertices_ak = getNumberOfSphereVertices();
	gNumElements_ak = getNumberOfSphereElements();

	PerVertexLighting_Initialize();
	PerFragmentLighting_Initialize();

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	Resize(WIN_WIDTH_ak, WIN_HEIGHT_ak);
}

void Resize(int width, int height) {
	if (height == 0) {
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix_ak = perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);

	gWidth = width;
	gHeight = height;
	switch (programToBeExecuted) {
	case 0: {
		PerVertexLighting_Resize(width, height);
		break;
	}
	case 1: {
		PerFragmentLighting_Resize(width, height);
		break;
	}
	default: {
		PerVertexLighting_Resize(width, height);
		break;
	}
	}
}

void Display(void) {

	switch (programToBeExecuted) {
	case 0: {
		PerVertexLighting_Display();
		break;
	}
	case 1: {
		PerFragmentLighting_Display();
		break;
	}
	default: {
		PerVertexLighting_Display();
		break;
	}
	}

	SwapBuffers(ghdc_ak);
}

void Update(void) {
	angleForXRotation_ak = angleForXRotation_ak + 0.01f;
	if (angleForXRotation_ak > 2 * M_PI)
		angleForXRotation_ak = 0.0f;

	if (keyPressed_ak == 1) {
		lightPosition_ak[1] = 100 * sin(angleForXRotation_ak);
		lightPosition_ak[2] = 100 * cos(angleForXRotation_ak);
	}

	angleForYRotation_ak = angleForYRotation_ak + 0.01f;
	if (angleForYRotation_ak > 2 * M_PI)
		angleForYRotation_ak = 0.0f;
	if (keyPressed_ak == 2) {
		lightPosition_ak[0] = 100 * sin(angleForYRotation_ak);
		lightPosition_ak[2] = 100 * cos(angleForYRotation_ak);
	}

	angleForZRotation_ak = angleForZRotation_ak + 0.01f;
	if (angleForZRotation_ak > 2 * M_PI)
		angleForZRotation_ak = 0.0f;
	if (keyPressed_ak == 3) {
		lightPosition_ak[0] = 100 * sin(angleForZRotation_ak);
		lightPosition_ak[1] = 100 * cos(angleForZRotation_ak);
	}
}

void Uninitialize(void) {
	if (gbFullScreen_ak == true) {
		dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);

		SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
		SetWindowPos(ghwnd_ak,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	PerVertexLighting_Uninitialize();
	PerFragmentLighting_Uninitialize();

	if (wglGetCurrentContext() == ghrc_ak) {
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc_ak) {
		wglDeleteContext(ghrc_ak);
		ghrc_ak = NULL;
	}

	if (ghdc_ak) {
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	if (gpFile_ak) {
		fclose(gpFile_ak);
		gpFile_ak = NULL;
	}
}