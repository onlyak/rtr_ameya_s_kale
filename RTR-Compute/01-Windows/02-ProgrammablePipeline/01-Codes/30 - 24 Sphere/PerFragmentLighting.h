#pragma once

void PerFragmentLighting_Initialize(void);
void PerFragmentLighting_Display(void);
void PerFragmentLighting_Update(void);
void PerFragmentLighting_Resize(int width, int height);
void PerFragmentLighting_Uninitialize(void);
