#include "Materials.h"
#include "PerVertexLighting.h"

GLuint perVertexLighting_VertexShaderObject_PSM;
GLuint perVertexLighting_FragmentShaderObject_PSM;
GLuint perVertexLighting_ShaderProgramObject_PSM;

GLuint perVertexLighting_vao_sphere_PSM;
GLuint perVertexLighting_vbo_position_sphere_PSM;
GLuint perVertexLighting_vbo_normal_sphere_PSM;
GLuint perVertexLighting_vbo_element_sphere_PSM;

GLuint perVertexLighting_modelMatrixUniform_PSM;
GLuint perVertexLighting_viewMatrixUniform_PSM;
GLuint perVertexLighting_projectionMatrixUniform_PSM;

GLuint perVertexLighting_laUniform_PSM;
GLuint perVertexLighting_ldUniform_PSM;
GLuint perVertexLighting_lsUniform_PSM;
GLuint perVertexLighting_lightPositionUniform_PSM;

GLuint perVertexLighting_kaUniform_PSM;
GLuint perVertexLighting_kdUniform_PSM;
GLuint perVertexLighting_ksUniform_PSM;
GLuint perVertexLighting_materialShininessUniform_PSM;

GLuint perVertexLighting_lKeyPressedUniform_PSM;

void PerVertexLighting_Initialize(void) {
    void PerVertexLighting_Resize(int, int);
    void PerVertexLighting_Uninitialize(void);

    perVertexLighting_VertexShaderObject_PSM = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 450 core"
        "\n"
        "in vec4 vPosition;"
        "in vec3 vNormal;"
        "uniform mat4 u_model_matrix;"
        "uniform mat4 u_view_matrix;"
        "uniform mat4 u_projection_matrix;"
        "uniform vec3 u_la;"
        "uniform vec3 u_ld;"
        "uniform vec3 u_ls;"
        "uniform vec4 u_light_position;"
        "uniform vec3 u_ka;"
        "uniform vec3 u_kd;"
        "uniform vec3 u_ks;"
        "uniform float u_material_shininess;"
        "uniform int u_lkey_pressed;"
        "out vec3 phong_ads_light;"
        "void main(void)"
        "{"
        "if(u_lkey_pressed == 1)"
        "{"
        "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"
        "vec3 transformed_normal = normalize(mat3((u_view_matrix * u_model_matrix)) * vNormal);"
        "vec3 light_direction = normalize(vec3(u_light_position - eye_coordinates));"
        "vec3 reflection_vector = reflect(-light_direction, transformed_normal);"
        "vec3 view_vector = normalize(-eye_coordinates.xyz);"
        "vec3 ambient = u_la * u_ka;"
        "vec3 diffuse = u_ld * u_kd * max(dot(light_direction, transformed_normal), 0.0);"
        "vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, view_vector), 0.0), u_material_shininess);"
        "phong_ads_light = ambient + diffuse + specular;"
        "}"
        "else"
        "{"
        "phong_ads_light = vec3(1.0, 1.0, 1.0);"
        "}"
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"
        "}";

    glShaderSource(perVertexLighting_VertexShaderObject_PSM, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(perVertexLighting_VertexShaderObject_PSM);

    GLint infoLogLength_PSM = 0;
    GLint shaderCompileStatus_PSM = 0;
    char* szBuffer_PSM = NULL;
    glGetShaderiv(perVertexLighting_VertexShaderObject_PSM, GL_COMPILE_STATUS, &shaderCompileStatus_PSM);
    if (shaderCompileStatus_PSM == GL_FALSE) {
        glGetShaderiv(perVertexLighting_VertexShaderObject_PSM, GL_INFO_LOG_LENGTH, &infoLogLength_PSM);
        if (infoLogLength_PSM > 0) {
            szBuffer_PSM = (char*)malloc(sizeof(char) * infoLogLength_PSM);
            if (szBuffer_PSM != NULL) {
                GLsizei written_PSM;
                glGetShaderInfoLog(perVertexLighting_VertexShaderObject_PSM, infoLogLength_PSM, &written_PSM, szBuffer_PSM);
                fprintf(gpFile_PSM, "Vertex shader compilation log: %s\n", szBuffer_PSM);
                free(szBuffer_PSM);
                DestroyWindow(ghwnd_PSM);
            }
        }
    }

    perVertexLighting_FragmentShaderObject_PSM = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentSourceCode_PSM =
        "#version 450 core"
        "\n"
        "in vec3 phong_ads_light;"
        "out vec4 FragColor;"
        "void main(void)"
        "{"
        "FragColor = vec4(phong_ads_light, 1.0);"
        "}";

    glShaderSource(perVertexLighting_FragmentShaderObject_PSM, 1, (const GLchar**)&fragmentSourceCode_PSM, NULL);
    glCompileShader(perVertexLighting_FragmentShaderObject_PSM);

    infoLogLength_PSM = 0;
    shaderCompileStatus_PSM = 0;
    szBuffer_PSM = NULL;
    glGetShaderiv(perVertexLighting_FragmentShaderObject_PSM, GL_COMPILE_STATUS, &shaderCompileStatus_PSM);
    if (shaderCompileStatus_PSM == GL_FALSE) {
        glGetShaderiv(perVertexLighting_FragmentShaderObject_PSM, GL_INFO_LOG_LENGTH, &infoLogLength_PSM);
        if (infoLogLength_PSM > 0) {
            szBuffer_PSM = (char*)malloc(sizeof(char) * infoLogLength_PSM);
            if (szBuffer_PSM != NULL) {
                GLsizei written_PSM;
                glGetShaderInfoLog(perVertexLighting_FragmentShaderObject_PSM, infoLogLength_PSM, &written_PSM, szBuffer_PSM);
                fprintf(gpFile_PSM, "Fragment shader compilation log: %s\n", szBuffer_PSM);
                free(szBuffer_PSM);
                DestroyWindow(ghwnd_PSM);
            }
        }
    }

    perVertexLighting_ShaderProgramObject_PSM = glCreateProgram();
    glAttachShader(perVertexLighting_ShaderProgramObject_PSM, perVertexLighting_VertexShaderObject_PSM);
    glAttachShader(perVertexLighting_ShaderProgramObject_PSM, perVertexLighting_FragmentShaderObject_PSM);

    glBindAttribLocation(perVertexLighting_ShaderProgramObject_PSM, ATTRIBUTE_POSITION_PSM, "vPosition");
    glBindAttribLocation(perVertexLighting_ShaderProgramObject_PSM, ATTRIBUTE_NORMAL_PSM, "vNormal");

    glLinkProgram(perVertexLighting_ShaderProgramObject_PSM);

    GLint shaderProgramLinkStatus = 0;
    glGetProgramiv(perVertexLighting_ShaderProgramObject_PSM, GL_LINK_STATUS, &shaderProgramLinkStatus);
    if (shaderProgramLinkStatus == GL_FALSE) {
        glGetProgramiv(perVertexLighting_ShaderProgramObject_PSM, GL_INFO_LOG_LENGTH, &infoLogLength_PSM);
        if (infoLogLength_PSM > 0) {
            szBuffer_PSM = (char*)malloc(sizeof(char) * infoLogLength_PSM);
            if (szBuffer_PSM != NULL) {
                GLsizei written_PSM;
                glGetProgramInfoLog(perVertexLighting_ShaderProgramObject_PSM, infoLogLength_PSM, &written_PSM, szBuffer_PSM);
                fprintf(gpFile_PSM, "Shader program link log: %s\n", szBuffer_PSM);
                free(szBuffer_PSM);
                DestroyWindow(ghwnd_PSM);
            }
        }
    }

    perVertexLighting_modelMatrixUniform_PSM = glGetUniformLocation(perVertexLighting_ShaderProgramObject_PSM, "u_model_matrix");
    perVertexLighting_viewMatrixUniform_PSM = glGetUniformLocation(perVertexLighting_ShaderProgramObject_PSM, "u_view_matrix");
    perVertexLighting_projectionMatrixUniform_PSM = glGetUniformLocation(perVertexLighting_ShaderProgramObject_PSM, "u_projection_matrix");
    perVertexLighting_laUniform_PSM = glGetUniformLocation(perVertexLighting_ShaderProgramObject_PSM, "u_la");
    perVertexLighting_ldUniform_PSM = glGetUniformLocation(perVertexLighting_ShaderProgramObject_PSM, "u_ld");
    perVertexLighting_lsUniform_PSM = glGetUniformLocation(perVertexLighting_ShaderProgramObject_PSM, "u_ls");
    perVertexLighting_lightPositionUniform_PSM = glGetUniformLocation(perVertexLighting_ShaderProgramObject_PSM, "u_light_position");
    perVertexLighting_kaUniform_PSM = glGetUniformLocation(perVertexLighting_ShaderProgramObject_PSM, "u_ka");
    perVertexLighting_kdUniform_PSM = glGetUniformLocation(perVertexLighting_ShaderProgramObject_PSM, "u_kd");
    perVertexLighting_ksUniform_PSM = glGetUniformLocation(perVertexLighting_ShaderProgramObject_PSM, "u_ks");
    perVertexLighting_materialShininessUniform_PSM = glGetUniformLocation(perVertexLighting_ShaderProgramObject_PSM, "u_material_shininess");
    perVertexLighting_lKeyPressedUniform_PSM = glGetUniformLocation(perVertexLighting_ShaderProgramObject_PSM, "u_lkey_pressed");

    glGenVertexArrays(1, &perVertexLighting_vao_sphere_PSM);
    glBindVertexArray(perVertexLighting_vao_sphere_PSM);

    glGenBuffers(1, &perVertexLighting_vbo_position_sphere_PSM);
    glBindBuffer(GL_ARRAY_BUFFER, perVertexLighting_vbo_position_sphere_PSM);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices_PSM), sphere_vertices_PSM, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE_POSITION_PSM, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(ATTRIBUTE_POSITION_PSM);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &perVertexLighting_vbo_normal_sphere_PSM);
    glBindBuffer(GL_ARRAY_BUFFER, perVertexLighting_vbo_normal_sphere_PSM);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals_PSM), sphere_normals_PSM, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE_NORMAL_PSM, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(ATTRIBUTE_NORMAL_PSM);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &perVertexLighting_vbo_element_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements_PSM), sphere_elements_PSM, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
    glEnable(GL_AUTO_NORMAL);
    glEnable(GL_NORMALIZE);
    perspectiveProjectionMatrix_PSM = mat4::identity();
}

void PerVertexLighting_Resize(int width, int height) {
}

void PerVertexLighting_Display(void) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(perVertexLighting_ShaderProgramObject_PSM);

    mat4 modelMatrix_PSM;
    mat4 viewMatrix_PSM;
    mat4 translateMatrix_PSM;

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
    } else {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 0);
    }

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    GLfloat materialAmbient_PSM[4];
    GLfloat materialDiffused_PSM[4];
    GLfloat materialSpecular_PSM[4];
    GLfloat materialShininess_PSM;

    // emerald
    materialAmbient_PSM[0] = 0.0215f;
    materialAmbient_PSM[1] = 0.1745f;
    materialAmbient_PSM[2] = 0.0215f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.07568f;
    materialDiffused_PSM[1] = 0.61424f;
    materialDiffused_PSM[2] = 0.07568f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.633f;
    materialSpecular_PSM[1] = 0.727811f;
    materialSpecular_PSM[2] = 0.633f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.6f * 128.0f;
    glViewport(gWidth * (-0.40f), gHeight * (0.35f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // jade
    materialAmbient_PSM[0] = 0.135f;
    materialAmbient_PSM[1] = 0.2225f;
    materialAmbient_PSM[2] = 0.1575f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.54f;
    materialDiffused_PSM[1] = 0.89f;
    materialDiffused_PSM[2] = 0.63f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.316228f;
    materialSpecular_PSM[1] = 0.316228f;
    materialSpecular_PSM[2] = 0.316228f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.1 * 128.0f;
    glViewport(gWidth * (-0.40f), gHeight * (0.20f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // obsidian
    materialAmbient_PSM[0] = 0.05375f;
    materialAmbient_PSM[1] = 0.05f;
    materialAmbient_PSM[2] = 0.06625f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.18275f;
    materialDiffused_PSM[1] = 0.17f;
    materialDiffused_PSM[2] = 0.22525f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.332741f;
    materialSpecular_PSM[1] = 0.328634f;
    materialSpecular_PSM[2] = 0.346435f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.3 * 128.0f;
    glViewport(gWidth * (-0.40f), gHeight * (0.05f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // pearl
    materialAmbient_PSM[0] = 0.25f;
    materialAmbient_PSM[1] = 0.20725f;
    materialAmbient_PSM[2] = 0.20725f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 1.0f;
    materialDiffused_PSM[1] = 0.829f;
    materialDiffused_PSM[2] = 0.829f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.296648f;
    materialSpecular_PSM[1] = 0.296648f;
    materialSpecular_PSM[2] = 0.296648f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.088 * 128.0f;
    glViewport(gWidth * (-0.40f), gHeight * (-0.10), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // ruby
    materialAmbient_PSM[0] = 0.1745f;
    materialAmbient_PSM[1] = 0.01175f;
    materialAmbient_PSM[2] = 0.01175f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.61424f;
    materialDiffused_PSM[1] = 0.04136f;
    materialDiffused_PSM[2] = 0.04136f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.727811f;
    materialSpecular_PSM[1] = 0.626959f;
    materialSpecular_PSM[2] = 0.626959f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.6 * 128.0f;
    glViewport(gWidth * (-0.40f), gHeight * (-0.25f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // turquoise
    materialAmbient_PSM[0] = 0.1f;
    materialAmbient_PSM[1] = 0.18725f;
    materialAmbient_PSM[2] = 0.1745f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.396f;
    materialDiffused_PSM[1] = 0.74151f;
    materialDiffused_PSM[2] = 0.69102f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.297254f;
    materialSpecular_PSM[1] = 0.30829f;
    materialSpecular_PSM[2] = 0.306678f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.1 * 128.0f;
    glViewport(gWidth * (-0.40f), gHeight * (-0.40f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // brass
    materialAmbient_PSM[0] = 0.329412f;
    materialAmbient_PSM[1] = 0.223529f;
    materialAmbient_PSM[2] = 0.027451f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.780392f;
    materialDiffused_PSM[1] = 0.568627f;
    materialDiffused_PSM[2] = 0.113725f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.992157f;
    materialSpecular_PSM[1] = 0.941176f;
    materialSpecular_PSM[2] = 0.807843f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.21794872 * 128.0f;
    glViewport(gWidth * (-0.145f), gHeight * (0.35f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);
    // bronze
    materialAmbient_PSM[0] = 0.2125f;
    materialAmbient_PSM[1] = 0.1275f;
    materialAmbient_PSM[2] = 0.054f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.714f;
    materialDiffused_PSM[1] = 0.4284f;
    materialDiffused_PSM[2] = 0.18144f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.393548f;
    materialSpecular_PSM[1] = 0.271906f;
    materialSpecular_PSM[2] = 0.166721f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.2 * 128.0f;
    glViewport(gWidth * (-0.145f), gHeight * (0.20f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // chrome
    materialAmbient_PSM[0] = 0.25f;
    materialAmbient_PSM[1] = 0.25f;
    materialAmbient_PSM[2] = 0.25f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.4f;
    materialDiffused_PSM[1] = 0.4f;
    materialDiffused_PSM[2] = 0.4f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.774597f;
    materialSpecular_PSM[1] = 0.774597f;
    materialSpecular_PSM[2] = 0.774597f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.6 * 128.0f;
    glViewport(gWidth * (-0.145f), gHeight * (0.05f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // copper
    materialAmbient_PSM[0] = 0.19125f;
    materialAmbient_PSM[1] = 0.0735f;
    materialAmbient_PSM[2] = 0.0225f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.7038f;
    materialDiffused_PSM[1] = 0.27048f;
    materialDiffused_PSM[2] = 0.0828f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.256777f;
    materialSpecular_PSM[1] = 0.137622f;
    materialSpecular_PSM[2] = 0.086014f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.1 * 128.0f;
    glViewport(gWidth * (-0.145f), gHeight * (-0.1f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // gold
    materialAmbient_PSM[0] = 0.24725f;
    materialAmbient_PSM[1] = 0.1995f;
    materialAmbient_PSM[2] = 0.0745f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.75164f;
    materialDiffused_PSM[1] = 0.60648f;
    materialDiffused_PSM[2] = 0.22648f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.628281f;
    materialSpecular_PSM[1] = 0.555802f;
    materialSpecular_PSM[2] = 0.366065f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.4 * 128.0f;
    glViewport(gWidth * (-0.145f), gHeight * (-0.25f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // silver
    materialAmbient_PSM[0] = 0.19225f;
    materialAmbient_PSM[1] = 0.19225f;
    materialAmbient_PSM[2] = 0.19225f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.50754f;
    materialDiffused_PSM[1] = 0.50754f;
    materialDiffused_PSM[2] = 0.50754f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.508273f;
    materialSpecular_PSM[1] = 0.508273f;
    materialSpecular_PSM[2] = 0.508273f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.4 * 128.0f;
    glViewport(gWidth * (-0.145f), gHeight * (-0.40f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // black
    materialAmbient_PSM[0] = 0.0f;
    materialAmbient_PSM[1] = 0.0f;
    materialAmbient_PSM[2] = 0.0f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.01f;
    materialDiffused_PSM[1] = 0.01f;
    materialDiffused_PSM[2] = 0.01f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.50f;
    materialSpecular_PSM[1] = 0.50f;
    materialSpecular_PSM[2] = 0.50f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.25 * 128.0f;
    glViewport(gWidth * (0.145f), gHeight * (0.35f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // cyan
    materialAmbient_PSM[0] = 0.0f;
    materialAmbient_PSM[1] = 0.1f;
    materialAmbient_PSM[2] = 0.06f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.0f;
    materialDiffused_PSM[1] = 0.50980392f;
    materialDiffused_PSM[2] = 0.50980392f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.50196078f;
    materialSpecular_PSM[1] = 0.50196078f;
    materialSpecular_PSM[2] = 0.50196078f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.25 * 128.0f;
    glViewport(gWidth * (0.145f), gHeight * (0.20f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // green
    materialAmbient_PSM[0] = 0.0f;
    materialAmbient_PSM[1] = 0.0f;
    materialAmbient_PSM[2] = 0.0f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.1f;
    materialDiffused_PSM[1] = 0.35f;
    materialDiffused_PSM[2] = 0.1f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.45f;
    materialSpecular_PSM[1] = 0.55f;
    materialSpecular_PSM[2] = 0.45f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.25 * 128.0f;
    glViewport(gWidth * (0.145f), gHeight * (0.05f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // red
    materialAmbient_PSM[0] = 0.0f;
    materialAmbient_PSM[1] = 0.0f;
    materialAmbient_PSM[2] = 0.0f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.5f;
    materialDiffused_PSM[1] = 0.0f;
    materialDiffused_PSM[2] = 0.0f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.7f;
    materialSpecular_PSM[1] = 0.6f;
    materialSpecular_PSM[2] = 0.6f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.25 * 128.0f;
    glViewport(gWidth * (0.145f), gHeight * (-0.10f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // white
    materialAmbient_PSM[0] = 0.0f;
    materialAmbient_PSM[1] = 0.0f;
    materialAmbient_PSM[2] = 0.0f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.55f;
    materialDiffused_PSM[1] = 0.55f;
    materialDiffused_PSM[2] = 0.55f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.70f;
    materialSpecular_PSM[1] = 0.70f;
    materialSpecular_PSM[2] = 0.70f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.25 * 128.0f;
    glViewport(gWidth * (0.145f), gHeight * (-0.25f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // yellow plastic
    materialAmbient_PSM[0] = 0.0f;
    materialAmbient_PSM[1] = 0.0f;
    materialAmbient_PSM[2] = 0.0f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.5f;
    materialDiffused_PSM[1] = 0.5f;
    materialDiffused_PSM[2] = 0.0f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.60f;
    materialSpecular_PSM[1] = 0.60f;
    materialSpecular_PSM[2] = 0.50f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.25 * 128.0f;
    glViewport(gWidth * (0.145f), gHeight * (-0.40f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // black
    materialAmbient_PSM[0] = 0.02f;
    materialAmbient_PSM[1] = 0.02f;
    materialAmbient_PSM[2] = 0.02f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.01f;
    materialDiffused_PSM[1] = 0.01f;
    materialDiffused_PSM[2] = 0.01f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.4f;
    materialSpecular_PSM[1] = 0.4f;
    materialSpecular_PSM[2] = 0.4f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.078125 * 128.0f;
    glViewport(gWidth * (0.40f), gHeight * (0.35f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // cyan
    materialAmbient_PSM[0] = 0.0f;
    materialAmbient_PSM[1] = 0.05f;
    materialAmbient_PSM[2] = 0.05f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.4f;
    materialDiffused_PSM[1] = 0.5f;
    materialDiffused_PSM[2] = 0.5f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.04f;
    materialSpecular_PSM[1] = 0.7f;
    materialSpecular_PSM[2] = 0.7f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.078125 * 128.0f;
    glViewport(gWidth * (0.40f), gHeight * (0.20f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // green
    materialAmbient_PSM[0] = 0.0f;
    materialAmbient_PSM[1] = 0.05f;
    materialAmbient_PSM[2] = 0.0f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.4f;
    materialDiffused_PSM[1] = 0.5f;
    materialDiffused_PSM[2] = 0.4f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.04f;
    materialSpecular_PSM[1] = 0.7f;
    materialSpecular_PSM[2] = 0.04f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.078125 * 128.0f;
    glViewport(gWidth * (0.40f), gHeight * (0.05f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // red
    materialAmbient_PSM[0] = 0.05f;
    materialAmbient_PSM[1] = 0.0f;
    materialAmbient_PSM[2] = 0.0f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.5f;
    materialDiffused_PSM[1] = 0.4f;
    materialDiffused_PSM[2] = 0.4f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.7f;
    materialSpecular_PSM[1] = 0.04f;
    materialSpecular_PSM[2] = 0.04f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.078125 * 128.0f;
    glViewport(gWidth * (0.40f), gHeight * (-0.10f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // white
    materialAmbient_PSM[0] = 0.05f;
    materialAmbient_PSM[1] = 0.05f;
    materialAmbient_PSM[2] = 0.05f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.5f;
    materialDiffused_PSM[1] = 0.5f;
    materialDiffused_PSM[2] = 0.5f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.7f;
    materialSpecular_PSM[1] = 0.7f;
    materialSpecular_PSM[2] = 0.7f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.078125 * 128.0f;
    glViewport(gWidth * (0.40f), gHeight * (-0.25f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // yellow rubber
    materialAmbient_PSM[0] = 0.05f;
    materialAmbient_PSM[1] = 0.05f;
    materialAmbient_PSM[2] = 0.0f;
    materialAmbient_PSM[3] = 1.0f;

    materialDiffused_PSM[0] = 0.5f;
    materialDiffused_PSM[1] = 0.5f;
    materialDiffused_PSM[2] = 0.4f;
    materialDiffused_PSM[3] = 1.0f;

    materialSpecular_PSM[0] = 0.7f;
    materialSpecular_PSM[1] = 0.7f;
    materialSpecular_PSM[2] = 0.04f;
    materialSpecular_PSM[3] = 1.0f;

    materialShininess_PSM = 0.078125 * 128.0f;
    glViewport(gWidth * (0.40f), gHeight * (-0.40f), gWidth, gHeight);

    if (bLight_PSM == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_PSM, 1);
        glUniform3fv(perVertexLighting_laUniform_PSM, 1.0, lightAmbient_PSM);
        glUniform3fv(perVertexLighting_ldUniform_PSM, 1.0, lightDiffused_PSM);
        glUniform3fv(perVertexLighting_lsUniform_PSM, 1.0, lightSpecular_PSM);
        glUniform4fv(perVertexLighting_lightPositionUniform_PSM, 1.0, lightPosition_PSM);
        glUniform3fv(perVertexLighting_kaUniform_PSM, 1.0, materialAmbient_PSM);
        glUniform3fv(perVertexLighting_kdUniform_PSM, 1.0, materialDiffused_PSM);
        glUniform3fv(perVertexLighting_ksUniform_PSM, 1.0, materialSpecular_PSM);
        glUniform1f(perVertexLighting_materialShininessUniform_PSM, materialShininess_PSM);
    }

    viewMatrix_PSM = mat4::identity();
    modelMatrix_PSM = mat4::identity();
    translateMatrix_PSM = mat4::identity();

    translateMatrix_PSM = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_PSM = translateMatrix_PSM;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_PSM, 1, GL_FALSE, modelMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_PSM, 1, GL_FALSE, viewMatrix_PSM);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_PSM, 1, GL_FALSE, perspectiveProjectionMatrix_PSM);

    glBindVertexArray(perVertexLighting_vao_sphere_PSM);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_PSM);
    glDrawElements(GL_TRIANGLES, gNumElements_PSM, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);
    glUseProgram(0);
}

void PerVertexLighting_Update(void) {
}

void PerVertexLighting_Uninitialize(void) {
    if (perVertexLighting_vao_sphere_PSM) {
        glDeleteVertexArrays(1, &perVertexLighting_vao_sphere_PSM);
        perVertexLighting_vao_sphere_PSM = 0;
    }

    if (perVertexLighting_vbo_position_sphere_PSM) {
        glDeleteVertexArrays(1, &perVertexLighting_vbo_position_sphere_PSM);
        perVertexLighting_vbo_position_sphere_PSM = 0;
    }

    if (perVertexLighting_vbo_normal_sphere_PSM) {
        glDeleteVertexArrays(1, &perVertexLighting_vbo_normal_sphere_PSM);
        perVertexLighting_vbo_normal_sphere_PSM = 0;
    }

    if (perVertexLighting_vbo_element_sphere_PSM) {
        glDeleteVertexArrays(1, &perVertexLighting_vbo_element_sphere_PSM);
        perVertexLighting_vbo_element_sphere_PSM = 0;
    }

    if (perVertexLighting_ShaderProgramObject_PSM) {
        glUseProgram(perVertexLighting_ShaderProgramObject_PSM);
        GLsizei shaderCount_PSM;
        glGetProgramiv(perVertexLighting_ShaderProgramObject_PSM, GL_ATTACHED_SHADERS, &shaderCount_PSM);

        GLuint* pShaders_PSM = NULL;
        pShaders_PSM = (GLuint*)malloc(sizeof(GLuint) * shaderCount_PSM);
        glGetAttachedShaders(perVertexLighting_ShaderProgramObject_PSM, shaderCount_PSM, &shaderCount_PSM, pShaders_PSM);

        for (int i_PSM = 0; i_PSM < shaderCount_PSM; i_PSM++) {
            glDetachShader(perVertexLighting_ShaderProgramObject_PSM, pShaders_PSM[i_PSM]);
            glDeleteShader(pShaders_PSM[i_PSM]);
            pShaders_PSM[i_PSM] = 0;
        }

        free(pShaders_PSM);
        glDeleteProgram(perVertexLighting_ShaderProgramObject_PSM);
        perVertexLighting_ShaderProgramObject_PSM = 0;

        glUseProgram(0);
    }
}