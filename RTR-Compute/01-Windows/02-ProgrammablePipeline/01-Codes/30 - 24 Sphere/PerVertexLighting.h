#pragma once

void PerVertexLighting_Initialize(void);
void PerVertexLighting_Display(void);
void PerVertexLighting_Update(void);
void PerVertexLighting_Resize(int width, int height);
void PerVertexLighting_Uninitialize(void);
