#include <limits.h>
#include <stdio.h>
#include "vmath.h"


using namespace vmath;

struct CustomStack {
    mat4 customMatrix;
    struct CustomStack* next;
};

struct CustomStack* newNode(mat4 customMatrix)
{
    struct CustomStack* stackNode =
        (struct CustomStack*)
        malloc(sizeof(struct CustomStack));
    stackNode->customMatrix = customMatrix;
    stackNode->next = NULL;
    return stackNode;
}

int isEmpty(struct CustomStack* root)
{
    return !root;
}

void push(struct CustomStack** root, mat4 customMatrix)
{
    struct CustomStack* stackNode = newNode(customMatrix);
    stackNode->next = *root;
    *root = stackNode;
}

mat4 pop(struct CustomStack** root)
{
    if (isEmpty(*root))
        return INT_MIN;
    struct CustomStack* temp = *root;
    *root = (*root)->next;
    mat4 popped = temp->customMatrix;
    free(temp);

    return popped;
}

mat4 peek(struct CustomStack* root)
{
    if (isEmpty(root))
        return INT_MIN;
    return root->customMatrix;
}


