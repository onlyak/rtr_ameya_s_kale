#include <windows.h>
#include <stdio.h> 
#include "Icon.h"
#include "Sphere.h"
#include "CustomStack.h"

#include <gl\glew.h>

#include <gl\GL.h>

#include "vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

enum
{
	ATTRIBUTE_POSITION = 0,
	ATTRIBUTE_COLOR,
	ATTRIBUTE_NORMAL,
	ATRIBUTE_TEXCOORD,
};

using namespace vmath;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gpFile_ak = NULL;

HWND ghwnd_ak = NULL;
HDC ghdc_ak = NULL;
HGLRC ghrc_ak = NULL;

DWORD dwStyle_ak;
WINDOWPLACEMENT wpPrev_ak = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow_ak = false;
bool gbEscapeKeyIsPressed_ak = false;
bool gbFullscreen_ak = false;

GLuint gVertexShaderObject_ak;
GLuint gFragmentShaderObject_ak;
GLuint gShaderProgramObject_ak;

GLuint mvpMatrixUniform_ak;
GLuint colorUniform_ak;

GLuint Vao_sphere_ak;
GLuint Vbo_position_sphere_ak;
GLuint Vbo_element_sphere_ak;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint numVertices;
GLuint numElements;

int day_ak = 0;
int year_ak = 0;
int hour_ak = 0;

mat4 perspectiveProjectMatrix_ak;

GLfloat sunColor_ak[] = { 1.0f, 1.0f, 0.0f };
GLfloat earthColor_ak[] = { 0.4f, 0.9f, 1.0f };
GLfloat moonColor_ak[] = { 1.0f, 1.0f, 1.0f };

struct CustomStack* solarSystem = NULL;


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update();

	WNDCLASSEX wndclass_ak;
	HWND hwnd_ak;
	MSG msg_ak;
	TCHAR szClassName_ak[] = TEXT("OpenGLPP");
	bool bDone_ak = false;

	if (fopen_s(&gpFile_ak, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile_ak, "Log File Is Successfully Opened.\n");
	}

	int iScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	int iScreenHeight = GetSystemMetrics(SM_CYSCREEN);

	int iScreenCenterX = iScreenWidth / 2;
	int iScreenCenterY = iScreenHeight / 2;

	int iWindowCenterX = WIN_WIDTH / 2;
	int iWindowsCenterY = WIN_HEIGHT / 2;

	int iWindowX = iScreenCenterX - iWindowCenterX;
	int iWindowY = iScreenCenterY - iWindowsCenterY;

	wndclass_ak.cbSize = sizeof(WNDCLASSEX);
	wndclass_ak.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass_ak.cbClsExtra = 0;
	wndclass_ak.cbWndExtra = 0;
	wndclass_ak.hInstance = hInstance;
	wndclass_ak.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass_ak.hIcon = LoadIcon(NULL, MAKEINTRESOURCE(MY_ICON));
	wndclass_ak.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass_ak.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));
	wndclass_ak.lpfnWndProc = WndProc;
	wndclass_ak.lpszClassName = szClassName_ak;
	wndclass_ak.lpszMenuName = NULL;

	RegisterClassEx(&wndclass_ak);

	hwnd_ak = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName_ak,
		TEXT("PP: Solar System"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		iWindowX,
		iWindowY,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);


	ghwnd_ak = hwnd_ak;

	ShowWindow(hwnd_ak, iCmdShow);
	SetForegroundWindow(hwnd_ak);
	SetFocus(hwnd_ak);

	initialize();

	while (bDone_ak == false)
	{
		if (PeekMessage(&msg_ak, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_ak.message == WM_QUIT)
				bDone_ak = true;
			else
			{
				TranslateMessage(&msg_ak);
				DispatchMessage(&msg_ak);
			}
		}
		else
		{
			update();
			display();

			if (gbActiveWindow_ak == true)
			{
				if (gbEscapeKeyIsPressed_ak == true)
					bDone_ak = true;
			}
		}
	}

	uninitialize();

	return((int)msg_ak.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow_ak = true;
		else
			gbActiveWindow_ak = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed_ak = true;
			break;
		case 0x46:
			if (gbFullscreen_ak == false)
			{
				ToggleFullscreen();
				gbFullscreen_ak = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen_ak = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_CHAR:
	{
		switch (wParam)
		{
			case 'D':
			{
				day_ak = (day_ak + 6) % 360;
				break;
			}
			case 'd':
			{
				day_ak = (day_ak - 6) % 360;
				break;
			}
			case 'Y':
			{
				year_ak = (year_ak + 3) % 360;
				day_ak = (day_ak - 6) % 360;
				break;
			}
			case 'y':
			{
				year_ak = (year_ak - 3) % 360;
				day_ak = (day_ak + 6) % 360;
				break;
			}
			default:
			{
				break;
			}
		}
		break;
	}
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi;

	if (gbFullscreen_ak == false)
	{
		dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);
		if (dwStyle_ak & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd_ak, &wpPrev_ak) && GetMonitorInfo(MonitorFromWindow(ghwnd_ak, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd_ak, GWL_STYLE, dwStyle_ak & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd_ak, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		SetWindowLong(ghwnd_ak, GWL_STYLE, dwStyle_ak | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
		SetWindowPos(ghwnd_ak, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	void uninitialize(void);
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc_ak = GetDC(ghwnd_ak);

	iPixelFormatIndex = ChoosePixelFormat(ghdc_ak, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	if (SetPixelFormat(ghdc_ak, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	ghrc_ak = wglCreateContext(ghdc_ak);
	if (ghrc_ak == NULL)
	{
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	if (wglMakeCurrent(ghdc_ak, ghrc_ak) == false)
	{
		wglDeleteContext(ghrc_ak);
		ghrc_ak = NULL;
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc_ak);
		ghrc_ak = NULL;
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	fprintf(gpFile_ak, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
	fprintf(gpFile_ak, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
	fprintf(gpFile_ak, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
	fprintf(gpFile_ak, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	GLint numExten_ak;

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExten_ak);
	for (int i = 0; i < numExten_ak; i++)
	{
		fprintf(gpFile_ak, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
	}

	//Vertex Shader
	//********** Vertex Shader runs per vertex **********
	/* 1. When we specify core, we tell OpenGL to use Programmable Pipeline. i.e Core Profile
	   2. in/out are glsl language specifier. Known to shader language only.
		  in---> loads incoming data from main program. It loads data only once.
	   3. vec4--> [x,y,z,w]
	   4. uniform---> load incoming data from main program. It loads data multiple times
	   5. gl_Position---> in-built variable of Vertex Shader
	*/
	// out_color is sent as an input to Fragment Shader
	gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* vertexShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"uniform mat4 u_mvpMatrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvpMatrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject_ak, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

	// compile Vertex Shader
	glCompileShader(gVertexShaderObject_ak);

	// Error checking for Vertex Shader
	GLint infoLogLength = 0;
	GLint shaderCompiledStatus = 0;
	char* szBuffer = NULL;

	glGetShaderiv(gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if (shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			szBuffer = (char*)malloc(infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_ak, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Vertex Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				szBuffer = NULL;
				DestroyWindow(ghwnd_ak);
			}
		}
	}

	//Fragment Shader
	/* out_color is the output of Vertex Shader */
	gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* vertexFragmentSourceCode =
		"#version 430" \
		"\n" \
		"out vec4 FragColor;" \
		"uniform vec3 u_color;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(u_color, 1.0);" \
		"}";

	glShaderSource(gFragmentShaderObject_ak, 1, (const GLchar**)&vertexFragmentSourceCode, NULL);

	// compile Fragment Shader
	glCompileShader(gFragmentShaderObject_ak);

	// Error Checking for Fragment Shader
	glGetShaderiv(gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
	if (shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			szBuffer = (char*)malloc(infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_ak, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Fragment Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				szBuffer = NULL;
				DestroyWindow(ghwnd_ak);
			}
		}
	}

	//Shader Program
	gShaderProgramObject_ak = glCreateProgram();
	glAttachShader(gShaderProgramObject_ak, gVertexShaderObject_ak);
	glAttachShader(gShaderProgramObject_ak, gFragmentShaderObject_ak);

	// Bind the attributes in shader with the enums in your main program
	/* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
	glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");


	glLinkProgram(gShaderProgramObject_ak);

	// Linking Error Checking
	GLint shaderProgramLinkStatus = 0;
	szBuffer = NULL;

	glGetProgramiv(gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
	if (shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			szBuffer = (char*)malloc(infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_ak, infoLogLength, &written, szBuffer);
				fprintf(gpFile_ak, "Shader Program Link Log: %s\n", szBuffer);
				free(szBuffer);
				szBuffer = NULL;
				DestroyWindow(ghwnd_ak);
			}
		}
	}

	//Get the information of uniform Post linking
	mvpMatrixUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_mvpMatrix");
	colorUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_color");


	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	numVertices = getNumberOfSphereVertices();
	numElements = getNumberOfSphereElements();

	glGenVertexArrays(1, &Vao_sphere_ak);
	glBindVertexArray(Vao_sphere_ak);

	// Sphere position Vbo
	glGenBuffers(1, &Vbo_position_sphere_ak);
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_sphere_ak);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Sphere element Vbo
	glGenBuffers(1, &Vbo_element_sphere_ak);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Vbo_element_sphere_ak);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);          
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Set PerspectiveMatrix to identity matrix 
	perspectiveProjectMatrix_ak = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject_ak);

	mat4 translateMatrix_ak = mat4::identity();
	mat4 xRotationMatrix_ak = mat4::identity();
	mat4 yRotationMatrix_ak = mat4::identity();
	mat4 zRotationMatrix_ak = mat4::identity();
	mat4 scaleMatrix_ak = mat4::identity();
	mat4 modelViewProjectionMatrix_ak = mat4::identity();

	translateMatrix_ak = vmath::translate(0.0f, 0.0f, -5.0f);

	push(&solarSystem, translateMatrix_ak);
		xRotationMatrix_ak = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
		modelViewProjectionMatrix_ak = perspectiveProjectMatrix_ak * peek(solarSystem);
		glUniformMatrix4fv(mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix_ak);

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		glUniform3fv(colorUniform_ak, 1, sunColor_ak);
		glBindVertexArray(Vao_sphere_ak);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Vbo_element_sphere_ak);
		glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
		glBindVertexArray(0);

		push(&solarSystem, translateMatrix_ak);

		mat4 top = peek(solarSystem);
		pop(&solarSystem);

		mat4 second = peek(solarSystem);

		yRotationMatrix_ak = vmath::rotate((GLfloat)year_ak, 0.0f, 1.0f, 0.0f);
		translateMatrix_ak = vmath::translate(1.7f, 0.0f, 0.0f);

		push(&solarSystem, top);
			mat4 intermediate = second * yRotationMatrix_ak * translateMatrix_ak;

			yRotationMatrix_ak = vmath::rotate((GLfloat)day_ak, 0.0f, 1.0f, 0.0f);
			scaleMatrix_ak = vmath::scale(0.5f, 0.5f, 0.5f);
			mat4 mid = intermediate * yRotationMatrix_ak * scaleMatrix_ak;

			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

			modelViewProjectionMatrix_ak = perspectiveProjectMatrix_ak * mid;
			glUniformMatrix4fv(mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix_ak);

			glUniform3fv(colorUniform_ak, 1, earthColor_ak);
			glBindVertexArray(Vao_sphere_ak);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Vbo_element_sphere_ak);
			glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);
		pop(&solarSystem);

		push(&solarSystem, intermediate);

			yRotationMatrix_ak = vmath::rotate((GLfloat)day_ak, 0.0f, 1.0f, 0.0f);
			translateMatrix_ak = vmath::translate(0.7f, 0.0f, 0.0f);

			mat4 last = peek(solarSystem) * yRotationMatrix_ak * translateMatrix_ak;

			yRotationMatrix_ak = vmath::rotate((GLfloat)hour_ak, 0.0f, 1.0f, 0.0f);
			scaleMatrix_ak = vmath::scale(0.3f, 0.3f, 0.3f);
			mat4 end = last * yRotationMatrix_ak * scaleMatrix_ak;
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

			modelViewProjectionMatrix_ak = perspectiveProjectMatrix_ak * end;
			glUniformMatrix4fv(mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix_ak);

			glUniform3fv(colorUniform_ak, 1, moonColor_ak);
			glBindVertexArray(Vao_sphere_ak);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Vbo_element_sphere_ak);
			glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
			glBindVertexArray(0);

			modelViewProjectionMatrix_ak = perspectiveProjectMatrix_ak * peek(solarSystem);
			glUniformMatrix4fv(mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix_ak);
		pop(&solarSystem);
	pop(&solarSystem);

	glUseProgram(0);

	SwapBuffers(ghdc_ak);
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	perspectiveProjectMatrix_ak = perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
}

void update()
{

}

void uninitialize(void)
{
	if (gbFullscreen_ak == true)
	{
		dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);
		SetWindowLong(ghwnd_ak, GWL_STYLE, dwStyle_ak | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
		SetWindowPos(ghwnd_ak, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	if (Vao_sphere_ak)
	{
		glDeleteVertexArrays(1, &Vao_sphere_ak);
		Vao_sphere_ak = 0;
	}

	if (Vbo_position_sphere_ak)
	{
		glDeleteVertexArrays(1, &Vbo_position_sphere_ak);
		Vbo_position_sphere_ak = 0;
	}

	// Safe Shader Release
	if (gShaderProgramObject_ak) {
		glUseProgram(gShaderProgramObject_ak);
		GLsizei shaderCount_ak;
		glGetProgramiv(gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

		GLuint* pShaders_ak = NULL;
		pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
		glGetAttachedShaders(gShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);

		for (GLsizei i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
			glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
			glDeleteShader(pShaders_ak[i_ak]);
			pShaders_ak[i_ak] = 0;
		}

		free(pShaders_ak);
		glDeleteProgram(gShaderProgramObject_ak);
		gShaderProgramObject_ak = 0;

		glUseProgram(0);
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc_ak);
	ghrc_ak = NULL;

	ReleaseDC(ghwnd_ak, ghdc_ak);
	ghdc_ak = NULL;

	if (gpFile_ak)
	{
		fprintf(gpFile_ak, "Log File Is Successfully Closed.\n");
		fclose(gpFile_ak);
		gpFile_ak = NULL;
	}
}
