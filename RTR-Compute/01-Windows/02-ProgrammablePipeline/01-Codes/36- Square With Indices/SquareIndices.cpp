#include <windows.h>
#include <stdio.h> 
#include "Icon.h"

#include <gl\glew.h>

#include <gl\GL.h>

#include "vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	ATTRIBUTE_POSITION = 0,
	ATTRIBUTE_COLOR,
	ATTRIBUTE_NORMAL,
	ATRIBUTE_TEXCOORD,
};

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gpFile_ak = NULL;
HWND ghwnd_ak = NULL;
DWORD dwStyle_ak = NULL;
WINDOWPLACEMENT wpPrev_ak = { sizeof(WINDOWPLACEMENT) };
bool gbFullScreen_ak = false;

HDC ghdc_ak = NULL;
HGLRC ghrc_ak = NULL;
bool gbActiveWindow_ak = false;

GLuint gVertexShaderObject_ak;
GLuint gFragmentShaderObject_ak;
GLuint gShaderProgramObject_ak;

GLuint vao_ak;
GLuint vbo_position_ak;
GLuint vbo_index_ak;
GLuint mvpMatrixUniform_ak;

mat4 perspectiveProjectionMatrix_ak;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    void Initialize(void);
    void Display(void);

    WNDCLASSEX wndclass_ak;
    HWND hwnd_ak;
    MSG msg_ak;
    TCHAR szAppName_ak[] = TEXT("MyApp");
    bool bDone_ak = false;

    if (fopen_s(&gpFile_ak, "log.txt", "w") != 0) {
        MessageBox(NULL, TEXT("Failure creating file"), TEXT("File IO failed"), MB_ICONSTOP | MB_OK);
        exit(0);
    }
    else {
        fprintf(gpFile_ak, "File successfully create and program started\n");
    }

    int iScreenWidth = GetSystemMetrics(SM_CXSCREEN);
    int iScreenHeight = GetSystemMetrics(SM_CYSCREEN);

    int iScreenCenterX = iScreenWidth / 2;
    int iScreenCenterY = iScreenHeight / 2;

    int iWindowCenterX = WIN_WIDTH / 2;
    int iWindowsCenterY = WIN_HEIGHT / 2;

    int iWindowX = iScreenCenterX - iWindowCenterX;
    int iWindowY = iScreenCenterY - iWindowsCenterY;

    wndclass_ak.cbSize = sizeof(WNDCLASSEX);
    wndclass_ak.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wndclass_ak.cbClsExtra = 0;
    wndclass_ak.cbWndExtra = 0;
    wndclass_ak.hInstance = hInstance;
    wndclass_ak.lpfnWndProc = WndProc;
    wndclass_ak.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));
    wndclass_ak.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass_ak.lpszClassName = szAppName_ak;
    wndclass_ak.lpszMenuName = NULL;
    wndclass_ak.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wndclass_ak.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));

    RegisterClassEx(&wndclass_ak);

    hwnd_ak = CreateWindowEx(
        WS_EX_APPWINDOW, szAppName_ak, TEXT("White square using indices"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, iWindowX, iWindowY, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);
    ghwnd_ak = hwnd_ak;

    Initialize();
    ShowWindow(hwnd_ak, iCmdShow);
    SetForegroundWindow(hwnd_ak);
    SetFocus(hwnd_ak);

    while (bDone_ak == false) {
        if (PeekMessage(&msg_ak, NULL, 0, 0, PM_REMOVE)) {
            if (msg_ak.message == WM_QUIT) {
                bDone_ak = true;
            }
            else {
                TranslateMessage(&msg_ak);
                DispatchMessage(&msg_ak);
            }
        }
        else {
            if (gbActiveWindow_ak == true) {
                Display();
            }
        }
    }

    return ((int)msg_ak.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
    void ToggleFullScreen(void);
    void Resize(int, int);
    void Uninitialize(void);

    switch (iMsg) {
    case WM_SETFOCUS: {
        gbActiveWindow_ak = true;
        break;
    }
    case WM_KILLFOCUS: {
        gbActiveWindow_ak = false;
        break;
    }
    case WM_ERASEBKGND: {
        return (0);
    }
    case WM_SIZE: {
        Resize(LOWORD(lParam), HIWORD(lParam));
        break;
    }
    case WM_KEYDOWN: {
        switch (wParam) {
        case VK_ESCAPE: {
            DestroyWindow(hwnd);
            break;
        }
        case 0x46:
        case 0x66: {
            ToggleFullScreen();
            break;
        }
        default: {
            break;
        }
        }
        break;
    }
    case WM_CLOSE: {
        DestroyWindow(hwnd);
        break;
    }
    case WM_DESTROY: {
        Uninitialize();
        PostQuitMessage(0);
        break;
    }
    default: {
        break;
    }
    }

    return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void) {
    MONITORINFO mi_ak = { sizeof(MONITORINFO) };

    if (gbFullScreen_ak == false) {
        dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);
        if ((dwStyle_ak & WS_OVERLAPPEDWINDOW)) {
            if ((GetWindowPlacement(ghwnd_ak, &wpPrev_ak) &&
                (GetMonitorInfo(MonitorFromWindow(ghwnd_ak, MONITORINFOF_PRIMARY), &mi_ak)))) {
                SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak & (~WS_OVERLAPPEDWINDOW)));
                SetWindowPos(ghwnd_ak,
                    HWND_TOP,
                    mi_ak.rcMonitor.left,
                    mi_ak.rcMonitor.top,
                    (mi_ak.rcMonitor.right - mi_ak.rcMonitor.left),
                    (mi_ak.rcMonitor.bottom - mi_ak.rcMonitor.top),
                    SWP_NOZORDER | SWP_FRAMECHANGED);
            }
        }
        ShowCursor(FALSE);
        gbFullScreen_ak = true;
    }
    else {
        SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak | WS_OVERLAPPEDWINDOW));
        SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
        SetWindowPos(ghwnd_ak,
            HWND_TOP,
            0,
            0,
            0,
            0,
            (SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOOWNERZORDER));
        ShowCursor(TRUE);
        gbFullScreen_ak = false;
    }
}

void Initialize(void) {
    void Resize(int, int);
    void Uninitialize(void);

    PIXELFORMATDESCRIPTOR pFD_ak;
    int iPixelFormatIndex_ak;

    ghdc_ak = GetDC(ghwnd_ak);

    ZeroMemory(&pFD_ak, sizeof(PIXELFORMATDESCRIPTOR));
    pFD_ak.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pFD_ak.nVersion = 1;
    pFD_ak.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pFD_ak.iPixelType = PFD_TYPE_RGBA;
    pFD_ak.cColorBits = 32;
    pFD_ak.cRedBits = 8;
    pFD_ak.cGreenBits = 8;
    pFD_ak.cBlueBits = 8;
    pFD_ak.cAlphaBits = 8;
    pFD_ak.cDepthBits = 32;

    iPixelFormatIndex_ak = ChoosePixelFormat(ghdc_ak, &pFD_ak);
    if (iPixelFormatIndex_ak == 0) {
        DestroyWindow(ghwnd_ak);
    }

    if (SetPixelFormat(ghdc_ak, iPixelFormatIndex_ak, &pFD_ak) == FALSE) {
        DestroyWindow(ghwnd_ak);
    }

    ghrc_ak = wglCreateContext(ghdc_ak);
    if (ghrc_ak == NULL) {
        DestroyWindow(ghwnd_ak);
    }

    if (wglMakeCurrent(ghdc_ak, ghrc_ak) == FALSE) {
        DestroyWindow(ghwnd_ak);
    }

    GLenum glew_error_ak = glewInit();
    if (glew_error_ak != GLEW_OK) {
        wglDeleteContext(ghrc_ak);
        ghrc_ak = NULL;
        ReleaseDC(ghwnd_ak, ghdc_ak);
        ghdc_ak = NULL;
    }

    fprintf(gpFile_ak, "\nOpenGL Vendor : %s \n", glGetString(GL_VENDOR));
    fprintf(gpFile_ak, "\nOpenGL Renderer : %s \n", glGetString(GL_RENDERER));
    fprintf(gpFile_ak, "\nOpenGL Version : %s \n", glGetString(GL_VERSION));
    fprintf(gpFile_ak, "\nOpenGL GLSL Version : %s \n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    GLint numExt_ak;

    glGetIntegerv(GL_NUM_EXTENSIONS, &numExt_ak);
    for (int i = 0; i < numExt_ak; i++) {
        fprintf(gpFile_ak, "\nOpenGL Enabled Extensions:%s\n", glGetStringi(GL_EXTENSIONS, i));
    }

    gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 460 core"
        "\n"
        "in vec4 vPosition;"
        "uniform mat4 u_mvp_matrix;"
        "void main(void)"
        "{"
        "gl_Position = u_mvp_matrix * vPosition;"
        "}";

    glShaderSource(gVertexShaderObject_ak, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(gVertexShaderObject_ak);

    GLint infoLogLength_ak = 0;
    GLint shaderCompileStatus_ak = 0;
    char* szBuffer_ak = NULL;
    glGetShaderiv(gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
        glGetShaderiv(gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetShaderInfoLog(gVertexShaderObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Vertex shader compilation log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                DestroyWindow(ghwnd_ak);
            }
        }
    }

    gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentSourceCode_ak =
        "#version 460 core"
        "\n"
        "out vec4 FragColor;"
        "void main(void)"
        "{"
        "FragColor = vec4(1.0, 1.0, 1.0, 1.0);"
        "}";

    glShaderSource(gFragmentShaderObject_ak, 1, (const GLchar**)&fragmentSourceCode_ak, NULL);
    glCompileShader(gFragmentShaderObject_ak);

    infoLogLength_ak = 0;
    shaderCompileStatus_ak = 0;
    szBuffer_ak = NULL;
    glGetShaderiv(gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
        glGetShaderiv(gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetShaderInfoLog(gFragmentShaderObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Fragment shader compilation log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                DestroyWindow(ghwnd_ak);
            }
        }
    }

    gShaderProgramObject_ak = glCreateProgram();
    glAttachShader(gShaderProgramObject_ak, gVertexShaderObject_ak);
    glAttachShader(gShaderProgramObject_ak, gFragmentShaderObject_ak);

    glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");

    glLinkProgram(gShaderProgramObject_ak);

    GLint shaderProgramLinkStatus = 0;
    glGetProgramiv(gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
    if (shaderProgramLinkStatus == GL_FALSE) {
        glGetProgramiv(gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetProgramInfoLog(gShaderProgramObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Shader program link log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                DestroyWindow(ghwnd_ak);
            }
        }
    }

    mvpMatrixUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_mvp_matrix");

    const GLfloat squareVertices_ak[] = {
        1.0f, 1.0f, 0.0f,
        -1.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 0.0f,
        1.0f, -1.0f, 0.0f };

    const GLuint squareIndices[] = {
        3, 1, 0,
        3, 1, 2 };

    glGenVertexArrays(1, &vao_ak);
    glBindVertexArray(vao_ak);

    glGenBuffers(1, &vbo_position_ak);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_ak);
    glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices_ak), squareVertices_ak, GL_STATIC_DRAW);

    glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(ATTRIBUTE_POSITION);

    glGenBuffers(1, &vbo_index_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_index_ak);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(squareIndices), squareIndices, GL_STATIC_DRAW);

    glBindVertexArray(0);

    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    perspectiveProjectionMatrix_ak = mat4::identity();

    Resize(WIN_WIDTH, WIN_HEIGHT);
}

void Resize(int width, int height) {
    if (height == 0) {
        height = 1;
    }
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    perspectiveProjectionMatrix_ak = vmath::perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
}

void Display(void) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(gShaderProgramObject_ak);

    mat4 modelViewMatrix_ak;
    mat4 modelViewProjectionMatrix_ak;
    mat4 translateMatrix_ak;

    modelViewMatrix_ak = mat4::identity();
    modelViewProjectionMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -3.0f);
    modelViewMatrix_ak = translateMatrix_ak;

    modelViewProjectionMatrix_ak = perspectiveProjectionMatrix_ak * modelViewMatrix_ak;
    glUniformMatrix4fv(mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix_ak);

    glBindVertexArray(vao_ak);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    glUseProgram(0);
    SwapBuffers(ghdc_ak);
}

void Uninitialize(void) {
    if (gbFullScreen_ak == true) {
        dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);

        SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak | WS_OVERLAPPEDWINDOW));
        SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
        SetWindowPos(ghwnd_ak,
            HWND_TOP,
            0,
            0,
            0,
            0,
            SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
        ShowCursor(TRUE);
    }

    if (vao_ak) {
        glDeleteVertexArrays(1, &vao_ak);
        vao_ak = 0;
    }

    if (vbo_position_ak) {
        glDeleteVertexArrays(1, &vbo_position_ak);
        vbo_position_ak = 0;
    }

    if (vbo_index_ak) {
        glDeleteVertexArrays(1, &vbo_index_ak);
        vbo_index_ak = 0;
    }

    if (gShaderProgramObject_ak) {
        glUseProgram(gShaderProgramObject_ak);
        GLsizei shaderCount_ak;
        glGetProgramiv(gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

        GLuint* pShaders_ak = NULL;
        pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
        glGetAttachedShaders(gShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);

        for (int i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
            glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
            glDeleteShader(pShaders_ak[i_ak]);
            pShaders_ak[i_ak] = 0;
        }

        free(pShaders_ak);
        glDeleteProgram(gShaderProgramObject_ak);
        gShaderProgramObject_ak = 0;

        glUseProgram(0);
    }

    if (wglGetCurrentContext() == ghrc_ak) {
        wglMakeCurrent(NULL, NULL);
    }

    if (ghrc_ak) {
        wglDeleteContext(ghrc_ak);
        ghrc_ak = NULL;
    }

    if (ghdc_ak) {
        ReleaseDC(ghwnd_ak, ghdc_ak);
        ghdc_ak = NULL;
    }

    if (gpFile_ak) {
        fprintf(gpFile_ak, "Program terminated successfully\n");
        fclose(gpFile_ak);
        gpFile_ak = NULL;
    }
}
	

