#define _USE_MATH_DEFINES
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <cmath>
#include "GeometryShapes.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "GLU32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gpFile_ak = NULL;

HWND ghwnd_ak = NULL;
DWORD dwStyle_ak = NULL;
WINDOWPLACEMENT wpPrev_ak = {sizeof(WINDOWPLACEMENT)};
bool gbFullScreen_ak = false;

HDC ghdc_ak = NULL;
HGLRC ghrc_ak = NULL;
bool gbActiveWindow_ak = false;


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void Initialize(void);
	void Display(void);

	WNDCLASSEX wndclass_ak;
	HWND hwnd_ak;
	MSG msg_ak;
	TCHAR szAppName_ak[] = TEXT("MyApp");
	bool bDone_ak = false;

	if (fopen_s(&gpFile_ak, "logs.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Cannot Open File"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile_ak, "Log File Starts Here ...");
	}

	int iScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	int iScreenHeight = GetSystemMetrics(SM_CYSCREEN);

	int iScreenCenterX = iScreenWidth / 2;
	int iScreenCenterY = iScreenHeight / 2;

	int iWindowCenterX = WIN_WIDTH / 2;
	int iWindowsCenterY = WIN_HEIGHT / 2;

	int iWindowX = iScreenCenterX - iWindowCenterX;
	int iWindowY = iScreenCenterY - iWindowsCenterY;

	wndclass_ak.cbSize = sizeof(WNDCLASSEX);
	wndclass_ak.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass_ak.cbClsExtra = 0;
	wndclass_ak.cbWndExtra = 0;
	wndclass_ak.hInstance = hInstance;
	wndclass_ak.lpfnWndProc = WndProc;
	wndclass_ak.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));
	wndclass_ak.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass_ak.lpszClassName = szAppName_ak;
	wndclass_ak.lpszMenuName = NULL;
	wndclass_ak.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass_ak.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));

	RegisterClassEx(&wndclass_ak);

	hwnd_ak = CreateWindowEx(WS_EX_APPWINDOW,
							 szAppName_ak,
							 TEXT("Geometry Shapes"),
							 WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
							 iWindowX,
							 iWindowY,
							 WIN_WIDTH,
							 WIN_HEIGHT,
							 NULL,
							 NULL,
							 hInstance,
							 NULL);
	ghwnd_ak = hwnd_ak;

	Initialize();
	ShowWindow(hwnd_ak, iCmdShow);
	SetForegroundWindow(hwnd_ak);
	SetFocus(hwnd_ak);

	while (bDone_ak == false)
	{
		if (PeekMessage(&msg_ak, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_ak.message == WM_QUIT)
			{
				bDone_ak = true;
			}
			else
			{
				TranslateMessage(&msg_ak);
				DispatchMessage(&msg_ak);
			}
		}
		else
		{
			if (gbActiveWindow_ak == true)
			{
				Display();
			}
		}
	}

	return ((int)msg_ak.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen(void);
	void Resize(int, int);
	void Uninitialize(void);

	switch (iMsg)
	{
	case WM_SETFOCUS:
	{
		gbActiveWindow_ak = true;
		break;
	}
	case WM_KILLFOCUS:
	{
		gbActiveWindow_ak = false;
		break;
	}
	case WM_ERASEBKGND:
	{
		return (0);
	}
	case WM_SIZE:
	{
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	}
	case WM_KEYDOWN:
	{
		switch (wParam)
		{
		case VK_ESCAPE:
		{
			DestroyWindow(hwnd);
			break;
		}
		case 0x46:
		case 0x66:
		{
			ToggleFullScreen();
			break;
		}
		default:
		{
			break;
		}
		}
		break;
	}
	case WM_CLOSE:
	{
		DestroyWindow(hwnd);
		break;
	}
	case WM_DESTROY:
	{
		Uninitialize();
		PostQuitMessage(0);
		break;
	}
	default:
	{
		break;
	}
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	MONITORINFO mi_ak = {sizeof(MONITORINFO)};

	if (gbFullScreen_ak == false)
	{
		dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);
		if ((dwStyle_ak & WS_OVERLAPPEDWINDOW))
		{
			if ((GetWindowPlacement(ghwnd_ak, &wpPrev_ak) && (GetMonitorInfo(MonitorFromWindow(ghwnd_ak, MONITORINFOF_PRIMARY), &mi_ak))))
			{
				SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak & (~WS_OVERLAPPEDWINDOW)));
				SetWindowPos(ghwnd_ak,
							 HWND_TOP,
							 mi_ak.rcMonitor.left,
							 mi_ak.rcMonitor.top,
							 (mi_ak.rcMonitor.right - mi_ak.rcMonitor.left),
							 (mi_ak.rcMonitor.bottom - mi_ak.rcMonitor.top),
							 SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen_ak = true;
	}
	else
	{
		SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
		SetWindowPos(ghwnd_ak,
					 HWND_TOP,
					 0,
					 0,
					 0,
					 0,
					 (SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOOWNERZORDER));
		ShowCursor(TRUE);
		gbFullScreen_ak = false;
	}
}

void Initialize(void)
{
	void Resize(int, int);

	PIXELFORMATDESCRIPTOR pFD_ak;
	int iPixelFormatIndex_ak;

	ghdc_ak = GetDC(ghwnd_ak);

	ZeroMemory(&pFD_ak, sizeof(PIXELFORMATDESCRIPTOR));
	pFD_ak.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pFD_ak.nVersion = 1;
	pFD_ak.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pFD_ak.iPixelType = PFD_TYPE_RGBA;
	pFD_ak.cColorBits = 32;
	pFD_ak.cRedBits = 8;
	pFD_ak.cGreenBits = 8;
	pFD_ak.cBlueBits = 8;
	pFD_ak.cAlphaBits = 8;

	iPixelFormatIndex_ak = ChoosePixelFormat(ghdc_ak, &pFD_ak);
	if (iPixelFormatIndex_ak == 0)
	{
		fprintf(gpFile_ak, "ChoosePixelFormat function failed");
		DestroyWindow(ghwnd_ak);
	}

	if (SetPixelFormat(ghdc_ak, iPixelFormatIndex_ak, &pFD_ak) == FALSE)
	{
		fprintf(gpFile_ak, "SetPixelFormat function failed");
		DestroyWindow(ghwnd_ak);
	}

	ghrc_ak = wglCreateContext(ghdc_ak);
	if (ghrc_ak == NULL)
	{
		fprintf(gpFile_ak, "wglCreateContext function failed");
		DestroyWindow(ghwnd_ak);
	}

	if (wglMakeCurrent(ghdc_ak, ghrc_ak) == FALSE)
	{
		fprintf(gpFile_ak, "wglMakeCurrent function failed");
		DestroyWindow(ghwnd_ak);
	}
	

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	Resize(WIN_WIDTH, WIN_HEIGHT);
}

void Resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
}

void Display(void)
{
	void DrawCircle(float);
	void Triangle(float);
	void Rectangle(float,float);
	void InCircle(float,float,float,float, float,float,float,float,float);
	void OuterCircle(float,float);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -1.3f);
	
	glBegin(GL_LINES);

	for (float y_axis_ak = 1.0f; y_axis_ak > -1.0f; y_axis_ak -= 0.05f)
	{

		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(y_axis_ak, 1.0f, 0.0f);
		glVertex3f(y_axis_ak, -1.f, 0.0f);
	}

	glEnd();

	glBegin(GL_LINES);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);

	glEnd();

	glBegin(GL_LINES);

	for (float x_axis_ak = 1.0f; x_axis_ak > -1.0f; x_axis_ak -= 0.05f)
	{

		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(1.0f, x_axis_ak, 0.0f);
		glVertex3f(-1.0f, x_axis_ak, 0.0f);
	}

	glEnd();

	glBegin(GL_LINES);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);

	glEnd();

	glTranslatef(0.0f, 0.0f, -1.0f);

	Triangle(0.5f);
	InCircle(0.0f, 0.5f, 0.0f,-0.5f, -0.5f, 0.0f,0.5f, -0.5f, 0.0f);
	Rectangle(1.0f,1.0f);
	OuterCircle(1.0f, 1.0f);

	SwapBuffers(ghdc_ak);
}

void Triangle(float value_ak)
{
	float count = -1;
	int noCount = 2000;
	GLfloat angle = 0;
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 0.0f);
	
	glVertex3f(0.0f, value_ak, 0.0f);
	glVertex3f(-value_ak, -value_ak, 0.0f);

	glVertex3f(-value_ak, -value_ak, 0.0f);
	glVertex3f(value_ak, -value_ak, 0.0f);

	glVertex3f(value_ak, -value_ak, 0.0f);
	glVertex3f(0.0f, value_ak, 0.0f);

	glEnd();
}

void InCircle(float x1_ak, float y1_ak, float z1_ak,float x2_ak, float y2_ak, float z2,float x3_ak, float y3_ak, float z3_ak)
{
	float count_ak = 0.0f;
	GLfloat angle_ak = 0.0f;

	float dist_a_b_ak = sqrt((x2_ak - x1_ak)*(x2_ak - x1_ak) + (y2_ak - y1_ak)*(y2_ak - y1_ak) + (z2 - z1_ak)*(z2 - z1_ak));
	float dist_b_c_ak = sqrt((x3_ak - x2_ak)*(x3_ak - x2_ak) + (y3_ak - y2_ak)*(y3_ak - y2_ak) + (z3_ak - z2)*(z3_ak - z2));
	float dist_c_a_ak = sqrt((x1_ak - x3_ak)*(x1_ak - x3_ak) + (y1_ak - y3_ak)*(y1_ak - y3_ak) + (z1_ak - z3_ak)*(z1_ak - z3_ak));

	float semiperimeter_ak = (dist_a_b_ak + dist_b_c_ak + dist_c_a_ak) / 2;
	float radius_ak = sqrt((semiperimeter_ak - dist_a_b_ak)*(semiperimeter_ak - dist_b_c_ak)*(semiperimeter_ak - dist_c_a_ak) / semiperimeter_ak);
	float Ox_ak = (x3_ak * dist_a_b_ak + x1_ak * dist_b_c_ak + x2_ak * dist_c_a_ak) / (semiperimeter_ak * 2);
	float Oy_ak = (y3_ak * dist_a_b_ak + y1_ak * dist_b_c_ak + y2_ak * dist_c_a_ak) / (semiperimeter_ak * 2);
	float Oz_ak = (z3_ak * dist_a_b_ak + z1_ak * dist_b_c_ak + z2 * dist_c_a_ak) / (semiperimeter_ak * 2);

	glBegin(GL_LINE_LOOP);
	glColor3f(1.0f,1.0f,0.0f);
	for (count_ak = 0; count_ak <= 2000; count_ak++) 
	{
		angle_ak = 2 * M_PI*count_ak / 2000;
		glVertex3f(cos(angle_ak)*radius_ak + Ox_ak, sin(angle_ak)*radius_ak + Oy_ak, 0.0f + Oz_ak);
	}

	glEnd();

}

void DrawCircle(float radius_ak) {
	float count_ak = 0.0f;
	GLfloat angle_ak = 0.0f;

	glBegin(GL_LINE_LOOP);
	glColor3f(1.0f,1.0f,0.0f);
	for (count_ak = 0; count_ak <= 2000; count_ak++) 
	{
		angle_ak = 2 * M_PI*count_ak / 2000;
		glVertex3f(cos(angle_ak)*radius_ak , sin(angle_ak)*radius_ak, 0.0f );
	}
	glEnd();
}

void Rectangle(float width_ak, float height_ak) 
{
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 0.0f);

	glVertex3f(width_ak/2, height_ak/2, 0.0f);
	glVertex3f(-width_ak / 2, height_ak / 2, 0.0f);
	glVertex3f(-width_ak / 2, height_ak / 2, 0.0f);
	glVertex3f(-width_ak / 2, -height_ak / 2, 0.0f);
	glVertex3f(-width_ak / 2, -height_ak / 2, 0.0f);
	glVertex3f(width_ak / 2, -height_ak / 2, 0.0f);
	glVertex3f(width_ak / 2, -height_ak / 2, 0.0f);
	glVertex3f(width_ak / 2, height_ak / 2, 0.0f);

	glEnd();
}

void OuterCircle(float width_ak, float height_ak) 
{
	float count_ak = 0.0f;
	GLfloat angle_ak = 0.0f;
	float radius_ak = sqrt(width_ak / 2 * width_ak / 2 + height_ak / 2 * height_ak / 2);

	glBegin(GL_LINE_LOOP);
	glColor3f(1.0f, 1.0f, 0.0f);
	for (count_ak = 0; count_ak <= 2000; count_ak++) {
		angle_ak = 2 * M_PI*count_ak / 2000;
		glVertex3f(cos(angle_ak)*radius_ak, sin(angle_ak)*radius_ak, 0.0f);
	}
	glEnd();
}

void Uninitialize(void)
{
	if (gbFullScreen_ak == true)
	{
		dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);

		SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
		SetWindowPos(ghwnd_ak,
					 HWND_TOP,
					 0,
					 0,
					 0,
					 0,
					 SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc_ak)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc_ak)
	{
		wglDeleteContext(ghrc_ak);
		ghrc_ak = NULL;
	}

	if (ghdc_ak)
	{
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	if (gpFile_ak)
	{
		fprintf(gpFile_ak, "Log File complete");
		fclose(gpFile_ak);
		gpFile_ak = NULL;
	}
}