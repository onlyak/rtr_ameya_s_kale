#include<windows.h>

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int iCmdShow)
{
    WNDCLASSEX wndClass;
    HWND hwnd;
    MSG msg;
    TCHAR szAppName[]=TEXT("MyApp");

    wndClass.cbSize=sizeof(WNDCLASSEX);
    wndClass.style=CS_HREDRAW | CS_VREDRAW;
    wndClass.cbWndExtra=0;
    wndClass.cbClsExtra=0;
    wndClass.lpfnWndProc=WndProc;
    wndClass.hInstance=hInstance;
    wndClass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
    wndClass.hCursor=LoadCursor(NULL,IDC_ARROW);
    wndClass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
    wndClass.lpszMenuName=NULL;
    wndClass.lpszClassName=szAppName;
    wndClass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);

    RegisterClassEx(&wndClass);

    hwnd=CreateWindow(szAppName,TEXT("Ameya's Window"),
                      WS_OVERLAPPEDWINDOW,
                      CW_USEDEFAULT,
                      CW_USEDEFAULT,
                      CW_USEDEFAULT,
                      CW_USEDEFAULT,
                      NULL,
                      NULL,
                      hInstance,
                      NULL);
    
    ShowWindow(hwnd,iCmdShow);
    UpdateWindow(hwnd);

    while(GetMessage(&msg,NULL,0,0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    switch(iMsg)
    {
        case WM_CREATE:
        MessageBox(hwnd,TEXT("WM_CREATE is received"),TEXT("Message"),MB_OK);
        break;

        case WM_LBUTTONDOWN:
        MessageBox(hwnd,TEXT("WM_LBUTTON is received"),TEXT("Message"),MB_OK);
        break;

        case WM_KEYDOWN:
        MessageBox(hwnd,TEXT("WM_KEYDOWN is received"),TEXT("Message"),MB_OK);
        break;

        case WM_KEYUP:
        MessageBox(hwnd,TEXT("WM_KEYUP is received"),TEXT("Message"),MB_OK);
        break;

        case WM_DESTROY:
        PostQuitMessage(0);
        break; 
    }

    return (DefWindowProc(hwnd,iMsg,wParam,lParam));
}