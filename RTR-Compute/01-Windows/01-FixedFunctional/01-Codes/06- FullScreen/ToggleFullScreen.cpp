#include<windows.h>

DWORD dwStyle;
WINDOWPLACEMENT wpPrev={sizeof(WINDOWPLACEMENT)};
bool gbFullScreen=false;
HWND ghwnd=NULL;

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
    WNDCLASSEX wndClass;
    HWND hwnd;
    MSG msg;
    TCHAR szAppName[]=TEXT("ToggleFullScreen");

    wndClass.cbSize=sizeof(WNDCLASSEX);
    wndClass.cbClsExtra=0;
    wndClass.cbWndExtra=0;
    wndClass.style=CS_HREDRAW | CS_VREDRAW;
    wndClass.lpfnWndProc=WndProc;
    wndClass.hInstance=hInstance;
    wndClass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
    wndClass.hIcon=LoadIcon(NULL,IDI_APPLICATION);
    wndClass.hCursor=LoadCursor(NULL,IDC_ARROW);
    wndClass.lpszMenuName=NULL;
	wndClass.lpszClassName=szAppName;
    wndClass.hIconSm=LoadIcon(NULL,IDI_APPLICATION);

    RegisterClassEx(&wndClass);

    hwnd=CreateWindow(szAppName,TEXT("Ameya's Window"),WS_OVERLAPPEDWINDOW,CW_USEDEFAULT,CW_USEDEFAULT,CW_USEDEFAULT,CW_USEDEFAULT,NULL,NULL,hInstance,NULL);

    ghwnd=hwnd;

    ShowWindow(hwnd,iCmdShow);
    UpdateWindow(hwnd);

    while(GetMessage(&msg,NULL,0,0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    void ToggleFullScreen(void);
    switch(iMsg)
    {
        case WM_KEYDOWN:
            switch(wParam)
            {
                case 0x46:
                case 0x66:
                    ToggleFullScreen();
                    break;
                default:
                    break;
            }
            break;
            
        case WM_DESTROY:
            PostQuitMessage(0);
			break;
    }

    return (DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullScreen(void)
{
    MONITORINFO mi = {sizeof(MONITORINFO)};

    if(gbFullScreen==false)
    {
        dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
        if(dwStyle & WS_OVERLAPPEDWINDOW)
        {
            if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi) )
            {
                SetWindowLong(ghwnd,GWL_STYLE,(dwStyle & (~WS_OVERLAPPEDWINDOW) ));

                SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_NOZORDER | SWP_FRAMECHANGED);
            }            
        }
        ShowCursor(FALSE);
        gbFullScreen=true;
    }
    else
    {
        SetWindowLong(ghwnd,GWL_STYLE,(dwStyle | WS_OVERLAPPEDWINDOW));
        SetWindowPlacement(ghwnd,&wpPrev);
        SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE|SWP_NOOWNERZORDER|SWP_NOZORDER|SWP_FRAMECHANGED);
        ShowCursor(TRUE);
        gbFullScreen=false;
    }
}