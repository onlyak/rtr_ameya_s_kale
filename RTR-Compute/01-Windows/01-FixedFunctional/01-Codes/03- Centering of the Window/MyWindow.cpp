#include<windows.h>

//global func declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void PosCenter(RECT *);

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	RECT rect = {0,0,800,600};
    PosCenter(&rect);


	//initialization
	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.lpfnWndProc = WndProc;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szAppName;
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register class WNDCLASSEX
	RegisterClassEx(&wndClass);

	//creating window
	hwnd = CreateWindow(szAppName,	
		TEXT("Ameya's Window"),
		WS_OVERLAPPEDWINDOW,
		rect.left,
        rect.top,
        rect.right,
        rect.bottom,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	//message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int)msg.wParam);
}

//definition
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	switch (iMsg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void PosCenter(RECT *rect)
{
    int screen_wd= GetSystemMetrics(SM_CXSCREEN);
    int screen_ht = GetSystemMetrics(SM_CYSCREEN);
    rect->left = (screen_wd - rect->right) / 2;
    rect->top = (screen_ht - rect->bottom) / 2;
}