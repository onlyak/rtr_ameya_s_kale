#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include "Lights.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "GLU32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gpFile_ak = NULL;

HWND ghwnd_ak = NULL;
DWORD dwStyle_ak = NULL;
WINDOWPLACEMENT wpPrev_ak = {sizeof(WINDOWPLACEMENT)};
bool gbFullScreen_ak = false;

HDC ghdc_ak = NULL;
HGLRC ghrc_ak = NULL;
bool gbActiveWindow_ak = false;
bool gbLight_ak= false;

GLfloat lightAmbient_ak[]={0.0f,0.0f,0.0f,1.0f}; 
GLfloat lightDiffuse_ak[]={1.0f,1.0f,1.0f,1.0f}; 
GLfloat lightSpecular_ak[] = {1.0f,0.0f,0.0f,1.0f };
GLfloat lightPosition_ak[]={0.0f,0.0f,0.0f,1.0f}; 

GLfloat light_model_ambient_ak[] = { 0.2f,0.2f,0.2f,1.0f };
GLfloat light_model_local_viewer_ak[] = { 0.0f };

GLfloat xRot_ak= 0.0f;
GLfloat yRot_ak = 0.0f;
GLfloat zRot_ak = 0.0f;
GLint keyPressed_ak= 0;

GLUquadric *quadric_ak[24];

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void Initialize(void);
	void Display(void);

	WNDCLASSEX wndclass_ak;
	HWND hwnd_ak;
	MSG msg_ak;
	TCHAR szAppName_ak[] = TEXT("MyApp");
	bool bDone_ak = false;

	if (fopen_s(&gpFile_ak, "logs.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Cannot Open File"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile_ak, "Log File Starts Here ...");
	}

	int iScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	int iScreenHeight = GetSystemMetrics(SM_CYSCREEN);

	int iScreenCenterX = iScreenWidth / 2;
	int iScreenCenterY = iScreenHeight / 2;

	int iWindowCenterX = WIN_WIDTH / 2;
	int iWindowsCenterY = WIN_HEIGHT / 2;

	int iWindowX = iScreenCenterX - iWindowCenterX;
	int iWindowY = iScreenCenterY - iWindowsCenterY;

	wndclass_ak.cbSize = sizeof(WNDCLASSEX);
	wndclass_ak.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass_ak.cbClsExtra = 0;
	wndclass_ak.cbWndExtra = 0;
	wndclass_ak.hInstance = hInstance;
	wndclass_ak.lpfnWndProc = WndProc;
	wndclass_ak.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));
	wndclass_ak.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass_ak.lpszClassName = szAppName_ak;
	wndclass_ak.lpszMenuName = NULL;
	wndclass_ak.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass_ak.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));

	RegisterClassEx(&wndclass_ak);

	hwnd_ak = CreateWindowEx(WS_EX_APPWINDOW,
							 szAppName_ak,
							 TEXT("24 Spheres"),
							 WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
							 iWindowX,
							 iWindowY,
							 WIN_WIDTH,
							 WIN_HEIGHT,
							 NULL,
							 NULL,
							 hInstance,
							 NULL);
	ghwnd_ak = hwnd_ak;

	Initialize();
	ShowWindow(hwnd_ak, iCmdShow);
	SetForegroundWindow(hwnd_ak);
	SetFocus(hwnd_ak);

	while (bDone_ak == false)
	{
		if (PeekMessage(&msg_ak, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_ak.message == WM_QUIT)
			{
				bDone_ak = true;
			}
			else
			{
				TranslateMessage(&msg_ak);
				DispatchMessage(&msg_ak);
			}
		}
		else
		{
			if (gbActiveWindow_ak == true)
			{
				Display();
			}
		}
	}

	return ((int)msg_ak.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen(void);
	void Resize(int, int);
	void Uninitialize(void);

	switch (iMsg)
	{
	case WM_SETFOCUS:
	{
		gbActiveWindow_ak = true;
		break;
	}
	case WM_KILLFOCUS:
	{
		gbActiveWindow_ak = false;
		break;
	}
	case WM_ERASEBKGND:
	{
		return (0);
	}
	case WM_SIZE:
	{
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	}
	case WM_KEYDOWN:
	{
		switch (wParam)
		{
		case VK_ESCAPE:
		{
			DestroyWindow(hwnd);
			break;
		}
		case 0x46:
		case 0x66:
		{
			ToggleFullScreen();
			break;
		}

		case 'L':
		case 'l':
		if(gbLight_ak==false)
		{
			gbLight_ak=true;
			glEnable(GL_LIGHTING);
		}
		else
		{
			gbLight_ak=false;
			glDisable(GL_LIGHTING);
		}
		break;
		case 'X':
		case 'x':
		{
			keyPressed_ak= 1;
			xRot_ak += 0.0f;
		}
		break;

		case 'Y':
		case 'y':
		{
			keyPressed_ak= 2;
			yRot_ak = 0.0f;
		}
		break;
		case 'Z':
		case 'z':
		{
			keyPressed_ak= 3;
			zRot_ak= 0.0f;
		}
		break;
		default:
		{
			break;
		}
		}
		break;
	}
	case WM_CLOSE:
	{
		DestroyWindow(hwnd);
		break;
	}
	case WM_DESTROY:
	{
		Uninitialize();
		PostQuitMessage(0);
		break;
	}
	default:
	{
		break;
	}
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	MONITORINFO mi_ak = {sizeof(MONITORINFO)};

	if (gbFullScreen_ak == false)
	{
		dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);
		if ((dwStyle_ak & WS_OVERLAPPEDWINDOW))
		{
			if ((GetWindowPlacement(ghwnd_ak, &wpPrev_ak) && (GetMonitorInfo(MonitorFromWindow(ghwnd_ak, MONITORINFOF_PRIMARY), &mi_ak))))
			{
				SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak & (~WS_OVERLAPPEDWINDOW)));
				SetWindowPos(ghwnd_ak,
							 HWND_TOP,
							 mi_ak.rcMonitor.left,
							 mi_ak.rcMonitor.top,
							 (mi_ak.rcMonitor.right - mi_ak.rcMonitor.left),
							 (mi_ak.rcMonitor.bottom - mi_ak.rcMonitor.top),
							 SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen_ak = true;
	}
	else
	{
		SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
		SetWindowPos(ghwnd_ak,
					 HWND_TOP,
					 0,
					 0,
					 0,
					 0,
					 (SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOOWNERZORDER));
		ShowCursor(TRUE);
		gbFullScreen_ak = false;
	}
}

void Initialize(void)
{
	void Resize(int, int);

	PIXELFORMATDESCRIPTOR pFD_ak;
	int iPixelFormatIndex_ak;

	ghdc_ak = GetDC(ghwnd_ak);

	ZeroMemory(&pFD_ak, sizeof(PIXELFORMATDESCRIPTOR));
	pFD_ak.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pFD_ak.nVersion = 1;
	pFD_ak.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pFD_ak.iPixelType = PFD_TYPE_RGBA;
	pFD_ak.cColorBits = 32;
	pFD_ak.cRedBits = 8;
	pFD_ak.cGreenBits = 8;
	pFD_ak.cBlueBits = 8;
	pFD_ak.cAlphaBits = 8;
	pFD_ak.cDepthBits = 32;

	iPixelFormatIndex_ak = ChoosePixelFormat(ghdc_ak, &pFD_ak);
	if (iPixelFormatIndex_ak == 0)
	{
		fprintf(gpFile_ak, "ChoosePixelFormat function failed");
		DestroyWindow(ghwnd_ak);
	}

	if (SetPixelFormat(ghdc_ak, iPixelFormatIndex_ak, &pFD_ak) == FALSE)
	{
		fprintf(gpFile_ak, "SetPixelFormat function failed");
		DestroyWindow(ghwnd_ak);
	}

	ghrc_ak = wglCreateContext(ghdc_ak);
	if (ghrc_ak == NULL)
	{
		fprintf(gpFile_ak, "wglCreateContext function failed");
		DestroyWindow(ghwnd_ak);
	}

	if (wglMakeCurrent(ghdc_ak, ghrc_ak) == FALSE)
	{
		fprintf(gpFile_ak, "wglMakeCurrent function failed");
		DestroyWindow(ghwnd_ak);
	}
	

	glShadeModel(GL_SMOOTH);	
	glClearDepth(1.0f);				
	glEnable(GL_DEPTH_TEST);			
	glDepthFunc(GL_LEQUAL);	
	glHint(GL_PERSPECTIVE_CORRECTION_HINT , GL_NICEST);

	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);

	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient_ak);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse_ak);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition_ak);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_model_ambient_ak);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, light_model_local_viewer_ak);
	glEnable(GL_LIGHT0);

	for (int i = 0; i < 24; i++)
	{
		quadric_ak[i] = gluNewQuadric();
	}
	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
	Resize(WIN_WIDTH, WIN_HEIGHT);
}

void Resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//gluPerspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
	//Ortho
	if (width <= height)
	{
		glOrtho(0.0f,		
			15.5f,			
			0.0f,			
			(15.5f*(GLfloat)height / (GLfloat)width),
			-10.0f,			
			10.0f);			
	}
	else
	{
		glOrtho(0.0f,
			(15.5f*(GLfloat)width / (GLfloat)height),
			0.0f,
			15.5f,
			-10.0f,
			10.0f);
	}
}

void Display(void)
{
	void Spheres24();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if (keyPressed_ak == 1)
	{
		glRotatef(xRot_ak, 1.0f, 0.0f, 0.0f);
		lightPosition_ak[1] = xRot_ak;
	}
	else if (keyPressed_ak == 2)
	{
		glRotatef(yRot_ak, 0.0f, 1.0f, 0.0f);
		lightPosition_ak[2] = yRot_ak;
	}
	else if (keyPressed_ak == 3)
	{
		glRotatef(zRot_ak, 0.0f, 0.0f, 1.0f);
		lightPosition_ak[0] = zRot_ak;
	}

	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition_ak);
	Spheres24();
	SwapBuffers(ghdc_ak);

	xRot_ak+=1.0f;
	yRot_ak+=1.0f;
	zRot_ak+=1.0f;
}

void Spheres24()
{	
	GLfloat materialAmbient_ak[4];
	GLfloat materialDiffuse_ak[4];
	GLfloat materialSpecular_ak[4];
	GLfloat materialShininess_ak[1];

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	//sphere 1
	materialAmbient_ak[0] = 0.0215;	
	materialAmbient_ak[1] = 0.1745;	
	materialAmbient_ak[2] = 0.0215;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.07568;	
	materialDiffuse_ak[1] = 0.61424;	
	materialDiffuse_ak[2] = 0.07568;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.633;	
	materialSpecular_ak[1] = 0.727811;	
	materialSpecular_ak[2] = 0.633;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.6 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f, 14.0f, 0.0f);	
	gluSphere(quadric_ak[0], 1.0f, 30, 30);

	//sphere 2
	materialAmbient_ak[0] = 0.135;	
	materialAmbient_ak[1] = 0.2225;	
	materialAmbient_ak[2] = 0.1575;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.54;	
	materialDiffuse_ak[1] = 0.89;	
	materialDiffuse_ak[2] = 0.63;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.316228;	
	materialSpecular_ak[1] = 0.316228;	
	materialSpecular_ak[2] = 0.316228;	 
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.1 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f, 11.5f, 0.0f);	
	gluSphere(quadric_ak[1], 1.0f, 30, 30);

	//sphere 3
	materialAmbient_ak[0] = 0.05375;	
	materialAmbient_ak[1] = 0.05;	
	materialAmbient_ak[2] = 0.06625;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.18275;	
	materialDiffuse_ak[1] = 0.17;	
	materialDiffuse_ak[2] = 0.22525;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.332741;	
	materialSpecular_ak[1] = 0.328634;	
	materialSpecular_ak[2] = 0.346435;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.3 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f, 9.0f, 0.0f);	
	gluSphere(quadric_ak[2], 1.0f, 30, 30);

	//sphere 4
	materialAmbient_ak[0] = 0.25;	
	materialAmbient_ak[1] = 0.20725;	
	materialAmbient_ak[2] = 0.20725;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 1.0;	
	materialDiffuse_ak[1] = 0.829;	
	materialDiffuse_ak[2] = 0.829;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.296648;	
	materialSpecular_ak[1] = 0.296648;	
	materialSpecular_ak[2] = 0.296648;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.088 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f, 6.5f, 0.0f);	
	gluSphere(quadric_ak[3], 1.0f, 30, 30);

	//sphere 5
	materialAmbient_ak[0] = 0.1745;	
	materialAmbient_ak[1] = 0.01175;	
	materialAmbient_ak[2] = 0.01175;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.61424;	
	materialDiffuse_ak[1] = 0.04136;	
	materialDiffuse_ak[2] = 0.04136;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.727811;	
	materialSpecular_ak[1] = 0.626959;	
	materialSpecular_ak[2] = 0.626959;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.6 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f, 4.0f, 0.0f);	
	gluSphere(quadric_ak[4], 1.0f, 30, 30);

	//sphere 6
	materialAmbient_ak[0] = 0.1;		
	materialAmbient_ak[1] = 0.18725;	
	materialAmbient_ak[2] = 0.1745;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.396;	
	materialDiffuse_ak[1] = 0.74151;	
	materialDiffuse_ak[2] = 0.69102;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.297254;	
	materialSpecular_ak[1] = 0.30829;	
	materialSpecular_ak[2] = 0.306678;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.6 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f, 1.5f, 0.0f);	
	gluSphere(quadric_ak[5], 1.0f, 30, 30);
	
	//sphere 7
	materialAmbient_ak[0] = 0.329412;	
	materialAmbient_ak[1] = 0.223529;	
	materialAmbient_ak[2] = 0.027451;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.780392;	
	materialDiffuse_ak[1] = 0.568627;	
	materialDiffuse_ak[2] = 0.113725;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.992157;	
	materialSpecular_ak[1] = 0.941176;	
	materialSpecular_ak[2] = 0.807843;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.21797842 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.0f, 14.0f, 0.0f);	
	gluSphere(quadric_ak[6], 1.0f, 30, 30);

	//sphere 8
	materialAmbient_ak[0] = 0.2125;	
	materialAmbient_ak[1] = 0.1275;	
	materialAmbient_ak[2] = 0.054;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.714;	
	materialDiffuse_ak[1] = 0.4284;	
	materialDiffuse_ak[2] = 0.18144;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.393548;	
	materialSpecular_ak[1] = 0.271906;	
	materialSpecular_ak[2] = 0.166721;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.2 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.0f, 11.5f, 0.0f);	
	gluSphere(quadric_ak[7], 1.0f, 30, 30);

	//sphere 9
	materialAmbient_ak[0] = 0.25;	
	materialAmbient_ak[1] = 0.25;	
	materialAmbient_ak[2] = 0.25;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.4;	
	materialDiffuse_ak[1] = 0.4;	
	materialDiffuse_ak[2] = 0.4;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.774597;	
	materialSpecular_ak[1] = 0.774597;	
	materialSpecular_ak[2] = 0.774597;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.6 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.0f, 9.0f, 0.0f);	
	gluSphere(quadric_ak[8], 1.0f, 30, 30);

	//sphere 10
	materialAmbient_ak[0] = 0.19125;	
	materialAmbient_ak[1] = 0.0735;	
	materialAmbient_ak[2] = 0.0225;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.7038;	
	materialDiffuse_ak[1] = 0.27048;	
	materialDiffuse_ak[2] = 0.0828;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.256777;	
	materialSpecular_ak[1] = 0.137622;	
	materialSpecular_ak[2] = 0.086014;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.1 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.0f, 6.5f, 0.0f);	
	gluSphere(quadric_ak[9], 1.0f, 30, 30);

	//sphere 11
	materialAmbient_ak[0] = 0.24725;	
	materialAmbient_ak[1] = 0.1995;	
	materialAmbient_ak[2] = 0.0745;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.75164;	
	materialDiffuse_ak[1] = 0.60648;	
	materialDiffuse_ak[2] = 0.22648;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.628281;	
	materialSpecular_ak[1] = 0.555802;	
	materialSpecular_ak[2] = 0.366065;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.4 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.0f, 4.0f, 0.0f);	
	gluSphere(quadric_ak[10], 1.0f, 30, 30);

	//sphere 12
	materialAmbient_ak[0] = 0.19225;		
	materialAmbient_ak[1] = 0.19225;	
	materialAmbient_ak[2] = 0.19225;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.50754;	
	materialDiffuse_ak[1] = 0.50754;	
	materialDiffuse_ak[2] = 0.50754;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.508273;	
	materialSpecular_ak[1] = 0.508273;	
	materialSpecular_ak[2] = 0.508273;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.4 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.0f, 1.5f, 0.0f);	
	gluSphere(quadric_ak[11], 1.0f, 30, 30);

	//sphere 13
	materialAmbient_ak[0] = 0.0;	
	materialAmbient_ak[1] = 0.0;	
	materialAmbient_ak[2] = 0.0;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.01;	
	materialDiffuse_ak[1] = 0.01;	
	materialDiffuse_ak[2] = 0.01;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.50;	
	materialSpecular_ak[1] = 0.50;	
	materialSpecular_ak[2] = 0.50;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.5f, 14.0f, 0.0f);	
	gluSphere(quadric_ak[12], 1.0f, 30, 30);

	//sphere 14
	materialAmbient_ak[0] = 0.0;	
	materialAmbient_ak[1] = 0.1;	
	materialAmbient_ak[2] = 0.06;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.0;	
	materialDiffuse_ak[1] = 0.50980392;	
	materialDiffuse_ak[2] = 0.50980392;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.50196078;	
	materialSpecular_ak[1] = 0.50196078;	
	materialSpecular_ak[2] = 0.50196078;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.5f, 11.5f, 0.0f);	
	gluSphere(quadric_ak[13], 1.0f, 30, 30);

	//sphere 15
	materialAmbient_ak[0] = 0.0;	
	materialAmbient_ak[1] = 0.0;	
	materialAmbient_ak[2] = 0.0;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.1;	
	materialDiffuse_ak[1] = 0.35;	
	materialDiffuse_ak[2] = 0.1;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.45;	
	materialSpecular_ak[1] = 0.55;	
	materialSpecular_ak[2] = 0.45;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.5f, 9.0f, 0.0f);	
	gluSphere(quadric_ak[14], 1.0f, 30, 30);

	//sphere 16
	materialAmbient_ak[0] = 0.0;	
	materialAmbient_ak[1] = 0.0;	
	materialAmbient_ak[2] = 0.0;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.5;	
	materialDiffuse_ak[1] = 0.0;	
	materialDiffuse_ak[2] = 0.0;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.7;	
	materialSpecular_ak[1] = 0.6;	
	materialSpecular_ak[2] = 0.6;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.5f, 6.5f, 0.0f);	
	gluSphere(quadric_ak[15], 1.0f, 30, 30);

	//sphere 17
	materialAmbient_ak[0] = 0.0;	
	materialAmbient_ak[1] = 0.0;	
	materialAmbient_ak[2] = 0.0;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.55;	
	materialDiffuse_ak[1] = 0.55;	
	materialDiffuse_ak[2] = 0.55;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.70;	
	materialSpecular_ak[1] = 0.70;	
	materialSpecular_ak[2] = 0.70;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.5f, 4.0f, 0.0f);	
	gluSphere(quadric_ak[16], 1.0f, 30, 30);

	//sphere 18
	materialAmbient_ak[0] = 0.0;		
	materialAmbient_ak[1] = 0.0;	
	materialAmbient_ak[2] = 0.0;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.5;	
	materialDiffuse_ak[1] = 0.5;	
	materialDiffuse_ak[2] = 0.0;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.60;	
	materialSpecular_ak[1] = 0.60;	
	materialSpecular_ak[2] = 0.50;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.5f, 1.5f, 0.0f);	
	gluSphere(quadric_ak[17], 1.0f, 30, 30);

	//sphere 19
	materialAmbient_ak[0] = 0.02;	
	materialAmbient_ak[1] = 0.02;	
	materialAmbient_ak[2] = 0.02;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.01;	
	materialDiffuse_ak[1] = 0.01;	
	materialDiffuse_ak[2] = 0.01;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.4;	
	materialSpecular_ak[1] = 0.4;	
	materialSpecular_ak[2] = 0.4;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(24.0f, 14.0f, 0.0f);	
	gluSphere(quadric_ak[18], 1.0f, 30, 30);

	//sphere 20
	materialAmbient_ak[0] = 0.0;	
	materialAmbient_ak[1] = 0.05;	
	materialAmbient_ak[2] = 0.05;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.4;	
	materialDiffuse_ak[1] = 0.5;	
	materialDiffuse_ak[2] = 0.5;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.04;	
	materialSpecular_ak[1] = 0.7;	
	materialSpecular_ak[2] = 0.7;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(24.0f, 11.5f, 0.0f);	
	gluSphere(quadric_ak[19], 1.0f, 30, 30);

	//sphere 21
	materialAmbient_ak[0] = 0.0;	
	materialAmbient_ak[1] = 0.05;	
	materialAmbient_ak[2] = 0.0;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.4;	
	materialDiffuse_ak[1] = 0.5;	
	materialDiffuse_ak[2] = 0.4;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.04;	
	materialSpecular_ak[1] = 0.7;	
	materialSpecular_ak[2] = 0.04;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(24.0f, 9.0f, 0.0f);	
	gluSphere(quadric_ak[20], 1.0f, 30, 30);

	//sphere 22
	materialAmbient_ak[0] = 0.05;	
	materialAmbient_ak[1] = 0.0;	
	materialAmbient_ak[2] = 0.0;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.5;	
	materialDiffuse_ak[1] = 0.4;	
	materialDiffuse_ak[2] = 0.4;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.7;	
	materialSpecular_ak[1] = 0.04;	
	materialSpecular_ak[2] = 0.04;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(24.0f, 6.5f, 0.0f);	
	gluSphere(quadric_ak[21], 1.0f, 30, 30);

	//sphere 23
	materialAmbient_ak[0] = 0.05;	
	materialAmbient_ak[1] = 0.05;	
	materialAmbient_ak[2] = 0.05;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.5;	
	materialDiffuse_ak[1] = 0.5;	
	materialDiffuse_ak[2] = 0.5;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.7;	
	materialSpecular_ak[1] = 0.7;	
	materialSpecular_ak[2] = 0.7;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(24.0f, 4.0f, 0.0f);	
	gluSphere(quadric_ak[22], 1.0f, 30, 30);

	//sphere 24
	materialAmbient_ak[0] = 0.05;		
	materialAmbient_ak[1] = 0.05;	
	materialAmbient_ak[2] = 0.0;	
	materialAmbient_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient_ak);

	materialDiffuse_ak[0] = 0.5;	
	materialDiffuse_ak[1] = 0.5;	
	materialDiffuse_ak[2] = 0.4;	
	materialDiffuse_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse_ak);

	materialSpecular_ak[0] = 0.7;	
	materialSpecular_ak[1] = 0.7;	
	materialSpecular_ak[2] = 0.04;	
	materialSpecular_ak[3] = 1.0f;		
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular_ak);

	materialShininess_ak[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess_ak);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(24.0f, 1.5f, 0.0f);	
	gluSphere(quadric_ak[23], 1.0f, 30, 30);

}

void Uninitialize(void)
{
	if (gbFullScreen_ak == true)
	{
		dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);

		SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
		SetWindowPos(ghwnd_ak,
					 HWND_TOP,
					 0,
					 0,
					 0,
					 0,
					 SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc_ak)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc_ak)
	{
		wglDeleteContext(ghrc_ak);
		ghrc_ak = NULL;
	}

	if (ghdc_ak)
	{
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	if (gpFile_ak)
	{
		fprintf(gpFile_ak, "Log File complete");
		fclose(gpFile_ak);
		gpFile_ak = NULL;
	}

	for (int i = 0; i < 24; i++)
	{
		if(quadric_ak[i])
			gluDeleteQuadric(quadric_ak[i]);
	}
}