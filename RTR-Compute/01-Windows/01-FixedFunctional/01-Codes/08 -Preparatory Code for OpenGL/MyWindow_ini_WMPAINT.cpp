#include<windows.h>
#include<stdio.h>
#include"MyWindow.h"
#include<GL/GL.h>

#pragma comment(lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

DWORD ak_dwStyle;
WINDOWPLACEMENT wpPrev={sizeof(WINDOWPLACEMENT)};
bool ak_gbFullScreen=false;
bool ak_gbActiveWindow=false;
HWND ak_ghwnd=NULL;
HDC ak_ghdc=NULL;
HGLRC ak_ghrc=NULL;
FILE *ak_gpFile = NULL;

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdLine, int iCmdShow)
{
    WNDCLASSEX ak_wndClass;
    HWND ak_hwnd;
    MSG ak_msg;
    TCHAR ak_szAppName[]=TEXT("Prep Template");
    bool ak_bDone=false;

    if (fopen_s(&ak_gpFile, "log.txt", "w"))
	{
		MessageBox(NULL, TEXT("Cannot Open File"), TEXT("Error"), MB_OK);
	}

    void Uninitialize(void);
    void Initialize(void);
    void display(void);
    void PosCenter(RECT *);

    ak_wndClass.cbSize=sizeof(WNDCLASSEX);
    ak_wndClass.cbClsExtra=0;
    ak_wndClass.cbWndExtra=0;
    ak_wndClass.style=CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    ak_wndClass.lpfnWndProc=WndProc;
    ak_wndClass.hInstance=hInstance;
    ak_wndClass.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
    ak_wndClass.hIcon=LoadIcon(hInstance,MAKEINTRESOURCE(MY_ICON));
    ak_wndClass.hCursor=LoadCursor(NULL,IDC_ARROW);
    ak_wndClass.lpszClassName=ak_szAppName;
    ak_wndClass.lpszMenuName=NULL;
    ak_wndClass.hIconSm=LoadIcon(hInstance,MAKEINTRESOURCE(MY_ICON));

    RegisterClassEx(&ak_wndClass);

    int screen_wd= GetSystemMetrics(SM_CXSCREEN);
    int screen_ht = GetSystemMetrics(SM_CYSCREEN);

    int centered_wd=screen_wd/2-(800/2);
    int centered_ht=screen_ht/2-(600/2);

    ak_hwnd=CreateWindowEx(WS_EX_APPWINDOW,ak_szAppName,TEXT("Ameya's Window"),WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_VISIBLE,centered_wd,centered_ht,WIN_WIDTH,WIN_HEIGHT,NULL,NULL,hInstance,NULL);
    ak_ghwnd=ak_hwnd;
    Initialize();

    ShowWindow(ak_hwnd,iCmdShow);
    SetForegroundWindow(ak_hwnd);
    SetFocus(ak_hwnd);

    while(ak_bDone==false)
    {
        if(PeekMessage(&ak_msg,NULL,0,0,PM_REMOVE))
        {
            if(ak_msg.message==WM_QUIT)
            {
                ak_bDone=true;
            }
            else
            {
                TranslateMessage(&ak_msg);
                DispatchMessage(&ak_msg);
            }
        }
        else
        {
            if(ak_gbActiveWindow==true)
            {
                //here you should call update function for OpenGL rendering

                //here you should call display function for OpenGL rendering
                //display();
            }
        }
    }
    return ((int)ak_msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
    void display(void);
    void ToggleFullScreen(void);
    void Resize(int,int);
	void Uninitialize(void);

    switch(iMsg)
    {
        case WM_KEYDOWN:
            switch(wParam)
            {
                case 0x46:
                case 0x66:
                    ToggleFullScreen();
                break;

                case VK_ESCAPE:
                    DestroyWindow(hwnd);
                break;

                default:
                    break;
            }
        break;

        case WM_SETFOCUS:
            ak_gbActiveWindow=true;
        break;
		
		case WM_PAINT:
			display();
		break;

        case WM_KILLFOCUS:
            ak_gbActiveWindow=false;
        break;

        case WM_ERASEBKGND:
            return 0;

        case WM_SIZE:
            Resize(LOWORD(lParam),HIWORD(wParam));
        break;

        case WM_CLOSE:
            DestroyWindow(hwnd);
        break;

        case WM_DESTROY:
            Uninitialize();
            PostQuitMessage(0);
        break;    
    }

    return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
    MONITORINFO ak_mi = {sizeof(MONITORINFO)};
    if(ak_gbFullScreen==false)
    {
        ak_dwStyle=GetWindowLong(ak_ghwnd,GWL_STYLE);
        if(ak_dwStyle & WS_OVERLAPPEDWINDOW)
        {
            if(GetWindowPlacement(ak_ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ak_ghwnd,MONITORINFOF_PRIMARY),&ak_mi) )
            {
                SetWindowLong(ak_ghwnd,GWL_STYLE,(ak_dwStyle & (~WS_OVERLAPPEDWINDOW) ));
                SetWindowPos(ak_ghwnd,HWND_TOP,ak_mi.rcMonitor.left,ak_mi.rcMonitor.top,ak_mi.rcMonitor.right-ak_mi.rcMonitor.left,ak_mi.rcMonitor.bottom-ak_mi.rcMonitor.top,SWP_NOZORDER | SWP_FRAMECHANGED);
            }            
        }
        ShowCursor(FALSE);
        ak_gbFullScreen=true;
    }
    else
    {
        SetWindowLong(ak_ghwnd,GWL_STYLE,(ak_dwStyle | WS_OVERLAPPEDWINDOW));
        SetWindowPlacement(ak_ghwnd,&wpPrev);
        SetWindowPos(ak_ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE|SWP_NOOWNERZORDER|SWP_NOZORDER|SWP_FRAMECHANGED);
        ShowCursor(TRUE);
        ak_gbFullScreen=false;
    }
}

void Initialize()
{
    void Resize(int,int);
    Resize(WIN_WIDTH,WIN_HEIGHT);

    PIXELFORMATDESCRIPTOR ak_pfd;
    int ak_iPixelFormatIndex;

    ak_ghdc=GetDC(ak_ghwnd);

    ZeroMemory(&ak_pfd,sizeof(PIXELFORMATDESCRIPTOR));

    ak_pfd.nSize=sizeof(ak_pfd);
    ak_pfd.nVersion=1;
    ak_pfd.dwFlags=PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL;
    ak_pfd.cColorBits=32;
    ak_pfd.cRedBits=8;
    ak_pfd.cGreenBits=8;
    ak_pfd.cBlueBits=8;
    ak_pfd.cAlphaBits=8;

    ak_iPixelFormatIndex=ChoosePixelFormat(ak_ghdc,&ak_pfd);
    if(ak_iPixelFormatIndex==0)
    {
        fprintf(ak_gpFile,"ChoosePixelFormat function failed");
        DestroyWindow(ak_ghwnd);
    }

    if(SetPixelFormat(ak_ghdc,ak_iPixelFormatIndex,&ak_pfd)==FALSE)
    {
        fprintf(ak_gpFile,"SetPixelFormat function failed");
        DestroyWindow(ak_ghwnd);
    }

    ak_ghrc=wglCreateContext(ak_ghdc);
    if(ak_ghdc==NULL)
    {
        fprintf(ak_gpFile,"wglCreateContext function failed");
        DestroyWindow(ak_ghwnd);
    }

    if(wglMakeCurrent(ak_ghdc,ak_ghrc)==FALSE)
    {
        fprintf(ak_gpFile,"wglMakeCurrent function failed");
        DestroyWindow(ak_ghwnd);
    }

    //set Clear Color
    glClearColor(0.0f,0.0f,1.0f,1.0f);

    Resize(WIN_WIDTH,WIN_HEIGHT);
}

void Resize(int width, int height)
{
    if(height==0)
        height=1;
    
    glViewport(0,0,GLsizei(width),GLsizei(height));
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glFlush();
}

void Uninitialize()
{
    if(ak_gbFullScreen==true)
    {
        ak_dwStyle=GetWindowLong(ak_ghwnd,GWL_STYLE);
        SetWindowLong(ak_ghwnd,GWL_STYLE,(ak_dwStyle | WS_OVERLAPPEDWINDOW));
        SetWindowPlacement(ak_ghwnd,&wpPrev);
        SetWindowPos(ak_ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE|SWP_NOOWNERZORDER|SWP_NOZORDER|SWP_FRAMECHANGED);
        ShowCursor(TRUE);
    }

    if(wglGetCurrentContext()==ak_ghrc)
    {
        wglMakeCurrent(NULL,NULL);
    }

    if(ak_ghrc)
    {
        wglDeleteContext(ak_ghrc);
        ak_ghrc=NULL;
    }

    if(ak_ghdc)
    {
        ReleaseDC(ak_ghwnd,ak_ghdc);
        ak_ghdc=NULL;
    }

    if(ak_gpFile)
    {
        fprintf(ak_gpFile,"Log File complete");
        fclose(ak_gpFile);
        ak_gpFile=NULL;
    }
}