#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <math.h>
#include <windowsx.h>
#include "DynamicIndia.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib, "Winmm.lib")
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "GLU32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gpFile_ak = NULL;

HWND ghwnd_ak = NULL;
DWORD dwStyle_ak = NULL;
WINDOWPLACEMENT wpPrev_ak = {sizeof(WINDOWPLACEMENT)};
bool gbFullScreen_ak = false;

HDC ghdc_ak = NULL;
HGLRC ghrc_ak = NULL;
bool gbActiveWindow_ak = false;

float PI = 3.14f;
float cd_1 = 0.0f;
float cd_2 = 0.0f;

int a = 0;
float ab = PI;

float x_1 = -4.5f;
float y_1 = 0.0f;
float x_2 = -4.5f;
float y_2 = -4.5f;

float iaf_x = -4.5f;
float iaf_y = 0.0f;

float x_3 = -6.6f;

float x_4 = 0.0f;
float y_4 = 0.0f;

float x_5 = 0.0f;
float y_5 = 0.0f;

float ab_3 = 1.5 * PI;

BOOL ab1;

HINSTANCE gInstance_ak = NULL;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void Initialize(void);
	void Display(void);

	WNDCLASSEX wndclass_ak;
	HWND hwnd_ak;
	MSG msg_ak;
	TCHAR szAppName_ak[] = TEXT("MyApp");
	bool bDone_ak = false;

	if (fopen_s(&gpFile_ak, "logs.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Cannot Open File"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile_ak, "Log File Starts Here ...");
	}

	int iScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	int iScreenHeight = GetSystemMetrics(SM_CYSCREEN);

	int iScreenCenterX = iScreenWidth / 2;
	int iScreenCenterY = iScreenHeight / 2;

	int iWindowCenterX = WIN_WIDTH / 2;
	int iWindowsCenterY = WIN_HEIGHT / 2;

	int iWindowX = iScreenCenterX - iWindowCenterX;
	int iWindowY = iScreenCenterY - iWindowsCenterY;

	wndclass_ak.cbSize = sizeof(WNDCLASSEX);
	wndclass_ak.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass_ak.cbClsExtra = 0;
	wndclass_ak.cbWndExtra = 0;
	wndclass_ak.hInstance = hInstance;
	wndclass_ak.lpfnWndProc = WndProc;
	wndclass_ak.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));
	wndclass_ak.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass_ak.lpszClassName = szAppName_ak;
	wndclass_ak.lpszMenuName = NULL;
	wndclass_ak.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass_ak.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));

	RegisterClassEx(&wndclass_ak);

	hwnd_ak = CreateWindowEx(WS_EX_APPWINDOW,
							 szAppName_ak,
							 TEXT("Dynamic India"),
							 WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
							 iWindowX,
							 iWindowY,
							 WIN_WIDTH,
							 WIN_HEIGHT,
							 NULL,
							 NULL,
							 hInstance,
							 NULL);
	ghwnd_ak = hwnd_ak;
	gInstance_ak = hInstance;

	Initialize();
	ShowWindow(hwnd_ak, iCmdShow);
	SetForegroundWindow(hwnd_ak);
	SetFocus(hwnd_ak);

	while (bDone_ak == false)
	{
		if (PeekMessage(&msg_ak, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_ak.message == WM_QUIT)
			{
				bDone_ak = true;
			}
			else
			{
				TranslateMessage(&msg_ak);
				DispatchMessage(&msg_ak);
			}
		}
		else
		{
			if (gbActiveWindow_ak == true)
			{
				Display();
			}
		}
	}

	return ((int)msg_ak.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen(void);
	void Resize(int, int);
	void Uninitialize(void);

	switch (iMsg)
	{
	case WM_SETFOCUS:
	{
		gbActiveWindow_ak = true;
		break;
	}
	case WM_KILLFOCUS:
	{
		gbActiveWindow_ak = false;
		break;
	}
	case WM_ERASEBKGND:
	{
		return (0);
	}
	case WM_SIZE:
	{
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	}
	case WM_KEYDOWN:
	{
		switch (wParam)
		{
		case VK_ESCAPE:
		{
			DestroyWindow(hwnd);
			break;
		}
		case 0x46:
		case 0x66:
		{
			ToggleFullScreen();
			break;
		}
		default:
		{
			break;
		}
		}
		break;
	}
	case WM_CLOSE:
	{
		DestroyWindow(hwnd);
		break;
	}
	case WM_DESTROY:
	{
		Uninitialize();
		PostQuitMessage(0);
		break;
	}

	case WM_CREATE:
		ab1 = PlaySound(MAKEINTRESOURCE(SONG_ID), gInstance_ak, SND_RESOURCE | SND_ASYNC);
		fprintf(gpFile_ak, "%dLog File completeeeee", ab1);

		break;
	default:
	{
		break;
	}
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	MONITORINFO mi_ak = {sizeof(MONITORINFO)};

	if (gbFullScreen_ak == false)
	{
		dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);
		if ((dwStyle_ak & WS_OVERLAPPEDWINDOW))
		{
			if ((GetWindowPlacement(ghwnd_ak, &wpPrev_ak) && (GetMonitorInfo(MonitorFromWindow(ghwnd_ak, MONITORINFOF_PRIMARY), &mi_ak))))
			{
				SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak & (~WS_OVERLAPPEDWINDOW)));
				SetWindowPos(ghwnd_ak,
							 HWND_TOP,
							 mi_ak.rcMonitor.left,
							 mi_ak.rcMonitor.top,
							 (mi_ak.rcMonitor.right - mi_ak.rcMonitor.left),
							 (mi_ak.rcMonitor.bottom - mi_ak.rcMonitor.top),
							 SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen_ak = true;
	}
	else
	{
		SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
		SetWindowPos(ghwnd_ak,
					 HWND_TOP,
					 0,
					 0,
					 0,
					 0,
					 (SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOOWNERZORDER));
		ShowCursor(TRUE);
		gbFullScreen_ak = false;
	}
}

void Initialize(void)
{
	void Resize(int, int);

	PIXELFORMATDESCRIPTOR pFD_ak;
	int iPixelFormatIndex_ak;

	ghdc_ak = GetDC(ghwnd_ak);

	ZeroMemory(&pFD_ak, sizeof(PIXELFORMATDESCRIPTOR));
	pFD_ak.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pFD_ak.nVersion = 1;
	pFD_ak.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pFD_ak.iPixelType = PFD_TYPE_RGBA;
	pFD_ak.cColorBits = 32;
	pFD_ak.cRedBits = 8;
	pFD_ak.cGreenBits = 8;
	pFD_ak.cBlueBits = 8;
	pFD_ak.cAlphaBits = 8;

	iPixelFormatIndex_ak = ChoosePixelFormat(ghdc_ak, &pFD_ak);
	if (iPixelFormatIndex_ak == 0)
	{
		fprintf(gpFile_ak, "ChoosePixelFormat function failed");
		DestroyWindow(ghwnd_ak);
	}

	if (SetPixelFormat(ghdc_ak, iPixelFormatIndex_ak, &pFD_ak) == FALSE)
	{
		fprintf(gpFile_ak, "SetPixelFormat function failed");
		DestroyWindow(ghwnd_ak);
	}

	ghrc_ak = wglCreateContext(ghdc_ak);
	if (ghrc_ak == NULL)
	{
		fprintf(gpFile_ak, "wglCreateContext function failed");
		DestroyWindow(ghwnd_ak);
	}

	if (wglMakeCurrent(ghdc_ak, ghrc_ak) == FALSE)
	{
		fprintf(gpFile_ak, "wglMakeCurrent function failed");
		DestroyWindow(ghwnd_ak);
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	Resize(WIN_WIDTH, WIN_HEIGHT);
}

void Resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
}

void Display(void)
{

	glTranslatef(0.0f, 0.0f, -1.0f);
	void IAF();

	void firstI();
	void N();
	void secondI();
	void D();
	void A();
	void Indianflag();
	void Draw_Aeroplane();
	void lowerPlane();

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);

	firstI();

	if (a == 1)
		N();
	if (a == 2)
		secondI();
	if (a == 3)
		A();
	if (a == 4)
		D();

	if (a == 6)

	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -4.0f);
		static float i_x = PI;

		if (ab < 1.5f * PI)
		{
			x_1 = 2.0f * cos(ab) - 5.0f;
			y_1 = 2.0f * sin(ab) + 1.5f;
		}
		glTranslatef(x_1, y_1, -4.0f);
		Draw_Aeroplane();
		IAF();

		ab += 0.0001f;
		x_1 += 0.0001f;

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -4.0f);

		static float ab_1 = PI;
		if (ab_1 > 0.5f * PI)
		{
			x_2 = 2.0f * cos(ab_1) - 5.0f;
			y_2 = 2.0f * sin(ab_1) - 2.5f;
		}
		glTranslatef(x_2, y_2, -4.0f);
		Draw_Aeroplane();
		IAF();

		ab_1 -= 0.0001f;
		x_2 += 0.0001f;

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(x_3, -0.5f, -8.0f);
		Draw_Aeroplane();
		IAF();
		//x_3+=0.001f;
		if (x_3 > 4.5f)
			x_3 += 0.0002f;
		else
			x_3 += 0.0001f;

		if (x_3 > 4.5f)
			a = 5;

		if (a == 5)
			lowerPlane();
	}

	if (x_3 > 3.5)
		Indianflag();

	SwapBuffers(ghdc_ak);
}

void IAF()
{
	glLineWidth(2.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-0.2f, 0.15f, 0.0f);
	glVertex3f(-0.2f, -0.15f, 0.0f);
	glVertex3f(0.0f, 0.15f, 0.0f);
	glVertex3f(-0.12f, -0.15f, 0.0f);
	glVertex3f(0.0f, 0.15f, 0.0f);
	glVertex3f(0.12f, -0.15f, 0.0f);
	glVertex3f(-0.07f, 0.0f, 0.0f);
	glVertex3f(0.07f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.15f, 0.0f);
	glVertex3f(0.2f, -0.15f, 0.0f);
	glVertex3f(0.2f, 0.15f, 0.0f);
	glVertex3f(0.35f, 0.15f, 0.0f);
	glVertex3f(0.2f, 0.02f, 0.0f);
	glVertex3f(0.3f, 0.02f, 0.0f);

	glEnd();
}

void lowerPlane()
{
	void Draw_Aeroplane();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.0f, 3.0f, -4.0f);

	if (ab_3 <= (2.0f * PI))
	{
		x_1 = 2.5f * cos(ab_3) + 4.5f;
		y_1 = 2.5f * sin(ab_3) + 2.0f;
	}
	glTranslatef(x_1, y_1, -4.0f);
	//Draw_Aeroplane();
	ab_3 += 0.0001f;
	// x_1+=0.0002f;
	// y_1+=0.0002f;

	static float ab_2 = 0.5f * PI;
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.0f, -3.5f, -4.0f);

	if (ab_2 >= 0.0f)
	{
		x_2 = 2.5f * cos(ab_2) + 4.5f;
		y_2 = 2.5f * sin(ab_2) - 3.0f;
	}
	glTranslatef(x_2, y_2, -4.0f);
	//Draw_Aeroplane();
	ab_2 -= 0.0001f;
	// x_2+=0.0002f;
	// y_2+=0.0002f;
}

void firstI()
{
	void N();

	static float x_f_I_ak = -2.3f;

	glLoadIdentity();
	glTranslatef(x_f_I_ak, 0.1f, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.1f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);

	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.15f, 0.1f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.15f, -0.6f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.05f, -0.6f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.05f, 0.1f, 0.0f);

	glEnd();

	if (x_f_I_ak < -1.3f)
		x_f_I_ak += 0.0001f;

	glLoadIdentity();
	glTranslatef(x_f_I_ak, -0.7f, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.1f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);

	glEnd();

	if (x_f_I_ak < -1.3f)
		x_f_I_ak += 0.0001f;

	else
		a = 1;
}

void secondI()
{

	void A();

	static float y_s_I_ak = -2.3f;

	glLoadIdentity();
	glTranslatef(0.7f, y_s_I_ak, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.1f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);

	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.15f, 0.1f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.15f, -0.6f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.05f, -0.6f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.05f, 0.1f, 0.0f);

	glEnd();

	if (y_s_I_ak < 0.1f)
		y_s_I_ak += 0.0001f;

	else
		a = 3;

	glLoadIdentity();
	glTranslatef(0.7f, y_s_I_ak - 0.8f, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.1f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);

	glEnd();

	if (y_s_I_ak < -0.7f)
		y_s_I_ak += 0.0001f;
}

void D()
{

	glLoadIdentity();
	glTranslatef(-0.3f, -0.7f, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(0.0f, cd_1, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);
	glColor3f(cd_1, cd_1, cd_1);
	glVertex3f(0.0f, 0.1f, 0.0f);
	glColor3f(cd_1, cd_2, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.1f, 1.0f, 0.0f);

	glEnd();

	glLoadIdentity();
	glTranslatef(0.05f, 0.1f, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(cd_1, cd_2, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(-0.25f, 0.2f, 0.0f);
	glVertex3f(-0.25f, 0.1f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);

	glEnd();

	glLoadIdentity();
	glTranslatef(0.05f, -0.7f, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(0.0f, cd_1, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(-0.25f, 0.2f, 0.0f);
	glVertex3f(-0.25f, 0.1f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);

	glEnd();

	glLoadIdentity();
	glTranslatef(0.15f, -0.7f, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(0.0f, cd_1, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(0.0f, 0.2f, 0.0f);
	glColor3f(cd_1, cd_2, 0.0f);
	glVertex3f(0.0f, 0.9f, 0.0f);
	glColor3f(cd_1, cd_1, cd_1);
	glVertex3f(0.1f, 0.9f, 0.0f);

	glEnd();

	if (cd_1 <= 1.0f)
		cd_1 += 0.001f;
	if (cd_2 <= 0.5f)
		cd_2 += 0.001f;

	a = 6;
}

void N()
{

	void secondI();

	static float n_f_ak = 2.3f;

	glLoadIdentity();
	glTranslatef(-1.0f, n_f_ak, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);
	glVertex3f(0.0f, 0.1f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.1f, 1.0f, 0.0f);

	glEnd();

	if (n_f_ak > -0.7f)
		n_f_ak -= 0.0001f;

	glLoadIdentity();
	glTranslatef(-0.6f, n_f_ak, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);
	glVertex3f(0.0f, 0.1f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.1f, 1.0f, 0.0f);

	glEnd();

	if (n_f_ak > -0.7f)
		n_f_ak -= 0.0001f;

	glLoadIdentity();
	glTranslatef(-1.0f, n_f_ak, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.3f, 0.1f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.4f, 0.1f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.1f, 1.0f, 0.0f);

	glEnd();

	if (n_f_ak > -0.7f)
		n_f_ak -= 0.0001f;

	else
		a = 2;
}

void A()
{
	static float a_x_ak = 2.3f;

	glLoadIdentity();
	glTranslatef(a_x_ak, -0.7f, -3.0f);

	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.3f, 0.1f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.4f, 0.1f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.1f, 1.0f, 0.0f);

	glEnd();

	if (a_x_ak > 1.1f)
		a_x_ak -= 0.0001f;

	else
		a = 4;

	glLoadIdentity();

	glTranslatef(a_x_ak + 0.2f, -0.7f, -3.0f);
	glScalef(-1.0f, 1.0f, 1.0f);

	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.3f, 0.1f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.4f, 0.1f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.1f, 1.0f, 0.0f);

	glEnd();

	if (a_x_ak > 1.3f)
		a_x_ak -= 0.0001f;
}

void Indianflag()
{
	glLoadIdentity();
	glTranslatef(1.9f, -0.43f, -5.0f);

	glBegin(GL_QUADS);

	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.1f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);

	glEnd();

	glLoadIdentity();
	glTranslatef(1.9f, -0.51f, -5.0f);

	glBegin(GL_QUADS);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.1f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);

	glEnd();

	glLoadIdentity();
	glTranslatef(1.9f, -0.59f, -5.0f);

	glBegin(GL_QUADS);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.1f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);

	glEnd();
}

void Draw_Aeroplane()
{

	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.4f, 0.1f, 0.0f);
	glVertex3f(-0.75f, 0.1f, 0.0f);
	glVertex3f(-0.75f, -0.1f, 0.0f);
	glVertex3f(-0.4f, -0.1f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.45f, 0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.45f, 0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.45f, -0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.45f, -0.18f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.45f, 0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.45f, -0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.8f, -0.18f, 0.0f);

	glEnd();

	glBegin(GL_TRIANGLES);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.45f, 0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.20f, 0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.45f, 0.45f, 0.0f);

	glEnd();

	glBegin(GL_TRIANGLES);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.25f, -0.13f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.25f, -0.40f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.45f, -0.13f, 0.0f);

	glEnd();
}

void Uninitialize(void)
{
	if (gbFullScreen_ak == true)
	{
		dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);

		SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
		SetWindowPos(ghwnd_ak,
					 HWND_TOP,
					 0,
					 0,
					 0,
					 0,
					 SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc_ak)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc_ak)
	{
		wglDeleteContext(ghrc_ak);
		ghrc_ak = NULL;
	}

	if (ghdc_ak)
	{
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	if (gpFile_ak)
	{
		fprintf(gpFile_ak, "Log File complete");
		fclose(gpFile_ak);
		gpFile_ak = NULL;
	}
}