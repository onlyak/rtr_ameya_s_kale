#include<windows.h>

//global func declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");


	BOOL fResult;
	RECT coordinates;
	fResult = SystemParametersInfo(SPI_GETWORKAREA, 0, &coordinates, 0);
	int iUpperLeftX = coordinates.left;
	int iUpperLeftY = coordinates.top;
	int iLowerRightX = coordinates.right;
	int iLowerRightY = coordinates.bottom;
	
	int screen_wd=iLowerRightX-iUpperLeftX;
    int screen_ht = iLowerRightY-iUpperLeftY;

    int centered_wd=screen_wd/2-(800/2);
    int centered_ht=screen_ht/2-(600/2);

	//initialization
	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.lpfnWndProc = WndProc;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = szAppName;
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register class WNDCLASSEX
	RegisterClassEx(&wndClass);

	//creating window
	hwnd = CreateWindow(szAppName,	
		TEXT("Ameya's Window"),
		WS_OVERLAPPEDWINDOW,
		centered_wd,
        centered_ht,
		800,
        600,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	//message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int)msg.wParam);
}

//definition
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	switch (iMsg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}
