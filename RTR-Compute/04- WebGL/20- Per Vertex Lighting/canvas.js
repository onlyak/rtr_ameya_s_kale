var canvas = null;
var gl = null;
var canvas_original_width;
var canvas_original_height;
var bFullscreen_ak = false;

const WebGLMacros = {
    ATTRIBUTE_VERTEX: 0,
    ATTRIBUTE_COLOR: 1,
    ATTRIBUTE_NORMAL: 2,
    ATTRIBUTE_TEXTURE0: 3,
};

var vertexShaderObject_ak;
var fragmentShaderObject_ak;
var shaderProgramObject_ak;

var modelUniform_ak;
var viewUniform_ak;
var projectionUniform_ak;
var ldUniform_ak;
var kdUniform_ak;
var laUniform_ak;
var kaUniform_ak;
var lsUniform_ak;
var ksUniform_ak;
var shininessUniform_ak;
var lightPositionUniform_ak;
var keyPressed_l_Uniform_ak;

var bLight_ak = false;
var bAnimate_ak = false;

var perspectiveProjectionMatrix_ak;

var lightAmbient_ak = new  Float32Array ([0.0,0.0,0.0]);
var lightDiffuse_ak = new  Float32Array ([1.0,1.0,1.0]);
var lightSpecular_ak = new Float32Array ([1.0,1.0,1.0]);
var lightPosition_ak = new Float32Array ([100.0,100.0,100.0,1.0]);

var materialAmbient_ak = new Float32Array ([0.0,0.0,0.0]);
var materialDiffuse_ak = new Float32Array ([1.0,1.0,1.0]);
var materialSpecular_ak =new Float32Array ([1.0,1.0,1.0]);
var materialShininess_ak = 50.0;

var sphere_ak=null;

var requestAnimationFrame = window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame  ||
                            window.mozRequestAnimationFrame ||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var cancelAnimationFrame = window.cancelAnimationFrame || 
                           window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
                           window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
                           window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
                           window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

function main(){

    canvas = document.getElementById("ASK");
    if(!canvas){
        console.log("Obtaining Canvas Failed");
    }
    else{
        // console.log("Obtaining Canvas Succeeded");
    }

    canvas_original_width = canvas.width;    
    canvas_original_height = canvas.height;
    
    // window is built-in variable
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("resize", resize, false);

    init();

    resize(); // warmup resize call

    draw(); // warmup re-paint call

}

function init(){
    gl = canvas.getContext("webgl2");
    if(!gl){
        console.log("Obtaining webgl2 Context Failed");
    }
    else{
        console.log("Obtaining webgl2 Context Succeeded");
    }
    
    gl.viewportWidth  = canvas.width;
    gl.viewportHeight  = canvas.height;

    //Vertex Shader
    var vertexShaderSourceCode = 
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;" +
    "in vec3 vNormal;" +
    "uniform mat4 u_model_matrix;" +
    "uniform mat4 u_view_matrix;" +
    "uniform mat4 u_projection_matrix;" +
    "uniform lowp int l_key_pressed;"+
    "uniform vec3 u_Ld;"+
    "uniform vec3 u_Kd;"+
    "uniform vec3 u_La;"+
    "uniform vec3 u_Ka;"+
    "uniform vec3 u_Ls;"+
    "uniform vec3 u_Ks;"+
    "uniform vec4 u_light_position;"+
    "uniform float u_shininess;" +
    "out vec3 phong_ads_light;" +
    "void main()" +
    "{" +
    "if (l_key_pressed == 1) " +
        "{" +
            "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" +
            "vec3 transformed_normal = normalize(mat3(u_view_matrix*u_model_matrix)*vNormal);"+
            "vec3 light_direction = normalize(vec3(u_light_position-eyeCoordinates));"+
            "vec3 reflection_vector = reflect(-light_direction, transformed_normal);"+
            "vec3 view_vector = normalize(-eyeCoordinates.xyz);"+
            "vec3 ambient = u_La * u_Ka;"+
            "vec3 diffuse = u_Ld * u_Kd * max(dot(light_direction, transformed_normal),0.0);"+
            "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, view_vector),0.0),u_shininess);" +
            "phong_ads_light = ambient + diffuse + specular;"+
        "}" +
        "else"+
        "{" +
            "phong_ads_light = vec3(1.0, 1.0, 1.0);" +
        "}" +
    "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
   "}";

    vertexShaderObject_ak = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject_ak, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject_ak);

    if(gl.getShaderParameter(vertexShaderObject_ak, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(vertexShaderObject_ak);
            if(error.length > 0){
                alert(error);
            }
    }

    //Fragment Shader
    var fragmentShaderSourceCode = 
    "#version 300 es"+
    "\n"+
    "precision highp float;" +
    "out vec4 FragColor;" +
    "in vec3 phong_ads_light;" +
    "void main()"+
    "{" +
        "FragColor = vec4(phong_ads_light, 1.0);"  +
    "}";

    fragmentShaderObject_ak = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject_ak, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject_ak);

    if(gl.getShaderParameter(fragmentShaderObject_ak, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(fragmentShaderObject_ak);
            if(error.length > 0){
                alert(error);
                uninitalize();
            }
    }

    //Shader Program
    shaderProgramObject_ak = gl.createProgram();
    gl.attachShader(shaderProgramObject_ak, vertexShaderObject_ak);
    gl.attachShader(shaderProgramObject_ak, fragmentShaderObject_ak);

    gl.bindAttribLocation(shaderProgramObject_ak, WebGLMacros.ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject_ak, WebGLMacros.ATTRIBUTE_NORMAL, "vNormal");

    //linking
    gl.linkProgram(shaderProgramObject_ak);

    if(gl.getProgramParameter(shaderProgramObject_ak, gl.LINK_STATUS) == false){
        var error = gl.getProgramInfoLog(shaderProgramObject_ak);
            if(error.length > 0){
                alert(error);
                uninitalize();
            }
    }

    //uniforms
    modelUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_model_matrix");
    viewUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_view_matrix");
    projectionUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_projection_matrix");
	ldUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_Ld");
	kdUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_Kd");
	laUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_La");
	kaUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_Ka");
	lsUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_Ls");
	ksUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_Ks");
    shininessUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_shininess");
	lightPositionUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_light_position");
    keyPressed_l_Uniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "l_key_pressed");
    
    sphere_ak = new Mesh();
    makeSphere(sphere_ak, 1.0, 30, 30);

	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
    gl.clearDepth(1.0);

    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    perspectiveProjectionMatrix_ak = mat4.create();
}

function resize(){
    if (bFullscreen_ak == true){
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else{
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    gl.viewport(0, 0, canvas.width,canvas.height);

    //perspective call
    mat4.perspective(perspectiveProjectionMatrix_ak, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
    
}

function draw(){
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObject_ak);

    if(bLight_ak){
        gl.uniform1i(keyPressed_l_Uniform_ak, 1);

        gl.uniform3fv(laUniform_ak, lightAmbient_ak);
		gl.uniform3fv(kaUniform_ak, materialAmbient_ak);
		gl.uniform3fv(ldUniform_ak, lightDiffuse_ak);
		gl.uniform3fv(kdUniform_ak, materialDiffuse_ak);
		gl.uniform3fv(lsUniform_ak, lightSpecular_ak);
		gl.uniform3fv(ksUniform_ak, materialSpecular_ak);

        gl.uniform4fv(lightPositionUniform_ak, [100.0,100.0,100.0,1.0]);

        gl.uniform1f(shininessUniform_ak, materialShininess_ak);
    }
    else
        gl.uniform1i(keyPressed_l_Uniform_ak, 0);

    var modelMatrix_ak = mat4.create();
    var viewMatrix_ak = mat4.create();

    mat4.translate(modelMatrix_ak, modelMatrix_ak, [0.0, 0.0, -3.0]);

    gl.uniformMatrix4fv(modelUniform_ak, false, modelMatrix_ak);
	gl.uniformMatrix4fv(viewUniform_ak, false, viewMatrix_ak);
	gl.uniformMatrix4fv(projectionUniform_ak, false, perspectiveProjectionMatrix_ak);

    sphere_ak.draw();

    gl.useProgram(null);

    update();

    requestAnimationFrame(draw, canvas);
}

function update(){

}

function degToRad(degree) {
    return (degree * Math.PI / 180.0);
}

function toggleFullscreen(){

    var fullscreen_element = document.fullscreenElement || 
                             document.webkitFullscreenElement ||
                             document.mozFullScreenElement ||
                             document.msFullscreenElement ||
                             null ;

    if (fullscreen_element == null) {
      bFullscreen_ak = true;

      if (canvas.requestFullscreen) {
        canvas.requestFullscreen();
      } else if (canvas.webkitRequestFullscreen) {
        canvas.webkitRequestFullscreen();
      } else if (canvas.mozRequestFullScreenElement) {
        canvas.mozRequestFullScreenElement();
      } else if (canvas.msRequestFullscreen) {
        canvas.msRequestFullscreen();
      }
    } else {
      bFullscreen_ak = false;

      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      }
    }
}

function keyDown(event){
    switch(event.keyCode){
        case 70:
            toggleFullscreen();
            break;
        
        case 27:
            uninitalize();
            window.close();
            break;
        
        case 76:
            if(bLight_ak == false){
                bLight_ak = true;
            }
            else{
                bLight_ak = false;
            }
            break;
    }
}

function uninitalize(){
    if(shaderProgramObject_ak){

        if(vertexShaderObject_ak){
            gl.detachShader(shaderProgramObject_ak, vertexShaderObject_ak);
            gl.deleteShader(vertexShaderObject_ak);
            vertexShaderObject_ak = null;
        }

        if(fragmentShaderObject_ak){
            gl.detachShader(shaderProgramObject_ak, fragmentShaderObject_ak);
            gl.deleteShader(fragmentShaderObject_ak);
            fragmentShaderObject_ak = null;
        }

        gl.deleteProgram(shaderProgramObject_ak);
        shaderProgramObject_ak = null;
    }
}
