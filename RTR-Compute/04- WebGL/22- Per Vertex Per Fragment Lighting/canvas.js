var canvas = null;
var gl = null;
var canvas_original_width;
var canvas_original_height;
var bFullscreen_ak = false;

const WebGLMacros = {
    ATTRIBUTE_VERTEX: 0,
    ATTRIBUTE_COLOR: 1,
    ATTRIBUTE_NORMAL: 2,
    ATTRIBUTE_TEXTURE0: 3,
};

var vertexShaderObject_ak_pv;
var fragmentShaderObject_ak_pv;
var shaderProgramObject_ak_pv;

var modelUniform_ak_pv;
var viewUniform_ak_pv;
var projectionUniform_ak_pv;
var ldUniform_ak_pv;
var kdUniform_ak_pv;
var laUniform_ak_pv;
var kaUniform_ak_pv;
var lsUniform_ak_pv;
var ksUniform_ak_pv;
var shininessUniform_ak_pv;
var lightPositionUniform_ak_pv;
var keyPressed_l_Uniform_ak_pv;

////////////////////////////////

var vertexShaderObject_ak_pf;
var fragmentShaderObject_ak_pf;
var shaderProgramObject_ak_pf;

var modelUniform_ak_pf;
var viewUniform_ak_pf;
var projectionUniform_ak_pf;
var ldUniform_ak_pf;
var kdUniform_ak_pf;
var laUniform_ak_pf;
var kaUniform_ak_pf;
var lsUniform_ak_pf;
var ksUniform_ak_pf;
var shininessUniform_ak_pf;
var lightPositionUniform_ak_pf;
var keyPressed_l_Uniform_ak_pf;

var bLight_ak = false;

var perspectiveProjectionMatrix_ak;

var lightAmbient_ak = new  Float32Array ([0.0,0.0,0.0]);
var lightDiffuse_ak = new  Float32Array ([1.0,1.0,1.0]);
var lightSpecular_ak = new Float32Array ([1.0,1.0,1.0]);
var lightPosition_ak = new Float32Array ([100.0,100.0,100.0,1.0]);

var materialAmbient_ak = new Float32Array ([0.0,0.0,0.0]);
var materialDiffuse_ak = new Float32Array ([1.0,1.0,1.0]);
var materialSpecular_ak =new Float32Array ([1.0,1.0,1.0]);
var materialShininess_ak = 50.0;

var sphere_ak=null;

var pvpf = 1;

var requestAnimationFrame = window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame  ||
                            window.mozRequestAnimationFrame ||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var cancelAnimationFrame = window.cancelAnimationFrame || 
                           window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
                           window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
                           window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
                           window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

function main(){

    canvas = document.getElementById("ASK");
    if(!canvas){
        console.log("Obtaining Canvas Failed");
    }
    else{
        // console.log("Obtaining Canvas Succeeded");
    }

    canvas_original_width = canvas.width;    
    canvas_original_height = canvas.height;
    
    // window is built-in variable
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("resize", resize, false);

    init();

    resize(); // warmup resize call

    draw(); // warmup re-paint call

}

function init(){
    gl = canvas.getContext("webgl2");
    if(!gl){
        console.log("Obtaining webgl2 Context Failed");
    }
    else{
        console.log("Obtaining webgl2 Context Succeeded");
    }
    
    gl.viewportWidth  = canvas.width;
    gl.viewportHeight  = canvas.height;

    /****************Per Vertex Shader Starts************************/

    //Vertex Shader
    var vertexShaderSourceCode_pv = 
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;" +
    "in vec3 vNormal;" +
    "uniform mat4 u_model_matrix;" +
    "uniform mat4 u_view_matrix;" +
    "uniform mat4 u_projection_matrix;" +
    "uniform lowp int l_key_pressed;"+
    "uniform vec3 u_Ld;"+
    "uniform vec3 u_Kd;"+
    "uniform vec3 u_La;"+
    "uniform vec3 u_Ka;"+
    "uniform vec3 u_Ls;"+
    "uniform vec3 u_Ks;"+
    "uniform vec4 u_light_position;"+
    "uniform float u_shininess;" +
    "out vec3 phong_ads_light;" +
    "void main()" +
    "{" +
    "if (l_key_pressed == 1) " +
        "{" +
            "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" +
            "vec3 transformed_normal = normalize(mat3(u_view_matrix*u_model_matrix)*vNormal);"+
            "vec3 light_direction = normalize(vec3(u_light_position-eyeCoordinates));"+
            "vec3 reflection_vector = reflect(-light_direction, transformed_normal);"+
            "vec3 view_vector = normalize(-eyeCoordinates.xyz);"+
            "vec3 ambient = u_La * u_Ka;"+
            "vec3 diffuse = u_Ld * u_Kd * max(dot(light_direction, transformed_normal),0.0);"+
            "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, view_vector),0.0),u_shininess);" +
            "phong_ads_light = ambient + diffuse + specular;"+
        "}" +
        "else"+
        "{" +
            "phong_ads_light = vec3(1.0, 1.0, 1.0);" +
        "}" +
    "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
   "}";

    vertexShaderObject_ak_pv = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject_ak_pv, vertexShaderSourceCode_pv);
    gl.compileShader(vertexShaderObject_ak_pv);

    if(gl.getShaderParameter(vertexShaderObject_ak_pv, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(vertexShaderObject_ak_pv);
            if(error.length > 0){
                alert(error);
            }
    }

    //Fragment Shader
    var fragmentShaderSourceCode_pv = 
    "#version 300 es"+
    "\n"+
    "precision highp float;" +
    "out vec4 FragColor;" +
    "in vec3 phong_ads_light;" +
    "void main()"+
    "{" +
        "FragColor = vec4(phong_ads_light, 1.0);"  +
    "}";

    fragmentShaderObject_ak_pv = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject_ak_pv, fragmentShaderSourceCode_pv);
    gl.compileShader(fragmentShaderObject_ak_pv);

    if(gl.getShaderParameter(fragmentShaderObject_ak_pv, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(fragmentShaderObject_ak_pv);
            if(error.length > 0){
                alert(error);
                uninitalize();
            }
    }

    //Shader Program
    shaderProgramObject_ak_pv = gl.createProgram();
    gl.attachShader(shaderProgramObject_ak_pv, vertexShaderObject_ak_pv);
    gl.attachShader(shaderProgramObject_ak_pv, fragmentShaderObject_ak_pv);

    gl.bindAttribLocation(shaderProgramObject_ak_pv, WebGLMacros.ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject_ak_pv, WebGLMacros.ATTRIBUTE_NORMAL, "vNormal");

    //linking
    gl.linkProgram(shaderProgramObject_ak_pv);

    if(gl.getProgramParameter(shaderProgramObject_ak_pv, gl.LINK_STATUS) == false){
        var error = gl.getProgramInfoLog(shaderProgramObject_ak_pv);
            if(error.length > 0){
                alert(error);
                uninitalize();
            }
    }

    //uniforms
    modelUniform_ak_pv = gl.getUniformLocation(shaderProgramObject_ak_pv, "u_model_matrix");
    viewUniform_ak_pv = gl.getUniformLocation(shaderProgramObject_ak_pv, "u_view_matrix");
    projectionUniform_ak_pv = gl.getUniformLocation(shaderProgramObject_ak_pv, "u_projection_matrix");
	ldUniform_ak_pv = gl.getUniformLocation(shaderProgramObject_ak_pv, "u_Ld");
	kdUniform_ak_pv = gl.getUniformLocation(shaderProgramObject_ak_pv, "u_Kd");
	laUniform_ak_pv = gl.getUniformLocation(shaderProgramObject_ak_pv, "u_La");
	kaUniform_ak_pv = gl.getUniformLocation(shaderProgramObject_ak_pv, "u_Ka");
	lsUniform_ak_pv = gl.getUniformLocation(shaderProgramObject_ak_pv, "u_Ls");
	ksUniform_ak_pv = gl.getUniformLocation(shaderProgramObject_ak_pv, "u_Ks");
    shininessUniform_ak_pv = gl.getUniformLocation(shaderProgramObject_ak_pv, "u_shininess");
	lightPositionUniform_ak_pv = gl.getUniformLocation(shaderProgramObject_ak_pv, "u_light_position");
    keyPressed_l_Uniform_ak_pv = gl.getUniformLocation(shaderProgramObject_ak_pv, "l_key_pressed");

    /*************************Per Vertex Shader Ends*********************************/


    /**************************Per Fragment Shader Starts****************************/
    //Vertex Shader
    var vertexShaderSourceCode_pf = 
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;" +
    "in vec3 vNormal;" +
    "uniform mat4 u_model_matrix;" +
    "uniform mat4 u_view_matrix;" +
    "uniform mat4 u_projection_matrix;" +
    "uniform lowp int l_key_pressed;"+
    "uniform vec4 u_light_position;"+
    "out vec3 transformed_normal;" +
    "out vec3 light_direction;" +
    "out vec3 view_vector;" +
    "void main()" +
    "{" +
    "if (l_key_pressed == 1) " +
        "{" +
            "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" +
            "transformed_normal = mat3(u_view_matrix*u_model_matrix) * vNormal;"+
            "light_direction = vec3(u_light_position-eyeCoordinates);"+
            "view_vector = -eyeCoordinates.xyz;"+
        "}" +
    "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
   "}";

    vertexShaderObject_ak_pf = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject_ak_pf, vertexShaderSourceCode_pf);
    gl.compileShader(vertexShaderObject_ak_pf);

    if(gl.getShaderParameter(vertexShaderObject_ak_pf, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(vertexShaderObject_ak_pf);
            if(error.length > 0){
                alert(error);
            }
    }

    //Fragment Shader
    var fragmentShaderSourceCode_pf = 
    "#version 300 es"+
    "\n"+
    "precision highp float;" +
    "out vec4 FragColor;" +
    "in vec3 transformed_normal;" +
    "in vec3 light_direction;" +
    "in vec3 view_vector;" +
    "uniform vec3 u_Ld;"+
    "uniform vec3 u_Kd;"+
    "uniform vec3 u_La;"+
    "uniform vec3 u_Ka;"+
    "uniform vec3 u_Ls;"+
    "uniform vec3 u_Ks;"+
    "uniform float u_shininess;" +
    "uniform lowp int l_key_pressed;"+
    "void main()"+
    "{" +
        "vec3 phong_ads_color;" +
        "if(l_key_pressed == 1)"+
        "{" +
            "vec3 norm_transformed_normal = normalize(transformed_normal);" +
            "vec3 norm_light_direction = normalize(light_direction);" +
            "vec3 norm_view_vector = normalize(view_vector);"+
            "vec3 reflection_vector = reflect(-norm_light_direction, norm_transformed_normal);"+
            "vec3 ambient = u_La * u_Ka;"+
            "vec3 diffuse = u_Ld * u_Kd * max(dot(norm_light_direction, norm_transformed_normal),0.0);"+
            "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, norm_view_vector),0.0),u_shininess);" +
            "phong_ads_color = ambient + diffuse + specular;"+
        "}"+
        "else"+
        "{"+
            "phong_ads_color = vec3(1.0f, 1.0f, 1.0f);"+
        "}"+
        "FragColor = vec4(phong_ads_color, 1.0);"  +
    "}";

    fragmentShaderObject_ak_pf = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject_ak_pf, fragmentShaderSourceCode_pf);
    gl.compileShader(fragmentShaderObject_ak_pf);

    if(gl.getShaderParameter(fragmentShaderObject_ak_pf, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(fragmentShaderObject_ak_pf);
            if(error.length > 0){
                alert(error);
                uninitalize();
            }
    }

    //Shader Program
    shaderProgramObject_ak_pf = gl.createProgram();
    gl.attachShader(shaderProgramObject_ak_pf, vertexShaderObject_ak_pf);
    gl.attachShader(shaderProgramObject_ak_pf, fragmentShaderObject_ak_pf);

    gl.bindAttribLocation(shaderProgramObject_ak_pf, WebGLMacros.ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject_ak_pf, WebGLMacros.ATTRIBUTE_NORMAL, "vNormal");

    //linking
    gl.linkProgram(shaderProgramObject_ak_pf);

    if(gl.getProgramParameter(shaderProgramObject_ak_pf, gl.LINK_STATUS) == false){
        var error = gl.getProgramInfoLog(shaderProgramObject_ak_pf);
            if(error.length > 0){
                alert(error);
                uninitalize();
            }
    }

    //uniforms
    modelUniform_ak_pf = gl.getUniformLocation(shaderProgramObject_ak_pf, "u_model_matrix");
    viewUniform_ak_pf = gl.getUniformLocation(shaderProgramObject_ak_pf, "u_view_matrix");
    projectionUniform_ak_pf = gl.getUniformLocation(shaderProgramObject_ak_pf, "u_projection_matrix");
	ldUniform_ak_pf = gl.getUniformLocation(shaderProgramObject_ak_pf, "u_Ld");
	kdUniform_ak_pf = gl.getUniformLocation(shaderProgramObject_ak_pf, "u_Kd");
	laUniform_ak_pf = gl.getUniformLocation(shaderProgramObject_ak_pf, "u_La");
	kaUniform_ak_pf = gl.getUniformLocation(shaderProgramObject_ak_pf, "u_Ka");
	lsUniform_ak_pf = gl.getUniformLocation(shaderProgramObject_ak_pf, "u_Ls");
	ksUniform_ak_pf = gl.getUniformLocation(shaderProgramObject_ak_pf, "u_Ks");
    shininessUniform_ak_pf = gl.getUniformLocation(shaderProgramObject_ak_pf, "u_shininess");
	lightPositionUniform_ak_pf = gl.getUniformLocation(shaderProgramObject_ak_pf, "u_light_position");
    keyPressed_l_Uniform_ak_pf = gl.getUniformLocation(shaderProgramObject_ak_pf, "l_key_pressed");
    /*************************Per Fragment Shader Ends*********************************/

    sphere_ak = new Mesh();
    makeSphere(sphere_ak, 1.0, 30, 30);

	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
    gl.clearDepth(1.0);

    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    perspectiveProjectionMatrix_ak = mat4.create();
}

function resize(){
    if (bFullscreen_ak == true){
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else{
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    gl.viewport(0, 0, canvas.width,canvas.height);

    //perspective call
    mat4.perspective(perspectiveProjectionMatrix_ak, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
    
}

function draw(){
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    if(pvpf == 1){
        gl.useProgram(shaderProgramObject_ak_pv);
        if(bLight_ak){
            gl.uniform1i(keyPressed_l_Uniform_ak_pv, 1);
    
            gl.uniform3fv(laUniform_ak_pv, lightAmbient_ak);
            gl.uniform3fv(kaUniform_ak_pv, materialAmbient_ak);
            gl.uniform3fv(ldUniform_ak_pv, lightDiffuse_ak);
            gl.uniform3fv(kdUniform_ak_pv, materialDiffuse_ak);
            gl.uniform3fv(lsUniform_ak_pv, lightSpecular_ak);
            gl.uniform3fv(ksUniform_ak_pv, materialSpecular_ak);
    
            gl.uniform4fv(lightPositionUniform_ak_pv, [100.0,100.0,100.0,1.0]);
    
            gl.uniform1f(shininessUniform_ak_pv, materialShininess_ak);
        }
        else
            gl.uniform1i(keyPressed_l_Uniform_ak_pv, 0);
    }
    else{
        gl.useProgram(shaderProgramObject_ak_pf);
        if(bLight_ak){
            gl.uniform1i(keyPressed_l_Uniform_ak_pf, 1);
    
            gl.uniform3fv(laUniform_ak_pf, lightAmbient_ak);
            gl.uniform3fv(kaUniform_ak_pf, materialAmbient_ak);
            gl.uniform3fv(ldUniform_ak_pf, lightDiffuse_ak);
            gl.uniform3fv(kdUniform_ak_pf, materialDiffuse_ak);
            gl.uniform3fv(lsUniform_ak_pf, lightSpecular_ak);
            gl.uniform3fv(ksUniform_ak_pf, materialSpecular_ak);
    
            gl.uniform4fv(lightPositionUniform_ak_pf, [100.0,100.0,100.0,1.0]);
    
            gl.uniform1f(shininessUniform_ak_pf, materialShininess_ak);
        }
        else
            gl.uniform1i(keyPressed_l_Uniform_ak_pf, 0);
    }

    var modelMatrix_ak = mat4.create();
    var viewMatrix_ak = mat4.create();

    mat4.translate(modelMatrix_ak, modelMatrix_ak, [0.0, 0.0, -3.0]);

    if(pvpf == 1){
        gl.uniformMatrix4fv(modelUniform_ak_pv, false, modelMatrix_ak);
	    gl.uniformMatrix4fv(viewUniform_ak_pv, false, viewMatrix_ak);
	    gl.uniformMatrix4fv(projectionUniform_ak_pv, false, perspectiveProjectionMatrix_ak);
    }

    else{
        gl.uniformMatrix4fv(modelUniform_ak_pf, false, modelMatrix_ak);
        gl.uniformMatrix4fv(viewUniform_ak_pf, false, viewMatrix_ak);
        gl.uniformMatrix4fv(projectionUniform_ak_pf, false, perspectiveProjectionMatrix_ak);    
    }

    sphere_ak.draw();

    gl.useProgram(null);

    update();

    requestAnimationFrame(draw, canvas);
}

function update(){

}

function degToRad(degree) {
    return (degree * Math.PI / 180.0);
}

function toggleFullscreen(){

    var fullscreen_element = document.fullscreenElement || 
                             document.webkitFullscreenElement ||
                             document.mozFullScreenElement ||
                             document.msFullscreenElement ||
                             null ;

    if (fullscreen_element == null) {
      bFullscreen_ak = true;

      if (canvas.requestFullscreen) {
        canvas.requestFullscreen();
      } else if (canvas.webkitRequestFullscreen) {
        canvas.webkitRequestFullscreen();
      } else if (canvas.mozRequestFullScreenElement) {
        canvas.mozRequestFullScreenElement();
      } else if (canvas.msRequestFullscreen) {
        canvas.msRequestFullscreen();
      }
    } else {
      bFullscreen_ak = false;

      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      }
    }
}

function keyDown(event){
    switch(event.keyCode){
        case 70:
            toggleFullscreen();
            break;
        
        case 27:
            uninitalize();
            window.close();
            break;
        
        case 76:
            if(bLight_ak == false){
                bLight_ak = true;
            }
            else{
                bLight_ak = false;
            }
            break;
        
        case 80:
            pvpf = 0;
            break;
        
        case 86:
            pvpf = 1;
            break;
    }
}

function uninitalize(){
    if(shaderProgramObject_ak_pv){

        if(vertexShaderObject_ak_pv){
            gl.detachShader(shaderProgramObject_ak_pv, vertexShaderObject_ak_pv);
            gl.deleteShader(vertexShaderObject_ak_pv);
            vertexShaderObject_ak_pv = null;
        }

        if(fragmentShaderObject_ak_pv){
            gl.detachShader(shaderProgramObject_ak_pv, fragmentShaderObject_ak_pv);
            gl.deleteShader(fragmentShaderObject_ak_pv);
            fragmentShaderObject_ak_pv = null;
        }

        gl.deleteProgram(shaderProgramObject_ak_pv);
        shaderProgramObject_ak_pv = null;
    }
}
