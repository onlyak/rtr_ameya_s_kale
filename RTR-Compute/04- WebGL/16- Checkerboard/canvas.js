var canvas = null;
var gl = null;
var canvas_original_width;
var canvas_original_height;
var bFullscreen_ak = false;

const WebGLMacros = {
    ATTRIBUTE_VERTEX: 0,
    ATTRIBUTE_COLOR: 1,
    ATTRIBUTE_NORMAL: 2,
    ATTRIBUTE_TEXTURE0: 3,
};

var vertexShaderObject_ak;
var fragmentShaderObject_ak;
var shaderProgramObject_ak;

var vaoCube_ak;

var vboPositionCube_ak;
var vboTexCoordCube_ak;

var mvpUniform_ak;

var perspectiveProjectionMatrix_ak;

var textureSamplerUniform_ak;
var textureCheckerboard_ak;

var checkImageWidth_ak = 64;
var checkImageHeight_ak = 64;

var checkImage_ak = new Uint8Array(checkImageWidth_ak * checkImageHeight_ak * 4);

var requestAnimationFrame = window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame  ||
                            window.mozRequestAnimationFrame ||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var cancelAnimationFrame = window.cancelAnimationFrame || 
                           window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
                           window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
                           window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
                           window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

function main(){

    canvas = document.getElementById("ASK");
    if(!canvas){
        console.log("Obtaining Canvas Failed");
    }
    else{
        // console.log("Obtaining Canvas Succeeded");
    }

    canvas_original_width = canvas.width;    
    canvas_original_height = canvas.height;
    
    // window is built-in variable
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("resize", resize, false);

    init();

    resize(); // warmup resize call

    draw(); // warmup re-paint call

}

function init(){
    gl = canvas.getContext("webgl2");
    if(!gl){
        console.log("Obtaining webgl2 Context Failed");
    }
    else{
        console.log("Obtaining webgl2 Context Succeeded");
    }
    
    gl.viewportWidth  = canvas.width;
    gl.viewportHeight  = canvas.height;

    //Vertex Shader
    var vertexShaderSourceCode = 
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;" +
    "in vec2 vTexCoord;" +
    "out vec2 out_texCoord;" +
    "uniform mat4 u_mvp_matrix;" +
    "void main()" +
    "{" +
          "gl_Position = u_mvp_matrix * vPosition;" +
          "out_texCoord = vTexCoord;"+
    "}";

    vertexShaderObject_ak = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject_ak, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject_ak);

    if(gl.getShaderParameter(vertexShaderObject_ak, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(vertexShaderObject_ak);
            if(error.length > 0){
                alert(error);
            }
    }

    //Fragment Shader
    var fragmentShaderSourceCode = 
    "#version 300 es"+
    "\n"+
    "precision highp float;" +
    "out vec4 FragColor;" +
    "in vec2 out_texCoord;" +
    "uniform highp sampler2D u_texture_sampler;"+
    "void main()"+
    "{" +
        "FragColor = texture(u_texture_sampler, out_texCoord);" +
    "}";

    fragmentShaderObject_ak = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject_ak, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject_ak);

    if(gl.getShaderParameter(fragmentShaderObject_ak, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(fragmentShaderObject_ak);
            if(error.length > 0){
                alert(error);
                uninitalize();
            }
    }

    //Shader Program
    shaderProgramObject_ak = gl.createProgram();
    gl.attachShader(shaderProgramObject_ak, vertexShaderObject_ak);
    gl.attachShader(shaderProgramObject_ak, fragmentShaderObject_ak);

    gl.bindAttribLocation(shaderProgramObject_ak, WebGLMacros.ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject_ak, WebGLMacros.ATTRIBUTE_TEXTURE0, "vTexCoord");

    //linking
    gl.linkProgram(shaderProgramObject_ak);

    if(gl.getProgramParameter(shaderProgramObject_ak, gl.LINK_STATUS) == false){
        var error = gl.getProgramInfoLog(shaderProgramObject_ak);
            if(error.length > 0){
                alert(error);
                uninitalize();
            }
    }

    //uniforms
    mvpUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_mvp_matrix");
    textureSamplerUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_texture_sampler");

    var cubeVertices = new Float32Array([
        -2.0, -1.0, 0.0,
        -2.0, 1.0, 0.0,
         0.0, 1.0, 0.0,
         0.0, -1.0, 0.0,

         1.0, -1.0, 0.0,
         1.0, 1.0, 0.0,
         2.41421, 1.0, -1.41421,
         2.41421, -1.0, -1.41421
    ]);

    //vao cube
    vaoCube_ak = gl.createVertexArray();
    gl.bindVertexArray(vaoCube_ak);

    //vbo position 
    vboPositionCube_ak = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboPositionCube_ak);
    gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    //vbo texcoord 
    vboTexCoordCube_ak = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboTexCoordCube_ak);
    gl.bufferData(gl.ARRAY_BUFFER, null, gl.DYNAMIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.ATTRIBUTE_TEXTURE0);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
    gl.clearDepth(1.0);

    loadTexture();

    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    perspectiveProjectionMatrix_ak = mat4.create();
}

function resize(){
    if (bFullscreen_ak == true){
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else{
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    gl.viewport(0, 0, canvas.width,canvas.height);

    //perspective call
    mat4.perspective(perspectiveProjectionMatrix_ak, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
    
}

function draw(){
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObject_ak);

    var cubeTexCoords = new Float32Array(16);

    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -5.0]);

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix_ak, modelViewMatrix );

    gl.uniformMatrix4fv(mvpUniform_ak, false, modelViewProjectionMatrix);

    gl.bindTexture(gl.TEXTURE_2D, textureCheckerboard_ak);
    gl.uniform1i(textureSamplerUniform_ak, 0);

    gl.bindVertexArray(vaoCube_ak);

 
    cubeTexCoords[0] = 0.0;
    cubeTexCoords[1] = 0.0;
    cubeTexCoords[2] = 0.0;
    cubeTexCoords[3] = 1.0;
    cubeTexCoords[4] = 1.0;
    cubeTexCoords[5] = 1.0;
    cubeTexCoords[6] = 1.0;
    cubeTexCoords[7] = 0.0;

    cubeTexCoords[8] = 0.0;
    cubeTexCoords[9] = 0.0;
    cubeTexCoords[10] = 0.0;
    cubeTexCoords[11] = 1.0;
    cubeTexCoords[12] = 1.0;
    cubeTexCoords[13] = 1.0;
    cubeTexCoords[14] = 1.0;
    cubeTexCoords[15] = 0.0;


    gl.bindBuffer(gl.ARRAY_BUFFER, vboTexCoordCube_ak);
    gl.bufferData(gl.ARRAY_BUFFER, cubeTexCoords, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
 
    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
    

    gl.bindVertexArray(null);

    gl.useProgram(null);

    update();

    requestAnimationFrame(draw, canvas);
}

function update(){

}

function loadTexture() {
    
    makeCheckImage();

    textureCheckerboard_ak = gl.createTexture();
    gl.pixelStorei(gl.UNPACK_ALIGNMENT, 1);
    gl.bindTexture(gl.TEXTURE_2D, textureCheckerboard_ak);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, checkImageWidth_ak, checkImageHeight_ak, 0, gl.RGBA, gl.UNSIGNED_BYTE, checkImage_ak);
    gl.bindTexture(gl.TEXTURE_2D, null);
}

function makeCheckImage() {
    for (i = 0; i < checkImageWidth_ak; i++) {
        for (j = 0; j < checkImageHeight_ak; j++) {
          c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;
          checkImage_ak[(i * 64 + j) * 4 + 0] = c;
          checkImage_ak[(i * 64 + j) * 4 + 1] = c;
          checkImage_ak[(i * 64 + j) * 4 + 2] = c;
          checkImage_ak[(i * 64 + j) * 4 + 3] = 255;
        }
      }
}

function degToRad(degree) {
    return (degree * Math.PI / 180.0);
}

function toggleFullscreen(){

    var fullscreen_element = document.fullscreenElement || 
                             document.webkitFullscreenElement ||
                             document.mozFullScreenElement ||
                             document.msFullscreenElement ||
                             null ;

    if (fullscreen_element == null) {
      bFullscreen_ak = true;

      if (canvas.requestFullscreen) {
        canvas.requestFullscreen();
      } else if (canvas.webkitRequestFullscreen) {
        canvas.webkitRequestFullscreen();
      } else if (canvas.mozRequestFullScreenElement) {
        canvas.mozRequestFullScreenElement();
      } else if (canvas.msRequestFullscreen) {
        canvas.msRequestFullscreen();
      }
    } else {
      bFullscreen_ak = false;

      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      }
    }
}

function keyDown(event){
    switch(event.keyCode){
        case 70:
            toggleFullscreen();
            break;
        
        case 27:
            uninitalize();
            window.close();
            break;
        
        default:
            break;

    }
}

function uninitalize(){

    if(vaoCube_ak){
        gl.deleteVertexArray(vaoCube_ak);
        vaoCube_ak = null;
    }

    if(vboPositionCube_ak){
        gl.deleteBuffer(vboPositionCube_ak);
        vboPositionCube_ak = null;
    }

    if(vboTexCoordCube_ak){
        gl.deleteBuffer(vboTexCoordCube_ak);
        vboTexCoordCube_ak = null;
    }

    if(textureCheckerboard_ak){
        gl.deleteTexture(textureCheckerboard_ak);
        textureCheckerboard_ak = null;
    }

    if(shaderProgramObject_ak){

        if(vertexShaderObject_ak){
            gl.detachShader(shaderProgramObject_ak, vertexShaderObject_ak);
            gl.deleteShader(vertexShaderObject_ak);
            vertexShaderObject_ak = null;
        }

        if(fragmentShaderObject_ak){
            gl.detachShader(shaderProgramObject_ak, fragmentShaderObject_ak);
            gl.deleteShader(fragmentShaderObject_ak);
            fragmentShaderObject_ak = null;
        }

        gl.deleteProgram(shaderProgramObject_ak);
        shaderProgramObject_ak = null;
    }
}
