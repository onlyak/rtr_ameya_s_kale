
var aav_gl = null; 
var aav_bFullscreen = false;
var aav_canvas_orignal_width;
var aav_canvas_orignal_height;

const WebGLMacros = 
{
    AAV_ATTRIBUTE_VERTEX:0,
    AAV_ATTRIBUTE_COLOR:1,
    AAV_ATTRIBUTE_NORMAL:2,
    AAV_ATTRIBUTE_TEXTURE:3
};

var aav_vertexShaderObject;
var aav_fragmentShaderObject;
var aav_shaderProgramObject;

var aav_vao_pyramid;
var aav_vbo_position_pyramid;
var aav_vbo_color_pyramid;
var aav_vbo_texture_pyramid;

var aav_vao_cube;
var aav_vbo_position_cube;
var aav_vbo_color_cube;
var aav_vbo_texture_cube;

var aav_mvpUniform;

var perspectiveProjectionMatrix;

var aav_anglePyramid = 0;
var aav_angleCube = 0;


var aav_requestAnimationFrame = 
window.requestAnimationFrame || 
window.webkitRequestAnimationFrame || 
window.mozRequestAnimationFrame || 
window.oRequestAnimationFrame || 
window.msRequestAnimationFrame;


var aav_cancelAnimationFrame = 
window.aav_cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame || 
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame || 
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;


var aav_stoneTexture ;
var aav_kundaliTexture ;
var aav_textureSamplerUniform;

var aav_texture_fb;
var aav_fbo;
var aav_rbo;


function main()
{
   
    canvas = document.getElementById("ASK");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    aav_canvas_orignal_width = canvas.width;
    aav_canvas_orignal_height = canvas.height;

   
    window.addEventListener("keydown",keyDown, false);
    window.addEventListener("click",mouseDown,false);
    window.addEventListener("resize",resize,false);

   
    init();

   
    resize();
    draw();
}

function toggleFullScreen()
{
   
    var aav_fullscreen_element = 
    document.fullscreenElement || 
    document.webkitFullscreenElement || 
    document.mozFullscreenElement || 
    document.msFullscreenElement || 
    null;

   
    if(aav_fullscreen_element == null)
    {
        if(canvas.requestFullScree)
            canvas.requestFullScree();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();        
        aav_bFullscreen = true;
    }
    else
    {
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        aav_bFullscreen =false;
    }
}

function init()
{
   
   
    aav_gl = canvas.getContext("webgl2");
    if(aav_gl == null)
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
    aav_gl.viewportWidth = canvas.width;
    aav_gl.viewportHeight = canvas.height;

   
    var aav_vertexShaderSourcedCode = 
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;"+
    "in vec4 vColor;"+
    "in vec2 vTexCoord;" +
    "uniform mat4 u_mvp_matrix;"+
    "out vec4 aav_out_color;"+
    "out vec2 aav_out_TexCoord;" +
    "void main(void)"+ 
    "{"+
    "gl_Position = u_mvp_matrix * vPosition;"+
    "aav_out_color = vColor;"+
    "aav_out_TexCoord = vTexCoord; " +
    "}";
    aav_vertexShaderObject = aav_gl.createShader(aav_gl.VERTEX_SHADER);
    aav_gl.shaderSource(aav_vertexShaderObject,aav_vertexShaderSourcedCode);
    aav_gl.compileShader(aav_vertexShaderObject);
    if(aav_gl.getShaderParameter(aav_vertexShaderObject,aav_gl.COMPILE_STATUS)== false)
    {
        var error = aav_gl.getShaderInfoLog(aav_vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

   
    var aav_fragmentShaderSourceCode = 
    "#version 300 es"+
    "\n"+ 
    "precision highp float;"+
    "in vec4 aav_out_color;"+
    "in vec2 aav_out_TexCoord;"+
    "uniform highp sampler2D u_texture_sampler;" +
    "out vec4 FragColor;"+
    "void main(void)"+
    "{"+
    "FragColor = texture(u_texture_sampler,aav_out_TexCoord);"+
    "}";
    aav_fragmentShaderObject = aav_gl.createShader(aav_gl.FRAGMENT_SHADER);
    aav_gl.shaderSource(aav_fragmentShaderObject,aav_fragmentShaderSourceCode);
    aav_gl.compileShader(aav_fragmentShaderObject);
    if(aav_gl.getShaderParameter(aav_fragmentShaderObject,aav_gl.COMPILE_STATUS) == false)
    {
        var error = aav_gl.getShaderInfoLog(aav_fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

   
    aav_shaderProgramObject = aav_gl.createProgram();
    aav_gl.attachShader(aav_shaderProgramObject,aav_vertexShaderObject);
    aav_gl.attachShader(aav_shaderProgramObject,aav_fragmentShaderObject);

   
    aav_gl.bindAttribLocation(aav_shaderProgramObject,WebGLMacros.AAV_ATTRIBUTE_VERTEX,"vPosition");
    aav_gl.bindAttribLocation(aav_shaderProgramObject,WebGLMacros.AAV_ATTRIBUTE_COLOR,"vColor");
    aav_gl.bindAttribLocation(aav_shaderProgramObject,WebGLMacros.AAV_ATTRIBUTE_TEXTURE,"vTexCoord");
   
    aav_gl.linkProgram(aav_shaderProgramObject);
    if(!aav_gl.getProgramParameter(aav_shaderProgramObject,aav_gl.LINK_STATUS))
    {
        var error = aav_gl.getProgramInfoLog(aav_shaderProgramObject);
        if(error.length >  0)
        {
            alert(error);
            uninitialize();
        }
    }

   
    aav_mvpUniform = aav_gl.getUniformLocation(aav_shaderProgramObject,"u_mvp_matrix");
    aav_textureSamplerUniform	 = aav_gl.getUniformLocation(aav_shaderProgramObject, "u_texture_sampler");

   
    var aav_pyramidVerteis = new Float32Array([
        0.0,1.0,0.0,
        -1.0,-1.0,1.0,
        1.0,-1.0,1.0,
        
        0.0,1.0,0.0,
        1.0,-1.0,1.0,
        1.0,-1.0,-1.0,
    
        0.0,1.0,0.0,
        -1.0,-1.0,-1.0,
        1.0,-1.0,-1.0,
    
        0.0,1.0,0.0,
        -1.0,-1.0,1.0,
        -1.0,-1.0,-1.0
    ]);
    
   
    var aav_pyramidTexCoord = new Float32Array([
        0.5, 1.0,
	    0.0, 0.0,
	    1.0, 0.0,

	    0.5, 1.0,
	    1.0, 0.0,
	    0.0, 0.0,

	    0.5, 1.0,
	    1.0, 0.0,
	    0.0, 0.0,

	    0.5, 1.0,
	    0.0, 0.0,
	    1.0, 0.0
    ]);

    var aav_cubeVerteis = new Float32Array([
        1.0,1.0,1.0,
        -1.0,1.0,1.0,
        -1.0,-1.0,1.0,
        1.0,-1.0,1.0,
    
        1.0, 1.0, -1.0,
        1.0, 1.0, 1.0,
        1.0, -1.0, 1.0,
        1.0, -1.0, -1.0,
    
        1.0, 1.0, -1.0,
        -1.0, 1.0, -1.0,
        -1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,
    
        -1.0, 1.0, -1.0,
        -1.0, 1.0, 1.0,
        -1.0, -1.0, 1.0,
        -1.0, -1.0, -1.0,
    
        1.0, 1.0, -1.0,
        -1.0, 1.0, -1.0,
        -1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
    
        1.0, -1.0, -1.0,
        -1.0, -1.0, -1.0,
        -1.0, -1.0, 1.0,
        1.0, -1.0, 1.0
    ]);
    
    

    var aav_cubeTexture = new Float32Array([
        0.0,0.0,
	    1.0,0.0,
	    1.0,1.0,
	    0.0,1.0,

	    0.0, 0.0,
	    1.0, 0.0,
	    1.0, 1.0,
	    0.0, 1.0,

        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,

        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,

        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,

        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0
    ]);


   
    aav_fbo = aav_gl.createFramebuffer();
    aav_gl.bindFramebuffer(aav_gl.FRAMEBUFFER, aav_fbo);

    aav_texture_fb = aav_gl.createTexture();
    aav_gl.bindTexture(aav_gl.TEXTURE_2D, aav_texture_fb);
    aav_gl.texImage2D(aav_gl.TEXTURE_2D, 0, aav_gl.RGB, 3840, 2160, 0, aav_gl.RGB, aav_gl.UNSIGNED_BYTE, null);
    aav_gl.texParameteri(aav_gl.TEXTURE_2D, aav_gl.TEXTURE_MIN_FILTER, aav_gl.LINEAR);
    aav_gl.texParameteri(aav_gl.TEXTURE_2D, aav_gl.TEXTURE_MAG_FILTER, aav_gl.LINEAR);
    aav_gl.framebufferTexture2D(aav_gl.FRAMEBUFFER, aav_gl.COLOR_ATTACHMENT0, aav_gl.TEXTURE_2D, aav_texture_fb, 0);

    aav_rbo = aav_gl.createRenderbuffer();
    aav_gl.bindRenderbuffer(aav_gl.RENDERBUFFER, aav_rbo);
    aav_gl.renderbufferStorage(aav_gl.RENDERBUFFER, aav_gl.DEPTH24_STENCIL8, 3840, 2160);
    aav_gl.bindRenderbuffer(aav_gl.RENDERBUFFER, null);
    aav_gl.framebufferRenderbuffer(aav_gl.FRAMEBUFFER, aav_gl.DEPTH_STENCIL_ATTACHMENT, aav_gl.RENDERBUFFER, aav_rbo);

    if (aav_gl.checkFramebufferStatus(aav_gl.FRAMEBUFFER) == aav_gl.FRAMEBUFFER_COMPLETE)
    {

    }

    aav_gl.bindFramebuffer(aav_gl.FRAMEBUFFER, null);
   
    aav_vao_pyramid = aav_gl.createVertexArray();
    aav_gl.bindVertexArray(aav_vao_pyramid);

   
    aav_vbo_position_pyramid = aav_gl.createBuffer();
    aav_gl.bindBuffer(aav_gl.ARRAY_BUFFER, aav_vbo_position_pyramid);
    aav_gl.bufferData(aav_gl.ARRAY_BUFFER, aav_pyramidVerteis, aav_gl.STATIC_DRAW);
    aav_gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_VERTEX,
                                            3,
                                            aav_gl.FLOAT,
                                            false,
                                            0,0);

    aav_gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_VERTEX);
    aav_gl.bindBuffer(aav_gl.ARRAY_BUFFER,null);


   

   
    aav_vbo_texture_pyramid = aav_gl.createBuffer();

    aav_gl.bindBuffer(aav_gl.ARRAY_BUFFER, aav_vbo_texture_pyramid);
    aav_gl.bufferData(aav_gl.ARRAY_BUFFER, aav_pyramidTexCoord, aav_gl.STATIC_DRAW);
    aav_gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_TEXTURE,
                                                2,
                                                aav_gl.FLOAT,
                                                false,
                                                0,0);
    aav_gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_TEXTURE);

    aav_gl.bindBuffer(aav_gl.ARRAY_BUFFER,null);

    aav_gl.bindVertexArray(null);


   
    aav_vao_cube = aav_gl.createVertexArray();
    aav_gl.bindVertexArray(aav_vao_cube);

   
    aav_vbo_position_cube = aav_gl.createBuffer();
    aav_gl.bindBuffer(aav_gl.ARRAY_BUFFER, aav_vbo_position_cube);
    aav_gl.bufferData(aav_gl.ARRAY_BUFFER, aav_cubeVerteis, aav_gl.STATIC_DRAW);
    aav_gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_VERTEX,
                                            3,
                                            aav_gl.FLOAT,
                                            false,
                                            0,0);

    aav_gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_VERTEX);
    aav_gl.bindBuffer(aav_gl.ARRAY_BUFFER,null);

   
    aav_vbo_texture_pyramid = aav_gl.createBuffer();

    aav_gl.bindBuffer(aav_gl.ARRAY_BUFFER, aav_vbo_texture_pyramid);
    aav_gl.bufferData(aav_gl.ARRAY_BUFFER, aav_cubeTexture, aav_gl.STATIC_DRAW);
    aav_gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_TEXTURE,
                                                2,
                                                aav_gl.FLOAT,
                                                false,
                                                0,0);
    aav_gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_TEXTURE);

    aav_gl.bindBuffer(aav_gl.ARRAY_BUFFER,null);


    aav_gl.bindVertexArray(null);


    aav_stoneTexture  = LoadGLTexture("stone.png");
   
   
    aav_gl.clearColor(0.0,0.0,0.0,1.0);

    aav_gl.enable(aav_gl.DEPTH_TEST);
    aav_gl.depthFunc(aav_gl.LEQUAL);
   
   
    perspectiveProjectionMatrix = mat4.create();
}


function LoadGLTexture(textureFileName)
{
    var aav_texture= aav_gl.createTexture();
    aav_texture.image = new Image();
    aav_texture.image.src = textureFileName;
   
    aav_texture.image.onload = function()
    {
        aav_gl.bindTexture(aav_gl.TEXTURE_2D,aav_texture);
        aav_gl.pixelStorei(aav_gl.UNPACK_FLIP_Y_WEBGL,1);
        aav_gl.texParameteri(aav_gl.TEXTURE_2D, aav_gl.TEXTURE_MAG_FILTER, aav_gl.NEAREST);
        aav_gl.texParameteri(aav_gl.TEXTURE_2D, aav_gl.TEXTURE_MIN_FILTER, aav_gl.NEAREST);
        
        aav_gl.texImage2D(aav_gl.TEXTURE_2D,0,aav_gl.RGBA,aav_gl.RGBA,aav_gl.UNSIGNED_BYTE,
            aav_texture.image);
            aav_gl.bindTexture(aav_gl.TEXTURE_2D,null);
    };
   return aav_texture;
}


function resize()
{
   
    if(aav_bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else
    {
        canvas.width = aav_canvas_orignal_width;
        canvas.height = aav_canvas_orignal_height;
    }

   
    aav_gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);

}

function draw()
{
   
    var aav_modelViewMatrix = mat4.create();
    var aav_transformMatrix = mat4.create();
    var aav_rotationMatrix  = mat4.create(); 
    var aav_modelViewProjectionMatrix = mat4.create();
   

    aav_gl.viewport(0, 0, 3840, 2160);
    aav_gl.bindFramebuffer(aav_gl.FRAMEBUFFER, aav_fbo);
    aav_gl.clearColor(0.0, 0.0, 1.0, 1.0);
    aav_gl.clear(aav_gl.COLOR_BUFFER_BIT | aav_gl.DEPTH_BUFFER_BIT);

    aav_gl.useProgram(aav_shaderProgramObject);

    
    aav_modelViewMatrix = mat4.create();
    aav_transformMatrix = mat4.create();
    aav_rotationMatrix = mat4.create();
    aav_modelViewProjectionMatrix = mat4.create();

    mat4.translate(aav_transformMatrix, aav_transformMatrix, [0.0, 0.0, -4.0]);
    mat4.rotateY(aav_rotationMatrix, aav_rotationMatrix, degToRad(aav_anglePyramid));
    mat4.multiply(aav_modelViewMatrix, aav_transformMatrix, aav_rotationMatrix);
    mat4.multiply(aav_modelViewProjectionMatrix, perspectiveProjectionMatrix, aav_modelViewMatrix);

    aav_gl.uniformMatrix4fv(aav_mvpUniform, false, aav_modelViewProjectionMatrix);

    aav_gl.activeTexture(aav_gl.TEXTURE0);
    aav_gl.bindTexture(aav_gl.TEXTURE_2D, aav_stoneTexture);
    aav_gl.uniform1i(aav_textureSamplerUniform, 0);

    aav_gl.bindVertexArray(aav_vao_pyramid);

    aav_gl.drawArrays(aav_gl.TRIANGLES, 0, 12);

    aav_gl.bindVertexArray(null);

    aav_gl.useProgram(null);

    aav_gl.bindFramebuffer(aav_gl.FRAMEBUFFER, null);


    aav_gl.viewport(0, 0, canvas.width, canvas.height);
    aav_gl.clearColor(0.0, 0.0, 0.0, 1.0);
    aav_gl.clear(aav_gl.COLOR_BUFFER_BIT|  aav_gl.DEPTH_BUFFER_BIT);
    aav_gl.useProgram(aav_shaderProgramObject);

    
   
    aav_modelViewMatrix = mat4.create();
    aav_transformMatrix = mat4.create();
    aav_scaleMatrix = mat4.create();
    aav_rotationMatrix  = mat4.create(); 
    aav_modelViewProjectionMatrix = mat4.create();

    mat4.translate(aav_transformMatrix,aav_transformMatrix,[0.0,0.0,-6.0]);
    mat4.rotateX(aav_rotationMatrix,aav_rotationMatrix,degToRad(aav_angleCube));
    mat4.rotateY(aav_rotationMatrix,aav_rotationMatrix,degToRad(aav_angleCube));
    mat4.rotateZ(aav_rotationMatrix,aav_rotationMatrix,degToRad(aav_angleCube));
    mat4.multiply(aav_modelViewMatrix,aav_scaleMatrix,aav_rotationMatrix);
    mat4.multiply(aav_modelViewMatrix,aav_transformMatrix,aav_modelViewMatrix);
    mat4.multiply(aav_modelViewProjectionMatrix, perspectiveProjectionMatrix, aav_modelViewMatrix);

    aav_gl.uniformMatrix4fv(aav_mvpUniform, false, aav_modelViewProjectionMatrix);

    aav_gl.activeTexture(aav_gl.TEXTURE0);
    aav_gl.bindTexture(aav_gl.TEXTURE_2D, aav_texture_fb);
    aav_gl.uniform1i(aav_textureSamplerUniform, 0);

    aav_gl.bindVertexArray(aav_vao_cube);
    aav_gl.drawArrays(aav_gl.TRIANGLE_FAN,0,4);
    aav_gl.drawArrays(aav_gl.TRIANGLE_FAN,4,4);
    aav_gl.drawArrays(aav_gl.TRIANGLE_FAN,8,4);
    aav_gl.drawArrays(aav_gl.TRIANGLE_FAN,12,4);
    aav_gl.drawArrays(aav_gl.TRIANGLE_FAN,16,4);
    aav_gl.drawArrays(aav_gl.TRIANGLE_FAN,20,4);
   
    aav_gl.bindVertexArray(null);


    aav_gl.useProgram(null);
    
   
    aav_anglePyramid = aav_anglePyramid + 1;
    if(aav_anglePyramid > 360)
    {
       aav_anglePyramid = 0;
    } 
    aav_angleCube = aav_angleCube + 1;
    if(aav_angleCube > 360)
    {
        aav_angleCube = 0;
    } 
  
    requestAnimationFrame(draw, canvas);
}


function degToRad(degree)
{
    return(degree * Math.PI/180.0);
}


function uninitialize()
{
   
    if(aav_vao_pyramid)
    {
        aav_gl.deleteVertexArary(aav_vao_pyramid);
        aav_vao_pyramid = null;
    }

    if(aav_vbo_position_pyramid)
    {
        aav_gl.deleteBuffer(aav_vbo_position_pyramid);
        aav_vbo_position_pyramid = null;
    }

    if(aav_vbo_color_pyramid)
    {
        aav_gl.deleteBuffer(aav_vbo_color_pyramid);
        aav_vbo_color_pyramid = null;
    }
    
    if(aav_vbo_texture_pyramid)
    {
        aav_gl.deleteBuffer(aav_vbo_texture_pyramid);
        aav_vbo_texture_pyramid = null;
    }

    if(aav_vao_cube)
    {
        aav_gl.deleteVertexArary(aav_vao_cube);
        aav_vao_cube = null;
    }

    if(aav_vbo_position_cube)
    {
        aav_gl.deleteBuffer(aav_vbo_position_cube);
        aav_vbo_position_cube = null;
    }

    if(aav_vbo_color_cube)
    {
        aav_gl.deleteBuffer(aav_vbo_color_cube);
        aav_vbo_color_cube = null;
    }

    if(aav_vbo_texture_cube)
    {
        aav_gl.deleteBuffer(aav_vbo_texture_cube);
        aav_vbo_texture_cube = null;
    }

	if (aav_stoneTexture)
	{
        aav_gl.deleteTextures(1, aav_stoneTexture);
	}

	if (aav_kundaliTexture)
	{
        aav_gl.deleteTextures(1, aav_kundaliTexture);
	}


    if(aav_shaderProgramObject)
    {
        if(aav_fragmentShaderObject)
        {
            aav_gl.detachShader(aav_shaderProgramObject,aav_fragmentShaderObject);
            aav_gl.deleteShader(aav_fragmentShaderObject);
            aav_fragmentShaderObject = null;
        }

        if(aav_vertexShaderObject)
        {
            aav_gl.detachShader(aav_shaderProgramObject,aav_vertexShaderObject);
            aav_gl.deleteShader(aav_vertexShaderObject);
            aav_vertexShaderObject = null;
        }

        aav_gl.deleteProgram(aav_shaderProgramObject);
        aav_shaderProgramObject = null;
    }
}

function keyDown(event)
{
   
    switch(event.keyCode)
    {
        case 27:
           
            uninitialize();
           
            window.close();
            break;
        case 70:
            toggleFullScreen();
            break;
    }
}

function mouseDown()
{
   
}