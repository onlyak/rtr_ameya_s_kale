var canvas = null;
var gl = null;
var canvas_original_width;
var canvas_original_height;
var bFullscreen_ak = false;

const WebGLMacros = {
    ATTRIBUTE_VERTEX: 0,
    ATTRIBUTE_COLOR: 1,
    ATTRIBUTE_NORMAL: 2,
    ATTRIBUTE_TEXTURE0: 3,
};

var vertexShaderObject_ak;
var fragmentShaderObject_ak;
var shaderProgramObject_ak;

var vao_ak;
var vboPosition_ak;
var vboColor_ak;
var mvpUniform_ak;

var perspectiveProjectionMatrix_ak;

var requestAnimationFrame = window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame  ||
                            window.mozRequestAnimationFrame ||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var cancelAnimationFrame = window.cancelAnimationFrame || 
                           window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
                           window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
                           window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
                           window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

function main(){

    canvas = document.getElementById("ASK");
    if(!canvas){
        console.log("Obtaining Canvas Failed");
    }
    else{
        // console.log("Obtaining Canvas Succeeded");
    }

    canvas_original_width = canvas.width;    
    canvas_original_height = canvas.height;
    
    // window is built-in variable
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("resize", resize, false);

    init();

    resize(); // warmup resize call

    draw(); // warmup re-paint call

}

function init(){
    gl = canvas.getContext("webgl2");
    if(!gl){
        console.log("Obtaining webgl2 Context Failed");
    }
    else{
        console.log("Obtaining webgl2 Context Succeeded");
    }
    
    gl.viewportWidth  = canvas.width;
    gl.viewportHeight  = canvas.height;

    //Vertex Shader
    var vertexShaderSourceCode = 
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;" +
    "out vec4 out_color;" +
    "in vec4 vColor;" +
    "uniform mat4 u_mvp_matrix;" +
    "void main()" +
    "{" +
          "gl_Position = u_mvp_matrix * vPosition;" +
          "out_color = vColor;"+
          "gl_PointSize = 1.0;"+
    "}";

    vertexShaderObject_ak = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject_ak, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject_ak);

    if(gl.getShaderParameter(vertexShaderObject_ak, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(vertexShaderObject_ak);
            if(error.length > 0){
                alert(error);
            }
    }

    //Fragment Shader
    var fragmentShaderSourceCode = 
    "#version 300 es"+
    "\n"+
    "precision highp float;" +
    "out vec4 FragColor;" +
    "in vec4 out_color;" +
    "void main()"+
    "{" +
        "FragColor = out_color;" +
    "}";

    fragmentShaderObject_ak = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject_ak, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject_ak);

    if(gl.getShaderParameter(fragmentShaderObject_ak, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(fragmentShaderObject_ak);
            if(error.length > 0){
                alert(error);
                uninitalize();
            }
    }

    //Shader Program
    shaderProgramObject_ak = gl.createProgram();
    gl.attachShader(shaderProgramObject_ak, vertexShaderObject_ak);
    gl.attachShader(shaderProgramObject_ak, fragmentShaderObject_ak);

    gl.bindAttribLocation(shaderProgramObject_ak, WebGLMacros.ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject_ak, WebGLMacros.ATTRIBUTE_COLOR, "vColor");

    //linking
    gl.linkProgram(shaderProgramObject_ak);

    if(gl.getProgramParameter(shaderProgramObject_ak, gl.LINK_STATUS) == false){
        var error = gl.getProgramInfoLog(shaderProgramObject_ak);
            if(error.length > 0){
                alert(error);
                uninitalize();
            }
    }

    //uniforms
    mvpUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_mvp_matrix");

    //vao
    vao_ak = gl.createVertexArray();
    gl.bindVertexArray(vao_ak);

    //vbo position
    vboPosition_ak = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboPosition_ak);
    gl.bufferData(gl.ARRAY_BUFFER, null, gl.DYNAMIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    //vbo color
    vboColor_ak = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboColor_ak);
    gl.bufferData(gl.ARRAY_BUFFER, null, gl.DYNAMIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    perspectiveProjectionMatrix_ak = mat4.create();
}

function resize(){
    if (bFullscreen_ak == true){
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else{
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    gl.viewport(0, 0, canvas.width,canvas.height);

    //perspective call
    mat4.perspective(perspectiveProjectionMatrix_ak, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
    
}

function draw(){
    gl.clear(gl.COLOR_BUFFER_BIT |  gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObject_ak);

    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -3.0]);

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix_ak, modelViewMatrix );

    gl.uniformMatrix4fv(mvpUniform_ak, false, modelViewProjectionMatrix);

    gl.bindVertexArray(vao_ak);

    graph();
    triangle(0.5);
    inCircle(0.0, 0.5, 0.0, -0.5, -0.5, 0.0, 0.5, -0.5, 0.0);
    rectangle(1.0, 1.0);
    outerCircle(1.0, 1.0);

    gl.bindVertexArray(null);

    gl.useProgram(null);

    requestAnimationFrame(draw, canvas);
}

function graph(){
    var y_axis_ak = 1.0;
    var x_axis_ak = 1.0;

    var lineVertices = new Float32Array([
    y_axis_ak, 1.0, 0.0, 
    y_axis_ak, -1.0, 0.0
    ]);
   
    var lineColor = new Float32Array([
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0
    ]);

for (y_axis_ak = 1.0; y_axis_ak > -1.01; y_axis_ak -= 0.05)
	{

		lineVertices[0] = y_axis_ak;
		lineVertices[3] = y_axis_ak;

		gl.bindBuffer(gl.ARRAY_BUFFER, vboPosition_ak);
		gl.bufferData(gl.ARRAY_BUFFER, lineVertices, gl.DYNAMIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		gl.bindBuffer(gl.ARRAY_BUFFER, vboColor_ak);
		gl.bufferData(gl.ARRAY_BUFFER, lineColor, gl.DYNAMIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		gl.drawArrays(gl.LINES, 0, 2);
	}

	//horizontal lines
	for (x_axis_ak = 1.0; x_axis_ak > -1.01; x_axis_ak -= 0.05)
	{

		lineVertices[0] = 1.0;
		lineVertices[1] = x_axis_ak;
		lineVertices[3] = -1.0;
		lineVertices[4] = x_axis_ak;

		gl.bindBuffer(gl.ARRAY_BUFFER, vboPosition_ak);
		gl.bufferData(gl.ARRAY_BUFFER, lineVertices, gl.DYNAMIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		gl.bindBuffer(gl.ARRAY_BUFFER, vboColor_ak);
		gl.bufferData(gl.ARRAY_BUFFER, lineColor, gl.DYNAMIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		gl.drawArrays(gl.LINES, 0, 2);
	}

	//green vertical line

	lineVertices[0] = 0.0;
	lineVertices[1] = 1.0;
	lineVertices[2] = 0.0;
	lineVertices[3] = 0.0;
	lineVertices[4] = -1.0;
	lineVertices[5] = 0.0;
		
	lineColor[0] = 0.0;
	lineColor[1] = 1.0;
	lineColor[2] = 0.0;		
	lineColor[3] = 0.0;
	lineColor[4] = 1.0;
	lineColor[5] = 0.0;

		
	gl.bindBuffer(gl.ARRAY_BUFFER, vboPosition_ak);
	gl.bufferData(gl.ARRAY_BUFFER, lineVertices, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindBuffer(gl.ARRAY_BUFFER, vboColor_ak);
	gl.bufferData(gl.ARRAY_BUFFER, lineColor, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.drawArrays(gl.LINES, 0, 2);

	//red horizontal line

	lineVertices[0] = 1.0;
	lineVertices[1] = 0.0;
	lineVertices[2] = 0.0;
	lineVertices[3] = -1.0;
	lineVertices[4] = 0.0;
	lineVertices[5] = 0.0;

	lineColor[0] = 1.0;
	lineColor[1] = 0.0;
	lineColor[2] = 0.0;
	lineColor[3] = 1.0;
	lineColor[4] = 0.0;
	lineColor[5] = 0.0;


	gl.bindBuffer(gl.ARRAY_BUFFER, vboPosition_ak);
	gl.bufferData(gl.ARRAY_BUFFER, lineVertices, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindBuffer(gl.ARRAY_BUFFER, vboColor_ak);
	gl.bufferData(gl.ARRAY_BUFFER, lineColor, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.drawArrays(gl.LINES, 0, 2);
}

function triangle(value){
    var lineVertices = new Float32Array([
        1.0, 1.0, 0.0,
        1.0, -1.0, 0.0
        ]);
        var lineColor = new Float32Array([
            0.0, 0.0, 1.0,
            0.0, 0.0, 1.0
        ]);
    
        lineVertices[0] = 0.0;
        lineVertices[1] = value;
        lineVertices[2] = 0.0;
        lineVertices[3] = -value;
        lineVertices[4] = -value;
        lineVertices[5] = 0.0;
    
        lineColor[0] = 1.0;
        lineColor[1] = 1.0;
        lineColor[2] = 0.0;
        lineColor[3] = 1.0;
        lineColor[4] = 1.0;
        lineColor[5] = 0.0;
    
        gl.bindBuffer(gl.ARRAY_BUFFER, vboPosition_ak);
        gl.bufferData(gl.ARRAY_BUFFER, lineVertices, gl.DYNAMIC_DRAW);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
    
        gl.bindBuffer(gl.ARRAY_BUFFER, vboColor_ak);
        gl.bufferData(gl.ARRAY_BUFFER, lineColor, gl.DYNAMIC_DRAW);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
    
        gl.drawArrays(gl.LINES, 0, 2);
    
        lineVertices[0] = -value;
        lineVertices[1] = -value;
        lineVertices[2] = 0.0;
        lineVertices[3] = value;
        lineVertices[4] = -value;
        lineVertices[5] = 0.0;
    
        lineColor[0] = 1.0;
        lineColor[1] = 1.0;
        lineColor[2] = 0.0;
        lineColor[3] = 1.0;
        lineColor[4] = 1.0;
        lineColor[5] = 0.0;
    
        gl.bindBuffer(gl.ARRAY_BUFFER, vboPosition_ak);
        gl.bufferData(gl.ARRAY_BUFFER, lineVertices, gl.DYNAMIC_DRAW);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
    
        gl.bindBuffer(gl.ARRAY_BUFFER, vboColor_ak);
        gl.bufferData(gl.ARRAY_BUFFER, lineColor, gl.DYNAMIC_DRAW);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
    
        gl.drawArrays(gl.LINES, 0, 2);
    
        lineVertices[0] = value;
        lineVertices[1] = -value;
        lineVertices[2] = 0.0;
        lineVertices[3] = 0.0;
        lineVertices[4] = value;
        lineVertices[5] = 0.0;
    
        lineColor[0] = 1.0;
        lineColor[1] = 1.0;
        lineColor[2] = 0.0;
        lineColor[3] = 1.0;
        lineColor[4] = 1.0;
        lineColor[5] = 0.0;
    
        gl.bindBuffer(gl.ARRAY_BUFFER, vboPosition_ak);
        gl.bufferData(gl.ARRAY_BUFFER, lineVertices, gl.DYNAMIC_DRAW);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
    
        gl.bindBuffer(gl.ARRAY_BUFFER, vboColor_ak);
        gl.bufferData(gl.ARRAY_BUFFER, lineColor, gl.DYNAMIC_DRAW);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
    
        gl.drawArrays(gl.LINES, 0, 2);
}

function inCircle( x1_ak,  y1_ak,  z1_ak,  x2_ak,  y2_ak,  z2,  x3_ak,  y3_ak,  z3_ak){
    var lineVertices = new Float32Array([
		0.0, 0.0, 0.0
    ]);
	var lineColor = new Float32Array([
		1.0, 1.0, 0.0
    ]);

	var count_ak = 0.0;
	var angle_ak = 0.0;

	var dist_a_b_ak = Math.sqrt((x2_ak - x1_ak) * (x2_ak - x1_ak) + (y2_ak - y1_ak) * (y2_ak - y1_ak) + (z2 - z1_ak) * (z2 - z1_ak));
	var dist_b_c_ak = Math.sqrt((x3_ak - x2_ak) * (x3_ak - x2_ak) + (y3_ak - y2_ak) * (y3_ak - y2_ak) + (z3_ak - z2) * (z3_ak - z2));
	var dist_c_a_ak = Math.sqrt((x1_ak - x3_ak) * (x1_ak - x3_ak) + (y1_ak - y3_ak) * (y1_ak - y3_ak) + (z1_ak - z3_ak) * (z1_ak - z3_ak));

	var semiperimeter_ak = (dist_a_b_ak + dist_b_c_ak + dist_c_a_ak) / 2;
	var radius_ak = Math.sqrt((semiperimeter_ak - dist_a_b_ak) * (semiperimeter_ak - dist_b_c_ak) * (semiperimeter_ak - dist_c_a_ak) / semiperimeter_ak);
	var Ox_ak = (x3_ak * dist_a_b_ak + x1_ak * dist_b_c_ak + x2_ak * dist_c_a_ak) / (semiperimeter_ak * 2);
	var Oy_ak = (y3_ak * dist_a_b_ak + y1_ak * dist_b_c_ak + y2_ak * dist_c_a_ak) / (semiperimeter_ak * 2);
	var Oz_ak = (z3_ak * dist_a_b_ak + z1_ak * dist_b_c_ak + z2 * dist_c_a_ak) / (semiperimeter_ak * 2);

	for (count_ak = 0; count_ak <= 2000; count_ak++)
	{

		lineVertices[0] = Math.cos(angle_ak) * radius_ak + Ox_ak;
		lineVertices[1] = Math.sin(angle_ak) * radius_ak + Oy_ak;
		lineVertices[2] = 0.0 + Oz_ak;

		angle_ak = 2 * Math.PI * count_ak / 2000;

		gl.bindBuffer(gl.ARRAY_BUFFER, vboPosition_ak);
		gl.bufferData(gl.ARRAY_BUFFER, lineVertices, gl.DYNAMIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		gl.bindBuffer(gl.ARRAY_BUFFER, vboColor_ak);
		gl.bufferData(gl.ARRAY_BUFFER, lineColor, gl.DYNAMIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		gl.drawArrays(gl.POINTS, 0, 1);

	}
}

function rectangle(width_ak, height_ak){
    var lineVertices = new Float32Array(24);
	var lineColor  = new Float32Array([
		1.0, 1.0, 0.0,
		1.0, 1.0, 0.0,
		1.0, 1.0, 0.0,
		1.0, 1.0, 0.0,
		1.0, 1.0, 0.0,
		1.0, 1.0, 0.0,
		1.0, 1.0, 0.0,
		1.0, 1.0, 0.0
    ]);

	lineVertices[0] = width_ak / 2;
	lineVertices[1] = height_ak / 2;
	lineVertices[2] = 0.0;
	lineVertices[3] = -width_ak / 2;
	lineVertices[4] = height_ak / 2;
	lineVertices[5] = 0.0;
	lineVertices[6] = -width_ak / 2;
	lineVertices[7] = height_ak / 2;
	lineVertices[8] = 0.0;
	lineVertices[9] = -width_ak / 2;
	lineVertices[10] = -height_ak / 2;
	lineVertices[11] = 0.0;
	lineVertices[12] = -width_ak / 2;
	lineVertices[13] = -height_ak / 2;
	lineVertices[14] = 0.0;
	lineVertices[15] = width_ak / 2;
	lineVertices[16] = -height_ak / 2;
	lineVertices[17] = 0.0;
	lineVertices[18] = width_ak / 2;
	lineVertices[19] = -height_ak / 2;
	lineVertices[20] = 0.0;
	lineVertices[21] = width_ak / 2;
	lineVertices[22] = height_ak / 2;
	lineVertices[23] = 0.0;


    gl.bindBuffer(gl.ARRAY_BUFFER, vboPosition_ak);
    gl.bufferData(gl.ARRAY_BUFFER, lineVertices, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindBuffer(gl.ARRAY_BUFFER, vboColor_ak);
    gl.bufferData(gl.ARRAY_BUFFER, lineColor, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.drawArrays(gl.LINES, 0, 8);
}

function outerCircle(width_ak, height_ak){
        var lineVertices  = new Float32Array([
        0.0, 0.0, 0.0
        ]);
        var lineColor = new Float32Array([
            1.0, 1.0, 0.0,
        ]);
    
        var count_ak = 0.0;
        var angle_ak = 0.0;
    
        var radius_ak = Math.sqrt(width_ak / 2 * width_ak / 2 + height_ak / 2 * height_ak / 2);
    
        for (count_ak = 0; count_ak <= 2000; count_ak++)
        {
    
            lineVertices[0] = Math.cos(angle_ak) * radius_ak;
            lineVertices[1] = Math.sin(angle_ak) * radius_ak;
            lineVertices[2] = 0.0;
    
            angle_ak = 2 * Math.PI * count_ak / 2000;
    
            gl.bindBuffer(gl.ARRAY_BUFFER, vboPosition_ak);
            gl.bufferData(gl.ARRAY_BUFFER, lineVertices, gl.DYNAMIC_DRAW);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);
    
            gl.bindBuffer(gl.ARRAY_BUFFER, vboColor_ak);
            gl.bufferData(gl.ARRAY_BUFFER, lineColor, gl.DYNAMIC_DRAW);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);
    
            gl.drawArrays(gl.POINTS, 0, 1);
        }
}

function toggleFullscreen(){

    var fullscreen_element = document.fullscreenElement || 
                             document.webkitFullscreenElement ||
                             document.mozFullScreenElement ||
                             document.msFullscreenElement ||
                             null ;

    if (fullscreen_element == null) {
      bFullscreen_ak = true;

      if (canvas.requestFullscreen) {
        canvas.requestFullscreen();
      } else if (canvas.webkitRequestFullscreen) {
        canvas.webkitRequestFullscreen();
      } else if (canvas.mozRequestFullScreenElement) {
        canvas.mozRequestFullScreenElement();
      } else if (canvas.msRequestFullscreen) {
        canvas.msRequestFullscreen();
      }
    } else {
      bFullscreen_ak = false;

      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      }
    }
}

function keyDown(event){
    switch(event.keyCode){
        case 70:
            toggleFullscreen();
            break;
        
        case 27:
            uninitalize();
            window.close();
            break;
    }
}

function uninitalize(){

    if(vao_ak){
        gl.deleteVertexArray(vao_ak);
        vao_ak = null;
    }

    if(vboPosition_ak){
        gl.deleteBuffer(vboPosition_ak);
        vboPosition_ak= null;
    }

    if(vboColor_ak){
        gl.deleteBuffer(vboColor_ak);
        vboColor_ak= null;
    }

    if(shaderProgramObject_ak){

        if(vertexShaderObject_ak){
            gl.detachShader(shaderProgramObject_ak, vertexShaderObject_ak);
            gl.deleteShader(vertexShaderObject_ak);
            vertexShaderObject_ak = null;
        }

        if(fragmentShaderObject_ak){
            gl.detachShader(shaderProgramObject_ak, fragmentShaderObject_ak);
            gl.deleteShader(fragmentShaderObject_ak);
            fragmentShaderObject_ak = null;
        }

        gl.deleteProgram(shaderProgramObject_ak);
        shaderProgramObject_ak = null;
    }
}
