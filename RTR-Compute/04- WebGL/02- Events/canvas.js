function main(){

    var canvas = document.getElementById("ASK");
    if(!canvas){
        console.log("Obtaining Canvas Failed");
    }
    else{
        console.log("Obtaining Canvas Succeeded");
    }

    console.log("Canvas Width = "+canvas.width+ " And Canvas Height = "+canvas.height);
    
    var context = canvas.getContext("2d");
    if(!context){
        console.log("Obtaining Context Failed");
    }
    else{
        console.log("Obtaining Context Succeeded");
    }

    context.fillStyle = "black";
    context.fillRect(0,0, canvas.width, canvas.height);
    context.textAlign = "center";
    context.font = "48px sans-serif";   

    var str = "Hello World";

    context.fillStyle = "white";
    context.fillText(str, canvas.width/2, canvas.height/2);

    // window is built-in variable
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);

}

function keyDown(event){
    alert("Key is pressed");
}

function mouseDown(){
    alert("Mouse is clicked.");
}
