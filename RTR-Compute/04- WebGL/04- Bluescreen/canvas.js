var canvas = null;
var gl = null;
var canvas_original_width;
var canvas_original_height;
var bFullscreen = false;

var requestAnimationFrame = window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame  ||
                            window.mozRequestAnimationFrame ||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var cancelAnimationFrame = window.cancelAnimationFrame || 
                           window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
                           window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
                           window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
                           window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;
function main(){

    canvas = document.getElementById("ASK");
    if(!canvas){
        console.log("Obtaining Canvas Failed");
    }
    else{
        console.log("Obtaining Canvas Succeeded");
    }

    canvas_original_width = canvas.width;    
    canvas_original_height = canvas.height;
    
    // window is built-in variable
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    init();

    resize(); // warmup resize call

    draw(); // warmup re-paint call

}

function init(){
    gl = canvas.getContext("webgl2");
    if(!gl){
        console.log("Obtaining webgl2 Context Failed");
    }
    else{
        console.log("Obtaining webgl2 Context Succeeded");
    }
    
    gl.viewportWidth  = canvas.width;
    gl.viewportHeight  = canvas.height;

    gl.clearColor(0.0, 0.0, 1.0, 1.0);
}

function resize(){
    if (bFullscreen == true){
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else{
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    gl.viewport(0, 0, canvas.width,canvas.height);
}

function draw(){
    gl.clear(gl.COLOR_BUFFER_BIT);
    requestAnimationFrame(draw, canvas);
}

function toggleFullscreen(){

    var fullscreen_element = document.fullscreenElement || 
                             document.webkitFullscreenElement ||
                             document.mozFullScreenElement ||
                             document.msFullscreenElement ||
                             null ;

    if(fullscreen_element == null){

        bFullscreen = true;

        if (canvas.requestFullscreen){
            canvas.requestFullscreen();
        }
        else if(canvas.webkitRequestFullscreen){
            canvas.webkitRequestFullscreen();
        }
        else if(canvas.mozRequestFullScreenElement){
            canvas.mozRequestFullScreenElement();
        }
        else if(canvas.msRequestFullscreen){
            canvas.msRequestFullscreen();
        }
    }
    else{

        bFullscreen = false;

        if(document.exitFullscreen){
            document.exitFullscreen();
        }
        else if(document.webkitExitFullscreen){
            document.webkitExitFullscreen();
        }
        else if(document.mozCancelFullScreen){
            document.mozCancelFullScreen();
        }
        else if(document.msExitFullscreen){
            document.msExitFullscreen();
        }
    }
}

function keyDown(event){
    switch(event.keyCode){
        case 70:
            toggleFullscreen();
            break;
    }
}

function mouseDown(){}
