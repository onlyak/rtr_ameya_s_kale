var canvas = null;
var gl = null;
var canvas_original_width;
var canvas_original_height;
var bFullscreen_ak = false;

const WebGLMacros = {
    ATTRIBUTE_VERTEX: 0,
    ATTRIBUTE_COLOR: 1,
    ATTRIBUTE_NORMAL: 2,
    ATTRIBUTE_TEXTURE0: 3,
};

var vertexShaderObject_ak;
var fragmentShaderObject_ak;
var shaderProgramObject_ak;

var vaoPyramid_ak;
var vaoCube_ak;

var vboPositionCube_ak;
var vboTexCoordCube_ak;

var vboPositionPyramid_ak;
var vboTexCoordPyramid_ak;

var mvpUniform_ak;

var perspectiveProjectionMatrix_ak;

var textureSamplerUniform_ak;
var textureStone_ak;
var textureKundali_ak;

var anglePyramid_ak = 0.0;
var angleCube_ak = 0.0;

var requestAnimationFrame = window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame  ||
                            window.mozRequestAnimationFrame ||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var cancelAnimationFrame = window.cancelAnimationFrame || 
                           window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
                           window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
                           window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
                           window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

function main(){

    canvas = document.getElementById("ASK");
    if(!canvas){
        console.log("Obtaining Canvas Failed");
    }
    else{
        // console.log("Obtaining Canvas Succeeded");
    }

    canvas_original_width = canvas.width;    
    canvas_original_height = canvas.height;
    
    // window is built-in variable
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("resize", resize, false);

    init();

    resize(); // warmup resize call

    draw(); // warmup re-paint call

}

function init(){
    gl = canvas.getContext("webgl2");
    if(!gl){
        console.log("Obtaining webgl2 Context Failed");
    }
    else{
        console.log("Obtaining webgl2 Context Succeeded");
    }
    
    gl.viewportWidth  = canvas.width;
    gl.viewportHeight  = canvas.height;

    //Vertex Shader
    var vertexShaderSourceCode = 
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;" +
    "in vec2 vTexCoord;" +
    "out vec2 out_texCoord;" +
    "uniform mat4 u_mvp_matrix;" +
    "void main()" +
    "{" +
          "gl_Position = u_mvp_matrix * vPosition;" +
          "out_texCoord = vTexCoord;"+
    "}";

    vertexShaderObject_ak = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject_ak, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject_ak);

    if(gl.getShaderParameter(vertexShaderObject_ak, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(vertexShaderObject_ak);
            if(error.length > 0){
                alert(error);
            }
    }

    //Fragment Shader
    var fragmentShaderSourceCode = 
    "#version 300 es"+
    "\n"+
    "precision highp float;" +
    "out vec4 FragColor;" +
    "in vec2 out_texCoord;" +
    "uniform highp sampler2D u_texture_sampler;"+
    "void main()"+
    "{" +
        "FragColor = texture(u_texture_sampler, out_texCoord);" +
    "}";

    fragmentShaderObject_ak = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject_ak, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject_ak);

    if(gl.getShaderParameter(fragmentShaderObject_ak, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(fragmentShaderObject_ak);
            if(error.length > 0){
                alert(error);
                uninitalize();
            }
    }

    //Shader Program
    shaderProgramObject_ak = gl.createProgram();
    gl.attachShader(shaderProgramObject_ak, vertexShaderObject_ak);
    gl.attachShader(shaderProgramObject_ak, fragmentShaderObject_ak);

    gl.bindAttribLocation(shaderProgramObject_ak, WebGLMacros.ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject_ak, WebGLMacros.ATTRIBUTE_TEXTURE0, "vTexCoord");

    //linking
    gl.linkProgram(shaderProgramObject_ak);

    if(gl.getProgramParameter(shaderProgramObject_ak, gl.LINK_STATUS) == false){
        var error = gl.getProgramInfoLog(shaderProgramObject_ak);
            if(error.length > 0){
                alert(error);
                uninitalize();
            }
    }

    //uniforms
    mvpUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_mvp_matrix");
    textureSamplerUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_texture_sampler");

    textureStone_ak = gl.createTexture();
    textureStone_ak.image = new Image();
    textureStone_ak.image.src = "stone.png"
    textureStone_ak.image.onload = function () {
        gl.bindTexture(gl.TEXTURE_2D, textureStone_ak);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, textureStone_ak.image);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.bindTexture(gl.TEXTURE_2D, null);
    }

    var pyramidVertices = new Float32Array([
		0.0, 1.0, 0.0,
		-1.0, -1.0, 1.0,
		1.0, -1.0, 1.0,

		0.0, 1.0, 0.0,
		1.0, -1.0, 1.0,
		1.0, -1.0, -1.0,

		0.0, 1.0, 0.0,
		1.0, -1.0, -1.0,
		-1.0, -1.0, -1.0,

		0.0, 1.0, 0.0,
		-1.0, -1.0, -1.0,
		-1.0, -1.0, 1.0
    ]);

    var pyramidTexCoord = new Float32Array([
        0.5, 1.0,
		0.0, 0.0,
		1.0, 0.0,
		0.5, 1.0,
		1.0, 0.0,
		0.0, 0.0,
		0.5, 1.0,
		1.0, 0.0,
		0.0, 0.0,
		0.5, 1.0,
		1.0, 0.0,
		0.0, 0.0,
		0.5, 1.0,
		0.0, 0.0,
		1.0, 0.0	
    ]);

    textureKundali_ak = gl.createTexture();
    textureKundali_ak.image = new Image();
    textureKundali_ak.image.src = "Vijay_Kundali.png"
    textureKundali_ak.image.onload = function () {
        gl.bindTexture(gl.TEXTURE_2D, textureKundali_ak);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, textureKundali_ak.image);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.bindTexture(gl.TEXTURE_2D, null);

    }

    var cubeVertices = new Float32Array([
		1.0, 1.0, -1.0,
		-1.0, 1.0, -1.0,
		-1.0, 1.0, 1.0,
		1.0, 1.0, 1.0,

		1.0, -1.0, -1.0,
		-1.0, -1.0, -1.0,
		-1.0, -1.0, 1.0,
		1.0, -1.0, 1.0,

		1.0, 1.0, 1.0,
		-1.0, 1.0, 1.0,
		-1.0, -1.0, 1.0,
		1.0, -1.0, 1.0,

		1.0, 1.0, -1.0,													
		-1.0, 1.0, -1.0,		
		-1.0, -1.0, -1.0, 
		1.0, -1.0, -1.0,

		1.0, 1.0, -1.0,
		1.0, 1.0, 1.0,
		1.0, -1.0, 1.0,
		1.0, -1.0, -1.0,

		-1.0, 1.0, 1.0,
		-1.0, 1.0, -1.0,
		-1.0, -1.0, -1.0,
		-1.0, -1.0, 1.0,	
    ]);

    var cubeTexCoord = new Float32Array([
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0
    ]);

    //vao pyramid
    vaoPyramid_ak = gl.createVertexArray();
    gl.bindVertexArray(vaoPyramid_ak);

    //vbo position 
    vboPositionPyramid_ak = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboPositionPyramid_ak);
    gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    //vbo texcoord 
    vboTexCoordPyramid_ak = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboTexCoordPyramid_ak);
    gl.bufferData(gl.ARRAY_BUFFER, pyramidTexCoord, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.ATTRIBUTE_TEXTURE0);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    //vao cube
    vaoCube_ak = gl.createVertexArray();
    gl.bindVertexArray(vaoCube_ak);

    //vbo position 
    vboPositionCube_ak = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboPositionCube_ak);
    gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    //vbo texcoord 
    vboTexCoordCube_ak = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboTexCoordCube_ak);
    gl.bufferData(gl.ARRAY_BUFFER, cubeTexCoord, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.ATTRIBUTE_TEXTURE0);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
    gl.clearDepth(1.0);


    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    perspectiveProjectionMatrix_ak = mat4.create();
}

function resize(){
    if (bFullscreen_ak == true){
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else{
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    gl.viewport(0, 0, canvas.width,canvas.height);

    //perspective call
    mat4.perspective(perspectiveProjectionMatrix_ak, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
    
}

function draw(){
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObject_ak);

    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [-1.5, 0.0, -5.0]);
     
    mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(anglePyramid_ak));

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix_ak, modelViewMatrix );

    gl.uniformMatrix4fv(mvpUniform_ak, false, modelViewProjectionMatrix);

    gl.bindTexture(gl.TEXTURE_2D, textureStone_ak);
    gl.uniform1i(textureSamplerUniform_ak, 0);

    gl.bindVertexArray(vaoPyramid_ak);

    gl.drawArrays(gl.TRIANGLES, 0, 12);

    gl.bindVertexArray(null);

    modelViewMatrix = mat4.create();
    modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [1.5, 0.0, -5.0]);

    mat4.scale(modelViewMatrix, modelViewMatrix, [0.75, 0.75, 0.75]);

    mat4.rotateX(modelViewMatrix, modelViewMatrix, degToRad(angleCube_ak));
    mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angleCube_ak));
    mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(angleCube_ak));

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix_ak, modelViewMatrix );

    gl.uniformMatrix4fv(mvpUniform_ak, false, modelViewProjectionMatrix);

    gl.bindTexture(gl.TEXTURE_2D, textureKundali_ak);
    gl.uniform1i(textureSamplerUniform_ak, 0);

    gl.bindVertexArray(vaoCube_ak);

    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);

    gl.bindVertexArray(null);

    gl.useProgram(null);

    update();

    requestAnimationFrame(draw, canvas);
}

function update(){

    anglePyramid_ak += 0.5;
    if(anglePyramid_ak >= 360)
        anglePyramid_ak = 0.0

    angleCube_ak += 0.5;
    if(angleCube_ak >= 360)
        angleCube_ak = 0.0
}

function degToRad(degree) {
    return (degree * Math.PI / 180.0);
}

function toggleFullscreen(){

    var fullscreen_element = document.fullscreenElement || 
                             document.webkitFullscreenElement ||
                             document.mozFullScreenElement ||
                             document.msFullscreenElement ||
                             null ;

    if (fullscreen_element == null) {
      bFullscreen_ak = true;

      if (canvas.requestFullscreen) {
        canvas.requestFullscreen();
      } else if (canvas.webkitRequestFullscreen) {
        canvas.webkitRequestFullscreen();
      } else if (canvas.mozRequestFullScreenElement) {
        canvas.mozRequestFullScreenElement();
      } else if (canvas.msRequestFullscreen) {
        canvas.msRequestFullscreen();
      }
    } else {
      bFullscreen_ak = false;

      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      }
    }
}

function keyDown(event){
    switch(event.keyCode){
        case 70:
            toggleFullscreen();
            break;
        
        case 27:
            uninitalize();
            window.close();
            break;
    }
}

function uninitalize(){

    if(vaoCube_ak){
        gl.deleteVertexArray(vaoCube_ak);
        vaoCube_ak = null;
    }

    if(vboPositionPyramid_ak){
        gl.deleteBuffer(vboPositionPyramid_ak);
        vboPositionPyramid_ak = null;
    }

    if(vboTexCoordPyramid_ak){
        gl.deleteBuffer(vboTexCoordPyramid_ak);
        vboTexCoordPyramid_ak = null;
    }

    if(vboPositionCube_ak){
        gl.deleteBuffer(vboPositionCube_ak);
        vboPositionCube_ak = null;
    }

    if(vboTexCoordCube_ak){
        gl.deleteBuffer(vboTexCoordCube_ak);
        vboTexCoordCube_ak = null;
    }

    if(textureStone_ak){
        gl.deleteTexture(textureStone_ak);
        textureStone_ak = null;
    }

    if(textureKundali_ak){
        gl.deleteTexture(textureKundali_ak);
        textureKundali_ak = null;
    }

    if(shaderProgramObject_ak){

        if(vertexShaderObject_ak){
            gl.detachShader(shaderProgramObject_ak, vertexShaderObject_ak);
            gl.deleteShader(vertexShaderObject_ak);
            vertexShaderObject_ak = null;
        }

        if(fragmentShaderObject_ak){
            gl.detachShader(shaderProgramObject_ak, fragmentShaderObject_ak);
            gl.deleteShader(fragmentShaderObject_ak);
            fragmentShaderObject_ak = null;
        }

        gl.deleteProgram(shaderProgramObject_ak);
        shaderProgramObject_ak = null;
    }
}
