var canvas = null;
var gl = null;
var canvas_original_width;
var canvas_original_height;
var bFullscreen_ak = false;

const WebGLMacros = {
    ATTRIBUTE_VERTEX: 0,
    ATTRIBUTE_COLOR: 1,
    ATTRIBUTE_NORMAL: 2,
    ATTRIBUTE_TEXTURE0: 3,
};

var vertexShaderObject_ak;
var fragmentShaderObject_ak;
var shaderProgramObject_ak;

var mvpUniform_ak;;

var bAnimate_ak = false;

var perspectiveProjectionMatrix_ak;

var shoulder_ak=null;
var elbow_ak=null;

var shoulderAngle_ak = 0;
var elbowAngle_ak = 0;

var customStack_ak = new customStack();

var requestAnimationFrame = window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame  ||
                            window.mozRequestAnimationFrame ||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var cancelAnimationFrame = window.cancelAnimationFrame || 
                           window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
                           window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
                           window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
                           window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

function main(){

    canvas = document.getElementById("ASK");
    if(!canvas){
        console.log("Obtaining Canvas Failed");
    }
    else{
        // console.log("Obtaining Canvas Succeeded");
    }

    canvas_original_width = canvas.width;    
    canvas_original_height = canvas.height;
    
    // window is built-in variable
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("resize", resize, false);

    init();

    resize(); // warmup resize call

    draw(); // warmup re-paint call

}

function init(){
    gl = canvas.getContext("webgl2");
    if(!gl){
        console.log("Obtaining webgl2 Context Failed");
    }
    else{
        console.log("Obtaining webgl2 Context Succeeded");
    }
    
    gl.viewportWidth  = canvas.width;
    gl.viewportHeight  = canvas.height;

    //Vertex Shader
    var vertexShaderSourceCode = 
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;" +
    "out vec4 out_color;" +
    "uniform mat4 u_mvp_matrix;" +
    "void main()" +
    "{" +
          "gl_Position = u_mvp_matrix * vPosition;" +
    "}";

    vertexShaderObject_ak = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject_ak, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject_ak);

    if(gl.getShaderParameter(vertexShaderObject_ak, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(vertexShaderObject_ak);
            if(error.length > 0){
                alert(error);
            }
    }

    //Fragment Shader
    var fragmentShaderSourceCode = 
    "#version 300 es"+
    "\n"+
    "precision highp float;" +
    "out vec4 FragColor;" +
    "void main()"+
    "{" +
        "FragColor = vec4(0.5, 0.35, 0.05, 1.0)" +
    "}";

    fragmentShaderObject_ak = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject_ak, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject_ak);

    if(gl.getShaderParameter(fragmentShaderObject_ak, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(fragmentShaderObject_ak);
            if(error.length > 0){
                alert(error);
                uninitalize();
            }
    }

    //Shader Program
    shaderProgramObject_ak = gl.createProgram();
    gl.attachShader(shaderProgramObject_ak, vertexShaderObject_ak);
    gl.attachShader(shaderProgramObject_ak, fragmentShaderObject_ak);

    gl.bindAttribLocation(shaderProgramObject_ak, WebGLMacros.ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject_ak, WebGLMacros.ATTRIBUTE_NORMAL, "vColor");

    //linking
    gl.linkProgram(shaderProgramObject_ak);

    if(gl.getProgramParameter(shaderProgramObject_ak, gl.LINK_STATUS) == false){
        var error = gl.getProgramInfoLog(shaderProgramObject_ak);
            if(error.length > 0){
                alert(error);
                uninitalize();
            }
    }

    //uniforms
    mvpUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_mvp_matrix");

    shoulder_ak = new Mesh();
    makeSphere(shoulder_ak, 0.5, 10, 10)

    elbow_ak = new Mesh();
    makeSphere(elbow_ak, 0.5, 10, 10)

	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
    gl.clearDepth(1.0);

    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    perspectiveProjectionMatrix_ak = mat4.create();
}

function resize(){
    if (bFullscreen_ak == true){
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else{
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    gl.viewport(0, 0, canvas.width,canvas.height);

    //perspective call
    mat4.perspective(perspectiveProjectionMatrix_ak, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
    
}

function draw(){
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObject_ak);

    var modelViewMatrix_ak = mat4.create();
    var modelViewProjectionMatrix_ak = mat4.create();

    mat4.translate(modelViewMatrix_ak, modelViewMatrix_ak, [0.0, 0.0, -6.0]);

    customStack_ak.push({...modelViewMatrix_ak});
        mat4.rotateZ(modelViewMatrix_ak, modelViewMatrix_ak,degToRad(shoulderAngle_ak));
        mat4.translate(modelViewMatrix_ak, modelViewMatrix_ak, [1.0, 0.0, 0.0]);
        customStack_ak.push({...modelViewMatrix_ak});
            mat4.scale(modelViewMatrix_ak, modelViewMatrix_ak, [2.0, 0.5, 1.0]);
            mat4.multiply(modelViewProjectionMatrix_ak, perspectiveProjectionMatrix_ak, modelViewMatrix_ak );
            gl.uniformMatrix4fv(mvpUniform_ak, false, modelViewProjectionMatrix_ak);

            shoulder_ak.draw();

            mat4.identity(modelViewMatrix_ak);
            mat4.identity(modelViewProjectionMatrix_ak);
       
            modelViewMatrix_ak=customStack_ak.pop();
            mat4.translate(modelViewMatrix_ak, modelViewMatrix_ak, [1.0, 0.0, 0.0]);
            mat4.rotateZ(modelViewMatrix_ak, modelViewMatrix_ak,degToRad(elbowAngle_ak));
            mat4.translate(modelViewMatrix_ak, modelViewMatrix_ak, [1.0, 0.0, 0.0]);
            customStack_ak.push({...modelViewMatrix_ak});
                mat4.scale(modelViewMatrix_ak, modelViewMatrix_ak, [2.0, 0.5, 1.0]);
                mat4.multiply(modelViewProjectionMatrix_ak, perspectiveProjectionMatrix_ak, modelViewMatrix_ak );
                gl.uniformMatrix4fv(mvpUniform_ak, false, modelViewProjectionMatrix_ak);

                elbow_ak.draw();
            customStack_ak.pop();
    customStack_ak.pop();

    gl.useProgram(null);

    update();

    requestAnimationFrame(draw, canvas);
}

function update(){

}

function degToRad(degree) {
    return (degree * Math.PI / 180.0);
}

function toggleFullscreen(){

    var fullscreen_element = document.fullscreenElement || 
                             document.webkitFullscreenElement ||
                             document.mozFullScreenElement ||
                             document.msFullscreenElement ||
                             null ;

    if (fullscreen_element == null) {
      bFullscreen_ak = true;

      if (canvas.requestFullscreen) {
        canvas.requestFullscreen();
      } else if (canvas.webkitRequestFullscreen) {
        canvas.webkitRequestFullscreen();
      } else if (canvas.mozRequestFullScreenElement) {
        canvas.mozRequestFullScreenElement();
      } else if (canvas.msRequestFullscreen) {
        canvas.msRequestFullscreen();
      }
    } else {
      bFullscreen_ak = false;

      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      }
    }
}

function keyDown(event){
    switch(event.keyCode){
        case 70:
            toggleFullscreen();
            break;
        
        case 27:
            uninitalize();
            window.close();
            break;
        
        case 83:
            shoulderAngle_ak = (shoulderAngle_ak + 3) % 360;
            break;

        case 69:
            elbowAngle_ak = (elbowAngle_ak + 3) % 360;
            break;
    }
}

function uninitalize(){
    if(shaderProgramObject_ak){

        if(vertexShaderObject_ak){
            gl.detachShader(shaderProgramObject_ak, vertexShaderObject_ak);
            gl.deleteShader(vertexShaderObject_ak);
            vertexShaderObject_ak = null;
        }

        if(fragmentShaderObject_ak){
            gl.detachShader(shaderProgramObject_ak, fragmentShaderObject_ak);
            gl.deleteShader(fragmentShaderObject_ak);
            fragmentShaderObject_ak = null;
        }

        gl.deleteProgram(shaderProgramObject_ak);
        shaderProgramObject_ak = null;
    }
}
