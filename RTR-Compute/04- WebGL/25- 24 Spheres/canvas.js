var canvas = null;
var gl = null;
var canvas_original_width;
var canvas_original_height;
var bFullscreen_ak = false;

const WebGLMacros = {
    ATTRIBUTE_VERTEX: 0,
    ATTRIBUTE_COLOR: 1,
    ATTRIBUTE_NORMAL: 2,
    ATTRIBUTE_TEXTURE0: 3,
};

var vertexShaderObject_ak;
var fragmentShaderObject_ak;
var shaderProgramObject_ak;

var modelUniform_ak;
var viewUniform_ak;
var projectionUniform_ak;
var ldUniform_ak;
var kdUniform_ak;
var laUniform_ak;
var kaUniform_ak;
var lsUniform_ak;
var ksUniform_ak;
var shininessUniform_ak;
var lightPositionUniform_ak;
var keyPressed_l_Uniform_ak;

var bLight_ak = false;
var bAnimate_ak = false;

var perspectiveProjectionMatrix_ak;

var lightAmbient_ak = new  Float32Array ([0.0,0.0,0.0]);
var lightDiffuse_ak = new  Float32Array ([1.0,1.0,1.0]);
var lightSpecular_ak = new Float32Array ([0.0,0.0,0.0]);
var lightPosition_ak = new Float32Array ([0.0,0.0,0.0,1.0]);

var materialAmbient_ak = new Float32Array ([0.0,0.0,0.0]);
var materialDiffuse_ak = new Float32Array ([1.0,1.0,1.0]);
var materialSpecular_ak =new Float32Array ([1.0,1.0,1.0]);
var materialShininess_ak = 50.0;

var sphere_ak=null;

var angleSphere_ak = 0.0;

var key_ak = 0;

var requestAnimationFrame = window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame  ||
                            window.mozRequestAnimationFrame ||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var cancelAnimationFrame = window.cancelAnimationFrame || 
                           window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
                           window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
                           window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
                           window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

function main(){

    canvas = document.getElementById("ASK");
    if(!canvas){
        console.log("Obtaining Canvas Failed");
    }
    else{
        // console.log("Obtaining Canvas Succeeded");
    }

    canvas_original_width = canvas.width;    
    canvas_original_height = canvas.height;
    
    // window is built-in variable
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("resize", resize, false);

    init();

    resize(); // warmup resize call

    draw(); // warmup re-paint call

}

function init(){
    gl = canvas.getContext("webgl2");
    if(!gl){
        console.log("Obtaining webgl2 Context Failed");
    }
    else{
        console.log("Obtaining webgl2 Context Succeeded");
    }
    
    gl.viewportWidth  = canvas.width;
    gl.viewportHeight  = canvas.height;

    //Vertex Shader
    var vertexShaderSourceCode = 
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;" +
    "in vec3 vNormal;" +
    "uniform mat4 u_model_matrix;" +
    "uniform mat4 u_view_matrix;" +
    "uniform mat4 u_projection_matrix;" +
    "uniform lowp int l_key_pressed;"+
    "uniform vec4 u_light_position;"+
    "out vec3 transformed_normal;" +
    "out vec3 light_direction;" +
    "out vec3 view_vector;" +
    "void main()" +
    "{" +
    "if (l_key_pressed == 1) " +
        "{" +
            "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" +
            "transformed_normal = mat3(u_view_matrix*u_model_matrix) * vNormal;"+
            "light_direction = vec3(u_light_position-eyeCoordinates);"+
            "view_vector = -eyeCoordinates.xyz;"+
        "}" +
    "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
   "}";

    vertexShaderObject_ak = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject_ak, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject_ak);

    if(gl.getShaderParameter(vertexShaderObject_ak, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(vertexShaderObject_ak);
            if(error.length > 0){
                alert(error);
            }
    }

    //Fragment Shader
    var fragmentShaderSourceCode = 
    "#version 300 es"+
    "\n"+
    "precision highp float;" +
    "out vec4 FragColor;" +
    "in vec3 transformed_normal;" +
    "in vec3 light_direction;" +
    "in vec3 view_vector;" +
    "uniform vec3 u_Ld;"+
    "uniform vec3 u_Kd;"+
    "uniform vec3 u_La;"+
    "uniform vec3 u_Ka;"+
    "uniform vec3 u_Ls;"+
    "uniform vec3 u_Ks;"+
    "uniform float u_shininess;" +
    "uniform lowp int l_key_pressed;"+
    "void main()"+
    "{" +
        "vec3 phong_ads_color;" +
        "if(l_key_pressed == 1)"+
        "{" +
            "vec3 norm_transformed_normal = normalize(transformed_normal);" +
            "vec3 norm_light_direction = normalize(light_direction);" +
            "vec3 norm_view_vector = normalize(view_vector);"+
            "vec3 reflection_vector = reflect(-norm_light_direction, norm_transformed_normal);"+
            "vec3 ambient = u_La * u_Ka;"+
            "vec3 diffuse = u_Ld * u_Kd * max(dot(norm_light_direction, norm_transformed_normal),0.0);"+
            "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, norm_view_vector),0.0),u_shininess);" +
            "phong_ads_color = ambient + diffuse + specular;"+
        "}"+
        "else"+
        "{"+
            "phong_ads_color = vec3(1.0f, 1.0f, 1.0f);"+
        "}"+
        "FragColor = vec4(phong_ads_color, 1.0);"  +
    "}";

    fragmentShaderObject_ak = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject_ak, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject_ak);

    if(gl.getShaderParameter(fragmentShaderObject_ak, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(fragmentShaderObject_ak);
            if(error.length > 0){
                alert(error);
                uninitalize();
            }
    }

    //Shader Program
    shaderProgramObject_ak = gl.createProgram();
    gl.attachShader(shaderProgramObject_ak, vertexShaderObject_ak);
    gl.attachShader(shaderProgramObject_ak, fragmentShaderObject_ak);

    gl.bindAttribLocation(shaderProgramObject_ak, WebGLMacros.ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject_ak, WebGLMacros.ATTRIBUTE_NORMAL, "vNormal");

    //linking
    gl.linkProgram(shaderProgramObject_ak);

    if(gl.getProgramParameter(shaderProgramObject_ak, gl.LINK_STATUS) == false){
        var error = gl.getProgramInfoLog(shaderProgramObject_ak);
            if(error.length > 0){
                alert(error);
                uninitalize();
            }
    }

    //uniforms
    modelUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_model_matrix");
    viewUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_view_matrix");
    projectionUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_projection_matrix");
	ldUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_Ld");
	kdUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_Kd");
	laUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_La");
	kaUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_Ka");
	lsUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_Ls");
	ksUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_Ks");
    shininessUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_shininess");
	lightPositionUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_light_position");
    keyPressed_l_Uniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "l_key_pressed");
    
    sphere_ak = new Mesh();
    makeSphere(sphere_ak, 0.5 , 30, 30);

	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
    gl.clearDepth(1.0);

    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    perspectiveProjectionMatrix_ak = mat4.create();
}

function resize(){
    if (bFullscreen_ak == true){
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else{
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    gl.viewport(0, 0, canvas.width,canvas.height);

    //perspective call
    mat4.perspective(perspectiveProjectionMatrix_ak, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
    
}

function draw(){
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObject_ak);

    if(bLight_ak){
        gl.uniform1i(keyPressed_l_Uniform_ak, 1);
    }
    else
        gl.uniform1i(keyPressed_l_Uniform_ak, 0);

    var modelMatrix_ak = mat4.create();
    var viewMatrix_ak = mat4.create();

    //sphere - 1
    materialAmbient_ak[0] = 0.0215; 
    materialAmbient_ak[1] = 0.1745; 
    materialAmbient_ak[2] = 0.0215; 
    materialAmbient_ak[3] = 1.0;   

    materialDiffuse_ak[0] = 0.07568; 
    materialDiffuse_ak[1] = 0.61424; 
    materialDiffuse_ak[2] = 0.07568; 
    materialDiffuse_ak[3] = 1.0;    

    materialSpecular_ak[0] = 0.633;   
    materialSpecular_ak[1] = 0.727811;
    materialSpecular_ak[2] = 0.633;   
    materialSpecular_ak[3] = 1.0;    

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width)*(-0.45), parseFloat(canvas.height)*(0.35), parseFloat(canvas.width) , parseFloat(canvas.height));

    uniformAndSphere();

    mat4.translate(modelMatrix_ak, modelMatrix_ak, [0.0, 0.0, -10.0]);

    gl.uniformMatrix4fv(modelUniform_ak, false, modelMatrix_ak);
	gl.uniformMatrix4fv(viewUniform_ak, false, viewMatrix_ak);
	gl.uniformMatrix4fv(projectionUniform_ak, false, perspectiveProjectionMatrix_ak);

    // sphere-2
    materialAmbient_ak[0] = 0.135;
    materialAmbient_ak[1] = 0.2225;
    materialAmbient_ak[2] = 0.1575;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[0] = 0.54;
    materialDiffuse_ak[1] = 0.89;
    materialDiffuse_ak[2] = 0.63;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.316228;
    materialSpecular_ak[1] = 0.316228;
    materialSpecular_ak[2] = 0.316228;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (-0.45), parseFloat(canvas.height) * (0.2), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    // sphere -3 
    materialAmbient_ak[0] = 0.05375;
    materialAmbient_ak[1] = 0.05;
    materialAmbient_ak[2] = 0.06625;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[0] = 0.18275;
    materialDiffuse_ak[1] = 0.17;
    materialDiffuse_ak[2] = 0.22525;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.332741;
    materialSpecular_ak[1] = 0.328634;
    materialSpecular_ak[2] = 0.346435;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (-0.45), parseFloat(canvas.height) * (0.05), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    // sphere-4
    materialAmbient_ak[0] = 0.25;
    materialAmbient_ak[1] = 0.20725;
    materialAmbient_ak[2] = 0.20725;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[0] = 1.0;
    materialDiffuse_ak[1] = 0.829;
    materialDiffuse_ak[2] = 0.829;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.296648;
    materialSpecular_ak[1] = 0.296648;
    materialSpecular_ak[2] = 0.296648;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (-0.45), parseFloat(canvas.height) * (-0.10), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    // sphere-5
    materialAmbient_ak[0] = 0.1745;
    materialAmbient_ak[1] = 0.01175;
    materialAmbient_ak[2] = 0.01175;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[0] = 0.61424;
    materialDiffuse_ak[1] = 0.04136;
    materialDiffuse_ak[2] = 0.04136;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.727811;
    materialSpecular_ak[1] = 0.626959;
    materialSpecular_ak[2] = 0.626959;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (-0.45), parseFloat(canvas.height) * (-0.25), parseFloat(canvas.width), parseFloat(canvas.height));
    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    // sphere-6
    materialAmbient_ak[0] = 0.1;
    materialAmbient_ak[1] = 0.18725;
    materialAmbient_ak[2] = 0.1745;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[0] = 0.396;
    materialDiffuse_ak[1] = 0.74151;
    materialDiffuse_ak[2] = 0.69102;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.297254;
    materialSpecular_ak[1] = 0.30829;
    materialSpecular_ak[2] = 0.306678;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (-0.45),parseFloat(canvas.height) * (-0.40), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    // sphere-7
    materialAmbient_ak[0] = 0.329412;
    materialAmbient_ak[1] = 0.223529;
    materialAmbient_ak[2] = 0.027451;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[0] = 0.780392;
    materialDiffuse_ak[1] = 0.568627;
    materialDiffuse_ak[2] = 0.113725;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.992157;
    materialSpecular_ak[1] = 0.941176;
    materialSpecular_ak[2] = 0.807843;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (-0.15), parseFloat(canvas.height) * (0.35), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    // sphere-8
    materialAmbient_ak[0] = 0.2125;
    materialAmbient_ak[1] = 0.1275;
    materialAmbient_ak[2] = 0.054;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[0] = 0.714;
    materialDiffuse_ak[1] = 0.4284;
    materialDiffuse_ak[2] = 0.18144;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.393548;
    materialSpecular_ak[1] = 0.271906;
    materialSpecular_ak[2] = 0.166721;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (-0.15), parseFloat(canvas.height) * (0.20), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    // sphere-9
    materialAmbient_ak[0] = 0.25;
    materialAmbient_ak[1] = 0.25;
    materialAmbient_ak[2] = 0.25;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[1] = 0.4;
    materialDiffuse_ak[2] = 0.4;
    materialDiffuse_ak[0] = 0.4;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.774597;
    materialSpecular_ak[1] = 0.774597;
    materialSpecular_ak[2] = 0.774597;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (-0.15), parseFloat(canvas.height) * (0.05), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();


    // sphere-10
    materialAmbient_ak[0] = 0.19125;
    materialAmbient_ak[1] = 0.0735;
    materialAmbient_ak[2] = 0.0225;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[0] = 0.7038;
    materialDiffuse_ak[1] = 0.27048;
    materialDiffuse_ak[2] = 0.0828;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.256777;
    materialSpecular_ak[1] = 0.137622;
    materialSpecular_ak[2] = 0.086014;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (-0.15), parseFloat(canvas.height) * (-0.1), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    // sphere-11
    materialAmbient_ak[0] = 0.24725;
    materialAmbient_ak[1] = 0.1995;
    materialAmbient_ak[2] = 0.0745;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[0] = 0.75164;
    materialDiffuse_ak[1] = 0.60648;
    materialDiffuse_ak[2] = 0.22648;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.628281;
    materialSpecular_ak[1] = 0.555802;
    materialSpecular_ak[2] = 0.366065;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (-0.15), parseFloat(canvas.height) * (-0.25), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    // sphere-12
    materialAmbient_ak[0] = 0.19225;
    materialAmbient_ak[1] = 0.19225;
    materialAmbient_ak[2] = 0.19225;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[1] = 0.50754;
    materialDiffuse_ak[2] = 0.50754;
    materialDiffuse_ak[0] = 0.50754;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.508273;
    materialSpecular_ak[1] = 0.508273;
    materialSpecular_ak[2] = 0.508273;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (-0.15), parseFloat(canvas.height) * (-0.40), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    // sphere-13
    materialAmbient_ak[0] = 0.0;
    materialAmbient_ak[1] = 0.0;
    materialAmbient_ak[2] = 0.0;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[0] = 0.1;
    materialDiffuse_ak[1] = 0.1;
    materialDiffuse_ak[2] = 0.1;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.50;
    materialSpecular_ak[1] = 0.50;
    materialSpecular_ak[2] = 0.50;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (0.15), parseFloat(canvas.height) * (0.35), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    // sphere-14
    materialAmbient_ak[0] = 0.0;
    materialAmbient_ak[1] = 0.1;
    materialAmbient_ak[2] = 0.06;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[0] = 0.0;
    materialDiffuse_ak[1] = 0.50980392;
    materialDiffuse_ak[2] = 0.50980392;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.50196078;
    materialSpecular_ak[1] = 0.50196078;
    materialSpecular_ak[2] = 0.50196078;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (0.15),parseFloat(canvas.height) * (0.20), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    // sphere-15
    materialAmbient_ak[0] = 0.0;
    materialAmbient_ak[1] = 0.0;
    materialAmbient_ak[2] = 0.0;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[0] = 0.1;
    materialDiffuse_ak[1] = 0.35;
    materialDiffuse_ak[2] = 0.1;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.45;
    materialSpecular_ak[1] = 0.55;
    materialSpecular_ak[2] = 0.45;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (0.15), parseFloat(canvas.height) * (0.05), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    // sphere-16
    materialAmbient_ak[0] = 0.0;
    materialAmbient_ak[1] = 0.0;
    materialAmbient_ak[2] = 0.0;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[0] = 0.5;
    materialDiffuse_ak[1] = 0.0;
    materialDiffuse_ak[2] = 0.0;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.7;
    materialSpecular_ak[1] = 0.6;
    materialSpecular_ak[2] = 0.6;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (0.15), parseFloat(canvas.height) * (-0.10), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    // sphere-17
    materialAmbient_ak[0] = 0.0;
    materialAmbient_ak[1] = 0.0;
    materialAmbient_ak[2] = 0.0;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[0] = 0.55;
    materialDiffuse_ak[1] = 0.55;
    materialDiffuse_ak[2] = 0.55;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.70;
    materialSpecular_ak[1] = 0.70;
    materialSpecular_ak[2] = 0.70;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (0.15), parseFloat(canvas.height) * (-0.25), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    // sphere-18
    materialAmbient_ak[0] = 0.0;
    materialAmbient_ak[1] = 0.0;
    materialAmbient_ak[2] = 0.0;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[0] = 0.5;
    materialDiffuse_ak[1] = 0.5;
    materialDiffuse_ak[2] = 0.0;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.60;
    materialSpecular_ak[1] = 0.60;
    materialSpecular_ak[2] = 0.50;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (0.15), parseFloat(canvas.height) * (-0.40), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    // sphere-19
    materialAmbient_ak[0] = 0.02;
    materialAmbient_ak[1] = 0.02;
    materialAmbient_ak[2] = 0.02;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[0] = 0.1;
    materialDiffuse_ak[1] = 0.1;
    materialDiffuse_ak[2] = 0.1;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.4;
    materialSpecular_ak[1] = 0.4;
    materialSpecular_ak[2] = 0.4;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (0.45), parseFloat(canvas.height) * (0.35), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    // sphere-20
    materialAmbient_ak[0] = 0.0;
    materialAmbient_ak[1] = 0.05;
    materialAmbient_ak[2] = 0.05;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[0] = 0.4;
    materialDiffuse_ak[1] = 0.5;
    materialDiffuse_ak[2] = 0.5;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.04;
    materialSpecular_ak[1] = 0.7;
    materialSpecular_ak[2] = 0.7;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (0.45), parseFloat(canvas.height) * (0.20), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    // sphere-21
    materialAmbient_ak[0] = 0.0;
    materialAmbient_ak[1] = 0.05;
    materialAmbient_ak[2] = 0.0;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[0] = 0.4;
    materialDiffuse_ak[1] = 0.5;
    materialDiffuse_ak[2] = 0.4;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.04;
    materialSpecular_ak[1] = 0.7;
    materialSpecular_ak[2] = 0.04;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (0.45), parseFloat(canvas.height) * (0.05), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    // sphere-22
    materialAmbient_ak[0] = 0.05;
    materialAmbient_ak[1] = 0.0;
    materialAmbient_ak[2] = 0.0;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[0] = 0.5;
    materialDiffuse_ak[1] = 0.4;
    materialDiffuse_ak[2] = 0.4;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.7;
    materialSpecular_ak[1] = 0.04;
    materialSpecular_ak[2] = 0.04;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (0.45), parseFloat(canvas.height) * (-0.10), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    // sphere-23
    materialAmbient_ak[0] = 0.05;
    materialAmbient_ak[1] = 0.05;
    materialAmbient_ak[2] = 0.05;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[0] = 0.5;
    materialDiffuse_ak[1] = 0.5;
    materialDiffuse_ak[2] = 0.5;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.7;
    materialSpecular_ak[1] = 0.7;
    materialSpecular_ak[2] = 0.7;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;

    gl.viewport(parseFloat(canvas.width) * (0.45), parseFloat(canvas.height) * (-0.25), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    // sphere-24
    materialAmbient_ak[0] = 0.05;
    materialAmbient_ak[1] = 0.05;
    materialAmbient_ak[2] = 0.0;
    materialAmbient_ak[3] = 1.0;

    materialDiffuse_ak[0] = 0.5;
    materialDiffuse_ak[1] = 0.5;
    materialDiffuse_ak[2] = 0.4;
    materialDiffuse_ak[3] = 1.0;

    materialSpecular_ak[0] = 0.7;
    materialSpecular_ak[1] = 0.7;
    materialSpecular_ak[2] = 0.04;
    materialSpecular_ak[3] = 1.0;

    materialShininess_ak = 128.0;
    gl.viewport(parseFloat(canvas.width) * (0.45), parseFloat(canvas.height) * (-0.40), parseFloat(canvas.width), parseFloat(canvas.height));

    gl.uniformMatrix4fv(modelUniform_ak,  false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak,  false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak,  false, perspectiveProjectionMatrix_ak);

    uniformAndSphere();

    gl.useProgram(null);

    if(bAnimate_ak)
        update();

    requestAnimationFrame(draw, canvas);
}

function update(){
    angleSphere_ak += 0.1;
    if (angleSphere_ak > 360)
        angleSphere_ak = 0.0;

    if (key_ak == 1)
    {
        lightPosition_ak[1] = 10 * Math.sin(angleSphere_ak);
        lightPosition_ak[2] = 10 * Math.cos(angleSphere_ak);
    }	
    if (key_ak == 2)
    {
        lightPosition_ak[0] = 10 * Math.sin(angleSphere_ak);
        lightPosition_ak[2] = 10 * Math.cos(angleSphere_ak);
    }	
    if (key_ak == 3)
    {
        lightPosition_ak[0] = 10 * Math.sin(angleSphere_ak);
        lightPosition_ak[1] = 10 * Math.cos(angleSphere_ak);
    }
}

function uniformAndSphere() {
    gl.uniform3fv(laUniform_ak, lightAmbient_ak);
    gl.uniform3fv(kaUniform_ak, materialAmbient_ak);
    gl.uniform3fv(ldUniform_ak, lightDiffuse_ak);
    gl.uniform3fv(kdUniform_ak, materialDiffuse_ak);
    gl.uniform3fv(lsUniform_ak, lightSpecular_ak);
    gl.uniform3fv(ksUniform_ak, materialSpecular_ak);

    gl.uniform4fv(lightPositionUniform_ak, lightPosition_ak);

    gl.uniform1f(shininessUniform_ak, materialShininess_ak);

    sphere_ak.draw();
}

function degToRad(degree) {
    return (degree * Math.PI / 180.0);
}

function toggleFullscreen(){

    var fullscreen_element = document.fullscreenElement || 
                             document.webkitFullscreenElement ||
                             document.mozFullScreenElement ||
                             document.msFullscreenElement ||
                             null ;

    if (fullscreen_element == null) {
      bFullscreen_ak = true;

      if (canvas.requestFullscreen) {
        canvas.requestFullscreen();
      } else if (canvas.webkitRequestFullscreen) {
        canvas.webkitRequestFullscreen();
      } else if (canvas.mozRequestFullScreenElement) {
        canvas.mozRequestFullScreenElement();
      } else if (canvas.msRequestFullscreen) {
        canvas.msRequestFullscreen();
      }
    } else {
      bFullscreen_ak = false;

      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      }
    }
}

function keyDown(event){
    switch(event.keyCode){
        case 70:
            toggleFullscreen();
            break;
        
        case 27:
            uninitalize();
            window.close();
            break;
        
        case 76:
            if(bLight_ak == false){
                bLight_ak = true;
            }
            else{
                bLight_ak = false;
            }
            break;

        case 65:
            if(bAnimate_ak == false)
                bAnimate_ak = true;
            else    
                bAnimate_ak = false;
            break;
        
        case 88:
            key_ak = 1;
            lightPosition_ak[0] = 0.0;
            lightPosition_ak[1] = 0.0;
            lightPosition_ak[2] = 0.0;
            break;
        
        case 89:
            key_ak = 2;
            lightPosition_ak[0] = 0.0;
            lightPosition_ak[1] = 0.0;
            lightPosition_ak[2] = 0.0;
            break;

        case 90:
            key_ak = 3;
            lightPosition_ak[0] = 0.0;
            lightPosition_ak[1] = 0.0;
            lightPosition_ak[2] = 0.0;
            break;
    }
}

function uninitalize(){
    if(shaderProgramObject_ak){

        if(vertexShaderObject_ak){
            gl.detachShader(shaderProgramObject_ak, vertexShaderObject_ak);
            gl.deleteShader(vertexShaderObject_ak);
            vertexShaderObject_ak = null;
        }

        if(fragmentShaderObject_ak){
            gl.detachShader(shaderProgramObject_ak, fragmentShaderObject_ak);
            gl.deleteShader(fragmentShaderObject_ak);
            fragmentShaderObject_ak = null;
        }

        gl.deleteProgram(shaderProgramObject_ak);
        shaderProgramObject_ak = null;
    }
}
