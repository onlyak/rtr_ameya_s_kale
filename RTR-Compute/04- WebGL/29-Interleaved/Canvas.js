var canvas_PSM = null;
var gl = null;
var bFullscreen_PSM = false;
var bLight_PSM = false;
var canvas_original_width_PSM;
var canvas_original_height_PSM;

const WebGLMacros = {
    ATTRIBUTE_VERTEX_PSM: 0,
    ATTRIBUTE_COLOR_PSM: 1,
    ATTRIBUTE_NORMAL_PSM: 2,
    ATTRIBUTE_TEXTURE_PSM: 3,
};


var vao_PSM;
var vbo_PSM;

var vertexShaderObject_PSM;
var fragmentShaderObject_PSM;
var shaderProgramObject_PSM;

var modelMatrixUniform_PSM;
var viewMatrixUniform_PSM;
var projectionMatrixUniform_PSM;

var lKeyPressedUniform_PSM;

var laUniform_PSM;
var ldUniform_PSM;
var lsUniform_PSM;
var lightPositionUniform_PSM;

var kaUniform_PSM;
var kdUniform_PSM;
var ksUniform_PSM;
var materialShininessUniform_PSM;

var lightAmbient_PSM = new Float32Array([0.1, 0.1, 0.1]);
var lightDiffused_PSM = new Float32Array([1.0, 1.0, 1.0]);
var lightPosition_PSM = new Float32Array([100.0, 100.0, 100.0, 1.0]);
var lightSpecular_PSM = new Float32Array([1.0, 1.0, 1.0]);
var materialAmbient_PSM = new Float32Array([0.0, 0.0, 0.0]);
var materialDiffused_PSM = new Float32Array([0.5, 0.2, 0.7]);
var materialSpecular_PSM = new Float32Array([0.7, 0.7, 0.7]);
var materialShininess_PSM = 50.0;


var cubeAngle_PSM = 0.0;
var stone_texture_PSM;

var requestAnimationFrame_PSM = window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame;

function main() {
    // Get canvas_PSM from DOMContentLoaded
    canvas_PSM = document.getElementById("psm");
    if (!canvas_PSM) {
        console.log("Obtaining canvas_PSM failed");
        return;
    } else {
        console.log("Obtaining canvas_PSM successful");
    }
    // Retrieve height and width of canvas_PSM

    console.log("Canvas width = " + canvas_PSM.width + " Canvas height = " + canvas_PSM.height);
    canvas_original_width_PSM = canvas_PSM.width;
    canvas_original_height_PSM = canvas_PSM.height;

    window.addEventListener('keydown', keyDown, false);
    window.addEventListener('resize', resize, false);

    init();
    resize();
    draw();
}

function toggleFullscreen() {

    var fullscreen_element_PSM = document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement ||
        null;

    if (fullscreen_element_PSM == null) {
        if (canvas_PSM.requestFullscreen) {
            canvas_PSM.requestFullscreen();
        }
        else if (canvas_PSM.webkitRequestFullscreen) {
            canvas_PSM.webkitRequestFullscreen();
        }
        else if (canvas_PSM.mozRequestFullScreenElement) {
            canvas_PSM.mozRequestFullScreenElement();
        }
        else if (canvas_PSM.msRequestFullscreen) {
            canvas_PSM.msRequestFullscreen();
        }
        bFullscreen_PSM = true;
    }
    else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
        else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
        else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        }
        else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
        bFullscreen_PSM = false;
    }
}

function keyDown(event) {
    switch (event.keyCode) {
        case 70:
            {
                toggleFullscreen();
                break;
            }
        case 27:
            {
                uninitialize();
                window.close();
                break;
            }
        case 76:
            {
                if (bLight_PSM == false) {
                    bLight_PSM = true;
                }
                else {
                    bLight_PSM = false;
                }
                break;
            }
    }
}

function init() {
    gl = canvas_PSM.getContext("webgl2");
    if (!gl) {
        console.log("Obtaining webgl2 context failed");
        return;
    } else {
        console.log("Obtaining webgl2 context successful");
    }

    gl.viewportWidth = canvas_PSM.width;
    gl.viewportHeight = canvas_PSM.height;

    var vertexShaderSourceCode_PSM =
        "#version 300 es" +
        "\n" +
        "precision mediump float;" +
        "precision highp int;" +
        "in vec4 vPosition;" +
        "in vec4 vColor;\n" +
        "in vec3 vNormal;" +
        "in vec2 vTexCoord;\n" +
        "uniform mat4 u_model_matrix;" +
        "uniform mat4 u_view_matrix;" +
        "uniform mat4 u_projection_matrix;" +
        "uniform vec4 u_light_position;" +
        "uniform int u_lkey_pressed;" +
        "out vec3 transformed_normal;" +
        "out vec3 light_direction;" +
        "out vec3 view_vector;" +
        "out vec4 out_color;\n" +
        "out vec2 out_texcoord;\n" +
        "void main(void)" +
        "{" +
        "if(u_lkey_pressed == 1)" +
        "{" +
        "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" +
        "transformed_normal = mat3((u_view_matrix * u_model_matrix)) * vNormal;" +
        "light_direction = vec3(u_light_position - eye_coordinates);" +
        "view_vector = -eye_coordinates.xyz;" +
        "}" +
        "out_texcoord = vTexCoord;\n" +
        "out_color = vColor;\n" +
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
        "}";

    vertexShaderObject_PSM = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject_PSM, vertexShaderSourceCode_PSM);
    gl.compileShader(vertexShaderObject_PSM);

    if (gl.getShaderParameter(vertexShaderObject_PSM, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObject_PSM);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    var fragmentShaderSourceCode_PSM =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "precision highp int;" +
        "in vec4 out_color;\n" +
        "in vec2 out_texcoord;\n" +
        "in vec3 transformed_normal;" +
        "in vec3 light_direction;" +
        "in vec3 view_vector;" +
        "uniform vec3 u_la;" +
        "uniform vec3 u_ld;" +
        "uniform vec3 u_ls;" +
        "uniform vec3 u_ka;" +
        "uniform vec3 u_kd;" +
        "uniform vec3 u_ks;" +
        "uniform float u_material_shininess;" +
        "uniform int u_lkey_pressed;" +
        "uniform sampler2D u_texture_sampler;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
        "vec3 phong_ads_light;" +
        "if(u_lkey_pressed == 1)" +
        "{" +
        "vec3 normalized_transformed_normal = normalize(transformed_normal);" +
        "vec3 normalized_light_direction = normalize(light_direction);" +
        "vec3 normalized_view_vector = normalize(view_vector);" +
        "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normal);" +
        "vec3 ambient = u_la * u_ka;" +
        "vec3 diffuse = u_ld * u_kd * max(dot(normalized_light_direction, normalized_transformed_normal), 0.0);" +
        "vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, normalized_view_vector), 0.0), u_material_shininess);" +
        "phong_ads_light = ambient + diffuse + specular;" +
        "}" +
        "else" +
        "{" +
        "phong_ads_light = vec3(1.0, 1.0, 1.0);" +
        "}" +
        "vec3 tex = vec3(texture(u_texture_sampler, out_texcoord));" +
        "FragColor = vec4(vec3(out_color) * phong_ads_light * tex, 1.0f);" +
        "}";

    fragmentShaderObject_PSM = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject_PSM, fragmentShaderSourceCode_PSM);
    gl.compileShader(fragmentShaderObject_PSM);

    if (gl.getShaderParameter(fragmentShaderObject_PSM, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(fragmentShaderObject_PSM);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    shaderProgramObject_PSM = gl.createProgram();
    gl.attachShader(shaderProgramObject_PSM, vertexShaderObject_PSM);
    gl.attachShader(shaderProgramObject_PSM, fragmentShaderObject_PSM);

    gl.bindAttribLocation(shaderProgramObject_PSM, WebGLMacros.ATTRIBUTE_VERTEX_PSM, "vPosition");
    gl.bindAttribLocation(shaderProgramObject_PSM, WebGLMacros.ATTRIBUTE_TEXTURE_PSM, "vTexCoord");
    gl.bindAttribLocation(shaderProgramObject_PSM, WebGLMacros.ATTRIBUTE_NORMAL_PSM, "vNormal");


    gl.linkProgram(shaderProgramObject_PSM);

    if (gl.getProgramParameter(shaderProgramObject_PSM, gl.LINK_STATUS) == false) {
        var error = gl.getProgramInfoLog(shaderProgramObject_PSM);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    modelMatrixUniform_PSM = gl.getUniformLocation(shaderProgramObject_PSM, "u_model_matrix");
    viewMatrixUniform_PSM = gl.getUniformLocation(shaderProgramObject_PSM, "u_view_matrix");
    projectionMatrixUniform_PSM = gl.getUniformLocation(shaderProgramObject_PSM, "u_projection_matrix");
    laUniform_PSM = gl.getUniformLocation(shaderProgramObject_PSM, "u_la");
    ldUniform_PSM = gl.getUniformLocation(shaderProgramObject_PSM, "u_ld");
    lsUniform_PSM = gl.getUniformLocation(shaderProgramObject_PSM, "u_ls");
    lightPositionUniform_PSM = gl.getUniformLocation(shaderProgramObject_PSM, "u_light_position");
    kaUniform_PSM = gl.getUniformLocation(shaderProgramObject_PSM, "u_ka");
    kdUniform_PSM = gl.getUniformLocation(shaderProgramObject_PSM, "u_kd");
    ksUniform_PSM = gl.getUniformLocation(shaderProgramObject_PSM, "u_ks");
    materialShininessUniform_PSM = gl.getUniformLocation(shaderProgramObject_PSM, "u_material_shininess");
    lKeyPressedUniform_PSM = gl.getUniformLocation(shaderProgramObject_PSM, "u_lkey_pressed");
    textureSamplerUniform_PSM = gl.getUniformLocation(shaderProgramObject_PSM, "u_texture_sampler");

    var cubePCNT_PSM = new Float32Array([
        1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
        -1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0,
        -1.0, -1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0,
        1.0, -1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0,

        1.0, 1.0, -1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
        1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0,
        1.0, -1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0,
        1.0, -1.0, -1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0,

        -1.0, 1.0, -1.0, 0.0, 0.0, 1.0, 0.0, 0.0, -1.0, 0.0, 0.0,
        1.0, 1.0, -1.0, 0.0, 0.0, 1.0, 0.0, 0.0, -1.0, 1.0, 0.0,
        1.0, -1.0, -1.0, 0.0, 0.0, 1.0, 0.0, 0.0, -1.0, 1.0, 1.0,
        -1.0, -1.0, -1.0, 0.0, 0.0, 1.0, 0.0, 0.0, -1.0, 0.0, 1.0,

        -1.0, 1.0, 1.0, 0.0, 1.0, 1.0, -1.0, 0.0, 0.0, 0.0, 0.0,
        -1.0, 1.0, -1.0, 0.0, 1.0, 1.0, -1.0, 0.0, 0.0, 1.0, 0.0,
        -1.0, -1.0, -1.0, 0.0, 1.0, 1.0, -1.0, 0.0, 0.0, 1.0, 1.0,
        -1.0, -1.0, 1.0, 0.0, 1.0, 1.0, -1.0, 0.0, 0.0, 0.0, 1.0,

        1.0, 1.0, -1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0,
        -1.0, 1.0, -1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0,
        -1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 1.0,
        1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0,

        1.0, -1.0, -1.0, 1.0, 1.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0,
        -1.0, -1.0, -1.0, 1.0, 1.0, 0.0, 0.0, -1.0, 0.0, 1.0, 0.0,
        -1.0, -1.0, 1.0, 1.0, 1.0, 0.0, 0.0, -1.0, 0.0, 1.0, 1.0,
        1.0, -1.0, 1.0, 1.0, 1.0, 0.0, 0.0, -1.0, 0.0, 0.0, 1.0]

    );

    vao_PSM = gl.createVertexArray();
    gl.bindVertexArray(vao_PSM);

    vbo_PSM = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_PSM);
    gl.bufferData(gl.ARRAY_BUFFER, cubePCNT_PSM, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.ATTRIBUTE_VERTEX_PSM, 3, gl.FLOAT, false, 11 * 4, (0 * 4));
    gl.enableVertexAttribArray(WebGLMacros.ATTRIBUTE_VERTEX_PSM);

    gl.vertexAttribPointer(WebGLMacros.ATTRIBUTE_COLOR_PSM, 3, gl.FLOAT, false, 11 * 4, (3 * 4));
    gl.enableVertexAttribArray(WebGLMacros.ATTRIBUTE_COLOR_PSM);

    gl.vertexAttribPointer(WebGLMacros.ATTRIBUTE_NORMAL_PSM, 3, gl.FLOAT, false, 11 * 4, (6 * 4));
    gl.enableVertexAttribArray(WebGLMacros.ATTRIBUTE_NORMAL_PSM);

    gl.vertexAttribPointer(WebGLMacros.ATTRIBUTE_TEXTURE_PSM, 2, gl.FLOAT, false, 11 * 4, (9 * 4));
    gl.enableVertexAttribArray(WebGLMacros.ATTRIBUTE_TEXTURE_PSM);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    stone_texture_PSM = gl.createTexture();
    stone_texture_PSM.image = new Image();
    stone_texture_PSM.image.src = "marble.bmp"
    stone_texture_PSM.image.onload = function () {
        gl.bindTexture(gl.TEXTURE_2D, stone_texture_PSM);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, stone_texture_PSM.image);
        gl.bindTexture(gl.TEXTURE_2D, null);
    }

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.clearDepth(1.0);

    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    perspectiveProjectionMatrix_PSM = mat4.create();
}

function resize() {
    if (bFullscreen_PSM == true) {
        canvas_PSM.width = window.innerWidth;
        canvas_PSM.height = window.innerHeight;
    }
    else {
        canvas_PSM.width = canvas_original_width_PSM;
        canvas_PSM.height = canvas_original_height_PSM;
    }

    gl.viewport(0, 0, canvas_PSM.width, canvas_PSM.height);

    mat4.perspective(perspectiveProjectionMatrix_PSM,
        45.0,
        parseFloat(canvas_PSM.width) / parseFloat(canvas_PSM.height),
        0.1,
        100.0
    );
}

function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.useProgram(shaderProgramObject_PSM);

    if (bLight_PSM == true) {
        gl.uniform1i(lKeyPressedUniform_PSM, 1);
        gl.uniform3fv(laUniform_PSM, lightAmbient_PSM);
        gl.uniform3fv(ldUniform_PSM, lightDiffused_PSM);
        gl.uniform3fv(lsUniform_PSM, lightSpecular_PSM);
        gl.uniform4fv(lightPositionUniform_PSM, lightPosition_PSM);
        gl.uniform3fv(kaUniform_PSM, materialAmbient_PSM);
        gl.uniform3fv(kdUniform_PSM, materialDiffused_PSM);
        gl.uniform3fv(ksUniform_PSM, materialSpecular_PSM);
        gl.uniform1f(materialShininessUniform_PSM, materialShininess_PSM);
    } else {
        gl.uniform1i(lKeyPressedUniform_PSM, 0);
    }

    var modelMatrix_PSM = mat4.create();
    var viewMatrix_PSM = mat4.create();



    mat4.translate(modelMatrix_PSM, modelMatrix_PSM, [0.0, 0.0, -5.0]);
    mat4.scale(modelMatrix_PSM, modelMatrix_PSM, [0.75, 0.75, 0.75]);
    mat4.rotateX(modelMatrix_PSM, modelMatrix_PSM, degToRad(cubeAngle_PSM));
    mat4.rotateY(modelMatrix_PSM, modelMatrix_PSM, degToRad(cubeAngle_PSM));
    mat4.rotateZ(modelMatrix_PSM, modelMatrix_PSM, degToRad(cubeAngle_PSM));

    gl.uniformMatrix4fv(modelMatrixUniform_PSM, false, modelMatrix_PSM);
    gl.uniformMatrix4fv(viewMatrixUniform_PSM, false, viewMatrix_PSM);
    gl.uniformMatrix4fv(projectionMatrixUniform_PSM, false, perspectiveProjectionMatrix_PSM);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, stone_texture_PSM);
    gl.uniform1i(textureSamplerUniform_PSM, 0);

    gl.bindVertexArray(vao_PSM);
    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
    gl.bindVertexArray(null);


    gl.useProgram(null);

    update();
    requestAnimationFrame(draw, canvas_PSM);
}

function update() {
    cubeAngle_PSM += 0.5;
    if (cubeAngle_PSM >= 360) {
        cubeAngle_PSM = 0.0
    }

}

function degToRad(degree) {
    return (degree * Math.PI / 180.0);
}

function uninitialize() {

    if (vao_PSM) {
        gl.deleteVertexArray(vao_PSM);
        vao_PSM = null;
    }

    if (vbo_PSM) {
        gl.deleteBuffer(vbo_PSM);
        vbo_PSM = null;
    }

    if (stone_texture_PSM) {
        gl.deleteTexture(stone_texture_PSM);
        stone_texture_PSM = null;
    }

    if (shaderProgramObject_PSM) {

        if (vertexShaderObject_PSM) {
            gl.detachShader(shaderProgramObject_PSM, vertexShaderObject_PSM);
            gl.deleteShader(vertexShaderObject_PSM);
            vertexShaderObject_PSM = null;
        }

        if (fragmentShaderObject_PSM) {
            gl.detachShader(shaderProgramObject_PSM, fragmentShaderObject_PSM);
            gl.deleteShader(fragmentShaderObject_PSM);
            fragmentShaderObject_PSM = null;
        }

        gl.deleteProgram(shaderProgramObject_PSM);
        shaderProgramObject_PSM = null;
    }
}