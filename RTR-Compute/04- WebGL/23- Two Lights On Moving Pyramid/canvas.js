var canvas = null;
var gl = null;
var canvas_original_width;
var canvas_original_height;
var bFullscreen_ak = false;

const WebGLMacros = {
    ATTRIBUTE_VERTEX: 0,
    ATTRIBUTE_COLOR: 1,
    ATTRIBUTE_NORMAL: 2,
    ATTRIBUTE_TEXTURE0: 3,
};

var vertexShaderObject_ak;
var fragmentShaderObject_ak;
var shaderProgramObject_ak;

var vaoPyramid_ak;

var vboPositionPyramid_ak;
var vboNormalsPyramid_ak;

var ldUniform_ak;
var kdUniform_ak;
var lightPositionUniform_ak;

var laUniform_ak;
var kaUniform_ak;

var lsUniform_ak;
var ksUniform_ak;

var shininessUniform_ak;
var singleTapLightUniform_ak;

var  modelUniform_ak;
var  viewUniform_ak;
var  projectionUniform_ak;
var  perspectiveProjectionMatrix_ak;

var bLight_ak = false;

var lightAmbient_ak = [
    0.0, 0.0, 0.0,
    0.0, 0.0, 0.0
];

var lightDiffuse_ak = [
    1.0, 0.0, 0.0,
    0.0, 0.0, 1.0
];

var lightSpecular_ak = [
    1.0, 0.0, 0.0,
    0.0, 0.0, 1.0
];

var lightPosition_ak = [
    2.0, 0.0, 0.0, 1.0, 
    -2.0, 0.0, 0.0, 1.0
];

var materialShininess_ak = 50.0;

var anglePyramid_ak = 0.0;

var requestAnimationFrame = window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame  ||
                            window.mozRequestAnimationFrame ||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var cancelAnimationFrame = window.cancelAnimationFrame || 
                           window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
                           window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
                           window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
                           window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

function main(){

    canvas = document.getElementById("ASK");
    if(!canvas){
        console.log("Obtaining Canvas Failed");
    }
    else{
        // console.log("Obtaining Canvas Succeeded");
    }

    canvas_original_width = canvas.width;    
    canvas_original_height = canvas.height;
    
    // window is built-in variable
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("resize", resize, false);

    init();

    resize(); // warmup resize call

    draw(); // warmup re-paint call

}

function init(){
    gl = canvas.getContext("webgl2");
    if(!gl){
        console.log("Obtaining webgl2 Context Failed");
    }
    else{
        console.log("Obtaining webgl2 Context Succeeded");
    }
    
    gl.viewportWidth  = canvas.width;
    gl.viewportHeight  = canvas.height;

    //Vertex Shader
    var vertexShaderSourceCode = 
    "#version 300 es"+
    "\n"+
    "precision highp float;"+
    "in vec4 vPosition;" +
    "in vec3 vNormal;"  +
    "uniform mat4 u_model_matrix;" +
    "uniform mat4 u_projection_matrix;" +
    "uniform mat4 u_view_matrix;" +
    "uniform lowp int u_single_tap_light;" +
    "uniform vec3 u_Ld[2];" +
    "uniform vec3 u_Kd;" +
    "uniform vec4 u_light_position[2];" +
    "uniform vec3 u_La[2];" +
    "uniform vec3 u_Ls[2];" +
    "uniform vec3 u_Ka;" +
    "uniform vec3 u_Ks;" +
    "uniform float u_shininess;" +
    "out vec3 phong_ads_light;" +
    "void main(void)" +
    "{" +
        "if (u_single_tap_light == 1) " +
        "{" +
            "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" +
            "vec3 transformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" +
            "vec3 view_vector = normalize(-eyeCoordinates.xyz);" +
            "phong_ads_light = vec3(0.0,0.0,0.0);"+
            "vec3 light_direction[2];"+
            "vec3 reflection_vector[2];"+
            "vec3 ambient[2];"+
            "vec3 diffuse[2];"+
            "vec3 specular[2];"+
            "for(int i =0; i<2 ; i++)"+
            "{"+
                "light_direction[i] = normalize(vec3(u_light_position[i] - eyeCoordinates));" +
                "reflection_vector[i] = reflect(-light_direction[i], transformed_normal);" +
                "ambient[i] = u_La[i] * u_Ka;" +
                "diffuse[i] = u_Ld[i] * u_Kd * max(dot(light_direction[i],transformed_normal),0.0);" +
                "specular[i] = u_Ls[i] * u_Ks * pow(max(dot(reflection_vector[i],view_vector),0.0),u_shininess);" +
                "phong_ads_light = phong_ads_light + ambient[i] + diffuse[i] + specular[i];" +
            "}"+
        "}" +
        "else" +
        "{" +
            "phong_ads_light = vec3(1.0f,1.0f,1.0f);" +
        "}"+
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
    "}";

    vertexShaderObject_ak = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject_ak, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject_ak);

    if(gl.getShaderParameter(vertexShaderObject_ak, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(vertexShaderObject_ak);
            if(error.length > 0){
                alert(error);
            }
    }

    //Fragment Shader
    var fragmentShaderSourceCode = 
    "#version 300 es"+
    "\n"+
    "precision highp float;" +
    "out vec4 FragColor;" +
    "in vec3 phong_ads_light;" +
    "void main()"+
    "{" +
        "FragColor = vec4(phong_ads_light, 1.0f);" +
    "}";

    fragmentShaderObject_ak = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject_ak, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject_ak);

    if(gl.getShaderParameter(fragmentShaderObject_ak, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(fragmentShaderObject_ak);
            if(error.length > 0){
                alert(error);
                uninitalize();
            }
    }

    //Shader Program
    shaderProgramObject_ak = gl.createProgram();
    gl.attachShader(shaderProgramObject_ak, vertexShaderObject_ak);
    gl.attachShader(shaderProgramObject_ak, fragmentShaderObject_ak);

    gl.bindAttribLocation(shaderProgramObject_ak, WebGLMacros.ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject_ak, WebGLMacros.ATTRIBUTE_NORMAL, "vNormal");

    //linking
    gl.linkProgram(shaderProgramObject_ak);

    if(gl.getProgramParameter(shaderProgramObject_ak, gl.LINK_STATUS) == false){
        var error = gl.getProgramInfoLog(shaderProgramObject_ak);
            if(error.length > 0){
                alert(error);
                uninitalize();
            }
    }

    //uniforms
    modelUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_model_matrix");
    viewUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_view_matrix");
    projectionUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_projection_matrix");
    ldUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_Ld");
    kdUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_Kd");
    laUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_La");
    kaUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_Ka");
    lsUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_Ls");
    ksUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_Ks");
    shininessUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_shininess");
    lightPositionUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_light_position");
    singleTapLightUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_single_tap_light");
    
    var pyramidVertices = new Float32Array([
		0.0, 1.0, 0.0,
		-1.0, -1.0, 1.0,
		1.0, -1.0, 1.0,

		0.0, 1.0, 0.0,
		1.0, -1.0, 1.0,
		1.0, -1.0, -1.0,

		0.0, 1.0, 0.0,
		1.0, -1.0, -1.0,
		-1.0, -1.0, -1.0,

		0.0, 1.0, 0.0,
		-1.0, -1.0, -1.0,
		-1.0, -1.0, 1.0
    ]);

    var pyramidNormals = new Float32Array([
        0.0, 0.447214, 0.894427,
        0.0, 0.447214, 0.894427,
        0.0, 0.447214, 0.894427,
 
        0.894427, 0.447214, 0.0,
        0.894427, 0.447214, 0.0,
        0.894427, 0.447214, 0.0,

        0.0, 0.447214, -0.894427,
        0.0, 0.447214, -0.894427,
        0.0, 0.447214, -0.894427,

        -0.894427, 0.447214, 0.0,
        -0.894427, 0.447214, 0.0,
        -0.894427, 0.447214, 0.0
    ]);

    
    //vao pyramid
    vaoPyramid_ak = gl.createVertexArray();
    gl.bindVertexArray(vaoPyramid_ak);

    //vbo position 
    vboPositionPyramid_ak = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboPositionPyramid_ak);
    gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    //vbo normals
    vboNormalsPyramid_ak = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboNormalsPyramid_ak);
    gl.bufferData(gl.ARRAY_BUFFER, pyramidNormals, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.ATTRIBUTE_NORMAL);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
    gl.clearDepth(1.0);

    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    perspectiveProjectionMatrix_ak = mat4.create();
}

function resize(){
    if (bFullscreen_ak == true){
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else{
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    gl.viewport(0, 0, canvas.width,canvas.height);

    //perspective call
    mat4.perspective(perspectiveProjectionMatrix_ak, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
    
}

function draw(){
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObject_ak);

    if(bLight_ak == true){
        gl.uniform1i(singleTapLightUniform_ak, 1);
    }
    else{
        gl.uniform1i(singleTapLightUniform_ak, 0);
    }

    gl.uniform3fv(laUniform_ak, lightAmbient_ak);
    gl.uniform3fv(kaUniform_ak, [0.0, 0.0, 0.0]);
    gl.uniform3fv(ldUniform_ak, lightDiffuse_ak);
    gl.uniform3fv(kdUniform_ak, [1.0, 1.0, 1.0]);
    gl.uniform3fv(lsUniform_ak, lightSpecular_ak);
    gl.uniform3fv(ksUniform_ak, [1.0, 1.0, 1.0]);

    gl.uniform4fv(lightPositionUniform_ak, lightPosition_ak);

    gl.uniform1f(shininessUniform_ak, materialShininess_ak);

    var modelMatrix_ak = mat4.create();
    var viewMatrix_ak = mat4.create();

    mat4.translate(modelMatrix_ak, modelMatrix_ak, [0.0, 0.0, -5.0]);
     
    mat4.rotateY(modelMatrix_ak, modelMatrix_ak, degToRad(anglePyramid_ak));

    gl.uniformMatrix4fv(modelUniform_ak, false, modelMatrix_ak);
    gl.uniformMatrix4fv(viewUniform_ak, false, viewMatrix_ak);
    gl.uniformMatrix4fv(projectionUniform_ak, false, perspectiveProjectionMatrix_ak);

    gl.bindVertexArray(vaoPyramid_ak);

    gl.drawArrays(gl.TRIANGLES, 0, 12);

    gl.bindVertexArray(null);

    gl.useProgram(null);

    update();

    requestAnimationFrame(draw, canvas);
}

function update(){

    anglePyramid_ak += 0.5;
    if(anglePyramid_ak >= 360)
        anglePyramid_ak = 0.0

}

function degToRad(degree) {
    return (degree * Math.PI / 180.0);
}

function toggleFullscreen(){

    var fullscreen_element = document.fullscreenElement || 
                             document.webkitFullscreenElement ||
                             document.mozFullScreenElement ||
                             document.msFullscreenElement ||
                             null ;

    if (fullscreen_element == null) {
      bFullscreen_ak = true;

      if (canvas.requestFullscreen) {
        canvas.requestFullscreen();
      } else if (canvas.webkitRequestFullscreen) {
        canvas.webkitRequestFullscreen();
      } else if (canvas.mozRequestFullScreenElement) {
        canvas.mozRequestFullScreenElement();
      } else if (canvas.msRequestFullscreen) {
        canvas.msRequestFullscreen();
      }
    } else {
      bFullscreen_ak = false;

      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      }
    }
}

function keyDown(event){
    switch(event.keyCode){
        case 70:
            toggleFullscreen();
            break;
        
        case 27:
            uninitalize();
            window.close();
            break;
        
        case 76:
            if(bLight_ak == false)
                bLight_ak = true;
            else
                bLight_ak = false;
    }
}

function uninitalize(){

    if(vaoPyramid_ak){
        gl.deleteVertexArray(vaoPyramid_ak);
        vaoPyramid_ak = null;
    }

    if(vboPositionPyramid_ak){
        gl.deleteBuffer(vboPositionPyramid_ak);
        vboPositionPyramid_ak = null;
    }

    if(vboNormalsPyramid_ak){
        gl.deleteBuffer(vboNormalsPyramid_ak);
        vboNormalsPyramid_ak = null;
    }

    if(shaderProgramObject_ak){

        if(vertexShaderObject_ak){
            gl.detachShader(shaderProgramObject_ak, vertexShaderObject_ak);
            gl.deleteShader(vertexShaderObject_ak);
            vertexShaderObject_ak = null;
        }

        if(fragmentShaderObject_ak){
            gl.detachShader(shaderProgramObject_ak, fragmentShaderObject_ak);
            gl.deleteShader(fragmentShaderObject_ak);
            fragmentShaderObject_ak = null;
        }

        gl.deleteProgram(shaderProgramObject_ak);
        shaderProgramObject_ak = null;
    }
}
