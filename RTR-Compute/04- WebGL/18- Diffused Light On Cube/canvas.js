var canvas = null;
var gl = null;
var canvas_original_width;
var canvas_original_height;
var bFullscreen_ak = false;

const WebGLMacros = {
    ATTRIBUTE_VERTEX: 0,
    ATTRIBUTE_COLOR: 1,
    ATTRIBUTE_NORMAL: 2,
    ATTRIBUTE_TEXTURE0: 3,
};

var vertexShaderObject_ak;
var fragmentShaderObject_ak;
var shaderProgramObject_ak;

var vaoCube_ak;

var vboPositionCube_ak;
var vboColorCube_ak;
var vboNormalCube_ak;

var modelViewMatrixUniform_ak;
var modelViewProjectionMatrixUniform_ak;
var ldUniform_ak;
var kdUniform_ak;
var lightPositionUniform_ak;
var keyPressed_l_Uniform_ak;

var bLight_ak = false;
var bAnimate_ak = false;

var perspectiveProjectionMatrix_ak;

var angleCube_ak = 0.0;

var requestAnimationFrame = window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame  ||
                            window.mozRequestAnimationFrame ||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame;

var cancelAnimationFrame = window.cancelAnimationFrame || 
                           window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
                           window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
                           window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
                           window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

function main(){

    canvas = document.getElementById("ASK");
    if(!canvas){
        console.log("Obtaining Canvas Failed");
    }
    else{
        // console.log("Obtaining Canvas Succeeded");
    }

    canvas_original_width = canvas.width;    
    canvas_original_height = canvas.height;
    
    // window is built-in variable
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("resize", resize, false);

    init();

    resize(); // warmup resize call

    draw(); // warmup re-paint call

}

function init(){
    gl = canvas.getContext("webgl2");
    if(!gl){
        console.log("Obtaining webgl2 Context Failed");
    }
    else{
        console.log("Obtaining webgl2 Context Succeeded");
    }
    
    gl.viewportWidth  = canvas.width;
    gl.viewportHeight  = canvas.height;

    //Vertex Shader
    var vertexShaderSourceCode = 
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;" +
    "in vec3 vNormal;" +
    "uniform mat4 u_model_view_matrix;" +
    "uniform mat4 u_projection_matrix;" +
    "uniform lowp int l_key_pressed;"+
    "uniform vec3 u_Ld;"+
    "uniform vec3 u_Kd;"+
    "uniform vec4 u_light_position;"+
    "out vec3 diffuse_light;" +
    "void main()" +
    "{" +
    "if (l_key_pressed == 1) " +
        "{" +
            "vec4 eyeCoordinates = u_model_view_matrix * vPosition;" +
            "mat3 normalMatrix = mat3(transpose(inverse(u_model_view_matrix)));" +
            "vec3 tnorm = normalize(normalMatrix * vNormal);" +
            "vec3 s = normalize(vec3(u_light_position - eyeCoordinates));" +
            "diffuse_light = u_Ld * u_Kd * max(dot(s, tnorm), 0.0);" +
        "}" +
    "gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;" +
   "}";

    vertexShaderObject_ak = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject_ak, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject_ak);

    if(gl.getShaderParameter(vertexShaderObject_ak, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(vertexShaderObject_ak);
            if(error.length > 0){
                alert(error);
            }
    }

    //Fragment Shader
    var fragmentShaderSourceCode = 
    "#version 300 es"+
    "\n"+
    "precision highp float;" +
    "precision lowp int;" +
    "out vec4 FragColor;" +
    "in vec3 diffuse_light;" +
    "uniform int l_key_pressed;" +
    "void main()"+
    "{" +
        "vec4 color;" +
        "if (l_key_pressed == 1) " +
        "{"+
            "color = vec4(diffuse_light, 1.0);"  +
        "}"+
        "else" +
        "{" +
            "color = vec4(1.0,1.0,1.0,1.0);" +
        "}" +
        "FragColor = color;" +
    "}";

    fragmentShaderObject_ak = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject_ak, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject_ak);

    if(gl.getShaderParameter(fragmentShaderObject_ak, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(fragmentShaderObject_ak);
            if(error.length > 0){
                alert(error);
                uninitalize();
            }
    }

    //Shader Program
    shaderProgramObject_ak = gl.createProgram();
    gl.attachShader(shaderProgramObject_ak, vertexShaderObject_ak);
    gl.attachShader(shaderProgramObject_ak, fragmentShaderObject_ak);

    gl.bindAttribLocation(shaderProgramObject_ak, WebGLMacros.ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject_ak, WebGLMacros.ATTRIBUTE_NORMAL, "vNormal");

    //linking
    gl.linkProgram(shaderProgramObject_ak);

    if(gl.getProgramParameter(shaderProgramObject_ak, gl.LINK_STATUS) == false){
        var error = gl.getProgramInfoLog(shaderProgramObject_ak);
            if(error.length > 0){
                alert(error);
                uninitalize();
            }
    }

    //uniforms
    modelViewMatrixUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_model_view_matrix");
    modelViewProjectionMatrixUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_projection_matrix");
    ldUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_Ld");
    kdUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_Kd");
    lightPositionUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_light_position");
    keyPressed_l_Uniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "l_key_pressed");
    
    var cubeVertices = new Float32Array([
		1.0, 1.0, -1.0,
		-1.0, 1.0, -1.0,
		-1.0, 1.0, 1.0,
		1.0, 1.0, 1.0,

		1.0, -1.0, 1.0,
		-1.0, -1.0, 1.0,
		-1.0, -1.0, -1.0,
		1.0, -1.0, -1.0,

		1.0, 1.0, 1.0,
		-1.0, 1.0, 1.0,
		-1.0, -1.0, 1.0,
		1.0, -1.0, 1.0,

		1.0, -1.0, -1.0,
		-1.0, -1.0, -1.0,
		-1.0, 1.0, -1.0,
		1.0, 1.0, -1.0,

		-1.0, 1.0, 1.0,
		-1.0, 1.0, -1.0,
		-1.0, -1.0, -1.0,
		-1.0, -1.0, 1.0,

		1.0, 1.0, -1.0,
		1.0, 1.0, 1.0,
		1.0, -1.0, 1.0,
		1.0, -1.0, -1.0,	
    ]);

    var cubeNormals = new Float32Array([
        0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,

		0.0, -1.0, 0.0,
		0.0, -1.0, 0.0,
		0.0, -1.0, 0.0,
		0.0, -1.0, 0.0,
		
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,

		0.0, 0.0, -1.0,
		0.0, 0.0, -1.0,
		0.0, 0.0, -1.0,
		0.0, 0.0, -1.0,
		
		-1.0, 0.0, 0.0,
		-1.0, 0.0, 0.0,
		-1.0, 0.0, 0.0,
		-1.0, 0.0, 0.0,

		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0
    ]);

    //vao cube
    vaoCube_ak = gl.createVertexArray();
    gl.bindVertexArray(vaoCube_ak);

    //vbo position 
    vboPositionCube_ak = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboPositionCube_ak);
    gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    //vbo normal
    vboNormalCube_ak = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboNormalCube_ak);
    gl.bufferData(gl.ARRAY_BUFFER, cubeNormals, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.ATTRIBUTE_NORMAL);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
    gl.clearDepth(1.0);


    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    perspectiveProjectionMatrix_ak = mat4.create();
}

function resize(){
    if (bFullscreen_ak == true){
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else{
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    gl.viewport(0, 0, canvas.width,canvas.height);

    //perspective call
    mat4.perspective(perspectiveProjectionMatrix_ak, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
    
}

function draw(){
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObject_ak);

    if(bLight_ak){
        gl.uniform1i(keyPressed_l_Uniform_ak, 1);
		gl.uniform3f(ldUniform_ak, 1.0, 1.0, 1.0);
		gl.uniform3f(kdUniform_ak, 0.5, 0.5, 0.5);

		gl.uniform4fv(lightPositionUniform_ak, [0.0, 0.0, 2.0, 1.0]);
    }
    else
        gl.uniform1i(keyPressed_l_Uniform_ak, 0);

    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -5.0]);

    mat4.rotateX(modelViewMatrix, modelViewMatrix, degToRad(angleCube_ak));
    mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angleCube_ak));
    mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(angleCube_ak));

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix_ak, modelViewMatrix );

    gl.uniformMatrix4fv(modelViewMatrixUniform_ak, false, modelViewMatrix);
	gl.uniformMatrix4fv(modelViewProjectionMatrixUniform_ak, false, perspectiveProjectionMatrix_ak);

    gl.bindVertexArray(vaoCube_ak);

    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);

    gl.bindVertexArray(null);

    gl.useProgram(null);

    if(bAnimate_ak)
        update();

    requestAnimationFrame(draw, canvas);
}

function update(){
    angleCube_ak += 1;
    if(angleCube_ak >= 360)
        angleCube_ak = 0
}

function degToRad(degree) {
    return (degree * Math.PI / 180.0);
}

function toggleFullscreen(){

    var fullscreen_element = document.fullscreenElement || 
                             document.webkitFullscreenElement ||
                             document.mozFullScreenElement ||
                             document.msFullscreenElement ||
                             null ;

    if (fullscreen_element == null) {
      bFullscreen_ak = true;

      if (canvas.requestFullscreen) {
        canvas.requestFullscreen();
      } else if (canvas.webkitRequestFullscreen) {
        canvas.webkitRequestFullscreen();
      } else if (canvas.mozRequestFullScreenElement) {
        canvas.mozRequestFullScreenElement();
      } else if (canvas.msRequestFullscreen) {
        canvas.msRequestFullscreen();
      }
    } else {
      bFullscreen_ak = false;

      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      }
    }
}

function keyDown(event){
    switch(event.keyCode){
        case 70:
            toggleFullscreen();
            break;
        
        case 27:
            uninitalize();
            window.close();
            break;
        
        case 76:
            if(bLight_ak == false){
                bLight_ak = true;
            }
            else{
                bLight_ak = false;
            }
            break;

        case 65:
            if(bAnimate_ak == false){
                bAnimate_ak = true;
            }
            else{
                bAnimate_ak = false;
            }
            break;
    }
}

function uninitalize(){

    if(vaoCube_ak){
        gl.deleteVertexArray(vaoCube_ak);
        vaoCube_ak = null;
    }

    if(vboPositionCube_ak){
        gl.deleteBuffer(vboPositionCube_ak);
        vboPositionCube_ak = null;
    }

    if(vboNormalCube_ak){
        gl.deleteBuffer(vboNormalCube_ak);
        vboNormalCube_ak = null;
    }

    if(shaderProgramObject_ak){

        if(vertexShaderObject_ak){
            gl.detachShader(shaderProgramObject_ak, vertexShaderObject_ak);
            gl.deleteShader(vertexShaderObject_ak);
            vertexShaderObject_ak = null;
        }

        if(fragmentShaderObject_ak){
            gl.detachShader(shaderProgramObject_ak, fragmentShaderObject_ak);
            gl.deleteShader(fragmentShaderObject_ak);
            fragmentShaderObject_ak = null;
        }

        gl.deleteProgram(shaderProgramObject_ak);
        shaderProgramObject_ak = null;
    }
}
