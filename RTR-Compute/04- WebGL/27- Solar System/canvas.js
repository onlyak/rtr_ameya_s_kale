var canvas_ak = null;
var gl = null;
var bFullscreen_ak = false;
var canvas_original_width_ak;
var canvas_original_height_ak;

const WebGLMacros = {
    ATTRIBUTE_VERTEX: 0,
    ATTRIBUTE_COLOR: 1,
    ATTRIBUTE_NORMAL: 2,
    ATTRIBUTE_TEXTURE: 3,
};

var vertexShaderObject_ak;
var fragmentShaderObject_ak;
var shaderProgramObject_ak;

var sphere_ak;

var mvpUniform_ak;
var perspectiveProjectionMatrix_ak;
var colorUniform_ak;
var day_ak = 0;
var year_ak = 0;
var hour_ak = 0;

var requestAnimationFrame_ak = window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame;

function main() {
    canvas_ak = document.getElementById("ASK");
    if (!canvas_ak) {
        console.log("Obtaining canvas_ak failed");
        return;
    } else {
        console.log("Obtaining canvas_ak successful");
    }

    console.log("Canvas width = " + canvas_ak.width + " Canvas height = " + canvas_ak.height);
    canvas_original_width_ak = canvas_ak.width;
    canvas_original_height_ak = canvas_ak.height;

    window.addEventListener('keydown', keyDown, false);
    window.addEventListener('resize', resize, false);

    init();
    resize();
    draw();
}

function toggleFullscreen() {

    var fullscreen_element_ak = document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement ||
        null;

    if (fullscreen_element_ak == null) {
        if (canvas_ak.requestFullscreen) {
            canvas_ak.requestFullscreen();
        }
        else if (canvas_ak.webkitRequestFullscreen) {
            canvas_ak.webkitRequestFullscreen();
        }
        else if (canvas_ak.mozRequestFullScreenElement) {
            canvas_ak.mozRequestFullScreenElement();
        }
        else if (canvas_ak.msRequestFullscreen) {
            canvas_ak.msRequestFullscreen();
        }
        bFullscreen_ak = true;
    }
    else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
        else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
        else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        }
        else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
        bFullscreen_ak = false;
    }
}

function keyDown(event) {
    switch (event.keyCode) {
        case 70:
            {
                toggleFullscreen();
                break;
            }
        case 27:
            {
                uninitialize();
                window.close();
                break;
            }
        case 68:
            day_ak = (day_ak + 6) % 360;
            hour_ak = (hour_ak + 1) % 360;
            break;
        case 100:
            day_ak = (day_ak - 6) % 360;
            hour_ak = (hour_ak - 1) % 360;
            break;
        case 89:
            year_ak = (year_ak + 3) % 360;
            day_ak = (day_ak - 6) % 360;
            hour_ak = (hour_ak + 1) % 360;
            break;
        case 121:
            year_ak = (year_ak - 3) % 360;
            day_ak = (day_ak + 6) % 360;
            hour_ak = (hour_ak - 1) % 360;
            break;
        case 72:
            hour_ak = (hour_ak + 1) % 360;
            break;
        case 104:
            hour_ak = (hour_ak - 1) % 360;
            break;
    }
}

function init() {
    gl = canvas_ak.getContext("webgl2");
    if (!gl) {
        console.log("Obtaining webgl2 context failed");
        return;
    } else {
        console.log("Obtaining webgl2 context successful");
    }

    gl.viewportWidth = canvas_ak.width;
    gl.viewportHeight = canvas_ak.height;

    var vertexShaderSourceCode_ak =
        "#version 300 es" +
        "\n" +
        "in vec4 vPosition;" +
        "uniform mat4 u_mvp_matrix;" +
        "void main()" +
        "{" +
        "gl_Position = u_mvp_matrix * vPosition;" +
        "}";

    vertexShaderObject_ak = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject_ak, vertexShaderSourceCode_ak);
    gl.compileShader(vertexShaderObject_ak);

    if (gl.getShaderParameter(vertexShaderObject_ak, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObject_ak);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    var fragmentShaderSourceCode_ak =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "out vec4 FragColor;" +
        "uniform vec3 u_color;" +
        "void main()" +
        "{" +
        "FragColor = vec4(u_color, 1.0);" +
        "}";

    fragmentShaderObject_ak = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject_ak, fragmentShaderSourceCode_ak);
    gl.compileShader(fragmentShaderObject_ak);

    if (gl.getShaderParameter(fragmentShaderObject_ak, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(fragmentShaderObject_ak);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    shaderProgramObject_ak = gl.createProgram();
    gl.attachShader(shaderProgramObject_ak, vertexShaderObject_ak);
    gl.attachShader(shaderProgramObject_ak, fragmentShaderObject_ak);

    gl.bindAttribLocation(shaderProgramObject_ak, WebGLMacros.ATTRIBUTE_VERTEX, "vPosition");

    gl.linkProgram(shaderProgramObject_ak);

    if (gl.getProgramParameter(shaderProgramObject_ak, gl.LINK_STATUS) == false) {
        var error = gl.getProgramInfoLog(shaderProgramObject_ak);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    mvpUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_mvp_matrix");
    colorUniform_ak = gl.getUniformLocation(shaderProgramObject_ak, "u_color");

    sphere_ak = new Mesh();
    makeSphere(sphere_ak, 0.6, 30, 30);

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.clearDepth(1.0);

    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    perspectiveProjectionMatrix_ak = mat4.create();
}

function resize() {
    if (bFullscreen_ak == true) {
        canvas_ak.width = window.innerWidth;
        canvas_ak.height = window.innerHeight;
    }
    else {
        canvas_ak.width = canvas_original_width_ak;
        canvas_ak.height = canvas_original_height_ak;
    }

    gl.viewport(0, 0, canvas_ak.width, canvas_ak.height);

    mat4.perspective(perspectiveProjectionMatrix_ak,
        45.0,
        parseFloat(canvas_ak.width) / parseFloat(canvas_ak.height),
        0.1,
        100.0
    );
}

function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.useProgram(shaderProgramObject_ak);

    var modelViewMatrix1_ak = mat4.create();
    var modelViewMatrix2_ak = mat4.create();
    var modelViewMatrix3_ak = mat4.create();
    var modelViewProjectionMatrix_ak = mat4.create();
    var translationMatrix_ak = mat4.create();
    var xRotationMatrix_ak = mat4.create();
    var yRotationMatrix_ak = mat4.create();
    var zRotationMatrix_ak = mat4.create();
    var scaleMatrix_ak = mat4.create();

    mat4.translate(translationMatrix_ak, translationMatrix_ak, [0.0, 0.0, -6.0]);
    mat4.multiply(modelViewMatrix1_ak, modelViewMatrix1_ak, translationMatrix_ak);
    mat4.multiply(modelViewProjectionMatrix_ak, perspectiveProjectionMatrix_ak, modelViewMatrix1_ak);
    gl.uniformMatrix4fv(mvpUniform_ak, false, modelViewProjectionMatrix_ak);

    var sunColor_ak = new Float32Array([1.0, 1.0, 0.0]);
    gl.uniform3fv(colorUniform_ak, sunColor_ak);
    sphere_ak.draw();

    mat4.rotateY(yRotationMatrix_ak, yRotationMatrix_ak, degToRad(year_ak));
    mat4.translate(translationMatrix_ak, translationMatrix_ak, [2.7, 0.0, 0.0]);
    mat4.multiply(modelViewMatrix2_ak, modelViewMatrix1_ak, yRotationMatrix_ak);
    mat4.multiply(modelViewMatrix2_ak, modelViewMatrix2_ak, translationMatrix_ak);

    mat4.rotateY(yRotationMatrix_ak, yRotationMatrix_ak, degToRad(day_ak));
    mat4.scale(scaleMatrix_ak, scaleMatrix_ak, [0.5, 0.5, 0.5]);
    mat4.multiply(modelViewMatrix3_ak, modelViewMatrix2_ak, yRotationMatrix_ak);
    mat4.multiply(modelViewMatrix3_ak, modelViewMatrix3_ak, scaleMatrix_ak);

    mat4.multiply(modelViewProjectionMatrix_ak, perspectiveProjectionMatrix_ak, modelViewMatrix3_ak);
    gl.uniformMatrix4fv(mvpUniform_ak, false, modelViewProjectionMatrix_ak);

    var earthColor_ak = new Float32Array([0.4, 0.9, 1.0]);
    gl.uniform3fv(colorUniform_ak, earthColor_ak);
    sphere_ak.draw();

    var translationMatrix_ak = mat4.create();
    var xRotationMatrix_ak = mat4.create();
    var yRotationMatrix_ak = mat4.create();
    var zRotationMatrix_ak = mat4.create();
    var scaleMatrix_ak = mat4.create();

    mat4.rotateY(yRotationMatrix_ak, yRotationMatrix_ak, degToRad(day_ak));
    mat4.translate(translationMatrix_ak, translationMatrix_ak, [0.7, 0.0, 0.0]);
    mat4.multiply(modelViewMatrix3_ak, modelViewMatrix2_ak, yRotationMatrix_ak);
    mat4.multiply(modelViewMatrix3_ak, modelViewMatrix3_ak, translationMatrix_ak);

    mat4.rotateY(yRotationMatrix_ak, yRotationMatrix_ak, degToRad(hour_ak));
    mat4.scale(scaleMatrix_ak, scaleMatrix_ak, [0.3, 0.3, 0.3]);
    mat4.multiply(modelViewMatrix3_ak, modelViewMatrix3_ak, yRotationMatrix_ak);
    mat4.multiply(modelViewMatrix3_ak, modelViewMatrix3_ak, scaleMatrix_ak);
    mat4.multiply(modelViewProjectionMatrix_ak, perspectiveProjectionMatrix_ak, modelViewMatrix3_ak);
    gl.uniformMatrix4fv(mvpUniform_ak, false, modelViewProjectionMatrix_ak);

    var moonColor_ak = new Float32Array([1.0, 1.0, 1.0]);
    gl.uniform3fv(colorUniform_ak, moonColor_ak);
    sphere_ak.draw();

    var translationMatrix_ak = mat4.create();
    var xRotationMatrix_ak = mat4.create();
    var yRotationMatrix_ak = mat4.create();
    var zRotationMatrix_ak = mat4.create();
    var scaleMatrix_ak = mat4.create();

    mat4.multiply(modelViewProjectionMatrix_ak, perspectiveProjectionMatrix_ak, modelViewMatrix2_ak);
    gl.uniformMatrix4fv(mvpUniform_ak, false, modelViewProjectionMatrix_ak);

    gl.useProgram(null);

    requestAnimationFrame(draw, canvas_ak);
}

function degToRad(degree) {
    return (degree * Math.PI / 180.0);
}

function uninitialize() {

    if (shaderProgramObject_ak) {

        if (vertexShaderObject_ak) {
            gl.detachShader(shaderProgramObject_ak, vertexShaderObject_ak);
            gl.deleteShader(vertexShaderObject_ak);
            vertexShaderObject_ak = null;
        }

        if (fragmentShaderObject_ak) {
            gl.detachShader(shaderProgramObject_ak, fragmentShaderObject_ak);
            gl.deleteShader(fragmentShaderObject_ak);
            fragmentShaderObject_ak = null;
        }

        gl.deleteProgram(shaderProgramObject_ak);
        shaderProgramObject_ak = null;
    }
}