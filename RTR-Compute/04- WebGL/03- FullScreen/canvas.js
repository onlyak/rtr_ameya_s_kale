var canvas = null;
var context = null;

function main(){

    canvas = document.getElementById("ASK");
    if(!canvas){
        console.log("Obtaining Canvas Failed");
    }
    else{
        console.log("Obtaining Canvas Succeeded");
    }

    console.log("Canvas Width = "+canvas.width+ " And Canvas Height = "+canvas.height);
    
    context = canvas.getContext("2d");
    if(!context){
        console.log("Obtaining Context Failed");
    }
    else{
        console.log("Obtaining Context Succeeded");
    }

    context.fillStyle = "black";
    context.fillRect(0,0, canvas.width, canvas.height);

    drawText("Hello World");
    
    // window is built-in variable
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);

}

function drawText(text){
    context.textAlign = "center";
    context.font = "48px sans-serif";   
    context.fillStyle = "white";
    context.fillText(text, canvas.width/2, canvas.height/2);
}

function toggleFullscreen(){

    var fullscreen_element = document.fullscreenElement || 
                             document.webkitFullscreenElement ||
                             document.mozFullScreenElement ||
                             document.msFullscreenElement ||
                             null ;

    if(fullscreen_element == null){
        if (canvas.requestFullscreen){
            canvas.requestFullscreen();
        }
        else if(canvas.webkitRequestFullscreen){
            canvas.webkitRequestFullscreen();
        }
        else if(canvas.mozRequestFullScreenElement){
            canvas.mozRequestFullScreenElement();
        }
        else if(canvas.msRequestFullscreen){
            canvas.msRequestFullscreen();
        }
    }
    else{
        if(document.exitFullscreen){
            document.exitFullscreen();
        }
        else if(document.webkitExitFullscreen){
            document.webkitExitFullscreen();
        }
        else if(document.mozCancelFullScreen){
            document.mozCancelFullScreen();
        }
        else if(document.msExitFullscreen){
            document.msExitFullscreen();
        }
    }
}

function keyDown(event){
    switch(event.keyCode){
        case 70:
            toggleFullscreen();
            drawText("Hello World"); // no re-paint event in javascript
            break;
    }
}

function mouseDown(){}
