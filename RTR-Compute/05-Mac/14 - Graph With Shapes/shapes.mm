#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h> // similar to windows.h
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h> // GL.h
#include "vmath.h"

using namespace vmath;

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

FILE *gpFile_ak=NULL;

GLuint Vbo_position_ak;
GLuint Vbo_color_ak;

// forward declaration
@interface AppDelegate: NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

int main(int argc,const char* argv[])
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSApp = [NSApplication sharedApplication];
    [NSApp setDelegate:[[AppDelegate alloc] init]];
    [NSApp run];
    [pool release];
    return(0);
}

@interface MyOpenGLView : NSOpenGLView
@end

@implementation AppDelegate
{
    @private
    NSWindow *window;
    MyOpenGLView *myOpenGLView_ak;
}

// WM_CREATE
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSBundle *appBundle_ak = [NSBundle mainBundle];
    NSString *appDirPath_ak =[appBundle_ak bundlePath];
    NSString *parentDirPath_ak = [appDirPath_ak stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath_ak = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath_ak];

    const char *pszLogFileNameWithPath=[logFileNameWithPath_ak cStringUsingEncoding:NSASCIIStringEncoding];

    gpFile_ak=fopen(pszLogFileNameWithPath,"w");
    if(gpFile_ak==NULL){
        [self release];
        [NSApp terminate:self];
    }

    fprintf(gpFile_ak,"Program Started Successfully\n");

    NSRect win_rect_ak; // internally CGRect structure
    win_rect_ak = NSMakeRect(0.0, 0.0, 800.0, 600.0); // NSPoint(x, y) NSSize(width, height) from C-Library

    window=[[NSWindow alloc]
        initWithContentRect    :    win_rect_ak
                styleMask    :    NSWindowStyleMaskTitled |
                                NSWindowStyleMaskClosable |
                                NSWindowStyleMaskMiniaturizable |
                                NSWindowStyleMaskResizable
                backing        :     NSBackingStoreBuffered
                defer        :    NO    ];
    [window setTitle:@"ASK: macOS Graph With Shapes"];
    [window center];

    myOpenGLView_ak = [[MyOpenGLView alloc]initWithFrame:win_rect_ak];
    [window setContentView:myOpenGLView_ak];

    [window setDelegate:self];
    [window makeKeyAndOrderFront:self]; // setFocus, setForeGroundWindow
}

// uninitialize
-(void) applicationWillTerminate:(NSNotification *)aNotification
{
    if(gpFile_ak) {
        fprintf(gpFile_ak,"Program terminated successfully\n");
        fclose(gpFile_ak);
        gpFile_ak=NULL;
    }
}

// NSWindowDelegate's method
-(void) windowWillClose:(NSNotification *)aNotification
{
    [NSApp terminate:self];
}

-(void)dealloc
{
    [myOpenGLView_ak release];
    [window release];
    [super dealloc]; // bubbling de-allocation
}

@end

@implementation MyOpenGLView
{
    @private
    CVDisplayLinkRef displayLink_ak;
    // global variables here
    enum {
        ATTRIBUTE_POSITION = 0,
        ATTRIBUTE_COLOR,
        ATTRIBUTE_NORMAL,
        ATTRIBUTE_TEXCOORD
    };
    GLuint gVertexShaderObject_ak;
    GLuint gFragmentShaderObject_ak;
    GLuint gShaderProgramObject_ak;

    GLuint mvpMatrixUniform_ak;
    GLuint Vao_ak;

    vmath::mat4 perspectiveProjectionMatrix_ak;
}

// id - returns object of any class
-(id) initWithFrame:(NSRect) frame
{
    self = [super initWithFrame: frame];
    if(self)
    {
        // pfd of windows
        NSOpenGLPixelFormatAttribute attributes_ak[] = {
            NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0
        };

        NSOpenGLPixelFormat *pixelFormat= [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes_ak]autorelease];
        if(pixelFormat==nil)
        {
            fprintf(gpFile_ak,"OpenGL pixel format error.\n");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *openGLContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:openGLContext];
    }
    return(self);
}

-(CVReturn) getFrameForTime:(const CVTimeStamp *)outputTime {
    // Multithreaded
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

-(void) prepareOpenGL
{
    [super prepareOpenGL];
    [[self openGLContext]makeCurrentContext];

    fprintf(gpFile_ak,"OpenGL version : %s\n",glGetString(GL_VERSION));
    fprintf(gpFile_ak,"GLSL version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

    // swap interval to avoid tearing
    GLint swapInt_ak=1;

    [[self openGLContext]setValues:&swapInt_ak forParameter:NSOpenGLCPSwapInterval];

    gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
        const GLchar *vertexShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec4 vColor;" \
        "out vec4 out_color;" \
        "uniform mat4 u_mvpMatrix;" \
        "void main(void)" \
        "{" \
        "gl_Position = u_mvpMatrix * vPosition;" \
        "out_color = vColor;" \
        "}";
        
        glShaderSource(gVertexShaderObject_ak,1,(const GLchar **)&vertexShaderSourceCode, NULL);

        // compile Vertex Shader
        glCompileShader(gVertexShaderObject_ak);

        // Error checking for Vertex Shader
        GLint infoLogLength = 0;
        GLint shaderCompiledStatus = 0;
        char *szBuffer = NULL;

        glGetShaderiv(gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
        if(shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
            if(infoLogLength>0)
            {
                szBuffer = (char *)malloc(infoLogLength);
                if(szBuffer!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gVertexShaderObject_ak, infoLogLength, &written, szBuffer);
                    fprintf(gpFile_ak, "Vertex Shader Compilation Log: %s\n",szBuffer);
                    free(szBuffer);
                    szBuffer = NULL;
                }
            }
        }

        //Fragment Shader
        /* out_color is the output of Vertex Shader */
        gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar *vertexFragmentSourceCode =
        "#version 410" \
        "\n" \
        "in vec4 out_color;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "FragColor = out_color;" \
        "}";

        glShaderSource(gFragmentShaderObject_ak,1,(const GLchar **)&vertexFragmentSourceCode, NULL);

        // compile Fragment Shader
        glCompileShader(gFragmentShaderObject_ak);

        // Error Checking for Fragment Shader
        glGetShaderiv(gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
        if(shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
            if(infoLogLength>0)
            {
                szBuffer = (char *)malloc(infoLogLength);
                if(szBuffer!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gFragmentShaderObject_ak, infoLogLength, &written, szBuffer);
                    fprintf(gpFile_ak, "Fragment Shader Compilation Log: %s\n",szBuffer);
                    free(szBuffer);
                    szBuffer = NULL;
                }
            }
        }

        //Shader Program
        gShaderProgramObject_ak = glCreateProgram();
        glAttachShader(gShaderProgramObject_ak,gVertexShaderObject_ak);
        glAttachShader(gShaderProgramObject_ak,gFragmentShaderObject_ak);

        // Bind the attributes in shader with the enums in your main program
        /* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
        glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");

        // For Color Attribute
        glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_COLOR, "vColor");

        glLinkProgram(gShaderProgramObject_ak);

        // Linking Error Checking
        GLint shaderProgramLinkStatus = 0;
        szBuffer = NULL;

        glGetProgramiv(gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
        if(shaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
            if(infoLogLength>0)
            {
                szBuffer = (char *)malloc(infoLogLength);
                if(szBuffer!=NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(gShaderProgramObject_ak, infoLogLength, &written, szBuffer);
                    fprintf(gpFile_ak, "Shader Program Link Log: %s\n",szBuffer);
                    free(szBuffer);
                    szBuffer = NULL;
                }
            }
        }

        //Get the information of uniform Post linking
        mvpMatrixUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_mvpMatrix");

        //Vertices Array Declaration
        const GLfloat triangleVertices[]=
        {   0.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, 0.0f,
            1.0f, -1.0f, 0.0f
        };

        //Color Array

        const GLfloat triangleColors[]=
        {
            1.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 1.0f
        };

        //Repeat the below steps of Vbo_position and call them in draw method
        glGenVertexArrays(1, &Vao_ak);
        glBindVertexArray(Vao_ak);

        // Push the above vertices to vPosition

        //Steps
        /* 1. Tell OpenGl to create one buffer in your VRAM
              Give me a symbol to identify. It is known as NamedBuffer
              In OpenGL terminology, it is called as GL_ARRAY_BUFFER. This is becase vertex has plenty of attributes
              like color, texture, etc. Also it requires contiguous memory.
              User identifies this variable as Vbo_position and GPU as GL_ARRAY_BUFFER.
           2. Bind with the above symbol. It doesn't unbind until 'unbind step' is performed eg- Railway track
           3. Insert triangle data into the buffer.
           4. Specify where to insert this data into shader and also how to use it.
           5. Enable the 'in' point.
           6. Unbind
        */
        glGenBuffers(1, &Vbo_position_ak);
        glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_ak);
        glBufferData(GL_ARRAY_BUFFER, 6*sizeof(float), NULL, GL_DYNAMIC_DRAW);
        // 3 is specified for 3 pairs for triangle vertices
        /* For Texture, specify 2*/
        // 4th parameter----> Normalized Co-ordinates
        // 5th How many strides to take?
        // 6th From which position
        glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);  //change tracks to link different attributes

        // Push color to vColor

        glGenBuffers(1, &Vbo_color_ak);
        glBindBuffer(GL_ARRAY_BUFFER, Vbo_color_ak);
        glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), NULL, GL_DYNAMIC_DRAW);
        glVertexAttribPointer(ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindVertexArray(0);

        glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glEnable(GL_CULL_FACE);
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        // Set OrthographicMatrix to identity matrix
    perspectiveProjectionMatrix_ak = vmath::mat4::identity();  // eqiuivalent to GLLoadIdentity

    // CV and CGL related code

    // create display link
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink_ak);

    // set callback function
    CVDisplayLinkSetOutputCallback(displayLink_ak,&MyDisplayLinkCallback,self);

    // convert NSOpenGLContext to CGL context
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];

    // convert pixel format
    CGLPixelFormatObj cglPixelFormatObj=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];

    // set converted context
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink_ak,cglContext,cglPixelFormatObj);

    // start display link
    CVDisplayLinkStart(displayLink_ak);

}

-(void) reshape
{
    [super reshape];
    // lock context as we are using multiple threads can be done using CGL
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    NSRect rect=[self bounds];

    if(rect.size.height < 0)
        rect.size.height=1;
    int width = rect.size.width;
    int height = rect.size.height;
    glViewport(0,0,(GLsizei)width,(GLsizei)height);
    perspectiveProjectionMatrix_ak = vmath::perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void) drawRect:(NSRect) dirtyRect
{
    [self drawView];
}

-(void)drawView
{
    
    void graph();
    void triangle(float);
    void inCircle(float, float, float, float, float, float, float, float, float);
    void rectangle(float, float);
    void outerCircle(float, float);
    
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Render
    glUseProgram(gShaderProgramObject_ak);
        
        //OpenGL Draw

        //Set ModelView and ModelViewProjection matrices to identity
        mat4 modelViewMatrix = mat4::identity();
        mat4 modelViewProjectionMatrix = mat4::identity();

        modelViewProjectionMatrix = perspectiveProjectionMatrix_ak*modelViewMatrix;

        // Translate call
        mat4 translateMatrix = translate(0.0f,0.0f,-3.0f);
        modelViewMatrix = translateMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix_ak*modelViewMatrix;

        // After this line, modelViewProjectionMatrix becomes u_mvpMatrix
        glUniformMatrix4fv(mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix);

        //Bind Vao
        glBindVertexArray(Vao_ak); //change tracks  //Begin

        graph();
        triangle(0.5f);
        inCircle(0.0f, 0.5f, 0.0f, -0.5f, -0.5f, 0.0f, 0.5f, -0.5f, 0.0f);
        rectangle(1.0f, 1.0f);
        outerCircle(1.0f, 1.0f);
        
        // Unbind Vao
        glBindVertexArray(0);  //end

        glUseProgram(0);
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL) acceptsFirstResponder{
    [[self window]makeFirstResponder:self];
    return YES;
}
// WM_KEYDOWN
-(void) keyDown:(NSEvent *)theEvent{
    int key = (int)[[theEvent characters]characterAtIndex:0];
    switch(key){
        //Esc
        case 27:
            [self release];
            [NSApp terminate: self];
            break;
        
        case 70:
        case 102:
            [[self window]toggleFullScreen:self];
            break;
    }
}
// WM_LBUTTONDOWN
-(void) mouseDown:(NSEvent *)theEvent{
    
}

// WM_RBUTTONDOWN
-(void) rightMouseDown:(NSEvent *)theEvent{
    
}

// WM_MBUTTONDOWN
-(void) otherMouseDown:(NSEvent *)theEvent{
    
}

-(void) dealloc{
    CVDisplayLinkStop(displayLink_ak);
    CVDisplayLinkRelease(displayLink_ak);
    // Uninitialize
    if(Vao_ak)
    {
        glDeleteVertexArrays(1, &Vao_ak);
        Vao_ak = 0;
    }

    if(Vbo_position_ak)
    {
        glDeleteVertexArrays(1, &Vbo_position_ak);
        Vbo_position_ak = 0;
    }

    if(Vbo_color_ak)
    {
        glDeleteVertexArrays(1, &Vbo_color_ak);
        Vbo_color_ak = 0;
    }

    if (gShaderProgramObject_ak) {
        glUseProgram(gShaderProgramObject_ak);
        GLsizei shaderCount_ak;
        glGetProgramiv(gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

        GLuint* pShaders_ak = NULL;
        pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
        glGetAttachedShaders(gShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);

        for (int i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
            glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
            glDeleteShader(pShaders_ak[i_ak]);
            pShaders_ak[i_ak] = 0;
        }

        free(pShaders_ak);
        glDeleteProgram(gShaderProgramObject_ak);
        gShaderProgramObject_ak = 0;

        glUseProgram(0);
    }
    [super dealloc];
}

@end

// Global C space functions
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp *outputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void *displayLinkContext)
{
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];
    return(result);
}


void graph()
{

    float y_axis_ak = 1.0f;
    float x_axis_ak = 1.0f;

    GLfloat lineVertices[6] = {
        y_axis_ak, 1.0f, 0.0f,
        y_axis_ak, -1.0f, 0.0f
    };
    GLfloat lineColor[] = {
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f
    };

    //vertical lines
    for (y_axis_ak = 1.0f; y_axis_ak > -1.01f; y_axis_ak -= 0.05f)
    {

        lineVertices[0] = y_axis_ak;
        lineVertices[3] = y_axis_ak;

        glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_ak);
        glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineVertices, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindBuffer(GL_ARRAY_BUFFER, Vbo_color_ak);
        glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineColor, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glDrawArrays(GL_LINES, 0, 2);
    }

    //horizontal lines
    for (x_axis_ak = 1.0f; x_axis_ak > -1.01f; x_axis_ak -= 0.05f)
    {

        lineVertices[0] = 1.0f;
        lineVertices[1] = x_axis_ak;
        lineVertices[3] = -1.0f;
        lineVertices[4] = x_axis_ak;

        glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_ak);
        glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineVertices, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindBuffer(GL_ARRAY_BUFFER, Vbo_color_ak);
        glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineColor, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glDrawArrays(GL_LINES, 0, 2);
    }

    //green vertical line

    lineVertices[0] = 0.0f;
    lineVertices[1] = 1.0f;
    lineVertices[2] = 0.0f;
    lineVertices[3] = 0.0f;
    lineVertices[4] = -1.0f;
    lineVertices[5] = 0.0f;

    lineColor[0] = 0.0f;
    lineColor[1] = 1.0f;
    lineColor[2] = 0.0f;
    lineColor[3] = 0.0f;
    lineColor[4] = 1.0f;
    lineColor[5] = 0.0f;


    glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_ak);
    glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineVertices, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ARRAY_BUFFER, Vbo_color_ak);
    glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineColor, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glDrawArrays(GL_LINES, 0, 2);

    //red horizontal line

    lineVertices[0] = 1.0f;
    lineVertices[1] = 0.0f;
    lineVertices[2] = 0.0f;
    lineVertices[3] = -1.0f;
    lineVertices[4] = 0.0f;
    lineVertices[5] = 0.0f;

    lineColor[0] = 1.0f;
    lineColor[1] = 0.0f;
    lineColor[2] = 0.0f;
    lineColor[3] = 1.0f;
    lineColor[4] = 0.0f;
    lineColor[5] = 0.0f;

    glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_ak);
    glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineVertices, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ARRAY_BUFFER, Vbo_color_ak);
    glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineColor, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glDrawArrays(GL_LINES, 0, 2);
}

void triangle(float value)
{
    GLfloat lineVertices[6] = {
    1.0f, 1.0f, 0.0f,
    1.0f, -1.0f, 0.0f
    };
    GLfloat lineColor[6] = {
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f
    };

    lineVertices[0] = 0.0f;
    lineVertices[1] = value;
    lineVertices[2] = 0.0f;
    lineVertices[3] = -value;
    lineVertices[4] = -value;
    lineVertices[5] = 0.0f;

    lineColor[0] = 1.0f;
    lineColor[1] = 1.0f;
    lineColor[2] = 0.0f;
    lineColor[3] = 1.0f;
    lineColor[4] = 1.0f;
    lineColor[5] = 0.0f;

    glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_ak);
    glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineVertices, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ARRAY_BUFFER, Vbo_color_ak);
    glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineColor, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glDrawArrays(GL_LINES, 0, 2);

    lineVertices[0] = -value;
    lineVertices[1] = -value;
    lineVertices[2] = 0.0f;
    lineVertices[3] = value;
    lineVertices[4] = -value;
    lineVertices[5] = 0.0f;

    lineColor[0] = 1.0f;
    lineColor[1] = 1.0f;
    lineColor[2] = 0.0f;
    lineColor[3] = 1.0f;
    lineColor[4] = 1.0f;
    lineColor[5] = 0.0f;

    glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_ak);
    glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineVertices, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ARRAY_BUFFER, Vbo_color_ak);
    glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineColor, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glDrawArrays(GL_LINES, 0, 2);

    lineVertices[0] = value;
    lineVertices[1] = -value;
    lineVertices[2] = 0.0f;
    lineVertices[3] = 0.0f;
    lineVertices[4] = value;
    lineVertices[5] = 0.0f;

    lineColor[0] = 1.0f;
    lineColor[1] = 1.0f;
    lineColor[2] = 0.0f;
    lineColor[3] = 1.0f;
    lineColor[4] = 1.0f;
    lineColor[5] = 0.0f;

    glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_ak);
    glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineVertices, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ARRAY_BUFFER, Vbo_color_ak);
    glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), lineColor, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glDrawArrays(GL_LINES, 0, 2);
}

void inCircle(float x1_ak, float y1_ak, float z1_ak, float x2_ak, float y2_ak, float z2, float x3_ak, float y3_ak, float z3_ak)
{
    GLfloat lineVertices[3] = {
        0.0f, 0.0f, 0.0f
    };
    GLfloat lineColor[3] = {
        1.0f, 1.0f, 0.0f,
    };

    float count_ak = 0.0f;
    GLfloat angle_ak = 0.0f;

    float dist_a_b_ak = sqrt((x2_ak - x1_ak) * (x2_ak - x1_ak) + (y2_ak - y1_ak) * (y2_ak - y1_ak) + (z2 - z1_ak) * (z2 - z1_ak));
    float dist_b_c_ak = sqrt((x3_ak - x2_ak) * (x3_ak - x2_ak) + (y3_ak - y2_ak) * (y3_ak - y2_ak) + (z3_ak - z2) * (z3_ak - z2));
    float dist_c_a_ak = sqrt((x1_ak - x3_ak) * (x1_ak - x3_ak) + (y1_ak - y3_ak) * (y1_ak - y3_ak) + (z1_ak - z3_ak) * (z1_ak - z3_ak));

    float semiperimeter_ak = (dist_a_b_ak + dist_b_c_ak + dist_c_a_ak) / 2;
    float radius_ak = sqrt((semiperimeter_ak - dist_a_b_ak) * (semiperimeter_ak - dist_b_c_ak) * (semiperimeter_ak - dist_c_a_ak) / semiperimeter_ak);
    float Ox_ak = (x3_ak * dist_a_b_ak + x1_ak * dist_b_c_ak + x2_ak * dist_c_a_ak) / (semiperimeter_ak * 2);
    float Oy_ak = (y3_ak * dist_a_b_ak + y1_ak * dist_b_c_ak + y2_ak * dist_c_a_ak) / (semiperimeter_ak * 2);
    float Oz_ak = (z3_ak * dist_a_b_ak + z1_ak * dist_b_c_ak + z2 * dist_c_a_ak) / (semiperimeter_ak * 2);

    for (count_ak = 0; count_ak <= 2000; count_ak++)
    {

        lineVertices[0] = cos(angle_ak) * radius_ak + Ox_ak;
        lineVertices[1] = sin(angle_ak) * radius_ak + Oy_ak;
        lineVertices[2] = 0.0f + Oz_ak;

        angle_ak = 2 * M_PI * count_ak / 2000;

        glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_ak);
        glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(float), lineVertices, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindBuffer(GL_ARRAY_BUFFER, Vbo_color_ak);
        glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(float), lineColor, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glDrawArrays(GL_POINTS, 0, 1);

    }

}

void rectangle(float width_ak, float height_ak)
{
    GLfloat lineVertices[24];
    GLfloat lineColor[24] = {
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f
    };

    lineVertices[0] = width_ak / 2;
    lineVertices[1] = height_ak / 2;
    lineVertices[2] = 0.0f;
    lineVertices[3] = -width_ak / 2;
    lineVertices[4] = height_ak / 2;
    lineVertices[5] = 0.0f;
    lineVertices[6] = -width_ak / 2;
    lineVertices[7] = height_ak / 2;
    lineVertices[8] = 0.0f;
    lineVertices[9] = -width_ak / 2;
    lineVertices[10] = -height_ak / 2;
    lineVertices[11] = 0.0f;
    lineVertices[12] = -width_ak / 2;
    lineVertices[13] = -height_ak / 2;
    lineVertices[14] = 0.0f;
    lineVertices[15] = width_ak / 2;
    lineVertices[16] = -height_ak / 2;
    lineVertices[17] = 0.0f;
    lineVertices[18] = width_ak / 2;
    lineVertices[19] = -height_ak / 2;
    lineVertices[20] = 0.0f;
    lineVertices[21] = width_ak / 2;
    lineVertices[22] = height_ak / 2;
    lineVertices[23] = 0.0f;


    glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_ak);
    glBufferData(GL_ARRAY_BUFFER, 24 * sizeof(float), lineVertices, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ARRAY_BUFFER, Vbo_color_ak);
    glBufferData(GL_ARRAY_BUFFER, 24 * sizeof(float), lineColor, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glDrawArrays(GL_LINES, 0, 8);
}

void outerCircle(float width_ak, float height_ak)
{
    GLfloat lineVertices[3] = {
    0.0f, 0.0f, 0.0f
    };
    GLfloat lineColor[3] = {
        1.0f, 1.0f, 0.0f,
    };

    float count_ak = 0.0f;
    GLfloat angle_ak = 0.0f;

    float radius_ak = sqrt(width_ak / 2 * width_ak / 2 + height_ak / 2 * height_ak / 2);

    for (count_ak = 0; count_ak <= 2000; count_ak++)
    {

        lineVertices[0] = cos(angle_ak) * radius_ak;
        lineVertices[1] = sin(angle_ak) * radius_ak;
        lineVertices[2] = 0.0f;

        angle_ak = 2 * M_PI * count_ak / 2000;

        glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_ak);
        glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(float), lineVertices, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindBuffer(GL_ARRAY_BUFFER, Vbo_color_ak);
        glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(float), lineColor, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glDrawArrays(GL_POINTS, 0, 1);
    }
}
