#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h> // similar to windows.h
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h> // GL.h
#include "vmath.h"
#include "sphere.h"

using namespace vmath;

Sphere *sphere;

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

FILE *gpFile_ak=NULL;

// forward declaration
@interface AppDelegate: NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

int main(int argc,const char* argv[])
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSApp = [NSApplication sharedApplication];
    [NSApp setDelegate:[[AppDelegate alloc] init]];
    [NSApp run];
    [pool release];
    return(0);
}

@interface MyOpenGLView : NSOpenGLView
@end

@implementation AppDelegate
{
    @private
    NSWindow *window;
    MyOpenGLView *myOpenGLView_ak;
}

// WM_CREATE
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSBundle *appBundle_ak = [NSBundle mainBundle];
    NSString *appDirPath_ak =[appBundle_ak bundlePath];
    NSString *parentDirPath_ak = [appDirPath_ak stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath_ak = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath_ak];

    const char *pszLogFileNameWithPath=[logFileNameWithPath_ak cStringUsingEncoding:NSASCIIStringEncoding];

    gpFile_ak=fopen(pszLogFileNameWithPath,"w");
    if(gpFile_ak==NULL){
        [self release];
        [NSApp terminate:self];
    }

    fprintf(gpFile_ak,"Program Started Successfully\n");

    NSRect win_rect_ak; // internally CGRect structure
    win_rect_ak = NSMakeRect(0.0, 0.0, 800.0, 600.0); // NSPoint(x, y) NSSize(width, height) from C-Library

    window=[[NSWindow alloc]
        initWithContentRect    :    win_rect_ak
                styleMask    :    NSWindowStyleMaskTitled |
                                NSWindowStyleMaskClosable |
                                NSWindowStyleMaskMiniaturizable |
                                NSWindowStyleMaskResizable
                backing        :     NSBackingStoreBuffered
                defer        :    NO    ];
    [window setTitle:@"ASK: Per Vertex Per Fragment Lighting"];
    [window center];

    myOpenGLView_ak = [[MyOpenGLView alloc]initWithFrame:win_rect_ak];
    [window setContentView:myOpenGLView_ak];

    [window setDelegate:self];
    [window makeKeyAndOrderFront:self]; // setFocus, setForeGroundWindow
}

// uninitialize
-(void) applicationWillTerminate:(NSNotification *)aNotification
{
    if(gpFile_ak) {
        fprintf(gpFile_ak,"Program terminated successfully\n");
        fclose(gpFile_ak);
        gpFile_ak=NULL;
    }
}

// NSWindowDelegate's method
-(void) windowWillClose:(NSNotification *)aNotification
{
    [NSApp terminate:self];
}

-(void)dealloc
{
    [myOpenGLView_ak release];
    [window release];
    [super dealloc]; // bubbling de-allocation
}

@end

@implementation MyOpenGLView
{
    @private
    CVDisplayLinkRef displayLink_ak;
    // global variables here
    enum {
        ATTRIBUTE_POSITION = 0,
        ATTRIBUTE_COLOR,
        ATTRIBUTE_NORMAL,
        ATTRIBUTE_TEXCOORD
    };
    //Per Vertex
    GLuint gVertexShaderObject_ak_pv;
    GLuint gFragmentShaderObject_ak_pv;

    //Per Fragment
    GLuint gVertexShaderObject_ak_pf;
    GLuint gFragmentShaderObject_ak_pf;

    GLuint gShaderProgramObject_ak_pv;
    GLuint gShaderProgramObject_ak_pf;

    GLuint Vao_sphere_Ak;
    GLuint Vbo_position_sphere_ak;
    GLuint Vbo_element_sphere_ak;
    GLuint Vbo_normal_sphere_ak;

    //Uniforms
    /********** 3 Uniforms**************/
    GLuint ldUniform_ak;
    GLuint kdUniform_ak;
    GLuint lightPositionUniform_ak;

    /************ 9 Uniforms******************/
    GLuint laUniform_ak; // light component of ambient light
    GLuint kaUniform_ak;

    GLuint lsUniform_ak;
    GLuint ksUniform_ak;

    GLuint shininessUniform_ak;  //single float value

    GLuint modelUniform_ak;
    GLuint viewUniform_ak;
    GLuint projectionUniform_ak;

    GLuint lKeyPressedUniform_ak;

    GLuint bLight;

    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    unsigned short sphere_elements[2280];

    int numVertices;
    int numElements;
    
    int pvpf;

    vmath::mat4 perspectiveProjectionMatrix_ak;
}

// id - returns object of any class
-(id) initWithFrame:(NSRect) frame
{
    self = [super initWithFrame: frame];
    if(self)
    {
        // pfd of windows
        NSOpenGLPixelFormatAttribute attributes_ak[] = {
            NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0
        };

        NSOpenGLPixelFormat *pixelFormat= [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes_ak]autorelease];
        if(pixelFormat==nil)
        {
            fprintf(gpFile_ak,"OpenGL pixel format error.\n");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *openGLContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:openGLContext];
    }
    return(self);
}

-(CVReturn) getFrameForTime:(const CVTimeStamp *)outputTime {
    // Multithreaded
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

-(void) prepareOpenGL
{
        [super prepareOpenGL];
        [[self openGLContext]makeCurrentContext];

        fprintf(gpFile_ak,"OpenGL version : %s\n",glGetString(GL_VERSION));
        fprintf(gpFile_ak,"GLSL version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

        // swap interval to avoid tearing
        GLint swapInt_ak=1;
        bLight = 0;
    
        pvpf = 1;
            
        sphere = new Sphere();
        
        [[self openGLContext]setValues:&swapInt_ak forParameter:NSOpenGLCPSwapInterval];

        //shader block here
    /*******Per Vertex Shader start***********/
        //Per Vertex Vertex Shader
        gVertexShaderObject_ak_pv = glCreateShader(GL_VERTEX_SHADER);
        const GLchar *vertexShaderSourceCode_pv =
        "#version 410 core" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;"  \
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform int u_LKeyPressed;" \
        "uniform vec3 u_Ld;" \
        "uniform vec3 u_Kd;" \
        "uniform vec4 u_light_position;" \
        "uniform vec3 u_La;" \
        "uniform vec3 u_Ls;" \
        "uniform vec3 u_Ka;" \
        "uniform vec3 u_Ks;" \
        "uniform float u_Shininess;" \
        "out vec3 phong_ads_light;" \
        "void main(void)" \
        "{" \
            "if (u_LKeyPressed == 1) " \
            "{" \
                "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
                "vec3 transformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
                "vec3 light_direction = normalize(vec3(u_light_position - eyeCoordinates));" \
                "vec3 reflection_vector = reflect(-light_direction, transformed_normal);" \
                "vec3 view_vector = normalize(-eyeCoordinates.xyz);" \
                "vec3 ambient = u_La * u_Ka;" \
                "vec3 diffuse = u_Ld * u_Kd * max(dot(light_direction,transformed_normal),0.0);" \
                "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector,view_vector),0.0),u_Shininess);" \
                "phong_ads_light = ambient + diffuse + specular;" \
            "}" \
            "else" \
            "{" \
                "phong_ads_light = vec3(1.0f,1.0f,1.0f);" \
            "}"
            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "}";
        
        glShaderSource(gVertexShaderObject_ak_pv,1,(const GLchar **)&vertexShaderSourceCode_pv, NULL);

        // compile Vertex Shader
        glCompileShader(gVertexShaderObject_ak_pv);

        // Error checking for Vertex Shader
        GLint infoLogLength = 0;
        GLint shaderCompiledStatus = 0;
        char *szBuffer = NULL;

        glGetShaderiv(gVertexShaderObject_ak_pv, GL_COMPILE_STATUS, &shaderCompiledStatus);
        if(shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObject_ak_pv, GL_INFO_LOG_LENGTH, &infoLogLength);
            if(infoLogLength>0)
            {
                szBuffer = (char *)malloc(infoLogLength);
                if(szBuffer!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gVertexShaderObject_ak_pv, infoLogLength, &written, szBuffer);
                    fprintf(gpFile_ak, "Vertex Shader Compilation Log: %s\n",szBuffer);
                    free(szBuffer);
                    szBuffer = NULL;
                    
                }
            }
        }

        //Per Vertex Fragment Shader
        /* out_color is the output of Vertex Shader */
        gFragmentShaderObject_ak_pv = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar* vertexFragmentSourceCode_pv =
        "#version 410 core" \
        "\n" \
        "in vec3 phong_ads_light;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
            "FragColor = vec4(phong_ads_light, 1.0f);"  \
        "}";

        glShaderSource(gFragmentShaderObject_ak_pv,1,(const GLchar **)&vertexFragmentSourceCode_pv, NULL);

        // compile Fragment Shader
        glCompileShader(gFragmentShaderObject_ak_pv);

        // Error Checking for Fragment Shader
        glGetShaderiv(gFragmentShaderObject_ak_pv, GL_COMPILE_STATUS, &shaderCompiledStatus);
        if(shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObject_ak_pv, GL_INFO_LOG_LENGTH, &infoLogLength);
            if(infoLogLength>0)
            {
                szBuffer = (char *)malloc(infoLogLength);
                if(szBuffer!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gFragmentShaderObject_ak_pv, infoLogLength, &written, szBuffer);
                    fprintf(gpFile_ak, "Fragment Shader Compilation Log: %s\n",szBuffer);
                    free(szBuffer);
                    szBuffer = NULL;
                    
                }
            }
        }

        //Shader Program
        gShaderProgramObject_ak_pv = glCreateProgram();
        glAttachShader(gShaderProgramObject_ak_pv,gVertexShaderObject_ak_pv);
        glAttachShader(gShaderProgramObject_ak_pv,gFragmentShaderObject_ak_pv);

        // Bind the attributes in shader with the enums in your main program
        /* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
        glBindAttribLocation(gShaderProgramObject_ak_pv, ATTRIBUTE_POSITION, "vPosition");

        glBindAttribLocation(gShaderProgramObject_ak_pv, ATTRIBUTE_NORMAL, "vNormal");
        
        glLinkProgram(gShaderProgramObject_ak_pv);

        // Linking Error Checking
        GLint shaderProgramLinkStatus = 0;
        szBuffer = NULL;

        glGetProgramiv(gShaderProgramObject_ak_pv, GL_LINK_STATUS, &shaderProgramLinkStatus);
        if (shaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObject_ak_pv, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                szBuffer = (char*)malloc(infoLogLength);
                if (szBuffer != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(gShaderProgramObject_ak_pv, infoLogLength, &written, szBuffer);
                    fprintf(gpFile_ak, "Shader Program Link Log: %s\n", szBuffer);
                    free(szBuffer);
                    szBuffer = NULL;
                    
                }
            }
        }

        if(pvpf == 1)
        {
            modelUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_model_matrix");
            viewUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_view_matrix");
            projectionUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_projection_matrix");

            ldUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_Ld");
            kdUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_Kd");
            lightPositionUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_light_position");

            laUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_La");
            kaUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_Ka");

            lsUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_Ls");
            ksUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_Ks");

            shininessUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_Shininess");

            lKeyPressedUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_LKeyPressed");
        }

        /*******Per Vertex Shader End***********/

        /************Per Fragment Shader Start********************************/
        gVertexShaderObject_ak_pf = glCreateShader(GL_VERTEX_SHADER);
        const GLchar* vertexShaderSourceCode =
            "#version 410 core" \
            "\n" \
            "in vec4 vPosition;" \
            "in vec3 vNormal;"  \
            "uniform mat4 u_model_matrix;" \
            "uniform mat4 u_projection_matrix;" \
            "uniform mat4 u_view_matrix;" \
            "uniform int u_LKeyPressed;" \
            "uniform vec4 u_light_position;" \
            "out vec3 transformed_normal;" \
            "out vec3 light_direction;" \
            "out vec3 view_vector;" \
            "void main(void)" \
            "{" \
            "if (u_LKeyPressed == 1) " \
            "{" \
            "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
            "transformed_normal = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
            "light_direction = vec3(u_light_position - eyeCoordinates);" \
            "view_vector = -eyeCoordinates.xyz;" \
            "}" \
            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
            "}";

        glShaderSource(gVertexShaderObject_ak_pf, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

        // compile Vertex Shader
        glCompileShader(gVertexShaderObject_ak_pf);

        // Error checking for Vertex Shader
        infoLogLength = 0;
        shaderCompiledStatus = 0;
        szBuffer = NULL;

        glGetShaderiv(gVertexShaderObject_ak_pf, GL_COMPILE_STATUS, &shaderCompiledStatus);
        if (shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObject_ak_pf, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                szBuffer = (char*)malloc(infoLogLength);
                if (szBuffer != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gVertexShaderObject_ak_pf, infoLogLength, &written, szBuffer);
                    fprintf(gpFile_ak, "Vertex Shader Compilation Log: %s\n", szBuffer);
                    free(szBuffer);
                    szBuffer = NULL;
                    
                }
            }
        }

        //Fragment Shader
        /* out_color is the output of Vertex Shader */
        gFragmentShaderObject_ak_pf = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar* vertexFragmentSourceCode =
            "#version 410 core" \
            "\n" \
            "out vec4 FragColor;" \
            "in vec3 transformed_normal;" \
            "in vec3 light_direction;" \
            "in vec3 view_vector;" \
            "uniform vec3 u_Ld;" \
            "uniform vec3 u_Kd;" \
            "uniform vec3 u_La;" \
            "uniform vec3 u_Ls;" \
            "uniform vec3 u_Ka;" \
            "uniform vec3 u_Ks;" \
            "uniform float u_Shininess;" \
            "uniform int u_LKeyPressed;" \
            "void main(void)" \
            "{" \
            "vec3 phong_ads_color;" \
            "if(u_LKeyPressed == 1)" \
            "{" \
            "vec3 norm_transformed_normal = normalize(transformed_normal);" \
            "vec3 norm_light_direction = normalize(light_direction);" \
            "vec3 norm_view_vector = normalize(view_vector);" \
            "vec3 reflection_vector = reflect(-norm_light_direction, norm_transformed_normal);" \
            "vec3 ambient = u_La * u_Ka;" \
            "vec3 diffuse = u_Ld * u_Kd * max(dot(norm_light_direction,norm_transformed_normal),0.0);" \
            "vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector,norm_view_vector),0.0),u_Shininess);" \
            "phong_ads_color = ambient + diffuse + specular;" \
            "}" \
            "else" \
            "{" \
            "phong_ads_color = vec3(1.0f,1.0f,1.0f);" \
            "}" \

            "FragColor = vec4(phong_ads_color, 1.0f);" \
            "}";

        glShaderSource(gFragmentShaderObject_ak_pf, 1, (const GLchar**)&vertexFragmentSourceCode, NULL);

        // compile Fragment Shader
        glCompileShader(gFragmentShaderObject_ak_pf);

        infoLogLength = 0;
        shaderCompiledStatus = 0;
        szBuffer = NULL;

        // Error Checking for Fragment Shader
        glGetShaderiv(gFragmentShaderObject_ak_pf, GL_COMPILE_STATUS, &shaderCompiledStatus);
        if (shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObject_ak_pf, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                szBuffer = (char*)malloc(infoLogLength);
                if (szBuffer != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gFragmentShaderObject_ak_pf, infoLogLength, &written, szBuffer);
                    fprintf(gpFile_ak, "Fragment Shader Compilation Log: %s\n", szBuffer);
                    free(szBuffer);
                    szBuffer = NULL;
                    
                }
            }
        }

        //Shader Program
        gShaderProgramObject_ak_pf = glCreateProgram();
        glAttachShader(gShaderProgramObject_ak_pf, gVertexShaderObject_ak_pf);
        glAttachShader(gShaderProgramObject_ak_pf, gFragmentShaderObject_ak_pf);

        // Bind the attributes in shader with the enums in your main program
        /* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
        glBindAttribLocation(gShaderProgramObject_ak_pf, ATTRIBUTE_POSITION, "vPosition");

        glBindAttribLocation(gShaderProgramObject_ak_pf, ATTRIBUTE_NORMAL, "vNormal");

        glLinkProgram(gShaderProgramObject_ak_pf);

        // Linking Error Checking
        shaderProgramLinkStatus = 0;
        szBuffer = NULL;

        glGetProgramiv(gShaderProgramObject_ak_pf, GL_LINK_STATUS, &shaderProgramLinkStatus);
        if (shaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObject_ak_pf, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                szBuffer = (char*)malloc(infoLogLength);
                if (szBuffer != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(gShaderProgramObject_ak_pf, infoLogLength, &written, szBuffer);
                    fprintf(gpFile_ak, "Shader Program Link Log: %s\n", szBuffer);
                    free(szBuffer);
                    szBuffer = NULL;
                    
                }
            }
        }

        if (pvpf == 0)
        {
            modelUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_model_matrix");
            viewUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_view_matrix");
            projectionUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_projection_matrix");

            ldUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_Ld");
            kdUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_Kd");
            lightPositionUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_light_position");

            laUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_La");
            kaUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_Ka");

            lsUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_Ls");
            ksUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_Ks");

            shininessUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_Shininess");

            lKeyPressedUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_LKeyPressed");
        }


        //Shader attributes for sphere
        sphere->getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere->getNumberOfSphereVertices();
        numElements = sphere->getNumberOfSphereElements();

        // Push the above vertices to vPosition

        //Steps
        /* 1. Tell OpenGl to create one buffer in your VRAM
              Give me a symbol to identify. It is known as NamedBuffer
              In OpenGL terminology, it is called as GL_ARRAY_BUFFER. This is becase vertex has plenty of attributes
              like color, texture, etc. Also it requires contiguous memory.
              User identifies this variable as Vbo_position and GPU as GL_ARRAY_BUFFER.
           2. Bind with the above symbol. It doesn't unbind until 'unbind step' is performed eg- Railway track
           3. Insert triangle data into the buffer.
           4. Specify where to insert this data into shader and also how to use it.
           5. Enable the 'in' point.
           6. Unbind
        */

        // Sphere Vao
        glGenVertexArrays(1, &Vao_sphere_Ak);
        glBindVertexArray(Vao_sphere_Ak);

        // Sphere position Vbo
        glGenBuffers(1, &Vbo_position_sphere_ak);
        glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_sphere_ak);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        // Sphere element Vbo
        glGenBuffers(1, &Vbo_element_sphere_ak);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Vbo_element_sphere_ak);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        // Sphere normals Vbo
        glGenBuffers(1, &Vbo_normal_sphere_ak);
        glBindBuffer(GL_ARRAY_BUFFER, Vbo_normal_sphere_ak);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
        glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(ATTRIBUTE_NORMAL);

        // unbind vao
        glBindVertexArray(0);


        glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        //glEnable(GL_CULL_FACE);
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        perspectiveProjectionMatrix_ak = vmath::mat4::identity();  // eqiuivalent to GLLoadIdentity

        // CV and CGL related code

        // create display link
        CVDisplayLinkCreateWithActiveCGDisplays(&displayLink_ak);

        // set callback function
        CVDisplayLinkSetOutputCallback(displayLink_ak,&MyDisplayLinkCallback,self);

        // convert NSOpenGLContext to CGL context
        CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];

        // convert pixel format
        CGLPixelFormatObj cglPixelFormatObj=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];

        // set converted context
        CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink_ak,cglContext,cglPixelFormatObj);

        // start display link
        CVDisplayLinkStart(displayLink_ak);

}

-(void) reshape
{
    [super reshape];
    // lock context as we are using multiple threads can be done using CGL
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    NSRect rect=[self bounds];

    if(rect.size.height < 0)
        rect.size.height=1;
    int width = rect.size.width;
    int height = rect.size.height;
    glViewport(0,0,(GLsizei)width,(GLsizei)height);
    perspectiveProjectionMatrix_ak = vmath::perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void) drawRect:(NSRect) dirtyRect
{
    [self drawView];
}

-(void)drawView
{
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Render
    
    GLfloat lightAmbient_ak[] = { 0.0f,0.0f,0.0f,1.0f };
    GLfloat lightDiffuse_ak[] = { 1.0f,1.0f,1.0f,1.0f };
    GLfloat lightSpecular_ak[] = { 1.0f,1.0f,1.0f,1.0f };
    GLfloat lightPosition_ak[] = { 100.0f,100.0f,100.0f,1.0f };

    GLfloat materialAmbient_ak[] = { 0.0f,0.0f,0.0f,1.0f };
    GLfloat materialDiffuse_ak[] = { 1.0f,1.0f,1.0f,1.0f };
    GLfloat materialSpecular_ak[] = { 1.0f,1.0f,1.0f,1.0f };
    GLfloat materialShininess_ak = 50.0f;

    if(pvpf==1)
    {
        glUseProgram(gShaderProgramObject_ak_pv);
        modelUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_model_matrix");
        viewUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_view_matrix");
        projectionUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_projection_matrix");

        ldUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_Ld");
        kdUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_Kd");
        lightPositionUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_light_position");

        laUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_La");
        kaUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_Ka");

        lsUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_Ls");
        ksUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_Ks");

        shininessUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_Shininess");

        lKeyPressedUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pv, "u_LKeyPressed");
    }
    else
    {
        glUseProgram(gShaderProgramObject_ak_pf);
        modelUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_model_matrix");
        viewUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_view_matrix");
        projectionUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_projection_matrix");

        ldUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_Ld");
        kdUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_Kd");
        lightPositionUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_light_position");

        laUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_La");
        kaUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_Ka");

        lsUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_Ls");
        ksUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_Ks");

        shininessUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_Shininess");

        lKeyPressedUniform_ak = glGetUniformLocation(gShaderProgramObject_ak_pf, "u_LKeyPressed");
    }

        if (bLight == true)
        {
            glUniform1i(lKeyPressedUniform_ak, 1);
            glUniform3fv(laUniform_ak,1,lightAmbient_ak);
            glUniform3fv(ldUniform_ak,1,lightDiffuse_ak);
            glUniform3fv(lsUniform_ak,1,lightSpecular_ak);
            glUniform4fv(lightPositionUniform_ak, 1, lightPosition_ak);

            glUniform3fv(kaUniform_ak, 1, materialAmbient_ak);
            glUniform3fv(kdUniform_ak,1, materialDiffuse_ak);
            glUniform3fv(ksUniform_ak,1, materialSpecular_ak);
            glUniform1f(shininessUniform_ak, materialShininess_ak);

        }
        else
            glUniform1i(lKeyPressedUniform_ak, 0);


        mat4 modelMatrix = mat4::identity();
        mat4 viewMatrix = mat4::identity();
        mat4 translationMatrix = mat4::identity();

        translationMatrix = translate(0.0f, 0.0f, -4.0f);
        modelMatrix = translationMatrix;

        glUniformMatrix4fv(modelUniform_ak, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(viewUniform_ak, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);
        
        //Sphere Begin
        glBindVertexArray(Vao_sphere_Ak);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Vbo_element_sphere_ak);
        glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
        
        glBindVertexArray(0); // Sphere end

        glUseProgram(0);
        
        CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
        CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL) acceptsFirstResponder{
    [[self window]makeFirstResponder:self];
    return YES;
}
// WM_KEYDOWN
-(void) keyDown:(NSEvent *)theEvent{
    int key = (int)[[theEvent characters]characterAtIndex:0];
    switch(key){
        //Esc
        case 27:
            [self release];
            [NSApp terminate: self];
            break;
        
        case 84:
        case 116:
            [[self window]toggleFullScreen:self];
            break;
        
        case 76:
        case 108:
            if (bLight == 0)
                bLight = 1;
            else
                bLight = 0;
            break;
            
        case 86:
        case 118:
            pvpf = 1;
            break;
        
        case 70:
        case 102:
            pvpf = 0;
            break;
    }
}
// WM_LBUTTONDOWN
-(void) mouseDown:(NSEvent *)theEvent{
    
}

// WM_RBUTTONDOWN
-(void) rightMouseDown:(NSEvent *)theEvent{
    
}

// WM_MBUTTONDOWN
-(void) otherMouseDown:(NSEvent *)theEvent{
    
}

-(void) dealloc{
    CVDisplayLinkStop(displayLink_ak);
    CVDisplayLinkRelease(displayLink_ak);
    // Uninitialize
    if(Vao_sphere_Ak)
    {
        glDeleteVertexArrays(1, &Vao_sphere_Ak);
        Vao_sphere_Ak = 0;
    }

    if(Vbo_position_sphere_ak)
    {
        glDeleteVertexArrays(1, &Vbo_position_sphere_ak);
        Vbo_position_sphere_ak = 0;
    }

    if(Vbo_element_sphere_ak)
    {
        glDeleteVertexArrays(1, &Vbo_element_sphere_ak);
        Vbo_element_sphere_ak = 0;
    }

    if (Vbo_normal_sphere_ak)
    {
        glDeleteVertexArrays(1, &Vbo_normal_sphere_ak);
        Vbo_normal_sphere_ak = 0;
    }


    if (gShaderProgramObject_ak_pv) {
        glUseProgram(gShaderProgramObject_ak_pv);
        GLsizei shaderCount_ak;
        glGetProgramiv(gShaderProgramObject_ak_pv, GL_ATTACHED_SHADERS, &shaderCount_ak);

        GLuint* pShaders_ak = NULL;
        pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
        glGetAttachedShaders(gShaderProgramObject_ak_pv, shaderCount_ak, &shaderCount_ak, pShaders_ak);

        for (GLsizei i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
            glDetachShader(gShaderProgramObject_ak_pv, pShaders_ak[i_ak]);
            glDeleteShader(pShaders_ak[i_ak]);
            pShaders_ak[i_ak] = 0;
        }

        free(pShaders_ak);
        glDeleteProgram(gShaderProgramObject_ak_pv);
        gShaderProgramObject_ak_pv = 0;

        glUseProgram(0);
    }
    [super dealloc];
}

@end

// Global C space functions
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp *outputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void *displayLinkContext)
{
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];
    return(result);
}

