#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h> // similar to windows.h
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h> // GL.h
#include "vmath.h"

using namespace vmath;

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

FILE *gpFile_ak=NULL;

GLfloat materialAmbient_ak[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat materialDiffuse_ak[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular_ak[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess_ak = 128.0f;

// forward declaration
@interface AppDelegate: NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

int main(int argc,const char* argv[])
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSApp = [NSApplication sharedApplication];
    [NSApp setDelegate:[[AppDelegate alloc] init]];
    [NSApp run];
    [pool release];
    return(0);
}

@interface MyOpenGLView : NSOpenGLView
@end

@implementation AppDelegate
{
    @private
    NSWindow *window;
    MyOpenGLView *myOpenGLView_ak;
}

// WM_CREATE
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSBundle *appBundle_ak = [NSBundle mainBundle];
    NSString *appDirPath_ak =[appBundle_ak bundlePath];
    NSString *parentDirPath_ak = [appDirPath_ak stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath_ak = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath_ak];

    const char *pszLogFileNameWithPath=[logFileNameWithPath_ak cStringUsingEncoding:NSASCIIStringEncoding];

    gpFile_ak=fopen(pszLogFileNameWithPath,"w");
    if(gpFile_ak==NULL){
        [self release];
        [NSApp terminate:self];
    }

    fprintf(gpFile_ak,"Program Started Successfully\n");

    NSRect win_rect_ak; // internally CGRect structure
    win_rect_ak = NSMakeRect(0.0, 0.0, 800.0, 600.0); // NSPoint(x, y) NSSize(width, height) from C-Library

    window=[[NSWindow alloc]
        initWithContentRect    :    win_rect_ak
                styleMask    :    NSWindowStyleMaskTitled |
                                NSWindowStyleMaskClosable |
                                NSWindowStyleMaskMiniaturizable |
                                NSWindowStyleMaskResizable
                backing        :     NSBackingStoreBuffered
                defer        :    NO    ];
    [window setTitle:@"ASK: macOS Two Lights On Pyramid"];
    [window center];

    myOpenGLView_ak = [[MyOpenGLView alloc]initWithFrame:win_rect_ak];
    [window setContentView:myOpenGLView_ak];

    [window setDelegate:self];
    [window makeKeyAndOrderFront:self]; // setFocus, setForeGroundWindow
}

// uninitialize
-(void) applicationWillTerminate:(NSNotification *)aNotification
{
    if(gpFile_ak) {
        fprintf(gpFile_ak,"Program terminated successfully\n");
        fclose(gpFile_ak);
        gpFile_ak=NULL;
    }
}

// NSWindowDelegate's method
-(void) windowWillClose:(NSNotification *)aNotification
{
    [NSApp terminate:self];
}

-(void)dealloc
{
    [myOpenGLView_ak release];
    [window release];
    [super dealloc]; // bubbling de-allocation
}

@end

@implementation MyOpenGLView
{
    @private
    CVDisplayLinkRef displayLink_ak;
    // global variables here
    enum {
        ATTRIBUTE_POSITION = 0,
        ATTRIBUTE_COLOR,
        ATTRIBUTE_NORMAL,
        ATTRIBUTE_TEXCOORD
    };
    GLuint gVertexShaderObject_ak;
    GLuint gFragmentShaderObject_ak;
    GLuint gShaderProgramObject_ak;

    GLuint mvpMatrixUniform_ak;

    GLuint Vao_pyramid_ak;
    GLuint Vbo_position_pyramid_ak;
    GLuint Vbo_normals_pyramid_ak;

    //Uniforms
    GLuint ldUniform_ak;
    GLuint kdUniform_ak;
    GLuint lightPositionUniform_ak;

    GLuint laUniform_ak;
    GLuint kaUniform_ak;

    GLuint lsUniform_ak;
    GLuint ksUniform_ak;

    GLuint shininessUniform_ak;

    GLuint modelUniform_ak;
    GLuint viewUniform_ak;
    GLuint projectionUniform_ak;

    GLuint lKeyPressedUniform_ak;

    struct Light
    {
        GLfloat lightAmbient_ak[3];
        GLfloat lightDiffuse_ak[3];
        GLfloat lightSpecular_ak[3];
        GLfloat lightPosition_ak[4];
    };
    struct Light light[2];

    float pAngle;

    GLuint bLight;

    mat4 perspectiveProjectMatrix;
}

// id - returns object of any class
-(id) initWithFrame:(NSRect) frame
{
    self = [super initWithFrame: frame];
    if(self)
    {
        // pfd of windows
        NSOpenGLPixelFormatAttribute attributes_ak[] = {
            NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0
        };

        NSOpenGLPixelFormat *pixelFormat= [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes_ak]autorelease];
        if(pixelFormat==nil)
        {
            fprintf(gpFile_ak,"OpenGL pixel format error.\n");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *openGLContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:openGLContext];
    }
    return(self);
}

-(CVReturn) getFrameForTime:(const CVTimeStamp *)outputTime {
    // Multithreaded
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

-(void) prepareOpenGL
{
    [super prepareOpenGL];
    [[self openGLContext]makeCurrentContext];

    fprintf(gpFile_ak,"OpenGL version : %s\n",glGetString(GL_VERSION));
    fprintf(gpFile_ak,"GLSL version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

    // swap interval to avoid tearing
    GLint swapInt_ak=1;
    
    bLight = 0;
    

    [[self openGLContext]setValues:&swapInt_ak forParameter:NSOpenGLCPSwapInterval];

    gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
        const GLchar* vertexShaderSourceCode =
            "#version 410 core" \
            "\n" \
            "in vec4 vPosition;" \
            "in vec3 vNormal;"
            "uniform mat4 u_model_matrix;"
            "uniform mat4 u_view_matrix;"
            "uniform mat4 u_projection_matrix;"
            "uniform int u_LKeyPressed;"
            "uniform vec3 u_Ld[2];"
            "uniform vec3 u_Kd;"
            "uniform vec3 u_La[2];"
            "uniform vec3 u_Ka;"
            "uniform vec3 u_Ls[2];"
            "uniform vec3 u_Ks;"
            "uniform vec4 u_light_position[2];"
            "uniform float u_shininess;"
            "out vec3 phong_ads_light;"
            "void main(void)" \
            "{" \

            "if(u_LKeyPressed==1 )"
            "{"
                "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;"
                "vec3 transformedNormal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"
                "vec3 view_vector = normalize(-eyeCoordinates.xyz);"
                "phong_ads_light = vec3(0.0f,0.0f,0.0f);"
                "vec3 light_direction[2];"
                "vec3 reflection_vector[2];"
                "vec3 ambient[2];"
                "vec3 diffuse[2];"
                "vec3 specular[2];"
                "for(int i=0;i<2;i++)"
                "{"
                    "light_direction[i] = normalize(vec3(u_light_position[i] - eyeCoordinates));"
                    "reflection_vector[i] = reflect(-light_direction[i], transformedNormal);"
                    "ambient[i]= u_La[i] * u_Ka;"
                    "diffuse[i] = u_Ld[i] * u_Kd * max(dot(light_direction[i], transformedNormal),0.0);"
                    "specular[i]= u_Ls[i] * u_Ks * pow(max(dot(reflection_vector[i],view_vector),0.0),u_shininess);"
                    
                    "phong_ads_light = phong_ads_light + ambient[i] + diffuse[i] + specular[i];"

                "}"
            "}"
            "else"
            "{"
                "phong_ads_light = vec3(1.0f,1.0f,1.0f);"
            "}"

            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"
        "}";
        
        glShaderSource(gVertexShaderObject_ak,1,(const GLchar **)&vertexShaderSourceCode, NULL);

        // compile Vertex Shader
        glCompileShader(gVertexShaderObject_ak);

        // Error checking for Vertex Shader
        GLint infoLogLength = 0;
        GLint shaderCompiledStatus = 0;
        char *szBuffer = NULL;

        glGetShaderiv(gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
        if(shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
            if(infoLogLength>0)
            {
                szBuffer = (char *)malloc(infoLogLength);
                if(szBuffer!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gVertexShaderObject_ak, infoLogLength, &written, szBuffer);
                    fprintf(gpFile_ak, "Vertex Shader Compilation Log: %s\n",szBuffer);
                    free(szBuffer);
                    szBuffer = NULL;
                }
            }
        }

        //Fragment Shader
        /* out_color is the output of Vertex Shader */
        gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar *vertexFragmentSourceCode =
        "#version 410 core" \
        "\n" \
        "out vec4 FragColor;" \
        "in vec3 phong_ads_light;"
        "void main(void)" \
        "{" \
            "FragColor = vec4(phong_ads_light,1.0f);" \
        "}";

        glShaderSource(gFragmentShaderObject_ak,1,(const GLchar **)&vertexFragmentSourceCode, NULL);

        // compile Fragment Shader
        glCompileShader(gFragmentShaderObject_ak);

        // Error Checking for Fragment Shader
        glGetShaderiv(gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
        if(shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
            if(infoLogLength>0)
            {
                szBuffer = (char *)malloc(infoLogLength);
                if(szBuffer!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gFragmentShaderObject_ak, infoLogLength, &written, szBuffer);
                    fprintf(gpFile_ak, "Fragment Shader Compilation Log: %s\n",szBuffer);
                    free(szBuffer);
                    szBuffer = NULL;
                }
            }
        }

        //Shader Program
        gShaderProgramObject_ak = glCreateProgram();
        glAttachShader(gShaderProgramObject_ak,gVertexShaderObject_ak);
        glAttachShader(gShaderProgramObject_ak,gFragmentShaderObject_ak);

        // Bind the attributes in shader with the enums in your main program
        /* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
        glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");
        glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_NORMAL, "vNormal");

        // Linking
        glLinkProgram(gShaderProgramObject_ak);

        // Linking Error Checking
        GLint shaderProgramLinkStatus = 0;
        szBuffer = NULL;

        glGetProgramiv(gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
        if(shaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
            if(infoLogLength>0)
            {
                szBuffer = (char *)malloc(infoLogLength);
                if(szBuffer!=NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(gShaderProgramObject_ak, infoLogLength, &written, szBuffer);
                    fprintf(gpFile_ak, "Shader Program Link Log: %s\n",szBuffer);
                    free(szBuffer);
                    szBuffer = NULL;
                }
            }
        }

        //Get the information of uniform Post linking
        modelUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_model_matrix");
        viewUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_view_matrix");
        projectionUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_projection_matrix");

        ldUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_Ld");
        kdUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_Kd");
        lightPositionUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_light_position");

        laUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_La");
        kaUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_Ka");

        lsUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_Ls");
        ksUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_Ks");

        shininessUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_shininess");

        lKeyPressedUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_LKeyPressed");

        //Pyramid
        //Vertices Array Declaration
        const GLfloat pyramidVertices[]=
        {
            0.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,

            0.0f, 1.0f, 0.0f,
            1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, -1.0f,

            0.0f, 1.0f, 0.0f,
            1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,

            0.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f
        };

        //Normals
        const GLfloat pyramidNormals[] =
        {
             0.0f, 0.447214f,0.894427f,
             0.0f, 0.447214f,0.894427f,
             0.0f, 0.447214f,0.894427f,

             0.894427f,0.447214f,0.0f,
             0.894427f,0.447214f,0.0f,
             0.894427f,0.447214f,0.0f,

             0.0f,0.447214f,-0.894427f,
             0.0f,0.447214f,-0.894427f,
             0.0f,0.447214f,-0.894427f,

             -0.894427f, 0.447214f, 0.0f,
             -0.894427f, 0.447214f, 0.0f,
             -0.894427f, 0.447214f, 0.0f
        };

        //Pyramid
        //Repeat the below steps of Vbo_position and call them in draw method
        glGenVertexArrays(1, &Vao_pyramid_ak);
        glBindVertexArray(Vao_pyramid_ak);

        // Push the above vertices to vPosition

        //Steps
        /* 1. Tell OpenGl to create one buffer in your VRAM
              Give me a symbol to identify. It is known as NamedBuffer
              In OpenGL terminology, it is called as GL_ARRAY_BUFFER. This is becase vertex has plenty of attributes
              like color, texture, etc. Also it requires contiguous memory.
              User identifies this variable as Vbo_position and GPU as GL_ARRAY_BUFFER.
           2. Bind with the above symbol. It doesn't unbind until 'unbind step' is performed eg- Railway track
           3. Insert triangle data into the buffer.
           4. Specify where to insert this data into shader and also how to use it.
           5. Enable the 'in' point.
           6. Unbind
        */
        // Pyramid
        //Position
        glGenBuffers(1, &Vbo_position_pyramid_ak);
        glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_pyramid_ak);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
        // 3 is specified for 3 pairs for vertices
        /* For Texture, specify 2*/
        // 4th parameter----> Normalized Co-ordinates
        // 5th How many strides to take?
        // 6th From which position
        glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);  //change tracks to link different attributes

        //Normals
        glGenBuffers(1, &Vbo_normals_pyramid_ak);
        glBindBuffer(GL_ARRAY_BUFFER, Vbo_normals_pyramid_ak);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormals), pyramidNormals, GL_STATIC_DRAW);
        glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        //Unbind Vao
        glBindVertexArray(0);

        glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glClearColor(0.25f, 0.25f, 0.25f, 0.0f);

        // Set PerspectiveMatrix to identity matrix
        perspectiveProjectMatrix = mat4::identity();// eqiuivalent to GLLoadIdentity

        // CV and CGL related code

        // create display link
        CVDisplayLinkCreateWithActiveCGDisplays(&displayLink_ak);

        // set callback function
        CVDisplayLinkSetOutputCallback(displayLink_ak,&MyDisplayLinkCallback,self);

        // convert NSOpenGLContext to CGL context
        CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];

        // convert pixel format
        CGLPixelFormatObj cglPixelFormatObj=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];

        // set converted context
        CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink_ak,cglContext,cglPixelFormatObj);

        // start display link
        CVDisplayLinkStart(displayLink_ak);

}

-(GLuint) loadTextureFromBMPFile:(const char *)imageFileName
{
    NSBundle *appBundle_ak = [NSBundle mainBundle];
    NSString *appDirPath_ak =[appBundle_ak bundlePath];
    NSString *parentDirPath_ak = [appDirPath_ak stringByDeletingLastPathComponent];
    NSString *imageFileNameWithPath_ak = [NSString stringWithFormat:@"%@/%s",parentDirPath_ak,imageFileName];

    // Get NSImage representation of image file
    NSImage *bmpImage=[[NSImage alloc]initWithContentsOfFile:imageFileNameWithPath_ak];

    if(!bmpImage) {
        fprintf(gpFile_ak, "NSImage conversion failed\n");
        return(0);
    }

    // Get CG image representation of NSImage
    CGImageRef cgImage=[bmpImage CGImageForProposedRect:nil context:nil hints:nil];

    // Get width and height
    int w=(int)CGImageGetWidth(cgImage);
    int h=(int)CGImageGetHeight(cgImage);

    CFDataRef imageData=CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    // convert to generic format data
    void* pixels=(void *) CFDataGetBytePtr(imageData);
    GLuint bitmapTexture;
    glGenTextures(1,&bitmapTexture);
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);
    glBindTexture(GL_TEXTURE_2D,bitmapTexture);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
    glTexImage2D(GL_TEXTURE_2D,
                    0,
                    GL_RGBA,
                    w,
                    h,
                    0,
                    GL_RGBA,
                    GL_UNSIGNED_BYTE,
                    pixels);

    glGenerateMipmap(GL_TEXTURE_2D);
    CFRelease(imageData);
    return(bitmapTexture);
}

-(void) reshape
{
    [super reshape];
    // lock context as we are using multiple threads can be done using CGL
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    NSRect rect=[self bounds];

    if(rect.size.height < 0)
        rect.size.height=1;
    int width = rect.size.width;
    int height = rect.size.height;
    glViewport(0,0,(GLsizei)width,(GLsizei)height);
    perspectiveProjectMatrix = vmath::perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void) drawRect:(NSRect) dirtyRect
{
    [self drawView];
}

-(void)drawView
{
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Render
        glUseProgram(gShaderProgramObject_ak);

        GLfloat lightAmbient[8] = {
                    light[0].lightAmbient_ak[0] = 0.0f,
                    light[0].lightAmbient_ak[1] = 0.0f,
                    light[0].lightAmbient_ak[2] = 0.0f,

                    light[1].lightAmbient_ak[0] = 0.0f,
                    light[1].lightAmbient_ak[1] = 0.0f,
                    light[1].lightAmbient_ak[2] = 0.0f,
        };

        GLfloat lightDiffuse[8] = {
                light[0].lightDiffuse_ak[0] = 1.0f,
                light[0].lightDiffuse_ak[1] = 0.0f,
                light[0].lightDiffuse_ak[2] = 0.0f,

                light[1].lightDiffuse_ak[0] = 0.0f,
                light[1].lightDiffuse_ak[1] = 0.0f,
                light[1].lightDiffuse_ak[2] = 1.0f,
        };

        GLfloat lightSpecular[8] = {
                light[0].lightSpecular_ak[0] = 1.0f,
                light[0].lightSpecular_ak[1] = 0.0f,
                light[0].lightSpecular_ak[2] = 0.0f,

                light[1].lightSpecular_ak[0] = 0.0f,
                light[1].lightSpecular_ak[1] = 0.0f,
                light[1].lightSpecular_ak[2] = 1.0f,
        };

        GLfloat lightPosition[8] = {
                light[0].lightPosition_ak[0] = 2.0f,
                light[0].lightPosition_ak[1] = 0.0f,
                light[0].lightPosition_ak[2] = 0.0f,
                light[0].lightPosition_ak[3] = 1.0f,

                light[1].lightPosition_ak[0] = -2.0f,
                light[1].lightPosition_ak[1] = 0.0f,
                light[1].lightPosition_ak[2] = 0.0f,
                light[1].lightPosition_ak[3] = 1.0f
        };

        if (bLight == true)
        {
                glUniform1i(lKeyPressedUniform_ak, 1);
                glUniform3fv(laUniform_ak, 2, lightAmbient);
                glUniform3fv(ldUniform_ak, 2, lightDiffuse);
                glUniform3fv(lsUniform_ak, 2, lightSpecular);
                glUniform4fv(lightPositionUniform_ak, 2, lightPosition);


            glUniform3fv(kaUniform_ak, 1, materialAmbient_ak);
            glUniform3fv(kdUniform_ak, 1, materialDiffuse_ak);
            glUniform3fv(ksUniform_ak, 1, materialSpecular_ak);
            glUniform1f(shininessUniform_ak, materialShininess_ak);

        }
        else
            glUniform1i(lKeyPressedUniform_ak, 0);


        mat4 modelMatrix = mat4::identity();
        mat4 viewMatrix = mat4::identity();
        mat4 translationMatrix = mat4::identity();
        mat4 rotationMatrix = mat4::identity();


        translationMatrix = translate(0.0f, 0.0f, -5.0f);
        rotationMatrix = rotate(pAngle, 0.0f, 1.0f, 0.0f);
        modelMatrix = translationMatrix * rotationMatrix;

        glUniformMatrix4fv(modelUniform_ak, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(viewUniform_ak, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionUniform_ak, 1, GL_FALSE, perspectiveProjectMatrix);

        //Pyramid Begin
        glBindVertexArray(Vao_pyramid_ak);
        glDrawArrays(GL_TRIANGLES, 0, 12);

        glBindVertexArray(0); // Pyramid end

        glUseProgram(0);


        CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
        CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
        
        pAngle+= 0.1f;
        if(pAngle >= 360.0f)
            pAngle = 0.0f;
}

-(BOOL) acceptsFirstResponder{
    [[self window]makeFirstResponder:self];
    return YES;
}
// WM_KEYDOWN
-(void) keyDown:(NSEvent *)theEvent{
    int key = (int)[[theEvent characters]characterAtIndex:0];
    switch(key){
        //Esc
        case 27:
            [self release];
            [NSApp terminate: self];
            break;
        
        case 70:
        case 102:
            [[self window]toggleFullScreen:self];
            break;
        
        case 76:
        case 108:
            if (bLight == 0)
                bLight = 1;
            else
                bLight = 0;
            break;
    }
}
// WM_LBUTTONDOWN
-(void) mouseDown:(NSEvent *)theEvent{
    
}

// WM_RBUTTONDOWN
-(void) rightMouseDown:(NSEvent *)theEvent{
    
}

// WM_MBUTTONDOWN
-(void) otherMouseDown:(NSEvent *)theEvent{
    
}

-(void) dealloc{
    CVDisplayLinkStop(displayLink_ak);
    CVDisplayLinkRelease(displayLink_ak);
    // Uninitialize
    if(Vao_pyramid_ak)
    {
        glDeleteVertexArrays(1, &Vao_pyramid_ak);
        Vao_pyramid_ak = 0;
    }

    if(Vbo_position_pyramid_ak)
    {
        glDeleteVertexArrays(1, &Vbo_position_pyramid_ak);
        Vbo_position_pyramid_ak = 0;
    }


    if (gShaderProgramObject_ak) {
        glUseProgram(gShaderProgramObject_ak);
        GLsizei shaderCount_ak;
        glGetProgramiv(gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

        GLuint* pShaders_ak = NULL;
        pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
        glGetAttachedShaders(gShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);

        for (int i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
            glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
            glDeleteShader(pShaders_ak[i_ak]);
            pShaders_ak[i_ak] = 0;
        }

        free(pShaders_ak);
        glDeleteProgram(gShaderProgramObject_ak);
        gShaderProgramObject_ak = 0;

        glUseProgram(0);
    }
    [super dealloc];
}

@end

// Global C space functions
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp *outputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void *displayLinkContext)
{
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];
    return(result);
}
