#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h> // similar to windows.h
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h> // GL.h
#include "vmath.h"
#include "sphere.h"

using namespace vmath;

Sphere *sphere;

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

FILE *gpFile_ak=NULL;

// forward declaration
@interface AppDelegate: NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

int main(int argc,const char* argv[])
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSApp = [NSApplication sharedApplication];
    [NSApp setDelegate:[[AppDelegate alloc] init]];
    [NSApp run];
    [pool release];
    return(0);
}

@interface MyOpenGLView : NSOpenGLView
@end

@implementation AppDelegate
{
    @private
    NSWindow *window;
    MyOpenGLView *myOpenGLView_ak;
}

// WM_CREATE
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSBundle *appBundle_ak = [NSBundle mainBundle];
    NSString *appDirPath_ak =[appBundle_ak bundlePath];
    NSString *parentDirPath_ak = [appDirPath_ak stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath_ak = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath_ak];

    const char *pszLogFileNameWithPath=[logFileNameWithPath_ak cStringUsingEncoding:NSASCIIStringEncoding];

    gpFile_ak=fopen(pszLogFileNameWithPath,"w");
    if(gpFile_ak==NULL){
        [self release];
        [NSApp terminate:self];
    }

    fprintf(gpFile_ak,"Program Started Successfully\n");

    NSRect win_rect_ak; // internally CGRect structure
    win_rect_ak = NSMakeRect(0.0, 0.0, 800.0, 600.0); // NSPoint(x, y) NSSize(width, height) from C-Library

    window=[[NSWindow alloc]
        initWithContentRect    :    win_rect_ak
                styleMask    :    NSWindowStyleMaskTitled |
                                NSWindowStyleMaskClosable |
                                NSWindowStyleMaskMiniaturizable |
                                NSWindowStyleMaskResizable
                backing        :     NSBackingStoreBuffered
                defer        :    NO    ];
    [window setTitle:@"ASK: Plain Sphere"];
    [window center];

    myOpenGLView_ak = [[MyOpenGLView alloc]initWithFrame:win_rect_ak];
    [window setContentView:myOpenGLView_ak];

    [window setDelegate:self];
    [window makeKeyAndOrderFront:self]; // setFocus, setForeGroundWindow
}

// uninitialize
-(void) applicationWillTerminate:(NSNotification *)aNotification
{
    if(gpFile_ak) {
        fprintf(gpFile_ak,"Program terminated successfully\n");
        fclose(gpFile_ak);
        gpFile_ak=NULL;
    }
}

// NSWindowDelegate's method
-(void) windowWillClose:(NSNotification *)aNotification
{
    [NSApp terminate:self];
}

-(void)dealloc
{
    [myOpenGLView_ak release];
    [window release];
    [super dealloc]; // bubbling de-allocation
}

@end

@implementation MyOpenGLView
{
    @private
    CVDisplayLinkRef displayLink_ak;
    // global variables here
    enum {
        ATTRIBUTE_POSITION = 0,
        ATTRIBUTE_COLOR,
        ATTRIBUTE_NORMAL,
        ATTRIBUTE_TEXCOORD
    };
    GLuint gVertexShaderObject_ak;
    GLuint gFragmentShaderObject_ak;
    GLuint gShaderProgramObject_ak;

    GLuint Vao_sphere;
    GLuint Vbo_position_sphere;
    GLuint Vbo_element_sphere;
    
    //Uniforms
    GLuint modelViewMatrixUniform;
    GLuint modelViewProjectionMatrixUniform;
    GLuint ldUniform;
    GLuint kdUniform;
    GLuint lightPositionUniform;

    GLuint lKeyPressedUniform;

    GLuint bLight;

    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    unsigned short sphere_elements[2280];

    int numVertices;
    int numElements;

    vmath::mat4 perspectiveProjectionMatrix_ak;
}

// id - returns object of any class
-(id) initWithFrame:(NSRect) frame
{
    self = [super initWithFrame: frame];
    if(self)
    {
        // pfd of windows
        NSOpenGLPixelFormatAttribute attributes_ak[] = {
            NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0
        };

        NSOpenGLPixelFormat *pixelFormat= [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes_ak]autorelease];
        if(pixelFormat==nil)
        {
            fprintf(gpFile_ak,"OpenGL pixel format error.\n");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *openGLContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:openGLContext];
    }
    return(self);
}

-(CVReturn) getFrameForTime:(const CVTimeStamp *)outputTime {
    // Multithreaded
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

-(void) prepareOpenGL
{
    [super prepareOpenGL];
    [[self openGLContext]makeCurrentContext];

    fprintf(gpFile_ak,"OpenGL version : %s\n",glGetString(GL_VERSION));
    fprintf(gpFile_ak,"GLSL version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

    // swap interval to avoid tearing
    GLint swapInt_ak=1;
    bLight = 0;
    
    sphere = new Sphere();
    
    [[self openGLContext]setValues:&swapInt_ak forParameter:NSOpenGLCPSwapInterval];

    //shader block here
    gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
        const GLchar *vertexShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;"  \
        "uniform mat4 u_model_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform int u_LKeyPressed;" \
        "uniform vec3 u_Ld;" \
        "uniform vec3 u_Kd;" \
        "uniform vec4 u_light_position;" \
        "out vec3 diffuse_light;" \
        "void main(void)" \
        "{" \
            "if (u_LKeyPressed == 1) " \
            "{" \
                "vec4 eyeCoordinates = u_model_view_matrix * vPosition;" \
                "mat3 normalMatrix = mat3(transpose(inverse(u_model_view_matrix)));" \
                "vec3 tnorm = normalize(normalMatrix * vNormal);" \
                "vec3 s = normalize(vec3(u_light_position - eyeCoordinates));" \
                "diffuse_light = u_Ld * u_Kd * max(dot(s, tnorm), 0.0);" \
            "}" \
            "gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;" \
        "}";
        
        glShaderSource(gVertexShaderObject_ak,1,(const GLchar **)&vertexShaderSourceCode, NULL);

        // compile Vertex Shader
        glCompileShader(gVertexShaderObject_ak);

        // Error checking for Vertex Shader
        GLint infoLogLength = 0;
        GLint shaderCompiledStatus = 0;
        char *szBuffer = NULL;

        glGetShaderiv(gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
        if(shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
            if(infoLogLength>0)
            {
                szBuffer = (char *)malloc(infoLogLength);
                if(szBuffer!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gVertexShaderObject_ak, infoLogLength, &written, szBuffer);
                    fprintf(gpFile_ak, "Vertex Shader Compilation Log: %s\n",szBuffer);
                    free(szBuffer);
                    szBuffer = NULL;
                }
            }
        }

        //Fragment Shader
        /* out_color is the output of Vertex Shader */
        gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar* vertexFragmentSourceCode =
        "#version 410 core" \
        "\n" \
        "in vec3 diffuse_light;" \
        "out vec4 FragColor;" \
        "uniform int u_LKeyPressed;" \
        "void main(void)" \
        "{" \
            "vec4 color;" \
            "if (u_LKeyPressed == 1) " \
            "{"
                "color = vec4(diffuse_light, 1.0);"  \
            "}"
            "else" \
            "{" \
                "color = vec4(1.0f,1.0f,1.0f,1.0f);" \
            "}" \
            "FragColor = color;" \
        "}";

        glShaderSource(gFragmentShaderObject_ak,1,(const GLchar **)&vertexFragmentSourceCode, NULL);

        // compile Fragment Shader
        glCompileShader(gFragmentShaderObject_ak);

        // Error Checking for Fragment Shader
        glGetShaderiv(gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
        if(shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
            if(infoLogLength>0)
            {
                szBuffer = (char *)malloc(infoLogLength);
                if(szBuffer!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gFragmentShaderObject_ak, infoLogLength, &written, szBuffer);
                    fprintf(gpFile_ak, "Fragment Shader Compilation Log: %s\n",szBuffer);
                    free(szBuffer);
                    szBuffer = NULL;
                }
            }
        }

        //Shader Program
        gShaderProgramObject_ak = glCreateProgram();
        glAttachShader(gShaderProgramObject_ak,gVertexShaderObject_ak);
        glAttachShader(gShaderProgramObject_ak,gFragmentShaderObject_ak);

        // Bind the attributes in shader with the enums in your main program
        /* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
        glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");

        glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_NORMAL, "vNormal");
        
        glLinkProgram(gShaderProgramObject_ak);

        // Linking Error Checking
        GLint shaderProgramLinkStatus = 0;
        szBuffer = NULL;

        glGetProgramiv(gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
        if(shaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
            if(infoLogLength>0)
            {
                szBuffer = (char *)malloc(infoLogLength);
                if(szBuffer!=NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(gShaderProgramObject_ak, infoLogLength, &written, szBuffer);
                    fprintf(gpFile_ak, "Shader Program Link Log: %s\n",szBuffer);
                    free(szBuffer);
                    szBuffer = NULL;
                }
            }
        }

        //Get the information of uniform Post linking
        modelViewMatrixUniform = glGetUniformLocation(gShaderProgramObject_ak, "u_model_view_matrix");
        modelViewProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject_ak, "u_projection_matrix");

        ldUniform = glGetUniformLocation(gShaderProgramObject_ak, "u_Ld");
        kdUniform = glGetUniformLocation(gShaderProgramObject_ak, "u_Kd");
        lightPositionUniform = glGetUniformLocation(gShaderProgramObject_ak, "u_light_position");
        lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject_ak, "u_LKeyPressed");


        //Shader attributes for sphere
        sphere->getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere->getNumberOfSphereVertices();
        numElements = sphere->getNumberOfSphereElements();

        // Push the above vertices to vPosition

        //Steps
        /* 1. Tell OpenGl to create one buffer in your VRAM
              Give me a symbol to identify. It is known as NamedBuffer
              In OpenGL terminology, it is called as GL_ARRAY_BUFFER. This is becase vertex has plenty of attributes
              like color, texture, etc. Also it requires contiguous memory.
              User identifies this variable as Vbo_position and GPU as GL_ARRAY_BUFFER.
           2. Bind with the above symbol. It doesn't unbind until 'unbind step' is performed eg- Railway track
           3. Insert triangle data into the buffer.
           4. Specify where to insert this data into shader and also how to use it.
           5. Enable the 'in' point.
           6. Unbind
        */

        //Sphere
        
        // Sphere Vao
        glGenVertexArrays(1, &Vao_sphere);
        glBindVertexArray(Vao_sphere);

        // Sphere position Vbo
        glGenBuffers(1, &Vbo_position_sphere);
        glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_sphere);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        // Sphere element Vbo
        glGenBuffers(1, &Vbo_element_sphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Vbo_element_sphere);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        // unbind vao
        glBindVertexArray(0);

        glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        //glEnable(GL_CULL_FACE);
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    perspectiveProjectionMatrix_ak = vmath::mat4::identity();  // eqiuivalent to GLLoadIdentity

    // CV and CGL related code

    // create display link
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink_ak);

    // set callback function
    CVDisplayLinkSetOutputCallback(displayLink_ak,&MyDisplayLinkCallback,self);

    // convert NSOpenGLContext to CGL context
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];

    // convert pixel format
    CGLPixelFormatObj cglPixelFormatObj=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];

    // set converted context
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink_ak,cglContext,cglPixelFormatObj);

    // start display link
    CVDisplayLinkStart(displayLink_ak);

}

-(void) reshape
{
    [super reshape];
    // lock context as we are using multiple threads can be done using CGL
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    NSRect rect=[self bounds];

    if(rect.size.height < 0)
        rect.size.height=1;
    int width = rect.size.width;
    int height = rect.size.height;
    glViewport(0,0,(GLsizei)width,(GLsizei)height);
    perspectiveProjectionMatrix_ak = vmath::perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void) drawRect:(NSRect) dirtyRect
{
    [self drawView];
}

-(void)drawView
{
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Render
    glUseProgram(gShaderProgramObject_ak);

    if (bLight == true)
        {
            glUniform1i(lKeyPressedUniform, 1);
            glUniform3f(ldUniform, 1.0f, 1.0f, 1.0f);
            glUniform3f(kdUniform, 0.5f, 0.5f, 0.5f);

            GLfloat lightPosition_ak[] = { 0.0f, 0.0f, 2.0f, 1.0f };
            glUniform4fv(lightPositionUniform, 1, lightPosition_ak);
        }

        else
            glUniform1i(lKeyPressedUniform, 0);
        
        //OpenGL Draw

        //Set ModelView and ModelViewProjection matrices to identity
        mat4 modelMatrix = mat4::identity();

        mat4 translateMatrix = translate(0.0f, 0.0f, -6.0f);
        modelMatrix = translateMatrix;

        glUniformMatrix4fv(modelViewMatrixUniform, 1, GL_FALSE, modelMatrix);

        glUniformMatrix4fv(modelViewProjectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

        //Sphere Begin
        glBindVertexArray(Vao_sphere);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Vbo_element_sphere);
        glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
        
        glBindVertexArray(0); // Sphere end

        glUseProgram(0);
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL) acceptsFirstResponder{
    [[self window]makeFirstResponder:self];
    return YES;
}
// WM_KEYDOWN
-(void) keyDown:(NSEvent *)theEvent{
    int key = (int)[[theEvent characters]characterAtIndex:0];
    switch(key){
        //Esc
        case 27:
            [self release];
            [NSApp terminate: self];
            break;
        
        case 70:
        case 102:
            [[self window]toggleFullScreen:self];
            break;
        
        case 76:
        case 108:
            if (bLight == 0)
                bLight = 1;
            else
                bLight = 0;
            break;
    }
}
// WM_LBUTTONDOWN
-(void) mouseDown:(NSEvent *)theEvent{
    
}

// WM_RBUTTONDOWN
-(void) rightMouseDown:(NSEvent *)theEvent{
    
}

// WM_MBUTTONDOWN
-(void) otherMouseDown:(NSEvent *)theEvent{
    
}

-(void) dealloc{
    CVDisplayLinkStop(displayLink_ak);
    CVDisplayLinkRelease(displayLink_ak);
    // Uninitialize
    if(Vao_sphere)
    {
        glDeleteVertexArrays(1, &Vao_sphere);
        Vao_sphere = 0;
    }

    if(Vbo_position_sphere)
    {
        glDeleteVertexArrays(1, &Vbo_position_sphere);
        Vbo_position_sphere = 0;
    }

    if(Vbo_element_sphere)
    {
        glDeleteVertexArrays(1, &Vbo_element_sphere);
        Vbo_element_sphere = 0;
    }

    if (gShaderProgramObject_ak) {
        glUseProgram(gShaderProgramObject_ak);
        GLsizei shaderCount_ak;
        glGetProgramiv(gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

        GLuint* pShaders_ak = NULL;
        pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
        glGetAttachedShaders(gShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);

        for (int i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
            glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
            glDeleteShader(pShaders_ak[i_ak]);
            pShaders_ak[i_ak] = 0;
        }

        free(pShaders_ak);
        glDeleteProgram(gShaderProgramObject_ak);
        gShaderProgramObject_ak = 0;

        glUseProgram(0);
    }
    [super dealloc];
}

@end

// Global C space functions
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp *outputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void *displayLinkContext)
{
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];
    return(result);
}
