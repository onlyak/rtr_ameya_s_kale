//
//  main.m
//  Window
//
//  Created by ameya kale on 13/06/21.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>

@end

int main(int argc, const char * argv[]) {
    
    NSAutoreleasePool *pool =[[NSAutoreleasePool alloc]init];
    NSApp = [NSApplication sharedApplication];
    
    [NSApp setDelegate:[[AppDelegate alloc]init]];
    
    //Message Loop
    [NSApp run];
    
    [pool release];
    
    return 0;
}

@interface MyView : NSView

@end

@implementation AppDelegate{
    @private
    
    NSWindow *window;
    MyView *view;
}

-(void)applicationDidFinishLaunching:(NSNotification *)aNotification{
    
    NSRect winRect;
    winRect = NSMakeRect(0.0, 0.0, 800.0, 600.0);
    
    window = [[NSWindow alloc]
              initWithContentRect: winRect
              styleMask: NSWindowStyleMaskTitled |
                         NSWindowStyleMaskClosable |
                         NSWindowStyleMaskMiniaturizable |
                         NSWindowStyleMaskResizable
              backing:   NSBackingStoreBuffered
              defer:     NO ];
    
    [window setTitle:@"ASK macOS Window"];
    [window center];
    
    view = [[MyView alloc]initWithFrame:winRect];
    [window setContentView:view];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)aNotification{
    
}

-(void)windowWillClose:(NSNotification *)aNotification{
    [NSApp terminate:self];
}

-(void)dealloc{
    
    [view release];
    [window release];
    [super dealloc];
}

@end

@implementation MyView{
    @private
    NSString *centralText;
}

-(id) initWithFrame:(NSRect)frame{
    self=[super initWithFrame:frame];
    if(self){
        centralText = @"Hello World";
    }    return self;
}

-(void) drawRect: (NSRect) dirtyRect{
    
    NSColor *backgroundColor = [NSColor blackColor];
    [backgroundColor set];
    NSRectFill(dirtyRect);
    
    NSDictionary *dictionaryForTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[NSFont fontWithName:@"Helvetica" size:32],
        NSFontAttributeName, [NSColor greenColor],
        NSForegroundColorAttributeName,
        nil];
    
    NSSize textSize = [centralText sizeWithAttributes: dictionaryForTextAttributes];
    
    NSPoint point;
    point.x = (dirtyRect.size.width/2) - (textSize.width/2);
    point.y = (dirtyRect.size.height/2) - (textSize.height/2) + 12;
    
    [centralText drawAtPoint:point withAttributes:dictionaryForTextAttributes];
}

-(BOOL) acceptsFirstResponder{
    [[self window]makeFirstResponder:self];
    return YES;
}

-(void) keyDown:(NSEvent *)theEvent{
    
    int key = (int)[[theEvent characters]characterAtIndex:0];
    
    switch(key){
        case 27:
            [self release];
            [NSApp terminate: self];
            break;
        
        case 'F':
        case 'f':
            centralText=@ "F Key Is Pressed";
            [[self window]toggleFullScreen:self];
            break;
    }
}

-(void) mouseDown:(NSEvent *)theEvent{
    
    centralText = @"Left mouse button is clicked";
    [self setNeedsDisplay:YES];
}

-(void) rightMouseDown:(NSEvent *)theEvent{
    
    centralText = @"Right mouse button is clicked";
    [self setNeedsDisplay:YES];
}

-(void) otherMouseDown:(NSEvent *)theEvent{
    
    centralText = @"Hello World";
    [self setNeedsDisplay:YES];
}

-(void) dealloc{
    [super dealloc];
}
@end
