#include "vmath.h"
#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>
#import <OpenGL/gl3.h>
#import <QuartzCore/CVDisplayLink.h>

using namespace vmath;

CVReturn
MyDisplayLinkCallback(CVDisplayLinkRef,
                      const CVTimeStamp*,
                      const CVTimeStamp*,
                      CVOptionFlags,
                      CVOptionFlags*,
                      void*);

FILE* gpFile_ak = NULL;
GLuint numberOfSegementsUniform_ak;
GLuint numberOfStripsUniform_ak;
GLuint lineColorUniform_ak;
GLuint uiNumberOfSegments_ak = 1;


@interface AppDelegate : NSObject<NSApplicationDelegate, NSWindowDelegate>
@end

int
main(int argc, const char* argv[])
{
  NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
  NSApp = [NSApplication sharedApplication];
  [NSApp setDelegate:[[AppDelegate alloc] init]];
  [NSApp run];
  [pool release];
  return (0);
}

@interface MyOpenGLView : NSOpenGLView
@end

@implementation AppDelegate {
@private
  NSWindow* window;
  MyOpenGLView* myOpenGLView_ak;
}


- (void)applicationDidFinishLaunching:(NSNotification*)aNotification
{
  NSBundle* appBundle_ak = [NSBundle mainBundle];
  NSString* appDirPath_ak = [appBundle_ak bundlePath];
  NSString* parentDirPath_ak =
    [appDirPath_ak stringByDeletingLastPathComponent];
  NSString* logFileNameWithPath_ak =
    [NSString stringWithFormat:@"%@/Log.txt", parentDirPath_ak];

  const char* pszLogFileNameWithPath =
    [logFileNameWithPath_ak cStringUsingEncoding:NSASCIIStringEncoding];

  gpFile_ak = fopen(pszLogFileNameWithPath, "w");
  if (gpFile_ak == NULL) {
    [self release];
    [NSApp terminate:self];
  }

  fprintf(gpFile_ak, "Program Started Successfully\n");

  NSRect win_rect_ak;
  win_rect_ak =
    NSMakeRect(0.0,
               0.0,
               800.0,
               600.0);

  window = [[NSWindow alloc]
    initWithContentRect:win_rect_ak
              styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable |
                        NSWindowStyleMaskMiniaturizable |
                        NSWindowStyleMaskResizable
                backing:NSBackingStoreBuffered
                  defer:NO];
  [window setTitle:@"ASK: macOS Tessalation shader"];
  [window center];

  myOpenGLView_ak = [[MyOpenGLView alloc] initWithFrame:win_rect_ak];
  [window setContentView:myOpenGLView_ak];

  [window setDelegate:self];
  [window makeKeyAndOrderFront:self];
}


- (void)applicationWillTerminate:(NSNotification*)aNotification
{
  if (gpFile_ak) {
    fprintf(gpFile_ak, "Program terminated successfully\n");
    fclose(gpFile_ak);
    gpFile_ak = NULL;
  }
}


- (void)windowWillClose:(NSNotification*)aNotification
{
  [NSApp terminate:self];
}

- (void)dealloc
{
  [myOpenGLView_ak release];
  [window release];
  [super dealloc];
}

@end

@implementation MyOpenGLView {
@private
  CVDisplayLinkRef displayLink_ak;

  enum
  {
    ATTRIBUTE_POSITION = 0,
    ATTRIBUTE_COLOR,
    ATTRIBUTE_NORMAL,
    ATTRIBUTE_TEXCOORD
  };
  GLuint gVertexShaderObject_ak;
  GLuint gTesselationControlShaderObject_ak;
  GLuint gTesselationEvaluationShaderObject_ak;
  GLuint gFragmentShaderObject_ak;
  GLuint gShaderProgramObject_ak;

  GLuint vao_ak;
  GLuint vbo_position_ak;
  GLuint mvpMatrixUniform_ak;

  vmath::mat4 perspectiveProjectionMatrix_ak;
}


- (id)initWithFrame:(NSRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {

    NSOpenGLPixelFormatAttribute attributes_ak[] = {
      NSOpenGLPFAOpenGLProfile,
      NSOpenGLProfileVersion4_1Core,
      NSOpenGLPFAScreenMask,
      CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
      NSOpenGLPFANoRecovery,
      NSOpenGLPFAAccelerated,
      NSOpenGLPFAColorSize,
      24,
      NSOpenGLPFADepthSize,
      24,
      NSOpenGLPFAAlphaSize,
      8,
      NSOpenGLPFADoubleBuffer,
      0
    };

    NSOpenGLPixelFormat* pixelFormat = [[[NSOpenGLPixelFormat alloc]
      initWithAttributes:attributes_ak] autorelease];
    if (pixelFormat == nil) {
      fprintf(gpFile_ak, "OpenGL pixel format error.\n");
      [self release];
      [NSApp terminate:self];
    }

    NSOpenGLContext* openGLContext =
      [[[NSOpenGLContext alloc] initWithFormat:pixelFormat
                                  shareContext:nil] autorelease];
    [self setPixelFormat:pixelFormat];
    [self setOpenGLContext:openGLContext];
  }
  return (self);
}

- (CVReturn)getFrameForTime:(const CVTimeStamp*)outputTime
{

  NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
  [self drawView];
  [pool release];
  return (kCVReturnSuccess);
}

- (void)prepareOpenGL
{
  [super prepareOpenGL];
  [[self openGLContext] makeCurrentContext];

  fprintf(gpFile_ak, "OpenGL version : %s\n", glGetString(GL_VERSION));
  fprintf(gpFile_ak,
          "GLSL version : %s\n",
          glGetString(GL_SHADING_LANGUAGE_VERSION));


  GLint swapInt_ak = 1;

  [[self openGLContext] setValues:&swapInt_ak
                     forParameter:NSOpenGLCPSwapInterval];


  gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 410 core"
        "\n"
        "in vec2 vPosition;"
        "void main(void)"
        "{"
        "gl_Position = vec4(vPosition, 0.0, 1.0);"
        "}";

    glShaderSource(gVertexShaderObject_ak, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(gVertexShaderObject_ak);

    GLint infoLogLength_ak = 0;
    GLint shaderCompileStatus_ak = 0;
    char* szBuffer_ak = NULL;
    glGetShaderiv(gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
        glGetShaderiv(gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetShaderInfoLog(gVertexShaderObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Vertex shader compilation log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                [self dealloc];
            }
        }
    }

    gTesselationControlShaderObject_ak = glCreateShader(GL_TESS_CONTROL_SHADER);
    const GLchar* tesselationControlShaderSourceCode =
        "#version 410 core"
        "\n"
        "layout(vertices = 4) out;"
        "uniform int numberOfSegments;"
        "uniform int numberOfStrips;"
        "void main(void)"
        "{"
        "gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;"
		"gl_TessLevelOuter[0]=float(numberOfStrips);"
		"gl_TessLevelOuter[1]=float(numberOfSegments);"
        "}";

    glShaderSource(gTesselationControlShaderObject_ak, 1, (const GLchar**)&tesselationControlShaderSourceCode, NULL);
    glCompileShader(gTesselationControlShaderObject_ak);

    infoLogLength_ak = 0;
    shaderCompileStatus_ak = 0;
    szBuffer_ak = NULL;
    glGetShaderiv(gTesselationControlShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
        glGetShaderiv(gTesselationControlShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetShaderInfoLog(gTesselationControlShaderObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Tesselation control shader compilation log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                [self dealloc];
            }
        }
    }

    gTesselationEvaluationShaderObject_ak = glCreateShader(GL_TESS_EVALUATION_SHADER);
    const GLchar* tesselationEvaluationShaderSourceCode =
        "#version 410 core "
        "\n"
        "layout(isolines)in;"
        "uniform mat4 u_mvp_matrix;"
        "void main(void)"
        "{"
        "float tessCoord = gl_TessCoord.x;"
        "vec3 p0 = gl_in[0].gl_Position.xyz;"
        "vec3 p1 = gl_in[1].gl_Position.xyz;"
        "vec3 p2 = gl_in[2].gl_Position.xyz;"
        "vec3 p3 = gl_in[3].gl_Position.xyz;"
        "vec3 p = p0 * (1.0-tessCoord) * (1.0-tessCoord) * (1.0-tessCoord) + p1 * 3.0 * tessCoord * (1.0-tessCoord) * (1.0-tessCoord) + p2 * tessCoord * tessCoord * (1.0-tessCoord) + p3 * tessCoord * tessCoord * tessCoord;"
        "gl_Position = u_mvp_matrix * vec4(p, 1.0);"
        "}";

    glShaderSource(gTesselationEvaluationShaderObject_ak, 1, (const GLchar**)&tesselationEvaluationShaderSourceCode, NULL);
    glCompileShader(gTesselationEvaluationShaderObject_ak);

    infoLogLength_ak = 0;
    shaderCompileStatus_ak = 0;
    szBuffer_ak = NULL;
    glGetShaderiv(gTesselationEvaluationShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
        glGetShaderiv(gTesselationEvaluationShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetShaderInfoLog(gTesselationEvaluationShaderObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Tesselation evaluation shader compilation log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                [self dealloc];
            }
        }
    }

    gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentSourceCode_ak =
        "#version 410 core"
        "\n"
        "out vec4 FragColor;"
        "uniform vec4 lineColor;"\
        "void main(void)"
        "{"
        "FragColor = lineColor;"
        "}";

    glShaderSource(gFragmentShaderObject_ak, 1, (const GLchar**)&fragmentSourceCode_ak, NULL);
    glCompileShader(gFragmentShaderObject_ak);

    infoLogLength_ak = 0;
    shaderCompileStatus_ak = 0;
    szBuffer_ak = NULL;
    glGetShaderiv(gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
        glGetShaderiv(gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetShaderInfoLog(gFragmentShaderObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Fragment shader compilation log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                [self dealloc];
            }
        }
    }

    gShaderProgramObject_ak = glCreateProgram();
    glAttachShader(gShaderProgramObject_ak, gVertexShaderObject_ak);
    glAttachShader(gShaderProgramObject_ak, gTesselationControlShaderObject_ak);
    glAttachShader(gShaderProgramObject_ak, gTesselationEvaluationShaderObject_ak);
    glAttachShader(gShaderProgramObject_ak, gFragmentShaderObject_ak);

    glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");

    glLinkProgram(gShaderProgramObject_ak);

    GLint shaderProgramLinkStatus = 0;
    glGetProgramiv(gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
    if (shaderProgramLinkStatus == GL_FALSE) {
        glGetProgramiv(gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetProgramInfoLog(gShaderProgramObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Shader program link log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                [self dealloc];
            }
        }
    }

    mvpMatrixUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_mvp_matrix");
    numberOfSegementsUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "numberOfSegments");
    numberOfStripsUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "numberOfStrips");
    lineColorUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "lineColor");

    const GLfloat lineVertices_ak[] = {
        -1.0f,-1.0f,
		-0.5f,1.0f,
		0.5f,-1.0f,
		1.0f,1.0f
    };

    glGenVertexArrays(1, &vao_ak);
    glBindVertexArray(vao_ak);

    glGenBuffers(1, &vbo_position_ak);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_ak);
    glBufferData(GL_ARRAY_BUFFER, sizeof(lineVertices_ak), lineVertices_ak, GL_STATIC_DRAW);

    glVertexAttribPointer(ATTRIBUTE_POSITION, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    glEnableVertexAttribArray(ATTRIBUTE_POSITION);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  perspectiveProjectionMatrix_ak =
    vmath::mat4::identity();




  CVDisplayLinkCreateWithActiveCGDisplays(&displayLink_ak);


  CVDisplayLinkSetOutputCallback(displayLink_ak, &MyDisplayLinkCallback, self);


  CGLContextObj cglContext =
    (CGLContextObj)[[self openGLContext] CGLContextObj];


  CGLPixelFormatObj cglPixelFormatObj =
    (CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];


  CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(
    displayLink_ak, cglContext, cglPixelFormatObj);


  CVDisplayLinkStart(displayLink_ak);
}

- (void)reshape
{
  [super reshape];

  CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
  NSRect rect = [self bounds];

  if (rect.size.height < 0)
    rect.size.height = 1;
  int width = rect.size.width;
  int height = rect.size.height;
  glViewport(0, 0, (GLsizei)width, (GLsizei)height);
  perspectiveProjectionMatrix_ak =
    vmath::perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
  CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
  [self drawView];
}

- (void)drawView
{
  [[self openGLContext] makeCurrentContext];
  CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   glUseProgram(gShaderProgramObject_ak);

    mat4 modelViewMatrix_ak;
    mat4 modelViewProjectionMatrix_ak;
    mat4 translateMatrix_ak;

    modelViewMatrix_ak = mat4::identity();
    modelViewProjectionMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -3.0f);
    modelViewMatrix_ak = translateMatrix_ak;

    modelViewProjectionMatrix_ak = perspectiveProjectionMatrix_ak * modelViewMatrix_ak;
    glUniformMatrix4fv(mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix_ak);
    glUniform1i(numberOfSegementsUniform_ak,uiNumberOfSegments_ak);
    glUniform1i(numberOfStripsUniform_ak,1);
    glUniform4fv(lineColorUniform_ak, 1, vec4(1.0f,1.0f,0.0f,1.0f));
    glBindVertexArray(vao_ak);
    glPatchParameteri(GL_PATCH_VERTICES,4);
	glDrawArrays(GL_PATCHES, 0, 4);
    glBindVertexArray(0);
    glUseProgram(0);
  CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);
  CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (BOOL)acceptsFirstResponder
{
  [[self window] makeFirstResponder:self];
  return YES;
}

- (void)keyDown:(NSEvent*)theEvent
{
  int key = (int)[[theEvent characters] characterAtIndex:0];
  switch (key) {
    case NSUpArrowFunctionKey:
    uiNumberOfSegments_ak++;
                    if (uiNumberOfSegments_ak >= 30)
                        uiNumberOfSegments_ak = 30;
            break;
    case NSDownArrowFunctionKey:
     uiNumberOfSegments_ak--;
                    if (uiNumberOfSegments_ak <= 0)
                        uiNumberOfSegments_ak = 1;
            break;

    case 27:
      [self release];
      [NSApp terminate:self];
      break;

    case 70:
    case 102:
      [[self window] toggleFullScreen:self];
      break;
  }
}

- (void)mouseDown:(NSEvent*)theEvent
{}


- (void)rightMouseDown:(NSEvent*)theEvent
{}


- (void)otherMouseDown:(NSEvent*)theEvent
{}

- (void)dealloc
{
  CVDisplayLinkStop(displayLink_ak);
  CVDisplayLinkRelease(displayLink_ak);

  if (vao_ak) {
    glDeleteVertexArrays(1, &vao_ak);
    vao_ak = 0;
  }

  if (vbo_position_ak) {
    glDeleteVertexArrays(1, &vbo_position_ak);
    vbo_position_ak = 0;
  }

  if (gShaderProgramObject_ak) {
    glUseProgram(gShaderProgramObject_ak);
    GLsizei shaderCount_ak;
    glGetProgramiv(
      gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

    GLuint* pShaders_ak = NULL;
    pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
    glGetAttachedShaders(gShaderProgramObject_ak,
                         shaderCount_ak,
                         &shaderCount_ak,
                         pShaders_ak);

    for (int i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
      glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
      glDeleteShader(pShaders_ak[i_ak]);
      pShaders_ak[i_ak] = 0;
    }

    free(pShaders_ak);
    glDeleteProgram(gShaderProgramObject_ak);
    gShaderProgramObject_ak = 0;

    glUseProgram(0);
  }
  if (gpFile_ak) {
    fprintf(gpFile_ak,
            "%s\t%s\t%s\t%d Program terminated successfully\n",
            __DATE__,
            __TIME__,
            __FILE__,
            __LINE__);
    fclose(gpFile_ak);
    gpFile_ak = NULL;
  }
  [super dealloc];
}

@end


CVReturn
MyDisplayLinkCallback(CVDisplayLinkRef displayLink,
                      const CVTimeStamp* now,
                      const CVTimeStamp* outputTime,
                      CVOptionFlags flagsIn,
                      CVOptionFlags* flagsOut,
                      void* displayLinkContext)
{
  CVReturn result =
    [(MyOpenGLView*)displayLinkContext getFrameForTime:outputTime];
  return (result);
}
