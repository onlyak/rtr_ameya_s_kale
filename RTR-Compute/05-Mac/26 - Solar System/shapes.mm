#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h> // similar to windows.h
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h> // GL.h
#include "vmath.h"
#include "sphere.h"

using namespace vmath;

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

FILE *gpFile_ak=NULL;

int day_ak = 0;
int year_ak = 0;
int hour_ak = 0;;


// forward declaration
@interface AppDelegate: NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

int main(int argc,const char* argv[])
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSApp = [NSApplication sharedApplication];
    [NSApp setDelegate:[[AppDelegate alloc] init]];
    [NSApp run];
    [pool release];
    return(0);
}

@interface MyOpenGLView : NSOpenGLView
@end

@implementation AppDelegate
{
    @private
    NSWindow *window;
    MyOpenGLView *myOpenGLView_ak;
}

// WM_CREATE
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSBundle *appBundle_ak = [NSBundle mainBundle];
    NSString *appDirPath_ak =[appBundle_ak bundlePath];
    NSString *parentDirPath_ak = [appDirPath_ak stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath_ak = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath_ak];

    const char *pszLogFileNameWithPath=[logFileNameWithPath_ak cStringUsingEncoding:NSASCIIStringEncoding];

    gpFile_ak=fopen(pszLogFileNameWithPath,"w");
    if(gpFile_ak==NULL){
        [self release];
        [NSApp terminate:self];
    }

    fprintf(gpFile_ak,"Program Started Successfully\n");

    NSRect win_rect_ak; // internally CGRect structure
    win_rect_ak = NSMakeRect(0.0, 0.0, 800.0, 600.0); // NSPoint(x, y) NSSize(width, height) from C-Library

    window=[[NSWindow alloc]
        initWithContentRect    :    win_rect_ak
                styleMask    :    NSWindowStyleMaskTitled |
                                NSWindowStyleMaskClosable |
                                NSWindowStyleMaskMiniaturizable |
                                NSWindowStyleMaskResizable
                backing        :     NSBackingStoreBuffered
                defer        :    NO    ];
    [window setTitle:@"ASK: macOS Solar system"];
    [window center];

    myOpenGLView_ak = [[MyOpenGLView alloc]initWithFrame:win_rect_ak];
    [window setContentView:myOpenGLView_ak];

    [window setDelegate:self];
    [window makeKeyAndOrderFront:self]; // setFocus, setForeGroundWindow
}

// uninitialize
-(void) applicationWillTerminate:(NSNotification *)aNotification
{
    if(gpFile_ak) {
        fprintf(gpFile_ak,"Program terminated successfully\n");
        fclose(gpFile_ak);
        gpFile_ak=NULL;
    }
}

// NSWindowDelegate's method
-(void) windowWillClose:(NSNotification *)aNotification
{
    [NSApp terminate:self];
}

-(void)dealloc
{
    [myOpenGLView_ak release];
    [window release];
    [super dealloc]; // bubbling de-allocation
}

@end

@implementation MyOpenGLView
{
    @private
    CVDisplayLinkRef displayLink_ak;
    // global variables here
    enum {
        ATTRIBUTE_POSITION = 0,
        ATTRIBUTE_COLOR,
        ATTRIBUTE_NORMAL,
        ATTRIBUTE_TEXCOORD
    };


    
GLuint gVertexShaderObject_ak;
GLuint gFragmentShaderObject_ak;
GLuint gShaderProgramObject_ak;

GLuint vao_sphere_ak;
GLuint vbo_position_sphere_ak;
GLuint vbo_normal_sphere_ak;
GLuint vbo_element_sphere_ak;

GLuint mvpMatrixUniform_ak;
GLuint colorUniform_ak;

mat4 perspectiveProjectionMatrix_ak;

float sphere_vertices_ak[1146];
float sphere_normals_ak[1146];
float sphere_textures_ak[764];
unsigned short sphere_elements_ak[2280];

int gNumVertices_ak;
int gNumElements_ak;


}

// id - returns object of any class
-(id) initWithFrame:(NSRect) frame
{
    self = [super initWithFrame: frame];
    if(self)
    {
        // pfd of windows
        NSOpenGLPixelFormatAttribute attributes_ak[] = {
            NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0
        };

        NSOpenGLPixelFormat *pixelFormat= [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes_ak]autorelease];
        if(pixelFormat==nil)
        {
            fprintf(gpFile_ak,"OpenGL pixel format error.\n");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *openGLContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:openGLContext];
    }
    return(self);
}

-(CVReturn) getFrameForTime:(const CVTimeStamp *)outputTime {
    // Multithreaded
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

-(void) prepareOpenGL
{
    [super prepareOpenGL];
    [[self openGLContext]makeCurrentContext];

    fprintf(gpFile_ak,"OpenGL version : %s\n",glGetString(GL_VERSION));
    fprintf(gpFile_ak,"GLSL version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

    // swap interval to avoid tearing
    GLint swapInt_ak=1;

    [[self openGLContext]setValues:&swapInt_ak forParameter:NSOpenGLCPSwapInterval];

    //shader block here
    gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 410 core"
        "\n"
        "in vec4 vPosition;"
        "uniform mat4 u_mvp_matrix;"
        "void main(void)"
        "{"
        "gl_Position = u_mvp_matrix * vPosition;"
        "}";

    glShaderSource(gVertexShaderObject_ak, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(gVertexShaderObject_ak);

    GLint infoLogLength_ak = 0;
    GLint shaderCompileStatus_ak = 0;
    char* szBuffer_ak = NULL;
    glGetShaderiv(gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
        glGetShaderiv(gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetShaderInfoLog(gVertexShaderObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Vertex shader compilation log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                [self dealloc];
            }
        }
    }

    gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentSourceCode_ak =
        "#version 410 core"
        "\n"
        "out vec4 FragColor;"
        "uniform vec3 u_color;"
        "void main(void)"
        "{"
        "FragColor = vec4(u_color, 1.0);"
        "}";

    glShaderSource(gFragmentShaderObject_ak, 1, (const GLchar**)&fragmentSourceCode_ak, NULL);
    glCompileShader(gFragmentShaderObject_ak);

    infoLogLength_ak = 0;
    shaderCompileStatus_ak = 0;
    szBuffer_ak = NULL;
    glGetShaderiv(gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
        glGetShaderiv(gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetShaderInfoLog(gFragmentShaderObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Fragment shader compilation log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                [self dealloc];
            }
        }
    }

    gShaderProgramObject_ak = glCreateProgram();
    glAttachShader(gShaderProgramObject_ak, gVertexShaderObject_ak);
    glAttachShader(gShaderProgramObject_ak, gFragmentShaderObject_ak);

    glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");

    glLinkProgram(gShaderProgramObject_ak);

    GLint shaderProgramLinkStatus = 0;
    glGetProgramiv(gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
    if (shaderProgramLinkStatus == GL_FALSE) {
        glGetProgramiv(gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetProgramInfoLog(gShaderProgramObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Shader program link log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                [self dealloc];
            }
        }
    }

    mvpMatrixUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_mvp_matrix");
    colorUniform_ak = glGetUniformLocation(gShaderProgramObject_ak, "u_color");

    Sphere *sphere = new Sphere();
    sphere->getSphereVertexData(sphere_vertices_ak, sphere_normals_ak, sphere_textures_ak, sphere_elements_ak);
    gNumVertices_ak = sphere->getNumberOfSphereVertices();
    gNumElements_ak = sphere->getNumberOfSphereElements();

    glGenVertexArrays(1, &vao_sphere_ak);
    glBindVertexArray(vao_sphere_ak);

    glGenBuffers(1, &vbo_position_sphere_ak);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere_ak);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices_ak), sphere_vertices_ak, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &vbo_normal_sphere_ak);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere_ak);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals_ak), sphere_normals_ak, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &vbo_element_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements_ak), sphere_elements_ak, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    perspectiveProjectionMatrix_ak = mat4::identity();  // eqiuivalent to GLLoadIdentity

    // CV and CGL related code

    // create display link
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink_ak);

    // set callback function
    CVDisplayLinkSetOutputCallback(displayLink_ak,&MyDisplayLinkCallback,self);

    // convert NSOpenGLContext to CGL context
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];

    // convert pixel format
    CGLPixelFormatObj cglPixelFormatObj=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];

    // set converted context
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink_ak,cglContext,cglPixelFormatObj);

    // start display link
    CVDisplayLinkStart(displayLink_ak);

}

-(void) reshape
{
    [super reshape];
    // lock context as we are using multiple threads can be done using CGL
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    NSRect rect=[self bounds];

    if(rect.size.height < 0)
        rect.size.height=1;
    int width = rect.size.width;
    int height = rect.size.height;
    glViewport(0,0,(GLsizei)width,(GLsizei)height);
    perspectiveProjectionMatrix_ak = vmath::perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void) drawRect:(NSRect) dirtyRect
{
    [self drawView];
}

-(void)drawView
{
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Render
glUseProgram(gShaderProgramObject_ak);

    mat4 modelViewMatrix1_ak = mat4::identity();
    mat4 modelViewMatrix2_ak = mat4::identity();
    mat4 modelViewMatrix3_ak = mat4::identity();

    mat4 modelViewProjectionMatrix_ak;
    mat4 translateMatrix_ak;
    mat4 xRotationMatrix_ak;
    mat4 yRotationMatrix_ak;
    mat4 zRotationMatrix_ak;
    mat4 scaleMatrix_ak;

    modelViewProjectionMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();
    xRotationMatrix_ak = mat4::identity();
    yRotationMatrix_ak = mat4::identity();
    zRotationMatrix_ak = mat4::identity();
    scaleMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -5.0f);
    modelViewMatrix1_ak = translateMatrix_ak;
    xRotationMatrix_ak = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
    modelViewProjectionMatrix_ak = perspectiveProjectionMatrix_ak * modelViewMatrix1_ak;
    glUniformMatrix4fv(mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix_ak);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    GLfloat sunColor_ak[] = { 1.0f, 1.0f, 0.0f };
    glUniform3fv(colorUniform_ak, 1, sunColor_ak);
    glBindVertexArray(vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);

    yRotationMatrix_ak = vmath::rotate((GLfloat)year_ak, 0.0f, 1.0f, 0.0f);
    translateMatrix_ak = vmath::translate(1.7f, 0.0f, 0.0f);

    modelViewMatrix2_ak = modelViewMatrix1_ak * yRotationMatrix_ak * translateMatrix_ak;

    yRotationMatrix_ak = vmath::rotate((GLfloat)day_ak, 0.0f, 1.0f, 0.0f);
    scaleMatrix_ak = vmath::scale(0.5f, 0.5f, 0.5f);
    modelViewMatrix3_ak = modelViewMatrix2_ak * yRotationMatrix_ak * scaleMatrix_ak;

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    modelViewProjectionMatrix_ak = perspectiveProjectionMatrix_ak * modelViewMatrix3_ak;
    glUniformMatrix4fv(mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix_ak);

    GLfloat earthColor[] = { 0.4f, 0.9f, 1.0f };
    glUniform3fv(colorUniform_ak, 1, earthColor);
    glBindVertexArray(vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);

    yRotationMatrix_ak = vmath::rotate((GLfloat)day_ak, 0.0f, 1.0f, 0.0f);
    translateMatrix_ak = vmath::translate(0.7f, 0.0f, 0.0f);

    modelViewMatrix3_ak = modelViewMatrix2_ak * yRotationMatrix_ak * translateMatrix_ak;

    yRotationMatrix_ak = vmath::rotate((GLfloat)hour_ak, 0.0f, 1.0f, 0.0f);
    scaleMatrix_ak = vmath::scale(0.3f, 0.3f, 0.3f);
    modelViewMatrix3_ak = modelViewMatrix3_ak * yRotationMatrix_ak * scaleMatrix_ak;
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    modelViewProjectionMatrix_ak = perspectiveProjectionMatrix_ak * modelViewMatrix3_ak;
    glUniformMatrix4fv(mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix_ak);

    GLfloat moonColor[] = { 1.0f, 1.0f, 1.0f };
    glUniform3fv(colorUniform_ak, 1, moonColor);
    glBindVertexArray(vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);

    modelViewProjectionMatrix_ak = perspectiveProjectionMatrix_ak * modelViewMatrix2_ak;
    glUniformMatrix4fv(mvpMatrixUniform_ak, 1, GL_FALSE, modelViewProjectionMatrix_ak);

    glUseProgram(0);
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    Update();
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}
void Update(void) {
  
}
-(BOOL) acceptsFirstResponder{
    [[self window]makeFirstResponder:self];
    return YES;
}
// WM_KEYDOWN
-(void) keyDown:(NSEvent *)theEvent{
    int key = (int)[[theEvent characters]characterAtIndex:0];
    switch(key){
        //Esc
        case 27:
            [self release];
            [NSApp terminate: self];
            break;
        
        case 70:
        case 102:
            [[self window]toggleFullScreen:self];
            break;
            case 68:
            day_ak = (day_ak + 6) % 360;
            hour_ak = (hour_ak + 1) % 360;
            break;
            case 100:
            day_ak = (day_ak - 6) % 360;
            hour_ak = (hour_ak - 1) % 360;
            break;
            case 89:
            year_ak = (year_ak + 3) % 360;
            day_ak = (day_ak - 6) % 360;
            hour_ak = (hour_ak + 1) % 360;
            break;
            case 121:
            year_ak = (year_ak - 3) % 360;
            day_ak = (day_ak + 6) % 360;
            hour_ak = (hour_ak - 1) % 360;
            break;
            case 72:
            hour_ak = (hour_ak + 1) % 360;
            break;
            case 104:
            hour_ak = (hour_ak - 1) % 360;
            break;

    }
}
// WM_LBUTTONDOWN
-(void) mouseDown:(NSEvent *)theEvent{
    
}

// WM_RBUTTONDOWN
-(void) rightMouseDown:(NSEvent *)theEvent{
    
}

// WM_MBUTTONDOWN
-(void) otherMouseDown:(NSEvent *)theEvent{
    
}

-(void) dealloc{
    CVDisplayLinkStop(displayLink_ak);
    CVDisplayLinkRelease(displayLink_ak);
    // Uninitialize
       if (vao_sphere_ak) {
        glDeleteVertexArrays(1, &vao_sphere_ak);
        vao_sphere_ak = 0;
    }

    if (vbo_position_sphere_ak) {
        glDeleteVertexArrays(1, &vbo_position_sphere_ak);
        vbo_position_sphere_ak = 0;
    }

    if (vbo_normal_sphere_ak) {
        glDeleteVertexArrays(1, &vbo_normal_sphere_ak);
        vbo_normal_sphere_ak = 0;
    }

    if (vbo_element_sphere_ak) {
        glDeleteVertexArrays(1, &vbo_element_sphere_ak);
        vbo_element_sphere_ak = 0;
    }

    if (gShaderProgramObject_ak) {
        glUseProgram(gShaderProgramObject_ak);
        GLsizei shaderCount_ak;
        glGetProgramiv(gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

        GLuint* pShaders_ak = NULL;
        pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
        glGetAttachedShaders(gShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);

        for (int i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
            glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
            glDeleteShader(pShaders_ak[i_ak]);
            pShaders_ak[i_ak] = 0;
        }

        free(pShaders_ak);
        glDeleteProgram(gShaderProgramObject_ak);
        gShaderProgramObject_ak = 0;

        glUseProgram(0);
    }
    if (gpFile_ak) {
        fclose(gpFile_ak);
        gpFile_ak = NULL;
    }
    [super dealloc];
}

@end

// Global C space functions
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp *outputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void *displayLinkContext)
{
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];
    return(result);
}
