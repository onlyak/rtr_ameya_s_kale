#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h> // similar to windows.h
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h> // GL.h
#include "vmath.h"
#include "sphere.h"

using namespace vmath;

Sphere *sphere;

GLfloat materialAmbient_ak[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat materialDiffuse_ak[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular_ak[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess_ak = 50.0f;

int pvpf = 1;


//Per Vertex
GLuint gVertexShaderObject_ak_pv;
GLuint gFragmentShaderObject_ak_pv;

//Per Fragment
GLuint gVertexShaderObject_ak_pf;
GLuint gFragmentShaderObject_ak_pf;

GLuint gShaderProgramObject_ak_pv;
GLuint gShaderProgramObject_ak_pf;

GLuint Vao_sphere;
GLuint Vbo_position_sphere;
GLuint Vbo_element_sphere;
GLuint Vbo_normal_sphere;

struct Light
{
    GLfloat lightAmbient_ak[3];
    GLfloat lightDiffuse_ak[3];
    GLfloat lightSpecular_ak[3];
    GLfloat lightPosition_ak[4];
};
struct Light lights[3];

//Uniforms
/********** 3 Uniforms**************/
GLuint ldUniform;
GLuint kdUniform;
GLuint lightPositionUniform;

/************ 9 Uniforms******************/
GLuint laUniform; // light component of ambient light
GLuint kaUniform;

GLuint lsUniform;
GLuint ksUniform;

GLuint shininessUniform;  //single float value

GLuint modelUniform;
GLuint viewUniform;
GLuint projectionUniform;

GLuint lKeyPressedUniform;

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

FILE *gpFile_ak=NULL;

// forward declaration
@interface AppDelegate: NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

int main(int argc,const char* argv[])
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSApp = [NSApplication sharedApplication];
    [NSApp setDelegate:[[AppDelegate alloc] init]];
    [NSApp run];
    [pool release];
    return(0);
}

@interface MyOpenGLView : NSOpenGLView
@end

@implementation AppDelegate
{
    @private
    NSWindow *window;
    MyOpenGLView *myOpenGLView_ak;
}

// WM_CREATE
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSBundle *appBundle_ak = [NSBundle mainBundle];
    NSString *appDirPath_ak =[appBundle_ak bundlePath];
    NSString *parentDirPath_ak = [appDirPath_ak stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath_ak = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath_ak];

    const char *pszLogFileNameWithPath=[logFileNameWithPath_ak cStringUsingEncoding:NSASCIIStringEncoding];

    gpFile_ak=fopen(pszLogFileNameWithPath,"w");
    if(gpFile_ak==NULL){
        [self release];
        [NSApp terminate:self];
    }

    fprintf(gpFile_ak,"Program Started Successfully\n");

    NSRect win_rect_ak; // internally CGRect structure
    win_rect_ak = NSMakeRect(0.0, 0.0, 800.0, 600.0); // NSPoint(x, y) NSSize(width, height) from C-Library

    window=[[NSWindow alloc]
        initWithContentRect    :    win_rect_ak
                styleMask    :    NSWindowStyleMaskTitled |
                                NSWindowStyleMaskClosable |
                                NSWindowStyleMaskMiniaturizable |
                                NSWindowStyleMaskResizable
                backing        :     NSBackingStoreBuffered
                defer        :    NO    ];
    [window setTitle:@"ASK: Per Vertex Per Fragment Lighting"];
    [window center];

    myOpenGLView_ak = [[MyOpenGLView alloc]initWithFrame:win_rect_ak];
    [window setContentView:myOpenGLView_ak];

    [window setDelegate:self];
    [window makeKeyAndOrderFront:self]; // setFocus, setForeGroundWindow
}

// uninitialize
-(void) applicationWillTerminate:(NSNotification *)aNotification
{
    if(gpFile_ak) {
        fprintf(gpFile_ak,"Program terminated successfully\n");
        fclose(gpFile_ak);
        gpFile_ak=NULL;
    }
}

// NSWindowDelegate's method
-(void) windowWillClose:(NSNotification *)aNotification
{
    [NSApp terminate:self];
}

-(void)dealloc
{
    [myOpenGLView_ak release];
    [window release];
    [super dealloc]; // bubbling de-allocation
}

@end

@implementation MyOpenGLView
{
    @private
    CVDisplayLinkRef displayLink_ak;
    // global variables here
    enum {
        ATTRIBUTE_POSITION = 0,
        ATTRIBUTE_COLOR,
        ATTRIBUTE_NORMAL,
        ATTRIBUTE_TEXCOORD
    };

    GLuint Vao_sphere_Ak;
    GLuint Vbo_position_sphere_ak;
    GLuint Vbo_element_sphere_ak;
    GLuint Vbo_normal_sphere_ak;

    GLuint bLight;
    
    float angle_ak;

    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    unsigned short sphere_elements[2280];

    int numVertices;
    int numElements;
    
    vmath::mat4 perspectiveProjectionMatrix_ak;
}

// id - returns object of any class
-(id) initWithFrame:(NSRect) frame
{
    self = [super initWithFrame: frame];
    if(self)
    {
        // pfd of windows
        NSOpenGLPixelFormatAttribute attributes_ak[] = {
            NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0
        };

        NSOpenGLPixelFormat *pixelFormat= [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes_ak]autorelease];
        if(pixelFormat==nil)
        {
            fprintf(gpFile_ak,"OpenGL pixel format error.\n");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *openGLContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:openGLContext];
    }
    return(self);
}

-(CVReturn) getFrameForTime:(const CVTimeStamp *)outputTime {
    // Multithreaded
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

-(void) prepareOpenGL
{
        [super prepareOpenGL];
        [[self openGLContext]makeCurrentContext];

        fprintf(gpFile_ak,"OpenGL version : %s\n",glGetString(GL_VERSION));
        fprintf(gpFile_ak,"GLSL version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

        // swap interval to avoid tearing
        GLint swapInt_ak=1;
        bLight = 0;
        
        angle_ak = 0.0f;
            
        sphere = new Sphere();
        
        [[self openGLContext]setValues:&swapInt_ak forParameter:NSOpenGLCPSwapInterval];
    
        //Shader attributes for sphere
        sphere->getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere->getNumberOfSphereVertices();
        numElements = sphere->getNumberOfSphereElements();
    
        void commonUniformLocation(GLuint);


        //Per Vertex Shader
        //********** Vertex Shader runs per vertex **********
        /* 1. When we specify core, we tell OpenGL to use Programmable Pipeline. i.e Core Profile
           2. in/out are glsl language specifier. Known to shader language only.
              in---> loads incoming data from main program. It loads data only once.
           3. vec4--> [x,y,z,w]
           4. uniform---> load incoming data from main program. It loads data multiple times
           5. gl_Position---> in-built variable of Vertex Shader
        */
        // out_color is sent as an input to Fragment Shader

        /*******Per Vertex Shader start***********/
        //Per Vertex Vertex Shader`
        gVertexShaderObject_ak_pv = glCreateShader(GL_VERTEX_SHADER);
        const GLchar *vertexShaderSourceCode_pv =
        "#version 410 core" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;"  \
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform int u_LKeyPressed;" \
        "uniform vec3 u_Ld[3];" \
        "uniform vec3 u_Kd;" \
        "uniform vec4 u_light_position[3];" \
        "uniform vec3 u_La[3];" \
        "uniform vec3 u_Ls[3];" \
        "uniform vec3 u_Ka;" \
        "uniform vec3 u_Ks;" \
        "uniform float u_Shininess;" \
        "out vec3 phong_ads_light;" \
        "void main(void)" \
        "{" \
            "if (u_LKeyPressed == 1) " \
            "{" \
                "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
                "vec3 transformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
                "vec3 view_vector = normalize(-eyeCoordinates.xyz);" \
                "phong_ads_light = vec3(0.0,0.0,0.0);"
                "vec3 light_direction[3];"
                "vec3 reflection_vector[3];"
                "vec3 ambient[3];"
                "vec3 diffuse[3];"
                "vec3 specular[3];"
                "for(int i =0; i<3 ; i++)"
                "{"
                "light_direction[i] = normalize(vec3(u_light_position[i] - eyeCoordinates));" \
                "reflection_vector[i] = reflect(-light_direction[i], transformed_normal);" \
                "ambient[i] = u_La[i] * u_Ka;" \
                "diffuse[i] = u_Ld[i] * u_Kd * max(dot(light_direction[i],transformed_normal),0.0);" \
                "specular[i] = u_Ls[i] * u_Ks * pow(max(dot(reflection_vector[i],view_vector),0.0),u_Shininess);" \
                "phong_ads_light = phong_ads_light + ambient[i] + diffuse[i] + specular[i];" \
                "}"
            "}" \
            "else" \
            "{" \
                "phong_ads_light = vec3(1.0f,1.0f,1.0f);" \
            "}"
            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "}";
        
        glShaderSource(gVertexShaderObject_ak_pv,1,(const GLchar **)&vertexShaderSourceCode_pv, NULL);

        // compile Vertex Shader
        glCompileShader(gVertexShaderObject_ak_pv);

        // Error checking for Vertex Shader
        GLint infoLogLength = 0;
        GLint shaderCompiledStatus = 0;
        char *szBuffer = NULL;

        glGetShaderiv(gVertexShaderObject_ak_pv, GL_COMPILE_STATUS, &shaderCompiledStatus);
        if(shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObject_ak_pv, GL_INFO_LOG_LENGTH, &infoLogLength);
            if(infoLogLength>0)
            {
                szBuffer = (char *)malloc(infoLogLength);
                if(szBuffer!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gVertexShaderObject_ak_pv, infoLogLength, &written, szBuffer);
                    fprintf(gpFile_ak, "Vertex Shader Compilation Log: %s\n",szBuffer);
                    free(szBuffer);
                    szBuffer = NULL;
                }
            }
        }

        //Per Vertex Fragment Shader
        /* out_color is the output of Vertex Shader */
        gFragmentShaderObject_ak_pv = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar* vertexFragmentSourceCode_pv =
        "#version 410 core" \
        "\n" \
        "in vec3 phong_ads_light;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
            "FragColor = vec4(phong_ads_light, 1.0f);"  \
        "}";

        glShaderSource(gFragmentShaderObject_ak_pv,1,(const GLchar **)&vertexFragmentSourceCode_pv, NULL);

        // compile Fragment Shader
        glCompileShader(gFragmentShaderObject_ak_pv);

        // Error Checking for Fragment Shader
        glGetShaderiv(gFragmentShaderObject_ak_pv, GL_COMPILE_STATUS, &shaderCompiledStatus);
        if(shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObject_ak_pv, GL_INFO_LOG_LENGTH, &infoLogLength);
            if(infoLogLength>0)
            {
                szBuffer = (char *)malloc(infoLogLength);
                if(szBuffer!=NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gFragmentShaderObject_ak_pv, infoLogLength, &written, szBuffer);
                    fprintf(gpFile_ak, "Fragment Shader Compilation Log: %s\n",szBuffer);
                    free(szBuffer);
                    szBuffer = NULL;
                }
            }
        }

        //Shader Program
        gShaderProgramObject_ak_pv = glCreateProgram();
        glAttachShader(gShaderProgramObject_ak_pv,gVertexShaderObject_ak_pv);
        glAttachShader(gShaderProgramObject_ak_pv,gFragmentShaderObject_ak_pv);

        // Bind the attributes in shader with the enums in your main program
        /* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
        glBindAttribLocation(gShaderProgramObject_ak_pv, ATTRIBUTE_POSITION, "vPosition");

        glBindAttribLocation(gShaderProgramObject_ak_pv, ATTRIBUTE_NORMAL, "vNormal");
        
        glLinkProgram(gShaderProgramObject_ak_pv);

        // Linking Error Checking
        GLint shaderProgramLinkStatus = 0;
        szBuffer = NULL;

        glGetProgramiv(gShaderProgramObject_ak_pv, GL_LINK_STATUS, &shaderProgramLinkStatus);
        if (shaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObject_ak_pv, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                szBuffer = (char*)malloc(infoLogLength);
                if (szBuffer != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(gShaderProgramObject_ak_pv, infoLogLength, &written, szBuffer);
                    fprintf(gpFile_ak, "Shader Program Link Log: %s\n", szBuffer);
                    free(szBuffer);
                    szBuffer = NULL;
                }
            }
        }

        if(pvpf == 1)
            commonUniformLocation(gShaderProgramObject_ak_pv);


        /*******Per Vertex Shader End***********/

        /************Per Fragment Shader Start********************************/
        gVertexShaderObject_ak_pf = glCreateShader(GL_VERTEX_SHADER);
        const GLchar* vertexShaderSourceCode =
            "#version 410 core" \
            "\n" \
            "in vec4 vPosition;" \
            "in vec3 vNormal;"  \
            "uniform mat4 u_model_matrix;" \
            "uniform mat4 u_projection_matrix;" \
            "uniform mat4 u_view_matrix;" \
            "uniform int u_LKeyPressed;" \
            "uniform vec4 u_light_position[3];" \
            "out vec3 transformed_normal;" \
            "out vec3 light_direction[3];" \
            "out vec3 view_vector;" \
            "void main(void)" \
            "{" \
            "if (u_LKeyPressed == 1) " \
            "{" \
            "vec4 eyeCoordinates = u_view_matrix * u_model_matrix * vPosition;" \
            "transformed_normal = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
            "view_vector = -eyeCoordinates.xyz;" \
            "for(int i = 0; i<3; i++)"
                "{"
                    "light_direction[i] = vec3(u_light_position[i] - eyeCoordinates);" \
                "}"
            "}" \
            "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
            "}";

        glShaderSource(gVertexShaderObject_ak_pf, 1, (const GLchar**)&vertexShaderSourceCode, NULL);

        // compile Vertex Shader
        glCompileShader(gVertexShaderObject_ak_pf);

        // Error checking for Vertex Shader
        infoLogLength = 0;
        shaderCompiledStatus = 0;
        szBuffer = NULL;

        glGetShaderiv(gVertexShaderObject_ak_pf, GL_COMPILE_STATUS, &shaderCompiledStatus);
        if (shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObject_ak_pf, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                szBuffer = (char*)malloc(infoLogLength);
                if (szBuffer != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gVertexShaderObject_ak_pf, infoLogLength, &written, szBuffer);
                    fprintf(gpFile_ak, "Vertex Shader Compilation Log: %s\n", szBuffer);
                    free(szBuffer);
                    szBuffer = NULL;
                }
            }
        }

        //Fragment Shader
        /* out_color is the output of Vertex Shader */
        gFragmentShaderObject_ak_pf = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar* vertexFragmentSourceCode =
            "#version 410 core" \
            "\n" \
            "out vec4 FragColor;" \
            "in vec3 transformed_normal;" \
            "in vec3 light_direction[3];" \
            "in vec3 view_vector;" \
            "uniform vec3 u_Ld[3];" \
            "uniform vec3 u_Kd;" \
            "uniform vec3 u_La[3];" \
            "uniform vec3 u_Ls[3];" \
            "uniform vec3 u_Ka;" \
            "uniform vec3 u_Ks;" \
            "uniform float u_Shininess;" \
            "uniform int u_LKeyPressed;" \
            "void main(void)" \
            "{" \
            "vec3 phong_ads_color;" \
            "if(u_LKeyPressed == 1)" \
            "{" \
            "vec3 norm_transformed_normal = normalize(transformed_normal);" \
            "vec3 norm_view_vector = normalize(view_vector);" \
            "vec3 norm_light_direction[3];"
            "vec3 reflection_vector[3];"
            "vec3 ambient[3];"
            "vec3 diffuse[3];"
            "vec3 specular[3];"
            "for(int i=0;i<3;i++)"
            "{"
            "norm_light_direction[i] = normalize(light_direction[i]);" \
            "reflection_vector[i] = reflect(-norm_light_direction[i], norm_transformed_normal);" \
            "ambient[i] = u_La[i] * u_Ka;" \
            "diffuse[i] = u_Ld[i] * u_Kd * max(dot(norm_light_direction[i],norm_transformed_normal),0.0);" \
            "specular[i] = u_Ls[i] * u_Ks * pow(max(dot(reflection_vector[i],norm_view_vector),0.0),u_Shininess);" \
            "phong_ads_color = phong_ads_color + ambient[i] + diffuse[i] + specular[i];" \
            "}"
            "}" \
            "else" \
            "{" \
            "phong_ads_color = vec3(1.0f,1.0f,1.0f);" \
            "}" \

            "FragColor = vec4(phong_ads_color, 1.0f);" \
            "}";

        glShaderSource(gFragmentShaderObject_ak_pf, 1, (const GLchar**)&vertexFragmentSourceCode, NULL);

        // compile Fragment Shader
        glCompileShader(gFragmentShaderObject_ak_pf);

        infoLogLength = 0;
        shaderCompiledStatus = 0;
        szBuffer = NULL;

        // Error Checking for Fragment Shader
        glGetShaderiv(gFragmentShaderObject_ak_pf, GL_COMPILE_STATUS, &shaderCompiledStatus);
        if (shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObject_ak_pf, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                szBuffer = (char*)malloc(infoLogLength);
                if (szBuffer != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gFragmentShaderObject_ak_pf, infoLogLength, &written, szBuffer);
                    fprintf(gpFile_ak, "Fragment Shader Compilation Log: %s\n", szBuffer);
                    free(szBuffer);
                    szBuffer = NULL;
                }
            }
        }

        //Shader Program
        gShaderProgramObject_ak_pf = glCreateProgram();
        glAttachShader(gShaderProgramObject_ak_pf, gVertexShaderObject_ak_pf);
        glAttachShader(gShaderProgramObject_ak_pf, gFragmentShaderObject_ak_pf);

        // Bind the attributes in shader with the enums in your main program
        /* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
        glBindAttribLocation(gShaderProgramObject_ak_pf, ATTRIBUTE_POSITION, "vPosition");

        glBindAttribLocation(gShaderProgramObject_ak_pf, ATTRIBUTE_NORMAL, "vNormal");

        glLinkProgram(gShaderProgramObject_ak_pf);

        // Linking Error Checking
        shaderProgramLinkStatus = 0;
        szBuffer = NULL;

        glGetProgramiv(gShaderProgramObject_ak_pf, GL_LINK_STATUS, &shaderProgramLinkStatus);
        if (shaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObject_ak_pf, GL_INFO_LOG_LENGTH, &infoLogLength);
            if (infoLogLength > 0)
            {
                szBuffer = (char*)malloc(infoLogLength);
                if (szBuffer != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(gShaderProgramObject_ak_pf, infoLogLength, &written, szBuffer);
                    fprintf(gpFile_ak, "Shader Program Link Log: %s\n", szBuffer);
                    free(szBuffer);
                    szBuffer = NULL;
                }
            }
        }

        if (pvpf == 0)
            commonUniformLocation(gShaderProgramObject_ak_pf);

        /*******************Per Fragment Shader Ends**************************/

        

        // Push the above vertices to vPosition

        //Steps
        /* 1. Tell OpenGl to create one buffer in your VRAM
              Give me a symbol to identify. It is known as NamedBuffer
              In OpenGL terminology, it is called as GL_ARRAY_BUFFER. This is becase vertex has plenty of attributes
              like color, texture, etc. Also it requires contiguous memory.
              User identifies this variable as Vbo_position and GPU as GL_ARRAY_BUFFER.
           2. Bind with the above symbol. It doesn't unbind until 'unbind step' is performed eg- Railway track
           3. Insert triangle data into the buffer.
           4. Specify where to insert this data into shader and also how to use it.
           5. Enable the 'in' point.
           6. Unbind
        */

        //Sphere
        
        // Sphere Vao
        glGenVertexArrays(1, &Vao_sphere);
        glBindVertexArray(Vao_sphere);

        // Sphere position Vbo
        glGenBuffers(1, &Vbo_position_sphere);
        glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_sphere);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        // Sphere element Vbo
        glGenBuffers(1, &Vbo_element_sphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Vbo_element_sphere);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        // Sphere normals Vbo
        glGenBuffers(1, &Vbo_normal_sphere);
        glBindBuffer(GL_ARRAY_BUFFER, Vbo_normal_sphere);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
        glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(ATTRIBUTE_NORMAL);

        // unbind vao
        glBindVertexArray(0);

        lights[0].lightAmbient_ak[0] = 0.0f;
        lights[0].lightAmbient_ak[1] = 0.0f;
        lights[0].lightAmbient_ak[2] = 0.0f;

        lights[1].lightAmbient_ak[0] = 0.0f;
        lights[1].lightAmbient_ak[1] = 0.0f;
        lights[1].lightAmbient_ak[2] = 0.0f;

        lights[2].lightAmbient_ak[0] = 0.0f;
        lights[2].lightAmbient_ak[1] = 0.0f;
        lights[2].lightAmbient_ak[2] = 0.0f;

        lights[0].lightDiffuse_ak[0] = 1.0f;
        lights[0].lightDiffuse_ak[1] = 0.0f;
        lights[0].lightDiffuse_ak[2] = 0.0f;

        lights[1].lightDiffuse_ak[0] = 0.0f;
        lights[1].lightDiffuse_ak[1] = 1.0f;
        lights[1].lightDiffuse_ak[2] = 0.0f;

        lights[2].lightDiffuse_ak[0] = 0.0f;
        lights[2].lightDiffuse_ak[1] = 0.0f;
        lights[2].lightDiffuse_ak[2] = 1.0f;

        lights[0].lightSpecular_ak[0] = 1.0f;
        lights[0].lightSpecular_ak[1] = 0.0f;
        lights[0].lightSpecular_ak[2] = 0.0f;

        lights[1].lightSpecular_ak[0] = 0.0f;
        lights[1].lightSpecular_ak[1] = 1.0f;
        lights[1].lightSpecular_ak[2] = 0.0f;

        lights[2].lightSpecular_ak[0] = 0.0f;
        lights[2].lightSpecular_ak[1] = 0.0f;
        lights[2].lightSpecular_ak[2] = 1.0f;

        lights[0].lightPosition_ak[0] = 0.0f;
        lights[0].lightPosition_ak[1] = 0.0f;
        lights[0].lightPosition_ak[2] = 0.0f;
        lights[0].lightPosition_ak[3] = 1.0f;

        lights[1].lightPosition_ak[0] = 0.0f;
        lights[1].lightPosition_ak[1] = 0.0f;
        lights[1].lightPosition_ak[2] = 0.0f;
        lights[1].lightPosition_ak[3] = 1.0f;

        lights[2].lightPosition_ak[0] = 0.0f;
        lights[2].lightPosition_ak[1] = 0.0f;
        lights[2].lightPosition_ak[2] = 0.0f;
        lights[2].lightPosition_ak[3] = 1.0f;

        glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        //glEnable(GL_CULL_FACE);
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        perspectiveProjectionMatrix_ak = vmath::mat4::identity();  // eqiuivalent to GLLoadIdentity

        // CV and CGL related code

        // create display link
        CVDisplayLinkCreateWithActiveCGDisplays(&displayLink_ak);

        // set callback function
        CVDisplayLinkSetOutputCallback(displayLink_ak,&MyDisplayLinkCallback,self);

        // convert NSOpenGLContext to CGL context
        CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];

        // convert pixel format
        CGLPixelFormatObj cglPixelFormatObj=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];

        // set converted context
        CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink_ak,cglContext,cglPixelFormatObj);

        // start display link
        CVDisplayLinkStart(displayLink_ak);

}

-(void) reshape
{
    [super reshape];
    // lock context as we are using multiple threads can be done using CGL
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    NSRect rect=[self bounds];

    if(rect.size.height < 0)
        rect.size.height=1;
    int width = rect.size.width;
    int height = rect.size.height;
    glViewport(0,0,(GLsizei)width,(GLsizei)height);
    perspectiveProjectionMatrix_ak = vmath::perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void) drawRect:(NSRect) dirtyRect
{
    [self drawView];
}

-(void)drawView
{
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Render

    GLfloat lightAmbient[9] = {
        lights[0].lightAmbient_ak[0],
        lights[0].lightAmbient_ak[1],
        lights[0].lightAmbient_ak[2],

        lights[1].lightAmbient_ak[0],
        lights[1].lightAmbient_ak[1],
        lights[1].lightAmbient_ak[2],

        lights[2].lightAmbient_ak[0],
        lights[2].lightAmbient_ak[1],
        lights[2].lightAmbient_ak[2]

        };

        GLfloat lightDiffuse[9] = {

        lights[0].lightDiffuse_ak[0],
        lights[0].lightDiffuse_ak[1],
        lights[0].lightDiffuse_ak[2],

        lights[1].lightDiffuse_ak[0],
        lights[1].lightDiffuse_ak[1],
        lights[1].lightDiffuse_ak[2],

        lights[2].lightDiffuse_ak[0],
        lights[2].lightDiffuse_ak[1],
        lights[2].lightDiffuse_ak[2]

        };

        GLfloat lightSpecular[9] = {

        lights[0].lightSpecular_ak[0],
        lights[0].lightSpecular_ak[1],
        lights[0].lightSpecular_ak[2],

        lights[1].lightSpecular_ak[0],
        lights[1].lightSpecular_ak[1],
        lights[1].lightSpecular_ak[2],


        lights[2].lightSpecular_ak[0],
        lights[2].lightSpecular_ak[1],
        lights[2].lightSpecular_ak[2]

        };

        GLfloat lightPosition[12] = {
        lights[0].lightPosition_ak[0],
        lights[0].lightPosition_ak[1],
        lights[0].lightPosition_ak[2],
        lights[0].lightPosition_ak[3],

        lights[1].lightPosition_ak[0],
        lights[1].lightPosition_ak[1],
        lights[1].lightPosition_ak[2],
        lights[1].lightPosition_ak[3],

        lights[2].lightPosition_ak[0],
        lights[2].lightPosition_ak[1],
        lights[2].lightPosition_ak[2],
        lights[2].lightPosition_ak[3]
        };


        if(pvpf==1)
            glUseProgram(gShaderProgramObject_ak_pv);
        else
            glUseProgram(gShaderProgramObject_ak_pf);


        if (bLight == true)
        {
            glUniform1i(lKeyPressedUniform, 1);
            glUniform3fv(laUniform,3, lightAmbient);
            glUniform3fv(ldUniform,3, lightDiffuse);
            glUniform3fv(lsUniform,3, lightSpecular);
            glUniform4fv(lightPositionUniform, 3, lightPosition);

            glUniform3fv(kaUniform, 1, materialAmbient_ak);
            glUniform3fv(kdUniform,1, materialDiffuse_ak);
            glUniform3fv(ksUniform,1, materialSpecular_ak);
            glUniform1f(shininessUniform, materialShininess_ak);

        }
        else
            glUniform1i(lKeyPressedUniform, 0);


        mat4 modelMatrix = mat4::identity();
        mat4 viewMatrix = mat4::identity();
        mat4 translationMatrix = mat4::identity();

        translationMatrix = translate(0.0f, 0.0f, -4.0f);
        modelMatrix = translationMatrix;

        glUniformMatrix4fv(modelUniform, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(viewUniform, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(projectionUniform, 1, GL_FALSE, perspectiveProjectionMatrix_ak);
        
        //Sphere Begin
        glBindVertexArray(Vao_sphere);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Vbo_element_sphere);
        glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
        
        glBindVertexArray(0); // Sphere end

        glUseProgram(0);
        
        CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
        CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
        angle_ak += 0.01f;
        if (angle_ak > 360)
            angle_ak = 0.0f;

        lights[0].lightPosition_ak[1] = 10 * sin(angle_ak);
        lights[0].lightPosition_ak[2] = 10 * cos(angle_ak);

        lights[1].lightPosition_ak[0] = 10 * sin(angle_ak);
        lights[1].lightPosition_ak[2] = 10 * cos(angle_ak);

        lights[2].lightPosition_ak[0] = 10 * sin(angle_ak);
        lights[2].lightPosition_ak[1] = 10 * cos(angle_ak);
}

-(BOOL) acceptsFirstResponder{
    [[self window]makeFirstResponder:self];
    return YES;
}
// WM_KEYDOWN
-(void) keyDown:(NSEvent *)theEvent{
    void commonUniformLocation(GLuint);
    int key = (int)[[theEvent characters]characterAtIndex:0];
    switch(key){
        //Esc
        case 27:
            [self release];
            [NSApp terminate: self];
            break;
        
        case 84:
        case 116:
            [[self window]toggleFullScreen:self];
            break;
        
        case 76:
        case 108:
            if (bLight == 0)
                bLight = 1;
            else
                bLight = 0;
            break;
            
        case 86:
        case 118:
            commonUniformLocation(gShaderProgramObject_ak_pv);
            pvpf = 0;
            break;
        
        case 70:
        case 102:
            commonUniformLocation(gShaderProgramObject_ak_pf);
            pvpf = 1;
            break;
    }
}
// WM_LBUTTONDOWN
-(void) mouseDown:(NSEvent *)theEvent{
    
}

// WM_RBUTTONDOWN
-(void) rightMouseDown:(NSEvent *)theEvent{
    
}

// WM_MBUTTONDOWN
-(void) otherMouseDown:(NSEvent *)theEvent{
    
}

-(void) dealloc{
    CVDisplayLinkStop(displayLink_ak);
    CVDisplayLinkRelease(displayLink_ak);
    // Uninitialize

    if (gShaderProgramObject_ak_pv) {
        glUseProgram(gShaderProgramObject_ak_pv);
        GLsizei shaderCount_ak;
        glGetProgramiv(gShaderProgramObject_ak_pv, GL_ATTACHED_SHADERS, &shaderCount_ak);

        GLuint* pShaders_ak = NULL;
        pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
        glGetAttachedShaders(gShaderProgramObject_ak_pv, shaderCount_ak, &shaderCount_ak, pShaders_ak);

        for (GLsizei i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
            glDetachShader(gShaderProgramObject_ak_pv, pShaders_ak[i_ak]);
            glDeleteShader(pShaders_ak[i_ak]);
            pShaders_ak[i_ak] = 0;
        }

        free(pShaders_ak);
        glDeleteProgram(gShaderProgramObject_ak_pv);
        gShaderProgramObject_ak_pv = 0;

        glUseProgram(0);
    }
    [super dealloc];
}

@end

// Global C space functions
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp *outputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void *displayLinkContext)
{
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];
    return(result);
}

void commonUniformLocation(GLuint shaderObject)
{
    modelUniform = glGetUniformLocation(shaderObject, "u_model_matrix");
    viewUniform = glGetUniformLocation(shaderObject, "u_view_matrix");
    projectionUniform = glGetUniformLocation(shaderObject, "u_projection_matrix");

    ldUniform = glGetUniformLocation(shaderObject, "u_Ld");
    kdUniform = glGetUniformLocation(shaderObject, "u_Kd");
    lightPositionUniform = glGetUniformLocation(shaderObject, "u_light_position");

    laUniform = glGetUniformLocation(shaderObject, "u_La");
    kaUniform = glGetUniformLocation(shaderObject, "u_Ka");

    lsUniform = glGetUniformLocation(shaderObject, "u_Ls");
    ksUniform = glGetUniformLocation(shaderObject, "u_Ks");

    shininessUniform = glGetUniformLocation(shaderObject, "u_Shininess");

    lKeyPressedUniform = glGetUniformLocation(shaderObject, "u_LKeyPressed");
}
