#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h> // similar to windows.h
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h> // GL.h
#include <math.h>
#include "vmath.h"
#include "sphere.h"

using namespace vmath;

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

FILE *gpFile_ak=NULL;
GLfloat sphereAngle_ak = 0.0f;
Sphere *sphere = new Sphere();


   GLfloat lightAmbient_ak[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffused_ak[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition_ak[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightSpecular_ak[] = { 1.0f, 1.0f, 1.0f };

    GLfloat materialAmbient_ak[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat materialDiffused_ak[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat materialSpecular_ak[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat materialShininess_ak = 50.0f;

    struct Light {
        GLfloat lightAmbient_ak[3];
        GLfloat lightDiffused_ak[3];
        GLfloat lightPosition_ak[4];
        GLfloat lightSpecular_ak[3];
    };

    struct Light lights[3];

    int  programToBeExecuted = 0;
     GLfloat lightAngle0_ak;
 GLfloat lightAngle1_ak = 0.0;
 GLfloat lightAngle2_ak = 0.0;

GLfloat angleForXRotation_ak = 0.0f;
GLfloat angleForYRotation_ak = 0.0f;
GLfloat angleForZRotation_ak = 0.0f;

 bool bLight_ak = 0.0;
 bool bAnimate_ak;

GLint keyPressed_ak = 0;

int gWidth = 800;
int    gHeight = 600;

// forward declaration
@interface AppDelegate: NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

int main(int argc,const char* argv[])
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSApp = [NSApplication sharedApplication];
    [NSApp setDelegate:[[AppDelegate alloc] init]];
    [NSApp run];
    [pool release];
    return(0);
}

@interface MyOpenGLView : NSOpenGLView
@end

@implementation AppDelegate
{
    @private
    NSWindow *window;
    MyOpenGLView *myOpenGLView_ak;
}

// WM_CREATE
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSBundle *appBundle_ak = [NSBundle mainBundle];
    NSString *appDirPath_ak =[appBundle_ak bundlePath];
    NSString *parentDirPath_ak = [appDirPath_ak stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath_ak = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath_ak];

    const char *pszLogFileNameWithPath=[logFileNameWithPath_ak cStringUsingEncoding:NSASCIIStringEncoding];

    gpFile_ak=fopen(pszLogFileNameWithPath,"w");
    if(gpFile_ak==NULL){
        [self release];
        [NSApp terminate:self];
    }

    fprintf(gpFile_ak,"Program Started Successfully\n");

    NSRect win_rect_ak; // internally CGRect structure
    win_rect_ak = NSMakeRect(0.0, 0.0, 800.0, 600.0); // NSPoint(x, y) NSSize(width, height) from C-Library

    window=[[NSWindow alloc]
        initWithContentRect    :    win_rect_ak
                styleMask    :    NSWindowStyleMaskTitled |
                                NSWindowStyleMaskClosable |
                                NSWindowStyleMaskMiniaturizable |
                                NSWindowStyleMaskResizable
                backing        :     NSBackingStoreBuffered
                defer        :    NO    ];
    [window setTitle:@"ASK: macOS 24 spheres"];
    [window center];

    myOpenGLView_ak = [[MyOpenGLView alloc]initWithFrame:win_rect_ak];
    [window setContentView:myOpenGLView_ak];

    [window setDelegate:self];
    [window makeKeyAndOrderFront:self]; // setFocus, setForeGroundWindow
}

// uninitialize
-(void) applicationWillTerminate:(NSNotification *)aNotification
{
    if(gpFile_ak) {
        fprintf(gpFile_ak,"Program terminated successfully\n");
        fclose(gpFile_ak);
        gpFile_ak=NULL;
    }
}

// NSWindowDelegate's method
-(void) windowWillClose:(NSNotification *)aNotification
{
    [NSApp terminate:self];
}

-(void)dealloc
{
    [myOpenGLView_ak release];
    [window release];
    [super dealloc]; // bubbling de-allocation
}

@end

@implementation MyOpenGLView
{
    @private
    CVDisplayLinkRef displayLink_ak;
    // global variables here
    enum {
        ATTRIBUTE_POSITION = 0,
        ATTRIBUTE_COLOR,
        ATTRIBUTE_NORMAL,
        ATTRIBUTE_TEXCOORD
    };



    GLuint gVertexShaderObject_ak;
    GLuint gFragmentShaderObject_ak;
    GLuint gShaderProgramObject_ak;

    GLuint vao_cube_ak;
    GLuint vbo_position_cube_ak;
    GLuint vbo_normal_cube_ak;
    GLuint modelViewMatrixUniform_ak;
    GLuint projectionMatrixUniform_ak;
    GLuint lKeyPressedUniform_ak;
    GLuint ldUniform_ak;
    GLuint kdUniform_ak;
    GLuint lightPositionUniform_ak;

GLuint perVertexLighting_VertexShaderObject_ak;
GLuint perVertexLighting_FragmentShaderObject_ak;
GLuint perVertexLighting_ShaderProgramObject_ak;

GLuint perVertexLighting_vao_sphere_ak;
GLuint perVertexLighting_vbo_position_sphere_ak;
GLuint perVertexLighting_vbo_normal_sphere_ak;
GLuint perVertexLighting_vbo_element_sphere_ak;

GLuint perVertexLighting_modelMatrixUniform_ak;
GLuint perVertexLighting_viewMatrixUniform_ak;
GLuint perVertexLighting_projectionMatrixUniform_ak;

GLuint perVertexLighting_laUniform_ak;
GLuint perVertexLighting_ldUniform_ak;
GLuint perVertexLighting_lsUniform_ak;
GLuint perVertexLighting_lightPositionUniform_ak;

GLuint perVertexLighting_kaUniform_ak;
GLuint perVertexLighting_kdUniform_ak;
GLuint perVertexLighting_ksUniform_ak;
GLuint perVertexLighting_materialShininessUniform_ak;

GLuint perVertexLighting_lKeyPressedUniform_ak;

GLuint perFragmentLighting_VertexShaderObject_ak;
GLuint perFragmentLighting_FragmentShaderObject_ak;
GLuint perFragmentLighting_ShaderProgramObject_ak;

GLuint perFragmentLighting_vao_sphere_ak;
GLuint perFragmentLighting_vbo_position_sphere_ak;
GLuint perFragmentLighting_vbo_normal_sphere_ak;
GLuint perFragmentLighting_vbo_element_sphere_ak;

GLuint perFragmentLighting_modelMatrixUniform_ak;
GLuint perFragmentLighting_viewMatrixUniform_ak;
GLuint perFragmentLighting_projectionMatrixUniform_ak;

GLuint perFragmentLighting_laUniform_ak;
GLuint perFragmentLighting_ldUniform_ak;
GLuint perFragmentLighting_lsUniform_ak;
GLuint perFragmentLighting_lightPositionUniform_ak;

GLuint perFragmentLighting_kaUniform_ak;
GLuint perFragmentLighting_kdUniform_ak;
GLuint perFragmentLighting_ksUniform_ak;
GLuint perFragmentLighting_materialShininessUniform_ak;

GLuint perFragmentLighting_lKeyPressedUniform_ak;

float sphere_vertices_ak[1146];
float sphere_normals_ak[1146];
float sphere_textures_ak[764];
unsigned short sphere_elements_ak[2280];

int gNumVertices_ak;
int gNumElements_ak;

    


    vmath::mat4 perspectiveProjectionMatrix_ak;
}

// id - returns object of any class
-(id) initWithFrame:(NSRect) frame
{
    self = [super initWithFrame: frame];
    if(self)
    {
        // pfd of windows
        NSOpenGLPixelFormatAttribute attributes_ak[] = {
            NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0
        };

        NSOpenGLPixelFormat *pixelFormat= [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes_ak]autorelease];
        if(pixelFormat==nil)
        {
            fprintf(gpFile_ak,"OpenGL pixel format error.\n");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *openGLContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:openGLContext];
    }
    return(self);
}

-(CVReturn) getFrameForTime:(const CVTimeStamp *)outputTime {
    // Multithreaded
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

-(void) prepareOpenGL
{
    [super prepareOpenGL];
    [[self openGLContext]makeCurrentContext];

    fprintf(gpFile_ak,"OpenGL version : %s\n",glGetString(GL_VERSION));
    fprintf(gpFile_ak,"GLSL version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

    // swap interval to avoid tearing
    GLint swapInt_ak=1;

    [[self openGLContext]setValues:&swapInt_ak forParameter:NSOpenGLCPSwapInterval];

     lights[0].lightAmbient_ak[0] = 0.0f;
    lights[0].lightAmbient_ak[1] = 0.0f;
    lights[0].lightAmbient_ak[2] = 0.0f;

    lights[0].lightDiffused_ak[0] = 1.0f;
    lights[0].lightDiffused_ak[1] = 0.0f;
    lights[0].lightDiffused_ak[2] = 0.0f;

    lights[0].lightPosition_ak[0] = 0.0f;
    lights[0].lightPosition_ak[1] = 0.0f;
    lights[0].lightPosition_ak[2] = 0.0f;
    lights[0].lightPosition_ak[3] = 1.0f;

    lights[0].lightSpecular_ak[0] = 1.0f;
    lights[0].lightSpecular_ak[1] = 0.0f;
    lights[0].lightSpecular_ak[2] = 0.0f;

    // blue light
    lights[1].lightAmbient_ak[0] = 0.0f;
    lights[1].lightAmbient_ak[1] = 0.0f;
    lights[1].lightAmbient_ak[2] = 0.0f;

    lights[1].lightDiffused_ak[0] = 0.0f;
    lights[1].lightDiffused_ak[1] = 1.0f;
    lights[1].lightDiffused_ak[2] = 0.0f;

    lights[1].lightPosition_ak[0] = 0.0f;
    lights[1].lightPosition_ak[1] = 0.0f;
    lights[1].lightPosition_ak[2] = 0.0f;
    lights[1].lightPosition_ak[3] = 1.0f;

    lights[1].lightSpecular_ak[0] = 0.0f;
    lights[1].lightSpecular_ak[1] = 1.0f;
    lights[1].lightSpecular_ak[2] = 0.0f;

    // blue light
    lights[2].lightAmbient_ak[0] = 0.0f;
    lights[2].lightAmbient_ak[1] = 0.0f;
    lights[2].lightAmbient_ak[2] = 0.0f;

    lights[2].lightDiffused_ak[0] = 0.0f;
    lights[2].lightDiffused_ak[1] = 0.0f;
    lights[2].lightDiffused_ak[2] = 1.0f;

    lights[2].lightPosition_ak[0] = 0.0f;
    lights[2].lightPosition_ak[1] = 0.0f;
    lights[2].lightPosition_ak[2] = 0.0f;
    lights[2].lightPosition_ak[3] = 1.0f;

    lights[2].lightSpecular_ak[0] = 0.0f;
    lights[2].lightSpecular_ak[1] = 0.0f;
    lights[2].lightSpecular_ak[2] = 1.0f;

    sphere->getSphereVertexData(sphere_vertices_ak, sphere_normals_ak, sphere_textures_ak, sphere_elements_ak);
    gNumVertices_ak = sphere->getNumberOfSphereVertices();
    gNumElements_ak = sphere->getNumberOfSphereElements();


         perVertexLighting_VertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode =
        "#version 410 core"
        "\n"
        "in vec4 vPosition;"
        "in vec3 vNormal;"
        "uniform mat4 u_model_matrix;"
        "uniform mat4 u_view_matrix;"
        "uniform mat4 u_projection_matrix;"
        "uniform vec3 u_la;"
        "uniform vec3 u_ld;"
        "uniform vec3 u_ls;"
        "uniform vec4 u_light_position;"
        "uniform vec3 u_ka;"
        "uniform vec3 u_kd;"
        "uniform vec3 u_ks;"
        "uniform float u_material_shininess;"
        "uniform int u_lkey_pressed;"
        "out vec3 phong_ads_light;"
        "void main(void)"
        "{"
        "if(u_lkey_pressed == 1)"
        "{"
        "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"
        "vec3 transformed_normal = normalize(mat3((u_view_matrix * u_model_matrix)) * vNormal);"
        "vec3 light_direction = normalize(vec3(u_light_position - eye_coordinates));"
        "vec3 reflection_vector = reflect(-light_direction, transformed_normal);"
        "vec3 view_vector = normalize(-eye_coordinates.xyz);"
        "vec3 ambient = u_la * u_ka;"
        "vec3 diffuse = u_ld * u_kd * max(dot(light_direction, transformed_normal), 0.0);"
        "vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, view_vector), 0.0), u_material_shininess);"
        "phong_ads_light = ambient + diffuse + specular;"
        "}"
        "else"
        "{"
        "phong_ads_light = vec3(1.0, 1.0, 1.0);"
        "}"
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"
        "}";

    glShaderSource(perVertexLighting_VertexShaderObject_ak, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(perVertexLighting_VertexShaderObject_ak);

    GLint infoLogLength_ak = 0;
    GLint shaderCompileStatus_ak = 0;
    char* szBuffer_ak = NULL;
    glGetShaderiv(perVertexLighting_VertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
        glGetShaderiv(perVertexLighting_VertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetShaderInfoLog(perVertexLighting_VertexShaderObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Vertex shader compilation log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                [self dealloc];
            }
        }
    }

    perVertexLighting_FragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar* fragmentSourceCode_ak =
        "#version 410 core"
        "\n"
        "in vec3 phong_ads_light;"
        "out vec4 FragColor;"
        "void main(void)"
        "{"
        "FragColor = vec4(phong_ads_light, 1.0);"
        "}";

    glShaderSource(perVertexLighting_FragmentShaderObject_ak, 1, (const GLchar**)&fragmentSourceCode_ak, NULL);
    glCompileShader(perVertexLighting_FragmentShaderObject_ak);

    infoLogLength_ak = 0;
    shaderCompileStatus_ak = 0;
    szBuffer_ak = NULL;
    glGetShaderiv(perVertexLighting_FragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
        glGetShaderiv(perVertexLighting_FragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetShaderInfoLog(perVertexLighting_FragmentShaderObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Fragment shader compilation log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                [self dealloc];
            }
        }
    }

    perVertexLighting_ShaderProgramObject_ak = glCreateProgram();
    glAttachShader(perVertexLighting_ShaderProgramObject_ak, perVertexLighting_VertexShaderObject_ak);
    glAttachShader(perVertexLighting_ShaderProgramObject_ak, perVertexLighting_FragmentShaderObject_ak);

    glBindAttribLocation(perVertexLighting_ShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(perVertexLighting_ShaderProgramObject_ak, ATTRIBUTE_NORMAL, "vNormal");

    glLinkProgram(perVertexLighting_ShaderProgramObject_ak);

    GLint shaderProgramLinkStatus = 0;
    glGetProgramiv(perVertexLighting_ShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
    if (shaderProgramLinkStatus == GL_FALSE) {
        glGetProgramiv(perVertexLighting_ShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetProgramInfoLog(perVertexLighting_ShaderProgramObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Shader program link log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                [self dealloc];
            }
        }
    }

    perVertexLighting_modelMatrixUniform_ak = glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_model_matrix");
    perVertexLighting_viewMatrixUniform_ak = glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_view_matrix");
    perVertexLighting_projectionMatrixUniform_ak = glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_projection_matrix");
    perVertexLighting_laUniform_ak = glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_la");
    perVertexLighting_ldUniform_ak = glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_ld");
    perVertexLighting_lsUniform_ak = glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_ls");
    perVertexLighting_lightPositionUniform_ak = glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_light_position");
    perVertexLighting_kaUniform_ak = glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_ka");
    perVertexLighting_kdUniform_ak = glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_kd");
    perVertexLighting_ksUniform_ak = glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_ks");
    perVertexLighting_materialShininessUniform_ak = glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_material_shininess");
    perVertexLighting_lKeyPressedUniform_ak = glGetUniformLocation(perVertexLighting_ShaderProgramObject_ak, "u_lkey_pressed");

    glGenVertexArrays(1, &perVertexLighting_vao_sphere_ak);
    glBindVertexArray(perVertexLighting_vao_sphere_ak);

    glGenBuffers(1, &perVertexLighting_vbo_position_sphere_ak);
    glBindBuffer(GL_ARRAY_BUFFER, perVertexLighting_vbo_position_sphere_ak);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices_ak), sphere_vertices_ak, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &perVertexLighting_vbo_normal_sphere_ak);
    glBindBuffer(GL_ARRAY_BUFFER, perVertexLighting_vbo_normal_sphere_ak);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals_ak), sphere_normals_ak, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &perVertexLighting_vbo_element_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements_ak), sphere_elements_ak, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
    

    perFragmentLighting_VertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
    vertexShaderSourceCode =
        "#version 410 core"
        "\n"
        "in vec4 vPosition;"
        "in vec3 vNormal;"
        "uniform mat4 u_model_matrix;"
        "uniform mat4 u_view_matrix;"
        "uniform mat4 u_projection_matrix;"
        "uniform vec4 u_light_position;"
        "uniform int u_lkey_pressed;"
        "out vec3 transformed_normal;"
        "out vec3 light_direction;"
        "out vec3 view_vector;"
        "void main(void)"
        "{"
        "if(u_lkey_pressed == 1)"
        "{"
        "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"
        "transformed_normal = mat3((u_view_matrix * u_model_matrix)) * vNormal;"
        "light_direction = vec3(u_light_position - eye_coordinates);"
        "view_vector = -eye_coordinates.xyz;"
        "}"
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"
        "}";

    glShaderSource(perFragmentLighting_VertexShaderObject_ak, 1, (const GLchar**)&vertexShaderSourceCode, NULL);
    glCompileShader(perFragmentLighting_VertexShaderObject_ak);

    infoLogLength_ak = 0;
    shaderCompileStatus_ak = 0;
    szBuffer_ak = NULL;
    glGetShaderiv(perFragmentLighting_VertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
        glGetShaderiv(perFragmentLighting_VertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetShaderInfoLog(perFragmentLighting_VertexShaderObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Vertex shader compilation log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                [self dealloc];
            }
        }
    }

    perFragmentLighting_FragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);

    fragmentSourceCode_ak =
        "#version 410 core"
        "\n"
        "in vec3 transformed_normal;"
        "in vec3 light_direction;"
        "in vec3 view_vector;"
        "uniform vec3 u_la;"
        "uniform vec3 u_ld;"
        "uniform vec3 u_ls;"
        "uniform vec3 u_ka;"
        "uniform vec3 u_kd;"
        "uniform vec3 u_ks;"
        "uniform float u_material_shininess;"
        "uniform int u_lkey_pressed;"
        "out vec4 FragColor;"
        "void main(void)"
        "{"
        "vec3 phong_ads_light;"
        "if(u_lkey_pressed == 1)"
        "{"
        "vec3 normalized_transformed_normal = normalize(transformed_normal);"
        "vec3 normalized_light_direction = normalize(light_direction);"
        "vec3 normalized_view_vector = normalize(view_vector);"
        "vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normal);"
        "vec3 ambient = u_la * u_ka;"
        "vec3 diffuse = u_ld * u_kd * max(dot(normalized_light_direction, normalized_transformed_normal), 0.0);"
        "vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, normalized_view_vector), 0.0), u_material_shininess);"
        "phong_ads_light = ambient + diffuse + specular;"
        "}"
        "else"
        "{"
        "phong_ads_light = vec3(1.0, 1.0, 1.0);"
        "}"
        "FragColor = vec4(phong_ads_light, 1.0);"
        "}";

    glShaderSource(perFragmentLighting_FragmentShaderObject_ak, 1, (const GLchar**)&fragmentSourceCode_ak, NULL);
    glCompileShader(perFragmentLighting_FragmentShaderObject_ak);

    infoLogLength_ak = 0;
    shaderCompileStatus_ak = 0;
    szBuffer_ak = NULL;
    glGetShaderiv(perFragmentLighting_FragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompileStatus_ak);
    if (shaderCompileStatus_ak == GL_FALSE) {
        glGetShaderiv(perFragmentLighting_FragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetShaderInfoLog(perFragmentLighting_FragmentShaderObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Fragment shader compilation log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                [self dealloc];
            }
        }
    }

    perFragmentLighting_ShaderProgramObject_ak = glCreateProgram();
    glAttachShader(perFragmentLighting_ShaderProgramObject_ak, perFragmentLighting_VertexShaderObject_ak);
    glAttachShader(perFragmentLighting_ShaderProgramObject_ak, perFragmentLighting_FragmentShaderObject_ak);

    glBindAttribLocation(perFragmentLighting_ShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(perFragmentLighting_ShaderProgramObject_ak, ATTRIBUTE_NORMAL, "vNormal");

    glLinkProgram(perFragmentLighting_ShaderProgramObject_ak);

    shaderProgramLinkStatus = 0;
    glGetProgramiv(perFragmentLighting_ShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
    if (shaderProgramLinkStatus == GL_FALSE) {
        glGetProgramiv(perFragmentLighting_ShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength_ak);
        if (infoLogLength_ak > 0) {
            szBuffer_ak = (char*)malloc(sizeof(char) * infoLogLength_ak);
            if (szBuffer_ak != NULL) {
                GLsizei written_ak;
                glGetProgramInfoLog(perFragmentLighting_ShaderProgramObject_ak, infoLogLength_ak, &written_ak, szBuffer_ak);
                fprintf(gpFile_ak, "Shader program link log: %s\n", szBuffer_ak);
                free(szBuffer_ak);
                [self dealloc];
            }
        }
    }

    perFragmentLighting_modelMatrixUniform_ak = glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_model_matrix");
    perFragmentLighting_viewMatrixUniform_ak = glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_view_matrix");
    perFragmentLighting_projectionMatrixUniform_ak = glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_projection_matrix");
    perFragmentLighting_laUniform_ak = glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_la");
    perFragmentLighting_ldUniform_ak = glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_ld");
    perFragmentLighting_lsUniform_ak = glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_ls");
    perFragmentLighting_lightPositionUniform_ak = glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_light_position");
    perFragmentLighting_kaUniform_ak = glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_ka");
    perFragmentLighting_kdUniform_ak = glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_kd");
    perFragmentLighting_ksUniform_ak = glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_ks");
    perFragmentLighting_materialShininessUniform_ak = glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_material_shininess");
    perFragmentLighting_lKeyPressedUniform_ak = glGetUniformLocation(perFragmentLighting_ShaderProgramObject_ak, "u_lkey_pressed");

    glGenVertexArrays(1, &perFragmentLighting_vao_sphere_ak);
    glBindVertexArray(perFragmentLighting_vao_sphere_ak);

    glGenBuffers(1, &perFragmentLighting_vbo_position_sphere_ak);
    glBindBuffer(GL_ARRAY_BUFFER, perFragmentLighting_vbo_position_sphere_ak);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices_ak), sphere_vertices_ak, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &perFragmentLighting_vbo_normal_sphere_ak);
    glBindBuffer(GL_ARRAY_BUFFER, perFragmentLighting_vbo_normal_sphere_ak);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals_ak), sphere_normals_ak, GL_STATIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &perFragmentLighting_vbo_element_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements_ak), sphere_elements_ak, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
    perspectiveProjectionMatrix_ak = vmath::mat4::identity();  // eqiuivalent to GLLoadIdentity

    // CV and CGL related code

    // create display link
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink_ak);

    // set callback function
    CVDisplayLinkSetOutputCallback(displayLink_ak,&MyDisplayLinkCallback,self);

    // convert NSOpenGLContext to CGL context
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];

    // convert pixel format
    CGLPixelFormatObj cglPixelFormatObj=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];

    // set converted context
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink_ak,cglContext,cglPixelFormatObj);

    // start display link
    CVDisplayLinkStart(displayLink_ak);

}

-(void) reshape
{
    [super reshape];
    // lock context as we are using multiple threads can be done using CGL
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    NSRect rect=[self bounds];

    if(rect.size.height < 0)
        rect.size.height=1;
    int width = rect.size.width;
    int height = rect.size.height;
    gWidth = width;
    gHeight = height;
    glViewport(0,0,(GLsizei)width,(GLsizei)height);
    perspectiveProjectionMatrix_ak = vmath::perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void) drawRect:(NSRect) dirtyRect
{
    [self drawView];
}

-(void)drawView
{
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Render
    switch (programToBeExecuted) {
    case 1: {
    glUseProgram(perFragmentLighting_ShaderProgramObject_ak);

    mat4 modelMatrix_ak;
    mat4 viewMatrix_ak;
    mat4 translateMatrix_ak;

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
    } else {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 0);
    }

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    GLfloat materialAmbient_ak[4];
    GLfloat materialDiffused_ak[4];
    GLfloat materialSpecular_ak[4];
    GLfloat materialShininess_ak;

    // emerald
    materialAmbient_ak[0] = 0.0215f;
    materialAmbient_ak[1] = 0.1745f;
    materialAmbient_ak[2] = 0.0215f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.07568f;
    materialDiffused_ak[1] = 0.61424f;
    materialDiffused_ak[2] = 0.07568f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.633f;
    materialSpecular_ak[1] = 0.727811f;
    materialSpecular_ak[2] = 0.633f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.6f * 128.0f;
    glViewport(gWidth * (-0.40f), gHeight * (0.35f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // jade
    materialAmbient_ak[0] = 0.135f;
    materialAmbient_ak[1] = 0.2225f;
    materialAmbient_ak[2] = 0.1575f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.54f;
    materialDiffused_ak[1] = 0.89f;
    materialDiffused_ak[2] = 0.63f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.316228f;
    materialSpecular_ak[1] = 0.316228f;
    materialSpecular_ak[2] = 0.316228f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.1 * 128.0f;
    glViewport(gWidth * (-0.40f), gHeight * (0.20f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // obsidian
    materialAmbient_ak[0] = 0.05375f;
    materialAmbient_ak[1] = 0.05f;
    materialAmbient_ak[2] = 0.06625f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.18275f;
    materialDiffused_ak[1] = 0.17f;
    materialDiffused_ak[2] = 0.22525f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.332741f;
    materialSpecular_ak[1] = 0.328634f;
    materialSpecular_ak[2] = 0.346435f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.3 * 128.0f;
    glViewport(gWidth * (-0.40f), gHeight * (0.05f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // pearl
    materialAmbient_ak[0] = 0.25f;
    materialAmbient_ak[1] = 0.20725f;
    materialAmbient_ak[2] = 0.20725f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 1.0f;
    materialDiffused_ak[1] = 0.829f;
    materialDiffused_ak[2] = 0.829f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.296648f;
    materialSpecular_ak[1] = 0.296648f;
    materialSpecular_ak[2] = 0.296648f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.088 * 128.0f;
    glViewport(gWidth * (-0.40f), gHeight * (-0.10), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // ruby
    materialAmbient_ak[0] = 0.1745f;
    materialAmbient_ak[1] = 0.01175f;
    materialAmbient_ak[2] = 0.01175f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.61424f;
    materialDiffused_ak[1] = 0.04136f;
    materialDiffused_ak[2] = 0.04136f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.727811f;
    materialSpecular_ak[1] = 0.626959f;
    materialSpecular_ak[2] = 0.626959f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.6 * 128.0f;
    glViewport(gWidth * (-0.40f), gHeight * (-0.25f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // turquoise
    materialAmbient_ak[0] = 0.1f;
    materialAmbient_ak[1] = 0.18725f;
    materialAmbient_ak[2] = 0.1745f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.396f;
    materialDiffused_ak[1] = 0.74151f;
    materialDiffused_ak[2] = 0.69102f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.297254f;
    materialSpecular_ak[1] = 0.30829f;
    materialSpecular_ak[2] = 0.306678f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.1 * 128.0f;
    glViewport(gWidth * (-0.40f), gHeight * (-0.40f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // brass
    materialAmbient_ak[0] = 0.329412f;
    materialAmbient_ak[1] = 0.223529f;
    materialAmbient_ak[2] = 0.027451f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.780392f;
    materialDiffused_ak[1] = 0.568627f;
    materialDiffused_ak[2] = 0.113725f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.992157f;
    materialSpecular_ak[1] = 0.941176f;
    materialSpecular_ak[2] = 0.807843f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.21794872 * 128.0f;
    glViewport(gWidth * (-0.145f), gHeight * (0.35f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);
    // bronze
    materialAmbient_ak[0] = 0.2125f;
    materialAmbient_ak[1] = 0.1275f;
    materialAmbient_ak[2] = 0.054f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.714f;
    materialDiffused_ak[1] = 0.4284f;
    materialDiffused_ak[2] = 0.18144f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.393548f;
    materialSpecular_ak[1] = 0.271906f;
    materialSpecular_ak[2] = 0.166721f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.2 * 128.0f;
    glViewport(gWidth * (-0.145f), gHeight * (0.20f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // chrome
    materialAmbient_ak[0] = 0.25f;
    materialAmbient_ak[1] = 0.25f;
    materialAmbient_ak[2] = 0.25f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.4f;
    materialDiffused_ak[1] = 0.4f;
    materialDiffused_ak[2] = 0.4f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.774597f;
    materialSpecular_ak[1] = 0.774597f;
    materialSpecular_ak[2] = 0.774597f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.6 * 128.0f;
    glViewport(gWidth * (-0.145f), gHeight * (0.05f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // copper
    materialAmbient_ak[0] = 0.19125f;
    materialAmbient_ak[1] = 0.0735f;
    materialAmbient_ak[2] = 0.0225f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.7038f;
    materialDiffused_ak[1] = 0.27048f;
    materialDiffused_ak[2] = 0.0828f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.256777f;
    materialSpecular_ak[1] = 0.137622f;
    materialSpecular_ak[2] = 0.086014f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.1 * 128.0f;
    glViewport(gWidth * (-0.145f), gHeight * (-0.1f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // gold
    materialAmbient_ak[0] = 0.24725f;
    materialAmbient_ak[1] = 0.1995f;
    materialAmbient_ak[2] = 0.0745f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.75164f;
    materialDiffused_ak[1] = 0.60648f;
    materialDiffused_ak[2] = 0.22648f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.628281f;
    materialSpecular_ak[1] = 0.555802f;
    materialSpecular_ak[2] = 0.366065f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.4 * 128.0f;
    glViewport(gWidth * (-0.145f), gHeight * (-0.25f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // silver
    materialAmbient_ak[0] = 0.19225f;
    materialAmbient_ak[1] = 0.19225f;
    materialAmbient_ak[2] = 0.19225f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.50754f;
    materialDiffused_ak[1] = 0.50754f;
    materialDiffused_ak[2] = 0.50754f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.508273f;
    materialSpecular_ak[1] = 0.508273f;
    materialSpecular_ak[2] = 0.508273f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.4 * 128.0f;
    glViewport(gWidth * (-0.145f), gHeight * (-0.40f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // black
    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.0f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.01f;
    materialDiffused_ak[1] = 0.01f;
    materialDiffused_ak[2] = 0.01f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.50f;
    materialSpecular_ak[1] = 0.50f;
    materialSpecular_ak[2] = 0.50f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.25 * 128.0f;
    glViewport(gWidth * (0.145f), gHeight * (0.35f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // cyan
    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.1f;
    materialAmbient_ak[2] = 0.06f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.0f;
    materialDiffused_ak[1] = 0.50980392f;
    materialDiffused_ak[2] = 0.50980392f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.50196078f;
    materialSpecular_ak[1] = 0.50196078f;
    materialSpecular_ak[2] = 0.50196078f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.25 * 128.0f;
    glViewport(gWidth * (0.145f), gHeight * (0.20f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // green
    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.0f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.1f;
    materialDiffused_ak[1] = 0.35f;
    materialDiffused_ak[2] = 0.1f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.45f;
    materialSpecular_ak[1] = 0.55f;
    materialSpecular_ak[2] = 0.45f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.25 * 128.0f;
    glViewport(gWidth * (0.145f), gHeight * (0.05f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // red
    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.0f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.5f;
    materialDiffused_ak[1] = 0.0f;
    materialDiffused_ak[2] = 0.0f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.7f;
    materialSpecular_ak[1] = 0.6f;
    materialSpecular_ak[2] = 0.6f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.25 * 128.0f;
    glViewport(gWidth * (0.145f), gHeight * (-0.10f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // white
    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.0f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.55f;
    materialDiffused_ak[1] = 0.55f;
    materialDiffused_ak[2] = 0.55f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.70f;
    materialSpecular_ak[1] = 0.70f;
    materialSpecular_ak[2] = 0.70f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.25 * 128.0f;
    glViewport(gWidth * (0.145f), gHeight * (-0.25f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // yellow plastic
    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.0f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.5f;
    materialDiffused_ak[1] = 0.5f;
    materialDiffused_ak[2] = 0.0f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.60f;
    materialSpecular_ak[1] = 0.60f;
    materialSpecular_ak[2] = 0.50f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.25 * 128.0f;
    glViewport(gWidth * (0.145f), gHeight * (-0.40f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // black
    materialAmbient_ak[0] = 0.02f;
    materialAmbient_ak[1] = 0.02f;
    materialAmbient_ak[2] = 0.02f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.01f;
    materialDiffused_ak[1] = 0.01f;
    materialDiffused_ak[2] = 0.01f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.4f;
    materialSpecular_ak[1] = 0.4f;
    materialSpecular_ak[2] = 0.4f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.078125 * 128.0f;
    glViewport(gWidth * (0.40f), gHeight * (0.35f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // cyan
    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.05f;
    materialAmbient_ak[2] = 0.05f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.4f;
    materialDiffused_ak[1] = 0.5f;
    materialDiffused_ak[2] = 0.5f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.04f;
    materialSpecular_ak[1] = 0.7f;
    materialSpecular_ak[2] = 0.7f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.078125 * 128.0f;
    glViewport(gWidth * (0.40f), gHeight * (0.20f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // green
    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.05f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.4f;
    materialDiffused_ak[1] = 0.5f;
    materialDiffused_ak[2] = 0.4f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.04f;
    materialSpecular_ak[1] = 0.7f;
    materialSpecular_ak[2] = 0.04f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.078125 * 128.0f;
    glViewport(gWidth * (0.40f), gHeight * (0.05f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // red
    materialAmbient_ak[0] = 0.05f;
    materialAmbient_ak[1] = 0.0f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.5f;
    materialDiffused_ak[1] = 0.4f;
    materialDiffused_ak[2] = 0.4f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.7f;
    materialSpecular_ak[1] = 0.04f;
    materialSpecular_ak[2] = 0.04f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.078125 * 128.0f;
    glViewport(gWidth * (0.40f), gHeight * (-0.10f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // white
    materialAmbient_ak[0] = 0.05f;
    materialAmbient_ak[1] = 0.05f;
    materialAmbient_ak[2] = 0.05f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.5f;
    materialDiffused_ak[1] = 0.5f;
    materialDiffused_ak[2] = 0.5f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.7f;
    materialSpecular_ak[1] = 0.7f;
    materialSpecular_ak[2] = 0.7f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.078125 * 128.0f;
    glViewport(gWidth * (0.40f), gHeight * (-0.25f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // yellow rubber
    materialAmbient_ak[0] = 0.05f;
    materialAmbient_ak[1] = 0.05f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.5f;
    materialDiffused_ak[1] = 0.5f;
    materialDiffused_ak[2] = 0.4f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.7f;
    materialSpecular_ak[1] = 0.7f;
    materialSpecular_ak[2] = 0.04f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.078125 * 128.0f;
    glViewport(gWidth * (0.40f), gHeight * (-0.40f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perFragmentLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perFragmentLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perFragmentLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perFragmentLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perFragmentLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perFragmentLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perFragmentLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perFragmentLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perFragmentLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perFragmentLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perFragmentLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perFragmentLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perFragmentLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);
    glUseProgram(0);
    break;
    }
    case 0: {

    
    glUseProgram(perVertexLighting_ShaderProgramObject_ak);

    mat4 modelMatrix_ak;
    mat4 viewMatrix_ak;
    mat4 translateMatrix_ak;

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
    } else {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 0);
    }

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    GLfloat materialAmbient_ak[4];
    GLfloat materialDiffused_ak[4];
    GLfloat materialSpecular_ak[4];
    GLfloat materialShininess_ak;

    // emerald
    materialAmbient_ak[0] = 0.0215f;
    materialAmbient_ak[1] = 0.1745f;
    materialAmbient_ak[2] = 0.0215f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.07568f;
    materialDiffused_ak[1] = 0.61424f;
    materialDiffused_ak[2] = 0.07568f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.633f;
    materialSpecular_ak[1] = 0.727811f;
    materialSpecular_ak[2] = 0.633f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.6f * 128.0f;
    glViewport(gWidth * (-0.40f), gHeight * (0.35f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // jade
    materialAmbient_ak[0] = 0.135f;
    materialAmbient_ak[1] = 0.2225f;
    materialAmbient_ak[2] = 0.1575f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.54f;
    materialDiffused_ak[1] = 0.89f;
    materialDiffused_ak[2] = 0.63f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.316228f;
    materialSpecular_ak[1] = 0.316228f;
    materialSpecular_ak[2] = 0.316228f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.1 * 128.0f;
    glViewport(gWidth * (-0.40f), gHeight * (0.20f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // obsidian
    materialAmbient_ak[0] = 0.05375f;
    materialAmbient_ak[1] = 0.05f;
    materialAmbient_ak[2] = 0.06625f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.18275f;
    materialDiffused_ak[1] = 0.17f;
    materialDiffused_ak[2] = 0.22525f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.332741f;
    materialSpecular_ak[1] = 0.328634f;
    materialSpecular_ak[2] = 0.346435f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.3 * 128.0f;
    glViewport(gWidth * (-0.40f), gHeight * (0.05f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // pearl
    materialAmbient_ak[0] = 0.25f;
    materialAmbient_ak[1] = 0.20725f;
    materialAmbient_ak[2] = 0.20725f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 1.0f;
    materialDiffused_ak[1] = 0.829f;
    materialDiffused_ak[2] = 0.829f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.296648f;
    materialSpecular_ak[1] = 0.296648f;
    materialSpecular_ak[2] = 0.296648f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.088 * 128.0f;
    glViewport(gWidth * (-0.40f), gHeight * (-0.10), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // ruby
    materialAmbient_ak[0] = 0.1745f;
    materialAmbient_ak[1] = 0.01175f;
    materialAmbient_ak[2] = 0.01175f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.61424f;
    materialDiffused_ak[1] = 0.04136f;
    materialDiffused_ak[2] = 0.04136f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.727811f;
    materialSpecular_ak[1] = 0.626959f;
    materialSpecular_ak[2] = 0.626959f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.6 * 128.0f;
    glViewport(gWidth * (-0.40f), gHeight * (-0.25f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // turquoise
    materialAmbient_ak[0] = 0.1f;
    materialAmbient_ak[1] = 0.18725f;
    materialAmbient_ak[2] = 0.1745f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.396f;
    materialDiffused_ak[1] = 0.74151f;
    materialDiffused_ak[2] = 0.69102f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.297254f;
    materialSpecular_ak[1] = 0.30829f;
    materialSpecular_ak[2] = 0.306678f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.1 * 128.0f;
    glViewport(gWidth * (-0.40f), gHeight * (-0.40f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // brass
    materialAmbient_ak[0] = 0.329412f;
    materialAmbient_ak[1] = 0.223529f;
    materialAmbient_ak[2] = 0.027451f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.780392f;
    materialDiffused_ak[1] = 0.568627f;
    materialDiffused_ak[2] = 0.113725f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.992157f;
    materialSpecular_ak[1] = 0.941176f;
    materialSpecular_ak[2] = 0.807843f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.21794872 * 128.0f;
    glViewport(gWidth * (-0.145f), gHeight * (0.35f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);
    // bronze
    materialAmbient_ak[0] = 0.2125f;
    materialAmbient_ak[1] = 0.1275f;
    materialAmbient_ak[2] = 0.054f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.714f;
    materialDiffused_ak[1] = 0.4284f;
    materialDiffused_ak[2] = 0.18144f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.393548f;
    materialSpecular_ak[1] = 0.271906f;
    materialSpecular_ak[2] = 0.166721f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.2 * 128.0f;
    glViewport(gWidth * (-0.145f), gHeight * (0.20f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // chrome
    materialAmbient_ak[0] = 0.25f;
    materialAmbient_ak[1] = 0.25f;
    materialAmbient_ak[2] = 0.25f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.4f;
    materialDiffused_ak[1] = 0.4f;
    materialDiffused_ak[2] = 0.4f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.774597f;
    materialSpecular_ak[1] = 0.774597f;
    materialSpecular_ak[2] = 0.774597f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.6 * 128.0f;
    glViewport(gWidth * (-0.145f), gHeight * (0.05f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // copper
    materialAmbient_ak[0] = 0.19125f;
    materialAmbient_ak[1] = 0.0735f;
    materialAmbient_ak[2] = 0.0225f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.7038f;
    materialDiffused_ak[1] = 0.27048f;
    materialDiffused_ak[2] = 0.0828f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.256777f;
    materialSpecular_ak[1] = 0.137622f;
    materialSpecular_ak[2] = 0.086014f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.1 * 128.0f;
    glViewport(gWidth * (-0.145f), gHeight * (-0.1f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // gold
    materialAmbient_ak[0] = 0.24725f;
    materialAmbient_ak[1] = 0.1995f;
    materialAmbient_ak[2] = 0.0745f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.75164f;
    materialDiffused_ak[1] = 0.60648f;
    materialDiffused_ak[2] = 0.22648f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.628281f;
    materialSpecular_ak[1] = 0.555802f;
    materialSpecular_ak[2] = 0.366065f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.4 * 128.0f;
    glViewport(gWidth * (-0.145f), gHeight * (-0.25f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // silver
    materialAmbient_ak[0] = 0.19225f;
    materialAmbient_ak[1] = 0.19225f;
    materialAmbient_ak[2] = 0.19225f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.50754f;
    materialDiffused_ak[1] = 0.50754f;
    materialDiffused_ak[2] = 0.50754f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.508273f;
    materialSpecular_ak[1] = 0.508273f;
    materialSpecular_ak[2] = 0.508273f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.4 * 128.0f;
    glViewport(gWidth * (-0.145f), gHeight * (-0.40f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // black
    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.0f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.01f;
    materialDiffused_ak[1] = 0.01f;
    materialDiffused_ak[2] = 0.01f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.50f;
    materialSpecular_ak[1] = 0.50f;
    materialSpecular_ak[2] = 0.50f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.25 * 128.0f;
    glViewport(gWidth * (0.145f), gHeight * (0.35f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // cyan
    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.1f;
    materialAmbient_ak[2] = 0.06f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.0f;
    materialDiffused_ak[1] = 0.50980392f;
    materialDiffused_ak[2] = 0.50980392f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.50196078f;
    materialSpecular_ak[1] = 0.50196078f;
    materialSpecular_ak[2] = 0.50196078f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.25 * 128.0f;
    glViewport(gWidth * (0.145f), gHeight * (0.20f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // green
    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.0f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.1f;
    materialDiffused_ak[1] = 0.35f;
    materialDiffused_ak[2] = 0.1f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.45f;
    materialSpecular_ak[1] = 0.55f;
    materialSpecular_ak[2] = 0.45f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.25 * 128.0f;
    glViewport(gWidth * (0.145f), gHeight * (0.05f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // red
    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.0f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.5f;
    materialDiffused_ak[1] = 0.0f;
    materialDiffused_ak[2] = 0.0f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.7f;
    materialSpecular_ak[1] = 0.6f;
    materialSpecular_ak[2] = 0.6f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.25 * 128.0f;
    glViewport(gWidth * (0.145f), gHeight * (-0.10f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // white
    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.0f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.55f;
    materialDiffused_ak[1] = 0.55f;
    materialDiffused_ak[2] = 0.55f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.70f;
    materialSpecular_ak[1] = 0.70f;
    materialSpecular_ak[2] = 0.70f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.25 * 128.0f;
    glViewport(gWidth * (0.145f), gHeight * (-0.25f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // yellow plastic
    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.0f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.5f;
    materialDiffused_ak[1] = 0.5f;
    materialDiffused_ak[2] = 0.0f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.60f;
    materialSpecular_ak[1] = 0.60f;
    materialSpecular_ak[2] = 0.50f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.25 * 128.0f;
    glViewport(gWidth * (0.145f), gHeight * (-0.40f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // black
    materialAmbient_ak[0] = 0.02f;
    materialAmbient_ak[1] = 0.02f;
    materialAmbient_ak[2] = 0.02f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.01f;
    materialDiffused_ak[1] = 0.01f;
    materialDiffused_ak[2] = 0.01f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.4f;
    materialSpecular_ak[1] = 0.4f;
    materialSpecular_ak[2] = 0.4f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.078125 * 128.0f;
    glViewport(gWidth * (0.40f), gHeight * (0.35f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // cyan
    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.05f;
    materialAmbient_ak[2] = 0.05f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.4f;
    materialDiffused_ak[1] = 0.5f;
    materialDiffused_ak[2] = 0.5f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.04f;
    materialSpecular_ak[1] = 0.7f;
    materialSpecular_ak[2] = 0.7f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.078125 * 128.0f;
    glViewport(gWidth * (0.40f), gHeight * (0.20f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // green
    materialAmbient_ak[0] = 0.0f;
    materialAmbient_ak[1] = 0.05f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.4f;
    materialDiffused_ak[1] = 0.5f;
    materialDiffused_ak[2] = 0.4f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.04f;
    materialSpecular_ak[1] = 0.7f;
    materialSpecular_ak[2] = 0.04f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.078125 * 128.0f;
    glViewport(gWidth * (0.40f), gHeight * (0.05f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // red
    materialAmbient_ak[0] = 0.05f;
    materialAmbient_ak[1] = 0.0f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.5f;
    materialDiffused_ak[1] = 0.4f;
    materialDiffused_ak[2] = 0.4f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.7f;
    materialSpecular_ak[1] = 0.04f;
    materialSpecular_ak[2] = 0.04f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.078125 * 128.0f;
    glViewport(gWidth * (0.40f), gHeight * (-0.10f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // white
    materialAmbient_ak[0] = 0.05f;
    materialAmbient_ak[1] = 0.05f;
    materialAmbient_ak[2] = 0.05f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.5f;
    materialDiffused_ak[1] = 0.5f;
    materialDiffused_ak[2] = 0.5f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.7f;
    materialSpecular_ak[1] = 0.7f;
    materialSpecular_ak[2] = 0.7f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.078125 * 128.0f;
    glViewport(gWidth * (0.40f), gHeight * (-0.25f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);

    // yellow rubber
    materialAmbient_ak[0] = 0.05f;
    materialAmbient_ak[1] = 0.05f;
    materialAmbient_ak[2] = 0.0f;
    materialAmbient_ak[3] = 1.0f;

    materialDiffused_ak[0] = 0.5f;
    materialDiffused_ak[1] = 0.5f;
    materialDiffused_ak[2] = 0.4f;
    materialDiffused_ak[3] = 1.0f;

    materialSpecular_ak[0] = 0.7f;
    materialSpecular_ak[1] = 0.7f;
    materialSpecular_ak[2] = 0.04f;
    materialSpecular_ak[3] = 1.0f;

    materialShininess_ak = 0.078125 * 128.0f;
    glViewport(gWidth * (0.40f), gHeight * (-0.40f), gWidth, gHeight);

    if (bLight_ak == true) {
        glUniform1i(perVertexLighting_lKeyPressedUniform_ak, 1);
        glUniform3fv(perVertexLighting_laUniform_ak, 1.0, lightAmbient_ak);
        glUniform3fv(perVertexLighting_ldUniform_ak, 1.0, lightDiffused_ak);
        glUniform3fv(perVertexLighting_lsUniform_ak, 1.0, lightSpecular_ak);
        glUniform4fv(perVertexLighting_lightPositionUniform_ak, 1.0, lightPosition_ak);
        glUniform3fv(perVertexLighting_kaUniform_ak, 1.0, materialAmbient_ak);
        glUniform3fv(perVertexLighting_kdUniform_ak, 1.0, materialDiffused_ak);
        glUniform3fv(perVertexLighting_ksUniform_ak, 1.0, materialSpecular_ak);
        glUniform1f(perVertexLighting_materialShininessUniform_ak, materialShininess_ak);
    }

    viewMatrix_ak = mat4::identity();
    modelMatrix_ak = mat4::identity();
    translateMatrix_ak = mat4::identity();

    translateMatrix_ak = vmath::translate(0.0f, 0.0f, -12.0f);

    modelMatrix_ak = translateMatrix_ak;

    glUniformMatrix4fv(perVertexLighting_modelMatrixUniform_ak, 1, GL_FALSE, modelMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_viewMatrixUniform_ak, 1, GL_FALSE, viewMatrix_ak);
    glUniformMatrix4fv(perVertexLighting_projectionMatrixUniform_ak, 1, GL_FALSE, perspectiveProjectionMatrix_ak);

    glBindVertexArray(perVertexLighting_vao_sphere_ak);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, perVertexLighting_vbo_element_sphere_ak);
    glDrawElements(GL_TRIANGLES, gNumElements_ak, GL_UNSIGNED_SHORT, 0);

    glBindVertexArray(0);
    glUseProgram(0);
    break;
    }
    }

    Update();
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}
void Update(void) {
    angleForXRotation_ak = angleForXRotation_ak + 0.01f;
    if (angleForXRotation_ak > 2 * M_PI)
        angleForXRotation_ak = 0.0f;

    if (keyPressed_ak == 1) {
        lightPosition_ak[1] = 100 * sin(angleForXRotation_ak);
        lightPosition_ak[2] = 100 * cos(angleForXRotation_ak);
    }

    angleForYRotation_ak = angleForYRotation_ak + 0.01f;
    if (angleForYRotation_ak > 2 * M_PI)
        angleForYRotation_ak = 0.0f;
    if (keyPressed_ak == 2) {
        lightPosition_ak[0] = 100 * sin(angleForYRotation_ak);
        lightPosition_ak[2] = 100 * cos(angleForYRotation_ak);
    }

    angleForZRotation_ak = angleForZRotation_ak + 0.01f;
    if (angleForZRotation_ak > 2 * M_PI)
        angleForZRotation_ak = 0.0f;
    if (keyPressed_ak == 3) {
        lightPosition_ak[0] = 100 * sin(angleForZRotation_ak);
        lightPosition_ak[1] = 100 * cos(angleForZRotation_ak);
    }
}
-(BOOL) acceptsFirstResponder{
    [[self window]makeFirstResponder:self];
    return YES;
}
// WM_KEYDOWN
-(void) keyDown:(NSEvent *)theEvent{
    int key = (int)[[theEvent characters]characterAtIndex:0];
    switch(key){
        //Esc
        case 27:
            [[self window]toggleFullScreen:self];
            break;
        // F
        case 70:
        case 102:
            programToBeExecuted = 1;
            break;
        // Q
        case 81:
        case 113:
            [self release];
            [NSApp terminate: self];
            break;
        // L
        case 76:
        case 108: {
            if (bLight_ak) {
                bLight_ak = false;
            } else {
                bLight_ak = true;
            }
            break;
        }
        // A
        case 65:
        case 97: {
            if (bAnimate_ak) {
                bAnimate_ak = false;
            } else {
                bAnimate_ak = true;
            }
            break;
        }
        // V
        case 86:
        case 118:
            programToBeExecuted = 0;
            break;
        case 88:
        case 120:
            keyPressed_ak = 1;
            angleForXRotation_ak = 0.0f;
            angleForYRotation_ak = 0.0f;
            angleForZRotation_ak = 0.0f;
            lightPosition_ak[0] = 0.0f;
            lightPosition_ak[1] = 0.0f;
            lightPosition_ak[2] = 0.0f;
            break;
        case 89:
        case 121:
            keyPressed_ak = 2;
            angleForXRotation_ak = 0.0f;
            angleForYRotation_ak = 0.0f;
            angleForZRotation_ak = 0.0f;
            lightPosition_ak[0] = 0.0f;
            lightPosition_ak[1] = 0.0f;
            lightPosition_ak[2] = 0.0f;
            break;
        case 90:
        case 122:
            keyPressed_ak = 3;
            angleForXRotation_ak = 0.0f;
            angleForYRotation_ak = 0.0f;
            angleForZRotation_ak = 0.0f;
            lightPosition_ak[0] = 0.0f;
            lightPosition_ak[1] = 0.0f;
            lightPosition_ak[2] = 0.0f;
            break;
        case 83:
        case 115:
            keyPressed_ak = 0;
            angleForXRotation_ak = 0.0f;
            angleForYRotation_ak = 0.0f;
            angleForZRotation_ak = 0.0f;
            lightPosition_ak[0] = 0.0f;
            lightPosition_ak[1] = 0.0f;
            lightPosition_ak[2] = 0.0f;
            break;
    }
}
// WM_LBUTTONDOWN
-(void) mouseDown:(NSEvent *)theEvent{
    
}

// WM_RBUTTONDOWN
-(void) rightMouseDown:(NSEvent *)theEvent{
    
}

// WM_MBUTTONDOWN
-(void) otherMouseDown:(NSEvent *)theEvent{
    
}

-(void) dealloc{
    CVDisplayLinkStop(displayLink_ak);
    CVDisplayLinkRelease(displayLink_ak);
    // Uninitialize
    if (perVertexLighting_vao_sphere_ak) {
        glDeleteVertexArrays(1, &perVertexLighting_vao_sphere_ak);
        perVertexLighting_vao_sphere_ak = 0;
    }

    if (perVertexLighting_vbo_position_sphere_ak) {
        glDeleteVertexArrays(1, &perVertexLighting_vbo_position_sphere_ak);
        perVertexLighting_vbo_position_sphere_ak = 0;
    }

    if (perVertexLighting_vbo_normal_sphere_ak) {
        glDeleteVertexArrays(1, &perVertexLighting_vbo_normal_sphere_ak);
        perVertexLighting_vbo_normal_sphere_ak = 0;
    }

    if (perVertexLighting_ShaderProgramObject_ak) {
        glUseProgram(perVertexLighting_ShaderProgramObject_ak);
        GLsizei shaderCount_ak;
        glGetProgramiv(perVertexLighting_ShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

        GLuint* pShaders_ak = NULL;
        pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
        glGetAttachedShaders(perVertexLighting_ShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);

        for (int i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
            glDetachShader(perVertexLighting_ShaderProgramObject_ak, pShaders_ak[i_ak]);
            glDeleteShader(pShaders_ak[i_ak]);
            pShaders_ak[i_ak] = 0;
        }

        free(pShaders_ak);
        glDeleteProgram(perVertexLighting_ShaderProgramObject_ak);
        perVertexLighting_ShaderProgramObject_ak = 0;

        glUseProgram(0);
    }
    if (perFragmentLighting_vao_sphere_ak) {
        glDeleteVertexArrays(1, &perFragmentLighting_vao_sphere_ak);
        perFragmentLighting_vao_sphere_ak = 0;
    }

    if (perFragmentLighting_vbo_position_sphere_ak) {
        glDeleteVertexArrays(1, &perFragmentLighting_vbo_position_sphere_ak);
        perFragmentLighting_vbo_position_sphere_ak = 0;
    }

    if (perFragmentLighting_vbo_normal_sphere_ak) {
        glDeleteVertexArrays(1, &perFragmentLighting_vbo_normal_sphere_ak);
        perFragmentLighting_vbo_normal_sphere_ak = 0;
    }

    if (perFragmentLighting_ShaderProgramObject_ak) {
        glUseProgram(perFragmentLighting_ShaderProgramObject_ak);
        GLsizei shaderCount_ak;
        glGetProgramiv(perFragmentLighting_ShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

        GLuint* pShaders_ak = NULL;
        pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
        glGetAttachedShaders(perFragmentLighting_ShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);

        for (int i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
            glDetachShader(perFragmentLighting_ShaderProgramObject_ak, pShaders_ak[i_ak]);
            glDeleteShader(pShaders_ak[i_ak]);
            pShaders_ak[i_ak] = 0;
        }

        free(pShaders_ak);
        glDeleteProgram(perFragmentLighting_ShaderProgramObject_ak);
        perFragmentLighting_ShaderProgramObject_ak = 0;

        glUseProgram(0);
    }

    if (gpFile_ak) {
        fclose(gpFile_ak);
        gpFile_ak = NULL;
    }
    [super dealloc];
}

@end

// Global C space functions
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp *outputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void *displayLinkContext)
{
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];
    return(result);
}
