#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h> // similar to windows.h
#import <QuartzCore/CVDisplayLink.h>
#import <OpenGL/gl3.h> // GL.h
#include "vmath.h"

using namespace vmath;

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

FILE *gpFile_ak=NULL;

// forward declaration
@interface AppDelegate: NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

int main(int argc,const char* argv[])
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSApp = [NSApplication sharedApplication];
    [NSApp setDelegate:[[AppDelegate alloc] init]];
    [NSApp run];
    [pool release];
    return(0);
}

@interface MyOpenGLView : NSOpenGLView
@end

@implementation AppDelegate
{
    @private
    NSWindow *window;
    MyOpenGLView *myOpenGLView_ak;
}

// WM_CREATE
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSBundle *appBundle_ak = [NSBundle mainBundle];
    NSString *appDirPath_ak =[appBundle_ak bundlePath];
    NSString *parentDirPath_ak = [appDirPath_ak stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath_ak = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath_ak];

    const char *pszLogFileNameWithPath=[logFileNameWithPath_ak cStringUsingEncoding:NSASCIIStringEncoding];

    gpFile_ak=fopen(pszLogFileNameWithPath,"w");
    if(gpFile_ak==NULL){
        [self release];
        [NSApp terminate:self];
    }

    fprintf(gpFile_ak,"Program Started Successfully\n");

    NSRect win_rect_ak; // internally CGRect structure
    win_rect_ak = NSMakeRect(0.0, 0.0, 800.0, 600.0); // NSPoint(x, y) NSSize(width, height) from C-Library

    window=[[NSWindow alloc]
        initWithContentRect    :    win_rect_ak
                styleMask    :    NSWindowStyleMaskTitled |
                                NSWindowStyleMaskClosable |
                                NSWindowStyleMaskMiniaturizable |
                                NSWindowStyleMaskResizable
                backing        :     NSBackingStoreBuffered
                defer        :    NO    ];
    [window setTitle:@"ASK: macOS Checkerboard"];
    [window center];

    myOpenGLView_ak = [[MyOpenGLView alloc]initWithFrame:win_rect_ak];
    [window setContentView:myOpenGLView_ak];

    [window setDelegate:self];
    [window makeKeyAndOrderFront:self]; // setFocus, setForeGroundWindow
}

// uninitialize
-(void) applicationWillTerminate:(NSNotification *)aNotification
{
    if(gpFile_ak) {
        fprintf(gpFile_ak,"Program terminated successfully\n");
        fclose(gpFile_ak);
        gpFile_ak=NULL;
    }
}

// NSWindowDelegate's method
-(void) windowWillClose:(NSNotification *)aNotification
{
    [NSApp terminate:self];
}

-(void)dealloc
{
    [myOpenGLView_ak release];
    [window release];
    [super dealloc]; // bubbling de-allocation
}

@end

@implementation MyOpenGLView
{
    @private
    CVDisplayLinkRef displayLink_ak;
    // global variables here
    enum {
        ATTRIBUTE_POSITION = 0,
        ATTRIBUTE_COLOR,
        ATTRIBUTE_NORMAL,
        ATTRIBUTE_TEXCOORD
    };
    GLuint gVertexShaderObject_ak;
    GLuint gFragmentShaderObject_ak;
    GLuint gShaderProgramObject_ak;

    GLuint mvpMatrixUniform;

    GLuint Vao_cube;
    GLuint Vbo_position_cube;
    GLuint Vbo_color_cube;

    GLuint Vbo_texture_cube;

    GLuint textureSamplerUniform;
    GLuint textureImage_ak;

    #define checkImageWidth_ak 64
    #define checkImageHeight_ak 64

    GLubyte checkImage_ak[checkImageWidth_ak][checkImageHeight_ak][4];

    vmath::mat4 perspectiveProjectionMatrix_ak;
}

// id - returns object of any class
-(id) initWithFrame:(NSRect) frame
{
    self = [super initWithFrame: frame];
    if(self)
    {
        // pfd of windows
        NSOpenGLPixelFormatAttribute attributes_ak[] = {
            NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFADepthSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADoubleBuffer,
            0
        };

        NSOpenGLPixelFormat *pixelFormat= [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes_ak]autorelease];
        if(pixelFormat==nil)
        {
            fprintf(gpFile_ak,"OpenGL pixel format error.\n");
            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *openGLContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        [self setPixelFormat:pixelFormat];
        [self setOpenGLContext:openGLContext];
    }
    return(self);
}

-(CVReturn) getFrameForTime:(const CVTimeStamp *)outputTime {
    // Multithreaded
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

-(void) prepareOpenGL
{
    [super prepareOpenGL];
    [[self openGLContext]makeCurrentContext];

    fprintf(gpFile_ak,"OpenGL version : %s\n",glGetString(GL_VERSION));
    fprintf(gpFile_ak,"GLSL version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

    // swap interval to avoid tearing
    GLint swapInt_ak=1;

    [[self openGLContext]setValues:&swapInt_ak forParameter:NSOpenGLCPSwapInterval];

    //shader block here
    gVertexShaderObject_ak = glCreateShader(GL_VERTEX_SHADER);
    const GLchar *vertexShaderSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec4 vColor;" \
    "out vec4 out_color;" \
    "in vec2 vTexCoord;" \
    "out vec2 out_texCoord;" \
    "uniform mat4 u_mvpMatrix;" \
    "void main(void)" \
    "{" \
    "gl_Position = u_mvpMatrix * vPosition;" \
    "out_color = vColor;" \
    "out_texCoord = vTexCoord;" \
    "}";
    
    glShaderSource(gVertexShaderObject_ak,1,(const GLchar **)&vertexShaderSourceCode, NULL);

    // compile Vertex Shader
    glCompileShader(gVertexShaderObject_ak);

    // Error checking for Vertex Shader
    GLint infoLogLength = 0;
    GLint shaderCompiledStatus = 0;
    char *szBuffer = NULL;

    glGetShaderiv(gVertexShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
    if(shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
        if(infoLogLength>0)
        {
            szBuffer = (char *)malloc(infoLogLength);
            if(szBuffer!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject_ak, infoLogLength, &written, szBuffer);
                fprintf(gpFile_ak, "Vertex Shader Compilation Log: %s\n",szBuffer);
                free(szBuffer);
                szBuffer = NULL;
            }
        }
    }

    //Fragment Shader
    /* out_color is the output of Vertex Shader */
    gFragmentShaderObject_ak = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar *vertexFragmentSourceCode =
    "#version 410" \
    "\n" \
    "in vec4 out_color;" \
    "out vec4 FragColor;" \
    "in vec2 out_texCoord;" \
    "uniform sampler2D u_texture_sampler;" \
    "void main(void)" \
    "{" \
    "FragColor = texture(u_texture_sampler, out_texCoord);" \
    "}";

    glShaderSource(gFragmentShaderObject_ak,1,(const GLchar **)&vertexFragmentSourceCode, NULL);

    // compile Fragment Shader
    glCompileShader(gFragmentShaderObject_ak);

    // Error Checking for Fragment Shader
    glGetShaderiv(gFragmentShaderObject_ak, GL_COMPILE_STATUS, &shaderCompiledStatus);
    if(shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
        if(infoLogLength>0)
        {
            szBuffer = (char *)malloc(infoLogLength);
            if(szBuffer!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject_ak, infoLogLength, &written, szBuffer);
                fprintf(gpFile_ak, "Fragment Shader Compilation Log: %s\n",szBuffer);
                free(szBuffer);
                szBuffer = NULL;
            }
        }
    }

    //Shader Program
    gShaderProgramObject_ak = glCreateProgram();
    glAttachShader(gShaderProgramObject_ak,gVertexShaderObject_ak);
    glAttachShader(gShaderProgramObject_ak,gFragmentShaderObject_ak);

    // Bind the attributes in shader with the enums in your main program
    /* Bind vPosition from shader to the position in your program. i.e 'in' statement as it is written first in shader*/
    glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_POSITION, "vPosition");

    // For Texture Attribute
    glBindAttribLocation(gShaderProgramObject_ak, ATTRIBUTE_TEXCOORD, "vTexCoord");

    glLinkProgram(gShaderProgramObject_ak);

    // Linking Error Checking
    GLint shaderProgramLinkStatus = 0;
    szBuffer = NULL;

    glGetProgramiv(gShaderProgramObject_ak, GL_LINK_STATUS, &shaderProgramLinkStatus);
    if(shaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject_ak, GL_INFO_LOG_LENGTH, &infoLogLength);
        if(infoLogLength>0)
        {
            szBuffer = (char *)malloc(infoLogLength);
            if(szBuffer!=NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject_ak, infoLogLength, &written, szBuffer);
                fprintf(gpFile_ak, "Shader Program Link Log: %s\n",szBuffer);
                free(szBuffer);
                szBuffer = NULL;
            }
        }
    }

    //Get the information of uniform Post linking
    mvpMatrixUniform = glGetUniformLocation(gShaderProgramObject_ak, "u_mvpMatrix");
    textureSamplerUniform = glGetUniformLocation(gShaderProgramObject_ak, "u_texture_sampler");

    [self loadTexture];

    glGenVertexArrays(1, &Vao_cube);
    glBindVertexArray(Vao_cube);

    //Position
    glGenBuffers(1, &Vbo_position_cube);
    glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_cube);
    // 3 is specified for 3 pairs for vertices
    /* For Texture, specify 2*/
    // 4th parameter----> Normalized Co-ordinates
    // 5th How many strides to take?
    // 6th From which position
    glBufferData(GL_ARRAY_BUFFER, 4*3*sizeof(float), NULL, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Texture
    glGenBuffers(1, &Vbo_texture_cube);
    glBindBuffer(GL_ARRAY_BUFFER, Vbo_texture_cube);
    glBufferData(GL_ARRAY_BUFFER, 8*sizeof(float), NULL, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(ATTRIBUTE_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(ATTRIBUTE_TEXCOORD);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glEnable(GL_TEXTURE_2D);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    perspectiveProjectionMatrix_ak = vmath::mat4::identity();  // eqiuivalent to GLLoadIdentity

    // CV and CGL related code

    // create display link
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink_ak);

    // set callback function
    CVDisplayLinkSetOutputCallback(displayLink_ak,&MyDisplayLinkCallback,self);

    // convert NSOpenGLContext to CGL context
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];

    // convert pixel format
    CGLPixelFormatObj cglPixelFormatObj=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];

    // set converted context
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink_ak,cglContext,cglPixelFormatObj);

    // start display link
    CVDisplayLinkStart(displayLink_ak);

}

-(void) loadTexture
{
    
    int i, j, c;
    for (i = 0; i < checkImageHeight_ak; i++) {
        for (j = 0; j < checkImageWidth_ak; j++) {
            c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;
            checkImage_ak[i][j][0] = (GLubyte)c;
            checkImage_ak[i][j][1] = (GLubyte)c;
            checkImage_ak[i][j][2] = (GLubyte)c;
            checkImage_ak[i][j][3] = 255;

        }
    }
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glGenTextures(1, &textureImage_ak);
    glBindTexture(GL_TEXTURE_2D, textureImage_ak);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,checkImageWidth_ak,checkImageHeight_ak,0,GL_RGBA,GL_UNSIGNED_BYTE,checkImage_ak);
}

-(void) reshape
{
    [super reshape];
    // lock context as we are using multiple threads can be done using CGL
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    NSRect rect=[self bounds];

    if(rect.size.height < 0)
        rect.size.height=1;
    int width = rect.size.width;
    int height = rect.size.height;
    glViewport(0,0,(GLsizei)width,(GLsizei)height);
    perspectiveProjectionMatrix_ak = vmath::perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void) drawRect:(NSRect) dirtyRect
{
    [self drawView];
}

-(void)drawView
{
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Render
    glUseProgram(gShaderProgramObject_ak);
        //Cube
        //Vertices
        GLfloat cubeVertices[] =
        {
            -2.0f,-1.0f, 0.0f ,
            -2.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, -1.0f, 0.0f
        };

        //TexCoord
        GLfloat cubeTexCoord[] =
        {
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 1.0f,
            1.0f, 0.0f
        };
        
        //OpenGL Draw
        // ModelViewMatrix and Load Identity
        mat4 modelViewMatrix = mat4::identity();
        mat4 modelViewProjectionMatrix = mat4::identity();

        mat4 translateMatrix = translate(0.0f, 0.0f, -6.0f);
        modelViewMatrix = modelViewMatrix * translateMatrix;

        modelViewProjectionMatrix = perspectiveProjectionMatrix_ak * modelViewMatrix;
        
        glUniformMatrix4fv(mvpMatrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);

        //Texture Cube
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureImage_ak);
        glUniform1i(textureSamplerUniform, 0);

        //Cube Begin
        glBindVertexArray(Vao_cube);

        glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_cube);
        glBufferData(GL_ARRAY_BUFFER, 4 * 3 * sizeof(float), cubeVertices,GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ARRAY_BUFFER, Vbo_texture_cube);
        glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(float), cubeTexCoord,GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

        cubeVertices[0] = 1.0f;
        cubeVertices[1] = -1.0f;
        cubeVertices[2] = 0.0f;
        cubeVertices[3] = 1.0f;
        cubeVertices[4] = 1.0f;
        cubeVertices[5] = 0.0f;
        cubeVertices[6] = 2.41421f;
        cubeVertices[7] = 1.0f;
        cubeVertices[8] = -1.41421f;
        cubeVertices[9] = 2.41421f;
        cubeVertices[10] = -1.0f;
        cubeVertices[11] = -1.41421f;

        glBindBuffer(GL_ARRAY_BUFFER, Vbo_position_cube);
        glBufferData(GL_ARRAY_BUFFER,4 * 3 * sizeof(float), cubeVertices,GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glDrawArrays(GL_TRIANGLE_FAN,0,4);

        glBindTexture(GL_TEXTURE_2D, 0);
        
        glBindVertexArray(0);

        glUseProgram(0);

    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL) acceptsFirstResponder{
    [[self window]makeFirstResponder:self];
    return YES;
}
// WM_KEYDOWN
-(void) keyDown:(NSEvent *)theEvent{
    int key = (int)[[theEvent characters]characterAtIndex:0];
    switch(key){
        //Esc
        case 27:
            [self release];
            [NSApp terminate: self];
            break;
        
        case 70:
        case 102:
            [[self window]toggleFullScreen:self];
            break;
    }
}
// WM_LBUTTONDOWN
-(void) mouseDown:(NSEvent *)theEvent{
    
}

// WM_RBUTTONDOWN
-(void) rightMouseDown:(NSEvent *)theEvent{
    
}

// WM_MBUTTONDOWN
-(void) otherMouseDown:(NSEvent *)theEvent{
    
}

-(void) dealloc{
    CVDisplayLinkStop(displayLink_ak);
    CVDisplayLinkRelease(displayLink_ak);
    // Uninitialize
    if(Vao_cube)
    {
        glDeleteVertexArrays(1, &Vao_cube);
        Vao_cube = 0;
    }

    if(Vbo_position_cube)
    {
        glDeleteVertexArrays(1, &Vbo_position_cube);
        Vbo_position_cube = 0;
    }

    if(Vbo_color_cube)
    {
        glDeleteVertexArrays(1, &Vbo_color_cube);
        Vbo_color_cube = 0;
    }

    if(textureImage_ak)
    {
        glDeleteTextures(1, &textureImage_ak);
        textureImage_ak = 0;
    }

    if (gShaderProgramObject_ak) {
        glUseProgram(gShaderProgramObject_ak);
        GLsizei shaderCount_ak;
        glGetProgramiv(gShaderProgramObject_ak, GL_ATTACHED_SHADERS, &shaderCount_ak);

        GLuint* pShaders_ak = NULL;
        pShaders_ak = (GLuint*)malloc(sizeof(GLuint) * shaderCount_ak);
        glGetAttachedShaders(gShaderProgramObject_ak, shaderCount_ak, &shaderCount_ak, pShaders_ak);

        for (int i_ak = 0; i_ak < shaderCount_ak; i_ak++) {
            glDetachShader(gShaderProgramObject_ak, pShaders_ak[i_ak]);
            glDeleteShader(pShaders_ak[i_ak]);
            pShaders_ak[i_ak] = 0;
        }

        free(pShaders_ak);
        glDeleteProgram(gShaderProgramObject_ak);
        gShaderProgramObject_ak = 0;

        glUseProgram(0);
    }
    [super dealloc];
}

@end

// Global C space functions
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp *outputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void *displayLinkContext)
{
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];
    return(result);
}
