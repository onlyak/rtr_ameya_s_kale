#include "DS_Proj.h"

void makeRasterFont(void)
{
	GLuint i, j, k, s;
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	fontOffset = glGenLists(128);

	for (i = 0, j = 'A'; i < 26; i++, j++)
	{
		glNewList(fontOffset + j, GL_COMPILE);
		glBitmap(8, 13, 0.0f, 0.0f, 11.0f, 0.0f, letters[i]);
		glEndList();
	}

	for (k = 0, s = '0'; k < 10; k++, s++)
	{
		glNewList(fontOffset + s, GL_COMPILE);
		glBitmap(8, 13, 0.0f, 2.0f, 10.0f, 0.0f, digits[k]);
		glEndList();
	}

	glNewList(fontOffset + ' ', GL_COMPILE);
	glBitmap(8, 13, 0.0f, 2.0f, 10.0f, 0.0f, space);
	glEndList();
}

void printString(const char *s)
{
	glPushAttrib(GL_LIST_BIT);
	glListBase(fontOffset);
	glCallLists(strlen(s), GL_UNSIGNED_BYTE, (GLubyte *)s);
	glPopAttrib();
}