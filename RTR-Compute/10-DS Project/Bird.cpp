#include "DS_Proj.h"

void Bird()
{
	void drawLeaf();
	b = gluNewQuadric();
	glRotatef(180.0f, 1.0f, 0.0f, 0.0);

	glPushMatrix();
	glTranslatef(-0.2, 0.7, 0.0);
	glColor3f(0.5f, 0.2f, 0.1f);
	glScalef(1.6f, 1.0f, 1.0f);
	gluSphere(b, 0.5, 32, 32); //body
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.5f, 0.5f, 0.0f);

	glRotatef(eat, 0.0f, 0.0f, 1.0f);
	glPushMatrix();
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glRotatef(45.0f, 0.0f, 1.0f, 0.0f);
	gluCylinder(b, 0.1f, 0.08f, 0.7f, 30, 30); //neck
	glTranslatef(0.0f, 0.0f, 0.9f);

	glPushMatrix();
	glColor3f(0.5f, 0.4f, 0.3f);
	gluSphere(b, 0.3, 32, 32); //head
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-0.2f, 0.0f, 0.0f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 0.5f);
	gluCylinder(b, 0.15f, 0.01f, 0.8f, 30, 30); //beak
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.2f, 0.15f, 0.15f);
	glColor3f(0.0f, 0.0f, 0.0f);
	gluSphere(b, 0.05, 30, 30); //eyes
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.2f, -0.15f, 0.15f);
	glColor3f(0.0f, 0.0f, 0.0f);
	gluSphere(b, 0.05, 30, 30); //eyes
	glPopMatrix();

	glPopMatrix();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-0.1, 1.7f, 0.2f);
	glPushMatrix();
	glColor3f(1.0f, 1.0f, 0.5f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	gluCylinder(b, 0.05f, 0.05f, 0.7f, 30, 30); //leg_1
	glPopMatrix();

	glPushMatrix();
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glRotatef(130.0f, 0.0f, 1.0f, 0.0f);
	gluCylinder(b, 0.05f, 0.05f, 0.3f, 30, 30); //leg_1_1
	glPopMatrix();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-0.1, 1.7f, -0.2f);
	glPushMatrix();
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	gluCylinder(b, 0.05f, 0.05f, 0.7f, 30, 30); //leg_2
	glPopMatrix();

	glPushMatrix();
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glRotatef(130.0f, 0.0f, 1.0f, 0.0f);
	gluCylinder(b, 0.05f, 0.05f, 0.3f, 30, 30); //leg_2_1
	glPopMatrix();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-1.0f, -0.2f, 0.0f);
	glRotatef(90.0f, 0.0, 1.0f, 0.0f);
	glColor3f(0.4f, 0.3f, 0.3f);
	drawLeaf();
	glPushMatrix();
	glTranslatef(0.0f, 1.0f, -0.001f);
	glRotatef(150.0f, 0.0, 0.0f, 1.0f);
	glColor3f(0.4f, 0.3f, 0.3f);
	drawLeaf();
	glPopMatrix();
	glPushMatrix();
	glTranslatef(0.0f, 1.0f, -0.001f);
	glRotatef(-150.0f, 0.0, 0.0f, 1.0f);
	glColor3f(0.4f, 0.3f, 0.3f);
	drawLeaf();
	glPopMatrix();
	glPopMatrix();
}