#pragma once

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "GLU32.lib")
#pragma comment(lib, "Winmm.lib")

extern int displaySteps;
extern int main_step;
extern float step;

#define SONG_ID 99

extern GLUquadric *quadric_ak;
extern GLUquadric *sphere_ak;
extern GLUquadric *cylinder_ak;
extern GLUquadric *b;

extern float zPosition;
extern float xPosition;
extern float eat;
extern bool eatBool;
extern float legAngle[2];
extern float armAngle[2];
 
extern float fist_angle;
extern float walkPosition;
extern float sideWayWalk;
extern bool isHumanoidWalking;
extern float pelvisAngle;
extern float pelvisYPosition;
extern float torsoAngle;
extern bool isPelvisInMotion;
extern float yPosition;

extern bool gbJump;
extern float fruitDrop;
extern float birdMov;
 
extern bool leg1;  
extern bool leg2;
extern bool arm1;
extern bool arm2;

extern GLubyte space[];
extern GLubyte letters[][13];
extern GLubyte digits[][13];
extern GLuint fontOffset;

extern void makeRasterFont(void);
extern void printString(const char *);
extern void Name();

extern void DrawTorso(void);
extern void DrawNeck(void);
extern void DrawHead(void);
extern void DrawRightArm(void);
extern void DrawLeftArm(void);
extern void DrawFractalTree(GLfloat, GLfloat);
extern void DrawRightLeg(float, float, float, float);
extern void DrawLeftLeg(float, float, float, float);
extern void Bird();
extern void DrawRobot(GLfloat, GLfloat, GLfloat);
extern void DrawFruit();
extern void DrawFood();
extern void DrawSphere();
extern void DrawCube(float,float,float);


