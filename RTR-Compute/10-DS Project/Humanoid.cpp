#include "DS_Proj.h"

void DrawCube(float xPos, float yPos, float zPos)
{
	glPushMatrix();
	glBegin(GL_POLYGON);

	//This is the top face
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, -1.0f);
	glVertex3f(-1.0f, 0.0f, -1.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);

	//This is the front face
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);

	//This is the right face*/
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, -1.0f);
	glVertex3f(0.0f, 0.0f, -1.0f);

	//This is the left face
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);

	//This is the bottom face
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);

	//This is the back face
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(0.0f, -1.0f, -1.0f);

	glEnd();
	glPopMatrix();
}

void DrawHead(float xPos, float yPos, float zPos)
{
	glPushMatrix();

	glColor3f(1.0f, 1.0f, 0.5f);
	glTranslatef(xPos, yPos, zPos);
	GLUquadric *sphere_ak = gluNewQuadric();
	glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);

	gluSphere(sphere_ak, 1.3f, 30, 30);

	glPopMatrix();
}


void DrawTorso(float xPos, float yPos, float zPos)
{
	glPushMatrix();

	glColor3f(0.0f, 0.0f, 0.9f);
	glTranslatef(xPos, yPos, zPos);

	glScalef(3.0f, 5.0f, 1.0f);
	DrawCube(0.0f, 0.0f, 0.0f);

	glPopMatrix();
}

void DrawArmLeft(float xPos, float yPos, float zPos)
{
	glPushMatrix();
	glColor3f(0.0f, 0.0f, 1.0f);
	glTranslatef(xPos, yPos, zPos);

	glPushMatrix();

	glScalef(1.0f, 4.0f, 1.0f);
	DrawCube(0.0f, 0.0f, 0.0f);

	glPopMatrix();

	glPushMatrix();
	glTranslatef(xPos - 2.5, yPos - 4, zPos);
	glRotatef(fist_angle, 1.0f, 0.0f, 0.0f);
	glPushMatrix();
	glScalef(1.0f, 4.0f, 1.0f);
	glColor3f(1.0f, 1.0f, 0.5f);
	DrawCube(0.0f, 0.0f, 0.0f);
	glPopMatrix();
	glPopMatrix();
	glPopMatrix();
}

void DrawArmRight(float xPos, float yPos, float zPos)
{
	glPushMatrix();
	glColor3f(0.0f, 0.0f, 1.0f);
	glTranslatef(xPos, yPos, zPos);

	glPushMatrix();

	glScalef(1.0f, 4.0f, 1.0f);
	DrawCube(0.0f, 0.0f, 0.0f);

	glPopMatrix();

	glPushMatrix();
	glTranslatef(xPos + 1.5, yPos - 4, zPos);
	glRotatef(fist_angle, 1.0f, 0.0f, 0.0f);
	glPushMatrix();
	glScalef(1.0f, 4.0f, 1.0f);
	glColor3f(1.0f, 1.0f, 0.5f);
	DrawCube(0.0f, 0.0f, 0.0f);
	glPopMatrix();
	glPopMatrix();
	glPopMatrix();
}

void DrawPelvis(float xPos, float yPos, float zPos)
{
	glPushMatrix();
	glColor3f(0.4f, 0.6f, 1.0f);
	glTranslatef(xPos, yPos, zPos);

	glScalef(3.0f, 1.0f, 1.0f);
	DrawCube(0.0f, 0.0f, 0.0f);

	glPopMatrix();
}


void DrawLeftLeg(float xPos, float yPos, float zPos, float angle)
{
	
	glPushMatrix();
	glColor3f(0.3f, 0.5f, 1.0f);
	glTranslatef(xPos, yPos, zPos);

	glPushMatrix();
	glScalef(1.0f, 5.0f, 1.0f);
	DrawCube(0.0f, 0.0f, 0.0f);
	glPopMatrix();
	
	glPushMatrix();

	glTranslatef(xPos + 0.5, yPos, zPos + 0.5);
	glRotatef(angle + 20.0f, 1.0f, 0.0f, 0.0f);
	glPushMatrix();
	glScalef(1.0f, 3.0f, 1.0f);
	glColor3f(0.5f, 0.5f, 1.0f);
	DrawCube(0.0f, 0.0f, 0.0f);
	glPopMatrix();
	glPopMatrix();
	glPopMatrix();
}

void DrawRightLeg(float xPos, float yPos, float zPos, float angle)
{
	glPushMatrix();
	glColor3f(0.3f, 0.5f, 1.0f);
	glTranslatef(xPos, yPos, zPos);

	glPushMatrix();
	glScalef(1.0f, 5.0f, 1.0f);
	DrawCube(0.0f, 0.0f, 0.0f);
	glPopMatrix();
	
	glPushMatrix();

	glTranslatef(xPos - 1.5, yPos, zPos + 0.5);
	glRotatef(angle + 20.0f, 1.0f, 0.0f, 0.0f);
	glPushMatrix();
	glScalef(1.0f, 3.0f, 1.0f);
	glColor3f(0.5f, 0.5f, 1.0f);
	DrawCube(0.0f, 0.0f, 0.0f);
	glPopMatrix();
	glPopMatrix();
	glPopMatrix();
}

void DrawRobot(float xPos, float yPos, float zPos)
{
	if (main_step == 1)
	{
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		isHumanoidWalking = true;
		if (isHumanoidWalking && sideWayWalk <= 17.0f)
		{
			sideWayWalk += 0.01f;
		}
		else
		{
			isHumanoidWalking = false;
			glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
		}
		if (!isHumanoidWalking && sideWayWalk >= 17.0f && gbJump)
		{
			if (pelvisYPosition <= -2.5f)
			{
				pelvisYPosition += 0.002f; //
				armAngle[0] = 540.0f;
			}
			else
			{
				gbJump = false;
			}
		}
		else if (!gbJump)
		{
			if (pelvisYPosition >= -4.0f)
			{
				pelvisYPosition -= 0.002f;
				if (fruitDrop >= -7.0f)
					fruitDrop -= 0.02f;
				armAngle[0] = 360.0f;
			}

			else
			{
				if (birdMov <= 1.0f)
					birdMov += 0.01f;
				displaySteps = 0;
			}
		}
	}

	glPushMatrix();
	glTranslatef(xPos, yPos, zPos); 

	if (main_step == 8)
	{
		glRotatef(torsoAngle, 1.0f, 0.0f, 0.0f);
	}

	DrawTorso(1.5f, 0.0f, 0.0f);
	DrawHead(0.0f, 1.0f, 0.0f);
	glPushMatrix();
	if (isHumanoidWalking)
	{
		if (arm1)
			armAngle[0] = armAngle[0] + 0.1f;
		else
			armAngle[0] = armAngle[0] - 0.1f;
	}

	if (armAngle[0] >= 15.0f)
		arm1 = false;
	if (armAngle[0] <= -15.0f)
		arm1 = true;

	glTranslatef(0.0f, -0.5f, 0.0f);
	glRotatef(armAngle[0], 1.0f, 0.0f, 0.0f);
	DrawArmLeft(2.5f, 0.0f, 0.0f);
	glPopMatrix();

	glPushMatrix();
	if (isHumanoidWalking)
	{
		if (arm2)
			armAngle[1] = armAngle[1] + 0.1f;
		else
			armAngle[1] = armAngle[1] - 0.1f;
	}

	
	if (armAngle[1] >= 15.0f)
		arm2 = false;
	if (armAngle[1] <= -15.0f)
		arm2 = true;

	glTranslatef(0.0f, -0.5f, 0.0f);
	glRotatef(armAngle[1], 1.0f, 0.0f, 0.0f);
	DrawArmRight(-1.5f, 0.0f, 0.0f);
	glPopMatrix();

	glPushMatrix();
	if (isPelvisInMotion)
	{
		glRotatef(pelvisAngle, 1.0f, 0.0f, 0.0f);
	}

	DrawPelvis(1.5f, -4.5f, 0.0f);

	glPushMatrix();

	if (isHumanoidWalking)
	{
		if (leg1)
			legAngle[0] = legAngle[0] + 0.1f;
		else
			legAngle[0] = legAngle[0] - 0.1f;
	}

	if (legAngle[0] >= 15.0f)
		leg1 = false;
	if (legAngle[0] <= -15.0f)
		leg1 = true;

	glTranslatef(0.0f, -0.5f, 0.0f);
	glRotatef(legAngle[0], 1.0f, 0.0f, 0.0f);
	DrawLeftLeg(-0.5f, -4.5f, -0.5f, legAngle[0]);
	glPopMatrix();

	glPushMatrix();
	if (isHumanoidWalking)
	{
		if (leg2)
			legAngle[1] = legAngle[1] + 0.1f;
		else
			legAngle[1] = legAngle[1] - 0.1f;
	}

	if (legAngle[1] >= 15.0f)
		leg2 = false;
	if (legAngle[1] <= -15.0f)
		leg2 = true;

	glTranslatef(0.0f, -0.5f, 0.0f);
	glRotatef(legAngle[1], 1.0f, 0.0f, 0.0f);
	DrawRightLeg(1.5f, -4.5f, -0.5f, legAngle[1]);

	glPopMatrix();
	glPopMatrix();
	glPopMatrix();
}