#include "DS_Proj.h"
#include "icon.h"
#include <windowsx.h>

#define WIN_WIDTH_ak 800
#define WIN_HEIGHT_ak 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE *gpFile_ak = NULL;
HWND ghwnd_ak = NULL;
DWORD dwStyle_ak = NULL;
WINDOWPLACEMENT wpPrev_ak = {sizeof(WINDOWPLACEMENT)};
bool gbFullScreen_ak = false;
HDC ghdc_ak = NULL;
HGLRC ghrc_ak = NULL;
bool gbActiveWindow_ak = false;

int displaySteps = 2;
int main_step = 0;
float step = 0;

GLUquadric *quadric_ak = NULL;
GLUquadric *sphere_ak = NULL;
GLUquadric *cylinder_ak = NULL;
GLUquadric *b = NULL;

float zPosition = 36.0f;
float xPosition = 0.0f;
float eat = 0.0f;
bool eatBool = true;
float legAngle[2] = {0.0f, 0.0f};
float armAngle[2] = {0.0f, 0.0f};

float fist_angle = 0.0f;
float walkPosition = 0.0f;
float sideWayWalk = 0.0f;
bool isHumanoidWalking = true;
float pelvisAngle = 0.0f;
float pelvisYPosition = -4.0f;
float torsoAngle = 0.0f;
bool isPelvisInMotion = true;
float yPosition = 0.0f;

bool gbJump = true;
float fruitDrop = 0.0f;
float birdMov = 0.0f;

bool leg1 = true;  
bool leg2 = false;

bool arm1 = true;
bool arm2 = false;

BOOL song_ak;
HINSTANCE gInstance_ak = NULL;

GLubyte space[] = {0x00,
				   0x00,
				   0x00,
				   0x00,
				   0x00,
				   0x00,
				   0x00,
				   0x00,
				   0x00,
				   0x00,
				   0x00,
				   0x00,
				   0x00};

GLubyte letters[][13] = {
	{0x00, 0x00, 0xc3, 0xc3, 0xc3, 0xc3, 0xff, 0xc3, 0xc3, 0xc3, 0x66, 0x3c, 0x18},
	{0x00, 0x00, 0xfe, 0xc7, 0xc3, 0xc3, 0xc7, 0xfe, 0xc7, 0xc3, 0xc3, 0xc7, 0xfe},
	{0x00, 0x00, 0x7e, 0xe7, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xe7, 0x7e},
	{0x00, 0x00, 0xfc, 0xce, 0xc7, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc7, 0xce, 0xfc},
	{0x00, 0x00, 0xff, 0xc0, 0xc0, 0xc0, 0xc0, 0xfc, 0xc0, 0xc0, 0xc0, 0xc0, 0xff},
	{0x00, 0x00, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xfc, 0xc0, 0xc0, 0xc0, 0xff},
	{0x00, 0x00, 0x7e, 0xe7, 0xc3, 0xc3, 0xcf, 0xc0, 0xc0, 0xc0, 0xc0, 0xe7, 0x7e},
	{0x00, 0x00, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xff, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3},
	{0x00, 0x00, 0x7e, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x7e},
	{0x00, 0x00, 0x7c, 0xee, 0xc6, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06},
	{0x00, 0x00, 0xc3, 0xc6, 0xcc, 0xd8, 0xf0, 0xe0, 0xf0, 0xd8, 0xcc, 0xc6, 0xc3},
	{0x00, 0x00, 0xff, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0},
	{0x00, 0x00, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xdb, 0xff, 0xff, 0xe7, 0xc3},
	{0x00, 0x00, 0xc7, 0xc7, 0xcf, 0xcf, 0xdf, 0xdb, 0xfb, 0xf3, 0xf3, 0xe3, 0xe3},
	{0x00, 0x00, 0x7e, 0xe7, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xe7, 0x7e},
	{0x00, 0x00, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xfe, 0xc7, 0xc3, 0xc3, 0xc7, 0xfe},
	{0x00, 0x00, 0x3f, 0x6e, 0xdf, 0xdb, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0x66, 0x3c},
	{0x00, 0x00, 0xc3, 0xc6, 0xcc, 0xd8, 0xf0, 0xfe, 0xc7, 0xc3, 0xc3, 0xc7, 0xfe},
	{0x00, 0x00, 0x7e, 0xe7, 0x03, 0x03, 0x07, 0x7e, 0xe0, 0xc0, 0xc0, 0xe7, 0x7e},
	{0x00, 0x00, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0xff},
	{0x00, 0x00, 0x7e, 0xe7, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3},
	{0x00, 0x00, 0x18, 0x3c, 0x3c, 0x66, 0x66, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3},
	{0x00, 0x00, 0xc3, 0xe7, 0xff, 0xff, 0xdb, 0xdb, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3},
	{0x00, 0x00, 0xc3, 0x66, 0x66, 0x3c, 0x3c, 0x18, 0x3c, 0x3c, 0x66, 0x66, 0xc3},
	{0x00, 0x00, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x3c, 0x3c, 0x66, 0x66, 0xc3},
	{0x00, 0x00, 0xff, 0xc0, 0xc0, 0x60, 0x30, 0x7e, 0x0c, 0x06, 0x03, 0x03, 0xff}};

GLubyte digits[][13] = {
	{0x00, 0xFF, 0xFF, 0xC3, 0xC3, 0xC3, 0xC3, 0xC3, 0xC3, 0xC3, 0xC3, 0xFF, 0xFF},
	{0x00, 0xFF, 0xFF, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x98, 0xD8, 0x78, 0x38},
	{0x00, 0xFF, 0xFF, 0xC0, 0xC0, 0xC0, 0xFF, 0xFF, 0x03, 0x03, 0x03, 0xFF, 0xFF},
	{0x00, 0xFF, 0xFF, 0x03, 0x03, 0x03, 0xFF, 0xFF, 0x03, 0x03, 0x03, 0xFF, 0xFF},
	{0x00, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0xFF, 0xFF, 0xC3, 0xC3, 0xC3, 0xC3},
	{0x00, 0xFF, 0xFF, 0x03, 0x03, 0x03, 0xFF, 0xFF, 0xC0, 0xC0, 0xC0, 0xFF, 0xFF},
	{0x00, 0xFF, 0xFF, 0xC3, 0xC3, 0xC3, 0xFF, 0xFF, 0xC0, 0xC0, 0xC0, 0xFF, 0xFF},
	{0x00, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0xFF, 0xFF},
	{0x00, 0xFF, 0xFF, 0xC3, 0xC3, 0xC3, 0xFF, 0xFF, 0xC3, 0xC3, 0xC3, 0xFF, 0xFF},
	{0x00, 0xFF, 0xFF, 0x03, 0x03, 0x03, 0xFF, 0xFF, 0xC3, 0xC3, 0xC3, 0xFF, 0xFF}};

GLuint fontOffset;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void Initialize(void);
	void Display(void);

	WNDCLASSEX wndclass_ak;
	HWND hwnd_ak;
	MSG msg_ak;
	TCHAR szAppName_ak[] = TEXT("MyApp");
	bool bDone_ak = false;

	if (fopen_s(&gpFile_ak, "logs.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Failure creating file"), TEXT("File IO failed"), MB_ICONSTOP | MB_OK);
	}
	else
	{
		fprintf(gpFile_ak,"Log File Starts Here ...");
	}

	int iScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	int iScreenHeight = GetSystemMetrics(SM_CYSCREEN);

	int iScreenCenterX = iScreenWidth / 2;
	int iScreenCenterY = iScreenHeight / 2;

	int iWindowCenterX = WIN_WIDTH_ak / 2;
	int iWindowsCenterY = WIN_HEIGHT_ak / 2;

	int iWindowX = iScreenCenterX - iWindowCenterX;
	int iWindowY = iScreenCenterY - iWindowsCenterY;

	wndclass_ak.cbSize = sizeof(WNDCLASSEX);
	wndclass_ak.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass_ak.cbClsExtra = 0;
	wndclass_ak.cbWndExtra = 0;
	wndclass_ak.hInstance = hInstance;
	wndclass_ak.lpfnWndProc = WndProc;
	wndclass_ak.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));
	wndclass_ak.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass_ak.lpszClassName = szAppName_ak;
	wndclass_ak.lpszMenuName = NULL;
	wndclass_ak.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass_ak.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));

	RegisterClassEx(&wndclass_ak);

	hwnd_ak = CreateWindowEx(WS_EX_APPWINDOW,
							  szAppName_ak,
							  TEXT("Ameya's Project"),
							  WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
							  iWindowX,
							  iWindowY,
							  WIN_WIDTH_ak,
							  WIN_HEIGHT_ak,
							  NULL,
							  NULL,
							  hInstance,
							  NULL);
	ghwnd_ak = hwnd_ak;

	Initialize();
	ShowWindow(hwnd_ak, iCmdShow);
	SetForegroundWindow(hwnd_ak);
	SetFocus(hwnd_ak);

	while (bDone_ak == false)
	{
		if (PeekMessage(&msg_ak, NULL, 0, 0, PM_REMOVE))
		{
			if (msg_ak.message == WM_QUIT)
			{
				bDone_ak = true;
			}
			else
			{
				TranslateMessage(&msg_ak);
				DispatchMessage(&msg_ak);
			}
		}
		else
		{
			if (gbActiveWindow_ak == true)
			{
				Display();
			}
		}
	}

	return ((int)msg_ak.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen(void);
	void Resize(int, int);
	void Uninitialize(void);

	switch (iMsg)
	{
	case WM_SETFOCUS:
	{
		gbActiveWindow_ak = true;
		break;
	}
	case WM_KILLFOCUS:
	{
		gbActiveWindow_ak = false;
		break;
	}
	case WM_ERASEBKGND:
	{
		return (0);
	}
	case WM_SIZE:
	{
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	}
	case WM_KEYDOWN:
	{
		switch (wParam)
		{
		case VK_ESCAPE:
		{
			DestroyWindow(hwnd);
			break;
		}
		case 0x46:
		case 0x66:
		{
			ToggleFullScreen();
			break;
		}
		default:
		{
			break;
		}
		}
		break;
	}
	case WM_CHAR:
	{
		switch (wParam)
		{
		case 's':
			zPosition += 0.1f;
			break;
		case 'w':
			zPosition -= 0.1f;
			break;
		case 'a':
			xPosition -= 0.1f;
			break;
		case 'd':
			xPosition += 0.1f;
			break;
		case 'e':
			yPosition += 0.1f;
			break;
		case 'r':
			yPosition -= 0.1f;
			break;
		default:
			break;
		}
		break;
	}
	case WM_CLOSE:
	{
		DestroyWindow(hwnd);
		break;
	}
	case WM_DESTROY:
	{
		Uninitialize();
		PostQuitMessage(0);
		break;
	}
	case WM_CREATE:
		song_ak = PlaySound(MAKEINTRESOURCE(SONG_ID), gInstance_ak, SND_RESOURCE | SND_ASYNC);
		fprintf(gpFile_ak, "%dLog File completeeeee", song_ak);
	default:
	{
		break;
	}
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	MONITORINFO mi_ak = {sizeof(MONITORINFO)};

	if (gbFullScreen_ak == false)
	{
		dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);
		if ((dwStyle_ak & WS_OVERLAPPEDWINDOW))
		{
			if ((GetWindowPlacement(ghwnd_ak, &wpPrev_ak) && (GetMonitorInfo(MonitorFromWindow(ghwnd_ak, MONITORINFOF_PRIMARY), &mi_ak))))
			{
				SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak & (~WS_OVERLAPPEDWINDOW)));
				SetWindowPos(ghwnd_ak,
							 HWND_TOP,
							 mi_ak.rcMonitor.left,
							 mi_ak.rcMonitor.top,
							 (mi_ak.rcMonitor.right - mi_ak.rcMonitor.left),
							 (mi_ak.rcMonitor.bottom - mi_ak.rcMonitor.top),
							 SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen_ak = true;
	}
	else
	{
		SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
		SetWindowPos(ghwnd_ak,
					 HWND_TOP,
					 0,
					 0,
					 0,
					 0,
					 (SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOOWNERZORDER));
		ShowCursor(TRUE);
		gbFullScreen_ak = false;
	}
}

void Initialize(void)
{
	void Resize(int, int);

	PIXELFORMATDESCRIPTOR pFD_ak;
	int iPixelFormatIndex_ak;

	ghdc_ak = GetDC(ghwnd_ak);

	ZeroMemory(&pFD_ak, sizeof(PIXELFORMATDESCRIPTOR));
	pFD_ak.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pFD_ak.nVersion = 1;
	pFD_ak.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pFD_ak.iPixelType = PFD_TYPE_RGBA;
	pFD_ak.cColorBits = 32;
	pFD_ak.cRedBits = 8;
	pFD_ak.cGreenBits = 8;
	pFD_ak.cBlueBits = 8;
	pFD_ak.cAlphaBits = 8;
	pFD_ak.cDepthBits = 32;

	iPixelFormatIndex_ak = ChoosePixelFormat(ghdc_ak, &pFD_ak);
	if (iPixelFormatIndex_ak == 0)
	{
		DestroyWindow(ghwnd_ak);
	}

	if (SetPixelFormat(ghdc_ak, iPixelFormatIndex_ak, &pFD_ak) == FALSE)
	{
		DestroyWindow(ghwnd_ak);
	}

	ghrc_ak = wglCreateContext(ghdc_ak);
	if (ghrc_ak == NULL)
	{
		DestroyWindow(ghwnd_ak);
	}

	if (wglMakeCurrent(ghdc_ak, ghrc_ak) == FALSE)
	{
		DestroyWindow(ghwnd_ak);
	}

	makeRasterFont();
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	Resize(WIN_WIDTH_ak, WIN_HEIGHT_ak);
}

void Resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
}

void Display(void)
{
	static GLfloat angle = 0.0f;
	static bool inc = false;
	bool startStop = true;
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if (displaySteps == 2)
	{
		glTranslatef(0.0f, 0.0f, -6.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glRasterPos3f(-1.0f, 0.0f, 0.0f);
		printString("ASTROMEDICOMP PRESENTS");
		if(step<=300)
		{
			step+=0.1f;
		}
		else
		{
			displaySteps=1;
		}
		
	}

	if (displaySteps == 0)
	{
		glTranslatef(0.0f, 0.0f, -4.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glTranslatef(0.0f, 0.0f, -3.0f);
		Name();
	}

	if (displaySteps == 1)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		gluLookAt(xPosition, yPosition, zPosition,
				  0.0f, 0.0f, 0.0f,
				  0.0f, 1.0f, 0.0f);

		glPushMatrix();
		glScalef(4.0f, 2.5f, 1.0f);
		glBegin(GL_QUADS);

		// right - green
		glColor3f(0.0f, 1.0f, 1.0f);
		glVertex3f(6.0f, 6.0f, -48.0f);
		glVertex3f(6.0f, 6.0f, 6.0f);
		glVertex3f(6.0f, -6.0f, 6.0f);
		glVertex3f(6.0f, -6.0f, -48.0f);

		// back - blue
		glColor3f(0.0f, 1.0f, 1.0f);
		glVertex3f(-6.0f, 6.0f, -48.0f);
		glVertex3f(6.0f, 6.0f, -48.0f);
		glVertex3f(6.0f, -6.0f, -48.0f);
		glVertex3f(-6.0f, -6.0f, -48.0f);

		// left - cyan
		glColor3f(0.0f, 1.0f, 1.0f);
		glVertex3f(-6.0f, 6.0f, 6.0f);
		glVertex3f(-6.0f, 6.0f, -48.0f);
		glVertex3f(-6.0f, -6.0f, -48.0f);
		glVertex3f(-6.0f, -6.0f, 6.0f);

		// top - magenta
		glColor3f(0.0f, 1.0f, 1.0f);
		glVertex3f(6.0f, 6.0f, -48.0f);
		glVertex3f(-6.0f, 6.0f, -48.0f);
		glVertex3f(-6.0f, 6.0f, 6.0f);
		glVertex3f(6.0f, 6.0f, 6.0f);

		// bottom - yellow
		glColor3f(0.0f, 0.9f, 0.2f);
		glVertex3f(6.0f, -6.0f, -48.0f);
		glVertex3f(-6.0f, -6.0f, -48.0f);
		glVertex3f(-6.0f, -6.0f, 6.0f);
		glVertex3f(6.0f, -6.0f, 6.0f);

		glEnd();
		glPopMatrix();

		glPushMatrix();
		glTranslatef(10.0f, -12.0f, 5.0f);
		glScalef(1.5f, 1.5f, 1.5f);
		DrawFractalTree(2.0f, 0.2f);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(5.0f, -12.0f, -3.0f);
		glScalef(1.0f, 1.0f, 1.0f);
		DrawFractalTree(2.0f, 0.2f);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-12.0f, -12.0f, -15.0f);
		glScalef(0.9f, 0.9f, 0.9f);
		DrawFractalTree(2.0f, 0.2f);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(15.0f + birdMov, -12.0f, 0.0f);
		glRotatef(-180.0f, 0.0f, 1.0f, 0.0f);
		Bird();
		glPopMatrix();

		glPushMatrix();
		glTranslatef(2.0f, -12.0f, -7.0f);
		glRotatef(-180.0f, 0.0f, 1.0f, 0.0f);
		glRotatef(185.0f, 0.0f, 1.0f, 0.0f);
		glScalef(0.8f, 0.8f, 0.8f);
		Bird();
		glTranslatef(2.0f,2.2f,3.0f);
		DrawFood();
		glTranslatef(0.5f,0.5f,0.5f);
		DrawFood();
		glTranslatef(-0.7f,0.5f,1.5f);
		DrawFood();
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-7.8f + sideWayWalk, pelvisYPosition - 6.0f, -36.0f + walkPosition);
		glScalef(0.3f, 0.3f, 0.3f);
		//glRotatef(90, 0.0f, 1.0f, 0.0f);
		DrawRobot(0.0f, 0.0f, 0.0f);
		glPopMatrix();

		glTranslatef(10.0f, -7.0f + fruitDrop, 0.0f);
		DrawFruit();

		if (eatBool)
		{
			if (eat <= 60.0f)
			{
				eat += 0.2f;
			}
			else
			{
				eatBool = false;
			}
		}

		else if (!eatBool)
		{
			if (eat >= 0.0f)
				eat -= 0.2f;
			else
			{
				eatBool = true;
			}
		}

		if (isHumanoidWalking && walkPosition <= 25.0f)
		{
			walkPosition += 0.02f;
		}
		else
		{
			isHumanoidWalking = false;
		}

		if (!isHumanoidWalking)
		{
			armAngle[0] += 0.1f;
			armAngle[1] += 0.1f;

			if (armAngle[0] >= 360)
			{
				armAngle[0] -= 0.1f;
				isHumanoidWalking = true;
				startStop = false;
			}

			if (armAngle[1] >= 360)
			{
				armAngle[1] -= 0.1f;
				isHumanoidWalking = true;
				startStop = false;
			}

			if (!startStop)
			{
				if (walkPosition <= 37.0f)
				{
					walkPosition += 0.01f;
				}
				else
				{
					isHumanoidWalking = false;
					main_step = 1;
				}
			}
		}
	}
	SwapBuffers(ghdc_ak);
}



void Uninitialize(void)
{
	if (gbFullScreen_ak == true)
	{
		dwStyle_ak = GetWindowLong(ghwnd_ak, GWL_STYLE);

		SetWindowLong(ghwnd_ak, GWL_STYLE, (dwStyle_ak | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd_ak, &wpPrev_ak);
		SetWindowPos(ghwnd_ak,
					 HWND_TOP,
					 0,
					 0,
					 0,
					 0,
					 SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc_ak)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc_ak)
	{
		wglDeleteContext(ghrc_ak);
		ghrc_ak = NULL;
	}

	if (ghdc_ak)
	{
		ReleaseDC(ghwnd_ak, ghdc_ak);
		ghdc_ak = NULL;
	}

	if (quadric_ak)
	{
		gluDeleteQuadric(quadric_ak);
		quadric_ak = NULL;
	}

	if (sphere_ak)
	{
		gluDeleteQuadric(sphere_ak);
		sphere_ak = NULL;
	}

	if (cylinder_ak)
	{
		gluDeleteQuadric(cylinder_ak);
		cylinder_ak = NULL;
	}

	if (b)
	{
		gluDeleteQuadric(b);
		b = NULL;
	}

	if (gpFile_ak)
	{
		fprintf(gpFile_ak,"Log File complete");
		fclose(gpFile_ak);
		gpFile_ak = NULL;
	}
}


