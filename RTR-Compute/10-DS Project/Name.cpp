#include "DS_Proj.h"

void Name()
{
    glLineWidth(5.0f);
    glBegin(GL_LINES);

    glColor3f(0.1f, 0.1f, 0.1f);
    glVertex3f(-0.7f, 0.7f, 0.0f);
    glVertex3f(-0.9f, -0.7f, 0.0f);

    glColor3f(0.1f, 0.1f, 0.1f);

    glVertex3f(-0.7f, 0.7f, 0.0f);
    glVertex3f(-0.5f, -0.7f, 0.0f);

    glColor3f(0.6f, 0.6f, 0.6f);

    glVertex3f(-0.49f, -0.7f, 0.0f);
    glVertex3f(-0.49f, 0.03f, 0.0f);

    glVertex3f(-0.847f, -0.33f, 0.0f);
    glVertex3f(-0.55f, -0.33f, 0.0f);

    glVertex3f(-0.49f, 0.03f, 0.0f);
    glVertex3f(-0.39f, -0.5f, 0.0f);

    glVertex3f(-0.39f, -0.5f, 0.0f);
    glVertex3f(-0.29f, 0.026f, 0.0f);

    glColor3f(0.6f, 0.6f, 0.6f);

    glVertex3f(-0.29f, 0.026f, 0.0f);
    glVertex3f(-0.29f, -0.7f, 0.0f);

    glColor3f(0.1f, 0.1f, 0.1f);

    glVertex3f(-0.27f, -0.7f, 0.0f);
    glVertex3f(-0.27f, 0.026f, 0.0f);

    glVertex3f(-0.2f, -0.33f, 0.0f);
    glVertex3f(0.0f, -0.33f, 0.0f);

    glColor3f(0.6f, 0.6f, 0.6f);

    glVertex3f(-0.27f, -0.7f, 0.0f);
    glVertex3f(-0.1f, -0.7f, 0.0f);

    glVertex3f(-0.27f, 0.026f, 0.0f);
    glVertex3f(-0.1f, 0.026f, 0.0f);

    glVertex3f(-0.08f, 0.026f, 0.0f);
    glVertex3f(0.13f, -0.33f, 0.0f);

    glVertex3f(0.28f, 0.026f, 0.0f);
    glVertex3f(-0.05f, -0.7f, 0.0f);

    glColor3f(0.1f, 0.1f, 0.1f);

    glVertex3f(0.31f, 0.026f, 0.0f);
    glVertex3f(-0.01f, -0.7f, 0.0f);

    glVertex3f(0.31f, 0.026f, 0.0f);
    glVertex3f(0.7f, -0.7f, 0.0f);

    glColor3f(0.6f, 0.6f, 0.6f);

    glVertex3f(0.15f, -0.35f, 0.0f);
    glVertex3f(0.51f, -0.35f, 0.0f);

    glEnd();
}