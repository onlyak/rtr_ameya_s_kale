#include "DS_Proj.h"

struct node
{
	int data;
	struct node *left;
	struct node *right;
};

struct node *newNode(float data)
{
	struct node *node = (struct node *)malloc(sizeof(struct node));

	node->data = data;

	node->left = NULL;
	node->right = NULL;
	return (node);
}

void DrawSphere(GLfloat sphereRadius)
{
	sphere_ak = gluNewQuadric();
	glPushMatrix();
	glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
	gluSphere(sphere_ak, sphereRadius, 30, 30);
	glPopMatrix();
}

void DrawFractalTree(GLfloat branchHeight, GLfloat trunkWidth)
{
	void DrawBranch(GLfloat, GLfloat);
	void DrawSphere(GLfloat);

	glPushMatrix();
	DrawBranch(branchHeight, trunkWidth);
	glTranslatef(0.0f, branchHeight, 0.0f);

	branchHeight -= 0.3f;
	trunkWidth -= 0.06f;

	struct node *root = newNode(-50.0f);
	root->left = newNode(25.0f);
	root->right = newNode(30.0f);

	if (branchHeight >= 1.2f)
	{
		glPushMatrix();
		glRotatef(root->data, 1.0f, 0.0f, 1.0f);
		DrawFractalTree(branchHeight, trunkWidth);
		glPopMatrix();

		glPushMatrix();
		glRotatef(root->left->data, 0.0f, 1.0f, 1.0f);
		DrawFractalTree(branchHeight, trunkWidth);
		glPopMatrix();

		glPushMatrix();
		glRotatef(root->right->data, 1.0f, 1.0f, 0.0f);
		DrawFractalTree(branchHeight, trunkWidth);
		glPopMatrix();
	}
	else
	{
		glColor3f(0.1f, 0.5f, 0.0f);
		DrawSphere(0.80f);
	}
	glPopMatrix();
}

void DrawBranch(GLfloat branchHeight, GLfloat trunkWidth)
{
	cylinder_ak = gluNewQuadric();
	glColor3f(0.64f, 0.16f, 0.16f);
	glPushMatrix();
	glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
	GLfloat topRadius_ak = trunkWidth;
	GLfloat bottomRadius_ak = trunkWidth - (0.3f * trunkWidth);
	gluCylinder(cylinder_ak, topRadius_ak, bottomRadius_ak, branchHeight, 30, 30);
	glPopMatrix();
}

void drawLeaf(void)
{
	glBegin(GL_POLYGON);
	glVertex3f(0.0, 0.0, 0.0);
	glVertex3f(0.167, 0.083, 0.0);
	glVertex3f(0.225, 0.208, 0.0);
	glVertex3f(0.25, 0.42, 0.0);
	glVertex3f(0.23, 0.583, 0.0);
	glVertex3f(0.167, 0.75, 0.0);
	glVertex3f(0.0, 1.0, 0.0);
	glVertex3f(-0.167, 0.75, 0.0);
	glVertex3f(-0.23, 0.583, 0.0);
	glVertex3f(-0.25, 0.42, 0.0);
	glVertex3f(-0.225, 0.208, 0.0);
	glVertex3f(-0.167, 0.083, 0.0);
	glEnd();
}

void DrawFruit()
{
	GLUquadric *sphere_ak = gluNewQuadric();
	glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	gluSphere(sphere_ak, 0.3f, 30, 30);
}

void DrawFood()
{
	GLUquadric *sphere_ak = gluNewQuadric();
	glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	gluSphere(sphere_ak, 0.1f, 30, 30);
}